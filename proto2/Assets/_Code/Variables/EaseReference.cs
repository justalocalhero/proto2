﻿using System;
using DG.Tweening;

[Serializable]
public class EaseReference
{

    public bool UseConstant = true;
    public Ease ConstantValue;
    public EaseVariable Variable;

    public EaseReference()
    { }

    public EaseReference(Ease value)
    {
        UseConstant = true;
        ConstantValue = value;
    }

    public Ease Value
    {
        get { return UseConstant ? ConstantValue : Variable.Value; }
    }

    public static implicit operator Ease(EaseReference reference)
    {
        return reference.Value;
    }
}
