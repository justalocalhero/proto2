﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName="Names/UnitResourceNames")]
public class UnitResourceNameDictionary : ScriptableObject 
{
	public UnitResourceName[] unitResourceNames = new UnitResourceName[Enum.GetNames(typeof(UnitResourceType)).Length];

	public string GetResourceTypeName(UnitResourceType type)
	{
		int index = (int)type;
		if(index < unitResourceNames.Length)
		{
			return unitResourceNames[index].name;
		}
		else
		{			
			Debug.LogWarning("Type " + type + " name not found in Unit Resource Name Dictionary.");
			return "";
		}
	}

	public string GetString(UnitResourceType type)
	{
		string typeString = GetResourceTypeName(type);

		return typeString;
	}
}

[Serializable]
public struct UnitResourceName {public UnitResourceType type; public string name; };

