﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName="Talents/UnitResourceDictionary")]
public class UnitResourceDictionary : ScriptableObject
{
    public SpriteManager spriteManager;
    public UnitResourceStats[] stats;
    private UnitResourceBuilder builder = new UnitResourceBuilder();

	
	public void OnEnable()
	{
		ValidateSprites();
	}

	public int SetSpriteIndex(Sprite sprite)
	{
		if(sprite == null) return -1;
		if(spriteManager == null) return -1;

		int spriteIndex = spriteManager.GetIndex(sprite);
		if(spriteIndex == -1)
			spriteIndex = spriteManager.AddSprite(sprite);
		if(spriteIndex == -1)
			Debug.LogWarning("Panic");

        return spriteIndex;
	}

	public void OnValidate()
	{
		ValidateSprites();
	}

    public void ValidateSprites()
    {
        for(int i = 0; i < stats.Length; i++)
        {
            UnitResourceStats temp = stats[i];
            int index = ValidateSpriteIndex(temp.sprite, temp.spriteIndex);

            if(index != temp.spriteIndex)
            {
                temp.spriteIndex = index;
                stats[i] = temp;
            }
        }
    }
    
	public int ValidateSpriteIndex(Sprite sprite, int spriteIndex)
	{
		if(sprite == null) return -1;
		if(spriteManager == null) return -1;

		if(sprite == spriteManager.GetSprite(spriteIndex)) 
			return spriteIndex;
		else 
			return SetSpriteIndex(sprite);
	}

    public List<UnitResource> Generate()
    {
        List<UnitResource> toReturn = new List<UnitResource>();

        for(int i = 0; i < stats.Length; i++)
        {   
            UnitResourceStats current = stats[i];
            if(i != (int)current.type) Debug.LogWarning("Unit Resource Dictionary out of order at: " + i);
            toReturn.Add(
                builder.Begin()
                    .Type(current.type)
                    .SpriteIndex(current.spriteIndex)
                    .EquipStats(current.stats)
                    .BaseCap(current.baseCap)
                    .AbsoluteCap(current.absoluteCap)
                    .Build()
            );
        }

        return toReturn;
    }

    public static List<UnitResource> GenerateEmpty()
    {
        List<UnitResource> toReturn = new List<UnitResource>();
        UnitResourceBuilder tempBuilder = new UnitResourceBuilder();

        for(int i = 0; i < Enum.GetNames(typeof(UnitResourceType)).Length; i++)
        {
            toReturn.Add(
                tempBuilder.Begin()
                    .Type((UnitResourceType)i)
                    .BaseCap(0)
                    .AbsoluteCap(0)
                    .EquipStats(new List<EquipStat>())
                    .Build()
            );
        }

        return toReturn;
    }
}

[Serializable]
public struct UnitResourceStats { public UnitResourceType type; public int baseCap; public int absoluteCap; public List<EquipStat> stats; public Sprite sprite; public int spriteIndex;}