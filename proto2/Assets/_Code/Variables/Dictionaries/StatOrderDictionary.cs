﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "UI/StatOrderDictionary")]
public class StatOrderDictionary : ScriptableObject
{
	public StatType[] stats;
	
}