﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName="Names/ActionConditionNames")]
public class ActionConditionNameDictionary : ScriptableObject 
{
	public ConditionTargetName[] conditionTargetNames = new ConditionTargetName[Enum.GetNames(typeof(ConditionTarget)).Length];
	public ConditionTypeName[] conditionTypeNames = new ConditionTypeName[Enum.GetNames(typeof(ConditionType)).Length];
	public ConditionComparisonName[] conditionComparisonNames  = new ConditionComparisonName[Enum.GetNames(typeof(ConditionComparison)).Length];
	
	public string GetConditionTargetString(ConditionTarget target)
	{
		int index = (int)target;
		if(index < conditionTargetNames.Length)
		{
			return conditionTargetNames[index].name;
		}
		else
		{			
			Debug.LogWarning("Target " + target + " name not found in Action Condition Name Dictionary.");
			return "";
		}
	}

	public string GetConditionTypeString(ConditionType type)
	{
		int index = (int)type;
		if(index < conditionTypeNames.Length)
		{
			return conditionTypeNames[index].name;
		}
		else
		{			
			Debug.LogWarning("Type " + type + " name not found in Action Condition Name Dictionary.");
			return "";
		}
	}

	public string GetComparisonString(ConditionComparison comparison)
	{
		int index = (int)comparison;
		if(index < conditionComparisonNames.Length)
		{
			return conditionComparisonNames[index].name;
		}
		else
		{			
			Debug.LogWarning("Comparison " + comparison + " name not found in Action Condition Name Dictionary.");
			return "";
		}
	}

	public NumberFormat GetNumberFormat(ConditionType type)
	{
		int index = (int)type;
		if(index < conditionTypeNames.Length)
		{
			return conditionTypeNames[index].numberFormat;
		}
		else
		{			
			Debug.LogWarning("Type " + type + " number format not found in Action Condition Name Dictionary.");
			return NumberFormat.none;
		}
	}

	public string GetNumberString(float value, NumberFormat format)
	{
		string toReturn = "";
		switch(format) 
		{
			case NumberFormat.none:
				break;
			case NumberFormat.flat:
				toReturn += value;
				break;
			case NumberFormat.percentage:
				toReturn += (Mathf.Round(100 * value) + "%");
				break;
			default:
				break;
		}
		return toReturn;
	}
	
	public string GetZeroString(ConditionTarget target, ConditionType type, ConditionComparison comparison)
	{		
		if(comparison == ConditionComparison.atLeast) return "";

		string targetString = GetConditionTargetString(target);
		string comparisonString = " no";
		string typeString = " " + GetConditionTypeString(type);

		return (targetString + comparisonString + typeString);
	}
	
	public string GetConditionString(float value, ConditionTarget target, ConditionType type, ConditionComparison comparison)
	{		
		if(value == 0) return GetZeroString(target, type, comparison);
		string numberString = GetNumberString(value, GetNumberFormat(type)) + " ";
		string targetString = GetConditionTargetString(target) + " ";
		string comparisonString = GetComparisonString(comparison) + " ";
		string typeString = GetConditionTypeString(type);

		return (targetString + numberString + comparisonString + typeString);
	}
}

[Serializable]
public struct ConditionTargetName {public ConditionTarget target; public string name;};

[Serializable]
public struct ConditionTypeName {public ConditionType type; public string name; public NumberFormat numberFormat; };

[Serializable]
public struct ConditionComparisonName {public ConditionComparison comparison; public string name; };

public enum NumberFormat { none, flat, percentage, }