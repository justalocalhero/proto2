﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Names/StatNames")]
public class StatNameDictionary : ScriptableObject 
{
	public StatName[] statNames = new StatName[Enum.GetNames(typeof(StatType)).Length];
	public ModName[] modNames = new ModName[Enum.GetNames(typeof(ModifierType)).Length];

	public string GetTypeName(StatType type)
	{
		if((int)type < statNames.Length)
		{
			return statNames[(int)type].name;
		}
		else
		{
			Debug.LogWarning("Stat type name unsupported");
			return null;
		}
	}

	public string GetModName(ModifierType type)
	{
		if((int)type < modNames.Length)
		{
			return modNames[(int)type].name;
		}
		else
		{
			Debug.LogWarning("Modifier type name unsupported");
			return null;
		}
	}

	public string GetStatName(StatType stat, ModifierType mod)
	{
		return (GetModName(mod) + GetTypeName(stat));
	}

	public string GetStatString(float amount, StatType stat, ModifierType mod)
	{
		string valueString;
		if(amount >= 0) valueString = "+";
		else valueString = "";
		if(mod == ModifierType.Flat)
		{
			valueString = " " + valueString + amount;			
		}
		else
		{
			valueString = " " + valueString + Mathf.Round(amount * 100);
		}
		valueString = GetTypeName(stat) + valueString + GetModName(mod);

		return valueString;
	}
}
[Serializable]
public struct StatName {public StatType type; public string name;};

[Serializable]
public struct ModName {public ModifierType type; public string name;};
