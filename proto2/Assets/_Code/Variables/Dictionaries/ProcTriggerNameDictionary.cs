﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName="Names/ProcTriggerNames")]
public class ProcTriggerNameDictionary : ScriptableObject {
	public ProcTriggerName[] procTypeNames = new ProcTriggerName[Enum.GetNames(typeof(ProcTrigger)).Length];

	public string GetString(ProcTarget target, ProcTrigger trigger)
	{
		int index = (int)trigger;
		if(index < procTypeNames.Length)
		{
			string toReturn = (target == ProcTarget.self) ? procTypeNames[index].selfName : procTypeNames[index].targetName;
			toReturn = toReturn.Replace("when unit ","when unit\n");
			toReturn = toReturn.Replace("when target ","when target\n");
			return toReturn;
		}
		else
		{			
			Debug.LogWarning("Trigger " + trigger + " name not found in Proc Trigger Name Dictionary.");
			return "";
		}
	}
}

[Serializable]
public struct ProcTriggerName {public ProcTrigger trigger; public string selfName; public string targetName;};
