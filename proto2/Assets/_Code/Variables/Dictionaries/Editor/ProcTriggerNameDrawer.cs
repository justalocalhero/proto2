﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(ProcTriggerName))]
public class ProcTriggerNameDrawer : PropertyDrawer 
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int triggerSize = 80;
        int selfNameSize = 160;        
        int targetNameSize = 160;
        int spacing = 5;
        int triggerIndent =  0;
        int selfNameIndent = triggerIndent + triggerSize + spacing;        
        int targetNameIndent = selfNameIndent + selfNameSize + spacing;

        Rect triggerRect = new Rect(position.x + triggerIndent, position.y, triggerSize, position.height);
        Rect selfNameRect = new Rect(position.x + selfNameIndent, position.y, selfNameSize, position.height);        
        Rect targetNameRect = new Rect(position.x + targetNameIndent, position.y, targetNameSize, position.height);

        EditorGUI.PropertyField(triggerRect, property.FindPropertyRelative("trigger"), GUIContent.none);
        EditorGUI.PropertyField(selfNameRect, property.FindPropertyRelative("selfName"), GUIContent.none);        
        EditorGUI.PropertyField(targetNameRect, property.FindPropertyRelative("targetName"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

}
