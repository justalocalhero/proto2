﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(ConditionTypeName))]
public class ConditionTypeNameDrawer : PropertyDrawer 
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int typeSize = 80;
        int nameSize = 120;
        int numberFormatSize = 80;
        int spacing = 5;
        int typeIndent =  0;
        int nameIndent = typeIndent + typeSize + spacing;
        int numberFormatIndent = nameIndent + nameSize + spacing;

        Rect typeRect = new Rect(position.x + typeIndent, position.y, typeSize, position.height);
        Rect nameRect = new Rect(position.x + nameIndent, position.y, nameSize, position.height);
        Rect numberFormatRect = new Rect(position.x + numberFormatIndent, position.y, numberFormatSize, position.height);

        EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("type"), GUIContent.none);
        EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("name"), GUIContent.none);        
        EditorGUI.PropertyField(numberFormatRect, property.FindPropertyRelative("numberFormat"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

}
