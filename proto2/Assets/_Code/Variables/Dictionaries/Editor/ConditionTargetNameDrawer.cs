﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(ConditionTargetName))]
public class ConditionTargetNameDrawer : PropertyDrawer 
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int targetSize = 80;
        int nameSize = 120;
        int spacing = 5;
        int targetIndent =  0;
        int nameIndent = targetIndent + targetSize + spacing;

        Rect targetRect = new Rect(position.x + targetIndent, position.y, targetSize, position.height);
        Rect nameRect = new Rect(position.x + nameIndent, position.y, nameSize, position.height);

        EditorGUI.PropertyField(targetRect, property.FindPropertyRelative("target"), GUIContent.none);
        EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("name"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

}
