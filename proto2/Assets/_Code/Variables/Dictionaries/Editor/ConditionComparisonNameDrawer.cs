﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(ConditionComparisonName))]
public class ConditionComparisonNameDrawer : PropertyDrawer 
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int comparisonSize = 80;
        int nameSize = 120;
        int spacing = 5;
        int comparisonIndent =  0;
        int nameIndent = comparisonIndent + comparisonSize + spacing;

        Rect comparisonRect = new Rect(position.x + comparisonIndent, position.y, comparisonSize, position.height);
        Rect nameRect = new Rect(position.x + nameIndent, position.y, nameSize, position.height);

        EditorGUI.PropertyField(comparisonRect, property.FindPropertyRelative("comparison"), GUIContent.none);
        EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("name"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

}
