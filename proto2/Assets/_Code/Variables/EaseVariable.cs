﻿using UnityEngine;
using DG.Tweening;

[CreateAssetMenu(menuName = "Variables/EaseVariable")]
public class EaseVariable : ScriptableObject 
{
#if UNITY_EDITOR
    [Multiline]
    public string DeveloperDescription = "";
#endif

    public Ease Value;

    public void SetValue(Ease value)
    {
        Value = value;
    }

    public void SetValue(EaseVariable value)
    {
        Value = value.Value;
    }
}
