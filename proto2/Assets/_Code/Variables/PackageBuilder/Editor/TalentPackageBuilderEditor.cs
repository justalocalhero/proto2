﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TalentPackageBuilder))]
public class TalentPackageBuilderEditor : Editor {

	public override void OnInspectorGUI()
	{
		TalentPackageBuilder myScript = (TalentPackageBuilder)target;

		myScript.spriteManager = (SpriteManager)EditorGUILayout.ObjectField(myScript.spriteManager, typeof(SpriteManager), true);
		myScript.raceClass = (RaceClass)EditorGUILayout.ObjectField(myScript.raceClass, typeof(RaceClass), true);
		myScript.talentName = EditorGUILayout.TextField("Talent Name", myScript.talentName);

		myScript.type = (TalentBuilderType)EditorGUILayout.EnumPopup("Talent Type", myScript.type);
		myScript.tier = EditorGUILayout.IntField("Talent Tier", myScript.tier);

		bool hasAbility = (myScript.type == TalentBuilderType.timedAbility || myScript.type == TalentBuilderType.triggeredAbility);
		if(hasAbility)
		{
			myScript.abilityType = (TalentAbilityType)EditorGUILayout.EnumPopup("Talent Ability Type", myScript.abilityType);

			bool hasEffect = myScript.abilityType == TalentAbilityType.buff || myScript.abilityType == TalentAbilityType.debuff;
			if(hasEffect)
			{
				myScript.talentEffectName = EditorGUILayout.TextField(myScript.abilityType + " Name", myScript.talentEffectName);
			}
		}

		if(myScript.type == TalentBuilderType.stat || myScript.type == TalentBuilderType.resource)
		{
			
		}

		for(int i = 0; i < myScript.options.Length; i++)
		{
			GUILayout.Space(20);
			myScript.options[i].name = EditorGUILayout.TextField("Option Name", myScript.options[i].name);

			if(hasAbility)
				myScript.options[i].type = (TalentAbilityType)EditorGUILayout.EnumPopup("Talent Ability Type", myScript.options[i].type);
		
			bool hasEffect = (myScript.options[i].type == TalentAbilityType.buff || myScript.options[i].type == TalentAbilityType.debuff);
		
			if(hasEffect)
			{
				myScript.options[i].effectName = EditorGUILayout.TextField(myScript.options[i].type + " Name", myScript.options[i].effectName);
			}

		}

		if(GUILayout.Button("Build Package"))
		{
			myScript.OnBuildButtonPress();
		}

	}
}