﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PackageBuilder : ScriptableObject 
{

	public void OnBuildButtonPress()
	{
		BuildPackage();
	}

	public virtual void BuildPackage()
	{

	}
}
