﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;
using System;

#if(UNITY_EDITOR)
[CreateAssetMenu(menuName="Package/TalentPackage")]
public class TalentPackageBuilder : PackageBuilder 
{
	public SpriteManager spriteManager;
	public RaceClass raceClass;
	public string talentName;	
	public int tier;
	public TalentBuilderType type;
	public TalentAbilityType abilityType;
	public string talentEffectName;

	public TalentOptionPackage[] options = new TalentOptionPackage[3];


	
	public override void BuildPackage()
	{
		string abilityCamelName = "_" + Utility.ToCamelCase(talentName);
		string talentCamelName = "_" + abilityCamelName;

		string thisPath = AssetDatabase.GetAssetPath(this);
		string parentPath = thisPath.Substring(0,thisPath.LastIndexOf('/')) + "/Talents";
		string talentPath = parentPath + "/" + tier + talentCamelName;
		string optionPath = talentPath + "/TalentOptions";
		string talentAssetPath = talentPath + "/" + talentCamelName + ".asset";
		string abilityAssetPath = talentPath + "/" + abilityCamelName + ".asset";


		TalentBuilder talentBuilder = ScriptableObject.CreateInstance<TalentBuilder>();
		talentBuilder.options = new TalentOptionBuilder[3];

		if(!Directory.Exists(parentPath))
		{
			var folder = Directory.CreateDirectory(parentPath);
		}

		if(!Directory.Exists(talentPath))
		{
			var folder = Directory.CreateDirectory(talentPath);
		}
		
		if(!Directory.Exists(talentAssetPath))
		{
			
			talentBuilder.name = talentName;

			AssetDatabase.CreateAsset(talentBuilder, (talentAssetPath));

			if(type == TalentBuilderType.timedAbility)
			{
				TalentAbilityBuilder abilityBuilder = ScriptableObject.CreateInstance<TalentAbilityBuilder>();
				abilityBuilder.name = talentName;
				abilityBuilder.spriteManager = spriteManager;

				talentBuilder.timedAbility = abilityBuilder;

				if(abilityType == TalentAbilityType.buff)
				{
		
					abilityBuilder.buffs = new BuffBuilder[1];
					abilityBuilder.buffs[0] = CreateBuffBuilder(talentEffectName, talentPath);
				}

				if(abilityType == TalentAbilityType.debuff)
				{
					abilityBuilder.debuffs = new DebuffBuilder[1];
					abilityBuilder.debuffs[0] = CreateDebuffBuilder(talentEffectName, talentPath);
				}				
				
				AssetDatabase.CreateAsset(abilityBuilder, abilityAssetPath);				
				AssetDatabase.SaveAssets();

				EditorUtility.SetDirty(abilityBuilder);
			}

			if(type == TalentBuilderType.triggeredAbility)
			{
				TalentTriggeredAbilityBuilder abilityBuilder = ScriptableObject.CreateInstance<TalentTriggeredAbilityBuilder>();
				abilityBuilder.name = talentName;
				abilityBuilder.spriteManager = spriteManager;

				talentBuilder.triggeredAbility = abilityBuilder;

				if(abilityType == TalentAbilityType.buff)
				{
		
					abilityBuilder.buffs = new BuffBuilder[1];
					abilityBuilder.buffs[0] = CreateBuffBuilder(talentEffectName, talentPath);
				}

				if(abilityType == TalentAbilityType.debuff)
				{
					abilityBuilder.debuffs = new DebuffBuilder[1];
					abilityBuilder.debuffs[0] = CreateDebuffBuilder(talentEffectName, talentPath);
				}

				AssetDatabase.CreateAsset(abilityBuilder, abilityAssetPath);
				AssetDatabase.SaveAssets();

				EditorUtility.SetDirty(abilityBuilder);
			}

			for(int i = 0; i < talentBuilder.options.Length; i++)
			{
				string numberedOptionPath = optionPath + i;
				if(!Directory.Exists(numberedOptionPath))
				{
					var folder = Directory.CreateDirectory(numberedOptionPath);
				}

				TalentOptionBuilder optionBuilder = ScriptableObject.CreateInstance<TalentOptionBuilder>();
				optionBuilder.name = options[i].name;
				AssetDatabase.CreateAsset(optionBuilder, numberedOptionPath + "/__" + Utility.ToCamelCase(options[i].name) + ".asset");

				if(type == TalentBuilderType.timedAbility || type == TalentBuilderType.triggeredAbility)
				{
					AbilityAugmentBuilder augmentBuilder = ScriptableObject.CreateInstance<AbilityAugmentBuilder>();

					optionBuilder.augmentBuilder = augmentBuilder;

					AssetDatabase.CreateAsset(augmentBuilder, numberedOptionPath + "/_" + options[i].name + ".asset");

					if(options[i].type == TalentAbilityType.buff)
					{
						augmentBuilder.buffs = new BuffBuilder[1];
						augmentBuilder.buffs[0] = CreateBuffBuilder(options[i].effectName, numberedOptionPath);
					}

					if(options[i].type == TalentAbilityType.debuff)
					{
						augmentBuilder.debuffs = new DebuffBuilder[1];
						augmentBuilder.debuffs[0] = CreateDebuffBuilder(options[i].effectName, numberedOptionPath);
					}
					EditorUtility.SetDirty(augmentBuilder);
				}
				
				AssetDatabase.SaveAssets();

				talentBuilder.options[i] = optionBuilder;
				EditorUtility.SetDirty(optionBuilder);
			}
		}

		EditorUtility.SetDirty(this);
		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
		EditorUtility.FocusProjectWindow();

		Selection.activeObject = talentBuilder;
		
		raceClass.AddTalent(talentBuilder, tier);
		EditorUtility.SetDirty(talentBuilder);


	}

	public BuffBuilder CreateBuffBuilder(string name, string path)
	{
		BuffBuilder buff = ScriptableObject.CreateInstance<BuffBuilder>();
		buff.name = name;
		buff.spriteManager = spriteManager;

		AssetDatabase.CreateAsset(buff, path + "/" + Utility.ToCamelCase(name) + ".asset");
		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
		EditorUtility.SetDirty(buff);

		return buff;
	}

	public DebuffBuilder CreateDebuffBuilder(string name, string path)
	{
		DebuffBuilder debuff = ScriptableObject.CreateInstance<DebuffBuilder>();
		debuff.name = name;
		debuff.spriteManager = spriteManager;

		AssetDatabase.CreateAsset(debuff, path + "/" + Utility.ToCamelCase(name) + ".asset");
		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
		EditorUtility.SetDirty(debuff);

		return debuff;
	}
	[Serializable]
	public struct TalentOptionPackage { public string name; public TalentAbilityType type; public string effectName; };
}

#endif

public enum TalentBuilderType { timedAbility, triggeredAbility, stat, resource, }
public enum TalentAbilityType{ simple, buff, debuff, }