﻿using UnityEngine;

[CreateAssetMenu(menuName = "UI/ColorVariable")]
public class ColorVariable : ScriptableObject {
#if UNITY_EDITOR
        [Multiline]
        public string DeveloperDescription = "";
#endif
        public Color Value;

        public void SetValue(Color value)
        {
            Value = value;
        }

        public void SetValue(ColorVariable value)
        {
            Value = value.Value;
        }
    }
