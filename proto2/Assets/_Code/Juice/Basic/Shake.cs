﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Juice
{
	public class Shake : MonoBehaviour 
	{
		public Transform target;
		public FloatReference duration, strengthX, strengthY, randomness;
		public IntReference vibrato;
		public BoolReference snapping, fadeout;

		public void Fire()
		{
			Fire(duration, new Vector2(strengthX, strengthY), vibrato, randomness, snapping, fadeout);
		}

		public void Fire(float magnitude)
		{
			
			Fire(duration, new Vector2(magnitude * strengthX, magnitude * strengthY), vibrato, randomness, snapping, fadeout);
		}

		public void Fire(float duration, Vector2 strength, int vibrato, float randomness, bool snapping, bool fadeOut)
		{
			target.DOShakePosition(duration, strength, vibrato, randomness, snapping, fadeOut);
		}
	}
}