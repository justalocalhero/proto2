﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnchanterEffects : MonoBehaviour 
{
	public Enchanter enchanter;
	public RectTransform targetIcon;

	public Shower showerPrototype;
	public Shimmer shimmerPrototype;
	public Flash flashPrototype;
	public Puff puffPrototype;

	private Shower shower;
	private Shimmer shimmer;
	private Flash flash;
	private Puff puff;

	void Start () 
	{
		shower = Object.Instantiate(showerPrototype, targetIcon);
		shimmer = Object.Instantiate(shimmerPrototype, targetIcon);
		flash = Object.Instantiate(flashPrototype, targetIcon);
		puff = Object.Instantiate(puffPrototype, targetIcon);
		puff.target = targetIcon;

		enchanter.onCraft += Play;
		flash.onMid += shower.Play;
		shimmer.onMid += flash.Play;
		flash.onMid += puff.Play;
	}

	public void Play(Equipment equipment)
	{
		shimmer.Play();
	}
}