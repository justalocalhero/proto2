﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DivinerFailEffect : MonoBehaviour 
{

	public Diviner diviner;
	public RectTransform targetIcon;

	public Flash flashPrototype;
	public Wobble wobblePrototype;

	private Flash flash;
	private Wobble wobble;

	void Start () 
	{
		flash = Object.Instantiate(flashPrototype, targetIcon);
		wobble = Object.Instantiate(wobblePrototype, targetIcon);
		wobble.target = targetIcon;

		diviner.onFail += Play;
		wobble.onFirstWobble += flash.Play;
	}

	public void Play()
	{
		wobble.Play();
	}
}
