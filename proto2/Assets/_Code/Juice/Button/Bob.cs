﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Bob : MonoBehaviour 
{
	private Sequence sequence;
	public RectTransform target{get; set;}
	public FloatReference yDif, yBounce, upDuration, bounceDuration, interval;
	public EaseReference upEase, bounceEase;

	public delegate void OnStart();
	public OnStart onStart;

	public delegate void OnComplete();
	public OnComplete onComplete;

	public delegate void OnMid();
	public OnMid onMid;

	public void Awake()
	{
		BuildSequence();
	}

	private void BuildSequence()
	{
		sequence = DOTween.Sequence().SetAutoKill(false);
		sequence.Pause();

		sequence.Append(
			target.DOAnchorPosY(0, 0)
			.OnComplete(() => RaiseOnStart())
		)
		.PrependInterval(interval)
		.Append(
			target.DOAnchorPosY(yDif, upDuration)
			.SetEase(upEase)
		)
		.Append(
			target.DOAnchorPosY(yBounce, bounceDuration)
			.SetEase(bounceEase)
			.OnComplete(() => RaiseOnMid())
		)
		.SetLoops(-1, LoopType.Yoyo);
	}

	public void Play()
	{
		sequence.Rewind();
		sequence.Restart();
	}

	public void Stop()
	{
		sequence.Rewind();
	}

	public void RaiseOnStart()
	{
		if(onStart != null) onStart();
	}

	public void RaiseOnComplete()
	{
		if(onComplete != null) onComplete();
	}

	public void RaiseOnMid()
	{
		if(onMid != null) onMid();
	}

	public void RubberBand()
	{
		sequence.PlayBackwards();
		RaiseOnMid();
	}
	
}
