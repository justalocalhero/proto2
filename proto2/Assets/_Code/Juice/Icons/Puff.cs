﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Puff : MonoBehaviour 
{

	public RectTransform target {get; set;}

	public delegate void OnStart();
	public OnStart onStart;

	public delegate void OnComplete();
	public OnComplete onComplete;

	public delegate void OnMid();
	public OnMid onMid;

	public FloatReference xDelta, yDelta, duration, midPointRatio;
	public EaseReference inEase, outEase;

	private Sequence sequence;

	public void Start()
	{
		BuildSequence();
	}
	
	private void BuildSequence()
	{
		sequence = DOTween.Sequence().SetAutoKill(false);
		sequence.Pause();

		sequence.Append(			
			target.DOSizeDelta(new Vector2(0, 0), 0)
			.OnComplete(() => RaiseOnStart())
		)
		.Append(
			target.DOSizeDelta(new Vector2(xDelta, yDelta), midPointRatio * duration)
			.SetEase(inEase)
			.OnComplete(() => RaiseOnMid())
		)
		.Append(
			target.DOSizeDelta(new Vector2(0, 0), (1 - midPointRatio) * duration)
			.SetEase(outEase)
			.OnComplete(() => RaiseOnComplete())
		);
	}

	public void Play()
	{
		sequence.Rewind();
		sequence.Restart();
	}

	public void Reset()
	{
		sequence.Rewind();
	}

	public void RaiseOnStart()
	{
		if(onStart != null) onStart();
	}

	public void RaiseOnComplete()
	{
		if(onComplete != null) onComplete();
	}

	public void RaiseOnMid()
	{
		if(onMid != null) onMid();
	}
}
