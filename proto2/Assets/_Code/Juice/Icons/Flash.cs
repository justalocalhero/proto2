﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Flash : MonoBehaviour 
{
	public Image image;

	public delegate void OnStart();
	public OnStart onStart;

	public delegate void OnComplete();
	public OnComplete onComplete;

	public delegate void OnMid();
	public OnMid onMid;

	public ColorReference startColor, midColor, endColor;
	public FloatReference duration, midPointRatio;
	public EaseReference inEase, outEase;

	private Sequence sequence;

	public void Start()
	{
		BuildSequence();
		image.color = startColor;
	}
	
	private void BuildSequence()
	{
		sequence = DOTween.Sequence().SetAutoKill(false);
		sequence.Pause();

		sequence.Append(
			image.DOColor(startColor, 0)
			.OnComplete(() => RaiseOnStart())
		)
		.Append(
			image.DOColor(midColor, duration * midPointRatio)
			.SetEase(inEase)
			.OnComplete(() => RaiseOnMid())
		)
		.Append(
			image.DOColor(endColor, duration * (1 - midPointRatio))
			.SetEase(outEase)
			.OnComplete(() => RaiseOnComplete())
		);

	}

	public void Play()
	{
		sequence.Rewind();
		sequence.Restart();
	}

	public void Reset()
	{
		sequence.Rewind();
	}

	public void RaiseOnStart()
	{
		if(onStart != null) onStart();
	}

	public void RaiseOnComplete()
	{
		if(onComplete != null) onComplete();
	}

	public void RaiseOnMid()
	{
		if(onMid != null) onMid();
	}

}
