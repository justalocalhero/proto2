﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shower : MonoBehaviour 
{
	public ParticleSystem particles;

	public void Play()
	{
		particles.Play();
	}
}
