﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Wobble : MonoBehaviour 
{
	public RectTransform target{get; set;}

	public delegate void OnStart();
	public OnStart onStart;

	public delegate void OnComplete();
	public OnComplete onComplete;

	public delegate void OnFirstWobble();
	public OnFirstWobble onFirstWobble;

	public delegate void OnSecondWobble();
	public OnSecondWobble onSecondWobble;

	public FloatReference rotation, yDelta, duration, firstWobbleRatio, secondWobbleRatio;
	public EaseReference firstEase, secondEase, resetEase, upEase, downEase;

	private Sequence sequence;

	public void Start()
	{
		BuildSequence();
		if(firstWobbleRatio >= secondWobbleRatio) 
			Debug.LogWarning("SecondWobbleRatio should be greater than FirstWobbleRatio.");
	}
	
	private void BuildSequence()
	{
		sequence = DOTween.Sequence().SetAutoKill(false);
		sequence.Pause();

		sequence.Append(			
			target.DOLocalRotate(Vector3.zero, 0)
			.OnComplete(() => RaiseOnStart())
		)
		.Join(
			target.DOAnchorPosY(0, 0)
		)
		.Append(
			target.DOLocalRotate(new Vector3(0, 0, rotation), firstWobbleRatio * duration)
			.SetEase(firstEase)
			.OnComplete(() => RaiseOnFirstWobble())
		)
		.Join(
			target.DOAnchorPosY(yDelta, secondWobbleRatio * duration)
			.SetEase(upEase)	
		)
		.Append(
			target.DOLocalRotate(new Vector3(0, 0, -rotation / 2), (secondWobbleRatio - firstWobbleRatio) * duration)
			.SetEase(secondEase)
			.OnComplete(() => RaiseOnSecondWobble())
		)
		.Append(
			target.DOLocalRotate(Vector3.zero, (1 - secondWobbleRatio) * duration)
			.SetEase(resetEase)
			.OnComplete(() => RaiseOnComplete())
		)
		.Join(
			target.DOAnchorPosY(0, (1 - secondWobbleRatio) * duration)
			.SetEase(downEase)
		);
	}

	public void Play()
	{
		sequence.Rewind();
		sequence.Restart();
	}

	public void RaiseOnStart()
	{
		if(onStart != null) onStart();
	}

	public void RaiseOnComplete()
	{
		if(onComplete != null) onComplete();
	}

	public void RaiseOnFirstWobble()
	{
		if(onFirstWobble != null) onFirstWobble();
	}

	public void RaiseOnSecondWobble()
	{
		if(onSecondWobble != null) onSecondWobble();
	}
}
