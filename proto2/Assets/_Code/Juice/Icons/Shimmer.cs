﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Shimmer : MonoBehaviour 
{
	public delegate void OnStart();
	public OnStart onStart;

	public delegate void OnComplete();
	public OnComplete onComplete;

	public delegate void OnMid();
	public OnMid onMid;

	public FloatReference duration, midPointRatio;
	public EaseReference ease;

	private float height;

	private Sequence sequence;

	public void Start()
	{
		height = (transform as RectTransform).rect.height;
		transform.localPosition = new Vector3(0, -height, 0);
		BuildSequence();
	}

	private void BuildSequence()
	{
		sequence = DOTween.Sequence().SetAutoKill(false);
		sequence.Pause();

		sequence.Append(
			transform.DOLocalMoveY(height, duration)
			.SetEase(ease)
		)
		.InsertCallback(0, () => RaiseOnStart())
		.OnComplete(() => RaiseOnComplete())
		.InsertCallback(duration * midPointRatio, () => RaiseOnMid());

	}

	public void Play()
	{
		sequence.Rewind();
		sequence.Restart();
	}

	public void RaiseOnStart()
	{
		if(onStart != null) onStart();
	}

	public void RaiseOnComplete()
	{
		if(onComplete != null) onComplete();
	}

	public void RaiseOnMid()
	{
		if(onMid != null) onMid();
	}

}
