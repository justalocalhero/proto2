﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QualityIndicator : MonoBehaviour 
{

	public InventorySlot slot;
	public Image image;
	public List<Sprite> qualitySprites;
	public IntReference plusOneIndex;

	public void Awake()
	{
		slot.onUpdate += UpdateUI;
	}

	private void UpdateUI()
	{
		if(CheckClear())
		{
			Clear();
			return;
		}
		
		SetSprite((slot.item as Equipment).quality.currentLevel);

	}

	public void Clear()
	{
		image.enabled = false;
	}

	private bool CheckClear()
	{
		if(slot == null) return true;
		if(slot.item == null) return true;
		if(!(slot.item is Equipment)) return true;

		Equipment equipment = slot.item as Equipment;

		if(equipment.quality == null) return true;
		if(equipment.quality.currentLevel == 0) return true;

		return false;
	}

	private void SetSprite(int qualityLevel)
	{
		int crunchedLevel = (qualityLevel > 0) ? qualityLevel - 1 : qualityLevel;
		int shiftedIndex = plusOneIndex + crunchedLevel; 
		int clampedIndex = Mathf.Clamp(shiftedIndex, 0, qualitySprites.Count - 1);

		image.sprite = qualitySprites[clampedIndex];
		
		if(!image.enabled) image.enabled = true;
	}
}
