﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaterialIndicator : MonoBehaviour 
{

	public InventorySlot slot;
	public Image image;
	public Sprite materialSprite;

	public void Awake()
	{
		slot.onUpdate += UpdateUI;
	}

	private void UpdateUI()
	{
		if(CheckClear())
		{
			Clear();
			return;
		}
		
		image.sprite = materialSprite;
		
		if(!image.enabled) image.enabled = true;

	}

	public void Clear()
	{
		image.enabled = false;
	}

	private bool CheckClear()
	{
		if(slot == null) return true;
		if(slot.item == null) return true;
		if(!(slot.item is Equipment)) return true;

		Equipment equipment = slot.item as Equipment;

		if(equipment.material == null) return true;
		if(equipment.material.IsDefault()) return true;

		return false;
	}
}
