﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnchantmentIndicator : MonoBehaviour 
{
	public InventorySlot slot;
	public Image image;
	public List<Sprite> enchantmentSprites;

	public void Awake()
	{
		slot.onUpdate += UpdateUI;
	}

	private void UpdateUI()
	{
		if(CheckClear())
		{
			Clear();
			return;
		}
		
		SetSprite((slot.item as Equipment).enchantment.currentLevel);

	}

	public void Clear()
	{
		image.enabled = false;
	}

	private bool CheckClear()
	{
		if(slot == null) return true;
		if(slot.item == null) return true;
		if(!(slot.item is Equipment)) return true;

		Equipment equipment = slot.item as Equipment;

		if(equipment.enchantment == null) return true;
		if(equipment.enchantment.currentLevel == 0) return true;

		return false;
	}

	private void SetSprite(int qualityLevel)
	{
		
		int clampedIndex = Mathf.Clamp(qualityLevel - 1, 0, enchantmentSprites.Count - 1);

		image.sprite = enchantmentSprites[clampedIndex];
		
		if(!image.enabled) image.enabled = true;
	}

}
