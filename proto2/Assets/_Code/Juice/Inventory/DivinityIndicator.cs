﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DivinityIndicator : MonoBehaviour {

	public InventorySlot slot;
	public Image image;
	public Sprite blessedSprite;
	public Sprite cursedSprite;

	public void Awake()
	{
		slot.onUpdate += UpdateUI;
	}

	private void UpdateUI()
	{
		if(CheckClear())
		{
			Clear();
			return;
		}
		
		Equipment equipment = slot.item as Equipment;

		if(equipment.divinity.currentLevel >  0) image.sprite = blessedSprite;
		else image.sprite = cursedSprite;

		if(!image.enabled) image.enabled = true;
	}

	public void Clear()
	{
		image.enabled = false;
	}

	private bool CheckClear()
	{
		if(slot == null) return true;
		if(slot.item == null) return true;
		if(!(slot.item is Equipment)) return true;

		Equipment equipment = slot.item as Equipment;

		if(equipment.divinity == null) return true;
		if(equipment.divinity.currentLevel == 0) return true;

		return false;
	}
}
