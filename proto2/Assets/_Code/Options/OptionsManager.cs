﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OptionsManager : MonoBehaviour 
{
	public SaveManager saveManager;
	public UIToggler toggler;
	public UIToggler resetToggler;
	public bool resetting;
	public Button saveButton, resetButton, confirmButton, backButton;

	public void Awake()
	{
		toggler.onShow += UpdateUI;
		toggler.onHide += Reset;
		saveButton.onClick.AddListener(HandleSave);
		resetButton.onClick.AddListener(HandleClear);
		confirmButton.onClick.AddListener(HandleConfirm);
		backButton.onClick.AddListener(HandleBack);
	}

	public void UpdateUI()
	{
		resetToggler.Set(resetting);
	}

	public void HandleSave()
	{
		saveManager.Save();
	}

	public void Reset()
	{
		resetting = false;

		UpdateUI();
	}

	public void HandleClear()
	{
		resetting = true;

		UpdateUI();
	}

	public void HandleConfirm()
	{
		if(!resetting) return;

		saveManager.Clear();
		Scene scene = SceneManager.GetActiveScene(); 
		SceneManager.LoadScene(scene.name);
	}

	public void HandleBack()
	{
		resetting = false;

		UpdateUI();
	}

	public void Show()
	{
		toggler.Show();
	}

	public void Hide()
	{
		toggler.Hide();
	}
}
