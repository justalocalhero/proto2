﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NineSlliceTest : MonoBehaviour 
{
	public Transform container;
	public Image imageProto;
	public Image testImage;
	public RectTransform imageTransform;

	public void Start()
	{
		testImage = Object.Instantiate(imageProto, container, false);
		imageTransform = testImage.GetComponent<RectTransform>();
	}

	public void LateUpdate()
	{
		if(Input.GetKeyDown(KeyCode.Minus))
		{
			AdjustSize(-1);
		}
		if(Input.GetKeyDown(KeyCode.Equals))
		{
			AdjustSize(1);

		}
	}

	public void AdjustSize(float adjustment)
	{
		Vector2 size = imageTransform.sizeDelta;
		Vector2 newSize = new Vector2(size.x + adjustment, size.y + adjustment);

		imageTransform.sizeDelta = newSize;
	}
}
