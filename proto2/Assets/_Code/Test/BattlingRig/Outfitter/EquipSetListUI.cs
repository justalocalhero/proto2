﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipSetListUI : SelectableListUI<Loot.EquipmentSet> 
{
	public EquipSetSlot slotPrototype;
    public override SelectableSlot _slotPrototype {get {return slotPrototype;}}
}
