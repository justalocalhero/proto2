﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class IntSlot : SelectableSlot<int>
{
	public TextMeshProUGUI mesh;

	public override void RegisterUpdateUIDelegates()
	{
		updateUI += UpdateText;
		base.RegisterUpdateUIDelegates();
	}

	public void UpdateText()
	{
		if(selectable != null) mesh.SetText(selectable.payload + "");
		else mesh.SetText("");
	}
}
