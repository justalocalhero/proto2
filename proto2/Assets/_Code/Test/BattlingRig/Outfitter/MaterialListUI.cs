﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialListUI : SelectableListUI<Loot.MaterialBuilder> 
{
	public MaterialSlot slotPrototype;
    public override SelectableSlot _slotPrototype {get {return slotPrototype;}}
}
