﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntListUI : SelectableListUI<int>
{	
	public IntSlot slotPrototype;
    public override SelectableSlot _slotPrototype {get {return slotPrototype;}}
}
