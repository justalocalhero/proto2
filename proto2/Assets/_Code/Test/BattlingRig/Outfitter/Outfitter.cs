﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Loot;

public class Outfitter : MonoBehaviour 
{
	// public List<EquipmentSet> equipmentSets;

	// public SwappableList<EquipmentSet> setList = new SwappableList<EquipmentSet>();
	// public SwappableList<MaterialBuilder> materialList = new SwappableList<MaterialBuilder>();
	// public SwappableList<EnchantmentBuilder> enchantmentList = new SwappableList<EnchantmentBuilder>();

	// public SwappableList<int> qualityLevel = new SwappableList<int>();
	// public SwappableList<int> enchantmentLevel = new SwappableList<int>();
	// public SwappableList<int> divinityLevel = new SwappableList<int>();

	// public IntReference minQuality, maxQuality, minEnchantment, maxEnchantment, minDivinity, maxDivinity;

	// public EquipmentBuilder builder = new EquipmentBuilder();

	// public delegate void OnChanged();
	// public OnChanged onChanged;

	// public delegate void OnMaterialSelected(MaterialBuilder material);
	// public OnMaterialSelected onMaterialSelected;

	// public delegate void OnDesignSelected(DesignBuilder design, AbilityAddon ability);
	// public OnDesignSelected onDesignSelected;

	// public delegate void OnEnchantmentSelected(EnchantmentBuilder enchantment);
	// public OnEnchantmentSelected onEnchantmentSelected;

	// public delegate void OnQualitySelected(QualityBuilder quality);
	// public OnQualitySelected onQualitySelected;

	// public delegate void OnDivinitySelected(DivinityBuilder divinity);
	// public OnDivinitySelected onDivinitySelected;

	// public void RaiseOnChanged()
	// {
	// 	if(onChanged != null) onChanged();
	// }

	// public void RaiseOnMaterialSelected(Selectable<MaterialBuilder> material)
	// {
	// 	if(onMaterialSelected != null) onMaterialSelected(material.payload);
	// }

	// public void RaiseOnDesignSelected(Selectable<EquipmentSet> set)
	// {
	// 	if(onDesignSelected != null) 
	// 	{
	// 		GenericEquipmentGenerator generator = GetGenerator(set.payload);
	// 		onDesignSelected(GetDesignBuilder(generator), generator.abilityAddon);
	// 	}
	// }

	// public void RaiseOnEnchantmentSelected(Selectable<EnchantmentBuilder> enchantment)
	// {
	// 	if(onEnchantmentSelected != null) onEnchantmentSelected(enchantment.payload);
	// }

	// public void RaiseOnQualitySelected(Selectable<int> quality)
	// {
	// 	if(onQualitySelected != null) onQualitySelected(GetQualityBuilder(GetGenerator(GetSet())));
	// }

	// public void RaiseOnDivinitySelected(Selectable<int> level)
	// {
	// 	if(onDivinitySelected != null) onDivinitySelected(GetDivinityBuilder(GetGenerator(GetSet())));
	// }

	// public void Awake()
	// {
	// 	BuildSets();
	// 	materialList.onChanged += RaiseOnMaterialSelected;
	// 	enchantmentList.onChanged += RaiseOnEnchantmentSelected;
	// 	divinityLevel.onChanged += RaiseOnDivinitySelected;
	// 	qualityLevel.onChanged += RaiseOnQualitySelected;
		
	// }

	// public void BuildSets()
	// {
	// 	foreach(EquipmentSet set in equipmentSets)
	// 	{
	// 		AddSet(set);
	// 	}

	// 	BuildIntList(qualityLevel, minQuality, maxQuality);
	// 	BuildIntList(enchantmentLevel, minEnchantment, maxEnchantment);
	// 	BuildIntList(divinityLevel, minDivinity, maxDivinity);
	// }

	// public void BuildIntList(SwappableList<int> list, int min, int max)
	// {
	// 	if(min > max) return;

	// 	for(int i = min; i <= max; i++)
	// 	{
	// 		Selectable<int> temp = new Selectable<int>(i);
	// 		temp.SetAvailable();

	// 		list.Add(temp);
	// 	}
	// }

	// public void AddSet(EquipmentSet set)
	// {
	// 	Selectable<EquipmentSet> temp = new Selectable<EquipmentSet>(set);

	// 	temp.RegisterOnToggle(HandleChange);
	// 	temp.SetAvailable();

	// 	setList.Add(temp);
	// }

	// public void HandleChange(Selectable<EquipmentSet> set)
	// {
	// 	if(!set.IsSelected())
	// 	{
	// 		materialList.Clear();
	// 		enchantmentList.Clear();

	// 		return;
	// 	};

	// 	SetMaterials(set.payload);
	// 	SetEnchantments(set.payload);

	// 	RaiseOnChanged();
	// 	RaiseOnDesignSelected(set);
	// }

	// public void SetMaterials(EquipmentSet set)
	// {
	// 	materialList.Clear();

	// 	GenericEquipmentGenerator generator = GetGenerator(set);

	// 	if(generator == null) return;

	// 	List<MaterialBuilder> materials = generator.GetMaterials();

	// 	foreach(MaterialBuilder builder in materials)
	// 	{
	// 		Selectable<MaterialBuilder> temp = new Selectable<MaterialBuilder>(builder);
	// 		temp.SetAvailable();

	// 		materialList.Add(temp);
	// 	}
	// }

	// public void SetEnchantments(EquipmentSet set)
	// {
	// 	enchantmentList.Clear();

	// 	GenericEquipmentGenerator generator = GetGenerator(set);

	// 	if(generator == null) return;

	// 	List<EnchantmentBuilder> enchantments = generator.GetEnchantments();

	// 	foreach(EnchantmentBuilder builder in enchantments)
	// 	{
	// 		Selectable<EnchantmentBuilder> temp = new Selectable<EnchantmentBuilder>(builder);
	// 		temp.SetAvailable();

	// 		enchantmentList.Add(temp);
	// 	}
	// }

	// public List<Equipment> Build()
	// {
	// 	EquipmentSet set = GetEquipmentSet();

	// 	List<Equipment> toReturn = new List<Equipment>();

	// 	if(set == null) return toReturn;

	// 	foreach(EquipmentCatalogEntry entry in set.generators)
	// 	{
	// 		toReturn.Add(Build(entry.generator));
	// 	}

	// 	return toReturn;
	// }

	// private Equipment Build(GenericEquipmentGenerator generator)
	// {

	// 	Equipment equipment = builder.Begin()
	// 		.WithSlot(GetSlot(generator), GetSlotMultiplier(generator))
	// 		.WithRarity(Rarity.ordinary)
	// 		.WithDesign(GetDesign(generator))
	// 		.WithMaterials(GetMaterial())
	// 		.WithQuality(GetQuality(generator, GetQualityLevel()))
	// 		.WithDivinity(GetDivinity(generator, GetDivinityLevel()))
	// 		.WithEnchantment(GetEnchantment(GetEnchantmentLevel()))
	// 		.WithAbilityAddon(GetAbilityAddon(generator))
	// 		.Build();

		
	// 	return equipment;
	// }

	// public EquipmentSet GetSet()
	// {
	// 	var sets = setList.GetSelected();
		
	// 	return (sets.Count > 0) ? sets[0] : null;
	// }

	// public GenericEquipmentGenerator GetGenerator(EquipmentSet set)
	// {
	// 	return (set.generators.Count > 0) ? set.generators[0].generator : null;
	// }

	// public DesignBuilder GetDesignBuilder(GenericEquipmentGenerator generator)
	// {
	// 	return null;
	// 	//return generator.designBuilder;
	// }

	// public Design GetDesign(GenericEquipmentGenerator generator)
	// {
	// 	DesignBuilder builder = GetDesignBuilder(generator);

	// 	return (builder != null) ? builder.Generate() : null;
	// }

	// public MaterialBuilder GetMaterialBuilder()
	// {
	// 	List<MaterialBuilder> temp = materialList.GetSelected();

	// 	return (temp.Count > 0) ? temp[0] : null;
	// }

	// public Loot.Material GetMaterial()
	// {
	// 	MaterialBuilder builder = GetMaterialBuilder();

	// 	return (builder != null) ? builder.Generate() : null;
	// }

	// public EnchantmentBuilder GetEnchantmentBuilder()
	// {
	// 	List<EnchantmentBuilder> temp = enchantmentList.GetSelected();

	// 	return (temp.Count > 0) ? temp[0] : null;
	// }

	// public Enchantment GetEnchantment(int level)
	// {
	// 	EnchantmentBuilder builder = GetEnchantmentBuilder();
		
	// 	return (builder != null) ? builder.Generate(level) : null;
	// }

	// public QualityBuilder GetQualityBuilder(GenericEquipmentGenerator generator)
	// {
	// 	return null;
	// 	//return generator.qualityBuilder;
	// }

	// public Quality GetQuality(GenericEquipmentGenerator generator, int level)
	// {
	// 	QualityBuilder builder = GetQualityBuilder(generator);

	// 	return (builder != null) ? builder.Generate(level) : null;
	// }

	// public DivinityBuilder GetDivinityBuilder(GenericEquipmentGenerator generator)
	// {
	// 	return null;
	// 	//return generator.divinityBuilder;
	// }

	// public Divinity GetDivinity(GenericEquipmentGenerator generator, int level)
	// {
	// 	DivinityBuilder builder = GetDivinityBuilder(generator);

	// 	return (builder != null) ? builder.Generate(level) : null;
	// }

	// public EquipSlot GetSlot(GenericEquipmentGenerator generator)
	// {
	// 	return new EquipSlot();
	// 	//return generator.slot;
	// }

	// public float GetSlotMultiplier(GenericEquipmentGenerator generator)
	// {
	// 	return generator.slotMap.GetMultiplier(GetSlot(generator));
	// }

	// public EquipmentSet GetEquipmentSet()
	// {
	// 	List<EquipmentSet> temp = setList.GetSelected();

	// 	return (temp.Count > 0) ? temp[0] : null;
	// }

	// public AbilityAddon GetAbilityAddon(GenericEquipmentGenerator generator)
	// {
	// 	return generator.abilityAddon;
	// }

	// public int GetQualityLevel()
	// {
	// 	List<int> temp = qualityLevel.GetSelected();
		
	// 	return(temp.Count > 0) ? temp[0] : 0;
	// }

	// public int GetEnchantmentLevel()
	// {
	// 	List<int> temp = enchantmentLevel.GetSelected();
		
	// 	return(temp.Count > 0) ? temp[0] : 0;
	// }

	// public int GetDivinityLevel()
	// {
	// 	List<int> temp = divinityLevel.GetSelected();
		
	// 	return(temp.Count > 0) ? temp[0] : 0;
	// }
}
