﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnchantmentSlot : SelectableSlot<Loot.EnchantmentBuilder>  
{
	public TextMeshProUGUI mesh;

	public override void RegisterUpdateUIDelegates()
	{
		updateUI += UpdateText;
		base.RegisterUpdateUIDelegates();
	}

	public void UpdateText()
	{
		// if(selectable != null && selectable.payload != null) mesh.SetText(selectable.payload.enchantmentName);
		// else mesh.SetText("");
	}
}
