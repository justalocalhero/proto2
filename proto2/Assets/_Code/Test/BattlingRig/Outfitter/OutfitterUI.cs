﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutfitterUI : MonoBehaviour 
{
	// public Outfitter outfitter;

	// public EquipSetListUI equipList;
	// public EnchantmentListUI enchantmentList;
	// public MaterialListUI materialList;
	// public Pager equipPager, enchantPager, materialPager;

	// public IntListUI qualityLevel, divinityLevel, enchantmentLevel;

	// private void Awake()
	// {
	// 	outfitter.onChanged += OnSetChanged;

	// 	equipPager.onChanged += UpdateEquipList;
	// 	enchantPager.onChanged += UpdateEnchantmentList;
	// 	materialPager.onChanged += UpdateMaterialList;
	// }

	// private void Start()
	// {
	// 	UpdateLists();
	// }

	// public void UpdateLists()
	// {

	// 	UpdateEquipList(equipPager.CurrentPage());
	// 	UpdateEnchantmentList(enchantPager.CurrentPage());
	// 	UpdateMaterialList(materialPager.CurrentPage());
	// 	qualityLevel.Set(outfitter.qualityLevel);
	// 	divinityLevel.Set(outfitter.divinityLevel);
	// 	enchantmentLevel.Set(outfitter.enchantmentLevel);
		
	// 	UpdatePagers();
	// }

	// public void UpdateEquipList(int page)
	// {
	// 	equipList.Set(outfitter.setList, page);
	// }

	// public void UpdateEnchantmentList(int page)
	// {
	// 	enchantmentList.Set(outfitter.enchantmentList, page);
	// }

	// public void UpdateMaterialList(int page)
	// {		
	// 	materialList.Set(outfitter.materialList, page);
	// }

	// public void UpdatePagers()
	// {		
	// 	UpdatePager(equipPager, outfitter.setList, equipList);
	// 	UpdatePager(enchantPager, outfitter.enchantmentList, enchantmentList);
	// 	UpdatePager(materialPager, outfitter.materialList, materialList);
	// }

	// public void UpdatePager<T>(Pager pager, SelectableList<T> list, SelectableListUI<T> listUI)
	// {
	// 	int entriesPerPage = listUI.EntriesPerPage();
	// 	int entries = list.selectables.Count;

	// 	int pages = (entriesPerPage > 0) ? Mathf.CeilToInt((entries + 0f) / entriesPerPage) : 0;

	// 	pager.SetMaxPages(pages);
	// }

	// public void OnSetChanged()
	// {		
	// 	UpdateEnchantmentList(enchantPager.CurrentPage());
	// 	UpdateMaterialList(materialPager.CurrentPage());

	// 	UpdatePagers();
	// }
}
