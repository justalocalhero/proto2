﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnchantmentListUI : SelectableListUI<Loot.EnchantmentBuilder> 
{
	public EnchantmentSlot slotPrototype;
    public override SelectableSlot _slotPrototype {get {return slotPrototype;}}
}
