﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompactEncounter : MonoBehaviour 
{
	public EnemyClassSelector selector;

	public Enemy GetNext()
	{
		Enemy enemy = new Enemy("Jill", 1, new UnitResourceManager());

		enemy.AddRaceClass(selector.GetSelectedClasses());

		return enemy;
	}
}
