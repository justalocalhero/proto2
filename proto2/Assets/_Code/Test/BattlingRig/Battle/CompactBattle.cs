﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CompactBattle : MonoBehaviour 
{
	public float timeFactor;
	private bool active;
	private float playerDamage, enemyDamage, playerHealing, enemyHealing, timePassed;
	private int playerDeath, enemyDeath;

	public CompactBullpen bullpen;
	public CompactEncounter encounter;

	private Player player;
	private Enemy enemy;

	public Button prepButton;
	public Button launchButton;
	public TextMeshProUGUI playerMesh;
	public TextMeshProUGUI enemyMesh;

	public DamageColorMap damageColors;
	public FloatingTextManager playerFloatingText;
	public FloatingTextManager enemyFloatingText;

	public void Awake()
	{
		prepButton.onClick.AddListener(OnPrepButton);
		launchButton.onClick.AddListener(OnLaunchButton);
	}

	public void Start()
	{
		ResetStats();
	}

	public void Update()
	{
		if(player != null && enemy != null && active)
		{
			float time = Time.deltaTime * timeFactor;
			timePassed += time;
			// player.UpdateTImers(time);
			// enemy.UpdateTImers(time);
		}
	}

	public void OnPrepButton()
	{
		bullpen.PrepPlayer();
	}

	public void OnLaunchButton()
	{
		ResetStats();
		AddPlayer(bullpen.GetNext());
		AddEnemy(encounter.GetNext());

		active = true;
		
	}

	private void ResetStats()
	{
		playerDamage = enemyDamage = playerHealing = enemyHealing = timePassed = 0;
		playerDeath = enemyDeath = 0;
	}

	public void AddPlayer(Player player)
	{
		if(player != null) 
		{
			player.onDamage -= RecordEnemyDamage;
			player.onDeath -= RecordPlayerDeath;
		}

		this.player = player;

		player.onDamage += RecordEnemyDamage;
		player.onDeath += RecordPlayerDeath;

		if(enemy != null)
			RegisterTargets();

		UpdateUI();

	}

	public void AddEnemy(Enemy enemy)
	{
		if(enemy != null) 
		{
			enemy.onDamage -= RecordPlayerDamage;
			enemy.onDeath -= RecordEnemyDeath;
		}

		this.enemy = enemy;

		enemy.onDamage += RecordPlayerDamage;
		enemy.onDeath += RecordEnemyDeath;

		if(player != null) 
			RegisterTargets();

		UpdateUI();
	}

	public void RegisterTargets()
	{
		// player.target = enemy;
		// enemy.target = player;
	}

	public void RecordPlayerDamage(Damage damage)
	{
		playerFloatingText.Prepare(GetDamageText(damage));
		if(ShouldRecordDamage(damage)) playerDamage += damage.value;
		if(ShouldRecordHealing(damage)) enemyHealing -= damage.value;

		UpdateUI();
	}

	public void RecordEnemyDamage(Damage damage)
	{		
		enemyFloatingText.Prepare(GetDamageText(damage));
		if(ShouldRecordDamage(damage)) enemyDamage += damage.value;
		if(ShouldRecordHealing(damage)) playerHealing -= damage.value;
		
		UpdateUI();
	}

	public void RecordPlayerDeath(Damage damage)
	{
		player.SoftReset();
		playerDeath++;
		
		UpdateUI();
	}

	public void RecordEnemyDeath(Damage damage)
	{
		enemy.SoftReset();
		enemyDeath++;
		
		UpdateUI();
	}

	public bool ShouldRecordDamage(Damage damage)
	{
		if(damage.type == DamageType.healing) return false;
		if(damage.result == ActionResult.hit || damage.result == ActionResult.crit) return true;
		return false;
	}

	public bool ShouldRecordHealing(Damage damage)
	{
		if(damage.type != DamageType.healing) return false;
		return true;
	}

	public void UpdateUI()
	{
		playerMesh.SetText(PlayerText());
		enemyMesh.SetText(EnemyText());
	}

	public string PlayerText()
	{
		if(player == null) return "No Player";

		return PlayerHP() + PlayerDPS() + PlayerHPS() + PlayerLifetime() + PlayerKDR();
	}

	public string EnemyText()
	{
		if(enemy == null) return "No Enemy";

		return EnemyHP() + EnemyDPS() + EnemyHPS() + EnemyLifetime();

	}

	public string PlayerHP()
	{
		return "\nHP: " + player.GetCurrentHitpoints() + " / " + player.GetMaxHitpoints();
	}

	public string PlayerDPS()
	{
		float value = (timePassed > 0) ? playerDamage / timePassed : 0;

		return "\nDPS: " + Utility.Truncate(value, 2);
	}

	public string PlayerHPS()
	{
		float value = (timePassed > 0) ? playerHealing / timePassed : 0;

		return "\nHPS: " + Utility.Truncate(value, 2);
	}

	public string PlayerKDR()
	{
		float value = (playerDeath > 0) ? enemyDeath / (0.0f + playerDeath) : enemyDeath;

		return "\nKDR: " + Utility.Truncate(value, 2);
	}

	public string EnemyHP()
	{
		return "\nHP: " + enemy.GetCurrentHitpoints() + " / " + enemy.GetMaxHitpoints();
	}

	public string EnemyDPS()
	{
		float value = (timePassed > 0) ? enemyDamage / timePassed : 0;

		return "\nDPS: " + Utility.Truncate(value, 2);
	}

	public string EnemyHPS()
	{
		float value = (timePassed > 0) ? enemyHealing / timePassed : 0;

		return "\nHPS: " + Utility.Truncate(value, 2);
	}

	public string PlayerLifetime()
	{
		bool hasDied = playerDeath > 0;
		float value = (hasDied) ? timePassed / playerDeath : timePassed;

		string valueString = Utility.Truncate(value, 2) + "s";
		if(!hasDied) valueString = ">" + valueString;

		return "\nLifetime: " + valueString;
	}

	public string EnemyLifetime()
	{
		bool hasDied = enemyDeath > 0;
		float value = (hasDied) ? timePassed / enemyDeath : timePassed;

		string valueString = Utility.Truncate(value, 2) + "s";
		if(!hasDied) valueString = ">" + valueString;

		return "\nLifetime: " + valueString;
	}

	public string GetDamageText(Damage damage)
	{
		Color color = damageColors.GetColor(damage.type);
		string colorString = ColorUtility.ToHtmlStringRGBA(color);
		string valueString = "" + Mathf.Round(Mathf.Abs(damage.value));
		if(damage.type == DamageType.healing) valueString = "+" + valueString;
		if(damage.result == ActionResult.crit) valueString += "!";

		return "<color=#" + colorString + ">" + valueString + "</color>";
	}
	
}
