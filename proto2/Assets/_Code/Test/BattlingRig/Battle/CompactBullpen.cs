﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompactBullpen : MonoBehaviour 
{
	public UnitResourceDictionary unitResourceDictionary;
	public PlayerClassSelector selector;
	public TalentTreeManager talents;
	public List<Outfitter> outfitters;

	public Player nextPlayer;

	public void PrepPlayer()
	{
		Player player = new Player("Bob", new UnitResourceManager(unitResourceDictionary), 0);
		player.SetTalentLevel(100);

		List<RaceClass> raceClasses = selector.GetSelectedClasses();

		player.AddRaceClass(raceClasses);
		
		talents.Set(player);

		nextPlayer = player;
	}

	public Player GetNext()
	{
		if(nextPlayer == null) PrepPlayer();

		nextPlayer.RemoveAllEquipment();
		nextPlayer.RemoveAllTalents();

		// foreach(Outfitter outfitter in outfitters)
		// {
		// 	nextPlayer.AddEquipment(outfitter.Build().ToArray());
		// }		

		nextPlayer.AddTalent(talents.GetSelected());

		nextPlayer.SoftReset();

		return nextPlayer;
	}
}