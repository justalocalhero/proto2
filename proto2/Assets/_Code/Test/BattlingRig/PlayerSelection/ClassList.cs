﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassList : SwappableList<RaceClass>
{
    public ClassList(List<RaceClass> classes)
    {
        foreach(RaceClass raceClass in classes)
        {
            Add(new Selectable<RaceClass>(raceClass));
        }
    }
}
