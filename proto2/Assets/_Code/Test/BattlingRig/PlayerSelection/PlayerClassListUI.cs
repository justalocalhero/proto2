﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClassListUI : SelectableListUI<ClassList> 
{
	public ClassListSlot slotPrototype;
    public override SelectableSlot _slotPrototype {get {return slotPrototype;}}
}
