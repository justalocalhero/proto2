﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClassSelectorUI : MonoBehaviour 
{
	public PlayerClassSelector selector;
	public List<ClassListUI> classLists;

	public void Start()
	{
		UpdateLists();
	}

	public void UpdateLists()
	{
		List<ClassList> selectorClassLists = (selector != null) ? selector.classLists : null;

		for(int i = 0; i < classLists.Count; i++)
		{
			if(selectorClassLists == null || i >= selectorClassLists.Count) classLists[i].Clear();
			else classLists[i].Set(selectorClassLists[i]);
		}
	}
}
