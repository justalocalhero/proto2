﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClassSelector : MonoBehaviour 
{
	public List<ClassManager> classManagers;
	public List<ClassList> classLists;

	public delegate void OnClassChanged(RaceClass raceClass);
	public OnClassChanged onClassChanged;

	public void Awake()
	{
		BuildClassLists();
	}

	public void BuildClassLists()
	{
		foreach(ClassManager manager in classManagers)
		{
			AddClassList(new ClassList(manager.GetClasses()));
		}
	}

	public List<RaceClass> GetSelectedClasses()
	{
		List<RaceClass> toReturn = new List<RaceClass>();

		foreach(ClassList list in classLists)
		{
			toReturn.AddRange(list.GetSelected());
		}


		return toReturn;
	}

	public void AddClassList(ClassList newList)
	{
		if(classLists == null)
		{
			classLists = new List<ClassList>();
		}

		newList.SetAvailable();

		newList.onChanged += RaiseOnClassChanged;

		classLists.Add(newList);
	}

	public void RaiseOnClassChanged(Selectable<RaceClass> selected)
	{
		if(onClassChanged != null) onClassChanged(selected.payload);
	}
}
