﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionListUI : SelectableListUI<ActionBuilder> 
{
	public ActionSlot slotPrototype;
    public override SelectableSlot _slotPrototype {get {return slotPrototype;}}
}
