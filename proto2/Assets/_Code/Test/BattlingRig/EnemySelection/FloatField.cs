﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FloatField : MonoBehaviour 
{
	public TMP_InputField input;

	private float value;

	public delegate void OnChanged(float value);
	public OnChanged onChanged;

	public void RaiseOnChanged()
	{
		if(onChanged != null) onChanged(value);
	}

	public void Set(float newValue)
	{
		value = newValue;
		input.text = newValue.ToString();
	}

	public void Clear()
	{
		Set(0);
	}

	private void Awake()
	{
		input.characterValidation = TMP_InputField.CharacterValidation.Decimal;

		input.onValueChanged.AddListener(HandleInput);

	}

	private void HandleInput(string str)
	{
		float.TryParse(str, out value);

		RaiseOnChanged();
	}

	public float GetValue()
	{
		return value;
	}

}
