﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyClassSelector : MonoBehaviour 
{
    public List<EncounterGenerator> encounterGenerators;
	public SwappableList<EncounterClassList> encounters;

    public delegate void OnEncounterChanged();
    public OnEncounterChanged onEncounterChanged;

    public delegate void OnClassChanged(RaceClass raceClass);
    public OnClassChanged onClassChanged;

    public void Awake()
    {
        BuildEncounterClassLists();
    }

    private void BuildEncounterClassLists()
    {
        foreach(EncounterGenerator generator in encounterGenerators)
        {            
            foreach(EncounterEnemyGenerator enemyGenerator in generator.enemies)
            {
                AddEncounter(new EncounterClassList(enemyGenerator.enemyGenerator));
            }
        }
    }
    
    public void AddEncounter(EncounterClassList encounter)
    {
        if(encounters == null) 
        {
            encounters = new SwappableList<EncounterClassList>();
            encounters.onChanged += RaiseOnEncounterChanged;
        }

        Selectable<EncounterClassList> temp = new Selectable<EncounterClassList>(encounter);
        temp.SetAvailable();

        encounter.onChanged += RaiseOnClassChanged;

        encounters.Add(temp);
    }

    public EncounterClassList GetSelected()
    {        
        return (encounters != null && encounters.selected != null)? encounters.selected.payload : null;
    }

    public void RaiseOnClassChanged(RaceClass raceClass)
    {
        if(onClassChanged != null) onClassChanged(raceClass);
    }

    public void RaiseOnEncounterChanged(Selectable<EncounterClassList> list)
    {
        if(onEncounterChanged != null) onEncounterChanged();
    }

    public List<RaceClass> GetSelectedClasses()
    {
        EncounterClassList classList = GetSelected();

        if(classList != null) return classList.GetSelectedClasses();
        else return new List<RaceClass>();
    }
}
