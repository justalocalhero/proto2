﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SelectableClassSlot : SelectableSlot<RaceClass>
{
	public TextMeshProUGUI mesh;

	public override void RegisterUpdateUIDelegates()
	{
		updateUI += UpdateText;
		base.RegisterUpdateUIDelegates();
	}

	public void UpdateText()
	{
		if(selectable != null && selectable.payload != null) mesh.SetText(selectable.payload.name);
		else mesh.SetText("");
	}
}
