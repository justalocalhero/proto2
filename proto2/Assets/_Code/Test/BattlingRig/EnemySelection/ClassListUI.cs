﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassListUI : SelectableListUI<RaceClass> 
{
	public SelectableClassSlot slotPrototype;
    public override SelectableSlot _slotPrototype {get {return slotPrototype;}}
}