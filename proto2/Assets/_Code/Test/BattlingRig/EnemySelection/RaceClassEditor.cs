﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RaceClassEditor : MonoBehaviour 
{
	public TextMeshProUGUI mesh;
	public AbilityEditor abilityEditor;
	public ActionListUI actionListUI;

	public EnemyClassSelector enemyClassSelector;
	public PlayerClassSelector playerClassSelector;
	public Button addButton;
	private RaceClass raceClass;
	public EquipStatField equipStatPrototype;
	public Transform statContainer;
	public FloatReference spacing;
	public IntReference rows, columns;

	private List<EquipStatField> stats = new List<EquipStatField>();
    private SwappableList<ActionBuilder> actions = new SwappableList<ActionBuilder>();

	public void Start()
	{
		BuildStatFields();
		actionListUI.Set(actions);
		
		actions.onChanged += UpdateAbilities;
		addButton.onClick.AddListener(HandleAdd);
		enemyClassSelector.onClassChanged += Set;
		playerClassSelector.onClassChanged += Set;
	}

	public void Set(RaceClass raceClass)
	{
		mesh.SetText(raceClass.GetName());
		this.raceClass = raceClass;
		UpdateStats();
		UpdateAbilityIcons();
	}

	public void UpdateStats()
	{
		EquipStat[] newStats = (raceClass != null)? raceClass.staticStats : new EquipStat[0];

		for(int i = 0; i < stats.Count; i++)
		{
			if(i < newStats.Length) stats[i].Set(newStats[i]);
			else stats[i].Clear();
		}
	}

	public void UpdateAbilityIcons()
	{
		AbilityBuilder[] abilities = (raceClass != null)? raceClass.abilities : new AbilityBuilder[0];
		TriggeredActionBuilder[] triggers = (raceClass != null)? raceClass.triggeredActions : new TriggeredActionBuilder[0];
	
		actions.Clear();

		foreach(AbilityBuilder ability in abilities)
		{
			Selectable<ActionBuilder> temp = new Selectable<ActionBuilder>(ability);
			temp.SetAvailable();

			actions.Add(temp);
		}
		
		foreach(TriggeredActionBuilder trigger in triggers)
		{
			Selectable<ActionBuilder> temp = new Selectable<ActionBuilder>(trigger);
			temp.SetAvailable();

			actions.Add(temp);
		}

		actionListUI.UpdateUI();
		UpdateAbilities();
	}

	public void UpdateAbilities(Selectable<ActionBuilder> builder)
	{
		UpdateAbilities();
	}

	public void UpdateAbilities()
	{
		List<ActionBuilder> selectedList = actions.GetSelected();
		ActionBuilder selectedAction = (selectedList.Count > 0) ? selectedList[0] : null;

		if(selectedAction != null) abilityEditor.Set(selectedAction);
		else abilityEditor.Clear();
	}

	public void BuildStatFields()
	{
		GridBuilder<EquipStatField> gridBuilder = new GridBuilder<EquipStatField>();

		stats = gridBuilder.Begin()
			.WithPrototype(equipStatPrototype)
			.WithContainer(statContainer)
			.WithSpacing(spacing, spacing)
			.WithRows(rows)
			.WithColumns(columns)
			.Build();

		SetStatIndeces();
		RegisterStatDelegates();
	}

	private void SetStatIndeces()
	{
		for(int i = 0; i < stats.Count; i++)
		{
			stats[i].index = i;
		}
	}

	private void RegisterStatDelegates()
	{
		for(int i = 0; i < stats.Count; i++)
		{
			stats[i].onDelete += HandleDelete;
			stats[i].onEdit += HandleEdit;
		}
	}

	public void HandleDelete(int index)
	{
		raceClass.DeleteStaticStat(index);

		UpdateStats();
	}

	public void HandleEdit(int index, EquipStat stat)
	{
		raceClass.EditStaticStat(index, stat);

		UpdateStats();
	}
	
	public void HandleAdd()
	{
		raceClass.ExtendStaticStats();

		UpdateStats();
	}


}
