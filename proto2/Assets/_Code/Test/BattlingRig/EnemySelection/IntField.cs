﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class IntField : MonoBehaviour 
{
	public TMP_InputField input;

	private int value;

	public delegate void OnChanged(int value);
	public OnChanged onChanged;

	public void RaiseOnChanged()
	{
		if(onChanged != null) onChanged(value);
	}

	public void Set(int newValue)
	{
		value = newValue;
		input.text = newValue.ToString();
	}

	private void Start()
	{
		input.characterValidation = TMP_InputField.CharacterValidation.Integer;

		input.onValueChanged.AddListener(HandleInput);

	}

	private void HandleInput(string str)
	{
		int.TryParse(str, out value);

		RaiseOnChanged();
	}

	public int GetValue()
	{
		return value;
	}

}
