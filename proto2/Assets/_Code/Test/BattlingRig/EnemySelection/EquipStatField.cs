﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipStatField : MonoBehaviour 
{
	public int index {get; set;}
	public FloatField floatField;
	public EnumDropdown modifierType;
	public EnumDropdown statType;
	public Button editButton;
	public Button deleteButton;

	public delegate void OnDelete(int index);
	public OnDelete onDelete;

	public delegate void OnEdit(int index, EquipStat stat);
	public OnEdit onEdit;

	public void Start()
	{
		editButton.onClick.AddListener(HandleEdit);
		deleteButton.onClick.AddListener(HandleDelete);
		modifierType.SetOptions(new ModifierType());
		statType.SetOptions(new StatType());

	}

	public void Set(EquipStat stat)
	{
		int modIndex = (int)stat.ModifierType;
		int statIndex = (int)stat.StatType;

		floatField.Set(stat.Value);
		modifierType.Set(modIndex);
		statType.Set(statIndex);

		Show();
	}

	public void Clear()
	{
		Hide();
	}

	public void Show()
	{
		if(!gameObject.activeSelf) gameObject.SetActive(true);
	}

	public void Hide()
	{
		if(gameObject.activeSelf) gameObject.SetActive(false);
	}

	public void HandleEdit()
	{
		if(onEdit != null) onEdit(index, GetStat());
	}

	public void HandleDelete()
	{
		if(onDelete != null) onDelete(index);
	}

	public EquipStat GetStat()
	{
		return new EquipStat()
		{
			Value = floatField.GetValue(),
			ModifierType = (ModifierType) modifierType.GetIndex(),
			StatType = (StatType) statType.GetIndex()
		};
	}
}
