﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class AbilityEditor : MonoBehaviour 
{
	public UIToggler toggler;
    private ActionBuilder actionBuilder;
	public TextMeshProUGUI mesh;
	public Button addButton;
    public DurationField durationField;
	public ConditionField conditionField;
	public DamageField damageFieldPrototype;
	public Transform damageContainer;
	public FloatReference spacing;
	public IntReference rows, columns;
	public Pager timedEffectPager;
	public TimedEffectEditor timedEffectEditor;

	private List<DamageField> damages = new List<DamageField>();


	public void Start()
	{
		BuildStatFields();
		RegisterConditionDelegates();
		RegisterDurationDelegates();

		timedEffectPager.onChanged += UpdateTimedEffects;
		addButton.onClick.AddListener(HandleDamageAdd);
	}

	public void Set(ActionBuilder actionBuilder)
	{
		mesh.SetText(actionBuilder.name);
        this.actionBuilder = actionBuilder;

		UpdateCondition();
		UpdateDuration();
		UpdateDamages();
		UpdateTimedEffects();

		Show();
	}

	public void Clear()
	{
		Hide();
	}

	public void Show()
	{
		toggler.Show();
	}

	public void Hide()
	{
		toggler.Hide();
	}

	public void UpdateDamages()
	{
		Damage[] newDamages = (actionBuilder != null)? actionBuilder.damages : new Damage[0];

		for(int i = 0; i < damages.Count; i++)
		{
			if(i < newDamages.Length) damages[i].Set(newDamages[i]);
			else damages[i].Clear();
		}

		if(newDamages.Length >= damages.Count) HideAddButton();
		else ShowAddButton();

	}

	public void UpdateCondition()
	{
		if(actionBuilder == null)
		{
			conditionField.Clear();
			return;
		}

		if(actionBuilder is TriggeredActionBuilder)
		{
			durationField.Clear();
			TriggeredActionBuilder temp = actionBuilder as TriggeredActionBuilder;
			conditionField.Set(temp.procTrigger, temp.procTarget, temp.procChance);
		}

		else durationField.Clear();
	}

	public void UpdateDuration()
	{
		if(actionBuilder == null)
		{
			durationField.Clear();
			return;
		}

		if(actionBuilder is AbilityBuilder)
		{
			conditionField.Clear();
			durationField.Set((actionBuilder as AbilityBuilder).period);
		}

		else durationField.Clear();
	}

	public void UpdateTimedEffects(int page)
	{
		UpdateTimedEffects();
	}
	
	public void UpdateTimedEffects()
	{
		if(actionBuilder == null)
		{
			timedEffectPager.SetMaxPages(0);
			return;
		}
		
		timedEffectPager.SetMaxPages(TimedEffectCount());

		timedEffectEditor.Set(GetEffect(timedEffectPager.CurrentPage()));
	}

	public TimedEffectBuilder GetEffect(int page)
	{
		if(actionBuilder == null) return null;

		int buffLength = actionBuilder.buffs.Length;

		if(buffLength > page) return actionBuilder.buffs[page];
		else if(actionBuilder.debuffs.Length > page - buffLength) return actionBuilder.debuffs[page - buffLength];
		else return null;
	}

	public int TimedEffectCount()
	{
		if(actionBuilder == null) return 0;
		else return actionBuilder.buffs.Length + actionBuilder.debuffs.Length;
	}

	public void BuildStatFields()
	{
		GridBuilder<DamageField> gridBuilder = new GridBuilder<DamageField>();

		damages = gridBuilder.Begin()
			.WithPrototype(damageFieldPrototype)
			.WithContainer(damageContainer)
			.WithSpacing(spacing, spacing)
			.WithRows(rows)
			.WithColumns(columns)
			.Build();

		SetDamageIndices();
		RegisterDamageDelegates();
	}

	private void SetDamageIndices()
	{
		for(int i = 0; i < damages.Count; i++)
		{
			damages[i].index = i;
		}
	}

	private void RegisterDamageDelegates()
	{
		for(int i = 0; i < damages.Count; i++)
		{
			damages[i].onDelete += HandleDamageDelete;
			damages[i].onEdit += HandleDamageEdit;
		}
	}

	private void RegisterConditionDelegates()
	{
		conditionField.onEdit += HandleConditionEdit;
	}

	public void HandleConditionEdit(ProcTrigger trigger, ProcTarget target, float chance)
	{
		if(!(actionBuilder is TriggeredActionBuilder)) return;
		TriggeredActionBuilder temp = actionBuilder as TriggeredActionBuilder;

		temp.EditCondition(trigger, target, chance);
	}

	private void RegisterDurationDelegates()
	{
		durationField.onEdit += HandleDurationEdit;
	}

	public void HandleDurationEdit(float duration)
	{
		if(!(actionBuilder is AbilityBuilder)) return;
		AbilityBuilder temp = actionBuilder as AbilityBuilder;

		temp.EditDuration(duration);
	}

	public void HandleDamageDelete(int index)
	{
		actionBuilder.DeleteDamage(index);

		UpdateDamages();
	}

	public void HandleDamageEdit(int index, Damage damage)
	{
		actionBuilder.EditDamage(index, damage);

		UpdateDamages();
	}
	
	public void HandleDamageAdd()
	{
		actionBuilder.ExtendDamage();

		UpdateDamages();
	}

	public void HideAddButton()
	{
		if(addButton.gameObject.activeSelf) addButton.gameObject.SetActive(false);
	}

	public void ShowAddButton()
	{
		if(!addButton.gameObject.activeSelf) addButton.gameObject.SetActive(true);

	}
}
