﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class Dropdown : MonoBehaviour 
{
	public TMP_Dropdown dropdown;
	private int index;

	public delegate void OnChanged(int index);
	public OnChanged onChanged;

	public void RaiseOnChanged()
	{
		if(onChanged != null) onChanged(index);
	}

	public void Start()
	{
		dropdown.onValueChanged.AddListener(HandleChange);
	}

	public void SetOptions(List<string> options)
	{		
		dropdown.AddOptions(options);
	}

	public void Set(int newIndex)
	{
		if(newIndex < 0 || newIndex >= dropdown.options.Count) return;

		index = newIndex;
		dropdown.value = newIndex;
	}

	public void HandleChange(int newIndex)
	{
		index = newIndex;
		RaiseOnChanged();
	}

	public int GetIndex()
	{
		return index;
	}
}
