﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class EnumDropdown : Dropdown 
{
	public void SetOptions(Enum e)
	{
		string[] names = Enum.GetNames(e.GetType());
		List<string> options = new List<string>(names);
		
		SetOptions(options);
	}
}
