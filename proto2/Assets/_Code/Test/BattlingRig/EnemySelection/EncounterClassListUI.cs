﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterClassListUI : SelectableListUI<EncounterClassList> 
{
	public EncounterSlot slotPrototype;
    public override SelectableSlot _slotPrototype {get {return slotPrototype;}}
}
