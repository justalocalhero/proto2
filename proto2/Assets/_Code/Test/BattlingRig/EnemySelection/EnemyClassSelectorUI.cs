﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyClassSelectorUI : MonoBehaviour 
{
	public EnemyClassSelector selector;

	public EncounterClassListUI encounters;
	public ClassListUI primary;
	public ClassListUI secondary;

	public void Awake()
	{
		selector.onEncounterChanged += UpdateClasses;
	}

	public void Start()
	{
		UpdateEncounters();
	}

	public void UpdateClasses(Selectable<EncounterClassList> encounter)
	{
		UpdateClasses();
	}

	public void UpdateEncounters()
	{
		bool clearEncounters = (selector == null || selector.encounters == null);

		if(clearEncounters)
			encounters.Clear();
		else
		{
			encounters.Set(selector.encounters);
		}
	}

	public void UpdateClasses()
	{
		if(selector == null) return;
		EncounterClassList encounter = selector.GetSelected();
		if(encounter == null) 
		{
			primary.Clear();
			secondary.Clear();
			return;
		}

		primary.Set(encounter.primary);
		secondary.Set(encounter.secondary);

	}
}
