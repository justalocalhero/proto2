﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TimedEffectEditor : MonoBehaviour 
{
    private TimedEffectBuilder timedEffect;
    public UIToggler toggler;
    public Button addDamageButton, addStatButton;
    public TextMeshProUGUI nameMesh;
    public EquipStatField equipStatFieldPrototype;
    public DamageField damageFieldPrototype;
    public Transform statContainer, damageContainer;
    public float spacing;

    private List<DamageField> damages = new List<DamageField>();
    private List<EquipStatField> stats = new List<EquipStatField>();
    public DurationField duration;
    public DurationField period;

    public void Start()
    {
		BuildStatFields();
        BuildDamageFields();
		BuildDurationField();
		BuildPeriodField();
        BuildButtons();
    }    

    public void Set(TimedEffectBuilder newEffect)
    {
        timedEffect = newEffect;

        if(timedEffect == null) 
        {
            Clear();
            return;
        }

        UpdateName();
		UpdateDuration();
        UpdatePeriod();
		UpdateDamages();
		UpdateStats();

        Show();
    }

	private void SetStatIndices()
	{
		for(int i = 0; i < stats.Count; i++)
		{
			stats[i].index = i;
		}
	}

	private void RegisterStatDelegates()
	{
		for(int i = 0; i < stats.Count; i++)
		{
			stats[i].onDelete += HandleStatDelete;
			stats[i].onEdit += HandleStatEdit;
		}
	}

    public void BuildDamageFields()
	{
		GridBuilder<DamageField> gridBuilder = new GridBuilder<DamageField>();

	    damages = gridBuilder.Begin()
			.WithPrototype(damageFieldPrototype)
			.WithContainer(damageContainer)
			.WithSpacing(spacing, spacing)
            .WithRows(3)
            .WithColumns(1)
			.Build();

		SetDamageIndices();
		RegisterDamageDelegates();
	}

	private void SetDamageIndices()
	{
		for(int i = 0; i < damages.Count; i++)
		{
			damages[i].index = i;
		}
	}

	private void RegisterDamageDelegates()
	{
		for(int i = 0; i < damages.Count; i++)
		{
			damages[i].onDelete += HandleDamageDelete;
			damages[i].onEdit += HandleDamageEdit;
		}
	}

	public void BuildDurationField()
    {
        period.SetLabel("Duration");
        duration.onEdit += HandleDurationEdit;
    }

	public void BuildPeriodField()
    {
        period.SetLabel("Period");
        period.onEdit += HandlePeriodEdit;
    }

    public void BuildButtons()
    {
        addDamageButton.onClick.AddListener(HandleDamageAdd);
        addStatButton.onClick.AddListener(HandleStatAdd);
    }

    public void BuildStatFields()
	{
		GridBuilder<EquipStatField> gridBuilder = new GridBuilder<EquipStatField>();

		stats = gridBuilder.Begin()
			.WithPrototype(equipStatFieldPrototype)
			.WithContainer(statContainer)
			.WithSpacing(spacing, spacing)
            .WithRows(3)
            .WithColumns(1)
			.Build();

		SetStatIndices();
		RegisterStatDelegates();
	}

    private void UpdateName()
    {
        nameMesh.SetText(timedEffect.name);
    }

    private void UpdateDuration()
    {
        duration.Set(timedEffect.duration);
    }
    
    private void UpdatePeriod()
    {
        if(timedEffect.periodicDamages.Length > 0) 
            period.Set(timedEffect.period);
        else 
            period.Clear();
    }

    private void UpdateDamages()
    {
        Damage[] newDamages = (timedEffect != null)? timedEffect.periodicDamages : new Damage[0];

		for(int i = 0; i < damages.Count; i++)
		{
			if(i < newDamages.Length) damages[i].Set(newDamages[i]);
			else damages[i].Clear();
		}

		if(newDamages.Length >= damages.Count) HideButton(addDamageButton);
		else ShowButton(addDamageButton);
    }

    private void UpdateStats()
    {
        EquipStat[] newStats = (timedEffect != null)? timedEffect.stats : new EquipStat[0];

		for(int i = 0; i < stats.Count; i++)
		{
			if(i < newStats.Length) stats[i].Set(newStats[i]);
			else stats[i].Clear();
		}

		if(newStats.Length >= stats.Count) HideButton(addStatButton);
		else ShowButton(addStatButton);
    }

    public void Clear()
    {
        Hide();
    }

    public void Show()
    {
        toggler.Show();
    }

    public void Hide()
    {
        toggler.Hide();
    }

    public void HandleDamageEdit(int index, Damage damage)
    {
        timedEffect.EditPeriodicDamage(index, damage);

        UpdateDamages();
    }

    public void HandleDamageAdd()
    {
        timedEffect.AddPeriodicDamage();

        UpdateDamages();
    }

    public void HandleDamageDelete(int index)
    {
        timedEffect.RemovePeriodicDamage(index);

        UpdateDamages();
    }

    public void HandleDurationEdit(float duration)
    {
        timedEffect.EditDuration(duration);

        UpdateDuration();
    }

    public void HandlePeriodEdit(float period)
    {
        timedEffect.EditPeriod(period);

        UpdatePeriod();
    }

    public void HandleStatEdit(int index, EquipStat stat)
    {
        timedEffect.EditStat(index, stat);
        
        UpdateStats();
    }

    public void HandleStatAdd()
    {
        timedEffect.AddStat();

        UpdateStats();
    }

    public void HandleStatDelete(int index)
    {
        timedEffect.RemoveStat(index);
        
        UpdateStats();
    }
    
    public void HideButton(Button button)
	{
		if(button.gameObject.activeSelf) button.gameObject.SetActive(false);
	}

	public void ShowButton(Button button)
	{
		if(!button.gameObject.activeSelf) button.gameObject.SetActive(true);
	}
}
