﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LootEditor : MonoBehaviour 
{
	public List<Outfitter> outfitters;
	public AbilityEditor abilityEditor;
	public ActionListUI actionListUI;

	public TextMeshProUGUI mesh;
	public Button addButton;
	private LootBuilder lootBuilder;
	private Loot.AbilityAddon ability;
	public EquipStatField equipStatPrototype;
	public Transform statContainer;
	public FloatReference spacing;
	public IntReference rows, columns;

	private List<EquipStatField> stats = new List<EquipStatField>();
    private SwappableList<ActionBuilder> actions = new SwappableList<ActionBuilder>();


	public void Start()
	{
		BuildStatFields();
		actionListUI.Set(actions);
		actions.onChanged += UpdateAbilities;
		
		addButton.onClick.AddListener(HandleAdd);

		// foreach(Outfitter outfitter in outfitters)
		// {
		// 	outfitter.onDesignSelected += Set;
		// 	outfitter.onDivinitySelected += Set;
		// 	outfitter.onEnchantmentSelected += Set;
		// 	outfitter.onMaterialSelected += Set;
		// 	outfitter.onQualitySelected += Set;
		// }
	}

	public void Clear()
	{
		mesh.SetText("");
		this.lootBuilder = null;
		UpdateStats();
	}

	public void Set(LootBuilder lootBuilder, Loot.AbilityAddon ability)
	{
		mesh.SetText(lootBuilder.GetName());
		this.ability = ability;
		this.lootBuilder = lootBuilder;
		UpdateStats();
		UpdateAbilityIcons();
	}

	public void Set(LootBuilder lootBuilder)
	{
		Set(lootBuilder, null);
	}

	public void UpdateStats()
	{
		List<EquipStat> newStats = (lootBuilder != null)? lootBuilder.GetStats() : new List<EquipStat>();

		for(int i = 0; i < stats.Count; i++)
		{
			if(i < newStats.Count) stats[i].Set(newStats[i]);
			else stats[i].Clear();
		}
	}
	
	public void UpdateAbilityIcons()
	{
		actions.Clear();

		AbilityBuilder[] abilities = (ability != null)? new AbilityBuilder[]{ability.abilityBuilder} : new AbilityBuilder[0];
		TriggeredActionBuilder[] triggers = (ability != null)? new TriggeredActionBuilder[]{ability.triggeredActionBuilder} : new TriggeredActionBuilder[0];
	

		foreach(AbilityBuilder ability in abilities)
		{
			if(ability != null)
			{
				Selectable<ActionBuilder> temp = new Selectable<ActionBuilder>(ability);
				temp.SetAvailable();

				actions.Add(temp);
			}
		}
		
		foreach(TriggeredActionBuilder trigger in triggers)
		{
			if(trigger != null)
			{				
				Selectable<ActionBuilder> temp = new Selectable<ActionBuilder>(trigger);
				temp.SetAvailable();

				actions.Add(temp);
			}
		}		

		Debug.Log(actions.selectables.Count);

		actionListUI.UpdateUI();
		UpdateAbilities();
	}

	public void UpdateAbilities(Selectable<ActionBuilder> builder)
	{
		UpdateAbilities();
	}

	public void UpdateAbilities()
	{
		List<ActionBuilder> selectedList = actions.GetSelected();
		ActionBuilder selectedAction = (selectedList.Count > 0) ? selectedList[0] : null;

		if(selectedAction != null) abilityEditor.Set(selectedAction);
		else abilityEditor.Clear();
	}

	public void BuildStatFields()
	{
		GridBuilder<EquipStatField> gridBuilder = new GridBuilder<EquipStatField>();

		stats = gridBuilder.Begin()
			.WithPrototype(equipStatPrototype)
			.WithContainer(statContainer)
			.WithSpacing(spacing, spacing)
			.WithRows(rows)
			.WithColumns(columns)
			.Build();

		SetStatIndeces();
		RegisterStatDelegates();
	}

	private void SetStatIndeces()
	{
		for(int i = 0; i < stats.Count; i++)
		{
			stats[i].index = i;
		}
	}

	private void RegisterStatDelegates()
	{
		for(int i = 0; i < stats.Count; i++)
		{
			stats[i].onDelete += HandleDelete;
			stats[i].onEdit += HandleEdit;
		}
	}

	public void HandleDelete(int index)
	{
		lootBuilder.RemoveStat(index);

		UpdateStats();
	}

	public void HandleEdit(int index, EquipStat stat)
	{
		lootBuilder.EditStat(index, stat);

		UpdateStats();
	}
	
	public void HandleAdd()
	{
		lootBuilder.AddStat();

		UpdateStats();
	}


}
