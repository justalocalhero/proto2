﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TextField : MonoBehaviour 
{
	public TMP_InputField input;

	private string value;

	public delegate void OnChanged(string value);
	public OnChanged onChanged;

	public void RaiseOnChanged()
	{
		if(onChanged != null) onChanged(value);
	}

	public void Set(string newValue)
	{
		if(newValue == null) value = "";
		else value = newValue;

		input.text = value.ToString();
	}

	private void Start()
	{

		input.onValueChanged.AddListener(HandleInput);

	}

	private void HandleInput(string str)
	{
		value = str;
		
		RaiseOnChanged();
	}

	public string GetValue()
	{
		return value;
	}

}
