﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageField : MonoBehaviour 
{
	public int index {get; set;}
	public FloatField damageValue;
	public FloatField damageFactor;
	public EnumDropdown damageType;
	public Button editButton;
	public Button deleteButton;

	public delegate void OnDelete(int index);
	public OnDelete onDelete;

	public delegate void OnEdit(int index, Damage damage);
	public OnEdit onEdit;

	public void Start()
	{
		editButton.onClick.AddListener(HandleEdit);
		deleteButton.onClick.AddListener(HandleDelete);
		damageType.SetOptions(new DamageType());

	}

	public void Set(Damage damage)
	{
		int typeIndex = (int)damage.type;

		damageType.Set(typeIndex);
		damageValue.Set(damage.value);
		damageFactor.Set(damage.damageFactor);

		Show();
	}

	public void Clear()
	{
		Hide();
	}

	public void Show()
	{
		if(!gameObject.activeSelf) gameObject.SetActive(true);
	}

	public void Hide()
	{
		if(gameObject.activeSelf) gameObject.SetActive(false);
	}

	public void HandleEdit()
	{
		if(onEdit != null) onEdit(index, GetDamage());
	}

	public void HandleDelete()
	{
		if(onDelete != null) onDelete(index);
	}

	public Damage GetDamage()
	{
		return new Damage()
		{
			value = damageValue.GetValue(),
			damageFactor = damageFactor.GetValue(),
			type = (DamageType) damageType.GetIndex()
		};
	}
}
