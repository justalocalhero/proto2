﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DurationField : MonoBehaviour 
{
	public TextMeshProUGUI label;
	public FloatField floatField;
	public Button editButton;

	public delegate void OnEdit(float duration);
	public OnEdit onEdit;

	public void Start()
	{
		SetLabel("Duration");
		editButton.onClick.AddListener(HandleEdit);
	}

	public void SetLabel(string text)
	{
		label.SetText(text);
	}

	public void Set(float duration)
	{
		floatField.Set(duration);

		Show();
	}

	public void Clear()
	{
		Hide();
	}

	public void Show()
	{
		if(!gameObject.activeSelf) gameObject.SetActive(true);
	}

	public void Hide()
	{
		if(gameObject.activeSelf) gameObject.SetActive(false);
	}

	public void HandleEdit()
	{
		if(onEdit != null) onEdit(floatField.GetValue());
	}
}
