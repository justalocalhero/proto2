﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ConditionField : MonoBehaviour 
{
	public TextMeshProUGUI label;
	public EnumDropdown condition;
	public EnumDropdown target;
	public FloatField chance;
	public Button editButton;

	public delegate void OnEdit(ProcTrigger trigger, ProcTarget target, float chance);
	public OnEdit onEdit;

	public void Start()
	{
		label.SetText("Condition");
		editButton.onClick.AddListener(HandleEdit);
		condition.SetOptions(new ProcTrigger());
		target.SetOptions(new ProcTarget());

	}

	public void Set(ProcTrigger trigger, ProcTarget target, float chance)
	{		
		SetTrigger(trigger);
		SetTarget(target);
		SetChance(chance);

		Show();
	}

	public void SetTrigger(ProcTrigger trigger)
	{
		int triggerIndex = (int)trigger;

		condition.Set(triggerIndex);
	}

	public void SetTarget(ProcTarget target)
	{	
		int targetIndex = (int)target;

		this.target.Set(targetIndex);
	}

	public void SetChance(float chance)
	{
		this.chance.Set(chance);
	}

	public void Clear()
	{
		Hide();
	}

	public void Show()
	{
		if(!gameObject.activeSelf) gameObject.SetActive(true);
	}

	public void Hide()
	{
		if(gameObject.activeSelf) gameObject.SetActive(false);
	}

	public void HandleEdit()
	{
		if(onEdit != null) onEdit((ProcTrigger)condition.GetIndex(), (ProcTarget)target.GetIndex(), chance.GetValue());
	}
}