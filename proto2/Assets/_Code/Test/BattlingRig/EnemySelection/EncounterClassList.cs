﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterClassList
{
	public SwappableList<RaceClass> primary {get; set;}
	public SwappableList<RaceClass> secondary {get; set;}

	public delegate void OnChanged(RaceClass raceClass);
	public OnChanged onChanged;

	public EncounterClassList(EnemyGenerator enemyGenerator)
	{
		primary = new SwappableList<RaceClass>();
		primary.onChanged += RaiseOnChanged;

		secondary = new SwappableList<RaceClass>();
		secondary.onChanged += RaiseOnChanged;

		BuildEncounterClassLists(enemyGenerator);
	}

	public void AddPrimary(List<RaceClass> raceClasses)
	{
		foreach(RaceClass raceClass in raceClasses)
		{
			AddPrimary(raceClass);
		}
	}

	public void AddPrimary(RaceClass raceClass)
	{
		Selectable<RaceClass> temp = new Selectable<RaceClass>(raceClass);
		temp.SetAvailable();

		primary.Add(temp);
	}

	public void AddSecondary(List<RaceClass> raceClasses)
	{
		foreach(RaceClass raceClass in raceClasses)
		{
			AddSecondary(raceClass);
		}
	}

	public void AddSecondary(RaceClass raceClass)
	{
		Selectable<RaceClass> temp = new Selectable<RaceClass>(raceClass);
		temp.SetAvailable();

		secondary.Add(temp);
	}

	public void RaiseOnChanged(Selectable<RaceClass> selectable)
	{
		RaiseOnChanged(selectable.payload);
	}

	public void RaiseOnChanged(RaceClass raceClass)
	{
		if(onChanged != null) onChanged(raceClass);
	}

	public List<RaceClass> GetSelectedClasses()
	{
		List<RaceClass> toReturn = new List<RaceClass>();

		toReturn.AddRange(primary.GetSelected());
		toReturn.AddRange(secondary.GetSelected());

		return toReturn;
	}

	public void BuildEncounterClassLists(EnemyGenerator generator)
    {
		foreach(GeneratorRaceClass primary in generator.primaryRaceClass)
		{
			AddPrimary(primary.raceClass);
		}

		foreach(GeneratorRaceClass secondary  in generator.secondaryRaceClass)
		{
			AddSecondary(secondary.raceClass);
		}        
    }
}
