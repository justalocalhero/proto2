﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattlingRigSelectionTab : MonoBehaviour 
{
	public MultiTabSelectorListUI tabSelector;

	public List<MultiTabSelector> tabs;
	private SwappableList<MultiTabSelector> tabList;

	void Start()
	{
		BuildTabs();
	}

	public void BuildTabs()
	{
		if(tabList == null)
		{
			tabList = new SwappableList<MultiTabSelector>();
		}

		foreach(MultiTabSelector tab in tabs)
		{
			Selectable<MultiTabSelector> temp = new Selectable<MultiTabSelector>(tab);

			temp.SetAvailable();
			temp.RegisterOnStateChanged(HandleChange);

			tabList.Add(temp);
		}

		tabSelector.Set(tabList);
	}

	public void HandleChange(Selectable<MultiTabSelector> selector)
	{
		var selectedTabs = new List<MultiTabSelector>();

		foreach(Selectable<MultiTabSelector> tab in tabList.selectables)
		{
			if(!tab.IsSelected()) tab.payload.Hide();	
			else selectedTabs.Add(tab.payload);		
		}

		foreach(MultiTabSelector tab in selectedTabs)
		{
			tab.Show();
		}		
	}
}
