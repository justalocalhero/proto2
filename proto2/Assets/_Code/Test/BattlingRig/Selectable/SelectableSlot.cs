﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectableSlot<T> : SelectableSlot
{
	new public Selectable<T> selectable {get {return _selectable as Selectable<T>;}}
	
	public void Set(Selectable<T> selectable)
	{
		base.Set(selectable);
	}
}

public class SelectableSlot : MonoBehaviour 
{
	protected Selectable _selectable;
	public Selectable selectable {get {return _selectable;}}

	public Button button;

	public ColorReference availableColor;
	public ColorReference unavailableColor;
	public ColorReference selectedColor;

	protected System.Action updateUI;

	public void Awake()
	{
		if(button != null) button.onClick.AddListener(OnClick);
		RegisterUpdateUIDelegates();
	}

	public void Start()
	{		
		UpdateUI();
	}

	public virtual void RegisterUpdateUIDelegates()
	{		
		updateUI += UpdateVisibility;
		updateUI += UpdateColor;
	}

	public void Set(Selectable selectable)
	{
		ClearSelectable();

		_selectable = selectable;
		selectable.RegisterOnStateChanged(HandleChange);

		UpdateUI();
	}

	private void HandleChange(Selectable selectable)
	{
		UpdateUI();
	}

	public void Clear()
	{
		ClearSelectable();

		UpdateUI();
	}

	public void ClearSelectable()
	{		
		if(selectable != null) selectable.DeregisterOnStateChanged(HandleChange);
		_selectable = null;
	}

	protected void OnClick()
	{
		if(selectable == null) 
		{
			Debug.LogWarning("null selectable.  Should not be clickable.");
			return;
		}

		selectable.Toggle();
	}

	protected void UpdateUI()
	{
		if(updateUI != null) updateUI();
	}

	protected void UpdateVisibility()
	{
		if(selectable == null || selectable.IsHidden()) 
			Hide();
		else 
			Show();
	}

	protected void UpdateColor()
	{
		if(selectable == null) return;

		if(selectable.IsAvailable())
			SetButtonColor(availableColor);
		else if(selectable.IsSelected())
			SetButtonColor(selectedColor);
		else if(selectable.IsUnavailable())
			SetButtonColor(unavailableColor);
	}

	protected void SetButtonColor(Color color)
	{
		if(button == null)
		{
			Debug.LogWarning("Button not found");
			return;
		}

		button.image.color = color;
	}

	protected void Show()
	{
		if(!button.gameObject.activeSelf) button.gameObject.SetActive(true);
	}

	protected void Hide()
	{
		if(button.gameObject.activeSelf) button.gameObject.SetActive(false);
	}
	
    public enum SlotState{hidden, unavailable, available, selected,};
}
