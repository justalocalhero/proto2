﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiTabSelectorListUI : SelectableListUI<MultiTabSelector>
{
    public MultiTabSelectorSlot slotPrototype;
    public override SelectableSlot _slotPrototype {get {return slotPrototype;}}
}
