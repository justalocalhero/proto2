﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectableList<T>
{
    public List<Selectable<T>> selectables = new List<Selectable<T>>();
     
	public System.Func<bool> canSelect {get; set;}

    public System.Action<Selectable<T>> handleChange;

    public delegate void OnChanged(Selectable<T> selectable);
    public OnChanged onChanged;

    public void Add(Selectable<T> selectable)
    {
        if(selectable == null) return;

        selectables.Add(selectable);
        if(canSelect != null) selectable.canSelect = canSelect;

        selectable.RegisterOnToggle(HandleChange);
    }

    public void Clear()
    {
        foreach(Selectable<T> selectable in selectables)
        {
            selectable.DeregisterOnToggle(HandleChange);
        }

        selectables.Clear();
    }

    private void HandleChange(Selectable<T> selectable)
    {
        if(handleChange != null) handleChange(selectable);
        if(onChanged != null) onChanged(selectable);
    }

    public int SelectedCount()
    {
        int count = 0;

        foreach(Selectable<T> t in selectables)
        {
            if(t.IsSelected()) count++;
        }

        return count;
    }

    public List<T> GetSelected()
    {
        List<T> toReturn = new List<T>();

        foreach(Selectable<T> selectable in selectables)
        {
            if(selectable.IsSelected()) toReturn.Add(selectable.payload);
        }

        return toReturn;
    }

    public void SetHidden()
	{
		foreach(Selectable selectable in selectables)
        {
            selectable.SetHidden();
        }
	}

    public void SetUnavailable()
    {
        foreach(Selectable selectable in selectables)
        {
            selectable.SetUnavailable();
        }
    }

    public void SetAvailable()
    {
        foreach(Selectable selectable in selectables)
        {
            selectable.SetAvailable();
        }
    }

    public void SetSelected()
    {
        foreach(Selectable selectable in selectables)
        {
            selectable.SetSelected();
        }
    }
}