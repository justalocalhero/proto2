﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SelectableListUI<T> : MonoBehaviour
{
	public abstract SelectableSlot _slotPrototype {get;}
	public SelectableList<T> selectableList;

	private List<SelectableSlot<T>> slots;
	
	public Transform slotContainer;
	public FloatReference spacing;
	public IntReference rows, columns;

	private int page = 0;
	private int entriesPerPage = 0;

	public void Awake() 
	{
		BuildSlots();
		entriesPerPage = slots.Count;
	}

	public void Set(SelectableList<T> list)
	{
		selectableList = list;

		UpdateUI();
	}

	public void Set(SelectableList<T> list, int page)
	{
		this.page = page;

		Set(list);
	}

	public void BuildSlots()
	{
		GridBuilder<MonoBehaviour> gridBuilder = new GridBuilder<MonoBehaviour>();

		List<MonoBehaviour> slotMono = gridBuilder.Begin()
			.WithPrototype(_slotPrototype)
			.WithContainer(slotContainer)
			.WithSpacing(spacing, spacing)
			.WithRows(rows)
			.WithColumns(columns)
			.Build();

		ConvertSlots(slotMono);
	}

	private void ConvertSlots(List<MonoBehaviour> slotMono)
	{
		slots = slotMono.ConvertAll<SelectableSlot<T>>(x => x as SelectableSlot<T>);
	}

	public void UpdateUI()
	{
		int listIndex = page * entriesPerPage;
		int slotIndex = 0;

		List<Selectable<T>> list = (selectableList != null) ? selectableList.selectables : null;

		if(list != null)
		{
			for(int i = listIndex; i < list.Count; i++)
			{
				Selectable<T> current = list[i];

				if(!current.IsHidden() && slots.Count > slotIndex) 
					slots[slotIndex++].Set(current);
			}
		}

		for(int i = slotIndex; i < slots.Count; i++)
		{
			slots[i].Clear();
		}
	}

	public void Clear()
	{
		for(int i = 0; i < slots.Count; i++)
		{
			slots[i].Clear();
		}
	}

	public int EntriesPerPage()
	{
		return entriesPerPage;
	}
}
