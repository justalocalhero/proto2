﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selectable<T> : Selectable
{
    public T payload {get; set;}
    private Delegate<Selectable<T>> onToggle;
    private Delegate<Selectable<T>> onStateChanged;

    public Selectable(T payload)
    {
        this.payload = payload;
    }   

    public void RegisterOnToggle(Delegate<Selectable<T>> func)
    {
        onToggle += func;
    }

    public void DeregisterOnToggle(Delegate<Selectable<T>> func)
    {
        onToggle -= func;
    }

    public void RegisterOnStateChanged(Delegate<Selectable<T>> func)
    {
        onStateChanged += func;
    }

    public void DeregisterOnStateChanged(Delegate<Selectable<T>> func)
    {
        onStateChanged -= func;
    }

    protected override void RaiseOnToggle()
    {
        if(onToggle != null) onToggle(this);
        base.RaiseOnToggle();
    }

    protected override void RaiseOnChanged()
    {
        if(onStateChanged != null) onStateChanged(this);
        base.RaiseOnChanged();
    }
}

public class Selectable
{
	private SelectionState currentState;
	public System.Func<bool> canSelect {get; set;}

    public delegate void Delegate<Selectable>(Selectable selectable);
    private Delegate<Selectable> onStateChanged;
    private Delegate<Selectable> onToggle;

    public void RegisterOnStateChanged(Delegate<Selectable> func)
    {
        onStateChanged += func;
    }

    public void DeregisterOnStateChanged(Delegate<Selectable> func)
    {
        onStateChanged -= func;
    }

    public void RegisterOnToggle(Delegate<Selectable> func)
    {
        onToggle += func;
    }

    public void DeregisterOnToggle(Delegate<Selectable> func)
    {
        onToggle -= func;
    }

    public void Toggle()
    {
        if(IsSelected()) Deselect();
        else Select();

        RaiseOnToggle();
    }

    public void Select()
    {
        if(!CanSelect()) return;
        
        SetSelected();
    }

    public bool CanSelect()
    {
        return (canSelect == null) ? true : canSelect();
    }

    public void Deselect()
    {		
		SetAvailable();
    }

    protected virtual void RaiseOnChanged()
    {
        if(onStateChanged!= null) onStateChanged(this);
    }

    protected virtual void RaiseOnToggle()
    {
        if(onToggle != null) onToggle(this);
    }

    protected void SetState(SelectionState state)
    {
		if(currentState == state) return;

        currentState = state;

        RaiseOnChanged();
    }

	public void SetHidden()
	{
		SetState(SelectionState.hidden);
	}

    public void SetUnavailable()
    {
        SetState(SelectionState.unavailable);
    }

    public void SetAvailable()
    {
        SetState(SelectionState.available);
    }

    public void SetSelected()
    {
        SetState(SelectionState.selected);
    }

	public bool IsHidden()
	{
		return currentState == SelectionState.hidden;
	}

    public bool IsSelected()
    {
        return currentState == SelectionState.selected;
    }

    public bool IsAvailable()
    {
        return currentState == SelectionState.available;
    }

    public bool IsUnavailable()
    {
        return currentState == SelectionState.unavailable;
    }

	public enum SelectionState {hidden, unavailable, available, selected};
}
