﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwappableList<T> : SelectableList<T> 
{
	public Selectable<T> selected {get; set;}

	public SwappableList()
	{
		handleChange += HandleChange;
	}

	private void HandleChange(Selectable<T> selected)
	{
		if(selected == null) return;

		if(this.selected != null && !this.selected.IsSelected()) this.selected = null;
		if(selected.IsSelected())
		{
			if(this.selected != null) this.selected.Deselect();
			this.selected = selected;
		}
	}
}
