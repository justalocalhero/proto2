﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pager : MonoBehaviour 
{
	public Button leftButton;
	public Button rightButton;
	private int maxPages, currentPage = 0;

	public delegate void OnChanged(int currentPage);
	public OnChanged onChanged;

	public void RaiseOnChanged()
	{
		if(onChanged != null) onChanged(currentPage);
	}
	
	public void Awake()
	{
		leftButton.onClick.AddListener(HandleLeft);
		rightButton.onClick.AddListener(HandleRight);
	}

	public void SetMaxPages(int maxPages)
	{
		this.maxPages = maxPages;

		UpdateButtons();
	}

	public void HandleLeft()
	{
		currentPage--;
		ClampPage();
		UpdateButtons();

		RaiseOnChanged();
	}

	public void HandleRight()
	{
		currentPage++;
		ClampPage();
		UpdateButtons();

		RaiseOnChanged();
	}

	public void ClampPage()
	{
		currentPage = Mathf.Clamp(currentPage, 0, maxPages);
	}

	public void UpdateButtons()
	{
		ClampPage();

		bool showLeft = currentPage > 0;
		bool showRight = currentPage < (maxPages - 1);

		UpdateButton(leftButton, showLeft);
		UpdateButton(rightButton, showRight);
	}

	public void UpdateButton(Button button, bool toShow)
	{
		if(toShow) ShowButton(button);
		else HideButton(button);
	}

	public void ShowButton(Button button)
	{
		if(!button.gameObject.activeSelf) button.gameObject.SetActive(true);
	}

	public void HideButton(Button button)
	{
		if(button.gameObject.activeSelf) button.gameObject.SetActive(false);
	}	

	public int CurrentPage()
	{
		return currentPage;
	}
}
