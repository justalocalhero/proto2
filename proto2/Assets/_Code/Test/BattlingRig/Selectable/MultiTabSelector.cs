﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MultiTabSelector 
{	
	public List<UIToggler> togglers;
		
	public void Show()
	{
		foreach(UIToggler toggler in togglers)
		{
			toggler.Show();
		}
	}

	public void Hide()
	{
		foreach(UIToggler toggler in togglers)
		{
			toggler.Hide();
		}
	}
}
