﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlotTester : MonoBehaviour 
{
	public InventorySlot slot;

	// Update is called once per frame
	void Update () 
	{
	}

	private void Toggle(Image image)
	{
		image.enabled = !image.enabled;
	}
}
