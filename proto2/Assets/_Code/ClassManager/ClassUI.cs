﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ClassUI : MonoBehaviour 
{
	public ClassManager classManager;
	public Transform slotContainer;
	private ClassSlot[] slots;
	public RaceClassText hoverTooltip;
	public RaceClassText clickTooltip;

	public TownBuilding classBuilding;
	public Transform tabButtonContainer;
	private Button[] tabButtons;

	public void  Start()
	{
		tabButtons = tabButtonContainer.GetComponentsInChildren<Button>();
		slots = slotContainer.GetComponentsInChildren<ClassSlot>();
		classManager.onClassChanged += UpdateUI;
		classBuilding.onUnlock += UpdateTabButtons;
		PassReferences();
		UpdateTabButtons();
	}

	public void PassReferences()
	{
		for(int i = 0; i < slots.Length; i++)
		{
			slots[i].SetClassManager(classManager);
			slots[i].hoverTooltip = hoverTooltip;
			slots[i].clickTooltip = clickTooltip;
		}
	}

	public void UpdateUI()
	{
		List<RaceClass> classes = classManager.GetClasses();
		if(classes == null) return;
		for(int i = 0; i < slots.Length; i++)
		{
			if(i < classes.Count && classes[i] != null)
			{
				slots[i].Add(classes[i]);
			}
			else slots[i].Clear();
		}
	}

	public void UpdateTabButtons()
	{

		for(int i = 0; i < tabButtons.Length; i++)
		{
			if(classBuilding.unlocked)
				tabButtons[i].gameObject.SetActive(true);
			else
				tabButtons[i].gameObject.SetActive(false);
		}
	}
}
