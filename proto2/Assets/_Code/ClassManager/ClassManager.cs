﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class ClassManager : MonoBehaviour 
{
	public delegate void OnClassChanged();
	public OnClassChanged onClassChanged;

	public delegate void OnPreparedSelectedChanged();
	public OnPreparedSelectedChanged onPreparedSelectedChanged;

	public List<RaceClass> classes = new List<RaceClass>();

	private RaceClass selectedClass, preparedClass;

	public void ClearSelected()
	{
		selectedClass = null;
	}

	public void ClearPrepared()
	{
		if(preparedClass != null)
		{
			preparedClass = null;
			RaiseOnSelectedPreparedChanged();
		}
	}

	public void Add(RaceClass newClass)
	{
		Add(newClass, true);
	}

	public void Add(RaceClass newClass, bool toRaise)
	{
		if(newClass == null) return;
		if(classes.Contains(newClass)) return;
		classes.Add(newClass);
		if(toRaise) RaiseOnClassChanged();
	}

	public void Add(List<RaceClass> classes)
	{
		if(classes == null) return;
		for(int i = 0; i < classes.Count; i++)
		{
			Add(classes[i], false);
		}
		RaiseOnClassChanged();
	}

	public void Remove(RaceClass oldClass)
	{
		Remove(oldClass, true);
	}

	public void Remove(RaceClass oldClass, bool toRaise)
	{
		if(oldClass == null) return;
		if(!classes.Contains(oldClass)) return;
		classes.Remove(oldClass);
		if(toRaise) RaiseOnClassChanged();
	}	

	public void Remove(List<RaceClass> classes)
	{
		if(classes == null) return;
		for(int i = 0; i < classes.Count; i++)
		{
			Remove(classes[i], false);
		}
		RaiseOnClassChanged();
	}

	public void PrepareClass(RaceClass newClass)
	{
		if(newClass == null) return;
		preparedClass = newClass;		
		RaiseOnSelectedPreparedChanged();
	}

	public void SelectPrepared()
	{
		if(preparedClass == null) return; 
		SelectClass(preparedClass);
		ClearPrepared();
	}

	public void SelectClass(RaceClass newClass)
	{
		if(newClass == null) return;
		selectedClass = newClass;
	}

	public void SetClasses(List<RaceClass> newClasses)
	{
		if(newClasses == null) return;
		if(newClasses.Count <= 0) return;
		classes = newClasses;
		RaiseOnClassChanged();
	}

	public List<RaceClass> GetClasses()
	{
		return classes;
	}

	// public void ApplyClassTier(ClassTier tier)
	// {
	// 	if(tier.toReset) SetClasses(tier.toAdd);
	// 	else
	// 	{
	// 		Remove(tier.toRemove);
	// 		Add(tier.toAdd);
	// 	}
	// }

	public void RaiseOnClassChanged()
	{
		if(onClassChanged != null) onClassChanged();
	}

	public void RaiseOnSelectedPreparedChanged()
	{
		if(onPreparedSelectedChanged != null) onPreparedSelectedChanged();
	}

	public RaceClass GetClass()
	{
		if(selectedClass == null) return GetRandomClass();
		else return GetSelectedClass();
	}

	public RaceClass GetSelectedClass()
	{
		return selectedClass;
	}

	public RaceClass GetPreparedClass()
	{
		return preparedClass;
	}

	public RaceClass GetRandomClass()
	{
		if(classes == null) return null;
		if(classes.Count <= 0) return null;
		return classes[UnityEngine.Random.Range(0, classes.Count)];
		
	}
}