﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ClassSelectorExpandedWindow : MonoBehaviour 
{
	private ClassManager currentManager;

	public Button selectButton;
	private TextMeshProUGUI selectText;
	public Button clearButton;
	private TextMeshProUGUI clearText;

	public TextMeshProUGUI mesh;

	public void Start()
	{
		selectText = selectButton.GetComponentInChildren<TextMeshProUGUI>();
		clearText = clearButton.GetComponentInChildren<TextMeshProUGUI>();
	}

	public void SelectClassManager(ClassManager classManager)
	{
		if(classManager == null) return;
		if(currentManager != null) currentManager.onPreparedSelectedChanged -= UpdateUI;
		currentManager = classManager;
		currentManager.onPreparedSelectedChanged += UpdateUI;
		UpdateUI();
	}

	public void OnTabButtonPressed(ClassManager classManager)
	{
		if(classManager == null) return;
		if(currentManager != null) currentManager.ClearPrepared();
		SelectClassManager(classManager);
	}

	public void OnSelectButtonClicked()
	{
		if(currentManager == null) return;

		currentManager.SelectPrepared();
		UpdateUI();
	}

	public void OnClearButtonClicked()
	{
		if(currentManager == null) return;

		currentManager.ClearSelected();
		UpdateUI();
	}

	public void UpdateUI()
	{
		UpdateText();
		UpdateButtons();
	}

	public void UpdateText()
	{
		if(currentManager == null) return;
		RaceClass selectedClass = currentManager.GetSelectedClass();
		if(selectedClass != null) 
		{
			string text = "Selected Class: " + selectedClass.GetName();
			mesh.SetText(text);
		}
		else 
			mesh.SetText("Select a class for your next adventurer.");
	}

	public void UpdateButtons()
	{
		if(currentManager == null) return;

		RaceClass selectedClass = currentManager.GetSelectedClass();
		RaceClass preparedClass = currentManager.GetPreparedClass();

		bool hasSelected = selectedClass != null;
		bool hasPrepared = preparedClass != null;

		string selectedName = (hasSelected) ? selectedClass.GetName() : "";
		string preparedName = (hasPrepared) ? preparedClass.GetName() : "";

		if(clearButton != null) 
		{
			clearButton.gameObject.SetActive(hasSelected);
			clearText.SetText("Clear\n" + selectedName);
		}
		
		if(selectButton != null) 
		{
			selectButton.gameObject.SetActive(hasPrepared);
			selectText.SetText("Select\n" + preparedName);
		}

	}
}
