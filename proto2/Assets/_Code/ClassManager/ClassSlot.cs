﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClassSlot : MonoBehaviour 
{
	private ClassManager classManager;
	private RaceClass characterClass;
	public Button button;
	public Image buttonImage;
	public RaceClassText hoverTooltip;
	public RaceClassText clickTooltip;

	public void SetClassManager(ClassManager newClassManager)
	{
		if(newClassManager == null) return;
		classManager = newClassManager;
	}

	public void Add(RaceClass newClass)
	{
		if(newClass == null) return;
		characterClass = newClass;
		button.enabled = true;
		buttonImage.enabled = true;
	}

	public void Clear()
	{
		characterClass = null;
		button.enabled = false;
		buttonImage.enabled = false;
	}

	public void Select()
	{
		if(characterClass == null) return;
		if(classManager == null) return;
		classManager.SelectClass(characterClass);
	}	

	public void OnClick()
	{
		clickTooltip.Set(characterClass);
		classManager.PrepareClass(characterClass);
	}

	public void OnPointerEnter()
	{
		hoverTooltip.Set(characterClass);
	}

	public void OnPointerExit()
	{
		hoverTooltip.Hide();
	}
}
