﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Temple : ReputationBuilding
{
    public override string GetSaveKey()
    {
        return "Temple" + GetName();
    }
}
