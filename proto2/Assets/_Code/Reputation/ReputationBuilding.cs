﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReputationBuilding : MonoBehaviour 
{
	[System.Serializable]
	public class ReputationSave
	{
		public int offerings;
		public int level;
	}

	private ReputationBuildingManager manager;
	public BoonBuilder[] boonBuilders;
	public Boon[] boons;
	new public StringReference name;
	public StringReference description;
	public IntReference baseOfferingsCost, offeringFactor;
	private int offerings;
	private int level;
	public int maxLevel;

	public delegate void OnLevelUp(int currentLevel);
	public OnLevelUp onLevelUp;

	public delegate void OnAddOffering(int addedOfferings, int totalOfferings);
	public OnAddOffering onAddOffering;

	void Awake()
	{
		boons = GenerateBoons();
	}

	public void Save(SaveFile saveFile)
	{
		if(saveFile == null) return;
		if(saveFile.reputationSaves == null) return;

		saveFile.reputationSaves[GetSaveKey()] = new ReputationSave()
		{
			level = this.level,
			offerings = this.offerings
		};
	}

	public void Load(SaveFile saveFile)
	{
		if(saveFile == null) return;
		if(saveFile.reputationSaves == null) return;

		if(!saveFile.reputationSaves.ContainsKey(GetSaveKey())) return;
		
		ReputationSave tempSave = saveFile.reputationSaves[GetSaveKey()];

		if(tempSave == null) return;
		
		SetLevel(tempSave.level);
		SetOfferings(tempSave.offerings);
	}

	public void LevelUp(int toAdd)
	{
		if(toAdd > 0)
		{
			AdjustLevel(toAdd);
			RaiseOnLevelUp();
		}
	}

	public void AddOffering(int toAdd)
	{
		if(toAdd > 0)
		{
			offerings += toAdd;
			RaiseOnAddOffering(toAdd);
			LevelUp(GetOfferingLevel() - level);
		}
	}

	public void AdjustLevel(int toAdd)
	{
		level += toAdd;
		level = Mathf.Clamp(level, 0, maxLevel);
	}

	public void SetOfferings(int offerings)
	{
		this.offerings = offerings;
		RaiseOnAddOffering(0);
	}
	
	public void SetLevel(int level)
	{
		this.level = level;
		RaiseOnLevelUp();
	}

	public void SetManager(ReputationBuildingManager newManager)
	{
		if(newManager == null) return;
		manager = newManager;
	}

	public int GetOfferingsAtLevel(int level)
	{
		return Mathf.CeilToInt(baseOfferingsCost * Mathf.Pow(offeringFactor, level));
	}

	public int GetOfferingsToLevel()
	{
		return (int) GetOfferingsAtLevel(level + 1) - offerings;
	}

	private int GetOfferingLevel()
	{
		return (int) Mathf.FloorToInt(Mathf.Log(offerings / baseOfferingsCost, offeringFactor));
	}

	public int GetLevel()
	{
		return level;
	}

	public int GetMaxLevel()
	{
		return maxLevel;
	}

	public bool IsMaxed()
	{
		return level < maxLevel;
	}

	public int GetOfferings()
	{
		return offerings;
	}

	public string GetName()
	{
		return name;
	}
	
	public virtual string GetSaveKey()
	{
		Debug.LogWarning("This shouldn't be reached");
		return "";
	}

	public string GetDescription()
	{
		return description;
	}
	
	public BoonBuilder[] GetBoonBuilders()
	{
		return boonBuilders;
	}

	public Boon[] GetBoons()
	{
		return boons;
	}

	public List<Sprite> GetSprites()
	{
		List<Sprite> toReturn = new List<Sprite>();

		foreach(BoonBuilder builder in boonBuilders)
		{
			toReturn.Add(builder.icon);
		}

		return toReturn;
	}

	public Boon GetBoon(int tier)
	{
		if(tier < 0 || tier >= boons.Length || tier >= level) return null;
		return boons[tier];

	}

	private Boon[] GenerateBoons()
	{
		Boon[] toReturn = new Boon[boonBuilders.Length];
		for(int i = 0; i < toReturn.Length; i++)
		{
			toReturn[i] = boonBuilders[i].Generate(i + 1);
		}

		return toReturn;
	}

	public BoonBuilder GetBoonBuilder(int tier)
	{
		if(tier >= 0 && tier < boonBuilders.Length)
		{
			return boonBuilders[tier];
		}
		else
		{
			Debug.LogWarning("Boon request out of range.  Requested tier: " + tier + " Available tiers: " + boonBuilders.Length);
			return null;
		}
	}

	public Boon GetNextBoon()
	{
		BoonBuilder builder = (IsMaxed()) ? GetBoonBuilder(level) : null;
		return builder == null ? null : builder.Generate(level);
	}

	public void RaiseOnLevelUp()
	{
		if(onLevelUp != null) onLevelUp(level);
	}

	public void RaiseOnAddOffering(int addedOfferings)
	{
		if(onAddOffering != null) onAddOffering(addedOfferings, offerings);
	}
}
