﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ReputationWindowUI : MonoBehaviour 
{   
    public Transform managerContainer;
	private ReputationBuildingManager[] managers;

    public Transform managerUIContainer;
    private ReputationManagerUI[] managerUIs;
    public Transform tabButtonContainer;    
    private Button[] tabButtons;

    public ReputationExpandedUI expandedUI;    

    public TextMeshProUGUI availableMesh;
    public ColoredText coloredText;
    public ColorReference maxedColor;
    public ColorReference defaultColor;

    public delegate void OnChanged();
    public OnChanged onChanged;

    public void Start()
    {
        tabButtons = tabButtonContainer.GetComponentsInChildren<Button>();
        managers = managerContainer.GetComponentsInChildren<ReputationBuildingManager>();
        managerUIs = managerUIContainer.GetComponentsInChildren<ReputationManagerUI>();
        RegisterDelegates();
        PassReferences();
        UpdateUI();

    }

    public void RegisterDelegates()
    {
        for(int i = 0; i < managers.Length; i++)
        {
            managers[i].onPreparedSelectedChanged += UpdateAvailableText;
            managers[i].onLevelUp += UpdateAvailableText;
            managers[i].onLevelUp += RaiseOnChanged;
        }


    }

    public void PassReferences()
    {
        for(int i = 0; i < managerUIs.Length; i++)
        {
            if(i < managers.Length)
                managerUIs[i].SetManager(managers[i]);
            else
                Debug.LogWarning("Insufficient Managers in reputation building selector");
        }
    }

    public void UpdateUI()
    {
        UpdateTabButtons();
        UpdateAvailableText();
    }

    public void HideButton(Button button)
    {
        if(button == null) return;
        button.gameObject.SetActive(false);
    }

    public void ShowButton(Button button)
    {
        if(button == null) return;
        button.gameObject.SetActive(true);
    }

    public void UpdateTabButtons()
    {
        for(int i = 0; i < tabButtons.Length; i++)
        {
            UpdateTabButton(i);
        }
    }

    public void UpdateTabButton(int index)
    {
        if(index >= tabButtons.Length) 
        {
            Debug.LogWarning("Index out of bound in UpdateTabButton at ReputationSelector Index: " 
                + index + " TabButtons: " + tabButtons.Length);
            return;
        }
        if(index >= managers.Length)
        {
            Debug.LogWarning("Index out of bound in UpdateTabButton at ReputationSelector Index: " 
                + index + " Managers: " + managers.Length);
            return;
        }

        ShowButton(tabButtons[index]);
    }

    public void OnMenuButtonPressed()
    {
        if(expandedUI == null) return;

        int index = GetAvailableManagerIndex();
        if(index >= 0 && index < managers.Length && index < managerUIs.Length)
        {
            expandedUI.SetReputationBuildingManager(managers[index]);
            managerUIs[index].Show();
        }
        else
        {
            expandedUI.SetReputationBuildingManager(null);
        }
    }

    public int GetAvailableManagerIndex()
    {
        int toReturn = -1;

        for(int i = managers.Length - 1; i >= 0; i--)
        {
           if(managers[i].GetLevel() > 0) toReturn = i;
        }

        return toReturn;
    }

    public void AddOffering(int offerings)
    {
        for(int i = 0; i < managers.Length; i++)
        {
            managers[i].AddOffering(offerings);
        }
    } 

    public int GetMaxLevel()
    {
        int maxLevel = 0;

        for(int i = 0; i < managers.Length; i++)
        {
            int level = managers[i].GetLevel();
            if(level > maxLevel) maxLevel = level;
        }

        return maxLevel;
    }

    public int GetSelectedCount()
    {
        int count = 0;

        for(int i = 0; i < managers.Length; i++)
        {
            count += managers[i].GetSelectedCount();
        }

        return count;
    }

    public int GetAvailableCount()
    {
        int count = 0;
        
        for(int i = 0; i < managers.Length; i++)
        {
            count += managers[i].GetAvailableCount();
        }

        return count;
    }

    public void RaiseOnChanged()
    {
        if(onChanged != null) onChanged();
    }

    public void UpdateAvailableText()
    {
        int available = GetAvailableCount();
        int selected = GetSelectedCount();
        string text = (available > 0) ? selected + " / " + available : "";
        Color color = (available == selected) ? maxedColor : defaultColor;
        availableMesh.SetText(coloredText.GetText(text, color));
    }

}
