﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Boons
{
	public class BoonCountUI : MonoBehaviour 
	{
		public ReputationManager reputationManager;
		public TextMeshProUGUI availableMesh;

		public ColorReference maxedColor;
		public ColorReference defaultColor;

		void Awake()
		{
			reputationManager.onChanged += UpdateUI;
			reputationManager.onTierUp += UpdateUI;
		}

		public void UpdateUI()
		{
			UpdateAvailableText();
		}

		public void UpdateAvailableText()
		{
			int available = reputationManager.GetAvailableCount();
			int selected = reputationManager.GetSelectedCount();
			string text = (available > 0) ? selected + " / " + available : "";
			Color color = (available == selected) ? maxedColor : defaultColor;;

			availableMesh.SetText(Tooltips.ColoredText.GetText(text, color));
		}
	}
}