﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Boons
{
	public class BoonManagerUI : MonoBehaviour 
	{
		public UIToggler toggler;
		public Tooltips.BoonCompareTooltip boonCompareTooltip;
		public Tooltips.BoonTooltip boonTooltip;
		public Transform slotContainer, headerContainer, boonManagerContainer;
		public BoonSlot slotPrototype;
		public HeaderSlot headerPrototype;
		public Button religionButton, cabalButton, guildButton;
		public ColorReference buttonSelectedColor, buttonAvailableColor;
		private List<BoonSlot> slots;
		private List<HeaderSlot> headerSlots;
		private BoonManager[] boonManagers;
		private BoonManager boonManager;
		private int tierCount, indexCount;

		void Awake()
		{
			boonManagers = boonManagerContainer.GetComponentsInChildren<BoonManager>();
			toggler.onShow += UpdateUI;
			toggler.onShow += SetDefaultSelected;
		}

		void Start()
		{
			SetDimensions();
			BuildSlots();
			BuildHeaders();
			SetNavigationButtons();
			UpdateUI();
		}

		private void SetNavigationButtons()
		{
			religionButton.onClick.AddListener(SetReligion);
			cabalButton.onClick.AddListener(SetCabal);
			guildButton.onClick.AddListener(SetGuild);
		}

		private void SetReligion()
		{
			SetBoonManager(boonManagers[0]);
		}

		private void SetCabal()
		{
			SetBoonManager(boonManagers[1]);
		}

		private void SetGuild()
		{
			SetBoonManager(boonManagers[2]);
		}

		private void SetDimensions()
		{
			indexCount = -1;
			tierCount = -1;

			foreach(BoonManager current in boonManagers)
			{
				int currentIndexCap = current.GetIndexCap();
				int currentTierCap = current.GetTierCap();

				if(currentIndexCap > indexCount) indexCount = currentIndexCap;
				if(currentTierCap >  tierCount) tierCount = currentTierCap;
			}
		}

		private void BuildSlots()
		{
			GridBuilder<BoonSlot> gridBuilder = new GridBuilder<BoonSlot>();

			slots = gridBuilder.Begin()
				.WithContainer(slotContainer)
				.WithPrototype(slotPrototype)
				.WithColumns(tierCount)
				.WithRows(indexCount)
				.WithSpacing(4, 4)
				.Build();

			RegisterSlots();
		}

		private void RegisterSlots()
		{
			int currentIndex = 0;

			for(int i = 0; i < indexCount; i++)
			{
				for(int j = 0; j < tierCount; j++)
				{
					if(currentIndex < 0 || currentIndex >= slots.Count) break;

					slots[currentIndex].SetTier(j);
					slots[currentIndex].SetIndex(i);
					slots[currentIndex].onClick += HandleClick;
					slots[currentIndex].onPointerEnter += HandleEnter;
					slots[currentIndex].onPointerExit += HandleExit;

					currentIndex++;
				}
			}
		}	
		
	public void BuildHeaders()
	{
		GridBuilder<HeaderSlot> gridBuilder = new GridBuilder<HeaderSlot>();

		headerSlots = gridBuilder.Begin()
			.WithPrototype(headerPrototype)
			.WithContainer(headerContainer)
			.WithColumns(5)
			.WithRows(1)
			.WithSpacing(4, 0)
			.Build();
			
		RegisterHeaders();

	}

	public void RegisterHeaders()
	{
		if(headerSlots[0] != null) headerSlots[0].SetText("I");
		if(headerSlots[1] != null) headerSlots[1].SetText("II");
		if(headerSlots[2] != null) headerSlots[2].SetText("III");
		if(headerSlots[3] != null) headerSlots[3].SetText("IV");
		if(headerSlots[4] != null) headerSlots[4].SetText("V");
	}

		public void SetBoonManager(BoonManager newBoonManager)
		{
			if(newBoonManager == null) return;

			ClearBoonManager();

			boonManager = newBoonManager;
			boonManager.onChanged += UpdateUI;

			UpdateSprites();

			UpdateUI();
		}

		private void UpdateSprites()
		{
			List<Sprite> sprites = boonManager.GetSprites();

			for(int i = 0; i < slots.Count; i++)
			{
				if(i < sprites.Count) slots[i].SetIcon(sprites[i]);
				else slots[i].ClearIcon();
			}
		}

		private void ClearBoonManager()
		{
			if(boonManager != null)
			{
				boonManager.onChanged -= UpdateUI;
				boonManager = null;
			}
		}

		private void UpdateUI()
		{
			if(!toggler.IsVisible()) return;

			UpdateNavigationButtons();
			UpdateLabels();
			UpdateSlots();
		}

		private void SetDefaultSelected()
		{
			if(IsAvailable(boonManagers[0])) SetReligion();
			else if(IsAvailable(boonManagers[1])) SetCabal();
			else if(IsAvailable(boonManagers[2])) SetGuild();
		}

		private void UpdateNavigationButtons()
		{
			SetButtonState(religionButton, IsAvailable(boonManagers[0]), IsSelected(0));
			SetButtonState(cabalButton, IsAvailable(boonManagers[1]), IsSelected(1));
			SetButtonState(guildButton, IsAvailable(boonManagers[2]), IsSelected(2));
		}

		private bool IsAvailable(BoonManager boonManager)
		{
			return boonManager.GetMaxLevel() > 0;
		}

		private bool IsSelected(int index)
		{
			if(index < 0 || index > boonManagers.Length) return false;

			return boonManagers[index] == boonManager;
		}

		private void SetButtonState(Button button, bool toShow, bool isSelected)
		{
			if(button.gameObject.activeSelf != toShow) button.gameObject.SetActive(toShow);
			if(button.gameObject.activeSelf) button.image.color = (isSelected) ? buttonSelectedColor : buttonAvailableColor;
		}

		private void UpdateSlots()
		{
			if(boonManager == null) return;

			int[] available = boonManager.GetAvailable();
			int[] selected = boonManager.GetSelected();

			for(int i = 0; i < slots.Count; i++)
			{
				slots[i].UpdateUI(available, selected);
			}
		}

		private void UpdateLabels()
		{
			if(boonManager == null) return;

			for(int i = 0; i < headerSlots.Count; i++)
			{
				if(i >= boonManager.GetAvailableCount())
					headerSlots[i].Hide();
				else if(boonManager.HasSelected(i))
					headerSlots[i].Selected();
				else
					headerSlots[i].Available();			
			}
		}
		

		private void Clear()
		{
			for(int i = 0; i < slots.Count; i++)
			{
				slots[i].Clear();
			}
		}
		
		private void HandleClick(int tier, int index)
		{
			if(boonManager == null) return;

			boonManager.TrySelect(tier, index);
		}

		private void HandleEnter(RectTransform rect, int tier, int index)
		{
			if(rect == null) return;
			if(boonManager == null) return;
			Boon temp = boonManager.GetBoon(tier, index);
			Boon selected = boonManager.GetSelectedBoon(tier);
			if(temp == null) return;

			if(selected == null || selected == temp) 
				boonTooltip.Set(temp, rect);
			else
				boonCompareTooltip.Set(selected, rect, temp);
		}

		private void HandleExit(int tier, int index)
		{
			if(boonManager == null) return;

			boonTooltip.Hide();
			boonCompareTooltip.Hide();

		}
	}
}