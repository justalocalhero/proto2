﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Boons
{
	public class BoonSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
	{
		private RectTransform rect;
		public UIToggler toggler;
		private int tier, index;
		public Button button;
		public Image icon;
		public ColorReference availableColor, selectedColor;

		public delegate void OnClick(int tier, int index);
		public OnClick onClick;

		public delegate void OnPointerEnter(RectTransform rect, int tier, int index);
		public OnPointerEnter onPointerEnter;

		public delegate void OnPointerExit(int tier, int index);
		public OnPointerExit onPointerExit;

		void Awake()
		{
			rect = transform as RectTransform;
			button.onClick.AddListener(HandleClick);
		}

		private void HandleClick()
		{
			RaiseOnClick();
			RaiseOnPointerEnter();
		}

		private void RaiseOnClick()
		{
			if(onClick != null) onClick(tier, index);
		}

		private void RaiseOnPointerEnter()
		{
			if(onPointerEnter != null) onPointerEnter(rect, tier, index);
		}

		private void RaiseOnPointerExit()
		{
			if(onPointerExit != null) onPointerExit(tier, index);
		}

		public void SetIcon(Sprite sprite)
		{
			icon.sprite = sprite;
		}

		public void ClearIcon()
		{
			icon.sprite = null;
		}

		public void SetTier(int tier)
		{
			this.tier = tier;
		}

		public void SetIndex(int index)
		{
			this.index = index;
		}

		public void UpdateUI(int[] available,  int[] selected)
		{
			bool availableValid = index >= 0 && index < available.Length;
			bool selectedValid = tier >= 0 && tier < selected.Length;

			if(!availableValid) Clear();
			else if(selectedValid && index == selected[tier]) SetSelected();
			else if(availableValid && tier < available[index]) SetAvailable();
			else SetHidden();
		}

		public void Clear()
		{
			Hide();
		}

		private void SetAvailable()
		{
			SetColor(availableColor);
			Show();
		}

		private void SetSelected()
		{
			SetColor(selectedColor);
			Show();
		}

		private void SetHidden()
		{
			Hide();
		}

		private void Show()
		{
			toggler.Show();
		}

		private void Hide()
		{
			toggler.Hide();
		}

		private void SetColor(Color color)
		{
			button.image.color = color;
		}

        void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
        {
            RaiseOnPointerEnter();
        }

        void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
        {
            RaiseOnPointerExit();
        }
    }
}