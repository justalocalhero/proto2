﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Reputation/Boon")]
public class BoonBuilder : ScriptableObject 
{
    new public string name;
    public Sprite icon;
    public EquipStat[] stats;
    public AbilityBuilder ability;

    public string GetName()
    {
        return name;
    }

    public Boon Generate(int tier)
	{
		Ability generatedAbility = (ability == null) ? null : ability.Generate();
		
		return new Boon(name, tier, stats, generatedAbility);
	}
}
