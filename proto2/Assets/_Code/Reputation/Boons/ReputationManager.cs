﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Boons
{
	public class ReputationManager : MonoBehaviour 
	{
		public SaveManager saveManager;

		public BoonManager religionManager, cabalManager, guildManager;

		public ReputationBuildingManager religionBuilding, cabalBuilding;

		public delegate void OnTierUp();
		public OnTierUp onTierUp;

		public delegate void OnChanged();
		public OnChanged onChanged;

		public void RaiseOnTierUp()
		{
			if(onTierUp != null) onTierUp();
		}

		public void RaiseOnChanged()
		{
			if(onChanged != null) onChanged();
		}

		void Awake()
		{
			if(saveManager != null)
			{
				saveManager.onPreSave += HandlePreSave;
				saveManager.onLoad += HandleLoad;
				saveManager.onPostLoad += HandlePostLoad;
			}

			RegisterManager(religionManager);
			RegisterManager(cabalManager);
			RegisterManager(guildManager);
		}

		private void RegisterManager(BoonManager boonManager)
		{
			boonManager.onChanged += RaiseOnChanged;
			boonManager.onTierUp += RaiseOnTierUp;
		}

		private void HandlePreSave(SaveFile saveFile)
		{
			if(saveFile == null) return;

			if(saveFile.religionSave != null) saveFile.religionSave = religionManager.Save();
			if(saveFile.cabalSave != null) saveFile.cabalSave = cabalManager.Save();
			if(saveFile.guildSave != null) saveFile.guildSave = guildManager.Save();

			saveFile.religionSelected = religionBuilding.SaveSelected();
			saveFile.cabalSelected = cabalBuilding.SaveSelected();

		}

		private void HandleLoad(SaveFile saveFile)
		{
			if(saveFile == null) return;

			if(saveFile.religionSave != null) religionManager.Load(saveFile.religionSave);
			else religionManager.ClearSelected(false);

			if(saveFile.cabalSave != null) cabalManager.Load(saveFile.cabalSave);
			else cabalManager.ClearSelected(false);

			if(saveFile.guildSave != null) guildManager.Load(saveFile.guildSave);
			else guildManager.ClearSelected(false);

			religionBuilding.LoadSelected(saveFile.religionSelected);
			cabalBuilding.LoadSelected(saveFile.cabalSelected);

		}

		private void HandlePostLoad()
		{
			religionManager.PostLoad();
			cabalManager.PostLoad();
			guildManager.PostLoad();

			religionBuilding.PostLoadSelected();
			cabalBuilding.PostLoadSelected();
		}

		public int GetSelectedCount()
		{
			int toReturn = 0;

			toReturn += religionManager.GetSelectedCount();
			toReturn += cabalManager.GetSelectedCount();
			toReturn += guildManager.GetSelectedCount();

			return toReturn;
		}
		
		public int GetAvailableCount()
		{
			int toReturn = 0;

			toReturn += religionManager.GetAvailableCount();
			toReturn += cabalManager.GetAvailableCount();
			toReturn += guildManager.GetAvailableCount();

			return toReturn;
		}

	}
}