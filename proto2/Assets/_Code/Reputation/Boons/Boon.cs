﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boon 
{
    private string name;
    private int tier;
    public EquipStat[] stats;
    public Ability ability;
    public BoonState boonState;
    public delegate void OnBoonStateChanged();
    public OnBoonStateChanged onBoonStateChanged;

    public Boon(string name, int tier, EquipStat[] stats, Ability ability)
    {
        this.name = name;
        this.tier = tier;
        this.stats = stats;
        this.ability = ability;
        boonState = BoonState.unavailable;
    }    

    public void SetTier(int tier)
    {
        this.tier = tier;
    }

    public int GetTier()
    {
        return tier;
    }

    public string GetName()
    {
        return name;
    }

    public BoonState GetBoonState()
    {
        return boonState;
    }

    public void SetBoonState(BoonState newState)
    {
        boonState = newState;
        RaiseOnBoonStateChanged();
    }

    public void RaiseOnBoonStateChanged()
    {
        if(onBoonStateChanged != null) onBoonStateChanged();
    }

    public virtual void OnRegister(Unit unit)
    {
        if(stats != null && stats.Length > 0) unit.AdjustStats(stats);
        if(ability != null) unit.AddAction(ability);

    }

    public virtual void OnDeregister(Unit unit)
    {
        if(stats != null && stats.Length > 0) unit.AdjustStats(stats, -1);
        if(ability != null) unit.RemoveAction(ability);
    }

    public Boon Clone()
    {
        return new Boon(name, tier, stats, (ability == null) ? null : (Ability) ability.Clone());
    }

    
}

public enum BoonState {selected, prepared, available, unavailable,}