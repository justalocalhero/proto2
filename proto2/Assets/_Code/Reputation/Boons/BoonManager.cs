﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Boons
{
	public class BoonManager : MonoBehaviour 
	{
		public Transform reputationBuildingContainer;
		private ReputationBuilding[] reputationBuildings;
		private int[] available;
		private int[] selected;
		private int maxLevel = -1;

		public delegate void OnChanged();
		public OnChanged onChanged;

		public delegate void OnTierUp();
		public OnTierUp onTierUp;

		public void RaiseOnChanged()
		{
			if(onChanged != null) onChanged();
		}

		public void RaiseOnTierUp()
		{
			if(onTierUp != null) onTierUp();
		}

		void Awake()
		{
			reputationBuildings = reputationBuildingContainer.GetComponentsInChildren<ReputationBuilding>();
			available = new int[GetIndexCap()];
			selected = new int[GetTierCap()];

			foreach(ReputationBuilding current in reputationBuildings)
			{
				current.onLevelUp += HandleLevelUp;
			}

			SetAvailable();
			SetSelected();
		}

		public BoonSave Save()
		{
			BoonSave boonSave = new BoonSave();
			boonSave.selected = selected;

			return boonSave;
		}

		public void Load(BoonSave boonSave)
		{
			if(boonSave == null) return;
			if(boonSave.selected == null) return;
			selected = boonSave.selected;
		}

		public void PostLoad()
		{
			RaiseOnChanged();
		}

		public void ClearSelected()
		{
			ClearSelected(false);
		}

		public void ClearSelected(bool toRaise)
		{
			SetSelected();

			if(toRaise) RaiseOnChanged();
		}

		private void HandleLevelUp(int currentLevel)
		{
			SetAvailable();

			RaiseOnTierUp();
			RaiseOnChanged();
		}

		public int GetIndexCap()
		{
			return reputationBuildings.Length;
		}

		public int GetTierCap()
		{
			int toReturn = 0;

			foreach(ReputationBuilding building in reputationBuildings)
			{
				int temp = building.GetMaxLevel();

				if(temp > toReturn) toReturn = temp;
			}

			return toReturn;
		}

		public int GetMaxLevel()
		{
			return maxLevel;
		}

		public void SetAvailableTier(int tier)
		{
			maxLevel = tier;

			RaiseOnTierUp();
		}

		private void SetAvailable()
		{
			for(int i = 0; i < available.Length; i++)
			{
				int current = reputationBuildings[i].GetLevel();
				available[i] = current;
				if(current > maxLevel) maxLevel = current;
			}
		}

		private void SetSelected()
		{
			for(int i = 0; i < selected.Length; i++)
			{
				selected[i] = -1;
			}
		}

		public int[] GetAvailable()
		{
			return available;
		}

		public int[] GetSelected()
		{
			return selected;
		}

		public bool HasSelected(int index)
		{
			if(index < 0 || index >= selected.Length) return false;

			return selected[index] >= 0;
		}

		public int GetSelectedCount()
		{
			int toReturn = 0;

			if(selected != null)
			{
				for(int i = 0; i < selected.Length; i++)
				{
					if(selected[i] >= 0) toReturn++;
				}
			}

			return toReturn;
		}

		public int GetAvailableCount()
		{
			int toReturn = 0;

			if(available != null)
			{
				for(int i = 0; i < available.Length; i++)
				{
					if(available[i] > toReturn) toReturn = available[i];
				}
			}

			return toReturn;
		}

		public void TrySelect(int tier, int index)
		{
			bool availableValid = index >= 0 && index < available.Length;
			bool selectedValid = tier >= 0 && tier < selected.Length;

			if(!availableValid) return;
			if(!selectedValid) return;
			if(tier >= available[index]) return;			
			if(selected[tier] == index) selected[tier] = -1;
			else selected[tier] = index;

			RaiseOnChanged();
		}

		public List<Boon> GetSelectedBoons()
		{
			List<Boon> toReturn = new List<Boon>();

			for(int i = 0; i < selected.Length; i++)
			{
				int index = selected[i];
				if(index < 0 || index >= reputationBuildings.Length) break;
				Boon temp = reputationBuildings[index].GetBoon(i);
				if(temp != null) toReturn.Add(temp.Clone());
			}

			return toReturn;
		}

		public Boon GetSelectedBoon(int tier)
		{
			if(tier < 0 || tier >= selected.Length) return null;
			return GetBoon(tier, selected[tier]);
			
		}

		public Boon GetBoon(int tier, int index)
		{
			if(index < 0 || index >= reputationBuildings.Length) return null;
			return reputationBuildings[index].GetBoon(tier);	
		}

		public List<Sprite> GetSprites()
		{
			List<Sprite> toReturn = new List<Sprite>();

			foreach(ReputationBuilding building in reputationBuildings)
			{
				toReturn.AddRange(building.GetSprites());
			}

			return toReturn;
		}
	}	
}