﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReputationBuildingManager : MonoBehaviour 
{
	public SaveManager saveManager;
	private int selectedIndex = -1;
	public ReputationExpandedUI expandedUI;

	public BattlegroundManager battlegroundManager;
	public IntVariable offeringsPerKill;

	public Transform buildingContainer;
	public ReputationBuilding[] reputationBuildings;

	public delegate void OnLevelUp();
	public OnLevelUp onLevelUp;

	public delegate void OnBuildingLevelUp();
	public OnBuildingLevelUp onBuildingLevelUp;

	public delegate void OnPreparedSelectedChanged();
	public OnPreparedSelectedChanged onPreparedSelectedChanged;

	public delegate void OnOfferingAdded();
	public OnOfferingAdded onOfferingAdded;

	private int level;

	public void Awake()
	{		
		reputationBuildings = buildingContainer.GetComponentsInChildren<ReputationBuilding>();
		foreach(ReputationBuilding building in reputationBuildings)
		{
			building.onLevelUp += RaiseOnBuildingLevelUp;
		}

		if(battlegroundManager != null)	battlegroundManager.onEnemyDeath += AddOffering;
		if(saveManager != null)
		{
			saveManager.onPreSave += Save;
			saveManager.onLoad += Load;
		}
	}

	public int SaveSelected()
	{
		return selectedIndex;
	}

	public void LoadSelected(int selected)
	{
		this.selectedIndex = selected;
	}

	public void PostLoadSelected()
	{
		Select(selectedIndex);
	}

	public void Save(SaveFile saveFile)
	{
		if(saveFile == null) return;

		foreach(ReputationBuilding reputation in reputationBuildings)
		{
			reputation.Save(saveFile);
		}
	}

	public void Load(SaveFile saveFile)
	{
		if(saveFile == null) return;

		foreach(ReputationBuilding reputation in reputationBuildings)
		{
			reputation.Load(saveFile);
		}
	}

	public ReputationBuilding GetSelected()
	{
		if(selectedIndex < 0 || selectedIndex >= reputationBuildings.Length) return null;
		return reputationBuildings[selectedIndex];
	}

	public void OnClick(int index)
	{
		if(index == selectedIndex) 
			ClearSelected();
		else Select(index);

	}

	public void ClearSelected()
	{
		if(selectedIndex == -1) return;
		selectedIndex = -1;
		RaiseOnPreparedSelectedChanged();
	}

	public void Select(int index)
	{
		if(index < 0 || index >= reputationBuildings.Length) return;

		selectedIndex = index;
		expandedUI.SetReputationBuildingManager(this);
		RaiseOnPreparedSelectedChanged();
	}
	
	public void RaiseOnLevelUp()
	{
		if(onLevelUp != null) onLevelUp();
	}
	
	public void RaiseOnBuildingLevelUp(int level)
	{
		if(onBuildingLevelUp != null) onBuildingLevelUp();
	}

	public void RaiseOnPreparedSelectedChanged()
	{
		if(onPreparedSelectedChanged != null) onPreparedSelectedChanged();
	}

	public void RaiseOnOfferingAdded()
	{
		if(onOfferingAdded != null) onOfferingAdded();
	}

	public ReputationBuilding[] GetBuildings()
	{
		return reputationBuildings;
	}

	public ReputationBuilding GetBuilding(int index)
	{
		if(index < 0 || index >= reputationBuildings.Length) return null;

		return reputationBuildings[index];
	}

	public void SetLevel(int newLevel)
	{
		level = newLevel;
		RaiseOnLevelUp();
	}

	public int GetLevel()
	{
		return level;
	}

	public int GetBuildingLevel()
	{
		int toReturn = -1;

		foreach(ReputationBuilding building in reputationBuildings)
		{
			int bLevel = building.GetLevel();
			if(bLevel > toReturn) toReturn = bLevel;
		}

		return toReturn;
	}

	public int GetAvailableCount()
	{
		int level = GetLevel();
		int maxIndex = Mathf.Clamp(reputationBuildings.Length, 0, level);
		bool available = false;
		for(int i = 0; i < maxIndex; i++)
		{
			if(reputationBuildings[i].IsMaxed()) 
			{
				available = true;
				break;
			}
		}

		return (available) ? 1 : 0;
	}

	public int GetSelectedCount()
	{
		return (GetSelected() != null && GetSelected().IsMaxed()) ? 1 : 0;
	}

	public void AddOffering()
	{
		AddOffering(offeringsPerKill.Value);
	}

	public void AddOffering(int offerings)
	{
		if(GetSelected() == null) return;
		if(offerings <= 0) return;
		GetSelected().AddOffering(offerings);
		RaiseOnOfferingAdded();
	}
}