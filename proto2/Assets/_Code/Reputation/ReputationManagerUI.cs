﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReputationManagerUI : MonoBehaviour 
{
	public ReputationBuildingManager manager;
    public Transform ReputationSlotContainer;
    public ReputationBuildingSlot[] slots;
    public UIToggler uiToggler;
    public ReputationExpandedUI expandedUI;

    public void Start()
    {
        slots = ReputationSlotContainer.GetComponentsInChildren<ReputationBuildingSlot>();
        SetManager(manager);
        PassReferences();
    }

    public void Show()
    {
        if(uiToggler == null) return;
        uiToggler.Show();
    }

    public void Hide()
    {
        if(uiToggler == null) return;
        uiToggler.Hide();
    }

    public void SetManager(ReputationBuildingManager newManager)
    {
        if(newManager == null) return;
        if(manager != null) 
        {
            manager.onLevelUp -= UpdateUI;
            manager.onPreparedSelectedChanged -= UpdateUI;
        }
        manager = newManager;
        manager.onLevelUp += UpdateUI;
        manager.onPreparedSelectedChanged += UpdateUI;
        PassReferences();
        UpdateUI();
    }

    public void PassReferences()
    {
        for(int i = 0; i < slots.Length; i++)
        {
            slots[i].SetManager(manager);
        }
    }

    public void UpdateUI()
    {
        UpdateButtonsExist();
        UpdateButtonsAvailable();
    }

    public void UpdateButtonsExist()
    {
        if(manager == null) return;
        ReputationBuilding[] buildings = manager.GetBuildings();

        for(int i = 0; i < slots.Length; i++)
        {
            if(i < buildings.Length)
            {
                slots[i].Set(buildings[i], i);
            }
        }
    }

    public void UpdateButtonsAvailable()
    {
        if(manager == null) return;        
        int level = manager.GetLevel();

        for(int i = 0; i < slots.Length; i++)
        {
            slots[i].UpdateUI(i < level);
        }
    }
}
