﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guild : ReputationBuilding
{
    public override string GetSaveKey()
    {
        return "Guild" + GetName();
    }
}
