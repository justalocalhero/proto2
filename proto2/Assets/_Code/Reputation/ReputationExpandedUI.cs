﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Tooltips;

public class ReputationExpandedUI : MonoBehaviour
{
	public UIToggler toggler;
	public RectTransform tooltipTarget;
	private ReputationBuildingManager currentManager;
	public TextMeshProUGUI mesh;
	public ReputationBoonTooltip tooltip;
	public ColorReference darkColor;

	public void Awake()
	{
		toggler.onShow += UpdateUI;
	}

	public void SetReputationBuildingManager(ReputationBuildingManager newManager)
	{
		if(newManager == null) return;
		if(currentManager != null) 
		{
			currentManager.onOfferingAdded -= UpdateText;
			currentManager.onPreparedSelectedChanged -= UpdateUI;
			currentManager.onBuildingLevelUp -= UpdateUI;
		}
		currentManager = newManager;
		currentManager.onOfferingAdded += UpdateText;
		currentManager.onPreparedSelectedChanged += UpdateUI;
		currentManager.onBuildingLevelUp += UpdateUI;
		UpdateUI();
	}

	public void UpdateUI()
	{
		if(!toggler.IsVisible()) return;
		UpdateText();
		UpdateTooltip();
	}

	public void UpdateText()
	{
		if(currentManager != null)
		{
			ReputationBuilding selected = currentManager.GetSelected();
			bool hasSelected = selected != null;

			Boon nextBoon = (selected == null) ? null : selected.GetNextBoon();
			bool hasNextBoon = nextBoon != null;

			if(selected != null) 
			{
				string text = Tooltips.SizedText.GetText(selected.GetDescription(), 2);
				text += Tooltips.ColoredText.GetText("\nLevel " + selected.GetLevel() + "\n\nOfferings: " + selected.GetOfferings(), darkColor);

				if(hasNextBoon) 
				{					
					text += Tooltips.ColoredText.GetText("\nNext Level: " + selected.GetOfferingsToLevel(), darkColor);
					text += "\n\nUnlocks: ";

				}
				else
				{
					text += Tooltips.ColoredText.GetText("\nMaximized", darkColor);
				}

				
				mesh.SetText(text);
			}
			else 
			{
				mesh.SetText("Select a Guild to support");
			}
		}
		else
		{	
			mesh.SetText("Select a Guild to support");
		}
	}

	public void UpdateTooltip()
	{
		bool hasManager = currentManager != null;
		
		ReputationBuilding selected = (hasManager) ? currentManager.GetSelected() : null;
		bool hasSelected = selected != null;

		Boon nextBoon = (hasSelected) ? selected.GetNextBoon() : null;
		bool hasNextBoon = nextBoon != null;

		if(hasNextBoon) 
		{
			tooltip.Set(nextBoon, tooltipTarget);				
		}
		else
		{
			tooltip.Hide();
		}
	}

}
