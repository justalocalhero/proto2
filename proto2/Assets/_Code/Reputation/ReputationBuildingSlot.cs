﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ReputationBuildingSlot : MonoBehaviour
{
	private ReputationBuildingManager manager;
	private ReputationBuilding reputationBuilding;
	private int index;
	public Button button;
	public TextMeshProUGUI mesh;
	public ColorReference availableColor;
	public ColorReference selectedColor;
	public ColorReference preparedColor;
	
	public void Set(ReputationBuilding newBuilding, int index)
	{
		if(newBuilding == null) return;
		this.index = index;
		reputationBuilding = newBuilding;
		mesh.SetText(newBuilding.GetName());
	}

	public void SetManager(ReputationBuildingManager newManager)
	{
		if(newManager == null) return;
		manager = newManager;
	}

	public void UpdateUI(bool enabled)
	{
		bool hasBuilding = (reputationBuilding != null);

		ReputationBuilding selected = manager.GetSelected();

		bool isSelected = selected != null && selected == reputationBuilding;

		button.gameObject.SetActive(hasBuilding);
		if(hasBuilding) button.gameObject.SetActive(enabled);

		if(isSelected) button.image.color = selectedColor;
		else button.image.color = availableColor;
	}

	public void OnButtonPressed()
	{
		Select();
	}

	public void Select()
	{
		manager.OnClick(index);
	}

	public void OnPointerEnter()
	{

	}

	public void OnPointerExit()
	{

	}
}
