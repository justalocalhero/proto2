﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampSupplyUI : SupplyListUI 
{
	public CampSupplyManager campSupplyManager;
	
	public CampSupplySlot campSupplySlotPrototype;
	public override SupplySlot supplySlotPrototype { get { return campSupplySlotPrototype; } }

	private List<CampSupplySlot> slots;

	public override void RegisterDelegates()
	{
		campSupplyManager.onChanged += UpdateUI;
	}	
	
	public override void UpdateUI()
	{
		UpdateSlots();
	}

	public override void ConvertSlots(List<MonoBehaviour> slotMono)
	{		
		slots = slotMono.ConvertAll<CampSupplySlot>(x => x as CampSupplySlot);
	}

	public void UpdateSlots()
	{
		int index = 0;

		foreach(CampSupply supply in campSupplyManager.supplies)
		{
			if(supply.IsAvailable() && index < slots.Count)
			{
				slots[index++].Set(supply);
			}
		}

		for(int i = index; i < slots.Count; i++)
		{
			slots[i].Clear();
		}
	}
}
