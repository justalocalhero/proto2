﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridBuilder<T> where T : MonoBehaviour
{

	float paddingX, paddingY;
	float spacingX, spacingY;
	int columns, rows, cap;
	T prototype;
	Transform container;	
	RectTransform containerRect;
	RectTransform prototypeRect;

	public GridBuilder<T> Begin()
	{
		return this;
	}

	public GridBuilder<T> WithPrototype(T prototype)
	{
		this.prototype = prototype;
		prototypeRect = prototype.transform as RectTransform;

		return this;
	}

	public GridBuilder<T> WithContainer(Transform container)
	{
		this.container = container;
		containerRect = container as RectTransform;

		return this;
	}

	public GridBuilder<T> WithPadding(float x, float y)
	{
		paddingX = x;
		paddingY = y;

		return this;
	}

	public GridBuilder<T> WithSpacing(float x, float y)
	{
		spacingX = x;
		spacingY = y;

		return this;
	}

	public GridBuilder<T> WithCap(int cap)
	{
		this.cap = cap;

		return this;
	}

	public GridBuilder<T> WithRows(int rows)
	{
		this.rows = rows;

		return this;
	}

	public GridBuilder<T> WithColumns(int columns)
	{
		this.columns = columns;

		return this;
	}

	public List<T> Build()
	{		
		List<T> toReturn = new List<T>();

		if(!Validate()) return toReturn;
		

		RectTransform containerRect = container as RectTransform;
        RectTransform slotRect = prototype.transform as RectTransform;

        float containerWidth = containerRect.rect.width;
        float containerHeight = containerRect.rect.height;

        float slotWidth = slotRect.rect.width + spacingX;
        float slotHeight = slotRect.rect.height + spacingY;

        float offsetX = .5f * (containerWidth - columns * slotWidth + spacingX) + slotWidth * slotRect.pivot.x - containerWidth * containerRect.pivot.x;
        float offsetY = .5f * (containerHeight - rows * slotHeight + spacingY) + slotHeight * (1 - slotRect.pivot.y) - containerHeight * containerRect.pivot.y;
		
		int count = 0;

        for(int i = 0; i < rows; i++)
        {
            for(int j = 0; j < columns; j++)
            {
				if(cap > 0 && count++ >= cap) break;

                float x = (j * slotWidth + offsetX);
                float y = -(i * slotHeight + offsetY);
                float z = 0;

                Vector3 tempPosition = new Vector3(x, y, z);
                    
                T temp = Object.Instantiate(prototype, container, false);
                (temp.transform as RectTransform).localPosition = tempPosition;

                toReturn.Add(temp);
            }
        }

		return toReturn;
	}

	private bool Validate()
	{
		if(prototype == null) return false;
		if(container == null) return false;

		if(rows <= 0) rows = CalculateRows();
		if(rows <= 0) return false;

		if(columns <= 0) columns = CalculateColumns();
		if(columns <= 0) return false;

		return true;
	}

	public int CalculateRows()
	{
        float containerHeight = containerRect.rect.height;
        float prototypeHeight = prototypeRect.rect.height + spacingY;
        int rows = (prototypeHeight > 0) ? Mathf.FloorToInt((containerHeight - 2 * paddingY) / (prototypeHeight)) : -1;

		return rows;
	}

	public int CalculateColumns()
	{
        float containerWidth = containerRect.rect.width;
        float prototypeWidth = prototypeRect.rect.width + spacingX;
        int columns = (prototypeWidth > 0) ? Mathf.FloorToInt((containerWidth - 2 * paddingX) / (prototypeWidth)) : -1;

		return columns;
	}

	public int GetRows()
	{
		return rows;
	}

	public int GetColumns()
	{
		return columns;
	}

	

}
