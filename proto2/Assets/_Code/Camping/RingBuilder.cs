﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingBuilder<T> where T : MonoBehaviour
{
	private T prototype;
	private Transform container;
	private RectTransform containerRect;
	private int count = 0;
	private float radius = 0;

	public RingBuilder<T> WithPrototype(T prototype)
	{
		this.prototype = prototype;

		return this;
	}

	public RingBuilder<T> WithContainer(Transform container)
	{
		this.container = container;
		containerRect = container as RectTransform;

		return this;
	}

	public RingBuilder<T> WithRadius(float radius)
	{
		this.radius = radius;

		return this;
	}

	public RingBuilder<T> WithCount(int count)
	{
		this.count = count;

		return this;
	}

	public List<T> Build()
	{
        RectTransform slotRect = prototype.transform as RectTransform;
		List<T> toReturn = new List<T>();
		float twoPi = Mathf.PI * 2;

		float width = slotRect.rect.width;
		float height = slotRect.rect.height;
		float pivotX = slotRect.pivot.x;
		float pivotY = slotRect.pivot.y;

		float offsetX = width * pivotX - width / 2;
		float offsetY = height * pivotY - height / 2;

		for(int i =  0; i < count; i++)
		{
			float angleRatio = (0.0f + i) / (0.0f + count);
			float angle = twoPi * angleRatio;

			float x = (radius + width / 2) * Mathf.Sin(angle) + offsetX;
			float y = (radius + height / 2) * Mathf.Cos(angle) + offsetY;


			T temp = Object.Instantiate(prototype, container, false);
			temp.transform.localPosition = new Vector2(x, y);

			toReturn.Add(temp);
		}

		return toReturn;
	}
}
