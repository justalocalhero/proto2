﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CampUI : SupplyListUI 
{
	public CampSupplyManager campSupplyManager;

	public TextMeshProUGUI headerText, supplyText;
	
	public ColoredText coloredText;
	public ColorReference healingColor, buffColor, supplyDefaultColor, supplyFilledColor;

	public CampSupplySlot campSupplySlotPrototype;
	public override SupplySlot supplySlotPrototype {get { return campSupplySlotPrototype; } }
	private List<CampSupplySlot> slots;

	public override void RegisterDelegates()
	{
		campSupplyManager.onChanged += UpdateUI;
	}

	public override void ConvertSlots(List<MonoBehaviour> slotMono)
	{
		slots = slotMono.ConvertAll<CampSupplySlot>(x => x as CampSupplySlot);
	}

	public override void UpdateUI()
	{
		UpdateHeaderText();
		UpdateSlots();
		UpdateSupplyText();
	}

	public void UpdateSlots()
	{
		int index = 0;

		foreach(CampSupply supply in campSupplyManager.supplies)
		{
			if(supply.IsSelected() && index < slots.Count)
			{
				slots[index++].Set(supply);
			}
		}

		for(int i = index; i < slots.Count; i++)
		{
			slots[i].Clear();
		}
	}

	public void UpdateHeaderText()
	{
		string text = "Camping:\n";

		text += GetHealingText();
		text += GetBuffText();

		headerText.SetText(text);
	}

	public void UpdateSupplyText()
	{
		int selected = campSupplyManager.SelectedCount();
		int max = campSupplyManager.maxSupplies.Value;
		string delimiter = " / ";
		Color color = (selected == max) ? supplyFilledColor : supplyDefaultColor;
		
		string text = coloredText.GetText(selected + delimiter + max, color);

		supplyText.SetText(text);
	}

	public string GetHealingText()
	{
		return coloredText.GetText("Player heals for " + Utility.ToPercent(campSupplyManager.campHealing.Value) + " of Hitpoints\n", healingColor);
	}

	public string GetBuffText()
	{
		return coloredText.GetText("Buff lasts for " + Utility.Truncate(campSupplyManager.buffDuration.Value, 2) +"s", buffColor);
	}
}
