﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampSupplyManager : MonoBehaviour, IUnlocker 
{
	public SaveManager saveManager;
	public Camp camp;
	public Bullpen bullpen;
	public SpriteManager spriteManager;
	public Sprite campingSprite;
	public int spriteIndex;
	public IntVariable maxSupplies;
	public FloatVariable campHealing, buffDuration;
	public List<CampSupplyBuilder> builders;
	public List<CampSupply> supplies {get; set;}

	public delegate void OnChanged();
	public OnChanged onChanged;


	public void Awake()
	{
		saveManager.onPreSave += Save;
		saveManager.onLoad += Load;
		BuildSupplies();
		ValidateSprite();
	}

	public void Save(SaveFile saveFile)
	{
		if(saveFile == null) return;
		if(supplies == null) return;

		bool[] suppliesSelected = new bool[supplies.Count];

		for(int i = 0; i < supplies.Count; i++)
		{
			suppliesSelected[i] = supplies[i].IsSelected();
		}

		saveFile.campSupplySave = suppliesSelected;
	}

	public void Load(SaveFile saveFile)
	{
		if(saveFile == null) return;
		if(saveFile.campSupplySave == null) return;

		for(int i = 0; i < supplies.Count; i++)
		{
			if(i < saveFile.campSupplySave.Length && saveFile.campSupplySave[i]) supplies[i].Select();
			else supplies[i].Deselect();
		}
	}

	public void SetSpriteIndex()
	{
		if(campingSprite == null) return;
		if(spriteManager == null) return;
		spriteIndex = spriteManager.GetIndex(campingSprite);
		if(spriteIndex == -1)
			spriteIndex = spriteManager.AddSprite(campingSprite);
		if(spriteIndex == -1)
			Debug.LogWarning("Panic");
	}

	public void OnValidate()
	{
		ValidateSprite();
	}

	public void ValidateSprite()
	{
		if(campingSprite == null) return;
		if(spriteManager == null) return;

		if(campingSprite == spriteManager.GetSprite(spriteIndex)) 
			return;
		else 
			SetSpriteIndex();
	}

	public void Start()
	{		
		camp.onUnlock += RaiseOnChanged;
		camp.onUpgrade += RaiseOnChanged;
		bullpen.onPlayerSent += AddBuff;
	}
	
	public void BuildSupplies()
	{
		supplies = new List<CampSupply>();

		foreach(CampSupplyBuilder builder in builders)
		{
			CampSupply temp = builder.Generate();

			temp.parent = this;
			temp.onChanged += RaiseOnChanged;

			supplies.Add(temp);
		}
	}

	public Buff GenerateBuff()
	{
		List<EquipStat> stats =  new List<EquipStat>();

		foreach(CampSupply supply in supplies)
		{
			if(supply.IsSelected()) stats.AddRange(supply.stats);
		}

		Buff buff = new Buff("Camping", buffDuration.Value, 0, 1, stats.ToArray(), new Damage[0], new Damage[0], spriteIndex);
		return buff;
	}

	public void AddBuff(Player player)
	{
		if(player == null) return;

		Buff buff = GenerateBuff();

		if(buff != null) player.SetCampBuff(buff);
	}

	public void SetLevel(int level)
	{
		bool changed = false;

		for(int i = 0; i < level; i++)
		{
			if(supplies != null && supplies.Count > i && supplies[i].IsLocked())
			{
				supplies[i].Unlock();
				changed = true;
			}
		}

		if(changed) RaiseOnChanged();
	}

	public bool CanSelect()
	{
		if(maxSupplies == null) 
		{
			Debug.LogWarning("Camp Supply Count not set.");
			return false;
		}

		return maxSupplies.Value > SelectedCount();
	}

	public int SelectedCount()
	{
		int count = 0;

		foreach(CampSupply supply in supplies)
		{
			if(supply.IsSelected()) count++;
		}

		return count;
	}

	public string UnlockDescription(int level)
	{
		if(supplies == null || level < 0 || level >= supplies.Count) return "";

		return supplies[level].name;
	}

	public void RaiseOnChanged(CampSupply supply)
	{
		RaiseOnChanged();
	}

	public void RaiseOnChanged()
	{
		if(onChanged != null) onChanged();
	}
}
