﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampSupply: Supply
{
	public List<EquipStat> stats {get; set;}

	public delegate void OnChanged(CampSupply supply);
	public OnChanged onChanged;

	public override void RaiseOnChanged()
	{
		if(onChanged != null) onChanged(this);
	}
}
