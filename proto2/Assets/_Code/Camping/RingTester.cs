﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingTester : MonoBehaviour 
{
	public Transform container;
	public MonoBehaviour prototype;
	public IntReference radius;
	public IntReference count;

	RingBuilder<MonoBehaviour> builder = new RingBuilder<MonoBehaviour>();
	
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.T))
		{
			builder.WithContainer(container)
			.WithPrototype(prototype)
			.WithRadius(radius)
			.WithCount(count)
			.Build();
		}
	}
}
