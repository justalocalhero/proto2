﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CampSupplySlot : SupplySlot
{
	private CampSupply campSupply;
	public override Supply supply { get { return campSupply; } }

	public ColorReference statColor;
	public FloatReference indentPercentage;
	public StatText statText;

	public void Set(CampSupply campSupply)
	{
		if(campSupply == null)
		{
			Clear();
			return;
		}
		
		this.campSupply = campSupply;

		UpdateText();
		Show();
	}

	public override void UpdateText()
	{
		string nameText = campSupply.name;
		string statText = "<pos=" + indentPercentage.Value +"%>";

		foreach(EquipStat stat in campSupply.stats)
		{
			statText += " " + this.statText.GetText(stat, statColor);
		}

		string totalText = nameText + statText;

		mesh.SetText(totalText);	
	}
}
