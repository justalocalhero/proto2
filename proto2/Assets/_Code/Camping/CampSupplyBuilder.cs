﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Camping/CampSupplyBuilder")]
public class CampSupplyBuilder : ScriptableObject
{
	new public string name;
	public List<EquipStat> stats;

	public CampSupply Generate()
	{
		CampSupply toReturn = new CampSupply();

		toReturn.name = name;
		toReturn.stats = stats;

		return toReturn;
	}
}
