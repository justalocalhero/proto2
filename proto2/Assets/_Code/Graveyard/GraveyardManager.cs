﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraveyardManager : MonoBehaviour 
{
	private List<Snapshot> snapshots;
	public FloatReference maxSnapshots;
	public IntVariable deathCount;

	public delegate void OnGraveyardChanged();
	public OnGraveyardChanged onGraveyardChanged;


	public void Start () {
		snapshots = new List<Snapshot>();
	}
	
	public void Initialize()
	{
		deathCount.Value = 0;
	}

	public void Add(Snapshot newSnapshot)
	{
		if(newSnapshot == null) return;
		if(snapshots.Contains(newSnapshot)) return;
		snapshots.Add(newSnapshot);
		while(snapshots.Count > maxSnapshots.Value) snapshots.RemoveAt(0);
		deathCount.Value++;
		RaiseOnGraveyardChanged();
	}

	public Snapshot GetSnapshotAt(int index)
	{
		if(index < 0 || index >= snapshots.Count) return null;
		if(snapshots[index] == null) return null;
		return snapshots[index];

	}

	public int GetSnapshotCount()
	{
		if(snapshots == null)
		{
			Debug.LogWarning("Snapshots not initialized in graveyard");
			return -1;
		}
		return snapshots.Count;

	}

	public void RaiseOnGraveyardChanged()
	{
		if(onGraveyardChanged != null)	onGraveyardChanged();
	}

	public int GetDeathCount()
	{
		return deathCount.Value;
	}
}
