﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraveyardUI : MonoBehaviour 
{
	public GraveyardManager graveyard;
	public GraveyardWatcher graveyardWatcher;
	public int currentIndex;
	public Button leftButton;
	public Button rightButton;

	public void Start() 
	{
		graveyard.onGraveyardChanged += UpdateUI;
	}

	public void OnLeftButtonPressed()
	{
		currentIndex++;
		UpdateUI();
	}

	public void OnRightButtonPressed()
	{
		currentIndex--;
		UpdateUI();
	}

	public void UpdateButtons()
	{
		if(leftButton == null)
		{
			Debug.LogWarning("Left Graveyard Button Not Assigned");
			return;
		}
		if(rightButton == null)
		{
			Debug.LogWarning("Right graveyard button not assigned.");
			return;
		}
		rightButton.gameObject.SetActive((currentIndex <= 0) ? false : true);
		leftButton.gameObject.SetActive((currentIndex >= graveyard.GetSnapshotCount() - 1) ? false : true);
		

	}

	public void UpdateUI()
	{
		UpdateButtons();
		graveyardWatcher.SetSnapshot(graveyard.GetSnapshotAt(currentIndex));
	}

	public void Reset()
	{
		currentIndex = graveyard.GetSnapshotCount() - 1;
		UpdateUI();
	}
}
