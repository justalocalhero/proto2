﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggeredProc 
{

}

public enum ProcTarget {self, target,}
public enum ProcTrigger 
{
    onHit, onCrit, onNonCrit, onBuff, onDebuff, onMiss, onDodge, onBlock, onHeal,
    onPhysical, onPoison, onBleed, onFire, onCold, onArcane, onDark,
    onSpell, onSupport, onPierce, onBlunt, onTech, onCurse,
};