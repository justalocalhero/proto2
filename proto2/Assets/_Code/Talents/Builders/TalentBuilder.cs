﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName="Talents/Talent")]
public class TalentBuilder : ScriptableObject 
{
	new public string name;
	public Sprite leftSprite;
	public Sprite rightSprite;
	public Unit parent;
	public TalentAbilityBuilder timedAbility;
	public TalentTriggeredAbilityBuilder triggeredAbility;
	public TalentOptionBuilder[] options;
	public List<UnitResourceAugment> resourceAugments;
	public EquipStat[] stats;
	
	public Talent Generate(int tier)
	{
		TalentOption[] newOptions = new TalentOption[options.Length];
		for(int i = 0; i < options.Length; i++)
		{
			newOptions[i] = options[i].Generate(tier);
		}

		Talent toReturn = new Talent(name, leftSprite, rightSprite, tier, 
			timedAbility == null ? null : timedAbility.Generate(), 
			triggeredAbility == null ? null : triggeredAbility.Generate(), newOptions, resourceAugments, stats);

		return toReturn;
	}
}


[Serializable]
public struct UnitResourceAugment { public UnitResourceType type; public int cap; public EquipStat[] stats; }