﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Talents/TalentOption")]
public class TalentOptionBuilder : ScriptableObject 
{
	new public string name;
	public Sprite sprite;
	public AbilityAugmentBuilder augmentBuilder;	
	public List<UnitResourceAugment> resourceAugments;
	public EquipStat[] stats;

	public TalentOption Generate(int tier)
	{
		return new TalentOption(name, sprite, (augmentBuilder == null) ? null : augmentBuilder.Generate(), tier, resourceAugments, stats);
	}
}
