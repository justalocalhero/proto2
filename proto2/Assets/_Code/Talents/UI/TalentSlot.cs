﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tooltips;

public class TalentSlot : MonoBehaviour 
{
	public RectTransform buttonRect {get; private set;}
	private TalentTreeUI parent;
	private Talent talent;
	public Button button;
	public Image leftImage;
	public Image rightImage;
	public Image buttonImage{get; set;}
	private TalentOptionSlot[] optionSlots;
	public TalentAnimationManager animationManager;
	private TalentTooltip tooltip;

	public void Start()
	{
		buttonRect = button.transform as RectTransform;
		buttonImage = button.GetComponent<Image>();
		optionSlots = GetComponentsInChildren<TalentOptionSlot>();

		PassReferences();
	}

	public void PassReferences()
	{
		PassTooltip();
		PassParent();
	}

	public void PassParent()
	{
		if(optionSlots == null) return;
		for(int i = 0; i < optionSlots.Length; i++)
		{
			optionSlots[i].SetParent(this);
		}
	}

	public void PassTooltip()
	{
		if(optionSlots == null) return;
		for(int i = 0; i < optionSlots.Length; i++)
		{
			optionSlots[i].SetTooltip(tooltip);
		}
	}

	public void Set(Talent newTalent)
	{
		if(newTalent == null) return;
		talent = newTalent;

		for(int i = 0; i < optionSlots.Length; i++)
		{
			if(i < talent.options.Length) optionSlots[i].Set(talent.options[i]);
			else optionSlots[i].Clear();

		}
		ResetAnimations();
	}

	public void SetParent(TalentTreeUI newParent)
	{
		parent = newParent;
		PassParent();
	}

	public void SetTooltip(TalentTooltip tooltip)
	{
		this.tooltip = tooltip;
		PassTooltip();
	}

	public void Clear()
	{
		talent = null;
		for(int i = 0; i < optionSlots.Length; i++)
		{
			optionSlots[i].Clear();
		}
		ResetAnimations();
	}

	public void ResetAnimations()
	{
		animationManager.ResetState();
	}

	public void Preview()
	{
		if(talent == null) return;
			
		animationManager.MoveIn();
		UpdateIcons();	

		for(int i = 0; i < optionSlots.Length; i++)
		{
			optionSlots[i].Preview();
		}
		
	
	}

	public void Show()
	{
		UpdateIcons();
		UpdateMotion();
		ShowOptions();
	}

	public void Hide()
	{
		animationManager.MoveOut();
	}

	public void UpdateIcons()
	{
		if(talent == null) return;
		UpdateIcon(leftImage, talent.leftSprite);
		UpdateIcon(rightImage, talent.rightSprite);
	}

	public void UpdateIcon(Image image, Sprite sprite)
	{
		if(sprite == null) HideIcon(image);
		else
		{
			image.sprite = sprite;
			image.enabled = true;
		}
	}

	public void HideIcons()
	{
		HideIcon(leftImage);
		HideIcon(rightImage);
	}

	public void HideIcon(Image image)
	{
		image.sprite = null;
		image.enabled = false;
	}

	public void UpdateMotion()
	{
		if(IsSelected() || CanSelect())
		{
			animationManager.MoveIn();
		}
		else
		{
			animationManager.MoveOut();
		}
	}

	public void UpdateLight()
	{
		if(CanSelect())
		{
			animationManager.Lighten();
		}
		else
		{
			animationManager.Darken();
		}
	}

	public void ShowOptions()
	{
		for(int i = 0; i < optionSlots.Length; i++)
		{
			optionSlots[i].Show();
		}
	}

	public bool CanSelect()
	{
		return (talent != null && talent.CanSelect());
	}

	public bool IsSelected()
	{
		return (talent != null && talent.IsSelected());
	}

	public void Select()
	{
		talent.Select();
		UpdateLight();
		ShowOptions();
	}

	public Talent GetTalent()
	{
		return talent;
	}

	public void OnButtonPressed()
	{
		if(CanSelect()) Select();
	}
	
	public void OnPointerEnter()
	{
		if(talent != null) tooltip.Set(talent, buttonRect);
	}

	public void OnPointerExit()
	{
		tooltip.Hide();
	}
}

