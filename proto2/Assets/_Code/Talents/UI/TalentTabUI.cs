﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TalentTabUI : MonoBehaviour 
{
    public Slot previewSlot;
	public Transform navigationButtonContainer;
	private Slot[] navigationSlots;
	public TalentTreeManager talentTreeManager;
	private TalentTree[] talentTrees;
	private TalentTreeUI[] talentTreeUIs;
	private TalentTreeUI activeTree;
	public UIToggler toggler;
	public ColorReference buttonSelectedColor, buttonAvailableColor;
	private bool previewing;

	public void Awake()
	{
		previewSlot.onClick += HandlePreview;
		navigationSlots = navigationButtonContainer.GetComponentsInChildren<Slot>();
		for(int i = 0; i < navigationSlots.Length; i++)
		{
			navigationSlots[i].SetIndex(i);
			navigationSlots[i].onClick += ActivateTree;
		}
	}

	public void Start()
	{
		talentTreeUIs = GetComponentsInChildren<TalentTreeUI>();
		talentTrees = talentTreeManager.GetComponentsInChildren<TalentTree>();
		
		talentTreeManager.onTalentChanged += ResetAnimation;
		talentTreeManager.onTalentChanged += UpdateUI;
		toggler.onShow += UpdateUI;
	}

	public void HandlePreview(int index)
	{
		if(activeTree == null) return;
		SetPreview(!previewing);
		if(previewing) 
			activeTree.Preview();
		else 
			activeTree.Show();

	}

	public void SetPreview(bool toSet)
	{
		previewing = toSet;
		previewSlot.SetColor((toSet) ? buttonSelectedColor : buttonAvailableColor);

	}

	public void UpdateUI()
	{
		SetPreview(false);
		int minIndex = talentTrees.Length + 1;
		UpdateButtons();
		for(int i = 0; i < talentTrees.Length; i++)
		{
			if(talentTrees[i].IsEmpty() || !talentTreeManager.HasPoints())
			{
				
			}
			else 
			{
				if(i < minIndex) minIndex = i;
			}
		}

		if(minIndex < talentTreeUIs.Length) 
		{
			ActivateTree(minIndex);
			previewSlot.Show();
		}
		else
		{
			previewSlot.Hide();
		}
	}

	public void UpdateButtons()
	{
		for(int i = 0; i < talentTreeUIs.Length; i++)
		{
			if(talentTrees[i].IsEmpty() || !talentTreeManager.HasPoints())
			{				
				navigationSlots[i].Hide();
			}
			else
			{
				bool isActive = talentTreeUIs[i] == activeTree;
				
				navigationSlots[i].SetText(talentTrees[i].GetName());
				navigationSlots[i].SetColor((isActive) ? buttonSelectedColor : buttonAvailableColor);
				navigationSlots[i].Show();
			}
		}
	}
	
	public void ResetAnimation()
	{
		HideActiveTree();
	}

	public void HideActiveTree()
	{
		if(activeTree != null) activeTree.Hide();
	}

	public void ActivateTree(int index)
	{
		if(index < 0 || index >= talentTreeUIs.Length) return;

		HideActiveTree();
		activeTree = talentTreeUIs[index];

		UpdateButtons();
		activeTree.UpdateText();
		activeTree.Show();
		SetPreview(false);
	}
}
