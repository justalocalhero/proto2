﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tooltips;

public class TalentOptionSlot : MonoBehaviour 
{
	private RectTransform parentRect;
	private TalentSlot parent;
	private TalentOption talentOption;
	public Button button;
	public Image icon;
	public Image buttonImage{get; set;}
	public TalentOptionAnimationManager animationManager;	
	private TalentTooltip tooltip;
	
	public void Start()
	{
		buttonImage = button.GetComponent<Image>();
	}

	public void Set(TalentOption talentOption)
	{
		if(talentOption == null) return;
		this.talentOption = talentOption;
		ResetAnimations();
	}

	public void SetParent(TalentSlot newParent)
	{
		if(newParent == null) return;
		parent = newParent;
		parentRect = parent.buttonRect;
		
	}

	public void SetTooltip(TalentTooltip newTooltip)
	{
		tooltip = newTooltip;
	}

	public void Clear()
	{
		talentOption = null;
		ResetAnimations();
	}

	public void Show()
	{
		UpdateMotion();
	}

	public void Preview()
	{
		if(talentOption == null) return;
		animationManager.Expand();
		UpdateIcon();
	}

	public void UpdateIcon()
	{
		if(talentOption == null)
			HideImage(icon);
		else
			UpdateImage(icon, talentOption.sprite);
	}

	public void HideIcon()
	{
		HideImage(icon);
	}

	public void UpdateImage(Image image, Sprite sprite)
	{
		if(sprite == null) HideImage(image);
		else
		{
			image.sprite = sprite;
			image.enabled = true;
		}
	}

	public void HideImage(Image image)
	{
		image.sprite = null;
		image.enabled = false;
	}

	public void UpdateMotion()
	{
		if(IsSelected() || CanSelect())
		{
			animationManager.Expand();
			UpdateIcon();
		}
		else
		{
			animationManager.Contract();
			HideIcon();
		}
	}

	public void UpdateLight()
	{
		if(CanSelect())
		{
			animationManager.Lighten();
		}
		else
		{
			animationManager.Darken();
		}
	}

	public void Hide()
	{
		animationManager.Contract();
	}

	public void ResetAnimations()
	{
		animationManager.ResetState();
	}

	public TalentOption GetTalentOption()
	{
		return talentOption;
	}

	public bool IsSelected()
	{
		return talentOption != null && talentOption.IsSelected();
	}

	public void Select()
	{
		talentOption.Select();
		UpdateLight();
	}

	public bool CanSelect()
	{
		return talentOption != null && talentOption.CanSelect();
	}

	public void OnButtonPressed()
	{
		if(CanSelect()) Select();
	}

	public void OnPointerEnter()
	{
		if(talentOption != null) tooltip.Set(parent.GetTalent(), talentOption, parentRect);
	}

	public void OnPointerExit()
	{
		tooltip.Hide();
	}
}
