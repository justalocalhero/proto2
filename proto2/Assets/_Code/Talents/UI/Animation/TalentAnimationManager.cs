﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using UnityEngine.UI;

public class TalentAnimationManager : MonoBehaviour 
{
	public TalentSlot talent;
	public MotionTweenStats inStats;
	public MotionTweenStats outStats;
	public ColorTweenStats lightenStats;
	public ColorTweenStats darkenStats;

	public void MoveIn()
	{
		DOTween.Kill(talent.transform);
		talent.transform.DOLocalMove(inStats.position.localPosition, inStats.timer).SetEase(inStats.ease);
	}	

	public void MoveOut()
	{
		DOTween.Kill(talent.transform);
		talent.transform.DOLocalMove(outStats.position.localPosition, outStats.timer).SetEase(outStats.ease);
	}

	public void Lighten()
	{
		DOTween.Kill(talent.buttonImage);
		talent.buttonImage.DOColor(lightenStats.color, lightenStats.timer).SetEase(lightenStats.ease);
	}

	public void Darken()
	{
		DOTween.Kill(talent.buttonImage);
		talent.buttonImage.DOColor(darkenStats.color, darkenStats.timer).SetEase(darkenStats.ease);
	}

	public void ResetState()
	{
		talent.buttonImage.color = lightenStats.color;
		talent.transform.localPosition = outStats.position.localPosition;
	}

	[Serializable]
	public struct MotionTweenStats {public EaseReference ease; public FloatReference timer; public Transform position;};

	[Serializable]
	public struct ColorTweenStats {public EaseReference ease; public FloatReference timer; public ColorReference color;};
}


