﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class TalentOptionAnimationManager : MonoBehaviour {

	public TalentOptionSlot option;
	private RectTransform optionRect;

	public MotionTweenStats expandStats;
	public MotionTweenStats contractStats;
	public ColorTweenStats lightenStats;
	public ColorTweenStats darkenStats;

	public void Start()
	{
		optionRect = option.transform as RectTransform;
	}

	public void Expand()
	{
		DOTween.Kill(optionRect);
		optionRect.DOSizeDelta(expandStats.position.sizeDelta, expandStats.timer).SetEase(expandStats.ease);
	}	

	public void Contract()
	{
		DOTween.Kill(optionRect);
		optionRect.DOSizeDelta(contractStats.position.sizeDelta, contractStats.timer).SetEase(contractStats.ease);
	}

	public void Lighten()
	{
		DOTween.Kill(option.buttonImage);
		option.buttonImage.DOColor(lightenStats.color, lightenStats.timer).SetEase(lightenStats.ease);
	}

	public void Darken()
	{
		DOTween.Kill(option.buttonImage);
		option.buttonImage.DOColor(darkenStats.color, darkenStats.timer).SetEase(darkenStats.ease);
	}

	public void ResetState()
	{
		option.buttonImage.color = lightenStats.color;
		optionRect.sizeDelta = contractStats.position.sizeDelta;
	}

	[Serializable]
	public struct MotionTweenStats {public EaseReference ease; public FloatReference timer; public RectTransform position;};

	[Serializable]
	public struct ColorTweenStats {public EaseReference ease; public FloatReference timer; public ColorReference color;};
}
