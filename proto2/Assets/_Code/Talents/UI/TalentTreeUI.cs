﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using Tooltips;

public class TalentTreeUI : MonoBehaviour 
{
    public TalentTooltip tooltip;
    public TextMeshProUGUI pointsMesh;
	public TalentTree talentTree;
    private TalentSlotPair[] slots;

    public void Start()
    {
        slots = GetComponentsInChildren<TalentSlotPair>();
        talentTree.onTreeChanged += UpdateSlots;
        talentTree.onTreeChanged += UpdateText;
        talentTree.onTalentSelected += Show;
        talentTree.onTalentSelected += UpdateText;
        PassReferences();
    }

    public void PassReferences()
    {
        for(int i = 0; i < slots.Length; i++)
        {
            slots[i].left.SetParent(this);
            slots[i].right.SetParent(this);
            slots[i].left.SetTooltip(tooltip);
            slots[i].right.SetTooltip(tooltip);
        }
    }

    public void UpdateText()
    {
        pointsMesh.SetText("" + talentTree.GetSpentPoints());
    }

    public void UpdateSlots()
    {
        List<TalentPair> talents = talentTree.GetTalentPairs();
        int slotIndex;
        int talentIndex;
        for(int i = 0; i < slots.Length; i++)
        {
            slotIndex = slots.Length - i - 1;
            talentIndex = talents.Count - i - 1;

            if(i < talents.Count)
            {
                slots[slotIndex].left.Set(talents[talentIndex].left);
                slots[slotIndex].right.Set(talents[talentIndex].right);
            }
            else
            {
                slots[slotIndex].left.Clear();
                slots[slotIndex].right.Clear();
            }
        }
    }

    public void Clear()
    {
        for(int i = 0; i < slots.Length; i++)
        {
            slots[i].left.Clear();
            slots[i].right.Clear();
        }
    }

    public void Preview()
    {
        for(int i = 0; i < slots.Length; i++)
        {
            slots[i].left.Preview();
            slots[i].right.Preview();
        }

    }

    public void Show()
    {
        for(int i = 0; i < slots.Length; i++)
        {
            slots[i].left.Show();
            slots[i].right.Show();
        }
    }

    public void Hide()
    {
        for(int i = 0; i < slots.Length; i++)
        {
            slots[i].left.Hide();
            slots[i].right.Hide();
        }
    }

    public int GetSpentPoints()
    {
        return talentTree.GetSpentPoints();
    }

    public int GetMaxPoints()
    {
        return talentTree.GetMaxPoints();
    }
}

[Serializable]
public struct TalentPair {public Talent left; public Talent right;}

[Serializable]
public struct TalentBuilderPair {public TalentBuilder left; public TalentBuilder right;}