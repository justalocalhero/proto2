﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TalentTreeManagerUI : MonoBehaviour 
{
	public NextPlayerSelector nextPlayerSelector;
	public TalentTreeManager talentTreeManager;
	public TextMeshProUGUI availablePointsMesh;
	public TextMeshProUGUI buttonPointsMesh;
	public ColorReference maxedColor;
	public ColorReference defaultColor;

	// Use this for initialization
	void Start () 
	{
		talentTreeManager.onTalentSelected += UpdateUI;
		talentTreeManager.onTalentChanged += UpdateUI;
		if(nextPlayerSelector != null) nextPlayerSelector.onCardSelected += HandleCardSelected;
		UpdateUI();
	}

	public void HandleCardSelected(PlayerCard card)
	{
		UpdateUI();
	}

	public void UpdateUI()
	{
		availablePointsMesh.SetText(GetAvailablePointsText(talentTreeManager));
		buttonPointsMesh.SetText(GetAvailablePointsText(talentTreeManager));
	}

	public string GetAvailablePointsText(TalentTreeManager manager)
	{
		int available = manager.GetAvailablePoints();
		int spent = manager.GetSpentPoints();

		string text = (available > 0) ? spent + " / " + available : "0  / 1";
		Color color = (available == spent && available > 0) ? maxedColor : defaultColor;

		return Tooltips.ColoredText.GetText(text, color);
	}
}
