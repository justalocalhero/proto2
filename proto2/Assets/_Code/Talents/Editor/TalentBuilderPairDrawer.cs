﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(TalentBuilderPair))]
public class TalentBuilderPairDrawer : PropertyDrawer {

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int leftSize = 180;
        int rightSize = 180;
        int spacing = 5;
        
        int leftIndent = 0;
        int rightIndent = leftIndent + spacing + leftSize;

        Rect leftRect = new Rect(position.x + leftIndent, position.y, leftSize, position.height);        
        Rect rightRect = new Rect(position.x + rightIndent, position.y, rightSize, position.height);

        EditorGUI.PropertyField(leftRect, property.FindPropertyRelative("left"), GUIContent.none);
        EditorGUI.PropertyField(rightRect, property.FindPropertyRelative("right"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
