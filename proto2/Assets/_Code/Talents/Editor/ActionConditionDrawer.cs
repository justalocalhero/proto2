﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(ActionCondition))]
public class ActionConditionDrawer : PropertyDrawer {

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int targetSize = 60;
        int comparisonSize = 60;
        int typeSize = 80;
		int valueSize = 40;
        int spacing = 5;
        
        int targetIndent = 0;
        int typeIndent = targetIndent + spacing + targetSize;
        int comparisonIndent = typeIndent + spacing + typeSize;
		int valueIndent = comparisonIndent +spacing + comparisonSize;

        Rect targetRect = new Rect(position.x + targetIndent, position.y, targetSize, position.height);        
        Rect comparisonRect = new Rect(position.x + comparisonIndent, position.y, comparisonSize, position.height);        
        Rect typeRect = new Rect(position.x + typeIndent, position.y, typeSize, position.height);
		Rect valueRect = new Rect(position.x + valueIndent, position.y, valueSize, position.height);

        EditorGUI.PropertyField(targetRect, property.FindPropertyRelative("target"), GUIContent.none);
        EditorGUI.PropertyField(comparisonRect, property.FindPropertyRelative("comparison"), GUIContent.none);
        EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("type"), GUIContent.none);		
        EditorGUI.PropertyField(valueRect, property.FindPropertyRelative("value"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
