﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalentTreeManager : MonoBehaviour 
{
	public delegate void OnTalentChanged();
	public OnTalentChanged onTalentChanged;

	public delegate void OnTalentSelected();
	public OnTalentSelected onTalentSelected;

	public delegate void OnTalentPreSelect();
	public OnTalentPreSelect onTalentPreSelected;

	public TalentTree[] talentTrees;

	public int spentPoints;
	public int maxPoints;
	public IntReference pointCap;
	private List<int> testIndeces;

	public void Reset()
	{

	}

	public void Set(Player unit)
	{		
		maxPoints = Mathf.Clamp(unit.GetTalentLevel(), 0, pointCap);
		spentPoints = 0;
		List<RaceClass> raceClasses = unit.GetRaceClasses();
		for(int i = 0; i < talentTrees.Length; i++)
		{
			if(i < raceClasses.Count) talentTrees[i].Set(raceClasses[i]);
			else talentTrees[i].Clear();
		}
		RaiseOnTalentChanged();
	}

	public List<Talent> GetSelected()
	{
		List<Talent> toReturn = new List<Talent>();

		for(int i = 0; i < talentTrees.Length; i++)
		{
			toReturn.AddRange(talentTrees[i].GetSelected());
		}

		return toReturn;
	}

	public List<int> GetSelectedIndeces()
	{
		List<int> toReturn = new List<int>();

		foreach(TalentTree tree in talentTrees)
		{
			toReturn.AddRange(tree.GetSelectedIndeces());
		}

		return toReturn;
	}

	public void RaiseOnTalentChanged()
	{
		if(onTalentChanged != null) onTalentChanged();
	}

	public bool CanSelect()
	{
		return (spentPoints < maxPoints);
	}

	public bool HasPoints()
	{
		return maxPoints > 0;
	}

	public void PreSelect()
	{
		RaiseOnTalentPreSelect();
	}

	public void Select()
	{
		spentPoints++;
		RaiseOnTalentSelected();
	}

	public void RaiseOnTalentSelected()
	{
		if(onTalentSelected != null) onTalentSelected();
	}

	public void RaiseOnTalentPreSelect()
	{
		if(onTalentPreSelected != null) onTalentPreSelected();
	}

	public int GetSpentPoints()
	{
		return spentPoints;
	}

	public int GetAvailablePoints()
	{
		int treeMaxPoints = 0;
		for(int i = 0; i < talentTrees.Length; i++)
		{
			treeMaxPoints += talentTrees[i].GetMaxPoints();
		}

		return Mathf.Clamp(treeMaxPoints, 0, maxPoints);
	}
}
