﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalentOption 
{
    public string name;
    public Sprite sprite;
    public AbilityAugment augment;
    private int tier;
    private Talent parent;
    private bool selected;
    public List<UnitResourceAugment> resourceAugments;
    public EquipStat[] permanentStats;
   
    public TalentOption(string name, Sprite sprite, AbilityAugment augment, int tier, List<UnitResourceAugment> resourceAugments, EquipStat[] permanentStats)
    {
        this.name = name;
        this.sprite = sprite;
        this.augment = augment;
        this.tier = tier;
        this.resourceAugments = resourceAugments;
        this.permanentStats = permanentStats;
    }

    public void SetParent(Talent newParent)
    {
        parent = newParent;
    }

    public Talent GetParent()
    {
        return parent;
    }

    public bool IsSelected()
    {
        return selected;
    }

    public bool CanSelect()
    {
        int tierID = 2 * tier + 1;
        return parent != null && parent.IsSelected() && parent.CanSelect(tierID);
    }

    public void Select()
    {
        selected = true;
        parent.SelectOption(this);        
    }

    public AbilityAugment GenerateAugment()
    {
        return augment.Clone();
    }

    public void OnRegister(Unit targetUnit)
    {
        if(resourceAugments != null) targetUnit.AddResourceAugments(resourceAugments);
        if(permanentStats != null) targetUnit.AdjustStats(permanentStats);
    }

    public void OnDeregister(Unit targetUnit)
    {
        if(resourceAugments != null) targetUnit.RemoveResourceAugments(resourceAugments);
        if(permanentStats != null) targetUnit.AdjustStats(permanentStats, -1);
    }

	public bool HasStats()
	{
		return permanentStats.Length > 0;
	}

	public bool HasResourceAugment()
	{
		return resourceAugments.Count > 0;
	}

	public bool HasStatic()
	{
		return HasStats() || HasResourceAugment();
	}

    public TalentOption Clone()
    {
        return new TalentOption(name, sprite, (augment == null) ? null : augment.Clone(), tier, resourceAugments, permanentStats);
    }
}
