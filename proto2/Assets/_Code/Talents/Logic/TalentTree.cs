﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalentTree : MonoBehaviour
{
	new public string name;
	public TalentTreeManager parent;
	public int spentPoints;

	public delegate void OnTreeChanged();
	public OnTreeChanged onTreeChanged;

	public delegate void OnTalentSelected();
	public OnTalentSelected onTalentSelected;

	private List<TalentPair> talentPairs = new List<TalentPair>();

	public List<TalentPair> GetTalentPairs()
	{
		return talentPairs;
	}

	public void PassReferences()
	{
		for(int i = 0; i < talentPairs.Count; i++)
		{
			if(talentPairs[i].left != null) talentPairs[i].left.SetParent(this);
			if(talentPairs[i].right != null) talentPairs[i].right.SetParent(this);
		}
	}

	public void Set(RaceClass raceClass)
	{
		if(raceClass == null) return;
		Reset();

		name = raceClass.GetName();
		TalentPair[] toAdd = raceClass.GenerateTalents();
		for(int i = 0; i < toAdd.Length; i++)
		{
			Add(toAdd[toAdd.Length - i - 1]);
		}
		PassReferences();
		RaiseOnTreeChanged();
	}

	public void Add(TalentPair talentPair)
	{
		if(talentPair.left != null && talentPair.right != null)
		talentPairs.Add(talentPair);
	}

	public void Clear()
	{
		talentPairs = new List<TalentPair>();
		RaiseOnTreeChanged();
	}

	public void Reset()
	{		
		talentPairs = new List<TalentPair>();
		spentPoints = 0;
	}

	public List<int> GetSelectedIndeces()
	{
		List<int> toReturn = new List<int>();

		for(int i = talentPairs.Count - 1; i >= 0; i--)
		{
			if(talentPairs[i].left != null && talentPairs[i].left.IsSelected()) 
			{
				toReturn.Add(0);
				toReturn.Add(talentPairs[i].left.GetSelectedOptionIndex());
			}
			else if(talentPairs[i].right != null && talentPairs[i].right.IsSelected()) 
			{
				toReturn.Add(1);
				toReturn.Add(talentPairs[i].right.GetSelectedOptionIndex());
			}
			else
			{
				toReturn.Add(-1);
				toReturn.Add(-1);
			}
		}

		return toReturn;
	}

	public List<Talent> GetSelected()
	{
		List<Talent> toReturn = new List<Talent>();
		for(int i = 0; i < talentPairs.Count; i++)
		{
			if(talentPairs[i].left != null && talentPairs[i].left.IsSelected()) 
				toReturn.Add(talentPairs[i].left);			
			if(talentPairs[i].right != null && talentPairs[i].right.IsSelected()) 
				toReturn.Add(talentPairs[i].right);			
		}
		return toReturn;
	}

	public int GetSpentPoints()
	{
		return spentPoints;
	}

	public int GetMaxPoints()
	{
		return (2 * talentPairs.Count);
	}

	public bool CanSelect(int tierID)
	{
		return (tierID == spentPoints && parent.CanSelect());
	}

	public void PreSelect()
	{
		parent.PreSelect();
	}

	public void Select()
	{
		spentPoints++;
		parent.Select();
		RaiseOnTalentSelected();
	}

	public void RaiseOnTalentSelected()
	{
		if(onTalentSelected != null) onTalentSelected();
	}

	public void RaiseOnTreeChanged()
	{
		if(onTreeChanged != null) onTreeChanged();
	}

	public bool IsEmpty()
	{
		return talentPairs.Count <= 0;
	}

	public string GetName()
	{
		return name;
	}
}
