﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Talent 
{
	public string name;
	public Sprite leftSprite;
	public Sprite rightSprite;
	private bool selected;
	public TalentTree parent;
	public TalentAbility timedAbility;
	public TalentTriggeredAbility triggeredAbility;
	public TalentOption[] options;
	public int tier;
	public List<UnitResourceAugment> resourceAugments;
	public EquipStat[] permanentStats;
	private TalentOption selectedOption;

	public Talent(string name, Sprite leftSprite, Sprite rightSprite, int tier, TalentAbility timedAbility, TalentTriggeredAbility triggeredAbility, TalentOption[] options, List<UnitResourceAugment> resourceAugments, EquipStat[] permanentStats)
	{
		this.name = name;
		this.leftSprite = leftSprite;
		this.rightSprite = rightSprite;
		this.tier = tier;
		this.timedAbility = timedAbility;
		this.triggeredAbility = triggeredAbility;
		this.options = options;
		for(int i = 0; i < options.Length; i++)
		{
			options[i].SetParent(this);
		}
		this.resourceAugments = resourceAugments;
		this.permanentStats = permanentStats;

	}

	public Talent(string name)
	{
		this.name = name;
	}

	public void SetParent(TalentTree newParent)
	{
		parent = newParent;
	}

	public string GetName()
	{
		return name;
	}

	public int GetTier()
	{
		return tier;
	}

	public bool IsSelected()
	{
		return selected;
	}

	public bool HasAugment()
	{
		return selectedOption != null;
	}

	public bool HasStats()
	{
		return permanentStats.Length > 0;
	}

	public bool HasResourceAugment()
	{
		return resourceAugments.Count > 0;
	}

	public bool HasStatic()
	{
		return HasStats() || HasResourceAugment();
	}

	public bool HasAbilityAndStatic()
	{
		return HasAbility() && HasStatic();
	}

	public bool HasAbility()
	{
		return (timedAbility != null || triggeredAbility != null);
	}

	public void Select()
	{
		parent.PreSelect();
		selected = true;
		parent.Select();
	}

	public bool CanSelect(int tierID)
	{
		return parent != null && parent.CanSelect(tierID);
	}

	public bool CanSelect()
	{
		int tierID = 2 * tier;
		return CanSelect(tierID);
	}

	public TalentOption GetSelectedOption()
	{
		return selectedOption;
	}

	public int GetSelectedOptionIndex()
	{
		for(int i = 0; i  < options.Length; i++)
		{
			if(options[i] == selectedOption) return i;
		}

		return -1;
	}

	public void SelectOption(int index)
	{
		if(index < 0 || index >= options.Length) return;

		SelectOption(options[index]);
	}

	public void SelectOption(TalentOption toSelect)
	{
		if(parent != null) parent.PreSelect();
		if(timedAbility != null && toSelect.augment != null) timedAbility.SelectAugment(toSelect.GenerateAugment());
		if(triggeredAbility != null && toSelect.augment != null) triggeredAbility.SelectAugment(toSelect.GenerateAugment());
		selectedOption = toSelect;
		if(parent != null) parent.Select();
	}

	public void OnRegister(Unit targetUnit)
	{
		if(timedAbility != null) targetUnit.AddAction(timedAbility);
		if(triggeredAbility != null) targetUnit.AddTriggeredAction(triggeredAbility);
		if(permanentStats != null) targetUnit.AdjustStats(permanentStats);
		if(resourceAugments != null) targetUnit.AddResourceAugments(resourceAugments);
		if(selectedOption != null) selectedOption.OnRegister(targetUnit);
	}

	public void OnDeregister(Unit targetUnit)
	{
		if(timedAbility != null) targetUnit.RemoveAction(timedAbility);
		if(triggeredAbility != null) targetUnit.RemoveTriggeredAction(triggeredAbility);
		if(permanentStats != null) targetUnit.AdjustStats(permanentStats, -1);
		if(resourceAugments != null) targetUnit.RemoveResourceAugments(resourceAugments);
		if(selectedOption != null) selectedOption.OnDeregister(targetUnit);
	}

	public Talent Clone()
	{
		Talent toReturn =  new Talent(name, leftSprite, rightSprite, tier, (timedAbility == null) ? null : timedAbility.Clone() as TalentAbility, (triggeredAbility == null) ? null : triggeredAbility.Clone() as TalentTriggeredAbility, options, resourceAugments, permanentStats);
		TalentOption[] toAdd = new TalentOption[options.Length];
		for(int i = 0; i < toAdd.Length; i++)
		{
			toAdd[i] = options[i].Clone();
		}
		toReturn.options = toAdd;
		return toReturn;
	}
}
