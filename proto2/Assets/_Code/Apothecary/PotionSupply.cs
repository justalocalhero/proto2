﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionSupply: Supply
{
	public string description;
	public Color descriptionColor;
	public delegate void OnChanged(PotionSupply supply);
	public OnChanged onChanged;

	public delegate void OnUse(Unit unit);
	public OnUse onUse;

	public override void RaiseOnChanged()
	{
		if(onChanged != null) onChanged(this);
	}

	public void Use(Unit unit)
	{
		if(onUse != null) onUse(unit);	
	}
}