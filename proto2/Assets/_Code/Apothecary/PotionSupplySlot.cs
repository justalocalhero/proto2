﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionSupplySlot : SupplySlot
{

	private PotionSupply potionSupply;
	public override Supply supply { get { return potionSupply; } }

	public FloatReference indentPercentage;
	public ColoredText coloredText;

	public void Set(PotionSupply potionSupply)
	{
		if(potionSupply == null)
		{
			Clear();
			return;
		}
		
		this.potionSupply = potionSupply;

		UpdateText();
		Show();
	}

	public override void UpdateText()
	{
		string nameText = potionSupply.name;
		string descriptionText = GetDescriptionText();

		string totalText = nameText + descriptionText;

		mesh.SetText(totalText);	
	}

	public string GetDescriptionText()
	{
		return coloredText.GetText("<pos=" + indentPercentage.Value +"%>" + potionSupply.description, potionSupply.descriptionColor);
	}
}
