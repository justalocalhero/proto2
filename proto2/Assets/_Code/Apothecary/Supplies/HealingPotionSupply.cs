﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Potions/Supplies/Healing")]
public class HealingPotionSupply : PotionSupplyBuilder
{
	public FloatReference amount;
	
	public override void OnUse(Unit unit)
	{
		unit.Heal(amount);
	}

	public override string GetDescription()
	{
		return base.GetDescription(amount.Value.ToString());
	}
}