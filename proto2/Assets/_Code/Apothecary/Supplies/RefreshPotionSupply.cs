﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Potions/Supplies/RefreshCooldowns")]
public class RefreshPotionSupply : PotionSupplyBuilder
{
	public override void OnUse(Unit unit)
	{
		unit.RefreshCooldowns();
	}
}
