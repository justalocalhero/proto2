﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Potions/Supplies/Escape")]
public class EscapePotionSupply : PotionSupplyBuilder
{
	public override void OnUse(Unit unit)
	{
		unit.Escape();
	}
}
