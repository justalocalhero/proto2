﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Potions/Supplies/Cleansing")]
public class CleansingPotionSupply : PotionSupplyBuilder
{
	public override void OnUse(Unit unit)
	{
		unit.ClearDebuffs();
	}
}
