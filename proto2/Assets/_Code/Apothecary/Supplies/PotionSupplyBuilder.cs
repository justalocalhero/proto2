﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PotionSupplyBuilder : ScriptableObject
{
	new public string name;
	public ColorReference descriptionColor;
	public StringReference descriptionPrefix;
	public StringReference descriptionSuffix;

	public virtual PotionSupply Generate()
	{
		PotionSupply toReturn = new PotionSupply();

		toReturn.name = name;
		toReturn.description = GetDescription();
		toReturn.descriptionColor = descriptionColor;
		toReturn.onUse += OnUse;

		return toReturn;
	}

	public virtual string GetDescription()
	{
		return GetDescription("");
	}

	public string GetDescription(string content)
	{
		
		return descriptionPrefix + " " + content + " " + descriptionSuffix;
	}

	public virtual void OnUse(Unit unit)
	{

	}
}

