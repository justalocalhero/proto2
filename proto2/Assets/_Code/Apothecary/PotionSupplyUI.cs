﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionSupplyUI : SupplyListUI 
{
	public PotionSupplyManager potionSupplyManager;
	
	public PotionSupplySlot potionSupplyPrototype;
	public override SupplySlot supplySlotPrototype { get { return potionSupplyPrototype; } }

	private List<PotionSupplySlot> slots;

	public override void RegisterDelegates()
	{
		potionSupplyManager.onChanged += UpdateUI;
	}	
	
	public override void UpdateUI()
	{
		UpdateSlots();
	}

	public override void ConvertSlots(List<MonoBehaviour> slotMono)
	{		
		slots = slotMono.ConvertAll<PotionSupplySlot>(x => x as PotionSupplySlot);
	}

	public void UpdateSlots()
	{
		int index = 0;

		foreach(PotionSupply supply in potionSupplyManager.supplies)
		{
			if(supply.IsAvailable() && index < slots.Count)
			{
				slots[index++].Set(supply);
			}
		}

		for(int i = index; i < slots.Count; i++)
		{
			slots[i].Clear();
		}
	}
}
