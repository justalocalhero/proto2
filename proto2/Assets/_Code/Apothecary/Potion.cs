﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Potion
{	
	public float healingAmount;
	public int maxCount;
	private int currentCount;
	public int spriteIndex;


	public List<PotionSupply> potionSupplies = new List<PotionSupply>();

	public Potion(int spriteIndex, int count, float baseHealingAmount, List<PotionSupply> potionSupplies)
	{
		this.spriteIndex = spriteIndex;
		maxCount = currentCount = count;
		healingAmount = baseHealingAmount;
		
		foreach(PotionSupply supply in potionSupplies)
		{
			AddSupply(supply);
		}
	}

    public int GetSprite()
    {
		return spriteIndex;
    }
	
	public void AddSupply(PotionSupply supply)
	{
		potionSupplies.Add(supply);
	}

	public bool CanUse()
	{
		return currentCount > 0;
	}

	public void Use(Unit unit)
	{
		if(!CanUse()) return;

		unit.Heal(healingAmount);
		
		foreach(PotionSupply supply in potionSupplies)
		{
			supply.Use(unit);
		}
		
		currentCount--;
	}

	public int GetCount()
	{
		return currentCount;
	}
}
