﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ApothecaryUI : SupplyListUI 
{

	public PotionSupplyManager potionSupplyManager;

	public TextMeshProUGUI headerText, supplyText;
	
	public ColoredText coloredText;
	public ColorReference potionCountColor, potionHealingColor, supplyDefaultColor, supplyFilledColor;

	public PotionSupplySlot potionSupplySlot;
	public override SupplySlot supplySlotPrototype {get { return potionSupplySlot; } }
	private List<PotionSupplySlot> slots;

	public override void RegisterDelegates()
	{
		potionSupplyManager.onChanged += UpdateUI;
	}

	public override void ConvertSlots(List<MonoBehaviour> slotMono)
	{
		slots = slotMono.ConvertAll<PotionSupplySlot>(x => x as PotionSupplySlot);
	}

	public override void UpdateUI()
	{
		UpdateHeaderText();
		UpdateSlots();
		UpdateSupplyText();
	}

	public void UpdateSlots()
	{
		int index = 0;

		foreach(PotionSupply supply in potionSupplyManager.supplies)
		{
			if(supply.IsSelected() && index < slots.Count)
			{
				slots[index++].Set(supply);
			}
		}

		for(int i = index; i < slots.Count; i++)
		{
			slots[i].Clear();
		}
	}

	public void UpdateHeaderText()
	{
		string text = "Potions:";
		
		text += GetHealingText();
		text += GetCountText();

		headerText.SetText(text);
	}

	private string GetCountText()
	{
		int value = potionSupplyManager.potionCount.Value;

		string toReturn = "\nAdventurers carry " + value;
		toReturn += (value > 1) ? " potions" : " potion";

		return coloredText.GetText(toReturn, potionCountColor);
	}

	private string GetHealingText()
	{
		return coloredText.GetText("\nEach potion heals for " + potionSupplyManager.baseHealing.Value + " hitpoints", potionHealingColor);
	}

	public void UpdateSupplyText()
	{
		int selected = potionSupplyManager.SelectedCount();
		int max = potionSupplyManager.maxSupplies.Value;
		string delimiter = " / ";
		Color color = (selected == max) ? supplyFilledColor : supplyDefaultColor;
		
		string text = coloredText.GetText(selected + delimiter + max, color);

		supplyText.SetText(text);
	}
}