﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionSupplyManager : MonoBehaviour, IUnlocker
{
	public SaveManager saveManager;
	public SpriteManager spriteManager;
	private int spriteIndex;
	public Apothecary apothecary;
	public Bullpen bullpen;
	public Sprite potionSprite;
	public IntVariable maxSupplies, potionCount;
	public FloatVariable baseHealing;
	public List<PotionSupplyBuilder> builders;
	public List<PotionSupply> supplies {get; set;}

	public delegate void OnChanged();
	public OnChanged onChanged;

	public void Awake()
	{
		saveManager.onPreSave += Save;
		saveManager.onLoad += Load;

		BuildSupplies();
		ValidateSprite();
	}

	public void Save(SaveFile saveFile)
	{
		if(saveFile == null) return;
		if(supplies == null) return;

		bool[] suppliesSelected = new bool[supplies.Count];

		for(int i = 0; i < supplies.Count; i++)
		{
			suppliesSelected[i] = supplies[i].IsSelected();
		}

		saveFile.potionSupplySave = suppliesSelected;
	}

	public void Load(SaveFile saveFile)
	{
		if(saveFile == null) return;
		if(saveFile.potionSupplySave == null) return;
		
		for(int i = 0; i < supplies.Count; i++)
		{
			if(i < saveFile.potionSupplySave.Length && saveFile.potionSupplySave[i]) supplies[i].Select();
			else supplies[i].Deselect();
		}
	}
	
	public void SetSpriteIndex()
	{
		if(potionSprite == null) return;
		if(spriteManager == null) return;
		spriteIndex = spriteManager.GetIndex(potionSprite);
		if(spriteIndex == -1)
			spriteIndex = spriteManager.AddSprite(potionSprite);
		if(spriteIndex == -1)
			Debug.LogWarning("Panic");
	}

	public void OnValidate()
	{
		ValidateSprite();
	}

	public void ValidateSprite()
	{
		if(potionSprite == null) return;
		if(spriteManager == null) return;

		if(potionSprite == spriteManager.GetSprite(spriteIndex)) 
			return;
		else 
			SetSpriteIndex();
	}

	public void Start()
	{		
		apothecary.onUnlock += RaiseOnChanged;
		apothecary.onUpgrade += RaiseOnChanged;
		bullpen.onPlayerSent += AddPotion;
	}
	
	public void BuildSupplies()
	{
		supplies = new List<PotionSupply>();

		foreach(PotionSupplyBuilder builder in builders)
		{
			PotionSupply temp = builder.Generate();

			temp.parent = this;
			temp.onChanged += RaiseOnChanged;

			supplies.Add(temp);
		}
	}

    public void SetLevel(int level)
	{
		bool changed = false;

		for(int i = 0; i < level; i++)
		{
			if(supplies != null && supplies.Count > i && supplies[i].IsLocked())
			{
				supplies[i].Unlock();
				changed = true;
			}
		}

		if(changed) RaiseOnChanged();
	}

	public bool CanSelect()
	{
		if(maxSupplies == null) 
		{
			Debug.LogWarning("Appothecary Supply Count not set.");
			return false;
		}

		return maxSupplies.Value > SelectedCount();
	}

	public int SelectedCount()
	{
		int count = 0;

		foreach(PotionSupply supply in supplies)
		{
			if(supply.IsSelected()) count++;
		}

		return count;
	}

	public string UnlockDescription(int level)
	{
		if(supplies == null || level < 0 || level >= supplies.Count) return "";

		return supplies[level].name;
	}

	public void RaiseOnChanged(PotionSupply supply)
	{
		RaiseOnChanged();
	}

	public void RaiseOnChanged()
	{
		if(onChanged != null) onChanged();
	}

	public void AddPotion(Unit unit)
	{
		unit.potion = GeneratePotion();
	}

	public Potion GeneratePotion()
	{
		List<PotionSupply> toAdd = new List<PotionSupply>();

		foreach(PotionSupply supply in supplies)
		{
			if(supply.IsSelected()) toAdd.Add(supply);
		}

		Potion potion = new Potion(spriteIndex, potionCount.Value, baseHealing.Value, toAdd);

		return potion;
	}
}
