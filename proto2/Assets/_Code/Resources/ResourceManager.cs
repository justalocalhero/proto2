﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ResourceManager : MonoBehaviour 
{
	public SaveManager saveManager;

	public Resource[] resources;

	public delegate void OnResourceChanged();
	public OnResourceChanged onResourceChanged;

	public void Awake()
	{
		saveManager.onPreSave += Save;
		saveManager.onLoad += Load;
	}

	public void Save(SaveFile saveFile)
	{
		if(saveFile == null) return;

		saveFile.resources = resources;

	}

	public void Load(SaveFile saveFile)
	{
		if(saveFile == null) return;
		if(saveFile.resources == null) return;

		SetResources(saveFile.resources);
	}

	public void Initialize()
	{
		ApplyChange(new Resource(){type = ResourceType.scrap, value = 1});
	}

	public Resource[] GetResources()
	{
		return resources;
	}

	public void SetResources(Resource[] newResources)
	{
		resources = newResources;

		RaiseOnResourceChanged();
	}

	public Resource GetResources(int index)
	{
		return resources[index];
	}

	public Resource GetResources(ResourceType type)
	{
		return resources[(int)type];
	}

	public void ApplyChange(Resource resource)
	{
		if((int)resource.type >= resources.Length)
		{
			Debug.LogWarning("Resource type: " + resource.type + " not supported.  Can not apply change.");
			return;
		}

		int index = (int)resource.type;
		resources[index].value += resource.value;		
		if(resource.value > 0) resources[index].lifetime += resource.value;
		

		RaiseOnResourceChanged();

	}

	public void ApplyChange(Resource[] newResources)
	{
		foreach(Resource resource in newResources)
		{
			ApplyChange(resource);
		}
	}

	public void ApplyChange(List<Resource> newResources)
	{
		foreach(Resource resource in newResources)
		{
			ApplyChange(resource);
		}
	}

	public bool CheckChange(Resource resource)
	{
		if((int)resource.type >= resources.Length)
		{
			Debug.LogWarning("Resource type: " + resource.type + " not supported.  Can not check change.");
			return false;
		}

		if(resources[(int)resource.type].value + resource.value >= 0)
		{
			return true;
		}
		return false;
	}

	public bool CheckChange(List<Resource> newResources)
	{
		bool check = true;

		List<Resource> merged = MergeResources(newResources);
		foreach(Resource resource in merged)
		{
			check = check && CheckChange(resource);
		}

		return check;
	}

	public bool CheckVisible(List<Resource> resourceCost)
	{
		bool check = true;
		
		foreach(Resource resource in resourceCost)
		{
			check = check && CheckVisible(resource);
		}

		return check;
	}

	public bool CheckVisible(Resource cost)
	{
		if((int)cost.type >= resources.Length)
		{
			Debug.LogWarning("Resource type: " + cost.type + " not supported.  Can not check change.");
			return false;
		}

		Resource current = resources[(int)cost.type];
		bool lessThanHalf = -cost.value <= 2 * current.value;
		bool lessThanLifetime = -cost.value <= current.lifetime;

		return(lessThanHalf || lessThanLifetime);
	}

	public void PrintResources()
	{
		for(int i = 0; i < resources.Length; i++)
		{
		}
	}

	public void RaiseOnResourceChanged()
	{
		if(onResourceChanged != null) onResourceChanged();
	}

	public bool isEmpty()
	{
		bool toReturn = true;
		
		for(int i = 0; i < resources.Length; i++)
		{
			if(resources[i].value > 0) toReturn = false;
		}

		return toReturn;
	}

	public List<Resource> MergeResources(List<Resource> resources)
	{
		List<Resource> toReturn = new List<Resource>();

		foreach(Resource resource in resources)
		{
			bool found = false;

			for(int i = 0; i < toReturn.Count; i++)
			{
				Resource current = toReturn[i];
				if(current.type == resource.type)
				{
					found = true;
					current.value += resource.value;
					toReturn[i] = current;
				}
			}

			if(!found) toReturn.Add(resource);
		}

		return toReturn;
	}
}

public enum ResourceType { scrap, essence, refinedMaterials, holyWater };
[Serializable]
public struct Resource { public ResourceType type; public string name; public int value; public int lifetime;};