﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Tooltips;

public class ResourceManagerUI : MonoBehaviour 
{
	public ResourceManager resourceManager;
	public Transform slotContainer;
	public ResourceSlot slotPrototype;
	public ResourceText resourceText;
	private List<ResourceSlot> slots;

	private void Awake()
	{
		BuildSlots();

		resourceManager.onResourceChanged += UpdateUI;
	}

	private void Start()
	{
		UpdateUI();
	}

	public void BuildSlots()
	{
		int cap = System.Enum.GetNames(typeof(ResourceType)).Length;

		GridBuilder<ResourceSlot> gridBuilder = new GridBuilder<ResourceSlot>();

		slots = gridBuilder.Begin()
			.WithPrototype(slotPrototype)
			.WithContainer(slotContainer)
			.WithColumns(1)
			.WithCap(cap)
			.WithSpacing(2, 2)
			.Build();

		RegisterSlots();
	}

	private void RegisterSlots()
	{
		foreach(ResourceSlot slot in slots)
		{
			slot.WithResourceText(resourceText);
		}
	}

	private void UpdateUI()
	{
		if(resourceManager == null) return;

		Resource[] resources = resourceManager.GetResources();

		if(resources == null) return;

		int nextIndex = 0;

		foreach(Resource resource in resources)
		{
			if(nextIndex < slots.Count && CheckResource(resource))
			{
				slots[nextIndex++].Set(resource);
			}
		}

		for(int i = nextIndex; i < slots.Count; i++)
		{
			slots[i].Clear();
		}
	}

	private bool CheckResource(Resource resource)
	{
		return(resource.lifetime > 0 || resource.value > 0);
	}
	
}
