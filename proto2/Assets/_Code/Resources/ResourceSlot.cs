﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Tooltips;

public class ResourceSlot : MonoBehaviour 
{
	public TextMeshProUGUI nameText;
	public TextMeshProUGUI valueText;
	private ResourceText resourceText;

	public ResourceSlot WithResourceText(ResourceText resourceText)
	{
		this.resourceText = resourceText;

		return this;
	}

	public void Set(Resource resource)
	{
		nameText.SetText(resourceText.GetNameText(resource));
		valueText.SetText(resourceText.GetValueText(resource));
	}

	public void Clear()
	{
		nameText.SetText("");
		valueText.SetText("");
	}
}
