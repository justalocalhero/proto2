﻿using System.Collections;
using System.Collections.Generic;
using RotaryHeart.Lib.SerializableDictionary;

[System.Serializable]
public class SaveFile 
{
	public string version;
	public System.DateTime lastActiveTime;
	public float totalActiveTime;
	public SpriteManager spriteManager;
	
	public Equipment[] inventory;	
    public Equipment[] equipped;
	public Equipment[] artifactSave;
	public ForgeSave forgeSave;
	public Resource[] resources;
	public List<Battleground.State> battlegroundStates;

	[System.Serializable]
	public class ReputationDictionary : SerializableDictionaryBase<string, ReputationBuilding.ReputationSave> {};
	public ReputationDictionary reputationSaves;

	[System.Serializable]
	public class BuildingDictionary : SerializableDictionaryBase<string, TownBuilding.BuildingSave> {};	
	public BuildingDictionary buildingSaves;

	public CraftingSave designSave;
	public CraftingSave qualitySave;
	public CraftingSave divinitySave;
	public CraftingSave enchantmentSave;
	public CraftingSave materialSave;

	public bool[] campSupplySave;
	public bool[] potionSupplySave;

	public BoonSave religionSave, cabalSave, guildSave;

	public int religionSelected, cabalSelected, guildSelected;

	public bool talentButtonActive, battleButtonActive;

	public bool townTutorialComplete, supportTutorialComplete, smelterTutorialComplete, 
		religionTutorialComplete, equipTutorialComplete, enchantmentTutorialComplete, 
		cabalTutorialComplete, blessingTutorialComplete, boonTutorialComplete, craftingTutorialComplete,
		battleTutorialComplete, tavernTutorialComplete, potionTutorialComplete, campTutorialComplete,
		ascensionTutorialComplete;
}

[System.Serializable]
public class CraftingSave
{
	public List<bool> craftingUnlocked;
}

[System.Serializable]
public class ForgeSave
{
	public List<Equipment> equipment;	
	public bool autoForgeEnabled;
	public int autoForgeLevel;
}

[System.Serializable]
public class BoonSave
{
	public int[] selected;
}