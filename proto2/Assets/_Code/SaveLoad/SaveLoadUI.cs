﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SaveLoadUI : MonoBehaviour
{
	public Button save, load, clear;
	public SaveManager saveManager;

	public void Awake()
	{
		save.onClick.AddListener(Save);
		load.onClick.AddListener(Load);
		clear.onClick.AddListener(Clear);
	}

	public void Save()
	{
		saveManager.Save();
	}

	public void Load()
	{
		saveManager.Load();
	}

	public void Clear()
	{
		saveManager.Clear();
		Scene scene = SceneManager.GetActiveScene(); 
		SceneManager.LoadScene(scene.name);
	}
}
