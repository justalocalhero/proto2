﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;

//courtesy of Robert Wahler 
//https://gist.github.com/robertwahler/b3110b3077b72b4c56199668f74978a0

public static class SaveLoad 
{

	public static void DeleteKey(string key) 
	{
		#if UNITY_EDITOR

			UnityEngine.PlayerPrefs.DeleteKey(key: key);

		#elif UNITY_WEBGL

			RemoveFromLocalStorage(key: key);

		#endif
	}

	public static bool HasKey(string key) 
	{
		#if UNITY_EDITOR

			return (UnityEngine.PlayerPrefs.HasKey(key: key));

		#elif UNITY_WEBGL

			return (HasKeyInLocalStorage(key) == 1);

		#endif
	}

	public static string GetString(string key) 
	{

		#if UNITY_EDITOR

			return (UnityEngine.PlayerPrefs.GetString(key: key));

		#elif UNITY_WEBGL

			return LoadFromLocalStorage(key: key);

		#endif
	}

	public static void SetString(string key, string value) 
	{

		#if UNITY_EDITOR

			UnityEngine.PlayerPrefs.SetString(key: key, value: value);

		#elif UNITY_WEBGL

			SaveToLocalStorage(key: key, value: value);

		#endif

	}

	public static void Save() 
	{

		#if UNITY_EDITOR

			UnityEngine.PlayerPrefs.Save();

		#endif
	}

	#if UNITY_WEBGL
	[DllImport("__Internal")]
	private static extern void SaveToLocalStorage(string key, string value);

	[DllImport("__Internal")]
	private static extern string LoadFromLocalStorage(string key);

	[DllImport("__Internal")]
	private static extern void RemoveFromLocalStorage(string key);

	[DllImport("__Internal")]
	private static extern int HasKeyInLocalStorage(string key);
	#endif
}
