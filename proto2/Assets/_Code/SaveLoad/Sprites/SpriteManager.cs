﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif


[CreateAssetMenu(menuName="SpriteManager")]
public class SpriteManager : ScriptableObject 
{
	public List<Sprite> sprites;

	public void Initialize()
	{
		for(int i = 0; i < sprites.Count; i++)
		{
			GetSprite(i);
		}
	}

	public Sprite GetSprite(int index)
	{
		if(index < 0 || index >= sprites.Count) 
			return null;
		
		return sprites[index];
	}

	public int AddSprite(Sprite sprite)
	{
		int index = -1;

		#if UNITY_EDITOR
		if(!sprites.Contains(sprite))
		{
			sprites.Add(sprite);
			Dirty();
			index = sprites.Count - 1;
		}
		#endif

		return index;
	}

	public int GetIndex(Sprite sprite)
	{
		for(int i = 0; i < sprites.Count; i++)
		{
			if(sprites[i] == sprite) 
				return i;
		}

		Debug.LogWarning("Sprite not found in spriteManager");
		return -1;
	}

	private void Dirty()
	{
		#if UNITY_EDITOR
		EditorUtility.SetDirty(this);
		#endif
	}
}