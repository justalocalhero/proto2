﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour 
{
	private bool autosaving;
	public SpriteManager spriteManager;
	public StringReference version;

	private string key = "SaveKeyBoi";
	public SaveFile saveFile;

	public delegate void OnPreSave(SaveFile saveFile);
	public OnPreSave onPreSave;

	public delegate void OnSave(SaveFile saveFile);
	public OnSave onSave;

	public delegate void OnPreLoad(SaveFile saveFile);
	public OnPreLoad onPreLoad;

	public delegate void OnLoad(SaveFile saveFile);
	public OnLoad onLoad;

	public delegate void OnPostLoad();
	public OnPostLoad onPostLoad;	

	private IEnumerator autosaveCoroutine;

	public IEnumerator Autosave()
	{
		while(autosaving)
		{
			Save();
			saveFile.totalActiveTime += 60;
			yield return new WaitForSeconds(60f);
		}
	}

	public void EnableAutosave()
	{
		autosaving = true;
		autosaveCoroutine = Autosave();
		StartCoroutine(autosaveCoroutine);
	}

	public void DisablAutosave()
	{
		autosaving = false;
		StopCoroutine(autosaveCoroutine);
	}

	public SaveFile GetSaveFile()
	{
		if(HasSaveFile())
			return JsonUtility.FromJson<SaveFile>(SaveLoad.GetString(key));
		else 
			return new SaveFile();
	}

	public string GetSaveString()
	{
		if(HasSaveFile())
			return SaveLoad.GetString(key);
		else 
			return "";
	}

	public bool HasSaveFile()
	{
		return SaveLoad.HasKey(key);
	}

	public void RaiseOnPreSave()
	{
		if(onPreSave != null) 
			onPreSave(saveFile);
	}

	public void RaiseOnSave()
	{
		if(onSave != null) 
			onSave(saveFile);
	}

	public void RaiseOnLoad()
	{
		if(onLoad != null) 
			onLoad(saveFile);
	}

	public void RaiseOnPreLoad()
	{
		if(onPreLoad != null)
			onPreLoad(saveFile);
	}

	public void RaiseOnPostLoad()
	{
		if(onPostLoad != null)
			onPostLoad();
	}

	public void Save()
	{
		if(saveFile == null) saveFile = new SaveFile();

		saveFile.spriteManager = spriteManager;

		RaiseOnPreSave();
		saveFile.lastActiveTime = System.DateTime.Now;
		saveFile.version = version;
		SaveLoad.SetString(key, JsonUtility.ToJson(saveFile));
		RaiseOnSave();
	}

	public void Load()
	{
		saveFile = GetSaveFile();		

		RaiseOnPreLoad();
		RaiseOnLoad();
		RaiseOnPostLoad();
	}

	public void Clear()
	{
		if(SaveLoad.HasKey(key))
			SaveLoad.DeleteKey(key);

		saveFile = new SaveFile();
	}
}
