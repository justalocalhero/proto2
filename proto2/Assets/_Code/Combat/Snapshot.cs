﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snapshot 
{

    public Damage killingBlow;
    public Timable[] buffs;
    public Timable[] debuffs;
    public int killCount;
    public int levelReached;

    public Snapshot(Player player, Damage newKillingBlow, int newkillCount, int newLevel)
    {
        buffs = player.buffs.ToArray();
        debuffs = player.debuffs.ToArray();
        killingBlow = newKillingBlow;
        killCount = newkillCount;
        levelReached = newLevel;
    }
  
}
