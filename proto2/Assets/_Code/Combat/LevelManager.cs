﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour 
{
	public IntVariable generatorMinLevel, generatorMaxLevel;
	private int dMin, dMax;
	
	public int GetNextHeroLevel()
	{
		int adjustment = Mathf.Clamp((dMax - dMin) / 2, 0, dMax);
		int value = UnityEngine.Random.Range(generatorMinLevel.Value + adjustment, generatorMaxLevel.Value + 1);
		int clampedValue = Mathf.Clamp(value, generatorMinLevel.Value, generatorMaxLevel.Value);

		dMin = clampedValue - generatorMinLevel.Value;
		dMax = generatorMaxLevel.Value - clampedValue;

		return clampedValue;
	}
}
