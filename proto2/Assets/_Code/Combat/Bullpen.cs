﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullpen : MonoBehaviour 
{
	public Player nextPlayer;
	public NextPlayerSelector nextPlayerSelector;
	public EquipmentManager equipment;
	public Inventory inventory;
	public TalentTreeManager talents;
	public ClassManager classes;
	public ClassManager occupations;
	public ClassManager races;
	public PlayerGenerator playerGenerator;
	public UnitTooltip playerStatWindow;

	public CampSupplyManager campSupplyManager;
	public LevelManager levelManager;
	public Boons.BoonManager religionManager;
	public Boons.BoonManager cabalManager;
	public Boons.BoonManager guildManager;
	
	public delegate void OnPlayerSent(Player player);
	public OnPlayerSent onPlayerSent;

	public delegate void OnPlayerAdded();
	public OnPlayerAdded onPlayerAdded;

	public void Start()
	{
		nextPlayerSelector.onCardSelected += SetPlayer;
		
		religionManager.onChanged += UpdateBlessings;
		cabalManager.onChanged += UpdateSpells;
		guildManager.onChanged += UpdateAdvantages;

		equipment.onEquipmentChanged += UpdatePlayerEquipStats;
		talents.onTalentPreSelected += ClearPlayerTalentStats;
		talents.onTalentSelected += UpdatePlayerTalentStats;

	}

	public Player GetNextPlayer(bool resetTalents)
	{
		Player toReturn = nextPlayer;
		nextPlayer = playerGenerator.Generate(0, campSupplyManager);
		equipment.RemoveAll();
		AddClasses();
		if(resetTalents) AddTalentTrees();
		UpdatePlayerStats();

		if(toReturn != null) RaiseOnPlayerSent(toReturn);

		return toReturn;
	}

	public void SetPlayer(PlayerCard playerCard)
	{
		nextPlayer = playerGenerator.Generate(playerCard, campSupplyManager);
		if(playerCard.equipment != null) 
		{
			bool hasAtSlot = equipment.HasAtSlot(playerCard.equipment);
			if(!hasAtSlot) equipment.Add(playerCard.equipment);
			else inventory.Add(playerCard.equipment);

		}
		AddTalentTrees();
		UpdatePlayerStats();

		RaiseOnPlayerAdded();
	}

	public Player GetNextPlayer()
	{
		return GetNextPlayer(true);
	}

	public void AddClasses()
	{
		if(races != null) AddClass(races);
		if(occupations != null) AddClass(occupations);
		if(classes != null) AddClass(classes);
	}

	public void AddClass(ClassManager manager)
	{		
		if(nextPlayer == null) return;
		if(manager == null) return;
		RaceClass toAdd = manager.GetClass();
		if(toAdd == null) return;
		nextPlayer.AddRaceClass(toAdd);
	}

	public void AddTalentTrees()
	{

		if(nextPlayer == null) return;
		if(nextPlayer.raceClasses == null) return;
		int index = Mathf.Clamp(nextPlayer.raceClasses.Count, 0, 3);
		for(int i = 0; i < index; i++)
		{
			talents.Set(nextPlayer);
		}
	}

	public void UpdatePlayerStats()
	{
		UpdatePlayerTalentStats();
		UpdatePlayerEquipStats();
		UpdateBoons();
	}

	public void UpdatePlayerEquipStats()
	{
		if(nextPlayer == null) return;
		nextPlayer.RemoveAllEquipment();
		nextPlayer.AddEquipment(equipment.equipment);
		UpdatePlayerStatsWindow();
	}

	public void ClearPlayerTalentStats()
	{
		nextPlayer.RemoveAllTalents();
	}

	public void UpdatePlayerTalentStats()
	{
		if(nextPlayer == null) return;
		nextPlayer.AddTalent(talents.GetSelected());
		UpdatePlayerStatsWindow();
	}

	public void UpdateBoons()
	{
		UpdateBlessings();
		UpdateSpells();
		UpdateAdvantages();
		UpdatePlayerStatsWindow();
	}

	public void UpdateBlessings()
	{
		if(nextPlayer == null) return;
		nextPlayer.RemoveAllBlessings();
		nextPlayer.AddBlessing(religionManager.GetSelectedBoons());
		UpdatePlayerStatsWindow();
	}

	public void UpdateSpells()
	{
		if(nextPlayer == null) return;
		nextPlayer.RemoveAllSpells();
		nextPlayer.AddSpell(cabalManager.GetSelectedBoons());
		UpdatePlayerStatsWindow();
	}

	public void UpdateAdvantages()
	{

	}

	public void UpdatePlayerStatsWindow()
	{
		if(nextPlayer == null) return;
		playerStatWindow.Set(nextPlayer);
	}

	public void UpdatePlayerClassStats()
	{
		if(nextPlayer == null) return;
		nextPlayer.AddRaceClass(classes.GetSelectedClass());
	}

	public void RaiseOnPlayerSent(Player player)
	{
		if(onPlayerSent != null) onPlayerSent(player);
	}

	public void RaiseOnPlayerAdded()
	{
		if(onPlayerAdded != null) onPlayerAdded();
	}
}
