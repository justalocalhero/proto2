﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Loot;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "RaceClass/RaceClass")]
public class RaceClass : ScriptableObject 
{
	#if UNITY_EDITOR
        [Multiline]
        public string DeveloperDescription = "";
	#endif

	new public string name;
	public EquipStat[] staticStats;
	public float statScalingPerLevel;
	public AbilityBuilder[] abilities;
	public TriggeredActionBuilder[] triggeredActions;
	public TalentBuilderPair[] talents;
	public int bonusLootMods;
	public int bonusLootLevel;
	public float identifyChance;
	public List<UnitResourceAugment> unitResourceAugments;
	public LootValue lootValue;



	public Ability[] GenerateAbilities()
	{
		Ability[] toReturn = new Ability[abilities.Length];
		
		for(int i = 0; i < abilities.Length; i++)
		{
			toReturn[i] = abilities[i].Generate();
		}

		return toReturn;
		
	}

	public TriggeredAction[] GenerateTriggeredActions()
	{
		TriggeredAction[] toReturn = new TriggeredAction[triggeredActions.Length];

		for(int i = 0; i < triggeredActions.Length; i++)
		{
			toReturn[i] = triggeredActions[i].Generate();
		}

		return toReturn;
	}

	public TalentPair[] GenerateTalents()
	{
		TalentPair[] toReturn = new TalentPair[talents.Length];

		for(int i = 0; i < toReturn.Length; i++)
		{
			toReturn[i] = new TalentPair()
			{
				left = (talents[i].left != null) ? talents[i].left.Generate(i) : null,
				right = (talents[i].right != null) ? talents[i].right.Generate(i) : null,
			};
		}

		return toReturn;
	}	
	
	public List<Talent> GetTalentsFromIndeces(List<int> indeces)
	{
		List<Talent> toReturn = new List<Talent>();

		for(int i = 0; i < indeces.Count; i++)
		{
			toReturn.Add(GetTalentFromIndex(i / 2, indeces[i], indeces[++i]));
		}

		return toReturn;
	}

	private Talent GetTalentFromIndex(int pairIndex, int talentIndex, int optionIndex)
	{
		if(pairIndex < 0 || pairIndex >= talents.Length) return null;

		TalentBuilderPair currentPair = talents[pairIndex];
		Talent currentTalent = null;

		if(talentIndex == 0) currentTalent = currentPair.left.Generate(pairIndex);
		if(talentIndex == 1) currentTalent = currentPair.right.Generate(pairIndex);

		if(currentTalent == null) return null;

		currentTalent.SelectOption(optionIndex);

		return currentTalent;


	}

	public string GetName()
	{
		return name;
	}

	public void SetStats()
	{

	}

	public EquipStat[] GetStatArray()
	{
		return staticStats;
	}

	public List<EquipStat> GetStatList()
	{
		List<EquipStat> toReturn = new List<EquipStat>();

		foreach(EquipStat stat in staticStats)
		{
			toReturn.Add(stat);
		}

		return toReturn;
	}

	public void SetStats(List<EquipStat> stats)
	{
		SetStats(stats.ToArray());	
	}

	public void SetStats(EquipStat[] stats)
	{
		staticStats = stats;

		Dirty();
	}

	public void EditStaticStat(int index, EquipStat stat)
	{
		if(index < 0 || index >= staticStats.Length) return;

		staticStats[index] = stat;

		Dirty();
	}

	public void DeleteStaticStat(int index)
	{
		if(index < 0 || index >= staticStats.Length) return;

		var stats = new List<EquipStat>(staticStats);
		stats.RemoveAt(index);
		staticStats = stats.ToArray();

		Dirty();
	}

	public void ExtendStaticStats()
	{
		var stats = new List<EquipStat>(staticStats);
		stats.Add(new EquipStat());
		staticStats = stats.ToArray();

		Dirty();
	}

	public void AddTalent(TalentBuilder talentBuilder, int tier)
	{
		if(tier < 1 || tier > 5) return;

		UpdateTalents(tier);
		
		int index = tier - 1;

		if(talents[index].left == null) talents[index].left = talentBuilder;
		else if(talents[index].right == null) talents[index].right = talentBuilder;
		
		Dirty();
	}

	private void UpdateTalents(int tier)
	{
		if(talents.Length >= tier) return;
		
		TalentBuilderPair[] newTalents = new TalentBuilderPair[tier];

		for(int i = 0; i < newTalents.Length; i++)
		{
			if(i < talents.Length)
			{
				newTalents[i] = talents[i];
			}
			else
			{
				newTalents[i] = new TalentBuilderPair();
			}
		}

		talents = newTalents;
	}
	
	private void Dirty()
	{
		#if UNITY_EDITOR
		EditorUtility.SetDirty(this);
		#endif
	}
}
