﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnchantmentTutorial : Tutorial 
{
    public Academy academy;

    public override void RegisterCheck()
    {
        academy.onUnlock += HandleCheck;
    }

    public override void OnComplete()
    {
        academy.onUnlock -= HandleCheck;
    }

    public override bool Check()
    {
        return academy.unlocked;
    }

    public override void HandleSave(SaveFile saveFile)
    {
        if(saveFile == null) return;
        
        saveFile.enchantmentTutorialComplete = complete;
    }

    public override void Load(SaveFile saveFile)
    {
        if(saveFile.enchantmentTutorialComplete) Complete();
    }	
}