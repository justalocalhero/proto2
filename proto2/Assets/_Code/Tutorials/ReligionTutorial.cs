﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReligionTutorial : Tutorial 
{
    public Shrine shrine;

    public override void RegisterCheck()
    {
        shrine.onUnlock += HandleCheck;
    }

    public override void OnComplete()
    {
        shrine.onUnlock -= HandleCheck;
    }

    public override bool Check()
    {
        return shrine.unlocked;
    }

    public override void HandleSave(SaveFile saveFile)
    {
        if(saveFile == null) return;
        
        saveFile.religionTutorialComplete = complete;
    }

    public override void Load(SaveFile saveFile)
    {
        if(saveFile.religionTutorialComplete) Complete();
    }	
}