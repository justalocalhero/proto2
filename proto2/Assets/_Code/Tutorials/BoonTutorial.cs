﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoonTutorial : Tutorial 
{
    public List<ReputationBuildingManager> reputationManagers;

    public override void RegisterCheck()
    {
        foreach(ReputationBuildingManager manager in reputationManagers)
        {
            manager.onBuildingLevelUp += HandleCheck;
        }
    }

    public override void OnComplete()
    {
        foreach(ReputationBuildingManager manager in reputationManagers)
        {
            manager.onBuildingLevelUp -= HandleCheck;
        }
    }

    public override bool Check()
    {
        foreach(ReputationBuildingManager manager in reputationManagers)
        {
            if(manager.GetBuildingLevel() > 0) return true;
        }

        return false;
    }

    public override void HandleSave(SaveFile saveFile)
    {
        if(saveFile == null) return;
        
        saveFile.boonTutorialComplete = complete;
    }

    public override void Load(SaveFile saveFile)
    {
        if(saveFile.boonTutorialComplete) Complete();
    }	
}