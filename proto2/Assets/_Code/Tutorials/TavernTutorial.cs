﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TavernTutorial : Tutorial 
{
    public Tavern tavern;

    public override void RegisterCheck()
    {
        tavern.onUnlock += HandleCheck;
    }

    public override void OnComplete()
    {
        tavern.onUnlock -= HandleCheck;
    }

    public override bool Check()
    {
        return tavern.unlocked;
    }

    public override void HandleSave(SaveFile saveFile)
    {
        if(saveFile == null) return;
        
        saveFile.tavernTutorialComplete = complete;
    }

    public override void Load(SaveFile saveFile)
    {
        if(saveFile.tavernTutorialComplete) Complete();
    }	
}