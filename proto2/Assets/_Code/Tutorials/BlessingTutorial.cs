﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlessingTutorial : Tutorial 
{
    public Altar altar;

    public override void RegisterCheck()
    {
        altar.onUnlock += HandleCheck;
    }

    public override void OnComplete()
    {
        altar.onUnlock -= HandleCheck;
    }

    public override bool Check()
    {
        return altar.unlocked;
    }

    public override void HandleSave(SaveFile saveFile)
    {
        if(saveFile == null) return;
        
        saveFile.blessingTutorialComplete = complete;
    }

    public override void Load(SaveFile saveFile)
    {
        if(saveFile.blessingTutorialComplete) Complete();
    }	
}
