﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionTutorial : Tutorial 
{
    public Apothecary apothecary;

    public override void RegisterCheck()
    {
        apothecary.onUnlock += HandleCheck;
    }

    public override void OnComplete()
    {
        apothecary.onUnlock -= HandleCheck;
    }

    public override bool Check()
    {
        return apothecary.unlocked;
    }

    public override void HandleSave(SaveFile saveFile)
    {
        if(saveFile == null) return;
        
        saveFile.potionTutorialComplete = complete;
    }

    public override void Load(SaveFile saveFile)
    {
        if(saveFile.potionTutorialComplete) Complete();
    }	
}