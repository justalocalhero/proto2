﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftingTutorial : Tutorial 
{
    public Armory armory;

    public override void RegisterCheck()
    {
        armory.onUnlock += HandleCheck;
    }

    public override void OnComplete()
    {
        armory.onUnlock -= HandleCheck;
    }

    public override bool Check()
    {
        return armory.unlocked;
    }

    public override void HandleSave(SaveFile saveFile)
    {
        if(saveFile == null) return;
        
        saveFile.craftingTutorialComplete = complete;
    }

    public override void Load(SaveFile saveFile)
    {
        if(saveFile.craftingTutorialComplete) Complete();
    }	
}
