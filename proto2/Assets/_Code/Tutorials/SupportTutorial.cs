﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SupportTutorial : Tutorial 
{
     public FloatReference timeTillSupportActive;

    public override void RegisterCheck()
    {
        saveManager.onSave += HandleCheck;
    }

    public override void OnComplete()
    {
        saveManager.onSave -= HandleCheck;
    }    

	public void HandleCheck(SaveFile saveFile)
	{
        if(Check(saveFile))
            Complete();
	}

    public bool Check(SaveFile saveFile)
    {
		return(saveFile.totalActiveTime > timeTillSupportActive);
    }

    public override void HandleSave(SaveFile saveFile)
    {
        if(saveFile == null) return;
        
        saveFile.supportTutorialComplete = complete;
    }

    public override void Load(SaveFile saveFile)
    {
        if(saveFile.supportTutorialComplete) Complete();
    }
	
}