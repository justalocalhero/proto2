﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipTutorial : Tutorial 
{
    public override bool Check()
    {
        return true;
    }

    public override void HandleSave(SaveFile saveFile)
    {
        if(saveFile == null) return;
        
        saveFile.equipTutorialComplete = complete;
    }

    public override void Load(SaveFile saveFile)
    {
        if(saveFile.equipTutorialComplete) Complete();
    }	
}