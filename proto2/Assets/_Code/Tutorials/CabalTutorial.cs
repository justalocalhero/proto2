﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CabalTutorial : Tutorial 
{
    public Tower tower;

    public override void RegisterCheck()
    {
        tower.onUnlock += HandleCheck;
    }

    public override void OnComplete()
    {
        tower.onUnlock -= HandleCheck;
    }

    public override bool Check()
    {
        return tower.unlocked;
    }

    public override void HandleSave(SaveFile saveFile)
    {
        if(saveFile == null) return;
        
        saveFile.cabalTutorialComplete = complete;
    }

    public override void Load(SaveFile saveFile)
    {
        if(saveFile.cabalTutorialComplete) Complete();
    }	
}