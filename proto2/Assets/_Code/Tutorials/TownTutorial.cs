﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownTutorial : Tutorial 
{
    public Inventory inventory;

    public override void RegisterCheck()
    {
        inventory.onInventoryChanged += HandleCheck;
    }

    public override void OnComplete()
    {
        inventory.onInventoryChanged -= HandleCheck;
    }

    public override bool Check()
    {
        return inventory.GetEquipmentCount() >= 5;
    }

    public override void HandleSave(SaveFile saveFile)
    {
        if(saveFile == null) return;
        
        saveFile.townTutorialComplete = complete;
    }

    public override void Load(SaveFile saveFile)
    {
        if(saveFile.townTutorialComplete) Complete();
    }	
}