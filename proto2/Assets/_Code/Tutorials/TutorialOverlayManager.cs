﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class TutorialOverlayManager : MonoBehaviour, IPointerClickHandler
{
	public UIToggler toggler;
	public FloatReference clickDelay;
	private float nextFireTime;
	public Queue<Tutorial> messageQueue = new Queue<Tutorial>();
	public TextMeshProUGUI text;

	public void ResetClickDelay()
	{
		nextFireTime = Time.time + clickDelay;
	}

	public void Push(Tutorial tutorial)
	{
		if(!messageQueue.Contains(tutorial)) 
		{
			messageQueue.Enqueue(tutorial);
			CheckNext();
		}
	}

	public void HandleClick()
	{
		if(messageQueue.Count > 0)
		{
			Tutorial tut = messageQueue.Dequeue();
			tut.Complete();
			CheckNext();
		}
	}

	public void CheckNext()
	{
		if(messageQueue.Count > 0)
		{
			Tutorial tut = messageQueue.Peek();
			text.SetText(tut.GetText());
			ResetClickDelay();
			Show();
		}
		else Hide();
	}

	public void Show()
	{
		toggler.Show();
	}

	public void Hide()
	{
		toggler.Hide();
	}

    public void OnPointerClick(PointerEventData eventData)
    {
		if(Time.time > nextFireTime) HandleClick();
    }

}
