﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmelterTutorial : Tutorial 
{
    public Smelter smelter;

    public override void RegisterCheck()
    {
        smelter.onUnlock += HandleCheck;
    }

    public override void OnComplete()
    {
        smelter.onUnlock -= HandleCheck;
    }

    public override bool Check()
    {
        return smelter.unlocked;
    }

    public override void HandleSave(SaveFile saveFile)
    {
        if(saveFile == null) return;
        
        saveFile.smelterTutorialComplete = complete;
    }

    public override void Load(SaveFile saveFile)
    {
        if(saveFile.smelterTutorialComplete) Complete();
    }	
}