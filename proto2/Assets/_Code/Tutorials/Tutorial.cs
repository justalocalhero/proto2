﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Tutorial : MonoBehaviour 
{
	public SaveManager saveManager;
	public TutorialOverlayManager tutorialOverlayManager;
	public TextAsset messageAsset;
	protected string message;
	public bool complete;

	public void Awake()
	{
		saveManager.onPreLoad += HandleLoad;
		saveManager.onPreSave += HandleSave;
		if(!saveManager.HasSaveFile()) RegisterCheck();
		if(messageAsset != null) message = messageAsset.text;

		OnAwake();
	}

	public virtual void OnAwake()
	{

	}

	public virtual void RegisterCheck()
	{
		
	}

	public string GetText()
	{
		return message;
	}

	public void Complete()
	{
		complete = true;
		OnComplete();
	}

	public virtual void OnComplete()
	{

	}

    public void HandleCheck()
    {
		if(Check())			
			tutorialOverlayManager.Push(this);
    }
    
	public virtual void HandleSave(SaveFile saveFile)
	{

	}

	public void HandleLoad(SaveFile saveFile)
	{
		Load(saveFile);
		AfterLoad();
	}
	
	public virtual void Load(SaveFile saveFile)
	{
		
	}

	public void AfterLoad()
	{
		if(!complete)
		{
			RegisterCheck();
		}
	}

	public virtual bool Check()
	{
		return false;
	}
}
