﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampTutorial : Tutorial 
{
    public Camp camp;

    public override void RegisterCheck()
    {
        camp.onUnlock += HandleCheck;
    }

    public override void OnComplete()
    {
        camp.onUnlock -= HandleCheck;
    }

    public override bool Check()
    {
        return camp.unlocked;
    }

    public override void HandleSave(SaveFile saveFile)
    {
        if(saveFile == null) return;
        
        saveFile.campTutorialComplete = complete;
    }

    public override void Load(SaveFile saveFile)
    {
        if(saveFile.campTutorialComplete) Complete();
    }	
}