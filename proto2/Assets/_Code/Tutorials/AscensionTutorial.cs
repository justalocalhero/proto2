﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AscensionTutorial : Tutorial
{
    public List<Battleground> battlegrounds;
    public IntReference levelRequirement;

    public override void RegisterCheck()
    {
		foreach(Battleground battleground in battlegrounds)
		{
			battleground.onBattlegroundLevelUp += HandleCheck;
		}
    }

    public override void OnComplete()
    {
		foreach(Battleground battleground in battlegrounds)
		{
			battleground.onBattlegroundLevelUp -= HandleCheck;
		}
    }

    public override bool Check()
    {
		foreach(Battleground battleground in battlegrounds)
		{
			if(battleground.GetMaxLevelReached() > levelRequirement) return true;
		}

		return false;
    }

    public override void HandleSave(SaveFile saveFile)
    {
        if(saveFile == null) return;
        
        saveFile.ascensionTutorialComplete = complete;
    }

    public override void Load(SaveFile saveFile)
    {
        if(saveFile.ascensionTutorialComplete) Complete();
    }	
}
