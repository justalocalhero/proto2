﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleTutorial : Tutorial 
{
    public EquipmentManager equipmentManager;
    
    public override void RegisterCheck()
    {
        equipmentManager.onEquipmentChanged += HandleCheck;
    }

    public override void OnComplete()
    {
        equipmentManager.onEquipmentChanged -= HandleCheck;
    }

    public override bool Check()
    {
        return equipmentManager.GetFilledSlots() >= 4;
    }

    public override void HandleSave(SaveFile saveFile)
    {
        if(saveFile == null) return;
        
        saveFile.battleTutorialComplete = complete;
    }

    public override void Load(SaveFile saveFile)
    {
        if(saveFile.battleTutorialComplete) Complete();
    }	
}