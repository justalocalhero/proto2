﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TownUI : MonoBehaviour 
{
    public Transform slotContainer;
    public Transform buildingContainer;
    public UIToggler toggler;
    public TextMeshProUGUI purchaseCountMesh;
    public TownBuildingUI townBuildingUI;
	private TownSlot[] slots;
    private TownBuilding[] townBuildings;
	private TownSlot selected;
    
	public TabTransitionManager centerTabManager;
	public TabTransitionManager rightTabManager;
    public TabTransitionController lockedCenterTab;
    public TabTransitionController lockedRightTab;

    public void Start()
    {
        slots = slotContainer.GetComponentsInChildren<TownSlot>();
        townBuildings = buildingContainer.GetComponentsInChildren<TownBuilding>();
        SetSlots();

        toggler.onHide += OnHide;
        toggler.onShow += OnShow;
    }

    public void SetSlots()
    {

        for(int i = 0; i < slots.Length; i++)
        {
            slots[i].townUI = this;
            slots[i].townBuildingUI = townBuildingUI;
            if(i < townBuildings.Length)
            {                
               slots[i].gameObject.SetActive(true);
               slots[i].Set(townBuildings[i]);
            }
            else
            {
                slots[i].gameObject.SetActive(false);
            }
        }
    }

    public void UpdatePurchaseMesh()
    {
        int count = 0;

        for(int i = 0; i < slots.Length; i++)
        {
            if(i < townBuildings.Length)
            {
               count += slots[i].GetPurchaseCount();
            }
        }

        purchaseCountMesh.SetText((count > 0) ? "" + count : "");
    }

    public void SelectSlot(TownSlot slot)
    {
        ResetSelected();
        selected = slot;
        selected.Select();
    }

    public void ResetSelected()
    {
        if(selected != null) selected.DeSelect();
    }

    public void OnHide()
    {
        ResetSelected();
        townBuildingUI.Clear();
    }

    public void OnShow()
    {
        slots[0].OnButtonPress();
    }

    public void SetLockedTabs()
    {
        SetCenterTab(lockedCenterTab);
        SetRightTab(lockedRightTab);
    }

    public void SetCenterTab(TabTransitionController toSet)
    {
        centerTabManager.SetTab(toSet);
    }

    public void SetRightTab(TabTransitionController toSet)
    {
        rightTabManager.SetTab(toSet);
    }
}
