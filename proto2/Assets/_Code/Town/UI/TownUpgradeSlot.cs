﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TownUpgradeSlot : MonoBehaviour 
{
	public Button button;
	public TextMeshProUGUI mesh;
	private TownUpgrade upgrade;
	private TownBuilding building;
	public TownUpgradeTooltip townUpgradeTooltip {get; set;}
	private ResourceManager resourceManager;
	public ColoredText coloredText;
	public ColorReference costAffordColor, costNoAffordColor, descriptionColor, nameColor;
	
	public void Set(TownUpgrade newUpgrade, TownBuilding newBuilding)
	{
		if(newBuilding == null) return;
		upgrade = newUpgrade;
		SetBuilding(newBuilding);
		UpdateUI();
	}

	private void SetBuilding(TownBuilding newBuilding)
	{
		if(newBuilding == null) return;
		if(building != null)
		{
			building.onVisibilityChange -= UpdateUI;
		}

		building = newBuilding;

		building.onVisibilityChange += UpdateUI;
	}

	public void SetResourceManager(ResourceManager manager)
	{
		if(manager == null) return;

		resourceManager = manager;
	}

	public Color GetCostColor()
	{
		if(resourceManager == null || upgrade == null) return new Color();
		return (resourceManager.CheckChange(upgrade.GetNextCost())) ? costAffordColor : costNoAffordColor;
	}

	public void OnButtonPress()
	{
		if(building.OnUpgradeButtonPress(upgrade))
		{
			UpdateUI();
		};
	}

	public void OnPointerEnter()
	{

	}

	public void OnPointerExit()
	{

	}

	public void UpdateUI()
	{
		UpdateMesh();
		UpdateState();
	}

	public void UpdateState()
	{
		
	}

	public void UpdateMesh()
	{
		string nameString = coloredText.GetText(upgrade.GetNameString(), nameColor) + "\n";
		string costString = coloredText.GetText(upgrade.GetCostString(), GetCostColor()) + "\n";
		string descriptionString = coloredText.GetText(upgrade.GetDescriptionString(), descriptionColor) + "\n";

		mesh.SetText(nameString + costString + descriptionString);
	}
}
