﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TownBuildingUI : MonoBehaviour 
{
	public Transform unlockTransform;
	private Button unlockButton;
	private TextMeshProUGUI unlockMesh;
	public TextMeshProUGUI labelMesh;

	public Transform upgradeTransform;
	private TownUpgradeSlot[] upgradeSlots;

	private TownBuilding townBuilding;
	private List<TownUpgrade> upgrades;

	public TownTooltip townTooltip;
	public TownUpgradeTooltip townUpgradeTooltip;

	public ResourceManager resourceManager;
	public ColoredText coloredText;
	public ColorReference costAffordColor, costNoAffordColor, descriptionColor, nameColor;

	public Slot leftUpgradeSlot, rightUpgradeSlot;
	private int currentPage = 0;
	private int upgradesPerPage;

	public void Awake()
	{
		leftUpgradeSlot.SetIndex(-1);
		rightUpgradeSlot.SetIndex(1);
		leftUpgradeSlot.onClick += Paginate;
		rightUpgradeSlot.onClick += Paginate;
	}

	public void Start()
	{
		upgradeSlots = upgradeTransform.GetComponentsInChildren<TownUpgradeSlot>();
		unlockButton = unlockTransform.GetComponentInChildren<Button>();
		unlockMesh = unlockTransform.GetComponentInChildren<TextMeshProUGUI>();

		foreach(TownUpgradeSlot slot in upgradeSlots)
		{
			slot.townUpgradeTooltip = townUpgradeTooltip;
			slot.SetResourceManager(resourceManager);
		}

		upgradesPerPage = upgradeSlots.Length;
	}

	public void Set(TownBuilding building)
	{
		if(building == null) 
		{
			Debug.LogWarning("Null townBuilding. Cannot Set.");
			return;
		}
		UpdateDelegates(building);
		townBuilding = building;
		if(!building.unlocked) townTooltip.Set(building);
		labelMesh.SetText(building.GetName());

		UpdateUI();
	}

	public void Clear()
	{
		ClearDelegates();
		townBuilding = null;
		labelMesh.SetText("");
		HideMono(unlockButton);
		HideUpgradeButtons();
		
	}

	public void ClearDelegates()
	{
		if(townBuilding != null)
		{
			townBuilding.onVisibilityChange -= UpdateUI;
			townBuilding.onUpgrade -= UpdateUI;
			townBuilding.onUnlock -= UpdateUI;
		}
	}

	public void UpdateDelegates(TownBuilding building)
	{
		if(building == null) return;

		ClearDelegates();
		building.onVisibilityChange += UpdateUI;
		building.onUpgrade += UpdateUI;
		building.onUnlock += UpdateUI;
	}

	public void UpdateUI()
	{
		UpdateButtons();
	}

	private void UpdateButtons()
	{
		UpdateNavigationButtons();
		UpdateUnlockButton();
		UpdateUpgradeButtons();
	}

	private void UpdateNavigationButtons()
	{
		if(townBuilding == null)
		{
			HideNavigationButtons();
			return;
		}

		int visibleCount = townBuilding.GetVisibleCount();
		int maxPages = (upgradesPerPage <= 0) ? 0 : (visibleCount - 1) / upgradesPerPage;
		currentPage = Mathf.Clamp(currentPage, 0, maxPages);

		leftUpgradeSlot.SetVisible(currentPage > 0);
		rightUpgradeSlot.SetVisible(currentPage < maxPages);


	}

	private void Paginate(int index)
	{
		if(townBuilding == null) return;
		
		currentPage += index;
		UpdateUI();
	}

	private void HideNavigationButtons()
	{
		leftUpgradeSlot.Hide();
		rightUpgradeSlot.Hide();
	}

	private void UpdateUnlockButton()
	{
		if(townBuilding.unlocked) HideMono(unlockButton);
		else 
		{
			UpdateUnlockMesh();
			ShowMono(unlockButton);
		}
	}


	public Color GetCostColor()
	{
		if(resourceManager == null || townBuilding == null) return new Color();
		return (resourceManager.CheckChange(townBuilding.GetUnlockCost())) ? costAffordColor : costNoAffordColor;
	}

	private void UpdateUnlockMesh()
	{
		string nameText = coloredText.GetText("Unlock " + townBuilding.GetName() + "\n", nameColor);
		string unlockText = coloredText.GetText(townBuilding.GetUnlockCostString(), GetCostColor());

		unlockMesh.SetText(nameText + unlockText);
	}

	private void UpdateUpgradeButtons()
	{		
		if(townBuilding.unlocked) ShowUpgradeButtons();
		else HideUpgradeButtons();
	}

	private void ShowUpgradeButtons()
	{
		upgrades = townBuilding.GetVisibleUpgrades();
		int pageIndex = (currentPage >= 0) ? currentPage * upgradesPerPage : 0;

		for(int i = 0; i < upgradeSlots.Length; i++)
		{
			int upgradeIndex = i + pageIndex;
			if(upgradeIndex < upgrades.Count) 
			{
				upgradeSlots[i].Set(upgrades[upgradeIndex], townBuilding);
				ShowMono(upgradeSlots[i]);
			}
			else 
			{
				HideMono(upgradeSlots[i]);
			}
		}
	}

	private void HideUpgradeButtons()
	{
		foreach(TownUpgradeSlot slot in upgradeSlots)
		{
			HideMono(slot);
		}
	}

	private void ShowMono(MonoBehaviour button)
	{
		button.gameObject.SetActive(true);
	}

	private void HideMono(MonoBehaviour button)
	{
		button.gameObject.SetActive(false);
	}

	public void OnUnlockButtonPressed()
	{
		townBuilding.OnUnlockButtonPress();
	}
}
