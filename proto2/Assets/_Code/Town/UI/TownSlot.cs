﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using System;

public class TownSlot : MonoBehaviour 
{
	public Button button;
	public Image buttonImage;
	public TextMeshProUGUI mesh;
	public ColorReference unavailableColor, selectedColor,  highlightColor, unlockedColor, availableTextColor, unavailableTextColor, unlockedTextColor;
	private TownBuilding townBuilding;
	public TownUI townUI{get;set;}
	public TownBuildingUI townBuildingUI{get;set;}
	private bool selected = false;
	private int purchaseCount = 0;

	public void Set(TownBuilding newBuilding)
	{
		if(newBuilding == null) return;
		
		UpdateDelegates(newBuilding);
		townBuilding = newBuilding;
		Hide();
		UpdateUI();
	}

	public void Show()
	{
		if(!gameObject.activeSelf) gameObject.SetActive(true);

	}

	public void Hide()
	{
		if(gameObject.activeSelf) gameObject.SetActive(false);
	}

	public void UpdateDelegates(TownBuilding newBuilding)
	{
		if(townBuilding != null)
		{
			townBuilding.onUnlock -= UpdateUI;
			townBuilding.onUpgrade -= UpdateUI;
			townBuilding.onVisibilityChange -= UpdateUI;
		}
		newBuilding.onUnlock += UpdateUI;
		newBuilding.onUpgrade += UpdateUI;
		newBuilding.onVisibilityChange += UpdateUI;
	}

	public void OnButtonPress()
	{
		townBuildingUI.Set(townBuilding);
		townUI.SelectSlot(this);
		UpdateUI();
	}

	public void UpdateUI()
	{
		UpdatePurchaseCount();
		UpdateButton();
		UpdateMesh();
		UpdateMainWindow();
	}

	public void UpdatePurchaseCount()
	{		
		int newCount = townBuilding.GetPurchaseCount();
		
		if(newCount != purchaseCount)
		{
			purchaseCount = newCount;
			townUI.UpdatePurchaseMesh();
		}
	}
	
	public void UpdateButton()
	{
		if(townBuilding == null) return;

		if(selected)
		{
			buttonImage.color = selectedColor;
			Show();
		}		
		else if(purchaseCount > 0)
		{
			buttonImage.color = highlightColor;
			Show();
		}
		else if(townBuilding.unlocked)
		{
			buttonImage.color = unlockedColor;
			Show();
		}
		else
		{
			buttonImage.color = unavailableColor;
		}
	}

	public void UpdateMesh()
	{
		if(townBuilding == null) return;

		Color textColor;
			
		if(purchaseCount > 0)
		{
			textColor = availableTextColor;
		}
		else if(townBuilding.unlocked)
		{
			textColor = unlockedTextColor;
		}
		else
		{
			textColor = unavailableTextColor;
		}

		string nameText = Tooltips.ColoredText.GetText(townBuilding.GetName(), textColor);

		mesh.SetText(nameText);
	}

	public void UpdateMainWindow()
	{
		if(selected) 
		{
			if(townBuilding.unlocked)
			{				
				townUI.SetCenterTab(townBuilding.centerWindowController);
				townUI.SetRightTab(townBuilding.rightWindowController);
			}
			else
			{
				townUI.SetLockedTabs();
			}
		}
	}

	public void DeSelect()
	{
		selected = false;
		UpdateUI();
	}

	public void Select()
	{
		selected = true;
		UpdateUI();
	}

	public int GetPurchaseCount()
	{
		return purchaseCount;
	}
}
