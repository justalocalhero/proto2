﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TownBuildingGroup : MonoBehaviour 
{
	public Transform childButtonContainer;
	public Button parentButton;

	public void Start()
	{
		Collapse();
	}

	public void Expand()
	{
		childButtonContainer.gameObject.SetActive(true);
		parentButton.gameObject.SetActive(false);
	}

	public void Collapse()
	{
		childButtonContainer.gameObject.SetActive(false);		
		parentButton.gameObject.SetActive(true);
	}
}
