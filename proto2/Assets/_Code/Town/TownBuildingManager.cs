﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownBuildingManager : MonoBehaviour 
{
	public SaveManager saveManager;
	private TownBuilding[] townBuildings;


	void Awake () 
	{
		townBuildings = GetComponentsInChildren<TownBuilding>();
		saveManager.onPreSave += Save;
		saveManager.onLoad += Load;
	}
	
	public void Save(SaveFile saveFile)
	{
		if(saveFile == null) return;

		foreach(TownBuilding building in townBuildings)
		{
			building.Save(saveFile);
		}
	}

	public void Load(SaveFile saveFile)
	{
		if(saveFile == null) return;

		foreach(TownBuilding building in townBuildings)
		{
			building.Load(saveFile);
		}
	}
}
