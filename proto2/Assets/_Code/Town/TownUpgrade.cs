﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

[Serializable]
public class TownUpgrade 
{
	public StringReference name;
	public string description{get; set;}
	public Resource[] cost;
	public int level{get;set;}
	
	[SerializeField]
	public UpgradeEvent action = new UpgradeEvent();

	[SerializeField]
	public UpgradeEvent setDescription = new UpgradeEvent();

	[SerializeField]
	public UpgradeEvent locked = new UpgradeEvent();

	[SerializeField]
	public UpgradeEvent unlocked = new UpgradeEvent();

	public TownUpgrade(ITownUpgrade upgrade)
	{
		this.action.AddListener(upgrade.Upgrade);
		this.setDescription.AddListener(upgrade.Description);

		this.name = upgrade.name;
		this.cost = upgrade.GetCost().ToArray();
	}

	public Resource GetCost(int level)
	{
		if(level >= cost.Length) return new Resource{};

		return cost[level];
	}

	public string GetNameString()
	{
		return name;
	}

	public string GetCostString()
	{
		if(level >= cost.Length) return "";

		string costString = "";
		Resource nextCost = GetNextCost();

		costString = ((-nextCost.value) + " " + nextCost.type + "\n");

		return costString;
	}

	public string GetDescriptionString()
	{
		return description;
	}

	public Resource GetNextCost()
	{
		return GetCost(level);
	}
};

[Serializable]
public class UpgradeEvent : UnityEvent<TownUpgrade>{};

