﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UnlockerUpgradeConfig : IntUpgradeConfig 
{
	public MonoBehaviour unlockerMono;
	private IUnlocker unlocker;

	public override void Init()
	{
		unlocker = unlockerMono as IUnlocker;

		if(unlocker == null)
		{
			Debug.LogWarning("unlockerMono did not contain IUnlocker");
		}
	}

	public override void OnUnlock()
	{
		value.Value = AtLevel(0);
		unlocker.SetLevel(value.Value);
	}

	public override void Upgrade(TownUpgrade upgrade)
	{
		value.Value = AtLevel(upgrade.level);
		unlocker.SetLevel(value.Value);
		upgrade.setDescription.Invoke(upgrade);
	}

	public override void Description(TownUpgrade upgrade)
	{
		upgrade.description = 
			descriptionPrefix
			+ "\n" + unlocker.UnlockDescription(AtLevel(upgrade.level));
	}
}
