﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ClassUpgradeConfig : TownUpgradeConfig
{
	[Space(10)]
	public List<RaceClass> classes;
	public ClassManager classManager;

	public override void Upgrade(TownUpgrade upgrade)
	{
		if(upgrade.level > 0) classManager.Add(classes[upgrade.level - 1]);
		upgrade.setDescription.Invoke(upgrade);
	}

	public override void Description(TownUpgrade upgrade)
	{
		if(upgrade.level >= classes.Count) 
		{
			upgrade.description = "";
		}
		else
		{
			upgrade.description = 
				"Unlocks " + classes[upgrade.level].GetName();
		}
	}
}

