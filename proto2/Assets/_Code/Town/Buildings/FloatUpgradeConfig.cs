﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FloatUpgradeConfig : TownUpgradeConfig 
{
	[Space(10)]
	public FloatVariable value;
	public FloatReference valueLocked, valueBase, valuePerLevel;

	[Space(10)]
	public StringReference descriptionPrefix;
	public StringReference descriptionSuffix;
	public DescriptionValueFormat format;

	public override void Upgrade(TownUpgrade upgrade)
	{
		value.Value = AtLevel(upgrade.level);
		upgrade.setDescription.Invoke(upgrade);
	}

	public override void Description(TownUpgrade upgrade)
	{
		upgrade.description = 
			descriptionPrefix
			+ " from " + FormatValue(AtLevel(upgrade.level), format)
			+ " to " + FormatValue(AtLevel(upgrade.level + 1), format)
			+ " " + descriptionSuffix;
	}

	public override void Locked()
	{
		value.Value = valueLocked;
	}

	public override void OnUnlock()
	{
		value.Value = AtLevel(0);
	}

	public float AtLevel(int level)
	{
		return valueBase + valuePerLevel * level;
	}

	public string FormatValue(float value, DescriptionValueFormat format)
	{
		switch(format)
		{
			case DescriptionValueFormat.flat:
				return Utility.Truncate(value, 2).ToString();
			case DescriptionValueFormat.percent:
				return Utility.ToPercent(value);
			case DescriptionValueFormat.mult:
				return Utility.Truncate(value, 2) + "x";
			case DescriptionValueFormat.seconds:
				return Utility.Truncate(value, 2).ToString() + "s";
		}

		return "";
	}

	public enum DescriptionValueFormat {percent, mult, flat, seconds}
}
