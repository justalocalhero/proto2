﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armory : TownBuilding 
{
    [Space(10)]
    public FloatUpgradeConfig designDiscovery;

	[Space(10)]
	public FloatUpgradeConfig materialDiscovery;

	[Space(10)]
	public FloatUpgradeConfig enchantmentDiscovery;

    [Space(10)]
	public FloatUpgradeConfig craftingDowngrade;

    [Space(10)]
	public FloatUpgradeConfig craftingUpgrade;

    protected override void OnAwake()
    {        
        AddUpgrade(designDiscovery);
		AddUpgrade(materialDiscovery);
        AddUpgrade(enchantmentDiscovery);
		AddUpgrade(craftingDowngrade);
		AddUpgrade(craftingUpgrade);
    }
}
