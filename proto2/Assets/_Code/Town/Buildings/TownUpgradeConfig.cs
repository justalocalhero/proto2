﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownUpgradeConfig : ITownUpgrade 
{

	public StringReference _name;
	public StringReference name 
	{ 
		get { return _name; }
		set { _name = value; }
	}

	[Space(10)]
	public CostStrategy costStrategy;
	public IntReference upgradeLevels, minCost, maxCost;
	public List<Resource> cost { get; set; }

	public List<Resource> GetCost()
	{
		return costStrategy.GetCost(ResourceType.scrap, minCost, maxCost, upgradeLevels);
	}

	public virtual void Upgrade(TownUpgrade upgrade)
	{

	}

	public virtual void Description(TownUpgrade upgrade)
	{
		
	}

	public virtual void Locked()
	{

	}

	public virtual void OnUnlock()
	{

	}

	public virtual void Init()
	{

	}
	
	public void RegisterTownBuilding(TownBuilding townBuilding)
	{
		townBuilding.onUnlock += OnUnlock;
		townBuilding.onLock += Locked;
	}
}
