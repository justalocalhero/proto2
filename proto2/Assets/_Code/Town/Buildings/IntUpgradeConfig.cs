﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class IntUpgradeConfig : TownUpgradeConfig 
{
	[Space(10)]
	public IntVariable value;
	public IntReference valueLocked, valueBase, valuePerLevel;

	[Space(10)]
	public StringReference descriptionPrefix;
	public StringReference descriptionSuffix;

	public override void Upgrade(TownUpgrade upgrade)
	{
		value.Value = AtLevel(upgrade.level);
		upgrade.setDescription.Invoke(upgrade);
	}

	public override void Description(TownUpgrade upgrade)
	{
		upgrade.description = 
			descriptionPrefix
			+ " from " + FormatValue(AtLevel(upgrade.level))
			+ " to " + FormatValue(AtLevel(upgrade.level + 1))
			+ descriptionSuffix;
	}

	public override void Locked()
	{
		value.Value = valueLocked;
	}

	public override void OnUnlock()
	{
		value.Value = AtLevel(0);
	}

	public int AtLevel(int level)
	{
		return valueBase + valuePerLevel * level;
	}

	public string FormatValue(int value)
	{
		return value.ToString();
	}
}
