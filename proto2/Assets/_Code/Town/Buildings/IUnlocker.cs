﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUnlocker 
{
	string UnlockDescription(int level);
	void SetLevel(int level);
	bool CanSelect();
}
