﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITownUpgrade
{
	StringReference name {get; set;}

	void Upgrade(TownUpgrade upgrade);
	void Description(TownUpgrade upgrade);

	List<Resource> GetCost();

}
