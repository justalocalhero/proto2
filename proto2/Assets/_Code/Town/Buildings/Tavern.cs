﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tavern : TownBuilding
{
    [Space(10)]
    public IntUpgradeConfig minLevel;

    [Space(10)]
    public IntUpgradeConfig maxLevel;

    [Space(10)]
    public ClassUpgradeConfig races;

    [Space(10)]
    public FloatUpgradeConfig weaponChance;

    [Space(10)]
    public FloatUpgradeConfig abilityChance;
    
    protected override void OnAwake()
    {
        AddUpgrade(minLevel);
        AddUpgrade(maxLevel);
        AddUpgrade(races);
        AddUpgrade(weaponChance);
        AddUpgrade(abilityChance);
    }
}
