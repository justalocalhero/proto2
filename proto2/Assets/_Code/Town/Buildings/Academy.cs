﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Academy : TownBuilding 
{
	[Space(10)]
	public ClassUpgradeConfig physClasses;

	[Space(10)]
	public ClassUpgradeConfig magicClasses;

	[Space(10)]
	public ClassUpgradeConfig techClasses;

	protected override void OnAwake()
	{
		AddUpgrade(physClasses);
		AddUpgrade(magicClasses);
		AddUpgrade(techClasses);
	}
}
