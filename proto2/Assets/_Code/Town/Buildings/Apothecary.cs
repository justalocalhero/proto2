﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apothecary : TownBuilding
{


	[Space(10)]
	public FloatUpgradeConfig baseHealing;

	[Space(10)]
	public UnlockerUpgradeConfig supplyUnlock;

	[Space(10)]
	public IntUpgradeConfig supplyMax;

	[Space(10)]
	public IntUpgradeConfig potionCount;

	protected override void OnAwake()
	{
		AddUpgrade(baseHealing);
		AddUpgrade(supplyUnlock);
		AddUpgrade(supplyMax);
		AddUpgrade(potionCount);
	}
}
