﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Altar : TownBuilding 
{
	[Space(10)]
	public FloatUpgradeConfig failChance;

	protected override void OnAwake()
	{
		AddUpgrade(failChance);
	}
}
