﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camp : TownBuilding
{
	[Space(10)]
	public ClassUpgradeConfig occupations;

	[Space(10)]
	public FloatUpgradeConfig lootTime;

	[Space(10)]
	public FloatUpgradeConfig campHealing;

	[Space(10)]
	public FloatUpgradeConfig campBuffDuration;

	[Space(10)]
	public UnlockerUpgradeConfig supplyUnlock;

	[Space(10)]
	public IntUpgradeConfig supplyMax;

	protected override void OnAwake()
	{
		AddUpgrade(occupations);
		AddUpgrade(lootTime);
		AddUpgrade(campHealing);
		AddUpgrade(campBuffDuration);
		AddUpgrade(supplyUnlock);
		AddUpgrade(supplyMax);
	}
}
