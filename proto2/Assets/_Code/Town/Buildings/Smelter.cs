﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smelter : TownBuilding 
{

    [Space(10)]
    public IntUpgradeConfig hammerCount;

    [Space(10)]
    public FloatUpgradeConfig scrapMultiplier;

    [Space(10)]
    public FloatUpgradeConfig junkValue;

    protected override void OnAwake()
    {
        AddUpgrade(hammerCount);
        AddUpgrade(scrapMultiplier);
        AddUpgrade(junkValue);
    }
}
