﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ReputationUpgradeConfig : IntUpgradeConfig 
{
	public ReputationBuildingManager reputationBuildingManager;

	public override void OnUnlock()
	{
		value.Value = AtLevel(0);
		reputationBuildingManager.SetLevel(value.Value);
	}

	public override void Upgrade(TownUpgrade upgrade)
	{
		value.Value = AtLevel(upgrade.level);
		reputationBuildingManager.SetLevel(value.Value);
		upgrade.setDescription.Invoke(upgrade);
	}

	public override void Description(TownUpgrade upgrade)
	{
		ReputationBuilding reputationBuilding = reputationBuildingManager.GetBuilding(AtLevel(upgrade.level));
		if(reputationBuilding == null) return;

		upgrade.description = 
			descriptionPrefix
			+ "\n" + reputationBuildingManager.GetBuilding(AtLevel(upgrade.level)).GetDescription();
	}
}
