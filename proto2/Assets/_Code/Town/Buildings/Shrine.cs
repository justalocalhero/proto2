﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shrine : TownBuilding 
{
    [Space(10)]
    public ReputationUpgradeConfig reputation;

    [Space(10)]
    public IntUpgradeConfig offerings;

    [Space(10)]
    public FloatUpgradeConfig rebirth;

    protected override void OnAwake()
    {
        AddUpgrade(reputation);
        AddUpgrade(offerings);
        AddUpgrade(rebirth);
    }
}
