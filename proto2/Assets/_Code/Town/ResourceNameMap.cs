﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Crafting/ResourceNameMap")]
public class ResourceNameMap : ScriptableObject 
{
	public List<ResourceName> resourceNames = new List<ResourceName>();

	public string GetName(ResourceType type)
	{
		int index = (int) type;

		if(index < 0 || index >= resourceNames.Count)
		{
			Debug.LogWarning("Resource Name Not Found.");

			return "";
		}

		ResourceName current = resourceNames[index];

		if(current.type != type)
		{
			Debug.LogWarning("Resource Type Mismatch. Expected: " + type + " Expected: " + current.type);

			return "";
		}

		return current.name;

	}

	public Sprite GetSprite(ResourceType type)
	{
		int index = (int) type;

		if(index < 0 || index >= resourceNames.Count)
		{
			Debug.LogWarning("Resource Name Not Found.");

			return null;
		}

		ResourceName current = resourceNames[index];

		if(current.type != type)
		{
			Debug.LogWarning("Resource Type Mismatch. Expected: " + type + " Expected: " + current.type);

			return null;
		}

		return current.sprite;

	}
}

[System.Serializable]
public struct ResourceName { public ResourceType type; public string name; public Sprite sprite;}
