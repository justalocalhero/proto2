﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class TownBuilding : MonoBehaviour 
{
	[System.Serializable]
	public class BuildingSave
	{
		public bool unlocked;
		public List<int> upgradeLevels;
	}

	public bool unlocked{get; set;}

	public ResourceManager resources;

	new public StringReference name;
	public StringReference description;

	public TabTransitionController centerWindowController;
	public TabTransitionController rightWindowController;

	public List<Resource> unlockCosts;

	public delegate void OnUnlock();
	public OnUnlock onUnlock;

	public delegate void OnLock();
	public OnLock onLock;

	public delegate void OnUpgrade();
	public OnUpgrade onUpgrade;

	public delegate void OnVisibilityChange();
	public OnVisibilityChange onVisibilityChange;

	public List<TownUpgrade> upgrades;

	private int purchaseCount = 0;
	private int visibleCount = 0;

	public void Awake()
	{
		resources.onResourceChanged += UpdateVisibility;

		OnAwake();
	}

	public void Start()
	{
		foreach(TownUpgrade upgrade in upgrades)
		{
			upgrade.setDescription.Invoke(upgrade);
		}
		
		UpdateVisibility();

		if(unlockCosts.Count == 0) unlocked = true;

		OnStart();
	}

	public void Save(SaveFile saveFile)
	{
		if(saveFile == null) return;
		if(saveFile.buildingSaves == null) return;

		List<int> levels = new List<int>();
		
		foreach(TownUpgrade upgrade in upgrades)
		{
			levels.Add(upgrade.level);
		}

		saveFile.buildingSaves[GetSaveKey()] = new BuildingSave()
		{
			unlocked = this.unlocked,
			upgradeLevels = levels
		};		
	}

	public void Load(SaveFile saveFile)
	{
		if(saveFile == null) return;
		if(saveFile.buildingSaves == null) return;
		if(!saveFile.buildingSaves.ContainsKey(GetSaveKey())) return;
		
		BuildingSave tempSave = saveFile.buildingSaves[GetSaveKey()];

		if(tempSave == null) return;
		
		if(tempSave.unlocked) Unlock();

		for(int i = 0; i < upgrades.Count; i++)
		{
			if(i < tempSave.upgradeLevels.Count) 
				SetUpgradeLevel(upgrades[i], tempSave.upgradeLevels[i]);
		}

		if(!tempSave.unlocked)
			Lock();
	}
	

	protected virtual void OnAwake()
	{

	}


	protected virtual void OnStart()
	{

	}

	private void AddUpgrade(TownUpgrade upgrade)
	{
		upgrade.setDescription.Invoke(upgrade);
		if(unlocked) upgrade.action.Invoke(upgrade);
		upgrades.Add(upgrade);
	}

	public void AddUpgrade(TownUpgradeConfig config)
	{
		config.Init();
		if(unlocked) config.OnUnlock();
		else config.Locked();

		config.RegisterTownBuilding(this);
		AddUpgrade(new TownUpgrade(config));
	}

	public string GetName()
	{
		return name;
	}

	public int GetMaxUnlockType()
	{
		int toReturn = -1;

		foreach(Resource cost in unlockCosts)
		{
			if((int)cost.type > toReturn) toReturn = (int)cost.type;
		}

		return toReturn;
	}

	public int GetMaxUnlockCost()
	{
		int order = -1;
		int toReturn = -1;

		foreach(Resource cost in unlockCosts)
		{
			int costType = (int) cost.type;
			if((int)cost.type > order) 
			{
				order = (int)cost.type;
				toReturn = -cost.value;
			}
			else if(order == costType) 
			{
				if(-cost.value > toReturn) toReturn = -cost.value;
			}
		}

		return toReturn;
	}

	public virtual string GetDescription()
	{
		return description;
	}

	public string GetUnlockCostString()
	{
		if(unlocked) return "";

		string costString = "";
		List<Resource> costs = GetUnlockCost();
		for(int i = 0; i < costs.Count; i++)
		{
			costString += (costs[i].type + " " + (-costs[i].value) + "\n");
		}

		return costString;
	}

	public List<Resource> GetUnlockCost()
	{
		return unlockCosts;
	}

	public Resource GetUpgradeCost(int upgradeIndex)
	{
		if(UpgradeIndexOutOfBounds(upgradeIndex)) return new Resource{};
		
		TownUpgrade current = upgrades[upgradeIndex];

		return GetUpgradeCost(current);			
	}

	public Resource GetUpgradeCost(TownUpgrade upgrade)
	{
		return upgrade.GetNextCost();	
	}

	public bool UpgradeIndexOutOfBounds(int upgradeIndex)
	{
		return (upgradeIndex < 0 || upgradeIndex >= upgrades.Count);
	}

	public bool OnUnlockButtonPress()
	{
		if(unlocked) return false;

		List<Resource> costs = GetUnlockCost();
		
		if(resources.CheckChange(costs))
		{

			resources.ApplyChange(costs);
			Unlock();
			return true;
		}
		return false;
	}

	public bool CheckUnlockCost()
	{
		return resources.CheckChange(GetUnlockCost());
	}

	public bool CheckVisible(TownUpgrade upgrade)
	{
		if(upgrade == null) return false;
		if(upgrade.level >= upgrade.cost.Length) return false;
		return resources.CheckVisible(GetUpgradeCost(upgrade));
	}

	public bool CheckPurchase(TownUpgrade upgrade)
	{
		if(upgrade == null) return false;
		if(upgrade.level >= upgrade.cost.Length) return false;
		return resources.CheckChange(GetUpgradeCost(upgrade));
	}

	public bool OnUpgradeButtonPress(TownUpgrade upgrade)
	{
		Resource cost = GetUpgradeCost(upgrade);

		if(resources.CheckChange(cost))
		{
			resources.ApplyChange(cost);
			SetUpgradeLevel(upgrade, upgrade.level + 1);
			return true;
		}

		return false;
	}

	public void SetUpgradeLevel(TownUpgrade upgrade, int level)
	{
		
		while(level > upgrade.level)
		{
			upgrade.level++;

			if(upgrade.action != null) upgrade.action.Invoke(upgrade);
		}
		
		if(upgrade.action != null) upgrade.action.Invoke(upgrade);

		UpdateVisibility();
		RaiseOnUpgrade();
	}

	public bool CheckUpgradeCost(TownUpgrade upgrade)
	{
		Resource cost = GetUpgradeCost(upgrade);
		return resources.CheckChange(cost);
	}

	public void Unlock()
	{
		unlocked = true;
		RaiseOnUnlock();
		
		UpdateVisibility();
	}

	public void Lock()
	{
		unlocked = false;
		RaiseOnLock();

		UpdateVisibility();
		
	}

	public void RaiseOnUnlock()
	{
		if(onUnlock != null) onUnlock();
	}

	public void RaiseOnLock()
	{
		if(onLock != null) onLock();
	}

	public void RaiseOnUpgrade()
	{
		if(onUpgrade != null) onUpgrade();
	}

	public void RaiseOnVisibilityChange()
	{
		if(onVisibilityChange != null) onVisibilityChange();
	}

	public string GetUpgradeDescription(int index)
	{
		if(UpgradeIndexOutOfBounds(index)) return "";
		else return upgrades[index].description;
	}

	public List<TownUpgrade> GetVisibleUpgrades()
	{
		List<TownUpgrade> toReturn = new List<TownUpgrade>();

		foreach(TownUpgrade upgrade in upgrades)
		{
			if(CheckVisible(upgrade)) toReturn.Add(upgrade);
		}

		return toReturn;
	}

	public bool HasAnyPurchase()
	{
		if(!unlocked && CheckUnlockCost()) return true;

		foreach(TownUpgrade upgrade in upgrades)
		{
			if(resources.CheckChange(GetUpgradeCost(upgrade))) return true;
		}

		return false;
	}

	private void UpdateVisibility()
	{
		UpdateVisibleCount();
		UpdatePurchaseCount();
	}

	private void UpdateVisibleCount()
	{
		int newCount = CalculateVisibleCount();
		if(newCount != visibleCount)
		{
			visibleCount = newCount;
			RaiseOnVisibilityChange();
		}
	}

	private void UpdatePurchaseCount()
	{
		int newCount = CalculatePurchaseCount();
		if(newCount != purchaseCount)
		{
			purchaseCount = newCount;
			RaiseOnVisibilityChange();
		}
	}

	private int CalculatePurchaseCount()
	{
		if(!unlocked)
		{
			return (resources.CheckChange(GetUnlockCost())) ? 1 : 0;
		}

		int count = 0;

		foreach(TownUpgrade upgrade in upgrades)
		{
			if(CheckPurchase(upgrade)) count++;
		}

		return count;
	}

	private int CalculateVisibleCount()
	{		
		if(!unlocked)
		{
			return (resources.CheckChange(GetUnlockCost())) ? 1 : 0;
		}

		int count = 0;

		foreach(TownUpgrade upgrade in upgrades)
		{
			if(CheckVisible(upgrade)) count++;
		}

		return count;
	}

	public int GetPurchaseCount()
	{
		return purchaseCount;
	}

	public int GetVisibleCount()
	{
		return visibleCount;
	}

	public string GetSaveKey()
	{
		return GetName();
	}
}

