﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(ResourceName))]
public class ResourceNameDrawer : PropertyDrawer
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int typeSize = 80;
        int nameSize = 80;
        int spriteSize = 80;
        int spacing = 5;
        int typeIndent = 0;
        int nameIndent = typeIndent + typeSize + spacing;
        int spriteIndent = nameIndent + nameSize + spacing;

        Rect resourceRect = new Rect(position.x + typeIndent, position.y, typeSize, position.height);
        Rect nameRect = new Rect(position.x + nameIndent, position.y, nameSize, position.height);
        Rect spriteRect = new Rect(position.x + spriteIndent, position.y, spriteSize, position.height);

        EditorGUI.PropertyField(resourceRect, property.FindPropertyRelative("type"), GUIContent.none);
        EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("name"), GUIContent.none);
        EditorGUI.PropertyField(spriteRect, property.FindPropertyRelative("sprite"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
	}
}
