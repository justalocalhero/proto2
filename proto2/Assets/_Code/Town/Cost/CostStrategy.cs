﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Cost/CostStrategy")]
public class CostStrategy : ScriptableObject
{
	public EasingFunction.Ease ease;
	private EasingFunction.Function easing;

	private int[] roundValues;

	public void OnEnable()
	{	
		easing = EasingFunction.GetEasingFunction(ease);
		roundValues = new int[6] {5, 10, 50, 100, 500, 1000};
	}

	public List<Resource> GetCost(ResourceType resourceType, float min, float max, int steps)
	{
		List<Resource> toReturn = new List<Resource>();

		List<int> values = ExceedingRound(Values(min, max, steps));

		foreach(int currentValue in values)
		{
			toReturn.Add(new Resource 
			{
				type = resourceType,
				value = -currentValue
			});
		}

		return toReturn;
	}

	public void Test(float min, float max, int steps)
	{
		List<float> raw = Values(min, max, steps);
		List<int> rounded = ExceedingRound(raw);

		string toPrint = "";

		for(int i = 0; i < raw.Count; i++)
		{
			toPrint += "\nRaw: " + raw[i] + " Rounded: " + rounded[i];
		}

		Debug.Log(toPrint);
	}

	public List<float> Values(float min, float max, int steps)
	{
		List<float> toReturn = new List<float>();
		float perStep = (steps - 1 > 0) ? 1.0f / (steps - 1.0f) : 0.0f;

		for(int i = 0; i < steps; i++)
		{
			float progression = i * perStep;

			toReturn.Add(easing(min, max, progression));
		}

		return toReturn;

	}

	private List<int> ExceedingRound(List<float> values)
	{
		List<int> toReturn = new List<int>();

		for(int i = 0; i < values.Count; i++)
		{
			int roundTo = RoundValue(values[i]);
			int roundedValue = Utility.RoundToNearest(values[i], roundTo);
			int exceedingValue = (i > 0) ?  Exceed(roundedValue, toReturn[i - 1], roundTo) : roundedValue;

			toReturn.Add(exceedingValue);
		}

		return toReturn;
	}

	private int RoundValue(float value)
	{
		int roundTo = 1;

		foreach(int roundValue in roundValues)
		{
			if(value >= 2 * roundValue && roundValue > roundTo) roundTo = roundValue;
		}

		return roundTo;
	}

	private int Exceed(int value, int toExceed, int roundTo)
	{
		if(value > toExceed) return value;

		int dif = Utility.CeilToNearest(toExceed - value + 1, roundTo);

		return value + dif;
	}
}
