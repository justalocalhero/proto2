﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Loot/RebirthItem")]
public class RebirthEquipment : ScriptableObject
{	
	public SpriteManager spriteManager;
	public StringReference itemName;
	public Sprite sprite;
	public int spriteIndex = -1;
	public EquipSlot equipSlot;
	public IntReference statCount;
	public FloatReference statPercent;
	public StatEquivalencies statEquivalencies;
	private EquipmentBuilder builder;

	
	public void OnEnable()
	{
		ValidateSprite();
	}

	public void SetSpriteIndex()
	{
		if(sprite == null) return;
		if(spriteManager == null) return;
		spriteIndex = spriteManager.GetIndex(sprite);
		if(spriteIndex == -1)
			spriteIndex = spriteManager.AddSprite(sprite);
		if(spriteIndex == -1)
			Debug.LogWarning("Panic");
	}

	public void OnValidate()
	{
		ValidateSprite();
	}

	public void ValidateSprite()
	{
		if(sprite == null) return;
		if(spriteManager == null) return;

		if(sprite == spriteManager.GetSprite(spriteIndex)) 
			return;
		else 
			SetSpriteIndex();
	}
	
	public Equipment Generate(Unit unit)
	{
		if(builder == null) builder = new EquipmentBuilder();
		
		Equipment equipment =
			builder.Begin()
			.WithName(itemName)
			.WithSlot(equipSlot, 1)
			.WithSpriteManager(spriteManager)
			.WithSpriteIndex(spriteIndex)
			.WithRarity(Rarity.artifact)
			.WithLevel(99)
			.Build();
			
		List<EquipStat> stats = GenerateStats(unit);
		equipment.AddMagicStats(stats);

		Ability ability = GenerateAbility(unit);
		if(ability != null) equipment.AddAbility(ability);

		return equipment;
	}

	private List<EquipStat> GenerateStats(Unit unit)
	{

		StatManager statManager = unit.GetStats();
		List<EquipStat> stats = statManager.GetFactorOrderedStats(statEquivalencies, statPercent);

		return stats.GetRange(0, Mathf.Clamp(statCount, 0, stats.Count - 1));
	}

	private Ability GenerateAbility(Unit unit)
	{
		return (unit.actions == null || unit.actions.Count <= 0)? null : (Ability) Utility.RandomFromList(unit.actions).Clone();
	}
	
}
