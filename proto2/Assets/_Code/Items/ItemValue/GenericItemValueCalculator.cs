﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Loot;

[CreateAssetMenu(menuName = "Generators/GenericItemValueCalculator")]
public class GenericItemValueCalculator : ScriptableObject 
{
	public FloatReference baseValue, enchantmentFactor, divinityFactor, qualityFactor, materialFactor;

	public int GetValue(Equipment equipment)
	{
		if(equipment == null) return 0;

		float value = CalculateValue(equipment);
		float clampedValue = Mathf.Clamp(value, 1, value);

		return Mathf.CeilToInt(clampedValue);
	}

	private float CalculateValue(Equipment equipment)
	{
		if(equipment == null) return 0;

		float qualityValue = CalculateValue(equipment.quality);
		float enchantmentValue = CalculateValue(equipment.enchantment);
		float materialValue = CalculateValue(equipment.material);
		float divinityValue = CalculateValue(equipment.divinity);

		return (baseValue + qualityValue + enchantmentValue + materialValue + divinityValue);
	}

	private float CalculateValue(Enchantment enchantment)
	{
		if(enchantment == null) return 0;

		int level = enchantment.currentLevel;

		return level * enchantmentFactor;
	}

	private float CalculateValue(Divinity divinity)
	{
		if(divinity == null) return 0;

		int level = divinity.currentLevel;

		return level * divinityFactor;
	}

	private float CalculateValue(Quality quality)
	{
		if(quality == null) return 0;

		int level = quality.currentLevel;

		return level * qualityFactor;
	}

	private float CalculateValue(Loot.Material material)
	{
		if(material == null) return 0;

		int level = (material == null) ? 0 : 1;

		return level * materialFactor;
	}
}
