﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Generators/ItemValueCalculator")]
public class ItemValueCalculator : ScriptableObject 
{
	public FloatReference statFactor, damageFactor, cooldownComparison, triggerCooldown;
	public StatEquivalencies statEQ;

	public int GetValue(Equipment equipment)
	{
		if(equipment == null) return 0;

		float value = CalculateValue(equipment);
		float clampedValue = Mathf.Clamp(value, 1, value);

		return Mathf.CeilToInt(clampedValue);
	}

	private float CalculateValue(Equipment equipment)
	{
		if(equipment == null) return 0;

		float statMultiplier = (equipment.slotMultiplier <= 0) ? 1 : 1 / equipment.slotMultiplier;
		float baseStatValue = statMultiplier * GetValue(equipment.GetBaseStats());
		float magicStatValue = statMultiplier * GetValue(equipment.GetMagicStats());
		float abilityValue = GetValue(equipment.abilities);
		float triggerValue = GetValue(equipment.triggeredActions);

		return (baseStatValue + magicStatValue + abilityValue + triggerValue);
	}

	private float GetValue(EquipStat[] stats)
	{
		if(stats == null || stats.Length == 0) return 0;

		float toReturn = 0;

		foreach(EquipStat stat in stats)
		{
			toReturn += GetValue(stat);
		}

		return toReturn;
	}

	private float GetValue(List<EquipStat> stats)
	{
		if(stats == null || stats.Count == 0) return 0;

		float toReturn = 0;

		foreach(EquipStat stat in stats)
		{
			toReturn += GetValue(stat);
		}

		return toReturn;
	}

	private float GetValue(EquipStat stat)
	{
		float detrimentalFactor = (stat.StatType == StatType.Weight) ? -1 : 1;
		float value = stat.Value * statEQ.GetInverseFactor(stat.StatType, stat.ModifierType) * statFactor;

		return detrimentalFactor * value;
	}

	public float GetValue(List<Ability> abilities)
	{
		if(abilities == null || abilities.Count == 0) return 0;

		float toReturn = 0;

		foreach(Ability ability in abilities)
		{
			toReturn += GetValue(ability);
		}

		return toReturn;
	}

	public float GetValue(List<TriggeredAction> triggers)
	{
		if(triggers == null || triggers.Count == 0) return 0;

		float toReturn = 0;

		foreach(TriggeredAction trigger in triggers)
		{
			toReturn += GetValue(trigger);
		}

		return toReturn;
	}

	public float GetValue(Ability ability)
	{
		if(ability == null) return 0;

		float cooldownValue = (ability.duration <=  0) ? 1 : cooldownComparison / ability.duration;
		float actionValue = (ability.action == null) ? 0 : GetValue(ability.action, ability.duration);

		return (cooldownValue * actionValue);
	}

	public float GetValue(TriggeredAction trigger)
	{
		if(trigger == null) return 0;

		float cooldownValue = (triggerCooldown <=  0) ? 1 : cooldownComparison / triggerCooldown;
		float actionValue = (trigger.action == null) ? 0 : GetValue(trigger.action, triggerCooldown);

		return (cooldownValue * actionValue);
	}

	public float GetValue(Action action, float cooldownValue)
	{
		if(action == null) return 0;

		float damageValue = GetValue(action.damages);
		float selfDamageValue = -GetValue(action.selfDamages);
		float buffValue = GetValue(action.buffs, Tooltips.EffectType.buff, cooldownValue);
		float debuffValue = GetValue(action.debuffs, Tooltips.EffectType.debuff, cooldownValue);

		return (damageValue + selfDamageValue + buffValue + debuffValue);
	}

	public float GetValue(TimedEffect[] effects, Tooltips.EffectType type, float cooldownValue)
	{
		if(effects == null || effects.Length == 0) return 0;

		float toReturn = 0;

		foreach(TimedEffect effect in effects)
		{
			toReturn += GetValue(effect, type, cooldownValue);
		}

		return toReturn;
	}

	public float GetValue(TimedEffect effect, Tooltips.EffectType type, float cooldownValue)
	{
		if(effect == null) return 0;

		float statSign = (type == Tooltips.EffectType.buff) ? 1 : -1;
		float damageSign = (type == Tooltips.EffectType.buff) ? -1 : 1;
		float durationValue = (effect.duration <=  0) ? 1 : cooldownValue / effect.duration;
		float statValue = GetValue(effect.stats);

		float damageValue = 0;
		damageValue += GetValue(effect.finalDamages);
		damageValue += GetPeriodicDamageValue(effect);



		return (durationValue * (statSign * statValue + damageSign * damageValue));
	}

	public float GetPeriodicDamageValue(TimedEffect effect)
	{
		if(effect == null) return 0;

		int ticks = (effect.period <= 0) ? 0 :  Mathf.FloorToInt(effect.duration / effect.period);

		float damageValue = GetValue(effect.periodicDamages);

		return damageValue * ticks;

	}

	public float GetValue(Damage[] damages)
	{
		if(damages == null || damages.Length == 0) return 0;

		float toReturn = 0;

		foreach(Damage damage in damages)
		{
			toReturn += GetValue(damage);
		}

		return toReturn;
	}

	public float GetValue(Damage damage)
	{
		float factor = (damage.type == DamageType.healing) ? -1 : 1;
		float value = damage.value * damageFactor * factor;

		return value;
	}
}
