﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Loot;

[System.Serializable]
public class Equipment : Item 
{
    public StatManager baseStats = new StatManager();
    public StatManager magicStats = new StatManager();
    public List<Tag> tags = new List<Tag>();
    public List<Ability> abilities = new List<Ability>();
    public List<TriggeredAction> triggeredActions = new List<TriggeredAction>();
    public Rarity rarity;
    public Design design;
    public Quality quality;
    public Divinity divinity;
    public Enchantment enchantment;
    public Loot.Material material;
    public float slotMultiplier;
    public bool enchanted;
    public bool blessed;
    public List<Resource> dismantleResources = new List<Resource>();

    public Equipment()
    {

    }

    public List<EquipStat> GetBaseStats()
    {
        return baseStats.GetNonZeroStats();
    }

    public void AddBaseStats(List<EquipStat> stats)
    {
        baseStats.AdjustStats(stats);
    }

    public List<EquipStat> GetMagicStats()
    {
        return magicStats.GetNonZeroStats();
    }

    public void AddMagicStats(List<EquipStat> stats)
    {
        magicStats.AdjustStats(stats);
    }

    public void AddAbility(Ability ability)
    {
        abilities.Add(ability);
    }

    public void AddTrigger(TriggeredAction triggeredAction)
    {
        triggeredActions.Add(triggeredAction);
    }

    public void AdjustBaseStats(List<EquipStat> stats)
    {
        AdjustBaseStats(stats, 1);
    }

    public void AdjustBaseStats(List<EquipStat> stats, float multiplier)
    {
        baseStats.AdjustStats(stats, GetMultiplier() * multiplier);
    }

    public void AdjustMagicStats(List<EquipStat> stats)
    {
        AdjustMagicStats(stats, 1);
    }

    public void AdjustMagicStats(List<EquipStat> stats, float multiplier)
    {
        magicStats.AdjustStats(stats, GetMultiplier() * multiplier);
    }

    public void AddDismantleResources(List<Resource> resources)
    {
        dismantleResources = resources;
    }

    public void OnRegister(Unit unit)
    {
        float multiplier = GetMultiplier();

        unit.AdjustStats(baseStats, multiplier);
        unit.AdjustStats(magicStats, multiplier);
        unit.AddAction(abilities);
        unit.AddTriggeredAction(triggeredActions);
    }

    public void OnDeregister(Unit unit)
    {
        float multiplier = GetMultiplier();

        unit.AdjustStats(baseStats, -multiplier);
        unit.AdjustStats(magicStats, -multiplier);
        unit.RemoveAction(abilities);
        unit.RemoveTriggeredAction(triggeredActions);
    }

    public void MergeLikeStats()
    {
        magicStats.MergeStats(baseStats);
    }

    public Rarity GetRarity()
    {
        return rarity;
    }

    public float GetMultiplier()
    {
        return slotMultiplier;
    }

    public List<Resource> GetDismantleResources()
    {
        List<Resource> toReturn = new List<Resource>();
        toReturn.AddRange(dismantleResources);

        if(enchantment != null)
        {
            toReturn.AddRange(enchantment.GetCraftingCost());
        }

        if(divinity != null && divinity.currentLevel > 0)
        {
            toReturn.AddRange(divinity.GetCraftingCost());
        }

        if(material != null)
        {
            toReturn.AddRange(material.GetCraftingCost());
        }

        if(design != null)
        {
            toReturn.AddRange(design.GetCraftingCost());
        }

        return toReturn;
    }

    public List<Resource> GetCraftingCost()
    {
        List<Resource> toReturn = new List<Resource>();
        
        if(material != null) 
            toReturn.AddRange(material.GetCraftingCost());
        if(design != null) 
            toReturn.AddRange(design.GetCraftingCost());

        return toReturn;
    }

    public void Disenchant()
    {
        if(enchantment == null) return;

        enchantment.UnregisterParent(this);
    }
}

[Serializable]
public enum EquipSlot { Head, Chest, Feet, Mainhand, Offhand, Hands, Trinket, Amulet, Ring };
public enum StatType 
{ 

    Hitpoints, 
    Haste, CritChance, CritMultiplier, 
    Damage, Armor,
    Bleed, BleedResistance,
    Poison, PoisonResistance,
    Healing, HealingResistance,
    Fire, FireResistance,
    Cold, ColdResistance,
    Arcane, ArcaneResistance,
    Dark, DarkResistance,
    ArmorToDamage, DamageToPoison, DamageToBleed,
    Lifesteal, Thorns, Reflect,
    Perception, Preparation, Stealth,
    Dodge, Block, Resilience,
    Multistrike, MultiBuff, PurgeChance, CleanseChance,
    Pathfinding, Accuracy, Affliction,
    SpellProficiency, PierceProficiency, BluntProficiency, SupportProficiency, TechProficiency, CurseProficiency,
    Weight, Strength, Dexterity, Intelligence, Constitution,
    
};
public enum ModifierType { Flat, Multiplier, GlobalMultiplier };

[Serializable]
public struct EquipStat 
{ 
    public ModifierType ModifierType;
    public StatType StatType; 
    public float Value; 

    public static List<EquipStat> MergeStats(List<EquipStat> stats)
	{
		List<EquipStat> toReturn = new List<EquipStat>();

		foreach(EquipStat stat in stats)
		{
			bool found = false;

			for(int i = 0; i < toReturn.Count; i++)
			{
				EquipStat current = toReturn[i];
				if(current.StatType == stat.StatType && current.ModifierType == stat.ModifierType)
				{
					found = true;
					current.Value += stat.Value;
					toReturn[i] = current;
				}
			}

			if(!found) toReturn.Add(stat);
		}

		return toReturn;
	
    }

    public static List<EquipStat> NegateStats(List<EquipStat> stats)
    {
        List<EquipStat> toReturn = new List<EquipStat>();

        foreach(EquipStat stat in stats)
        {
            toReturn.Add(NegateStat(stat));
        }

        return toReturn;
        
    }

    public static EquipStat NegateStat(EquipStat stat)
    {
        EquipStat toReturn = stat;
        toReturn.Value = (-1 * toReturn.Value);
        return toReturn;
    }
}


public enum Rarity { ordinary, named, artifact, starting};