﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
    [CreateAssetMenu(menuName="Loot/MaterialCatalog")]
    public class MaterialCatalog : ScriptableObject
    {
        public List<MaterialSet> sets;

        public MaterialBuilder GetMaterial(int difficulty)
        {
            return GetRandomBuilder(GetEntries(difficulty));
        }

        private List<MaterialCatalogEntry> GetEntries(int difficulty)
        {
            List<MaterialCatalogEntry> toReturn = new List<MaterialCatalogEntry>();

            // foreach(MaterialSet set in sets)
            // {
            //     toReturn.AddRange(set.GetEntries(difficulty));
            // }

            return toReturn;
        }

        private MaterialBuilder GetRandomBuilder(List<MaterialCatalogEntry> entries)
		{
			float total = 0;

			foreach(MaterialCatalogEntry entry in entries)
			{
				total += entry.frequency;
			}

			float r = UnityEngine.Random.Range(0, total);

			foreach(MaterialCatalogEntry entry in entries)
			{
				if(r < entry.frequency) return entry.builder;
				else r -= entry.frequency;
			}

			return null;
		}

        public List<MaterialBuilder> GetCraftableMaterials()
        {
            List<MaterialBuilder> toReturn = new List<MaterialBuilder>();

            // foreach(MaterialSet set in sets)
            // {
            //     toReturn.AddRange(set.GetCraftableMaterials());
            // }

            return toReturn;
        }        
	
		public List<MaterialBuilder> GetMaterials()
		{
			List<MaterialBuilder> toReturn = new List<MaterialBuilder>();

            // foreach(MaterialSet set in sets)
            // {
            //     toReturn.AddRange(set.GetMaterials());
            // }

            return toReturn;
		}
        
    }
}