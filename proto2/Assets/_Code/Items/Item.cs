﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item 
{
    public string name;
    public int spriteIndex;
    public int level;
    public EquipSlot slot;

    public Item()
    {

    }
    
    public EquipSlot GetSlot()
    {
        return slot;
    }

    public virtual string GetName()
    {
        return name;
    }

    public int GetSprite()
    {
        return spriteIndex;
    }
}
