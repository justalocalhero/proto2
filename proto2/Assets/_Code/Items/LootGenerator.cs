﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Loot
{
	[CreateAssetMenu(menuName="Loot/LootGenerator")]
	public class LootGenerator : ScriptableObject 
	{		
		public NamedEquipmentSet named;
		public ItemValueCalculator itemValueCalculator;
		public GenericItemValueCalculator genericItemValueCalculator;
		public LootManager lootManager;

		private NamedEquipmentBuilder namedBuilder = new NamedEquipmentBuilder();

		public Equipment Generate(LootRollManager lootRollManager, LootProfile lootProfile)
		{
			float r = UnityEngine.Random.Range(0, 1.0f);
			if(r < named.chance) 
				return GenerateNamed();
			else 
				return GenerateGeneric(lootRollManager, lootProfile);
			
		}

		private Equipment GenerateGeneric(LootRollManager lootRollManager, LootProfile lootProfile)
		{
			Equipment equipment = lootManager.Generate(lootRollManager, lootProfile);

			equipment.MergeLikeStats();
			equipment.level = genericItemValueCalculator.GetValue(equipment);
			
			return equipment;
		}

		private Equipment GenerateNamed()
		{
			Equipment equipment = namedBuilder.Generate(named.profile.GetNext());

			equipment.MergeLikeStats();
			equipment.level = itemValueCalculator.GetValue(equipment);

			return equipment;
		}
	}
	
	[Serializable]
	public struct NamedEquipmentSet {public ArchetypeProfile profile; public float chance;}
}
