﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiftManager : MonoBehaviour 
{
	public EquipmentArchetype emergencyWeapon;
	public Inventory inventory;
	public Bullpen bullpen;
	private NamedEquipmentBuilder builder;
	public ItemValueCalculator itemValueCalculator;

	public void Awake()
	{
		bullpen.onPlayerSent += HandlePlayerSent;
	}

	private void HandlePlayerSent(Unit unit)
	{
		HandleEmergencyWeapon();
	}

	private void HandleEmergencyWeapon()
	{
		if(!inventory.HasSlot(EquipSlot.Mainhand))
		{
			if(builder == null) builder = new NamedEquipmentBuilder();

			Equipment toAdd = builder.Generate(emergencyWeapon);
			toAdd.level = itemValueCalculator.GetValue(toAdd);

			inventory.Add(toAdd);
		}

	}
}
