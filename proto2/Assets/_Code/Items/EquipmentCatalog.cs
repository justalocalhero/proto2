﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Loot
{
	[CreateAssetMenu(menuName="Equipment/EquipmentCatalog")]
	public class EquipmentCatalog : ScriptableObject 
	{
		// public List<EquipmentCatalogSet> sets;

		// public GenericEquipmentGenerator Generate()
		// {
		// 	EquipmentCatalogSet currentSet = RandomSet(sets);
		// 	EquipmentCatalogEntry currentEntry = currentSet.equipmentSet.RandomEntry();
		// 	GenericEquipmentGenerator generator = currentEntry.generator;

		// 	return generator;
		// }

		// private EquipmentCatalogSet RandomSet(List<EquipmentCatalogSet> sets)
		// {
		// 	float total = 0;

		// 	foreach(EquipmentCatalogSet currentSet in sets)
		// 	{
		// 		total += currentSet.frequency;
		// 	}

		// 	float r = UnityEngine.Random.Range(0, total);

		// 	foreach(EquipmentCatalogSet currentSet in sets)
		// 	{
		// 		if(r < currentSet.frequency) return currentSet;
		// 		else r -= currentSet.frequency;
		// 	}

		// 	Debug.LogWarning("Equipment Set could not be found");
		// 	return new EquipmentCatalogSet();
		// }

		// public List<EquipmentSet> GetCraftableSets()
		// {
		// 	List<EquipmentSet> toReturn = new List<EquipmentSet>();

		// 	foreach(EquipmentCatalogSet set in sets)
		// 	{
		// 		if(set.equipmentSet.CraftingUnlocked()) toReturn.Add(set.equipmentSet);
		// 	}

		// 	return toReturn;
		// }
	
	}
	
	[Serializable]
	public struct EquipmentCatalogSet {public EquipmentSet equipmentSet; public float frequency;}

	[Serializable]
	public struct EquipmentCatalogEntry {public GenericEquipmentGenerator generator; public float frequency;}
}

