﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(Loot.EnchantmentCatalogEntry))]
public class EnchantmentCatalogEntryDrawer : PropertyDrawer 
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int generatorSize = 160;
        int frequencySize = 80;
        int spacing = 5;
        int generatorIndent = 0;
        int frequencyIndent = generatorIndent + generatorSize + spacing;

        Rect generatorRect = new Rect(position.x, position.y, generatorSize, position.height);
        Rect frequencyRect = new Rect(position.x + frequencyIndent, position.y, frequencySize, position.height);

        EditorGUI.PropertyField(generatorRect, property.FindPropertyRelative("builder"), GUIContent.none);
        EditorGUI.PropertyField(frequencyRect, property.FindPropertyRelative("frequency"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
