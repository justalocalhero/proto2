﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(Loot.MinMax))]
public class MinMaxDrawer : PropertyDrawer 
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

		EditorGUI.LabelField(position, label);

        int generatorSize = 60;
        int frequencySize = 60;
        int spacing = 5;
        int generatorIndent = 160;
        int frequencyIndent = generatorIndent + generatorSize + spacing;

        Rect generatorRect = new Rect(position.x + generatorIndent, position.y, generatorSize, position.height);
        Rect frequencyRect = new Rect(position.x + frequencyIndent, position.y, frequencySize, position.height);

        EditorGUI.PropertyField(generatorRect, property.FindPropertyRelative("min"), GUIContent.none);
        EditorGUI.PropertyField(frequencyRect, property.FindPropertyRelative("max"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
