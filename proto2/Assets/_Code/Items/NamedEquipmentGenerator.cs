﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NamedEquipmentBuilder
{
	private EquipmentBuilder builder = new EquipmentBuilder();

	public Equipment Generate(EquipmentArchetype archetype)
	{
		Equipment equipment =
			builder.Begin()
			.WithName(archetype.name)
			.WithSlot(archetype.slot, 1)
			.WithSpriteManager(archetype.spriteManager)
			.WithSpriteIndex(archetype.spriteIndex)
			.WithRarity(archetype.rarity)
			.Build();
		
		
		List<Ability> abilities = archetype.GetAbilities();
		foreach(Ability ability in abilities)
		{
			equipment.AddAbility(ability);
		}

		List<TriggeredAction> triggeredActions = archetype.GetTriggeredActions();
		foreach(TriggeredAction triggeredAction in triggeredActions)
		{
			equipment.AddTrigger(triggeredAction);
		}

		equipment.magicStats.AdjustStats(archetype.GetStaticStats());
		
		equipment.AddDismantleResources(archetype.recipeCost.craftingCost);

		return equipment;
	}
}
