﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	public class QualityDictionaryWindow : MonoBehaviour 
	{
		public StatWindow statWindowPrototype;
		private StatWindow statWindow;
		public Transform statContainer;
		private QualityProfile profile;
		public TextField textField;
		public IntField difficultyField;

		public delegate void OnEdit();
		public OnEdit onEdit;

		public UIToggler toggler;
		
		public void Show()
		{
			toggler.Show();
		}

		public void Hide()
		{
			toggler.Hide();
		}

		public void RaiseOnEdit()
		{
			if(onEdit != null) onEdit();
		}

		public void Awake()
		{
			statWindow = Object.Instantiate(statWindowPrototype, statContainer, false);
			difficultyField.onChanged += HandleDifficulty;
			textField.onChanged += HandleName;
			statWindow.onEdit += RaiseOnEdit;
		}

		public void Set(QualityProfile profile)
		{
			this.profile =  profile;
			statWindow.Set(profile.stats);
			textField.Set(profile.name);
		}

		public void HandleName(string text)
		{
			profile.name = text;

			RaiseOnEdit();
		}

		public void HandleDifficulty(int difficulty)
		{
			profile.difficulty = difficulty;

			RaiseOnEdit();
		}
	}
}
