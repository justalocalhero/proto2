﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	public class StatWindow : MonoBehaviour 
	{
		public Transform slotContainer;
		public StatSlot slotPrototype;
		private List<StatSlot> slots;
		private List<EquipStat> stats;
		
		public delegate void OnEdit();
		public OnEdit onEdit;

		public void RaiseOnEdit()
		{
			if(onEdit != null) onEdit();
		}

		public void Awake()
		{
			BuildSlots();
		}

		public void BuildSlots()
		{
			GridBuilder<StatSlot> gridBuilder = new GridBuilder<StatSlot>();

			slots = gridBuilder.Begin()
				.WithPrototype(slotPrototype)
				.WithContainer(slotContainer)
				.WithColumns(1)
				.WithSpacing(2, 2)
				.Build();

			RegisterSlots();
		}

		public void RegisterSlots()
		{
			int index = 0;

			foreach(StatSlot slot in slots)
			{
				slot.index = index++;
				slot.onModChanged += HandleMod;
				slot.onTypeChanged += HandleType;
				slot.onValueChanged += HandleValue;
				slot.onDelete += HandleDelete;
				slot.onAdd += HandleAdd;
			}
		}

		public void HandleMod(int index, int value)
		{
			EquipStat current = stats[index];
			current.ModifierType = (ModifierType)value;
			stats[index] = current;

			RaiseOnEdit();
			UpdateUI();
		}

		public void HandleType(int index, int value)
		{
			EquipStat current = stats[index];
			current.StatType = (StatType)value;
			stats[index] = current;

			RaiseOnEdit();
			UpdateUI();
		}

		public void HandleValue(int index, float value)
		{
			EquipStat current = stats[index];
			current.Value = value;
			stats[index] = current;

			RaiseOnEdit();
			UpdateUI();
		}

		public void HandleDelete(int index)
		{
			stats.RemoveAt(index);

			RaiseOnEdit();
			UpdateUI();
		}

		public void HandleAdd(int index)
		{
			stats.Add(new EquipStat(){Value = 1});

			RaiseOnEdit();
			UpdateUI();
		}
	
		public void Set(List<EquipStat> stats)
		{
			if(stats == null) return;
			this.stats = stats;

			UpdateUI();
		}

		public List<EquipStat> GetStats()
		{
			return stats;
		}

		public void Clear()
		{
			this.stats = new List<EquipStat>();

			UpdateUI();
		}

		public void UpdateUI()
		{
			UpdateSlots();
		}

		public void UpdateSlots()
		{

			for(int i = 0; i < slots.Count; i++)
			{
				if(i < stats.Count)
				{
					slots[i].Set(stats[i]);
					slots[i].Show();
				}
				else if(i == stats.Count)
				{
					slots[i].ShowAdd();
				}
				else 
				{
					slots[i].Hide();
				}
			}
		}
	}
}