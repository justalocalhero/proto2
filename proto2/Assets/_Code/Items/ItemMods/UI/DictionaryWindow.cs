﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

namespace Loot
{
	public class DictionaryWindow : MonoBehaviour 
	{
		public TextMeshProUGUI label;
		
		public DesignDictionary designDictionary;
		public QualityDictionary qualityDictionary;
		public DivinityDictionary divinityDictionary;
		public EnchantmentDictionary enchantmentDictionary;
		public MaterialDictionary materialDictionary;

		private List<Entry<DesignProfile>> designList;
		private List<Entry<QualityProfile>> qualityList;
		private List<Entry<DivinityProfile>> divinityList;
		private List<Entry<EnchantmentProfile>> enchantmentList;
		private List<Entry<MaterialProfile>> materialList;


		public DesignDictionaryWindow designWindow;
		public QualityDictionaryWindow qualityWindow;
		public DivinityDictionaryWindow divinityWindow;
		public EnchantmentDictionaryWindow enchantmentWindow;
		public MaterialDictionaryWindow materialWindow;

		public RecipeCostMap recipeCostMap;

		public Button designButton, qualityButton, divinityButton, enchantmentButton, materialButton, leftButton, rightButton, clearTags, allTags;

		public Transform slotContainer, statTagContainer, equipTagContainer, slotTagContainer, damageTagContainer;
		public LabelSlot slotPrototype;
		public TagSlot tagPrototype;
		private List<Tag> tags = new List<Tag>();
		private List<LabelSlot> builderSlots;
		private List<TagSlot> tagSlots;
		private int maxPages;
		private int currentPage;
		private int entriesPerPage;

		public enum DictionaryState { none, design, quality, divinity, enchantment, material };
		public DictionaryState currentState;


		public void Awake()
		{
			designButton.onClick.AddListener(DesignPressed);
			qualityButton.onClick.AddListener(QualityPressed);
			divinityButton.onClick.AddListener(DivinityPressed);
			enchantmentButton.onClick.AddListener(EnchantmentPressed);
			materialButton.onClick.AddListener(MaterialPressed);
			leftButton.onClick.AddListener(HandleLeft);
			rightButton.onClick.AddListener(HandleRight);
			clearTags.onClick.AddListener(ClearTags);
			allTags.onClick.AddListener(AllTags);
			designWindow.onEdit += HandleDesignEdit;
			qualityWindow.onEdit += HandleQualityEdit;
			divinityWindow.onEdit += HandleDivinityEdit;
			enchantmentWindow.onEdit += HandleEnchantmentEdit;
			materialWindow.onEdit += HandleMaterialEdit;


			BuildTags();
			BuildSlots();
		}

		public void ClearTags()
		{
			SetAllTags(false);
		}

		public void AllTags()
		{
			SetAllTags(true);
		}

		public void SetAllTags(bool toSet)
		{
			foreach(TagSlot slot in tagSlots)
			{
				slot.Set(toSet);
			}

			UpdateTags();
		}

		public void HideWindows()
		{
			designWindow.Hide();
			qualityWindow.Hide();
			divinityWindow.Hide();
			enchantmentWindow.Hide();
			materialWindow.Hide();
		}
		public void DesignPressed()
		{
			label.SetText("Design");
			currentPage = 0;
			currentState = DictionaryState.design;
			HideWindows();
			UpdateUI();
		}

		public void QualityPressed()
		{
			label.SetText("Quality");
			currentPage = 0;
			currentState = DictionaryState.quality;
			HideWindows();
			UpdateUI();
		}

		public void DivinityPressed()
		{
			label.SetText("Divinity");
			currentPage = 0;
			currentState = DictionaryState.divinity;
			HideWindows();
			UpdateUI();
		}

		public void EnchantmentPressed()
		{
			label.SetText("Enchantment");
			currentPage = 0;
			currentState = DictionaryState.enchantment;
			HideWindows();
			UpdateUI();
		}

		public void MaterialPressed()
		{
			label.SetText("Material");
			currentPage = 0;
			currentState = DictionaryState.material;
			HideWindows();
			UpdateUI();
		}

		public void HandleLeft()
		{
			AlterPage(-1);
		}

		public void HandleRight()
		{
			AlterPage(1);
		}

		public void AlterPage(int toChange)
		{
			int temp = currentPage + toChange;
			if(temp >= 0 && temp <= maxPages) 
			{
				currentPage = temp;
				UpdateUI();
			}
		}

		public void UpdateTags()
		{
			tags = new List<Tag>();

			foreach(TagSlot tagSlot in tagSlots)
			{
				if(tagSlot.selected) tags.Add(new Tag()
				{
					depth = tagSlot.depth,
					width = tagSlot.width,
				});
			}

			HideWindows();
			UpdateUI();
		}

		public void HandleTag()
		{
			UpdateTags();
		}

		public void HandleDelete(int index)
		{
			Remove(index, currentPage * entriesPerPage);
			
			HideWindows();
			UpdateUI();
		}

		public void Remove(int index, int startIndex)
		{
			switch(currentState)
			{
				case DictionaryState.none:
					break;
				case DictionaryState.design:
					designDictionary.Remove(tags, index + startIndex);
					break;
				case DictionaryState.quality:
					qualityDictionary.Remove(tags, index + startIndex);
					break;
				case DictionaryState.divinity:
					divinityDictionary.Remove(tags, index + startIndex);
					break;
				case DictionaryState.enchantment:
					enchantmentDictionary.Remove(tags, index + startIndex);
					break;
				case DictionaryState.material:
					materialDictionary.Remove(tags, index + startIndex);
					break;				
			}
		}

		public void HandleAdd(int index)
		{
			Extend(index, currentPage * entriesPerPage);

			HideWindows();
			UpdateUI();
		}

	
		public void Extend(int index, int startIndex)
		{
			switch(currentState)
			{
				case DictionaryState.none:
					break;
				case DictionaryState.design:
					designDictionary.Extend(
						new DesignProfile(){
							name = "design", 
							recipeCost = recipeCostMap.recipeCosts[0]}, 
						tags);
					break;
				case DictionaryState.quality:
					qualityDictionary.Extend(
						new QualityProfile(){
							name = "quality", 
							recipeCost = recipeCostMap.recipeCosts[0]}, 
						tags);
					break;
				case DictionaryState.divinity:
					divinityDictionary.Extend(
						new DivinityProfile(){
							name = "Bless",
							recipeCost = recipeCostMap.recipeCosts[0]}, 
						tags);
					break;
				case DictionaryState.enchantment:
					enchantmentDictionary.Extend(
						new EnchantmentProfile(){
							name = "enchantment",
							recipeCost = recipeCostMap.recipeCosts[0]}, 
						tags);
					break;
				case DictionaryState.material:
					materialDictionary.Extend(
						new MaterialProfile(){
							name = "material",
							recipeCost = recipeCostMap.recipeCosts[0]}, 
						tags);
					break;				
			}
		}

		public void HandleSelected(int index)
		{
			HandleSelected(index, currentPage * entriesPerPage);
		}

		public void HandleSelected(int index, int startIndex)
		{
			switch(currentState)
			{
				case DictionaryState.none:
					break;
				case DictionaryState.design:
					designWindow.Set(designDictionary.GetFilled(tags)[index + startIndex].payload);
					HideWindows();
					designWindow.Show();
					return;
				case DictionaryState.quality:
					qualityWindow.Set(qualityDictionary.GetFilled(tags)[index + startIndex].payload);
					HideWindows();
					qualityWindow.Show();
					return;
				case DictionaryState.divinity:
					divinityWindow.Set(divinityDictionary.GetFilled(tags)[index + startIndex].payload);
					HideWindows();
					divinityWindow.Show();
					return;
				case DictionaryState.enchantment:
					enchantmentWindow.Set(enchantmentDictionary.GetFilled(tags)[index + startIndex].payload);
					HideWindows();
					enchantmentWindow.Show();
					return;
				case DictionaryState.material:
					materialWindow.Set(materialDictionary.GetFilled(tags)[index + startIndex].payload);
					HideWindows();
					materialWindow.Show();
					return;				
			}
			UpdateUI();
		}

		public void HandleDesignEdit()
		{
			designDictionary.Dirty();
			UpdateUI();
		}

		public void HandleQualityEdit()
		{
			qualityDictionary.Dirty();
			UpdateUI();
		}

		public void HandleDivinityEdit()
		{
			divinityDictionary.Dirty();
			UpdateUI();
		}

		public void HandleEnchantmentEdit()
		{
			enchantmentDictionary.Dirty();
			UpdateUI();
		}

		public void HandleMaterialEdit()
		{
			materialDictionary.Dirty();
			UpdateUI();
		}

		public void BuildTags()
		{
			tagSlots = new List<TagSlot>();
			
			int depth = 0;

			tagSlots.AddRange(BuildTags(depth++, new StatCategories(), statTagContainer));
			tagSlots.AddRange(BuildTags(depth++, new EquipCategories(), equipTagContainer));
			tagSlots.AddRange(BuildTags(depth++, new SlotCategories(), slotTagContainer));
			tagSlots.AddRange(BuildTags(depth++, new DamageCategories(), damageTagContainer));
		}

		public List<TagSlot> BuildTags(int depth, Enum e, Transform container)
		{
			string[] names = Enum.GetNames(e.GetType());
			GridBuilder<TagSlot> gridBuilder = new GridBuilder<TagSlot>();

			List<TagSlot> toReturn = gridBuilder.Begin()
				.WithPrototype(tagPrototype)
				.WithContainer(container)
				.WithCap(names.Length - 1)
				.Build();

			
			int width = 1;
			foreach(TagSlot slot in toReturn)
			{
				slot.SetName(names[width]);
				slot.depth = depth;
				slot.width = width++;
				slot.onToggle += HandleTag;
				slot.Set(false);
				slot.Show();
			}

			return toReturn;

		}

		public void BuildSlots()
		{
			GridBuilder<LabelSlot> gridBuilder = new GridBuilder<LabelSlot>();

			builderSlots = gridBuilder.Begin()
				.WithPrototype(slotPrototype)
				.WithContainer(slotContainer)
				.WithColumns(1)
				.WithRows(10)
				.Build();

			RegisterSlots();
			entriesPerPage = builderSlots.Count;
		}
		public void RegisterSlots()
		{
			int index = 0;

			foreach(LabelSlot slot in builderSlots)
			{
				slot.index = index++;
				slot.onSelected += HandleSelected;
				slot.onDelete += HandleDelete;
				slot.onAdd += HandleAdd;
			}
		}

		public void UpdateUI()
		{
			Set();
			switch(currentState)
			{
				case DictionaryState.none:
					break;
				case DictionaryState.design:
					UpdateUI(designList);
					return;
				case DictionaryState.quality:
					UpdateUI(qualityList);
					return;
				case DictionaryState.divinity:
					UpdateUI(divinityList);
					return;
				case DictionaryState.enchantment:
					UpdateUI(enchantmentList);
					return;
				case DictionaryState.material:
					UpdateUI(materialList);
					return;				
			}

		}
		
		public void Set()
		{
			switch(currentState)
			{
				case DictionaryState.none:
					break;
				case DictionaryState.design:
					designList = designDictionary.GetFilled(tags);
					break;
				case DictionaryState.quality:
					qualityList = qualityDictionary.GetFilled(tags);
					break;
				case DictionaryState.divinity:
					divinityList = divinityDictionary.GetFilled(tags);
					break;
				case DictionaryState.enchantment:
					enchantmentList = enchantmentDictionary.GetFilled(tags);
					break;
				case DictionaryState.material:
					materialList = materialDictionary.GetFilled(tags);
					break;				
			}
		}

		public void UpdateUI<T>(List<Entry<T>> selected) where T : ComponentProfile
		{
			if(selected == null) return;
			UpdateNavigationButtons(selected.Count);
			UpdateSlots(selected);
		}
		
		public void UpdateNavigationButtons(int listCount)
		{
			maxPages = (entriesPerPage <=  0) ? 0 : listCount / entriesPerPage;
			currentPage = Mathf.Clamp(currentPage, 0, maxPages);
			if(currentPage > 0) SetButtonState(leftButton, true);
			else SetButtonState(leftButton, false);

			if(maxPages > currentPage) SetButtonState(rightButton, true);
			else SetButtonState(rightButton, false);
		}

		public void SetButtonState(Button button, bool toShow)
		{
			if(button == null) return;
			if(button.gameObject.activeSelf != toShow) button.gameObject.SetActive(toShow);
		}

		public void UpdateSlots<T>(List<Entry<T>> currentList) where T : ComponentProfile
		{
			if(currentList == null) return;

			int pageIndex = currentPage * entriesPerPage;
			int clampedIndex = Mathf.Clamp(pageIndex, 0, pageIndex);

			for(int i = 0; i < builderSlots.Count; i++)
			{
				if(i < currentList.Count - clampedIndex)
				{
					builderSlots[i].Set(currentList[i + clampedIndex].payload.name);
					builderSlots[i].Show();
				}
				else if(i == currentList.Count - clampedIndex)
				{
					builderSlots[i].ShowAdd();
				}
				else
				{
					builderSlots[i].Hide();
				}
			}
		}

		public List<Tag> GetTags()
		{
			return tags;
		}
		
	}
}