﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Loot
{
	public class LabelSlot : ModSlot
	{
		public Button selectButton;
		public TextMeshProUGUI label;

		public delegate void OnSelected(int index);
		public OnSelected onSelected;

		public void RaiseOnSelected()
		{
			if(onSelected != null) onSelected(index);
		}

		public override void OnAwake()
		{
			selectButton.onClick.AddListener(HandleSelect);
		}

		public void Set(string text)
		{
			label.SetText(text);
		}

		public void HandleSelect()
		{
			RaiseOnSelected();
		}
	}
}