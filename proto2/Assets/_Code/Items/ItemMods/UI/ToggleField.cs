﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ToggleField : MonoBehaviour 
{
	public Toggle toggle;
	public TextMeshProUGUI label;

	public delegate void OnChanged(bool isOn);
	public OnChanged onChanged;

	public void RaiseOnChanged(bool isOn)
	{
		onChanged(isOn);
	}

	public void Awake()
	{
		toggle.onValueChanged.AddListener(RaiseOnChanged);
	}

	public void SetText(string text)
	{
		label.SetText(text);
	}

	public void Set(bool selected)
	{
		toggle.isOn = selected;
	}
}
