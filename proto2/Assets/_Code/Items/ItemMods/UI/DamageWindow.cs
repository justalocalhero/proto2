﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	public class DamageWindow : MonoBehaviour 
	{
		public Transform slotContainer;
		public DamageSlot slotPrototype;
		private List<DamageSlot> slots;
		private List<Damage> damages;
		
		public delegate void OnEdit();
		public OnEdit onEdit;

		public void RaiseOnEdit()
		{
			if(onEdit != null) onEdit();
		}

		public void Awake()
		{
			BuildSlots();
		}

		public void BuildSlots()
		{
			GridBuilder<DamageSlot> gridBuilder = new GridBuilder<DamageSlot>();

			slots = gridBuilder.Begin()
				.WithPrototype(slotPrototype)
				.WithContainer(slotContainer)
				.WithColumns(1)
				.WithSpacing(2, 2)
				.Build();

			RegisterSlots();
		}

		public void RegisterSlots()
		{
			int index = 0;

			foreach(DamageSlot slot in slots)
			{
				slot.index = index++;
				slot.onTypeChanged += HandleType;
				slot.onValueChanged += HandleValue;
				slot.onFactorChanged += HandleFactor;
				slot.onDelete += HandleDelete;
				slot.onAdd += HandleAdd;
			}
		}
	
		public void Set(List<Damage> damages)
		{
			if(damages == null) return;
			this.damages = damages;

			UpdateUI();
		}

		public void Clear()
		{
			this.damages = new List<Damage>();

			UpdateUI();
		}

		public List<Damage> GetDamages()
		{
			return damages;
		}

		public void HandleType(int index, int value)
		{
			Damage current = damages[index];
			current.type = (DamageType)value;
			damages[index] = current;

			RaiseOnEdit();
			UpdateUI();
		}

		public void HandleValue(int index, float value)
		{
			Damage current = damages[index];
			current.value = value;
			damages[index] = current;

			RaiseOnEdit();
			UpdateUI();
		}

		public void HandleFactor(int index,  float value)
		{
			Damage current = damages[index];
			current.damageFactor = value;
			damages[index] = current;

			RaiseOnEdit();
			UpdateUI();
		}

		public void HandleDelete(int index)
		{
			damages.RemoveAt(index);

			RaiseOnEdit();
			UpdateUI();
		}

		public void HandleAdd(int index)
		{
			damages.Add(new Damage());

			RaiseOnEdit();
			UpdateUI();
		}

		public void UpdateUI()
		{
			UpdateSlots();
		}

		public void UpdateSlots()
		{

			for(int i = 0; i < slots.Count; i++)
			{
				if(i < damages.Count)
				{
					slots[i].Set(damages[i]);
					slots[i].Show();
				}
				else if(i == damages.Count)
				{
					slots[i].ShowAdd();
				}
				else 
				{
					slots[i].Hide();
				}
			}
		}
	}
}