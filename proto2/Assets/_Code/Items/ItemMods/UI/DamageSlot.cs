﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	public class DamageSlot : ModSlot
	{
		public EnumDropdown typeDropdown;
		public FloatField valueField;
		public FloatField factorField;

		public delegate void OnTypeChanged(int index, int dropdownIndex);
		public OnTypeChanged onTypeChanged;

		public void RaiseOnTypeChanged(int dropdownIndex)
		{
			if(onTypeChanged != null) onTypeChanged(index, dropdownIndex);
		}

		public delegate void OnValueChanged(int index, float value);
		public OnValueChanged onValueChanged;

		public void RaiseOnValueChanged(float value)
		{
			if(onValueChanged != null) onValueChanged(index, value);
		}

		public delegate void OnFactorChanged(int index, float value);
		public OnFactorChanged onFactorChanged;

		public void RaiseOnFactorChanged(float value)
		{
			if(onFactorChanged != null) onFactorChanged(index, value);
		}

		public override void OnAwake()
		{
			typeDropdown.SetOptions(new DamageType());
			typeDropdown.onChanged += HandleType;
			valueField.onChanged += HandleValue;
			factorField.onChanged += HandleFactor;
		}

		public void HandleType(int dropdownIndex)
		{
			RaiseOnTypeChanged(dropdownIndex);
		}

		public void HandleValue(float value)
		{
			RaiseOnValueChanged(value);
		}

		public void HandleFactor(float value)
		{
			RaiseOnFactorChanged(value);
		}

		public void Set(Damage damage)
		{
			typeDropdown.Set((int)damage.type);
			valueField.Set(damage.value);
			factorField.Set(damage.damageFactor);
		}
	}
}