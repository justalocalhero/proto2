﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	public class DesignDictionaryWindow : MonoBehaviour 
	{
		public SpriteManager spriteManager;
		public ToggleField craftable, craftingUnlocked;
		public SpriteList equipmentSprites, abilitySprites;
		public Transform statContainer, equipmentSpriteWindowContainer, abilitySpriteWindowContainer, effectSpriteWindowContainer, equipmentSpriteContainer, 
			abilitySpriteContainer, effectSpriteContainer, 
			abilityDamageContainer, effectDamageContainer, effectStatContainer;
		public StatWindow statWindowPrototype;
		private StatWindow equipmentStatWindow, effectStatWindow;
		public DamageWindow damageWindowPrototype;
		private DamageWindow abilityDamageWindow, effectDamageWindow;
		public FloatField cooldownField, durationField, periodField;
		public IntField difficultyField;
		public SpriteWindow spriteWindowPrototype;
		private SpriteWindow equipmentSpriteWindow, abilitySpriteWindow, effectSpriteWindow;
		public SpriteSlot spriteSlotPrototype;
		private SpriteSlot equipmentSpriteSlot, abilitySpriteSlot, effectSpriteSlot;
		private DesignProfile designProfile;
		public TextField equipmentName, abilityName, effectName;
		public EnumDropdown proficiencyType, effectType;
		public Dropdown costField;
		public RecipeCostMap costMap;

		public delegate void OnEdit();
		public OnEdit onEdit;

		public UIToggler toggler;
		
		public void Show()
		{
			toggler.Show();
		}

		public void Hide()
		{
			toggler.Hide();
			equipmentSpriteWindow.Hide();
		}

		public void RaiseOnEdit()
		{
			if(onEdit != null) onEdit();
		}

		public void Awake()
		{
			BuildEquipmentComponents();
			BuildAbilityComponents();
			BuildEffectComponents();
		}

		public void BuildEquipmentComponents()
		{
			equipmentName.onChanged += HandleEquipmentName;

			difficultyField.onChanged += HandleDifficulty;
			costField.SetOptions(costMap.GetNames());
			costField.onChanged += HandleCost;

			equipmentSpriteSlot = Object.Instantiate(spriteSlotPrototype, equipmentSpriteContainer, false);
			equipmentSpriteSlot.onSelected += HandleEquipmentSpriteClicked;

			equipmentSpriteWindow = Object.Instantiate(spriteWindowPrototype, equipmentSpriteWindowContainer, false);
			equipmentSpriteWindow.SetSpriteList(equipmentSprites);
			equipmentSpriteWindow.onSelected += HandleEquipmentSprite;

			equipmentStatWindow = Object.Instantiate(statWindowPrototype, statContainer, false);
			equipmentStatWindow.onEdit += RaiseOnEdit;

			craftable.SetText("Craftable");
			craftable.onChanged += HandleCraftable;
			craftingUnlocked.SetText("Unlocked");
			craftingUnlocked.onChanged += HandleDefaultCraftable;
		}

		public void BuildAbilityComponents()
		{		
			proficiencyType.SetOptions(new ProficiencyType());			
			proficiencyType.onChanged += HandleProficiencyType;

			abilityName.onChanged += HandleAbilityName;

			cooldownField.onChanged += HandleCooldown;

			abilitySpriteWindow = Object.Instantiate(spriteWindowPrototype, abilitySpriteWindowContainer, false);
			abilitySpriteWindow.SetSpriteList(abilitySprites);
			abilitySpriteWindow.onSelected += HandleAbilitySprite;

			abilitySpriteSlot = Object.Instantiate(spriteSlotPrototype, abilitySpriteContainer, false);
			abilitySpriteSlot.onSelected += HandleAbilitySpriteClicked;
			
			abilityDamageWindow = Object.Instantiate(damageWindowPrototype, abilityDamageContainer, false);
			abilityDamageWindow.onEdit += RaiseOnEdit;

		}

		public void BuildEffectComponents()
		{
			effectType.SetOptions(new EffectProfile.EffectType());			
			effectType.onChanged += HandleEffectType;
			
			effectName.onChanged += HandleEffectName;

			effectSpriteWindow = Object.Instantiate(spriteWindowPrototype, effectSpriteWindowContainer, false);
			effectSpriteWindow.SetSpriteList(abilitySprites);
			effectSpriteWindow.onSelected += HandleEffectSprite;

			effectSpriteSlot = Object.Instantiate(spriteSlotPrototype, effectSpriteContainer, false);
			effectSpriteSlot.onSelected += HandleEffectSpriteClicked;

			periodField.onChanged += HandlePeriod;
			durationField.onChanged += HandleDuration;

			effectDamageWindow = Object.Instantiate(damageWindowPrototype, effectDamageContainer, false);
			effectDamageWindow.onEdit += RaiseOnEdit;

			effectStatWindow = Object.Instantiate(statWindowPrototype, effectStatContainer, false);
			effectStatWindow.onEdit += RaiseOnEdit;
		}

		public void Set(DesignProfile designProfile)
		{
			this.designProfile = designProfile;

			difficultyField.Set(designProfile.difficulty);
			costField.Set(costMap.GetIndex(designProfile.recipeCost));
			equipmentName.Set(designProfile.name);
			equipmentSpriteSlot.SetSprite(spriteManager.GetSprite(designProfile.GetSprite()));
			equipmentStatWindow.Set(designProfile.stats);
			craftable.Set(designProfile.craftable);
			craftingUnlocked.Set(designProfile.craftingUnlocked);

			Set(designProfile.abilityProfile);
		}

		public void Set(AbilityProfile abilityProfile)
		{
			proficiencyType.Set((int)abilityProfile.proficiencyType);
			abilityName.Set(abilityProfile.name);
			abilitySpriteSlot.SetSprite(spriteManager.GetSprite(abilityProfile.GetSprite()));
			cooldownField.Set(abilityProfile.period);
			abilityDamageWindow.Set(abilityProfile.damages);

			Set(abilityProfile.effectProfile);
		}

		public void Set(EffectProfile effectProfile)
		{
			effectType.Set((int)effectProfile.effectType);
			effectName.Set(effectProfile.name);
			effectSpriteSlot.SetSprite(spriteManager.GetSprite(effectProfile.GetSprite()));
			effectDamageWindow.Set(effectProfile.damages);
			effectStatWindow.Set(effectProfile.stats);
			durationField.Set(effectProfile.duration);
			periodField.Set(effectProfile.period);
		}

		public void HandleEquipmentName(string text)
		{
			designProfile.name = text;

			RaiseOnEdit();
		}

		public void HandleAbilityName(string text)
		{
			designProfile.abilityProfile.name = text;

			RaiseOnEdit();
		}

		public void HandleEffectName(string text)
		{
			designProfile.abilityProfile.effectProfile.name = text;

			RaiseOnEdit();
		}

		public void HandleEquipmentSpriteClicked(int index)
		{
			equipmentSpriteWindow.Show();
			abilitySpriteWindow.Hide();
			effectSpriteWindow.Hide();
		}

		public void HandleEquipmentSprite(Sprite sprite)
		{
			designProfile.spriteIndex = spriteManager.GetIndex(sprite);
			equipmentSpriteSlot.SetSprite(sprite); 

			equipmentSpriteWindow.Hide();

			RaiseOnEdit();
		}
		
		public void HandleAbilitySpriteClicked(int index)
		{
			equipmentSpriteWindow.Hide();
			abilitySpriteWindow.Show();
			effectSpriteWindow.Hide();
		}

		public void HandleAbilitySprite(Sprite sprite)
		{
			designProfile.abilityProfile.spriteIndex = spriteManager.GetIndex(sprite);
			abilitySpriteSlot.SetSprite(sprite); 

			abilitySpriteWindow.Hide();

			RaiseOnEdit();
		}
		
		public void HandleEffectSpriteClicked(int index)
		{
			equipmentSpriteWindow.Hide();
			abilitySpriteWindow.Hide();
			effectSpriteWindow.Show();
		}

		public void HandleEffectSprite(Sprite sprite)
		{
			designProfile.abilityProfile.effectProfile.spriteIndex = spriteManager.GetIndex(sprite);
			effectSpriteSlot.SetSprite(sprite); 

			effectSpriteWindow.Hide();

			RaiseOnEdit();
		}

		public void HandleEffectType(int index)
		{
			designProfile.abilityProfile.effectProfile.effectType = (EffectProfile.EffectType)index;

			RaiseOnEdit();
		}

		public void HandleProficiencyType(int index)
		{
			designProfile.abilityProfile.proficiencyType = (ProficiencyType)index;

			RaiseOnEdit();
		}

		public void HandleCooldown(float cooldown)
		{
			designProfile.abilityProfile.period = cooldown;

			RaiseOnEdit();
		}

		public void HandleDuration(float duration)
		{
			designProfile.abilityProfile.effectProfile.duration = duration;

			RaiseOnEdit();
		}

		public void HandlePeriod(float period)
		{
			designProfile.abilityProfile.effectProfile.period = period;

			RaiseOnEdit();			
		}

		public void HandleDifficulty(int difficulty)
		{
			designProfile.difficulty = difficulty;

			RaiseOnEdit();
		}

		public void HandleCost(int costIndex)
		{
			if(costIndex < 0|| costIndex >= costMap.recipeCosts.Count) return;

			RecipeCost cost = costMap.recipeCosts[costIndex];
			designProfile.recipeCost = cost;

			RaiseOnEdit();
		}

		public void HandleCraftable(bool isOn)
		{
			designProfile.craftable = isOn;

			RaiseOnEdit();
		}

		public void HandleDefaultCraftable(bool isOn)
		{
			designProfile.craftingUnlocked = isOn;

			RaiseOnEdit();
			
		}
	}
}
