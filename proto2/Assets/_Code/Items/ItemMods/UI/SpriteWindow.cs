﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Loot
{
	public class SpriteWindow : MonoBehaviour 
	{
		private SpriteList spriteList;
		public Transform slotContainer;
		public SpriteSlot slotPrototype;
		private List<SpriteSlot> slots;

		public delegate void OnSelected(Sprite sprite);
		public OnSelected onSelected;

		public UIToggler toggler;

		public void RaiseOnSelected(Sprite sprite)
		{
			if(onSelected != null) onSelected(sprite);
		}

		public void SetSpriteList(SpriteList spriteList)
		{
			this.spriteList = spriteList;
			BuildSlots();
		}

		public void Show()
		{
			toggler.Show();
		}

		public void Hide()
		{
			toggler.Hide();
		}

		public void BuildSlots()
		{
			GridBuilder<SpriteSlot> gridBuilder = new GridBuilder<SpriteSlot>();

			slots = gridBuilder.Begin()
				.WithPrototype(slotPrototype)
				.WithContainer(slotContainer)
				.WithCap(spriteList.sprites.Count)
				.WithSpacing(2, 2)
				.Build();

			RegisterSlots();
		}

		public void RegisterSlots()
		{
			int index = 0;

			foreach(SpriteSlot slot in slots)
			{
				slot.SetSprite(spriteList.sprites[index]);
				slot.index = index++;
				slot.onSelected += HandleSelect;
			}
		}

		public void HandleSelect(int index)
		{
			if(index < 0 || index >= spriteList.sprites.Count) return;

			RaiseOnSelected(spriteList.sprites[index]);
		}

	}
}