﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Loot
{
	public class EnumSlot : ModSlot 
	{
		public EnumDropdown dropdown;
		public delegate void OnEnumChanged(int index, int dropdownIndex);
		public OnEnumChanged onEnumChanged;

		public void RaiseOnEnumChanged(int dropdownIndex)
		{
			if(onEnumChanged != null) onEnumChanged(index, dropdownIndex);
		}

		public override void OnAwake()
		{
			dropdown.onChanged += HandleChange;
		}

		public void HandleChange(int dropdownIndex)
		{
			RaiseOnEnumChanged(dropdownIndex);
		}

		public void Set(int i)
		{
			dropdown.Set(i);
		}
	}
}