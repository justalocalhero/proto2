﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TextSlot : MonoBehaviour 
{
	public UIToggler toggler;
	public Button button;
	public TextMeshProUGUI mesh;
	public ColorReference selectedColor, availableColor;
	private int index;

	public delegate void OnClicked(int index);
	public OnClicked onClicked;

	public void RaiseOnClicked()
	{
		if(onClicked != null) onClicked(index);
	}

	public void Awake()
	{
		button.onClick.AddListener(HandleClick);
	}

	public void SetIndex(int index)
	{
		this.index = index;
	}

	public void SetText(string text)
	{
		mesh.SetText(text);

		Show();
	}

	public void SetSelected(bool selected)
	{
		SetColor((selected)? selectedColor : availableColor);
	}

	private void SetColor(Color color)
	{
		if(button.image.color != color) button.image.color = color;
	}

	public void HandleClick()
	{
		RaiseOnClicked();
	}

	public void Show()
	{
		toggler.Show();
	}

	public void Hide()
	{
		toggler.Hide();
	}
}
