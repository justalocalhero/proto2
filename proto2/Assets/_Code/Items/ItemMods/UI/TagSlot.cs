﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Loot
{
	public class TagSlot : MonoBehaviour 
	{
		public UIToggler toggler;
		public Button button;
		public TextMeshProUGUI text;
		public bool selected;
		public int depth;
		public int width;
		public ColorReference selectedColor, defaultColor;

		public delegate void OnToggle();
		public OnToggle onToggle;

		public void Awake()
		{
			button.onClick.AddListener(Toggle);
		}

		public void RaiseOnToggle()
		{
			if(onToggle != null) onToggle();
		}

		public void Set(bool selected)
		{
			this.selected = selected;
			button.image.color = (selected) ? selectedColor : defaultColor;
		}

		public void SetName(string name)
		{
			text.SetText(name);
		}

		public void Toggle()
		{
			Set(!selected);

			RaiseOnToggle();
		}

		public void Show()
		{
			toggler.Show();
		}

		public void Hide()
		{
			toggler.Hide();
		}
	}
}