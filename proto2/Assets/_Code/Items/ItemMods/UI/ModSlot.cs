﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Loot
{
	public class ModSlot : MonoBehaviour 
	{
		public int index {get; set;}
		public Button addButton;
		public Button deleteButton;
		public UIToggler editToggler;
		public UIToggler addToggler;

		public delegate void OnDelete(int index);
		public OnDelete onDelete;

		public void RaiseOnDelete()
		{
			if(onDelete != null) onDelete(index);
		}

		public delegate void OnAdd(int index);
		public OnAdd onAdd;

		public void RaiseOnAdd()
		{
			if(onAdd != null) onAdd(index);
		}


		public void Awake()
		{
			addButton.onClick.AddListener(HandleAdd);
			deleteButton.onClick.AddListener(HandleDelete);

			OnAwake();
		}

		public virtual void OnAwake()
		{

		}

		public virtual void HandleAdd()
		{
			RaiseOnAdd();
		}

		public virtual void HandleDelete()
		{
			RaiseOnDelete();
		}

		public void Hide()
		{
			editToggler.Hide();
			addToggler.Hide();
		}

		public void Show()
		{
			editToggler.Show();
			addToggler.Hide();
		}

		public void ShowAdd()
		{
			addToggler.Show();
			editToggler.Hide();
		}
	}
}