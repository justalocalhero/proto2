﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Variables/SpriteList")]
public class SpriteList : ScriptableObject 
{
	public List<Sprite> sprites;
}
