﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	public class DivinityDictionaryWindow : MonoBehaviour 
	{
		public StatWindow statWindowPrototype;
		private StatWindow statWindow;
		public Transform statContainer;
		private DivinityProfile divinityProfile;
		public TextField textField;
		public IntField difficultyField;
		public Dropdown costField;
		public RecipeCostMap costMap;

		public delegate void OnEdit();
		public OnEdit onEdit;

		public UIToggler toggler;
		
		public void Show()
		{
			toggler.Show();
		}

		public void Hide()
		{
			toggler.Hide();
		}

		public void RaiseOnEdit()
		{
			if(onEdit != null) onEdit();
		}

		public void Awake()
		{
			statWindow = Object.Instantiate(statWindowPrototype, statContainer, false);
			costField.SetOptions(costMap.GetNames());
			costField.onChanged += HandleCost;
			difficultyField.onChanged += HandleDifficulty;
			textField.onChanged += HandleName;
			statWindow.onEdit += RaiseOnEdit;
		}

		public void Set(DivinityProfile divinityProfile)
		{
			this.divinityProfile = divinityProfile;
			costField.Set(costMap.GetIndex(divinityProfile.recipeCost));
			difficultyField.Set(divinityProfile.difficulty);
			statWindow.Set(divinityProfile.stats);
			textField.Set(divinityProfile.name);
		}

		public void HandleName(string text)
		{
			divinityProfile.name = text;

			RaiseOnEdit();
		}

		public void HandleDifficulty(int difficulty)
		{
			divinityProfile.difficulty = difficulty;

			RaiseOnEdit();
		}

		public void HandleCost(int costIndex)
		{
			if(costIndex < 0|| costIndex >= costMap.recipeCosts.Count) return;

			RecipeCost cost = costMap.recipeCosts[costIndex];
			divinityProfile.recipeCost = cost;

			RaiseOnEdit();
		}
	}
}
