﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteSlot : MonoBehaviour
{
	public Button button;
	public Image icon;
	public int index;

	public delegate void OnSelected(int index);
	public OnSelected onSelected;

	public void RaiseOnSelected()
	{
		if(onSelected != null) onSelected(index);
	}

	public void Awake()
	{
		button.onClick.AddListener(RaiseOnSelected);
	}

	public void SetSprite(Sprite sprite)
	{
		if(sprite == null) return;

		icon.sprite = sprite;
	}
}
