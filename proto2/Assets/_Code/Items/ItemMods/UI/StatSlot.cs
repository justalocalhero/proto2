﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	public class StatSlot : ModSlot 
	{
		public EnumDropdown modDropdown;
		public EnumDropdown typeDropdown;
		public FloatField valueField;

		public delegate void OnModChanged(int index, int dropdownIndex);
		public OnModChanged onModChanged;

		public void RaiseOnModChanged(int dropdownIndex)
		{
			if(onModChanged != null) onModChanged(index, dropdownIndex);
		}

		public delegate void OnTypeChanged(int index, int dropdownIndex);
		public OnTypeChanged onTypeChanged;

		public void RaiseOnTypeChanged(int dropdownIndex)
		{
			if(onTypeChanged != null) onTypeChanged(index, dropdownIndex);
		}

		public delegate void OnValueChanged(int index, float value);
		public OnValueChanged onValueChanged;

		public void RaiseOnValueChanged(float value)
		{
			if(onValueChanged != null) onValueChanged(index, value);
		}

		public override void OnAwake()
		{
			modDropdown.SetOptions(new ModifierType());
			typeDropdown.SetOptions(new StatType());
			modDropdown.onChanged += HandleMod;
			typeDropdown.onChanged += HandleType;
			valueField.onChanged += HandleValue;
		}

		public void HandleMod(int dropdownIndex)
		{
			RaiseOnModChanged(dropdownIndex);
		}

		public void HandleType(int dropdownIndex)
		{
			RaiseOnTypeChanged(dropdownIndex);
		}

		public void HandleValue(float value)
		{
			RaiseOnValueChanged(value);
		}

		public void Set(EquipStat stat)
		{
			modDropdown.Set((int)stat.ModifierType);
			typeDropdown.Set((int)stat.StatType);
			valueField.Set(stat.Value);
		}
	}
}