﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Tooltips;

namespace Loot
{
	public class EquipmentPreview : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
	{
		public SpriteManager spriteManager;
		public Image image;
		private RectTransform rect;
		private Equipment equipment;
		public LootComponentDictionary dict;
		public EquipSlotMultiplierMap slotMap;
		public LootValue testValue;
		public DictionaryWindow window;
		public EquipmentTooltip tooltip;
		private EquipmentBuilder equipmentBuilder = new EquipmentBuilder();
		public LootRollManager lootRollManager = new LootRollManager();

		public void Awake()
		{
			rect = image.transform as RectTransform;
			SetLootRolls();
		}

		public void OnValidate()
		{
			SetLootRolls();
		}

		public void SetLootRolls()
		{
			lootRollManager = new LootRollManager();
			lootRollManager.AddLootValues(testValue);

		}

        public void OnPointerEnter(PointerEventData eventData)
        {
            if(equipment != null) tooltip.Set(equipment, rect);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
			tooltip.Hide();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Set(window.GetTags());
			OnPointerEnter(eventData);
        }

		public void Set(List<Tag> tags)
		{
			equipment = Generate(GetSlot(tags), tags, lootRollManager);
			image.sprite = spriteManager.GetSprite(equipment.GetSprite());
		}
		
		private Equipment Generate(EquipSlot slot, List<Tag> tags, LootRollManager rolls)
		{
			return
				equipmentBuilder.Begin()
				.WithTags(tags)
				.WithSlot(slot, slotMap.GetMultiplier(slot))
				.WithRarity(Rarity.ordinary)
				.WithDesign(GetDesign(tags, rolls))
				.WithQuality(GetQuality(tags, rolls))
				.WithDivinity(GetDivinity(tags, rolls))
				.WithEnchantment(GetEnchantment(tags, rolls))
				.WithMaterials(GetMaterial(tags, rolls))
				.Build();
		}

		private Design GetDesign(List<Tag> tags, LootRollManager rolls)
		{
			return dict.GenerateDesign(tags, rolls.DesignDifficulty());
		}

		private Quality GetQuality(List<Tag> tags, LootRollManager rolls)
		{
			return dict.GenerateQuality(tags, 0, rolls.QualityLevel());
		}

		private Divinity GetDivinity(List<Tag> tags, LootRollManager rolls)
		{
			return dict.GenerateDivinity(tags, 0, rolls.DivinityLevel());
		}

		private Enchantment GetEnchantment(List<Tag> tags, LootRollManager rolls)
		{
			return dict.GenerateEnchantment(tags, rolls.EnchantmentDifficulty(), rolls.EnchantmentLevel());
		}

		private Material GetMaterial(List<Tag> tags, LootRollManager rolls)
		{
			return dict.GenerateMaterial(tags, rolls.MaterialDifficulty());
		}

		private EquipSlot GetSlot(List<Tag> tags)
		{
			foreach(Tag tag in tags)
			{
				if(tag.depth == 2) return (EquipSlot)(tag.width - 1);
			}

			return new EquipSlot();
		}
    }
}