﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	public class EnchantmentDictionaryWindow : MonoBehaviour 
	{
		public ToggleField craftable, craftingUnlocked;
		public StatWindow statWindowPrototype;
		private StatWindow statWindow;
		public Transform statContainer;
		private EnchantmentProfile profile;
		public TextField textField;
		public IntField difficultyField;
		public Dropdown costField;
		public RecipeCostMap costMap;

		public delegate void OnEdit();
		public OnEdit onEdit;

		public UIToggler toggler;
		
		public void Show()
		{
			toggler.Show();
		}

		public void Hide()
		{
			toggler.Hide();
		}

		public void RaiseOnEdit()
		{
			if(onEdit != null) onEdit();
		}

		public void Awake()
		{
			statWindow = Object.Instantiate(statWindowPrototype, statContainer, false);
			costField.SetOptions(costMap.GetNames());
			costField.onChanged += HandleCost;
			difficultyField.onChanged += HandleDifficulty;
			textField.onChanged += HandleName;
			statWindow.onEdit += RaiseOnEdit;
			craftable.SetText("Craftable");
			craftable.onChanged += HandleCraftable;
			craftingUnlocked.SetText("Unlocked");
			craftingUnlocked.onChanged += HandleDefaultCraftable;
		}

		public void Set(EnchantmentProfile profile)
		{
			this.profile = profile;
			costField.Set(costMap.GetIndex(profile.recipeCost));
			difficultyField.Set(profile.difficulty);
			statWindow.Set(profile.stats);
			textField.Set(profile.name);
			craftable.Set(profile.craftable);
			craftingUnlocked.Set(profile.craftingUnlocked);
		}

		public void HandleName(string text)
		{
			profile.name = text;

			RaiseOnEdit();
		}

		public void HandleDifficulty(int difficulty)
		{
			profile.difficulty = difficulty;

			RaiseOnEdit();
		}

		public void HandleCost(int costIndex)
		{
			if(costIndex < 0|| costIndex >= costMap.recipeCosts.Count) return;

			RecipeCost cost = costMap.recipeCosts[costIndex];
			profile.recipeCost = cost;

			RaiseOnEdit();
		}

		public void HandleCraftable(bool isOn)
		{
			profile.craftable = isOn;

			RaiseOnEdit();
		}

		public void HandleDefaultCraftable(bool isOn)
		{
			profile.craftingUnlocked = isOn;

			RaiseOnEdit();
			
		}
	}
}