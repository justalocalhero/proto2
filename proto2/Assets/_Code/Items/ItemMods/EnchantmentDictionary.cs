﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	[CreateAssetMenu(menuName="Loot/EnchantmentDictionary")]
	public class EnchantmentDictionary : LootDictionary<EnchantmentProfile>  
	{		
		public List<EnchantmentEntry> divList;

		public override void Init()
		{
			foreach(EnchantmentEntry e in divList)
			{
				Register(new Entry<EnchantmentProfile>
				{
					payload = e.payload,
					tags = e.tags
				});
			}
		}

		public override void Add(Entry<EnchantmentProfile> entry)
		{
			EnchantmentEntry tempEntry = new EnchantmentEntry
			{
				payload = entry.payload,
				tags = entry.tags,
			};
				
			divList.Add(tempEntry);

			Dirty();
		}

		public override void Delete(int index)
		{
			divList.RemoveAt(index);

			Dirty();
		}
		
	}
	
	[System.Serializable]
	public struct EnchantmentEntry
	{ 
		public EnchantmentProfile payload;
		public List<Tag> tags; 
	}
}