﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	[CreateAssetMenu(menuName="Loot/LootComponentDictionary")]
	public class LootComponentDictionary : ScriptableObject 
	{
		public IntReference qualityMin, qualityMax, divinityMin, divinityMax, enchantmentMin, enchantmentMax;
		public DesignDictionary designDictionary;
		public QualityDictionary qualityDictionary;
		public DivinityDictionary divinityDictionary;
		public EnchantmentDictionary enchantmentDictionary;
		public MaterialDictionary materialDictionary;

		public StatEquivalencies statEquivalencies;

		public Design GenerateDesign(List<Tag> tags, int difficulty)
		{
			List<Entry<DesignProfile>> profiles = designDictionary.Get(tags);

			if(profiles == null || profiles.Count == 0) return null;

			DesignProfile profile = GetWithDifficulty(profiles, difficulty);

			if(profile == null) return null;

			Design toReturn = new Design(profile, statEquivalencies);

			return toReturn;
		}

		public Quality GenerateQuality(List<Tag> tags, int difficulty, int level)
		{
			List<Entry<QualityProfile>> profiles = qualityDictionary.Get(tags);

			if(profiles == null || profiles.Count == 0) return null;			

			QualityProfile profile = GetWithDifficulty(profiles, difficulty);

			if(profile == null) return null;

			Quality toReturn = new Quality(profile, statEquivalencies, qualityMin, qualityMax, level);

			return toReturn;
		}

		public Divinity GenerateDivinity(List<Tag> tags, int difficulty, int level)
		{
			List<Entry<DivinityProfile>> profiles = divinityDictionary.Get(tags);

			if(profiles == null || profiles.Count == 0) return null;

			DivinityProfile profile = GetWithDifficulty(profiles, difficulty);

			if(profile == null) return null;

			Divinity toReturn = new Divinity(profile, statEquivalencies, divinityMin, divinityMax, level);

			return toReturn;
		}

		public Enchantment GenerateEnchantment(List<Tag> tags, int difficulty, int level)
		{
			List<Entry<EnchantmentProfile>> profiles = enchantmentDictionary.Get(tags);

			if(profiles == null || profiles.Count == 0) return null;

			EnchantmentProfile profile = GetWithDifficulty(profiles, difficulty);

			if(profile == null) return null;

			Enchantment toReturn = new Enchantment(profile, statEquivalencies, enchantmentMin, enchantmentMax, level);

			return toReturn;
		}

		public Material GenerateMaterial(List<Tag> tags, int difficulty)
		{
			List<Entry<MaterialProfile>> profiles = materialDictionary.Get(tags);

			if(profiles == null || profiles.Count == 0) return null;

			MaterialProfile profile = GetWithDifficulty(profiles, difficulty);

			if(profile == null) return null;

			Material toReturn = new Material(profile, statEquivalencies);

			return toReturn;
		}

		private T GetWithDifficulty<T>(List<Entry<T>> list, int difficulty) where T : ComponentProfile
		{
			List<T> alternatives = new List<T>();

			foreach(Entry<T> entry in list)
			{
				if(entry.payload.difficulty <= difficulty) alternatives.Add(entry.payload);
			}

			return(alternatives.Count > 0) ? Utility.RandomFromList(alternatives) : null;
		}
	}
}