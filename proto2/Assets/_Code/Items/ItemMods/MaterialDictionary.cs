﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	[CreateAssetMenu(menuName="Loot/MaterialDictionary")]
	public class MaterialDictionary : LootDictionary<MaterialProfile>  
	{
		public List<MaterialEntry> divList;

		public override void Init()
		{
			foreach(MaterialEntry e in divList)
			{
				Register(new Entry<MaterialProfile>
				{
					payload = e.payload,
					tags = e.tags,
				});
			}
		}

		public override void Add(Entry<MaterialProfile> entry)
		{
			MaterialEntry tempEntry = new MaterialEntry
			{
				payload = entry.payload,
				tags = entry.tags,
			};
				
			divList.Add(tempEntry);

			Dirty();
		}

		public override void Delete(int index)
		{
			divList.RemoveAt(index);

			Dirty();
		}
		
	}
	
	[System.Serializable]
	public struct MaterialEntry
	{ 
		public MaterialProfile payload;
		public List<Tag> tags; 
	}
	
}