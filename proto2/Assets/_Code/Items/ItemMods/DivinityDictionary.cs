﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Loot
{
	[CreateAssetMenu(menuName="Loot/DivinityDictionary")]
	public class DivinityDictionary : LootDictionary<DivinityProfile>  
	{		
		public List<DivinityEntry> divList;

		public override void Init()
		{
			foreach(DivinityEntry e in divList)
			{
				Register(new Entry<DivinityProfile>
				{
					payload = e.payload,
					tags = e.tags
				});
			}
		}

		public override void Add(Entry<DivinityProfile> entry)
		{
			DivinityEntry tempEntry = new DivinityEntry
			{
				payload = entry.payload,
				tags = entry.tags,
			};
				
			divList.Add(tempEntry);

			Dirty();
		}

		public override void Delete(int index)
		{
			divList.RemoveAt(index);

			Dirty();
		}
		
		public override List<Entry<DivinityProfile>> GetCraftable(List<Tag> tags)
		{
			List<Entry<DivinityProfile>> profiles = GetFilled(tags);


			return profiles;
		}
		
	}
	
	[Serializable]
	public struct DivinityEntry
	{ 
		public DivinityProfile payload;
		public List<Tag> tags; 
	}
}