﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Trees
{
	public class TagTree<T>
	{
		private Node<T> head;

		private int depth;

		public TagTree(int[] dimensions)
		{
			depth = dimensions.Length;
			head = new Node<T>(dimensions, 0);
		}

		public List<T> Get(List<Loot.Tag> tags)
		{
			return Get(GetTraversal(tags));
		}

		private List<T> Get(int[][] traversal)
		{
			if(head == null) return null;

			return head.Get(traversal);
		}

		public List<T> GetFilled(List<Loot.Tag> tags)
		{
			return Get(GetFilledTraversal(tags));
		}

		public void Register(T toRegister, List<Loot.Tag> tags)
		{
			Register(toRegister, GetTraversal(tags));
		}

		private void Register(T toRegister, int[][] traversal)
		{
			head.Register(toRegister, traversal);
		}

		public void Remove(T toRegister, List<Loot.Tag> tags)
		{
			Remove(toRegister, GetTraversal(tags));
		}

		private void Remove(T toRegister, int[][] traversal)
		{
			head.Remove(toRegister, traversal);
		}

		public int[][] GetTraversal(List<Loot.Tag> tags)
		{
			List<int>[] lists = new List<int>[depth];
			int[][] toReturn = new int[depth][];

			for(int i = 0; i < lists.Length; i++)
			{
				lists[i] = new List<int>();
			}

			foreach(Loot.Tag tag in tags)
			{
				if(!lists[tag.depth].Contains(tag.width)) lists[tag.depth].Add(tag.width);
			}

			for(int i = 0; i < toReturn.Length; i++)
			{
				if(lists[i].Count == 0) lists[i].Add(0);
				toReturn[i] = lists[i].ToArray();
			}

			return toReturn;
		}

		public int[][] GetFilledTraversal(List<Loot.Tag> tags)
		{
			List<int>[] lists = new List<int>[depth];
			int[][] toReturn = new int[depth][];

			for(int i = 0; i < lists.Length; i++)
			{
				lists[i] = new List<int>();
			}

			foreach(Loot.Tag tag in tags)
			{
				if(!lists[tag.depth].Contains(tag.width)) 
					lists[tag.depth].Add(tag.width);
			}

			for(int i = 0; i < toReturn.Length; i++)
			{
				if(lists[i].Count == 0)
				{
					int count = Loot.Tag.GetWidthCountAtDepth(i);
					
					for(int j = 0; j < count; j++)
					{
						lists[i].Add(j);
					}
				}
				toReturn[i] = lists[i].ToArray();
			}

			return toReturn;
		}	
	}
}