﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Trees
{
	public class Node<T> 
	{
		protected int depth;
		private Node<T>[] children;

		public Node(int depth)
		{
			this.depth = depth;
		}

		public Node(int[] dimensions, int depth)
		{
			this.depth = depth;
			int width = dimensions[depth];
			bool nextIsLeaf = (dimensions.Length == (depth + 1));

			children = new Node<T>[width];

			for(int i = 0; i < width; i++)
			{
				if(nextIsLeaf) children[i] = new Leaf<T>(depth + 1);
				else children[i] = new Node<T>(dimensions, depth + 1);
			}
		}

		public List<T> Get(int[][] traversal)
		{
			return Get(traversal, new List<T>());
		}

		protected virtual List<T> Get(int[][] traversal, List<T> list)
		{
			int[] temp = traversal[depth];
			bool containsZero = false;

			for(int i = 0; i < temp.Length; i++)
			{
				int index = temp[i];
				children[index].Get(traversal, list);
				if(index == 0) containsZero = true;
			}

			if(!containsZero) 
			{
				children[0].Get(traversal, list);
			}

			
			return list;
		}
		
		public virtual void Register(T toRegister, int[][] traversal)
		{
			int[] temp = traversal[depth];

			for(int i = 0; i < temp.Length; i++)
			{
				int index = temp[i];
				children[index].Register(toRegister, traversal);
			}
		}
		
		public virtual void Remove(T toRegister, int[][] traversal)
		{
			int[] temp = traversal[depth];

			for(int i = 0; i < temp.Length; i++)
			{
				int index = temp[i];
				children[index].Remove(toRegister, traversal);
			}
		}
	}
}