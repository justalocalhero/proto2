﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Trees;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Loot
{
	public class LootDictionary<T> : ScriptableObject where T: ComponentProfile
	{
		public List<Entry<T>> list;
		protected TagTree<Entry<T>> tree;

		public void OnEnable()
		{
			int[] dimensions = BuildDimensions();

			BuildTree(dimensions);
			list = new List<Entry<T>>();

			Init();
		}

		public virtual void Init()
		{

		}

		public CraftingSave Save()
		{
			List<bool> toSave = new List<bool>();

			foreach(Entry<T> entry in list)
			{
				toSave.Add(entry.payload.craftingUnlocked);
			}

			return new CraftingSave()
			{
				craftingUnlocked = toSave
			};
		}

		public void Load(SaveFile saveFile, CraftingSave save) 
		{
			if(save == null) return;
			for(int i = 0; i < list.Count; i++)
			{
				list[i].payload.Load(saveFile);
				if(i < save.craftingUnlocked.Count)
					list[i].payload.craftingUnlocked = save.craftingUnlocked[i];
			}
		}

		public void BuildTree(int[] dimensions)
		{
			tree = new TagTree<Entry<T>>(BuildDimensions());
		}

		public List<Entry<T>> Get(List<Tag> tags)
		{
			return tree.Get(tags);
		}

		public List<Entry<T>> GetFilled(List<Tag> tags)
		{
			return tree.GetFilled(tags);
		}
		
		public virtual List<Entry<T>> GetCraftable(List<Tag> tags)
		{
			List<Entry<T>> profiles = GetFilled(tags);

			List<Entry<T>> craftable = new List<Entry<T>>();

			foreach(Entry<T> profile in profiles)
			{
				if(profile.payload.craftable && profile.payload.craftingUnlocked) 
					craftable.Add(profile);
			}

			return craftable;
		}

		public List<Entry<T>> GetAll()
		{
			return list;
		}

		protected void Register(Entry<T> entry)
		{
			if(!list.Contains(entry)) list.Add(entry);
			tree.Register(entry, entry.tags);
		}

		public void Remove(Entry<T> entry)
		{
			int index = list.IndexOf(entry);
			list.RemoveAt(index);
			tree.Remove(entry, entry.tags);
			Delete(index);
		}

		public virtual void Delete(int index)
		{
			
		}

		public void Remove(List<Tag> tagTypes, int index)
		{
			List<Entry<T>> entries = Get(tagTypes);

			Remove(entries[index]);
		}

		public void Extend(T newPayload, List<Tag> newTags)
		{
			Entry<T> tempEntry = new Entry<T>
			{
				payload = newPayload,
				tags = newTags
			};

			Register(tempEntry);
			Add(tempEntry);
		}

		public virtual void Add(Entry<T> entry)
		{

		}

		public int[] BuildDimensions()
		{
			int[] dimensions = new int[4];

			dimensions[0] = System.Enum.GetNames(typeof(StatCategories)).Length;
			dimensions[1] = System.Enum.GetNames(typeof(EquipCategories)).Length;
			dimensions[2] = System.Enum.GetNames(typeof(SlotCategories)).Length;
			dimensions[3] = System.Enum.GetNames(typeof(DamageCategories)).Length;

			return dimensions;
		}

		public static Enum GetDepthEnum(int depth)
		{
			switch(depth)
			{
				case 0:
					return new StatCategories();
				case 1:
					return new EquipCategories();
				case 2:
					return new SlotCategories();
				case 3:
					return new DamageCategories();
				default:
					Debug.LogWarning("Null Depth Enum at: " + depth);
					return null;
			}
		}

		public static int GetEnumDepth(Enum e)
		{
			if(e.GetType() == typeof(StatCategories))
			{
				return 0;
			}
			else if(e.GetType() == typeof(EquipCategories))
			{
				return 1;
			}
			else if(e.GetType() == typeof(SlotCategories))
			{
				return 2;
			}
			else if(e.GetType() == typeof(DamageCategories))
			{
				return 3;
			}
			else 
			{
				Debug.LogWarning("Null Depth at: " + e.GetType());
				return -1;
			}
		}

		public void Dirty()
		{
			#if UNITY_EDITOR
			EditorUtility.SetDirty(this);
			#endif
		}

		
		
	}

	public enum StatCategories { none, physical, magical, defensive, utility }
	public enum EquipCategories { none, cloth, leather, plate, weapon, shield, jewelry }
	public enum SlotCategories { none, head, chest, feet, mainhand, offhand, hands, trinket, amulet, ring }
	public enum DamageCategories { none, physical, poison, bleed, healing, fire, cold, arcane, dark, }

	public struct Entry<T> where T: ComponentProfile
	{ 
		public T payload; 
		public List<Tag> tags;
    }		

	[System.Serializable]
	public struct Tag 
	{ 
		public int depth; 
		public int width; 


		public static List<Tag> GetTags(StatCategories statCategory, EquipCategories equipCategory, SlotCategories slotCategory, DamageCategories damageCategory)
		{
			List<Tag> toReturn = new List<Tag>();

			toReturn.Add(GetTag(statCategory));
			toReturn.Add(GetTag(equipCategory));
			toReturn.Add(GetTag(slotCategory));
			toReturn.Add(GetTag(damageCategory));

			return toReturn;
		}

		public static List<Tag> GetTags(List<StatCategories> statCategory, List<EquipCategories> equipCategory, List<SlotCategories> slotCategory, List<DamageCategories> damageCategory)
		{		
			List<Tag> toReturn = new List<Tag>();

			toReturn.AddRange(GetTags(statCategory));
			toReturn.AddRange(GetTags(equipCategory));	
			toReturn.AddRange(GetTags(slotCategory));	
			toReturn.AddRange(GetTags(damageCategory));		

			return toReturn;
		}
		
		public static Tag GetTag(StatCategories statCategory)
		{
			return new Tag() {
				depth = 0,
				width = (int)statCategory
			};
		}
		
		public static Tag GetTag(EquipCategories equipCategory)
		{
			return new Tag() {
				depth = 1,
				width = (int)equipCategory
			};
		}
		
		public static Tag GetTag(SlotCategories slotCategory)
		{
			return new Tag() {
				depth = 2,
				width = (int)slotCategory
			};
		}
		
		public static Tag GetTag(DamageCategories damageCategory)
		{
			return new Tag() {
				depth = 3,
				width = (int)damageCategory
			};
		}

		public static int GetWidthCountAtDepth(int depth)
		{
			switch(depth)
			{
				case 0:
					return GetWidthCountAtDepth(0, new StatCategories());
				case 1:
					return GetWidthCountAtDepth(1, new EquipCategories());
				case 2:
					return GetWidthCountAtDepth(2, new SlotCategories());
				case 3:
					return GetWidthCountAtDepth(3, new DamageCategories());
				default:
					break;
			}

			return 0;
		}

		public static int GetWidthCountAtDepth(int depth, Enum e)
		{
			return Enum.GetNames(e.GetType()).Length;
		}
		
		public static List<Tag> GetTags(List<StatCategories> statCategory)
		{
			List<Tag> toReturn = new List<Tag>();

			foreach(StatCategories current in statCategory)
			{
				toReturn.Add(GetTag(current));
			}

			return toReturn;
		}
		
		public static List<Tag> GetTags(List<EquipCategories> statCategory)
		{
			List<Tag> toReturn = new List<Tag>();

			foreach(EquipCategories current in statCategory)
			{
				toReturn.Add(GetTag(current));
			}

			return toReturn;
		}
		
		public static List<Tag> GetTags(List<SlotCategories> statCategory)
		{
			List<Tag> toReturn = new List<Tag>();

			foreach(SlotCategories current in statCategory)
			{
				toReturn.Add(GetTag(current));
			}

			return toReturn;
		}
		
		public static List<Tag> GetTags(List<DamageCategories> statCategory)
		{
			List<Tag> toReturn = new List<Tag>();

			foreach(DamageCategories current in statCategory)
			{
				toReturn.Add(GetTag(current));
			}

			return toReturn;
		}
		
	}
}