﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Trees
{
	public class Leaf<T> : Node<T> 
	{
		List<T> payload;

		public Leaf(int depth) : base(depth)
		{
			payload = new List<T>();
		}

		protected override List<T> Get(int[][] traversal, List<T> list)
		{
			foreach(T t in payload)
			{
				if(!list.Contains(t)) list.Add(t);
			}
			//list.AddRange(payload);

			return list;
		}

		public override void Register(T toRegister, int[][] traversal)
		{
			if(!payload.Contains(toRegister))
				payload.Add(toRegister);
		}

		public override void Remove(T toRegister, int[][] traversal)
		{
			if(payload.Contains(toRegister))
				payload.Remove(toRegister);
		}
	}
}