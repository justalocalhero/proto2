﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Loot
{
	[CreateAssetMenu(menuName="Loot/QualityDictionary")]
	public class QualityDictionary : LootDictionary<QualityProfile>  
	{		
		public List<QualityEntry> divList;

		public override void Init()
		{
			foreach(QualityEntry e in divList)
			{
				Register(new Entry<QualityProfile>
				{
					payload = e.payload,
					tags = e.tags
				});
			}
		}

		public override void Add(Entry<QualityProfile> entry)
		{
			QualityEntry tempEntry = new QualityEntry
			{
				payload = entry.payload,
				tags = entry.tags,
			};
				
			divList.Add(tempEntry);

			Dirty();
		}

		public override void Delete(int index)
		{
			divList.RemoveAt(index);

			Dirty();
		}
		
		public override List<Entry<QualityProfile>> GetCraftable(List<Tag> tags)
		{
			List<Entry<QualityProfile>> profiles = GetFilled(tags);


			return profiles;
		}
		
	}
	
	[Serializable]
	public struct QualityEntry
	{ 
		public QualityProfile payload;
		public List<Tag> tags; 
	}
}