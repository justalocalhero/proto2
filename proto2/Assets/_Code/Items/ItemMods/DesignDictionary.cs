﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Loot
{
	[CreateAssetMenu(menuName="Loot/DesignDictionary")]
	public class DesignDictionary : LootDictionary<DesignProfile>  
	{
		public List<DesignEntry> divList;

		public override void Init()
		{
			foreach(DesignEntry e in divList)
			{
				Register(new Entry<DesignProfile>
				{
					payload = e.payload,
					tags = e.tags
				});
			}
		}

		public override void Add(Entry<DesignProfile> entry)
		{
			DesignEntry tempEntry = new DesignEntry
			{
				payload = entry.payload,
				tags = entry.tags,
			};
				
			divList.Add(tempEntry);

			Dirty();
		}

		public override void Delete(int index)
		{
			divList.RemoveAt(index);

			Dirty();
		}
		
	}
	
	[Serializable]
	public struct DesignEntry
	{ 
		public DesignProfile payload;
		public List<Tag> tags; 
	}
}