﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Loot;

public class EquipmentBuilder
{
	private Equipment equipment;
    
	public EquipmentBuilder Begin()
	{
		equipment = new Equipment();
		return this;
	}

    public EquipmentBuilder WithName(string name)
    {
        equipment.name = name;
        return this;
    }

    public EquipmentBuilder WithTags(List<Tag> tags)
    {
        equipment.tags = tags;
        return this;
    }

    public EquipmentBuilder WithSpriteIndex(int spriteIndex)
    {
        equipment.spriteIndex = spriteIndex;
        return this;
    }

    public EquipmentBuilder WithSpriteManager(SpriteManager spriteManager)
    {
        return this;
    }


    public EquipmentBuilder WithRarity(Rarity rarity)
    {
        equipment.rarity = rarity;
        return this;
    }

    public EquipmentBuilder WithSlot(EquipSlot slot, float slotMultiplier)
    {
        equipment.slot = slot;
        equipment.slotMultiplier = slotMultiplier;
        return this;
    }

    public EquipmentBuilder WithDesign(Design design)
    {
        if(design != null) design.RegisterParent(equipment);
        return this;
    }

    public EquipmentBuilder WithQuality(Quality quality)
    {
        if(quality != null) quality.RegisterParent(equipment);
        return this;
    }

	public EquipmentBuilder WithDivinity(Divinity divinity)
    {
        if(divinity != null) divinity.RegisterParent(equipment);
        return this;
    }

    public EquipmentBuilder WithMaterials(Loot.Material material)
    {
        if(material != null) material.RegisterParent(equipment);
        return this;
    }

    public EquipmentBuilder WithEnchantment(Enchantment enchantment)
    {
        if(enchantment != null) enchantment.RegisterParent(equipment);
        return this;
    }

    public EquipmentBuilder WithAbilityAddon(AbilityAddon abilityAddon)
    {
        if(abilityAddon.HasAbility()) equipment.AddAbility(abilityAddon.GenerateAbility());
        if(abilityAddon.HasTrigger()) equipment.AddTrigger(abilityAddon.GenerateTrigger());

        return this;
    }
     
    public EquipmentBuilder WithAbility(Ability ability)
    {
        equipment.AddAbility(ability);
        return this;
    }

    public EquipmentBuilder WithTriggeredAction(TriggeredAction triggeredAction)
    {
        equipment.AddTrigger(triggeredAction);
        return this;
    }
     
    public EquipmentBuilder WithLevel(int level)
    {
        equipment.level = level;
        return this;
    }


    public Equipment Build()
    {
        return equipment;
    }
}
