﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	[System.Serializable]
	public class LootRollManager
	{
		public float level;
		public float difficulty;
		public MinMax designLevel;
		public MinMax divinityLevel;
		public MinMax qualityLevel;
		public MinMax enchantmentLevel;
		public MinMax materialChance;
		public MinMax enchantmentChance;
		
		private Dice dice = new Dice();

		public void AddLootValues(LootValue lootValues)
		{
			designLevel = Add(designLevel, lootValues.designLevel);
			divinityLevel = Add(divinityLevel, lootValues.divinityLevel);
			qualityLevel = Add(qualityLevel, lootValues.qualityLevel);
			enchantmentLevel = Add(enchantmentLevel, lootValues.enchantmentLevel);
			materialChance = Add(materialChance, lootValues.materialChance);
			enchantmentChance = Add(enchantmentChance, lootValues.enchantmentChance);
		}

		public void AddDifficulty(float difficulty)
		{
			this.difficulty += difficulty;
		}

		public void AddLevel(float level)
		{
			this.level = level;
		}

		public MinMax Add(MinMax first, MinMax second)
		{
			return new MinMax
			{
				min = first.min + second.min,
				max = first.max + second.max,
			};
		}

		public int DesignDifficulty()
		{
			float max = designLevel.max;
			
			int maxLevel = dice.ChainRoll(max);

			int level = maxLevel;

			return level;
		}

		public int DivinityLevel()
		{
			float min = divinityLevel.min;
			float max = divinityLevel.max;

			int level = ConbinedRoll(min, max);
			
			return level;
		}

		public int QualityLevel()
		{
			float min = qualityLevel.min;
			float max = qualityLevel.max;

			int level = ConbinedRoll(min, max);

			return level;
		}
		
		public int MaterialDifficulty()
		{
			float max = materialChance.max;
			
			int maxLevel = dice.ChainRoll(max);

			int level = maxLevel;

			return level;
		}

		public int EnchantmentDifficulty()
		{
			float max = enchantmentChance.max;

			int maxLevel = dice.ChainRoll(max);

			int difficulty = maxLevel;

			return difficulty;
		}

		public int EnchantmentLevel()
		{
			float max = enchantmentLevel.max;

			int maxLevel = dice.ChainRoll(max);

			int level = maxLevel;

			return level;
		}

		private int ConbinedRoll(float min, float max)
		{
			int minLevel = dice.ChainRoll(min);
			int maxLevel = dice.ChainRoll(max);

			int level = maxLevel - minLevel;

			return level;
		}
	}
}
