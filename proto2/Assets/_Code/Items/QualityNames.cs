﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Names/QualityNames")]
public class QualityNames : ScriptableObject
{
	public IntReference startingIndex;
	public List<StringReference> names;

	public string Get(int index)
	{
		if(names == null || names.Count == 0) return "";
		int adjustedIndex = index - startingIndex;
		int clampedIndex = Mathf.Clamp(adjustedIndex, 0, names.Count - 1);

		return names[clampedIndex];
	}
}
