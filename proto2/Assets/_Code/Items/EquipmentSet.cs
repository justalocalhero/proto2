﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	[CreateAssetMenu(menuName="Equipment/EquipmentSet")]
	public class EquipmentSet : ScriptableObject 
	{
		// public string setName;
		// public List<EquipmentCatalogEntry> generators;

		// public EquipmentCatalogEntry RandomEntry()
		// {
		// 	float total = 0;

		// 	foreach(EquipmentCatalogEntry entry in generators)
		// 	{
		// 		total += entry.frequency;
		// 	}

		// 	float r = UnityEngine.Random.Range(0, total);

		// 	foreach(EquipmentCatalogEntry entry in generators)
		// 	{
		// 		if(r < entry.frequency) return entry;
		// 		else r -= entry.frequency;
		// 	}

		// 	Debug.LogWarning("Catalog Entry could not be found");
		// 	return new EquipmentCatalogEntry();
		// }

		// public bool CraftingUnlocked()
		// {
		// 	foreach(EquipmentCatalogEntry entry in generators)
		// 	{
		// 		if(entry.generator.CraftingUnlocked()) return true;
		// 	}

		// 	return false;
		// }

		// public bool CanAfford(ResourceManager resourceManager)
		// {
		// 	foreach(EquipmentCatalogEntry entry in generators)
		// 	{
		// 		if(entry.generator.CanAfford(resourceManager)) return true;
		// 	}

		// 	return false;
		// }

		// public List<GenericEquipmentGenerator> GetCraftableGenerators()
		// {
		// 	List<GenericEquipmentGenerator> toReturn = new List<GenericEquipmentGenerator>();

		// 	foreach(EquipmentCatalogEntry entry in generators)
		// 	{
		// 		if(entry.generator.CraftingUnlocked()) toReturn.Add(entry.generator);
		// 	}

		// 	return toReturn;
		// }
	}
}
