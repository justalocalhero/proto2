﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RebirthManager : MonoBehaviour 
{
	public BattlegroundManager battlegroundManager;
	public RebirthEquipment rebirthEquipment;
	public Inventory inventory;
	public FloatVariable rebirthChance;
	

	public void Start() 
	{
		battlegroundManager.onPlayerDeath += Check;
	}
	
	public void Check(Player player)
	{
		if(UnityEngine.Random.Range(0, 1.0f) < rebirthChance.Value) inventory.Add(rebirthEquipment.Generate(player));
	}
}
