﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Loot/ArtifactGenerator")]
public class ArtifactEquipmentGenerator : ScriptableObject
{	
	public StringReference itemName;
	public SpriteManager spriteManager;
	public Sprite sprite;
	public int spriteIndex = -1;
	public EquipSlot equipSlot;
	public StatEquivalencies statEquivalencies;
	public List<EquipStat> stats;
	private EquipmentBuilder builder;
	public AbilityBuilder abilityBuilder;
	public RecipeCost recipeCost;
	
	public void OnEnable()
	{
		ValidateSprite();
	}

	public void SetSpriteIndex()
	{
		if(sprite == null) return;
		if(spriteManager == null) return;
		spriteIndex = spriteManager.GetIndex(sprite);
		if(spriteIndex == -1)
			spriteIndex = spriteManager.AddSprite(sprite);
		if(spriteIndex == -1)
			Debug.LogWarning("Panic");
	}

	public void OnValidate()
	{
		ValidateSprite();
	}

	public void ValidateSprite()
	{
		if(sprite == null) return;
		if(spriteManager == null) return;

		if(sprite == spriteManager.GetSprite(spriteIndex)) 
			return;
		else 
			SetSpriteIndex();
	}
	
	public Equipment Generate()
	{
		if(builder == null) builder = new EquipmentBuilder();
		
		Equipment equipment =
			builder.Begin()
			.WithName(itemName)
			.WithSlot(equipSlot, 1)
			.WithSpriteManager(spriteManager)
			.WithSpriteIndex(spriteIndex)
			.WithRarity(Rarity.artifact)
			.WithLevel(99)
			.Build();
		
		equipment.AddMagicStats(stats);
		equipment.AddDismantleResources(recipeCost.craftingCost);

		if(abilityBuilder != null) equipment.AddAbility(abilityBuilder.Generate());

		return equipment;
	}

	
}