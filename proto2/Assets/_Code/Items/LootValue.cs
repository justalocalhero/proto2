﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Loot
{
	[CreateAssetMenu(menuName="Loot/LootValue")]
	public class LootValue : ScriptableObject
	{	
		public MinMax designLevel;
		public MinMax divinityLevel;
		public MinMax qualityLevel;
		public MinMax enchantmentLevel;
		public MinMax materialChance;
		public MinMax enchantmentChance;
	}

	[Serializable]
	public struct MinMax {public float min; public float max;}
}
