﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Loot
{
	public class QualitySetter 
	{
		private Dice dice = new Dice();

		public Equipment Set(Equipment equipment, LootRollManager lootRollManager)
		{
			int level = Level(lootRollManager);
			equipment.quality.AlterLevel(level, equipment);
				
			return equipment;
		}

		private int Level(LootRollManager lootRollManager)
		{
			float min = lootRollManager.qualityLevel.min;
			float max = lootRollManager.qualityLevel.max;

			int minLevel = dice.ChainRoll(min);
			int maxLevel = dice.ChainRoll(max);

			int level = maxLevel - minLevel;

			return level;
		}
	}
}