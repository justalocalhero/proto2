﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Loot
{
	[Serializable]
	public class AbilityAddon 
	{
		public AbilityType currentType;
		public TriggeredActionBuilder triggeredActionBuilder;
		public AbilityBuilder abilityBuilder;

		public bool HasNone()
		{
			return currentType == AbilityType.none;
		}

		public bool HasAbility()
		{
			return currentType == AbilityType.ability;
		}

		public bool HasTrigger()
		{
			return currentType == AbilityType.trigger;
		}

		public Ability GenerateAbility()
		{
			return abilityBuilder.Generate();
		}

		public TriggeredAction GenerateTrigger()
		{
			return triggeredActionBuilder.Generate();
		}

		public enum AbilityType {none, ability, trigger};
	}
}