﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot 
{
	[System.Serializable]
	public class Material : Component
	{
		public MaterialProfile profile;

		public Material()
		{
			
		}

        public Material(MaterialProfile profile, StatEquivalencies statEquivalencies)
        {
            this.name = profile.name;
            this.recipeCost = profile.recipeCost;
            AddProfile(profile);
            AddStats(profile.stats, statEquivalencies);
        }

		public override void RegisterParent(Equipment parent)
        {
            if(parent == null) return;

            parent.material = this;

			parent.AdjustBaseStats(stats);
        }

        public void AddProfile(MaterialProfile profile)
        {
            this.profile = profile;
        }

        public bool CanUnlock()
        {
            return profile != null && profile.craftable && !profile.craftingUnlocked;
        }

        public void Unlock()
        {
            if(CanUnlock()) profile.craftingUnlocked = true;
        }	
    }
}
