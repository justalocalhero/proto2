﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Loot
{
	public class DivinitySetter 
	{
		private Dice dice = new Dice();

		public Equipment Set(Equipment equipment, Divinity divinity, LootRollManager lootRollManager)
		{
			int level = Level(lootRollManager);
			equipment.divinity.AlterLevel(level, equipment);
				
			return equipment;
		}

		private int Level(LootRollManager lootRollManager)
		{
			float min = lootRollManager.divinityLevel.min;
			float max = lootRollManager.divinityLevel.max;

			int minLevel = dice.ChainRoll(min);
			int maxLevel = dice.ChainRoll(max);

			int level = maxLevel - minLevel;
			
			return level;
		}
	}
}