﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot 
{
	[System.Serializable]
	public class Enchantment : Component
	{
		public int maxLevel;
		public int minLevel;
		public int currentLevel;
		public EnchantmentProfile profile;

		public Enchantment()
		{

		}

		public Enchantment(int minLevel, int maxLevel, int currentLevel)
		{
			this.minLevel = minLevel;
			this.maxLevel = maxLevel;
			this.currentLevel = Mathf.Clamp(currentLevel, minLevel, maxLevel);
		}

		public Enchantment(EnchantmentProfile profile, StatEquivalencies statEquivalencies, int minLevel, int maxLevel, int currentLevel)
		{
			this.name = profile.name;
			this.recipeCost = profile.recipeCost;
			AddProfile(profile);
			AddStats(profile.stats, statEquivalencies);
			this.minLevel = minLevel;
			this.maxLevel = maxLevel;
			this.currentLevel = Mathf.Clamp(currentLevel, minLevel, maxLevel);
		}

		public override void RegisterParent(Equipment parent)
		{
			if(parent == null) return;
			
			parent.Disenchant();
			parent.enchantment = this;
			parent.AdjustMagicStats(stats, currentLevel);
		}
		
		public void UnregisterParent(Equipment parent)
		{
			if(parent == null) return;
	
			parent.AdjustMagicStats(stats, -currentLevel);
			parent.enchantment = null;
		}

		public void AlterLevel(int levelChange, Equipment parent)
        {
            if(levelChange == 0) return;
            
            if(CanAlter(levelChange)) 
			{
				currentLevel += levelChange;
           		parent.AdjustMagicStats(stats, levelChange);
			}
            else 
			{
				int nextLevelChange = 0;
				if(levelChange > 0) nextLevelChange = levelChange - 1;
				if(levelChange < 0) nextLevelChange = levelChange + 1;
				if(nextLevelChange != 0) AlterLevel(nextLevelChange, parent);
			}
        }

        public bool CanAlter(int levelChange)
        {
            int prospectiveLevel = currentLevel + levelChange;
			return prospectiveLevel >= minLevel && prospectiveLevel <= maxLevel;
        }

        public void AddProfile(EnchantmentProfile profile)
        {
            this.profile = profile;
        }

        public bool CanUnlock()
        {
            return profile != null && profile.craftable && !profile.craftingUnlocked;
        }

        public void Unlock()
        {
            if(CanUnlock()) profile.craftingUnlocked = true;
        }
	}
}
