﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot 
{
	[System.Serializable]
	public class Quality : Component
	{
		public QualityProfile profile;

		public int minLevel;
		public int maxLevel;
		public int currentLevel;

        public Quality()
		{

			
		}

		public Quality(int minLevel, int maxLevel, int currentLevel)
		{
			this.minLevel = minLevel;
			this.maxLevel = maxLevel;
			this.currentLevel = Mathf.Clamp(currentLevel, minLevel, maxLevel);
		}

		public Quality(QualityProfile profile, StatEquivalencies statEquivalencies, int minLevel, int maxLevel, int currentLevel)
		{
            this.name = profile.name;
			this.recipeCost = profile.recipeCost;
			AddProfile(profile);
			AddStats(profile.stats, statEquivalencies);
			this.minLevel = minLevel;
			this.maxLevel = maxLevel;
			this.currentLevel = Mathf.Clamp(currentLevel, minLevel, maxLevel);
		}
        
        public override void RegisterParent(Equipment parent)
        {
            if(parent == null) return;

            parent.quality = this;

            parent.AdjustBaseStats(stats, currentLevel);

        }

        public void AlterLevel(int levelChange, Equipment parent)
        {
            if(levelChange == 0) return;
            
            if(CanAlter(levelChange)) 
			{
				currentLevel += levelChange;
           		parent.AdjustBaseStats(stats, levelChange);
			}
            else 
			{
				int nextLevelChange = 0;
				if(levelChange > 0) nextLevelChange = levelChange - 1;
				if(levelChange < 0) nextLevelChange = levelChange + 1;
				if(nextLevelChange != 0) AlterLevel(nextLevelChange, parent);
			}
        }

        public bool CanAlter(int levelChange)
        {
            int prospectiveLevel = currentLevel + levelChange;
			return prospectiveLevel >= minLevel && prospectiveLevel <= maxLevel;
        }

        public void AddProfile(QualityProfile profile)
        {
            this.profile = profile;
        }
    }
}
