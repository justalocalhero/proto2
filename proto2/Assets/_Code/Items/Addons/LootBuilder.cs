﻿using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;

public class LootBuilder : ScriptableObject
{
	public List<EquipStat> stats;
	public StatEquivalencies statEquivalencies;

	public virtual string GetName()
	{
		return "Missing Label";
	}

	public List<EquipStat> GetStats()
	{
		List<EquipStat> toReturn = new List<EquipStat>();

		foreach(EquipStat stat in stats)
		{
			toReturn.Add(statEquivalencies.FactorStat(stat));
		}

		return toReturn;
	}

	public void RemoveStat(int index)
	{
		if(StatIndexInvalid(index)) return;

		stats.RemoveAt(index);

		Save();
	}

	public void AddStat()
	{
		stats.Add(new EquipStat());

		Save();
	}

	public void EditStat(int index, EquipStat stat)
	{
		if(StatIndexInvalid(index)) return;
		
		stats[index] = statEquivalencies.DefactorStat(stat);

		Save();
	}

	protected bool StatIndexInvalid(int index)
	{
		return (stats == null || index < 0 || index >= stats.Count);
	}

	private void Save()
	{
		#if UNITY_EDITOR
		EditorUtility.SetDirty(this);
		#endif
	}
}
