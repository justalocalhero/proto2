﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
    public class Component
    {
        public string name;
        public List<Resource> craftingCost = new List<Resource>();
        public List<EquipStat> stats = new List<EquipStat>();
        public RecipeCost recipeCost;
        
        public virtual void RegisterParent(Equipment parent)
        {

        }

        public void AddStats(List<EquipStat> newStats, StatEquivalencies statEquivalencies)
        {
            foreach(EquipStat stat in newStats)
            {
                AddStat(stat, statEquivalencies);
            }
        }

        protected void AddStat(EquipStat stat, StatEquivalencies statEquivalencies)
        {
            stats.Add(statEquivalencies.FactorStat(stat));
        }

        public void AddCost(List<Resource> costs)
        {
            craftingCost.AddRange(costs);
        }

        public List<EquipStat> GetStats()
        {
            return Utility.CloneList(stats);
        }

        public List<Resource> GetUpgradeCost()
        {
            if(recipeCost == null) return new List<Resource>();
            if(recipeCost.upgradeCost == null) return new List<Resource>();
            return Utility.CloneList(recipeCost.upgradeCost);
        }

        public List<Resource> GetCraftingCost()
        {
            if(recipeCost == null) return new List<Resource>();
            if(recipeCost.craftingCost == null) return new List<Resource>();
            return Utility.CloneList(recipeCost.craftingCost);
        }

        public bool IsDefault()
        {
            return stats.Count == 0;
        }	
    }
}
