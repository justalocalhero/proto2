﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	[CreateAssetMenu(menuName="Equipment/Enchantment")]
	public class EnchantmentBuilder : LootBuilder 
	{
		// public string enchantmentName;
		// public FloatReference difficulty;
		
		// public IntReference minLevel;
		// public IntReference maxLevel;

		// public RecipeCost recipeCost;

		// public bool craftingUnlocked {get; private set;}		

		// public override string GetName()
		// {
		// 	return enchantmentName;
		// }

		// public Enchantment Generate(int level)
		// {
		// 	Enchantment toReturn = new Enchantment(minLevel, maxLevel, level);
		// 	toReturn.builder = this;
		// 	toReturn.name = enchantmentName;
		// 	toReturn.AddStats(Utility.CloneList(stats), statEquivalencies);

		// 	return toReturn;
		// }

		// public List<Resource> GetCraftingCost()
		// {
		// 	return Utility.CloneList(recipeCost.craftingCost);
		// }

		// public List<Resource> GetUpgradeCost()
		// {
		// 	return Utility.CloneList(recipeCost.upgradeCost);
		// }

		// public void Unlock()
		// {
		// 	if(!craftingUnlocked) craftingUnlocked = true;
		// }

		// public void Lock()
		// {
		// 	if(craftingUnlocked) craftingUnlocked = false;
		// }

		// public bool CraftingUnlocked()
		// {
		// 	return craftingUnlocked;
		// }
	}
}