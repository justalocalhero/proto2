﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	[CreateAssetMenu(menuName="Equipment/Quality")]
	public class QualityBuilder : LootBuilder 
	{
		public IntReference minLevel;
		public IntReference maxLevel;

		public RecipeCost recipeCost;

		public override string GetName()
		{
			return "Quality";
		}

		public Quality Generate(int level)
		{
			Quality toReturn = new Quality(minLevel, maxLevel, level);

			toReturn.AddStats(Utility.CloneList(stats), statEquivalencies);

			return toReturn;
		}

		public List<Resource> GetUpgradecost()
		{
			return recipeCost.upgradeCost;
		}
	}
}