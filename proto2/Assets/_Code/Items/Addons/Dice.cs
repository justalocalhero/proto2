﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice
{

	public float Normal(float minValue, float maxValue, int rolls)
	{
		if(maxValue <= minValue) return 0;

		float valueRange = maxValue - minValue;

		float perRoll = (rolls > 0)? valueRange / rolls : 0;
		float r = minValue;

		for(int i = 0; i < rolls; i++)
		{
			r += UnityEngine.Random.Range(0, perRoll);
		}
		
		return r;
	}

	public int ChainRoll(float chance)
	{
		int roll = 0;

		bool keepRolling = true;

		while(keepRolling)
		{
			float r = UnityEngine.Random.Range(0, 1.0f);
			if(r < chance)
			{
				roll++;
				if(roll > 10) keepRolling = false;
			}
			else keepRolling = false;
		}

		return roll;
	}
}
