﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName="Dictionaries/EquipSlotMultipliers")]
public class EquipSlotMultiplierMap : ScriptableObject 
{
	public EquipSlotMultiplier[] multipliers = new EquipSlotMultiplier[System.Enum.GetNames(typeof(EquipSlot)).Length];

	public float GetMultiplier(EquipSlot slot)
	{
		if((int)slot < 0 || (int)slot >= multipliers.Length) 
		{
			Debug.LogWarning("Index Out of Range");
			return 0;
		}
		if(multipliers[(int)slot].slot != slot)
		{
			Debug.LogWarning("Slot missmatch");
			return 0;
		}
		return multipliers[(int)slot].multiplier;
	}
}

[Serializable]
public struct EquipSlotMultiplier {public EquipSlot slot; public float multiplier;}