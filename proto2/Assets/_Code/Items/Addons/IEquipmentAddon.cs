﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot 
{
	public interface IEquipmentAddon
	{
		void RegisterParent(Equipment parent);

	}
}
