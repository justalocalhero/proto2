﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{    
    [System.Serializable]
	public class Design : Component
	{
        private Ability ability;
        public DesignProfile profile;

        public Design(DesignProfile profile, StatEquivalencies statEquivalencies)
        {
            GenericAbilityBuilder abilityBuilder = new GenericAbilityBuilder();

            this.name = profile.name;
            this.recipeCost = profile.recipeCost;
            AddProfile(profile);
			AddStats(profile.stats, statEquivalencies);
			AddAbility(abilityBuilder.Generate(profile.abilityProfile));
        }

        public void Load(SaveFile saveFile)
        {
            if(saveFile == null) return;

            profile.Load(saveFile);
        }

        public void AddAbility(Ability ability)
        {
            this.ability = ability;
        }

        public override void RegisterParent(Equipment parent)
        {
            parent.design = this;
            parent.AdjustBaseStats(stats);
            if(ability != null) parent.AddAbility(ability);
            parent.spriteIndex = profile.spriteIndex;
        }

        public void AddProfile(DesignProfile profile)
        {
            this.profile = profile;
        }

        public bool CanUnlock()
        {
            return profile != null && profile.craftable && !profile.craftingUnlocked;
        }

        public void Unlock()
        {
            if(CanUnlock()) profile.craftingUnlocked = true;
        }
	}
}
