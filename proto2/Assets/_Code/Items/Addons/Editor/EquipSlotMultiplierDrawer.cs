﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(EquipSlotMultiplier))]
public class EquipSlotMultiplierDrawer : PropertyDrawer 
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int slotSize = 80;
        int multiplierSize = 50;
        int spacing = 5;
        int slotIndent = 0;
        int multiplierIndent = slotIndent + slotSize + spacing;

        Rect slotRect = new Rect(position.x + slotIndent, position.y, slotSize, position.height);
        Rect multiplierRect = new Rect(position.x + multiplierIndent, position.y, multiplierSize, position.height);

        EditorGUI.PropertyField(slotRect, property.FindPropertyRelative("slot"), GUIContent.none);
        EditorGUI.PropertyField(multiplierRect, property.FindPropertyRelative("multiplier"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}