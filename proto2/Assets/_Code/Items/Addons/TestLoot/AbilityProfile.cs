﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	[System.Serializable]
	public class AbilityProfile
	{
		public string name;
		public float period;
		public ProficiencyType proficiencyType;
		public int spriteIndex;
		public List<Damage> damages = new List<Damage>();

		public EffectProfile effectProfile = new EffectProfile();

		public int GetSprite()
		{
			return spriteIndex;
		}

	}
}