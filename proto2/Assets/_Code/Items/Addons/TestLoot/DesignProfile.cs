﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	[System.Serializable]
	public class DesignProfile : ComponentProfile
	{
		public int spriteIndex;
		public AbilityProfile abilityProfile = new AbilityProfile();

		public int GetSprite()
		{
			return spriteIndex;
		}
	}
}
