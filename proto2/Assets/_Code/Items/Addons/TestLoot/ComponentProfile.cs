﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	[System.Serializable]
	public class ComponentProfile
	{
		public string name;
		public bool craftable;
		public bool craftingUnlocked;
		public int difficulty = 0;
		public List<EquipStat> stats = new List<EquipStat>();
		public RecipeCost recipeCost;

		public virtual void Load(SaveFile saveFile)
		{
			
		}
	}
}