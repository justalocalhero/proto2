﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	public class GenericAbilityBuilder
	{
		public Ability Generate(AbilityProfile profile)
		{
			if(profile == null) return null;
			if(profile.period <= 0) return null;

			Action action = new Action(profile.name, profile.spriteIndex, profile.proficiencyType, new ActionCondition[0], 
				GenerateBuff(profile.effectProfile), GenerateDebuff(profile.effectProfile), 
				GenerateDamages(profile), GenerateSelfDamages(profile), 
				new ConditionalStat[0], new ConditionalResource[0], 0, 0, 0);
				
			Ability ability = new Ability(profile.name, profile.period, action, profile.spriteIndex);

			return ability;
		}

		public Damage[] GenerateDamages(AbilityProfile profile)
		{
			List<Damage> toReturn = new List<Damage>();

			foreach(Damage damage in profile.damages)
			{
				if(damage.type != DamageType.healing) toReturn.Add(damage);
			}

			return toReturn.ToArray();
		}

		public Damage[] GenerateSelfDamages(AbilityProfile profile)
		{
			List<Damage> toReturn = new List<Damage>();

			foreach(Damage damage in profile.damages)
			{
				if(damage.type == DamageType.healing) toReturn.Add(damage);
			}
			
			return toReturn.ToArray();

		}

		public Buff[] GenerateBuff(EffectProfile profile)
		{
			if(profile.effectType != EffectProfile.EffectType.buff)  return new Buff[0];

			Buff effect = GetBuff(profile);

			return new Buff[]{effect};
		}

		public Debuff[] GenerateDebuff(EffectProfile profile)
		{			
			if(profile.effectType != EffectProfile.EffectType.debuff)  return new Debuff[0];

			Debuff effect = GetDebuff(profile);

			return new Debuff[]{effect};
		}

		public Buff GetBuff(EffectProfile profile)
		{
			return new Buff(profile.name, profile.duration, profile.period, 0, profile.stats.ToArray(), new Damage[0], profile.damages.ToArray(), profile.spriteIndex);
		}
		public Debuff GetDebuff(EffectProfile profile)
		{
			return new Debuff(profile.name, profile.duration, profile.period, 0, profile.stats.ToArray(), new Damage[0], profile.damages.ToArray(), profile.spriteIndex);
		}
	}
}