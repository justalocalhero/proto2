﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	[System.Serializable]
	public class EffectProfile 
	{		
		public EffectType effectType;
		public string name;
		public float duration;
		public float period;
		public int spriteIndex;
		public List<Damage> damages = new List<Damage>();
		public List<EquipStat> stats = new List<EquipStat>();

		public int GetSprite()
		{
			return spriteIndex;
		}

		public enum EffectType {none, buff, debuff}
	}
}