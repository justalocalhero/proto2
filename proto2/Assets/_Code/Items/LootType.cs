﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	[CreateAssetMenu(menuName="Loot/LootType")]
	public class LootType : ScriptableObject 
	{
		public float physical;
		public float magical;
		public float light;
		public float heavy;
	}
}
