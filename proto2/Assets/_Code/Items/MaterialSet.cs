﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Loot
{
	[CreateAssetMenu(menuName="Loot/MaterialSet")]
	public class MaterialSet : ScriptableObject
	{

		// public List<MaterialCatalogEntry> entries = new List<MaterialCatalogEntry>();

		// public List<MaterialCatalogEntry> GetEntries(int difficulty)
		// {
		// 	List<MaterialCatalogEntry> toReturn = new List<MaterialCatalogEntry>();

		// 	foreach(MaterialCatalogEntry entry in entries)
		// 	{
		// 		if(entry.builder.difficulty <= difficulty) toReturn.Add(entry);
		// 	}

		// 	return toReturn;
		// }

		// public List<MaterialBuilder> GetCraftableMaterials()
		// {
		// 	List<MaterialBuilder> toReturn = new List<MaterialBuilder>();

		// 	foreach(MaterialCatalogEntry entry in entries)
		// 	{
		// 		if(entry.builder.CraftingUnlocked()) toReturn.Add(entry.builder);
		// 	}

		// 	return toReturn;
		// }

		// public List<MaterialBuilder> GetMaterials()
		// {
		// 	List<MaterialBuilder> toReturn = new List<MaterialBuilder>();

		// 	foreach(MaterialCatalogEntry entry in entries)
		// 	{
		// 		toReturn.Add(entry.builder);
		// 	}

		// 	return toReturn;
		// }
	}

	[Serializable]
	public struct MaterialCatalogEntry {public MaterialBuilder builder; public float frequency;}
}