﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Loot
{
	[CreateAssetMenu(menuName="Loot/EnchantmentSet")]
	public class EnchantmentSet : ScriptableObject
	{
		// public string setName;
		
		// public List<EnchantmentCatalogEntry> entries = new List<EnchantmentCatalogEntry>();

		// public List<EnchantmentCatalogEntry> GetEntries(int difficulty)
		// {
		// 	List<EnchantmentCatalogEntry> toReturn = new List<EnchantmentCatalogEntry>();

		// 	foreach(EnchantmentCatalogEntry entry in entries)
		// 	{
		// 		if(entry.builder.difficulty <= difficulty) toReturn.Add(entry);
		// 	}

		// 	return toReturn;
		// }

		// public bool CraftingUnlocked()
		// {
		// 	foreach(EnchantmentCatalogEntry entry in entries)
		// 	{
		// 		if(entry.builder.CraftingUnlocked()) return true;
		// 	}

		// 	return false;
		// }

		// public List<EnchantmentBuilder> GetCraftableBuilders()
		// {
		// 	List<EnchantmentBuilder> toReturn = new List<EnchantmentBuilder>();

		// 	foreach(EnchantmentCatalogEntry entry in entries)
		// 	{
		// 		if(entry.builder.CraftingUnlocked()) toReturn.Add(entry.builder);
		// 	}

		// 	return toReturn;
		// }

		// public List<EnchantmentBuilder> GetEnchantments()
		// {
		// 	List<EnchantmentBuilder> toReturn = new List<EnchantmentBuilder>();

		// 	foreach(EnchantmentCatalogEntry entry in entries)
		// 	{
		// 		toReturn.Add(entry.builder);
		// 	}

		// 	return toReturn;
		// }

		// public bool CanAfford(ResourceManager resourceManager)
		// {
		// 	foreach(EnchantmentCatalogEntry entry in entries)
		// 	{
		// 		if(resourceManager.CheckChange(entry.builder.GetCraftingCost())) return true;
		// 	}

		// 	return false;
		// }

		// public bool Contains(EnchantmentBuilder enchantmentBuilder)
		// {
		// 	foreach(EnchantmentCatalogEntry entry in entries)
		// 	{
		// 		if(entry.builder == enchantmentBuilder) return true;
		// 	}
		// 	return false;
		// }
	}

	[Serializable]
	public struct EnchantmentCatalogEntry {public EnchantmentBuilder builder; public float frequency;}
}