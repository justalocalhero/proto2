﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
    [CreateAssetMenu(menuName="Loot/EnchantmentCatalog")]
    public class EnchantmentCatalog : ScriptableObject
    {
        public List<EnchantmentSet> sets;

        public EnchantmentBuilder GetEnchantment(int difficulty)
        {
            return GetRandomBuilder(GetEntries(difficulty));
        }

        private List<EnchantmentCatalogEntry> GetEntries(int difficulty)
        {
            List<EnchantmentCatalogEntry> toReturn = new List<EnchantmentCatalogEntry>();

            // foreach(EnchantmentSet set in sets)
            // {
            //    toReturn.AddRange(set.GetEntries(difficulty));
            // }

            return toReturn;
        }

        private EnchantmentBuilder GetRandomBuilder(List<EnchantmentCatalogEntry> entries)
		{
			float total = 0;

			foreach(EnchantmentCatalogEntry entry in entries)
			{
				total += entry.frequency;
			}

			float r = UnityEngine.Random.Range(0, total);

			foreach(EnchantmentCatalogEntry entry in entries)
			{
				if(r < entry.frequency) return entry.builder;
				else r -= entry.frequency;
			}

			return null;
		}

        public List<EnchantmentSet> GetCraftableSets()
		{
			List<EnchantmentSet> toReturn = new List<EnchantmentSet>();

			// foreach(EnchantmentSet set in sets)
			// {
			// 	if(set.CraftingUnlocked()) toReturn.Add(set);
			// }

			return toReturn;
		}

		public List<EnchantmentBuilder> GetEnchantments()
		{
			List<EnchantmentBuilder> toReturn = new List<EnchantmentBuilder>();

            // foreach(EnchantmentSet set in sets)
            // {
            //     toReturn.AddRange(set.GetEnchantments());
            // }

            return toReturn;
		}
        
		public bool Contains(EnchantmentSet enchantmentSet)
		{
			foreach(EnchantmentSet set in sets)
			{
				if(set == enchantmentSet) return true;
			}
			return false;
		}

		public bool Contains(EnchantmentBuilder enchantmentBuilder)
		{
			// foreach(EnchantmentSet set in sets)
			// {
			// 	if(set.Contains(enchantmentBuilder)) return true;
			// }

			return false;
		}
    }
}