﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArtifactManager : MonoBehaviour 
{
	public SaveManager saveManager;
	public List<ArtifactEquipmentGenerator> defaultArtifacts;
	public Forge forge;
	public List<Battleground> battlegrounds;
	public List<Equipment> equipments;
	public FloatReference dropChance;
	public IntReference maxArtifacts;

	public void Awake()
	{
        if(saveManager != null)
        {
            saveManager.onPreSave += Save;
            saveManager.onLoad += Load;
        }

		foreach(Battleground battleground in battlegrounds)
		{
			RegisterBattleground(battleground);
		}

		forge.onEquipmentDismantled += Add;

		AddDefaultArtifacts();
	}

	public void Save(SaveFile saveFile)
	{
		if(saveFile == null) return;

        saveFile.artifactSave = equipments.ToArray();

	}

	public void Load(SaveFile saveFile)
	{
		if(saveFile == null) return;
		if(saveFile.artifactSave == null) return;

		Clear();

		foreach(Equipment equipment in saveFile.artifactSave)
		{
			Add(equipment);
		}
	}

	public void RegisterBattleground(Battleground battleground)
	{
		if(battleground == null) return;

		battleground.onPlayerDeath += HandlePlayer;
		battleground.onAscended += HandlePlayer;
	}

	public void Clear()
	{
		equipments = new List<Equipment>();
	}

	private void HandlePlayer(Player player)
	{
		if(player == null || player.equipment == null) return;
		foreach(Equipment equipment in player.equipment)
		{
			if(equipment.rarity == Rarity.artifact) Add(equipment);	
		}
	}

	public bool CheckDrop()
	{
		if(equipments.Count <= 0) return false;

		float r = UnityEngine.Random.Range(0,1f);
		float failChance = 1 - dropChance;
		float totalFailChance = Mathf.Pow(failChance, equipments.Count);
		float successChance = 1 - totalFailChance;

		return r < successChance;

	}

	public Equipment GetDrop()
	{
		if(equipments.Count > 0) return Utility.RemoveRandomFromList(equipments);
		return null;
	}

	public void AddDefaultArtifacts()
	{
		foreach(ArtifactEquipmentGenerator gen in defaultArtifacts)
		{
			Add(gen.Generate());
		}
	}

	public void Add(Equipment equipment)
	{
		if(equipment == null) return;

		if(CanAdd(equipment)) equipments.Add(equipment);
	}

	public bool CanAdd(Equipment equipment)
	{
		if(equipments.Count >= maxArtifacts) return false;
		if(equipment.GetRarity() != Rarity.artifact) return false;

		foreach(Equipment e in equipments)
		{
			if(e.GetName() == equipment.GetName()) 
			{
				return false;
			}
		}

		return true;
	}
}
