﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Loot;

public class CraftingSaver : MonoBehaviour 
{
	public SaveManager saveManager;
	public CraftingManager craftingManager;

	public delegate void OnLoad();
	public OnLoad onLoad;

	public void RaiseOnLoad()
	{
		if(onLoad != null) 
			onLoad();
	}

	void Awake () 
	{
		saveManager.onPreSave += Save;
		saveManager.onLoad += Load;
	}
	
	private void Save(SaveFile saveFile)
	{
		craftingManager.Save(saveFile);
	}

	private void Load(SaveFile saveFile)
	{
		craftingManager.Load(saveFile);

		RaiseOnLoad();
	}
}
