﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Loot;

public class EnchantmentUnlocker : MonoBehaviour 
{
	public Forge forge;

	public FloatReference unlockChance;

	public delegate void OnUnlocked(string name);
	public OnUnlocked onUnlocked;

	public void RaiseOnUnlocked(string name)
	{
		if(onUnlocked != null) onUnlocked(name);
	}

	public void Start()
	{
		forge.onEquipmentDismantled += TryUnlock;
	}	

	private void TryUnlock(Equipment equipment)
	{
		if(equipment == null) return;
		if(equipment.enchantment == null) return;
		
		if(UnityEngine.Random.Range(0, 1.0f) < unlockChance && equipment.enchantment.CanUnlock())
		{
			equipment.enchantment.Unlock();
			RaiseOnUnlocked(equipment.enchantment.name);
		}
	}
}
