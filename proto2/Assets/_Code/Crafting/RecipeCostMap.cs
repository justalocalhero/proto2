﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Crafting/RecipeCostMap")]
public class RecipeCostMap : ScriptableObject
{
	public List<RecipeCost> recipeCosts;

	public List<string> GetNames()
	{
		List<string> toReturn = new List<string>();

		foreach(RecipeCost cost in recipeCosts)
		{
			toReturn.Add(cost.name);
		}

		return toReturn;
	}

	public int GetIndex(RecipeCost recipeCost)
	{
		for(int i = 0; i < recipeCosts.Count; i++)
		{
			if(recipeCosts[i] == recipeCost) return i;
		}

		Debug.LogWarning("Recipe Cost not found.");
		return 0;
	}
}
