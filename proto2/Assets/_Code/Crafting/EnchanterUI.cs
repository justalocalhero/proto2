﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Loot;
using TMPro;
using Tooltips;

public class EnchanterUI : MonoBehaviour 
{
	public UIToggler toggler;
	public CraftingBench craftingBench;
	public TextMeshProUGUI costText, chanceText;
	public Button enchantButton;
	public IntReference enchantmentCount, enchantmentCount2, enchantmentCount3;
	public FloatReference enchantmentRadius, enchantmentRadius2, enchantmentRadius3;
	public Transform orbitContainer;
	public OrbitSlot orbitPrototype;
	private List<OrbitSlot> enchantmentSlots;
	public ResourceManager resourceManager;
	public Enchanter enchanter;
	public ColorReference selectedColor, availableColor, unavailableColor, availableIconColor, unavailableIconColor, positiveColor, negativeColor;
	public Sprite enchantmentSprite;
	public ComponentProfileTooltip componentTooltip;
	public ComponentProfileText componentText;

	public void Awake()
	{
		BuildOrbits();
		enchantButton.onClick.AddListener(HandleEnchant);
		resourceManager.onResourceChanged += UpdateUI;
		enchanter.onEnchantmentChanged += UpdateEnchantmentSlots;
		enchanter.onEnchantmentChanged += UpdateText;
		enchanter.onEnchantmentChanged += UpdateEnchantmentButton;
		toggler.onShow += craftingBench.SetAsRouterTarget;
		toggler.onShow += UpdateUI;
		
	}

	public void Start()
	{
		UpdateUI();
	}

	public void BuildOrbits()
	{
		enchantmentSlots = BuildOrbitSlots(orbitContainer, orbitPrototype, 0, enchantmentCount, enchantmentRadius, HandleEnchantmentClick, HandleEnchantmentEnter, HandleEnchantmentExit);
		enchantmentSlots.AddRange(BuildOrbitSlots(orbitContainer, orbitPrototype, enchantmentSlots.Count, enchantmentCount2, enchantmentRadius2, HandleEnchantmentClick, HandleEnchantmentEnter, HandleEnchantmentExit));
		enchantmentSlots.AddRange(BuildOrbitSlots(orbitContainer, orbitPrototype, enchantmentSlots.Count, enchantmentCount3, enchantmentRadius3, HandleEnchantmentClick, HandleEnchantmentEnter, HandleEnchantmentExit));
	}
	
	public List<OrbitSlot> BuildOrbitSlots(Transform container, OrbitSlot prototype, int startIndex, int count, float radius, OrbitSlot.OnClicked onClick, OrbitSlot.OnEnter onEnter, OrbitSlot.OnExit onExit)
	{
		RingBuilder<OrbitSlot> builder = new RingBuilder<OrbitSlot>();

		List<OrbitSlot> toReturn = builder
			.WithContainer(container)
			.WithPrototype(prototype)
			.WithCount(count)
			.WithRadius(radius)
			.Build();

		int index = startIndex;

		foreach(OrbitSlot slot in toReturn)
		{
			slot.index = index++;
			slot.onClicked += onClick;
			slot.onEnter += onEnter;
			slot.onExit += onExit;
		}

		return toReturn;
	}

	public void UpdateUI()
	{
		if(!toggler.IsVisible()) return;
		UpdateEnchantmentSlots();
		UpdateText();
		UpdateEnchantmentButton();
	}

	public void UpdateEnchantmentSlots()
	{
		List<Entry<EnchantmentProfile>> profiles = enchanter.GetCraftableEnchantments();
		int selectedIndex = enchanter.GetSelectedEnchantmentIndex();

		for(int i = 0; i < enchantmentSlots.Count; i++)
		{
			OrbitSlot current = enchantmentSlots[i];

			if(i < profiles.Count)
			{
				if(i == selectedIndex)
				{
					current.SetButtonColor(selectedColor);
					current.SetIconColor(availableIconColor);
				}
				else if(CheckResources(profiles[i].payload))
				{
					current.SetButtonColor(availableColor);
					current.SetIconColor(availableIconColor);
				}
				else
				{
					current.SetButtonColor(unavailableColor);
					current.SetIconColor(unavailableIconColor);
				}

				current.SetIcon(enchantmentSprite);			
				current.Show();
			}
			else
			{
				current.Hide();
			}
		}
	}

	public void UpdateText()
	{
		ComponentProfile profile = null;

		if(enchanter.HasEquipment())
		{
			
		}
		else
		{
			ClearText();
			return;
		}

		if(enchanter.HasSelectedEnchantment())
		{
			profile = enchanter.GetSelectedEnchantment().payload;
		}
		else
		{
			ClearText();
			return;
		}

		costText.SetText(componentText.CraftingCostString(profile, resourceManager, "\n", "", ""));
		chanceText.SetText(ChanceString());

	}

	public void UpdateEnchantmentButton()
	{
		bool buttonActive = enchanter.HasEquipment() && enchanter.HasSelectedEnchantment();		

		enchantButton.interactable = buttonActive;
	}

	private void ClearText()
	{
		costText.SetText("");
		chanceText.SetText("");

	}

	private string ChanceString()
	{
		string text = "";

		text += Tooltips.ColoredText.GetText(Utility.ToPercent(enchanter.defaultFailChance.Value) + " chance for failure.\n", negativeColor);
		text += Tooltips.ColoredText.GetText(Utility.ToPercent(enchanter.qualityUp.Value) + " chance for amplification.\n", positiveColor);

		return text;
	}

	public void HandleEnchantmentClick(int index)
	{
		List<Entry<EnchantmentProfile>> profiles = enchanter.GetCraftableEnchantments();
		EnchantmentProfile profile = profiles[index].payload;

		if(CheckResources(profile))
			enchanter.SelectEnchantment(index);
	}

	public void HandleEnchantmentEnter(int index)
	{
		OrbitSlot slot = enchantmentSlots[index];
		List<Entry<EnchantmentProfile>> profiles = enchanter.GetCraftableEnchantments();
		EnchantmentProfile profile = profiles[index].payload;

		Equipment equipment = enchanter.GetEquipment();
		Enchantment enchantment = (equipment != null) ? equipment.enchantment : null;
		EnchantmentProfile replaced = (enchantment != null) ? enchantment.profile : null;

		if(replaced != null)
		{
			componentTooltip.SetCraft(profile, replaced, equipment.slot, resourceManager, slot.rect);
		}
		else
		{
			componentTooltip.SetCraft(profile, equipment.slot, resourceManager, slot.rect);
		}

	}

	public void HandleEnchantmentExit(int index)
	{
		componentTooltip.Hide();
	}

	public bool CheckResources(EnchantmentProfile profile)
	{
		return resourceManager.CheckChange(profile.recipeCost.craftingCost);
	}

	public void HandleEnchant()
	{
		enchanter.Enchant();
	}
}