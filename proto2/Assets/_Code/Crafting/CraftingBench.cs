﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftingBench : MonoBehaviour, IEquipmentReciever
{
	public EquipmentRouter equipmentRouter;
	public Equipment equipment {get; set;}
	public Crafter crafter;
	public Enchanter enchanter;
	public Diviner diviner;

	public delegate void OnChanged();
	public OnChanged onChanged;

	public void Start()
	{
		crafter.onCraft += Recieve;
		enchanter.onCraft += RaiseOnChanged;
		diviner.onCraft += RaiseOnChanged;
	}

	private void Recieve(Equipment equipment)
	{
		if(equipment == null) return;
		if(equipment.rarity != Rarity.ordinary) return;
		this.equipment = equipment;

		RaiseOnChanged();
	}

	public void Recieve(Equipment equipment, Inventory inventory)
	{
		Recieve(equipment);
	}

	public void Clear()
	{
		equipment = null;

		RaiseOnChanged();
	}

	public bool HasEquipment()
	{
		return equipment != null;
	}

	public void RaiseOnChanged(Equipment equipment)
	{
		if(onChanged != null) onChanged();
	}

	public void RaiseOnChanged()
	{
		if(onChanged != null) onChanged();
	}

	public void SetAsRouterTarget()
	{
		equipmentRouter.SetActiveReciever(this);
	}
}
