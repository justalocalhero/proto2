﻿using System.Collections;
using System.Collections.Generic;
using Loot;
using TMPro;
using Tooltips;
using UnityEngine;
using UnityEngine.UI;

public class DivinerUI : MonoBehaviour 
{
	public UIToggler toggler;
	public CraftingBench craftingBench;
	public TextMeshProUGUI costText, chanceText;
	public Button blessButton;
	public Transform orbitContainer;
	public OrbitSlot orbitPrototype;
	private OrbitSlot blessingSlot;
	public ResourceManager resourceManager;
	public Diviner diviner;
	public ColorReference selectedColor, availableColor, unavailableColor,  availableIconColor, unavailableIconColor, positiveColor, negativeColor;
	public Sprite blessingSprite;
	public ComponentProfileTooltip componentTooltip;
	public ComponentProfileText componentText;

	public void Awake()
	{
		BuildOrbits();
		blessButton.onClick.AddListener(HandleBless);
		resourceManager.onResourceChanged += UpdateUI;
		diviner.onEquipmentChanged += UpdateUI;
		toggler.onShow += craftingBench.SetAsRouterTarget;
		toggler.onShow += UpdateUI;
	}

	public void Start()
	{
		UpdateUI();
	}

	public void BuildOrbits()
	{
		blessingSlot = BuildOrbitSlot(orbitContainer, orbitPrototype, HandleDivinityEnter, HandleDivinityExit);
	}
	
	public OrbitSlot BuildOrbitSlot(Transform container, OrbitSlot prototype, OrbitSlot.OnEnter onEnter, OrbitSlot.OnExit onExit)
	{
		OrbitSlot slot = Object.Instantiate(prototype, container, false);

		slot.index = 0;
		slot.onEnter += onEnter;
		slot.onExit += onExit;
		

		return slot;
	}

	public void UpdateUI()
	{
		if(!toggler.IsVisible()) return;
		UpdateBlessingSlot();
		UpdateText();
		UpdateBlessingButton();
	}

	public void UpdateBlessingSlot()
	{
		bool hasDivinity = diviner.HasDivinity();
		bool canAfford = hasDivinity && CheckResources(diviner.GetDivinity());

		if(hasDivinity)
		{
			if(canAfford)
			{
				blessingSlot.SetButtonColor(selectedColor);
				blessingSlot.SetIconColor(availableIconColor);
			}
			else
			{
				blessingSlot.SetButtonColor(unavailableColor);
				blessingSlot.SetIconColor(unavailableIconColor);
			}

			blessingSlot.SetIcon(blessingSprite);			
			blessingSlot.Show();
		}
		else
		{
			blessingSlot.Hide();
		}
		
	}

	public void UpdateText()
	{
		ComponentProfile profile = null;

		if(diviner.HasEquipment())
		{
			
		}
		else
		{
			ClearText();
			return;
		}

		if(diviner.HasDivinity())
		{
			profile = diviner.GetDivinity();
		}
		else
		{
			ClearText();
			return;
		}

		costText.SetText(componentText.CraftingCostString(profile, resourceManager, "\n", "", ""));
		chanceText.SetText(ChanceString());

	}

	public void UpdateBlessingButton()
	{
		blessButton.interactable = diviner.CanBless();
	}

	private void ClearText()
	{
		costText.SetText("");
		chanceText.SetText("");

	}

	private string ChanceString()
	{
		string text = "";

		text += Tooltips.ColoredText.GetText(Utility.ToPercent(diviner.defaultFailChance.Value) + " chance for failure.\n", negativeColor);
		

		return text;
	}

	public void HandleDivinityEnter(int index)
	{
		if(!diviner.HasEquipment())
			return;
		if(!diviner.HasDivinity())
			return;

		EquipSlot slot = diviner.GetEquipment().slot;
		DivinityProfile profile = diviner.GetDivinity();		

		componentTooltip.SetCraft(profile, slot, resourceManager, blessingSlot.rect);
	}

	public void HandleDivinityExit(int index)
	{
		componentTooltip.Hide();
	}

	public bool CheckResources(DivinityProfile profile)
	{
		if(profile == null) return false;
		if(profile.recipeCost == null) return false;
		return resourceManager.CheckChange(profile.recipeCost.craftingCost);
	}

	public void HandleBless()
	{
		diviner.Bless();
	}
}