﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(menuName="Crafting/RecipeCost")]
[System.Serializable]
public class RecipeCost : ScriptableObject 
{
	public List<Resource> craftingCost;
	public List<Resource> upgradeCost;
}
