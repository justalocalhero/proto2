﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PointerHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public RectTransform rect { get; private set; }
	public delegate void OnEnter();
	public  OnEnter onEnter;

	public delegate void OnExit();
	public OnExit onExit;

	public delegate void OnClick();
	public OnClick onClick;

    public void Awake()
    {
        rect = transform as RectTransform;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(onEnter != null) onEnter();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(onExit != null) onExit();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if(onClick != null) onClick();
    }
}
