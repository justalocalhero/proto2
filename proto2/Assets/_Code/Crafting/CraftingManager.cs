﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace Loot
{
	[CreateAssetMenu(menuName="Crafting/CraftingManager")]
	public class CraftingManager : ScriptableObject
	{
		public DesignDictionary designDictionary;
		public QualityDictionary qualityDictionary;
		public DivinityDictionary divinityDictionary;
		public EnchantmentDictionary enchantmentDictionary;
		public MaterialDictionary materialDictionary;

		public void Save(SaveFile saveFile)
		{
			if(saveFile == null) return;

			saveFile.designSave = designDictionary.Save();
			saveFile.qualitySave = qualityDictionary.Save();
			saveFile.divinitySave = divinityDictionary.Save();
			saveFile.enchantmentSave = enchantmentDictionary.Save();
			saveFile.materialSave = materialDictionary.Save();
		}

		public void Load(SaveFile saveFile)
		{
			if(saveFile == null) return;

			if(saveFile.designSave != null) designDictionary.Load(saveFile, saveFile.designSave);
			if(saveFile.qualitySave != null) qualityDictionary.Load(saveFile, saveFile.qualitySave);
			if(saveFile.divinitySave != null) divinityDictionary.Load(saveFile, saveFile.divinitySave);
			if(saveFile.enchantmentSave != null) enchantmentDictionary.Load(saveFile, saveFile.enchantmentSave);
			if(saveFile.materialSave != null) materialDictionary.Load(saveFile, saveFile.materialSave);
		}

		public List<Entry<DesignProfile>> GetCraftableDesigns(List<Tag> tags)
		{
			return designDictionary.GetCraftable(tags);
		}

		public List<Entry<QualityProfile>> GetCraftableQualities(List<Tag> tags)
		{
			return qualityDictionary.Get(tags);
		}

		public List<Entry<DivinityProfile>> GetCraftableDivinities(List<Tag> tags)
		{
			return divinityDictionary.Get(tags);
		}

		public List<Entry<EnchantmentProfile>> GetCraftableEnchantments(List<Tag> tags)
		{
			return enchantmentDictionary.GetCraftable(tags);
		}

		public List<Entry<MaterialProfile>> GetCraftableMaterials(List<Tag> tags)
		{
			return materialDictionary.GetCraftable(tags);
		}

		public List<Tag> GetTags(EquipSlot slot)
		{
			List<SlotCategories> slots = new List<SlotCategories>();
			slots.Add(GetSlotCategory(slot));

			List<Tag> tags = Tag.GetTags(slots);

			return tags;
		}
		
		public List<Entry<DesignProfile>> GetCraftableDesigns(EquipSlot slot)
		{
			return GetCraftableDesigns(GetTags(slot));
		}

		public List<Entry<DesignProfile>> GetCraftableMaterials(EquipSlot slot, Entry<DesignProfile> entry)
		{

			List<StatCategories> stats = Enum.GetValues(typeof(StatCategories)).Cast<StatCategories>().ToList();
			List<EquipCategories> equips = Enum.GetValues(typeof(EquipCategories)).Cast<EquipCategories>().ToList();
			List<DamageCategories> damages = Enum.GetValues(typeof(DamageCategories)).Cast<DamageCategories>().ToList();
			
			List<SlotCategories> slots = new List<SlotCategories>();
			slots.Add(GetSlotCategory(slot));

			List<Tag> tags = Tag.GetTags(stats, equips, slots, damages);

			return GetCraftableDesigns(tags);
		}
		
		public List<Entry<EnchantmentProfile>> GetCraftableEnchantments(Equipment equipment)
		{
			return GetCraftableEnchantments(equipment.tags);
		}

		public SlotCategories GetSlotCategory(EquipSlot slot)
		{
			int index = (int) slot;
			return (SlotCategories) (index + 1);
		}

		public List<EquipSlot> GetCraftableSlots()
		{
			List<EquipSlot> toReturn = new List<EquipSlot>();

			int slotCount = Enum.GetNames(typeof(EquipSlot)).Length;

			for(int i = 0; i < slotCount; i++)
			{
				List<Entry<DesignProfile>> designs = GetCraftableDesigns((EquipSlot)i);

				if(designs.Count > 0) toReturn.Add((EquipSlot)i);
			}

			return toReturn;
		}
	}
}