﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Loot;

public class Enchanter : MonoBehaviour 
{
    public CraftingManager craftingManager;
    public ResourceManager resourceManager;
    public CraftingBench craftingBench;

    private List<Entry<EnchantmentProfile>> craftableEnchantments = new List<Entry<EnchantmentProfile>>();
    private int selectedEnchantmentIndex = -1;

    public delegate void OnCraft(Equipment equipment);
	public OnCraft onCraft;	

    public delegate void OnEquipmentChanged();
    public OnEquipmentChanged onEquipmentChanged;
    
    public delegate void OnEnchantmentChanged();
    public OnEnchantmentChanged onEnchantmentChanged;

    public delegate void OnFail();
    public OnFail onFail;

	private Dice dice = new Dice();
	public FloatVariable qualityUp, defaultFailChance;

	public StatEquivalencies statEquivalencies;

	public IntReference levelMin, levelMax;

	public void RaiseOnCraft(Equipment equipment)
	{
		if(onCraft != null) onCraft(equipment);
	}

	public void RaiseOnEquipmentChanged()
	{
		if(onEquipmentChanged != null) onEquipmentChanged();
	}

	public void RaiseOnEnchantmentChanged()
	{
		if(onEnchantmentChanged != null) onEnchantmentChanged();
	}

    public void RaiseOnFailed()
    {
        if(onFail != null) onFail();
    }

    public void Awake()
    {
        craftingBench.onChanged += UpdateEnchantments;
        craftingBench.onChanged += UpdateEquipment;
    }
    
    public void Start()
    {
        UpdateEnchantments();
    }
    
    public void UpdateEquipment()
    {
        EditEnchantment(-1);
        UpdateEnchantments();
        RaiseOnEquipmentChanged();
    }

    public void UpdateEnchantments()
	{
        if(!HasEquipment())
        {
            craftableEnchantments = new List<Entry<EnchantmentProfile>>();
            selectedEnchantmentIndex = -1;
            RaiseOnEnchantmentChanged();

            return;
        }

        Entry<EnchantmentProfile> temp  = new Entry<EnchantmentProfile>();
		bool hadSelected = HasSelectedEnchantment();
		if(hadSelected) 
            temp = GetSelectedEnchantment();

		craftableEnchantments = craftingManager.GetCraftableEnchantments(craftingBench.equipment.tags);

		if(hadSelected)
		{
			selectedEnchantmentIndex = -1;
			
			for(int i = 0; i < craftableEnchantments.Count; i++)
			{
				if(craftableEnchantments[i].payload == temp.payload)
				{
					selectedEnchantmentIndex = i;
					break;
				}
			}
		}

		RaiseOnEnchantmentChanged();
	}

    private void EditEnchantment(int index)
	{
		if(index == selectedEnchantmentIndex) return;

		selectedEnchantmentIndex = index;

	}

	public void SelectEnchantment(int index)
	{
		EditEnchantment(index);

		RaiseOnEnchantmentChanged();
	}

    public bool HasEquipment()
    {
        return craftingBench.equipment != null;
    }

    public Equipment GetEquipment()
    {
        return craftingBench.equipment;
    }

    public bool HasSelectedEnchantment()
    {
        return (selectedEnchantmentIndex >= 0 && selectedEnchantmentIndex < craftableEnchantments.Count);
    }

    public void Enchant()
    {
        if(!HasEquipment())
        {
            return;
        }
        if(!HasSelectedEnchantment())
        {
            return;
        }

        Enchantment enchantment = new Enchantment(GetSelectedEnchantment().payload, statEquivalencies, levelMin, levelMax, GetEnchantmentLevel());

        if(resourceManager.CheckChange(enchantment.GetCraftingCost()))
        {
            resourceManager.ApplyChange(enchantment.GetCraftingCost());

            if(!GetFailed())
            {
                enchantment.RegisterParent(craftingBench.equipment);
                RaiseOnCraft(craftingBench.equipment);
            }
            else
            {
                RaiseOnFailed();
            }
        }
    }
    
    public Entry<EnchantmentProfile> GetSelectedEnchantment()
    {
        return craftableEnchantments[selectedEnchantmentIndex];
    }

	public List<Entry<EnchantmentProfile>> GetCraftableEnchantments()
	{
		return craftableEnchantments;
	}

    public int GetSelectedEnchantmentIndex()
    {
        return selectedEnchantmentIndex;
    }

	public int GetEnchantmentLevel()
	{
		int maxLevel = dice.ChainRoll(qualityUp.Value);

		int level = maxLevel;

		return level;		
	}

    public bool GetFailed()
    {
        float r = UnityEngine.Random.Range(0f, 1f);

        return (r < defaultFailChance.Value);
    }
}
