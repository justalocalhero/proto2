﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Tooltips;

public class CraftingBenchUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public SpriteManager spriteManager;
	private RectTransform rect;
	public CraftingBench craftingBench;
	public Image frame;
	public Image icon;
	public EquipmentTooltip tooltip;
	public UIToggler toggler;

	public void Start()
	{
		rect = transform as RectTransform;
		craftingBench.onChanged += UpdateUI;
	}

	public void UpdateUI()
	{
		UpdateIcon();
	}

	public void UpdateIcon()
	{
		if(craftingBench.HasEquipment())
		{
			icon.sprite = spriteManager.GetSprite(craftingBench.equipment.GetSprite());
			toggler.Show();
		}
		else
		{
			toggler.Hide();
		}
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(craftingBench.HasEquipment())
		{
			tooltip.Set(craftingBench.equipment, rect);
		}
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tooltip.Hide();
    }
	
}
