﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class OrbitSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public int index {get; set; }
	public RectTransform rect {get; set;}

	public Button button;
	public Image icon;

	public UIToggler toggler;

	public delegate void OnClicked(int index);
	public OnClicked onClicked;

	public void RaiseOnClicked()
	{
		if(onClicked != null) onClicked(index);
	}

	public delegate void OnEnter(int index);
	public OnEnter onEnter;

	public void RaiseOnEnter()
	{
		if(onEnter != null) onEnter(index);
	}

	public delegate void OnExit(int index);
	public OnExit onExit;

	public void RaiseOnExit()
	{
		if(onExit != null) onExit(index);
	}

	public void Awake()
	{
		rect = transform as RectTransform;
		button.onClick.AddListener(HandleClick);
	}

	public void SetIcon(Sprite sprite)
	{
		icon.sprite = sprite;
	}
	
	public void HandleClick()
	{
		RaiseOnClicked();
	}

	public void SetButtonColor(Color color)
	{
		button.image.color = color;
	}

	public void SetIconColor(Color color)
	{
		icon.color = color;
	}

	public void Show()
	{
		toggler.Show();
	}

	public void Hide()
	{
		toggler.Hide();
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
        RaiseOnEnter();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        RaiseOnExit();
    }
}
