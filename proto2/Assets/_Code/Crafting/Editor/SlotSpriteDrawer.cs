﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(SlotSprite))]
public class SlotSpriteDrawer : PropertyDrawer {

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int slotSize = 80;
        int spriteSize = 80;
        int spacing = 5;
        int slotIndent = 0;
        int spriteIndent = slotIndent + slotSize + spacing;

        Rect slotRect = new Rect(position.x, position.y, slotSize, position.height);
        Rect spriteRect = new Rect(position.x + spriteIndent, position.y, spriteSize, position.height);

        EditorGUI.PropertyField(slotRect, property.FindPropertyRelative("slot"), GUIContent.none);
        EditorGUI.PropertyField(spriteRect, property.FindPropertyRelative("sprite"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
