﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Loot;

public class Diviner : MonoBehaviour 
{
    public CraftingManager craftingManager;
    public ResourceManager resourceManager;
    public CraftingBench craftingBench;

    public delegate void OnCraft(Equipment equipment);
	public OnCraft onCraft;

    public delegate void OnFail();
    public OnFail onFail;

    public delegate void OnEquipmentChanged();
    public OnEquipmentChanged onEquipmentChanged;

	private Dice dice = new Dice();
	public FloatVariable defaultFailChance;

	public StatEquivalencies statEquivalencies;

	public IntReference levelMin, levelMax;

	public void RaiseOnCraft(Equipment equipment)
	{
		if(onCraft != null) onCraft(equipment);
	}

	public void RaiseOnEquipmentChanged()
	{
		if(onEquipmentChanged != null) onEquipmentChanged();
	}

    public void RaiseOnFailed()
    {
        if(onFail != null) onFail();
    }

    public void Awake()
    {
        craftingBench.onChanged += UpdateEquipment;
    }
    
    public void UpdateEquipment()
    {
        RaiseOnEquipmentChanged();
    }

    public bool HasEquipment()
    {
        return craftingBench.equipment != null;
    }

    public Equipment GetEquipment()
    {
        return craftingBench.equipment;
    }

    public bool HasDivinity()
    {
        return craftingBench.equipment != null && craftingBench.equipment.divinity != null;
    }

    public bool CanBless()
    {
        if(!HasEquipment())
        {
            return false;
        }
        if(!HasDivinity())
        {
            return false;
        }

        Divinity divinity = craftingBench.equipment.divinity;

        if(divinity.currentLevel >= levelMax)
        {
            return false;
        }

        if(!resourceManager.CheckChange(divinity.GetUpgradeCost()))
        {
            return false;
        }

        return true;

    }

    public void Bless()
    {
        if(!CanBless()) return;
        
        Divinity divinity = craftingBench.equipment.divinity;

        resourceManager.ApplyChange(divinity.GetCraftingCost());

        if(!GetFailed())
        {
            divinity.AlterLevel(1, craftingBench.equipment);
            RaiseOnCraft(craftingBench.equipment);
        }
        else 
        {
            RaiseOnFailed();
        }
        
    }
    
    public DivinityProfile GetDivinity()
    {
        return craftingBench.equipment.divinity.profile;
    }

    public bool GetFailed()
    {
        float r = UnityEngine.Random.Range(0f, 1f);

        return (r < defaultFailChance.Value);
    }
}
