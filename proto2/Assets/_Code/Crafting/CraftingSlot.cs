﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CraftingSlot : MonoBehaviour 
{	
	public TextMeshProUGUI mesh;
	public Button button;
    public ResourceManager resourceManager {get; set;}
	public ColorReference availableColor {get; set;}
	public ColorReference unavailableColor {get; set;}
	public ColorReference selectedColor {get; set;}

    protected CraftingSlotState currentState;

	public void Start()
	{
		button.onClick.AddListener(OnClick);
	}

	protected virtual void OnClick()
	{
		
	}

	protected virtual void ResetState()
	{

	}

	protected void UpdateColor()
	{
		switch(currentState)
		{
			case CraftingSlotState.selected:
				button.image.color = selectedColor;
				break;
			case CraftingSlotState.available:
				button.image.color = availableColor;
				break;
			case CraftingSlotState.unavailable:
				button.image.color = unavailableColor;
				break;
			default:
				break;
		}
	}

    protected void SetState(CraftingSlotState state)
    {
		if(currentState == state) return;

        currentState = state;
		UpdateColor();
    }

	protected void SetHidden()
	{
		SetState(CraftingSlotState.hidden);
	}

    protected void SetUnavailable()
    {
        SetState(CraftingSlotState.unavailable);
    }

    protected void SetAvailable()
    {
        SetState(CraftingSlotState.available);
    }

    protected void SetSelected()
    {
        SetState(CraftingSlotState.selected);
    }

	public bool IsHidden()
	{
		return currentState == CraftingSlotState.hidden;
	}

    public bool IsSelected()
    {
        return currentState == CraftingSlotState.selected;
    }

    public bool IsAvailable()
    {
        return currentState == CraftingSlotState.available;
    }

    public bool IsUnavailable()
    {
        return currentState == CraftingSlotState.unavailable;
    }

    public void Select()
    {
        SetSelected();
    }

    public void Unselect()
    {
		SetAvailable();
        ResetState();
    }
	
    public enum CraftingSlotState{hidden, unavailable, available, selected,};
}
