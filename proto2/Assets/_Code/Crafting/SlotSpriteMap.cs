﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="UI/SlotSpriteMap")]
public class SlotSpriteMap : ScriptableObject
{
	[SerializeField]
	private List<SlotSprite> slotSprites;

	public Sprite Get(EquipSlot slot)
	{
		int index = (int) slot;

		if(index < 0 || index >= slotSprites.Count)
		{
			Debug.LogWarning("Index out of bounds at: " + slot);
			return null;
		}

		SlotSprite current = slotSprites[index];

		if(current.slot != slot)
		{
			Debug.LogWarning("Slot Missmatch at: " + index + " Expected: " + slot + " Found: " + current.slot);
			return null;
		}

		return current.sprite;
	}
}

[System.Serializable]
public struct SlotSprite {public EquipSlot slot; public Sprite sprite; }