﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Loot;

public class Crafter : MonoBehaviour 
{
	public CraftingManager craftingManager;
	public ResourceManager resourceManager;
	private MaterialBuilder materialBuilder;
	private List<Tag> tags = new List<Tag>();
	private List<EquipSlot> craftableSlots = new List<EquipSlot>();
	private int selectedSlotIndex = -1;
	private List<Entry<DesignProfile>> craftableDesigns = new List<Entry<DesignProfile>>();
	private int selectedDesignIndex = -1;
	private List<Entry<MaterialProfile>> craftableMaterials = new List<Entry<MaterialProfile>>();
	private int selectedMaterialIndex = -1;

	public delegate void OnCraft(Equipment equipment);
	public OnCraft onCraft;	

    public delegate void OnSlotChanged();
    public OnSlotChanged onSlotChanged;

	public delegate void OnDesignChanged();
	public OnDesignChanged onDesignChanged;

	public delegate void OnMaterialChanged();
	public OnMaterialChanged onMaterialChanged;

	private Dice dice = new Dice();
	public FloatVariable qualityUp;
	public FloatVariable qualityDown;

	private EquipmentBuilder equipmentBuilder = new EquipmentBuilder();
	public StatEquivalencies statEquivalencies;
	public EquipSlotMultiplierMap equipSlotMultiplierMap;
	public IntReference qualityMin, qualityMax;	

	public void RaiseOnCraft(Equipment equipment)
	{
		if(onCraft != null) onCraft(equipment);
	}

	public void Awake()
	{
		UpdateSlots();
	}

	public void UpdateSlots()
	{
		EquipSlot temp = new EquipSlot();
		bool hadSelectedSlot = HasSelectedSlot();
		if(hadSelectedSlot) temp = GetSelectedSlot();

		craftableSlots = craftingManager.GetCraftableSlots();

		if(hadSelectedSlot)
		{
			selectedSlotIndex = -1;
			
			for(int i = 0; i < craftableSlots.Count; i++)
			{
				if(craftableSlots[i] == temp)
				{
					selectedSlotIndex = i;
					break;
				}
			}
		}

		RaiseOnSlotChanged();
	}

	public void UpdateDesigns()
	{
		if(HasSelectedSlot()) 
		{
			Entry<DesignProfile> temp = new Entry<DesignProfile>();
			bool hadSelected = HasSelectedDesign();
			if(hadSelected) temp = GetSelectedDesign();

			craftableDesigns = craftingManager.GetCraftableDesigns(GetSelectedSlot());

			if(hadSelected)
			{
				selectedDesignIndex = -1;

				for(int i = 0; i < craftableDesigns.Count; i++)
				{
					if(craftableDesigns[i].payload == temp.payload)
					{
						selectedDesignIndex = i;
						break;
					}
				}
			}

			RaiseOnDesignChanged();

		}
		else 
		{
			craftableDesigns = new List<Entry<DesignProfile>>();
			RaiseOnDesignChanged();
		}
	}

	public void UpdateMaterials()
	{
		if(HasSelectedDesign()) 
		{
			Entry<MaterialProfile> temp = new Entry<MaterialProfile>();
			bool hadSelected = HasSelectedMaterial();
			if(hadSelected) temp = GetSelectedMaterial();

			craftableMaterials = craftingManager.GetCraftableMaterials(GetSelectedDesign().tags);

			if(hadSelected)
			{
				selectedMaterialIndex = -1;

				for(int i = 0; i < craftableMaterials.Count; i++)
				{
					if(craftableMaterials[i].payload == temp.payload)
					{
						selectedMaterialIndex = i;
						break;
					}
				}
			}

			RaiseOnMaterialChanged();

		}
		else 
		{
			craftableMaterials = new List<Entry<MaterialProfile>>();
			RaiseOnMaterialChanged();
		}
	}

	private void EditSlot(int index)
	{
		if(index == selectedSlotIndex) return;

		selectedSlotIndex = index;
		EditDesign(-1);
		UpdateDesigns();

	}

	public void SelectSlot(int index)
	{
		EditSlot(index);

		RaiseOnSlotChanged();
	}

	public void UnselectSlot()
	{
		SelectSlot(-1);
	}

	private void EditDesign(int index)
	{
		if(index == selectedDesignIndex) return;

		selectedDesignIndex = index;
		EditMaterial(-1);
		UpdateMaterials();

	}

	public void SelectDesign(int index)
	{
		EditDesign(index);

		RaiseOnDesignChanged();
	}

	public void UnselectDesign()
	{
		SelectDesign(-1);
	}

	public void EditMaterial(int index)
	{
		if(index == selectedMaterialIndex) return;

		selectedMaterialIndex = index;

	}

	public void SelectMaterial(int index)
	{
		EditMaterial(index);

		RaiseOnMaterialChanged();
	}

	public void UnselectMaterial()
	{
		SelectMaterial(-1);
	}

	public void Craft()
	{
		EquipSlot slot;
		List<Tag> tags;
		DesignProfile designProfile;
		MaterialProfile materialProfile = null;
		QualityProfile qualityProfile;
		DivinityProfile divinityProfile;

		bool hasMaterial = false;

		if(HasSelectedSlot()) 
			slot = GetSelectedSlot();
		else 
			return;

		if(HasSelectedDesign()) 
		{
			Entry<DesignProfile> entry = GetSelectedDesign();
			designProfile = entry.payload;
			tags = entry.tags;
		}
		else 
			return;

		hasMaterial = HasSelectedMaterial();

		if(hasMaterial) 
			materialProfile = GetSelectedMaterial().payload;

		divinityProfile = Utility.RandomFromList(craftingManager.GetCraftableDivinities(tags)).payload;
		qualityProfile = Utility.RandomFromList(craftingManager.GetCraftableQualities(tags)).payload;

		Design design = new Design(designProfile, statEquivalencies);
		Quality quality = new Quality(qualityProfile, statEquivalencies, qualityMin, qualityMax, GetQualityLevel());
		Divinity divinity = new Divinity(divinityProfile, statEquivalencies, -1, 1, 0);
		Loot.Material material = (hasMaterial) ? new Loot.Material(materialProfile, statEquivalencies) : null;



		Equipment equipment = equipmentBuilder.Begin()
			.WithTags(tags)
			.WithSlot(slot, equipSlotMultiplierMap.GetMultiplier(slot))
			.WithRarity(Rarity.ordinary)
			.WithDesign(design)
			.WithQuality(quality)
			.WithDivinity(divinity)
			.WithMaterials(material)
			.Build();

		if(resourceManager.CheckChange(equipment.GetCraftingCost()))
		{
			resourceManager.ApplyChange(equipment.GetCraftingCost());
			RaiseOnCraft(equipment);
		}

	}

	public int GetQualityLevel()
	{
		int minLevel = dice.ChainRoll(qualityDown.Value);
		int maxLevel = dice.ChainRoll(qualityUp.Value);

		int level = maxLevel - minLevel;

		return level;		
	}

	public void RaiseOnSlotChanged()
	{
		if(onSlotChanged != null) onSlotChanged();
	}

	public void RaiseOnDesignChanged()
	{
		if(onDesignChanged != null) onDesignChanged();
	}

	public void RaiseOnMaterialChanged()
	{
		if(onMaterialChanged != null) onMaterialChanged();
	}

	public List<EquipSlot> GetCraftableSlots()
	{
		return craftableSlots;
	}

	public List<Entry<DesignProfile>> GetCraftableDesigns()
	{
		return craftableDesigns;
	}

	public List<Entry<MaterialProfile>> GetCraftableMaterials()
	{
		return craftableMaterials;
	}

	public int GetSelectedSlotIndex()
	{
		return selectedSlotIndex;
	} 

	public int GetSelectedDesignIndex()
	{
		return selectedDesignIndex;
	}

	public int GetSelectedMaterialIndex()
	{
		return selectedMaterialIndex;
	}

	public bool HasSelectedSlot()
	{
		return (selectedSlotIndex >= 0 && selectedSlotIndex < craftableSlots.Count);
	}

	public bool HasSelectedMaterial()
	{
		return (selectedMaterialIndex >= 0 && selectedMaterialIndex < craftableMaterials.Count);
	}

	public bool HasSelectedDesign()
	{
		return (selectedDesignIndex >= 0 && selectedDesignIndex < craftableDesigns.Count);
	}

	public EquipSlot GetSelectedSlot()
	{
		if(HasSelectedSlot()) 
		{
			return craftableSlots[selectedSlotIndex];
		}
		else
		{
			Debug.LogWarning("Index out of bounds");
			return new EquipSlot();
		}
	}

	public Entry<MaterialProfile> GetSelectedMaterial()
	{
		if(HasSelectedMaterial()) 
		{
			return craftableMaterials[selectedMaterialIndex];
		}
		else
		{
			Debug.LogWarning("Index out of bounds");
			return new Entry<MaterialProfile>();
		}
	}

	public Entry<DesignProfile> GetSelectedDesign()
	{
		if(HasSelectedDesign()) 
		{
			return craftableDesigns[selectedDesignIndex];
		}
		else
		{
			Debug.LogWarning("Index out of bounds");
			return new Entry<DesignProfile>();
		}
	}
}
