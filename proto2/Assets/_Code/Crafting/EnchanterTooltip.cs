﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnchanterTooltip : MonoBehaviour 
{
	// public Enchanter enchanter;
	// public TextMeshProUGUI costMesh;
	// public TextMeshProUGUI statMesh;
	// public TextMeshProUGUI descriptionMesh;
	// public StatText statText;
	// public ColorReference statColor;

	// public void Start()
	// {
	// 	enchanter.onChanged += UpdateUI;
	// }

	// public void UpdateUI()
	// {
	// 	UpdateCostText();
	// 	UpdateDescriptionText();
	// 	UpdateStatText();
	// }

	// public void UpdateCostText()
	// {
	// 	// if(enchanter.enchantmentBuilder == null)
	// 	// {
	// 	// 	costMesh.SetText("");
	// 	// 	return;
	// 	// }

	// 	// List<Resource> costs = enchanter.enchantmentBuilder.GetCraftingCost();

	// 	// string costString = "Cost";

	// 	// for(int i = 0; i < costs.Count; i++)
	// 	// {
	// 	// 	costString += ("\n" +costs[i].type + " " + (-costs[i].value));
	// 	// }

	// 	// costMesh.SetText(costString);
	// }

	// public void UpdateDescriptionText()
	// {
	// 	if(enchanter.enchantmentBuilder == null)
	// 	{
	// 		descriptionMesh.SetText("");
	// 		return;
	// 	}

	// 	string failText = Utility.ToPercent(enchanter.failChance) + " to fail\n";
	// 	string levelText = Utility.ToPercent(enchanter.levelChance) + " to amplify";

	// 	string totalText = failText + levelText;

	// 	descriptionMesh.SetText(totalText);
	// }

	// public void UpdateStatText()
	// {
	// 	if(enchanter.enchantmentBuilder == null)
	// 	{
	// 		statMesh.SetText("");
	// 		return;
	// 	}

	// 	if(enchanter.craftingBench.equipment == null)
	// 	{
	// 		statMesh.SetText("");
	// 		return;
	// 	}

	// 	float slotMultiplier = enchanter.craftingBench.equipment.GetMultiplier();
	// 	List<EquipStat> stats = enchanter.enchantmentBuilder.GetStats();

	// 	string statsText = "";
		
	// 	foreach(EquipStat stat in stats)
	// 	{
	// 		statsText += statText.GetText(stat, slotMultiplier, statColor) + "\n";
	// 	}

	// 	statMesh.SetText(statsText);
	// }
}
