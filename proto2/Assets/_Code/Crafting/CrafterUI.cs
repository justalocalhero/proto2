﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Loot;
using System;
using Tooltips;
using TMPro;

public class CrafterUI : MonoBehaviour
{
	public UIToggler toggler;
	public SpriteManager spriteManager;
	public PointerHandler pointerHandler;
	public Image craftIcon, frameIcon;
	public TextMeshProUGUI costText, chanceText;
	public Button craftButton;
	private int equipCount = System.Enum.GetNames(typeof(EquipSlot)).Length;
	public IntReference designCount, materialCount;
	public FloatReference equipRadius, designRadius, materialRadius;
	public Transform orbitContainer;
	public OrbitSlot orbitPrototype;
	private List<OrbitSlot> equipSlots, designSlots, materialSlots;
	public ResourceManager resourceManager;
	public Crafter crafter;
	public ColorReference selectedColor, availableColor, unavailableColor,  availableIconColor, unavailableIconColor, positiveColor, negativeColor;
	public SlotSpriteMap slotSprites;
	public Sprite materialSprite;
	public ComponentProfileTooltip componentTooltip;
	public EquipSlotTooltip equipSlotTooltip;
	public ComponentProfileText componentText;

	public void Awake()
	{
		BuildOrbits();
		resourceManager.onResourceChanged += UpdateUI;
		crafter.onSlotChanged += UpdateEquipSlots;
		crafter.onDesignChanged += UpdateDesignSlots;
		crafter.onDesignChanged += UpdateText;
		crafter.onDesignChanged += UpdateCraftButton;
		crafter.onMaterialChanged += UpdateMaterialSlots;
		crafter.onMaterialChanged += UpdateIcon;
		crafter.onMaterialChanged += UpdateText;
		crafter.onMaterialChanged += UpdateCraftButton;
		craftButton.onClick.AddListener(HandleCraft);
		pointerHandler.onEnter += HandlePreviewEnter;
		pointerHandler.onExit += HandlePreviewExit;
		toggler.onShow += UpdateUI;
	}

	public void Start()
	{
		UpdateUI();
	}

	public void BuildOrbits()
	{
		equipSlots = BuildOrbitSlots(orbitContainer, orbitPrototype, equipCount, equipRadius, HandleEquipClick, HandleEquipEnter, HandleEquipExit);
		designSlots = BuildOrbitSlots(orbitContainer, orbitPrototype, designCount, designRadius, HandleDesignClick, HandleDesignEnter, HandleDesignExit);
		materialSlots = BuildOrbitSlots(orbitContainer, orbitPrototype, materialCount, materialRadius, HandleMaterialClick, HandleMaterialEnter, HandleMaterialExit);
	}

	public void HandleEquipClick(int index)
	{
		crafter.SelectSlot(index);
	}

	public void HandleDesignClick(int index)
	{		
		List<Entry<DesignProfile>> profiles = crafter.GetCraftableDesigns();
		DesignProfile profile = profiles[index].payload;
		
		if(CheckResources(profile)) crafter.SelectDesign(index);
	}

	public void HandleMaterialClick(int index)
	{
		List<Entry<MaterialProfile>> profiles = crafter.GetCraftableMaterials();
		MaterialProfile profile = profiles[index].payload;

		if(CheckResources(profile)) crafter.SelectMaterial(index);
	}

	public void HandleEquipEnter(int index)
	{
		OrbitSlot slot = equipSlots[index];
		List<EquipSlot> equips = crafter.GetCraftableSlots();
		EquipSlot equip = equips[index];

		equipSlotTooltip.Set(equip, slot.rect);
	}

	public void HandleDesignEnter(int index)
	{
		OrbitSlot slot = designSlots[index];
		EquipSlot equipSlot = crafter.GetSelectedSlot();
		List<Entry<DesignProfile>> profiles = crafter.GetCraftableDesigns();
		DesignProfile profile = profiles[index].payload;

		componentTooltip.SetCraft(profile, equipSlot, resourceManager, slot.rect);
	}

	public void HandleMaterialEnter(int index)
	{
		OrbitSlot slot = materialSlots[index];
		EquipSlot equipSlot = crafter.GetSelectedSlot();
		Entry<DesignProfile> designProfile = crafter.GetSelectedDesign();
		List<Entry<MaterialProfile>> profiles = crafter.GetCraftableMaterials();
		MaterialProfile profile = profiles[index].payload;

		List<ComponentProfile> componentProfiles = new List<ComponentProfile>{profile, designProfile.payload};

		componentTooltip.SetCraft(componentProfiles, equipSlot, resourceManager, slot.rect);
	}
	
	public void HandlePreviewEnter()
	{
		EquipSlot equipSlot = crafter.GetSelectedSlot();

		List<ComponentProfile> profiles = new List<ComponentProfile>();
		if(crafter.HasSelectedMaterial()) profiles.Add(crafter.GetSelectedMaterial().payload);
		if(crafter.HasSelectedDesign()) profiles.Add(crafter.GetSelectedDesign().payload);

		if(profiles.Count > 0) componentTooltip.SetStats(profiles, equipSlot, pointerHandler.rect);

		
	}

	public void HandleEquipExit(int index)
	{
		equipSlotTooltip.Hide();
	}

	public void HandleDesignExit(int index)
	{
		componentTooltip.Hide();
	}

	public void HandleMaterialExit(int index)
	{
		componentTooltip.Hide();
	}

	public void HandlePreviewExit()
	{
		componentTooltip.Hide();
	}

	public List<OrbitSlot> BuildOrbitSlots(Transform container, OrbitSlot prototype, int count, float radius, OrbitSlot.OnClicked onClick, OrbitSlot.OnEnter onEnter, OrbitSlot.OnExit onExit)
	{
		RingBuilder<OrbitSlot> builder = new RingBuilder<OrbitSlot>();

		List<OrbitSlot> toReturn = builder
			.WithContainer(container)
			.WithPrototype(prototype)
			.WithCount(count)
			.WithRadius(radius)
			.Build();

		int index = 0;

		foreach(OrbitSlot slot in toReturn)
		{
			slot.index = index++;
			slot.onClicked += onClick;
			slot.onEnter += onEnter;
			slot.onExit += onExit;
		}

		return toReturn;
	}

	public void UpdateUI()
	{
		if(!toggler.IsVisible()) return;
		UpdateEquipSlots();
		UpdateDesignSlots();
		UpdateMaterialSlots();
		UpdateIcon();
		UpdateText();
		UpdateCraftButton();
	}

	public void UpdateEquipSlots()
	{
		List<EquipSlot> slots = crafter.GetCraftableSlots();
		int selectedIndex = crafter.GetSelectedSlotIndex();

		for(int i = 0; i < equipSlots.Count; i++)
		{
			OrbitSlot current = equipSlots[i];

			if(i < slots.Count)
			{
				if(i == selectedIndex)
					current.SetButtonColor(selectedColor);
				else
					current.SetButtonColor(availableColor);

				current.SetIcon(slotSprites.Get(slots[i]));				
				current.Show();
			}
			else
			{
				current.Hide();
			}
		}
	}

	public void UpdateDesignSlots()
	{
		List<Entry<DesignProfile>> slots = crafter.GetCraftableDesigns();
		int selectedIndex = crafter.GetSelectedDesignIndex();

		for(int i = 0; i < designSlots.Count; i++)
		{
			OrbitSlot current = designSlots[i];

			if(i < slots.Count)
			{
				if(i == selectedIndex)
				{
					current.SetButtonColor(selectedColor);
					current.SetIconColor(availableIconColor);
				}
				else if(CheckResources(slots[i].payload))
				{
					current.SetButtonColor(availableColor);
					current.SetIconColor(availableIconColor);
				}
				else
				{
					current.SetButtonColor(unavailableColor);
					current.SetIconColor(unavailableIconColor);
				}

				current.SetIcon(spriteManager.GetSprite(slots[i].payload.GetSprite()));
				current.Show();
			}
			else
			{
				current.Hide();
			}
		}
	}

	public void UpdateMaterialSlots()
	{
		List<Entry<MaterialProfile>> slots = crafter.GetCraftableMaterials();
		int selectedIndex = crafter.GetSelectedMaterialIndex();

		for(int i = 0; i < materialSlots.Count; i++)
		{
			OrbitSlot current = materialSlots[i];

			if(i < slots.Count)
			{
				if(i == selectedIndex)
					current.SetButtonColor(selectedColor);
				else if(CheckResources(slots[i].payload))
					current.SetButtonColor(availableColor);
				else
					current.SetButtonColor(unavailableColor);

				current.SetIcon(materialSprite);			
				current.Show();
			}
			else
			{
				current.Hide();
			}
		}
	}

	public void UpdateIcon()
	{
		if(crafter.HasSelectedDesign())
		{
			craftIcon.sprite = spriteManager.GetSprite(crafter.GetSelectedDesign().payload.GetSprite());
			if(!craftIcon.IsActive()) 
				craftIcon.enabled = true;
			if(!frameIcon.IsActive()) 
				frameIcon.enabled = true;
		}
		else
		{
			craftIcon.sprite = null;
			if(craftIcon.IsActive())
				craftIcon.enabled = false;
			if(frameIcon.IsActive()) 
				frameIcon.enabled = false;
		}
	}

	public void UpdateText()
	{
		List<ComponentProfile> profiles = new List<ComponentProfile>();
		if(!crafter.HasSelectedSlot())
		{
			ClearText();
			return;
		}

		if(crafter.HasSelectedMaterial())
		{
			profiles.Add(crafter.GetSelectedMaterial().payload);
		}

		if(crafter.HasSelectedDesign())
		{
			profiles.Add(crafter.GetSelectedDesign().payload);
		}
		else
		{
			ClearText();
			return;
		}

		costText.SetText(componentText.CraftingCostString(profiles, resourceManager, "\n", "", ""));
		chanceText.SetText(ChanceString());

	}

	public void UpdateCraftButton()
	{
		bool buttonActive = false;

		if(crafter.HasSelectedDesign())
		{
			if(crafter.HasSelectedDesign())
			{
				if(CheckResources(crafter.GetSelectedDesign().payload))
				{
					buttonActive = true;
				}
				else
				{
					buttonActive = false;
				}
			}	
			else
			{
				if(CheckResources(crafter.GetSelectedMaterial().payload))
				{					
					buttonActive = true;
				}
				else
				{
					buttonActive = false;
				}
			}
		}
		else
		{
			buttonActive = false;
		}

		craftButton.interactable = buttonActive;

	}

	private void ClearText()
	{
		costText.SetText("");
		chanceText.SetText("");

	}

	private string ChanceString()
	{
		string text = "";

		text += Tooltips.ColoredText.GetText(Utility.ToPercent(crafter.qualityUp.Value) + " chance for superior item.\n", positiveColor);
		text += Tooltips.ColoredText.GetText(Utility.ToPercent(crafter.qualityDown.Value) + " chance for inferior item.\n", negativeColor);

		return text;
	}

	public bool CheckResources(DesignProfile profile)
	{
		return CheckResources(profile.recipeCost.craftingCost);
	}

	public bool CheckResources(MaterialProfile profile)
	{
		Entry<DesignProfile> design = crafter.GetSelectedDesign();
		List<Resource> toCheck = new List<Resource>();

		List<Resource> designCost = design.payload.recipeCost.craftingCost;

		toCheck.AddRange(designCost);
		toCheck.AddRange(profile.recipeCost.craftingCost);

		return CheckResources(toCheck);
	}

	public bool CheckResources(List<Resource> resources)
	{
		return resourceManager.CheckChange(resources);
	}

	public void HandleCraft()
	{
		crafter.Craft();
	}

}
