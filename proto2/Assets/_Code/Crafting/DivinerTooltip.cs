﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DivinerTooltip : MonoBehaviour 
{

	// public Diviner diviner;
	// public TextMeshProUGUI costMesh;
	// public TextMeshProUGUI statMesh;
	// public TextMeshProUGUI descriptionMesh;
	// public StatText statText;
	// public ColorReference statColor;

	// public void Start()
	// {
	// 	diviner.onChanged += UpdateUI;
	// }

	// public void UpdateUI()
	// {
	// 	UpdateCostText();
	// 	UpdateDescriptionText();
	// 	UpdateStatText();
	// }

	// public void UpdateCostText()
	// {
	// 	if(!diviner.HasTarget())
	// 	{
	// 		costMesh.SetText("");
	// 		return;
	// 	}

	// 	List<Resource> costs = diviner.GetUpgradeCost();

	// 	string costString = "Cost";

	// 	for(int i = 0; i < costs.Count; i++)
	// 	{
	// 		costString += ("\n" +costs[i].type + " " + (-costs[i].value));
	// 	}

	// 	costMesh.SetText(costString);
	// }

	// public void UpdateDescriptionText()
	// {
	// 	if(!diviner.HasTarget())
	// 	{
	// 		descriptionMesh.SetText("");
	// 		return;
	// 	}

	// 	string failText = Utility.ToPercent(diviner.failChance) + " to fail\n";

	// 	string totalText = failText;

	// 	descriptionMesh.SetText(totalText);
	// }

	// public void UpdateStatText()
	// {
	// 	if(!diviner.HasTarget())
	// 	{
	// 		statMesh.SetText("");
	// 		return;
	// 	}
		
	// 	if(diviner.craftingBench.equipment == null)
	// 	{
	// 		statMesh.SetText("");
	// 		return;
	// 	}

	// 	float slotMultiplier = diviner.craftingBench.equipment.GetMultiplier();
	// 	List<EquipStat> stats = diviner.GetCraftingStats();

	// 	string statsText = "";
		
	// 	foreach(EquipStat stat in stats)
	// 	{
	// 		statsText += statText.GetText(stat, slotMultiplier, statColor) + "\n";
	// 	}

	// 	statMesh.SetText(statsText);
	// }
}
