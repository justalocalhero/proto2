﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecipeBook<T> 
{
	public List<Recipe> recipes = new List<Recipe>();

	public void Add(List<T> toAdd)
	{
		foreach(T t in toAdd)
		{
			recipes.Add(new Recipe{
				recipe = t,
				unlocked = false,
			});
		}
	}

	public void UnlockRecipe(T toUnlock)
	{
		for(int i = 0; i < recipes.Count; i++)
		{
			Recipe current = recipes[i];
			if(current.recipe.Equals(toUnlock) && !current.unlocked) 
			{
				current.unlocked = true;
				recipes[i] = current;
			}			
		}
	}

	public List<T> GetUnlockedRecipes()
	{
		List<T> toReturn = new List<T>();

		foreach(Recipe recipe in recipes)
		{
			if(recipe.unlocked) toReturn.Add(recipe.recipe);
		}

		return toReturn;
	}
	
	public struct Recipe {public T recipe; public bool unlocked;}
}
