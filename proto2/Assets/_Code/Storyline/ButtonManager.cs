﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonManager : MonoBehaviour, IPointerEnterHandler 
{
	public UIToggler toggler;
	protected bool newlyVisible;
	
	public delegate void OnChanged();
	public OnChanged onChanged;

	public delegate void OnHighlight();
	public OnHighlight onHighlight;

	public delegate void OnSeen();
	public OnSeen onSeen;

	public bool IsVisible()
	{
		return toggler.IsVisible();
	}

	public void RaiseOnChanged()
	{
		if(onChanged != null) onChanged();
	}

	public virtual void Activate()
	{
		Show();
	}

	public virtual void Deactivate()
	{
		Hide();
	}

	public void Show()
	{
		toggler.Show();
		RaiseOnChanged();
	}

	public void Hide()
	{
		toggler.Hide();
		RaiseOnChanged();
	}

	public void Highlight()
	{
		Show();
		//RaiseOnHighlight();
	}

	public void See()
	{
		RaiseOnSeen();
	}

	public void RaiseOnHighlight()
	{
		if(onHighlight != null) onHighlight();
	}

	public void RaiseOnSeen()
	{
		if(onSeen != null) onSeen();
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
        See();
    }
}
