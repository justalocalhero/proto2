﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SceneInitializer : MonoBehaviour 
{
	public SaveManager saveManager;
	public SpriteManager spriteManager;
	public Bullpen bullpen;
	public TalentTreeManager talents;
	public MenuManager menuManager;
	public BattlegroundManager battlegroundManager;
	public EquipmentGenerator defaultEquipment;
	public Inventory inventoryManager;
	public ResourceManager resourceManager;
	public EquipTutorial equipTutorial;
	

	void Start () 
	{
		
		Shader.EnableKeyword("UNITY_UI_CLIP_RECT");
		
		inventoryManager.Add(defaultEquipment.GenerateAll());

		talents.Reset();
		bullpen.GetNextPlayer();
		bullpen.UpdatePlayerStats();
		battlegroundManager.Initialize();

		resourceManager.Initialize();

		spriteManager.Initialize();

		if(saveManager.HasSaveFile()) saveManager.Load();
		else equipTutorial.HandleCheck();
		saveManager.EnableAutosave();
	}
}
