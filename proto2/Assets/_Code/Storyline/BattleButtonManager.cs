﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleButtonManager : ButtonManager
{
	public SaveManager saveManager;
	public EquipmentManager equipmentManager;
	private bool active = false;

	public void Awake()
	{
		equipmentManager.onEquipmentChanged += Check;
		
		if(saveManager != null)
		{
			saveManager.onPreSave += Save;
			saveManager.onLoad += Load;
		}
	}

	public void Save(SaveFile saveFile)
	{
		saveFile.battleButtonActive = active;
	}

	public void Load(SaveFile saveFile)
	{
		if(saveFile.battleButtonActive) Activate();
		else Deactivate();
	}

	public void Check()
	{
		if(equipmentManager.GetFilledSlots() >= 4) 
		{
			Activate();
			Highlight();
		}
	}

	public override void Activate()
	{
		active = true;
		equipmentManager.onEquipmentChanged -= Check;
		Show();
	}

	public override void Deactivate()
	{
		if(active)
		{
			active = false;
			equipmentManager.onEquipmentChanged += Check;
		}
		Hide();
	}
}
