﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButtonHighlight : MonoBehaviour 
{
	public Bob bob;
	public ButtonManager buttonManager;

	// Use this for initialization
	void Awake () 
	{
		bob.target = buttonManager.transform as RectTransform;

		buttonManager.onHighlight += bob.Play;
		buttonManager.onSeen += bob.Stop;
	}
}
