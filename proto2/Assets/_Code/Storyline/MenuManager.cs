﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour 
{
	public bool debugMode;
	public Transform buttonContainer;
	private ButtonManager[] buttonManagers;
	public FloatReference buttonWidth;

	public void Awake()
	{
		buttonManagers = buttonContainer.GetComponentsInChildren<ButtonManager>();
		foreach(ButtonManager button in buttonManagers)
		{
			button.onChanged += UpdateUI;
		}
	}

	public void Start()
	{
		if(debugMode)
		{
			foreach(ButtonManager button in buttonManagers)
			{
				button.Activate();
			}
		}
	}

	public void UpdateUI()
	{
		int count = 0;

		foreach(ButtonManager button in buttonManagers)
		{
			if(button.IsVisible())
				MoveButton(button, count++);
			
		}
	}

	public void MoveButton(ButtonManager button, int index)
	{
		RectTransform buttonRect = button.transform as RectTransform;

		buttonRect.anchoredPosition = new Vector2(buttonWidth * index, 0);
	}
		
}
