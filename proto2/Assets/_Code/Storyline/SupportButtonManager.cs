﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SupportButtonManager : MonoBehaviour 
{
	public UIToggler toggler;
	public SaveManager saveManager;
	public FloatReference timeTillSupportActive;

	public void Awake()
	{
		saveManager.onSave += Check;
	}

	public void Check(SaveFile saveFile)
	{
		if(saveFile.totalActiveTime > timeTillSupportActive)
		{
			saveManager.onSave -= Check;
			toggler.Show();
		}
	}
}
