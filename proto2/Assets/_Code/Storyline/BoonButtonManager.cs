﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Boons
{
	public class BoonButtonManager : ButtonManager
	{
		public Transform container;
		private BoonManager[] boonManagers;

		public void Awake()
		{
			boonManagers = container.GetComponentsInChildren<BoonManager>();
			for(int i = 0; i < boonManagers.Length; i++)
			{
				boonManagers[i].onTierUp += Check;
			}
		}

		public void Check()
		{
			foreach(BoonManager boonManager in boonManagers)
			{				
				if(boonManager.GetMaxLevel() > 0)
				{
					Activate();
					Highlight();
				}
			}
		}

		public override void Activate()
		{
			for(int i = 0; i < boonManagers.Length; i++)
			{
				boonManagers[i].onTierUp -= Check;
			}
			
			Show();
		}
	}
}