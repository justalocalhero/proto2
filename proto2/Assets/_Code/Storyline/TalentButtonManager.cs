﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalentButtonManager : ButtonManager
{
	public SaveManager saveManager;
	public TalentTreeManager talentTreeManager;
	private bool active = false;

	public void Awake()
	{
		talentTreeManager.onTalentChanged += Check;
		
		if(saveManager != null)
		{
			saveManager.onPreSave += Save;
			saveManager.onLoad += Load;
		}
	}

	public void Start()
	{

	}

	public void Save(SaveFile saveFile)
	{
		saveFile.talentButtonActive = active;
	}

	public void Load(SaveFile saveFile)
	{
		if(saveFile.talentButtonActive) Activate();
		else Deactivate();
	}

	public void Check()
	{
		if(talentTreeManager.GetAvailablePoints() > 0) 
		{
			Activate();
			Highlight();
		}
	}

	public override void Activate()
	{
		active = true;
		talentTreeManager.onTalentChanged -= Check;
		Show();
	}

	public override void Deactivate()
	{
		if(active)
		{
			active = false;
			talentTreeManager.onTalentChanged += Check;
		}
		Hide();
	}
}
