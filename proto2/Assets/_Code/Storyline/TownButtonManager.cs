﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownButtonManager : ButtonManager
{
	public Inventory inventory;

	public void Awake()
	{
		inventory.onInventoryChanged += Check;
	}

	public void Check()
	{
		if(inventory.GetEquipmentCount() > 4) 
		{
			Activate();
			Highlight();
		}
	}

	public override void Activate()
	{
		inventory.onInventoryChanged -= Check;
		Show();
	}
}
