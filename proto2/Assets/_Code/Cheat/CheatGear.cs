﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatGear : MonoBehaviour 
{
	public EquipmentGenerator equipmentGenerator;
	public List<ArtifactEquipmentGenerator> artifactGenerators;
	public Inventory inventory;

	public void OnButtonPressed()
	{
		inventory.Add(equipmentGenerator.GenerateAll());
		foreach(ArtifactEquipmentGenerator generator in artifactGenerators)
		{
			inventory.Add(generator.Generate());
		}
	}
}
