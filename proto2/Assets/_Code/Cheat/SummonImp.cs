﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SummonImp : MonoBehaviour 
{
	public UIToggler panelToggler;
	public TextMeshProUGUI mesh;
	public TextMeshProUGUI soulShardMesh;
	public ParticleSystem[] particleSystems;
	private float nextReleaseTime;
	public FloatReference releaseTime;
	public FloatReference graceTime;
	private bool pressButton;

	public void Update()
	{
		if(!panelToggler.IsVisible()) return;

		if(!pressButton && Time.time + graceTime > nextReleaseTime)
		{
			mesh.SetText("Press Any Key \nTo Become Puzzle Guru For Life");
		}
		if(Input.anyKeyDown && Time.time > nextReleaseTime)
		{
			Hide();
		}
	}

	public void Show()
	{
		mesh.SetText("No Soul Shards");
		nextReleaseTime = Time.time + releaseTime;

		panelToggler.Show();
		foreach(ParticleSystem particle in particleSystems)
		{
			particle.Play();
		}
	}

	public void Hide()
	{
		panelToggler.Hide();
		foreach(ParticleSystem particle in particleSystems)
		{
			particle.Stop();
		}
		soulShardMesh.SetText("Soul Shards: 0");

	}
}
