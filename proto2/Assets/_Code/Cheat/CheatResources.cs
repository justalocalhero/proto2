﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatResources : MonoBehaviour 
{
	public ResourceManager resourceManager;

	public void OnButtonPressed()
	{
		resourceManager.ApplyChange(new Resource{type = ResourceType.scrap, value = 1000});
		resourceManager.ApplyChange(new Resource{type = ResourceType.essence, value = 5});
		resourceManager.ApplyChange(new Resource{type = ResourceType.holyWater, value = 1});
		resourceManager.ApplyChange(new Resource{type = ResourceType.refinedMaterials, value = 5});
	}
}
