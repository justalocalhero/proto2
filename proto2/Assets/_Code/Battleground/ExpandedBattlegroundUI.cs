﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ExpandedBattlegroundUI : MonoBehaviour 
{
	public UIToggler toggler;
	public LootSlider lootSlider; 
	public Battleground defaultBattleground;
	private Battleground battleground;
	public LaunchButton launchButton;
	public TextMeshProUGUI titleMesh, bodyMesh, lootMesh;
	public ColoredText coloredText;
	public ColorReference nameColor, levelColor, descriptionColor, killColor, lootColor, lootWarningColor;
	public IntReference lootSpace;

	public delegate void OnLoot(Equipment equipment);
	public OnLoot onLoot;

	public void RaiseOnLoot(Equipment equipment)
	{
		if(onLoot != null) 
			onLoot(equipment);
	}

	public void Awake()
	{
		toggler.onShow += UpdateUI;
	}

	public void Start()
	{
		SetBattleground(defaultBattleground);
	}

	public void Update()
	{
		if(!toggler.IsVisible()) return;

		UpdateLootSlider();
	}

	public void SetBattleground(Battleground newBattleground)
	{
		if(newBattleground == null) return;

		if(battleground != null)
		{
			battleground.onBattlegroundLevelUp -= UpdateUI;
			battleground.onEnemyDeath -= UpdateUI;
			battleground.onPlayerDeath -= UpdateUI;
			battleground.onLoot -= HandleLoot;
		}

		battleground = newBattleground;
		battleground.onBattlegroundLevelUp += UpdateUI;
		battleground.onEnemyDeath += UpdateUI;
		battleground.onPlayerDeath += UpdateUI;
		battleground.onLoot += HandleLoot;

		launchButton.SetBattleground(battleground);

		UpdateUI();

	}

	public void HandleLoot(Equipment equipment)
	{
		if(equipment == null) return;

		UpdateUI();
		RaiseOnLoot(equipment);
	}

	public void UpdateUI(Player player)
	{
		UpdateUI();
	}

	public void UpdateUI()
	{
		if(!toggler.IsVisible()) return;

		titleMesh.SetText(GetTitleText());
		bodyMesh.SetText(GetBodyText());
		lootMesh.SetText(GetLootString());
		UpdateLootSlider();

	}

	public void UpdateLootSlider()
	{
		if(battleground == null) 
			lootSlider.Hide();
		else if(!battleground.HasLoot()) 
			lootSlider.Hide();
		else 
			lootSlider.Set(battleground.LootGatherScale());
	}

	public string GetTitleText()
	{
		if(battleground == null) return "";
		
		string nameString = GetNameString();
		
		return (nameString);
	}

	public string GetBodyText()
	{
		if(battleground == null) return "";

		string maxLevelString = GetLevelString();
		string killString = GetKillString();
		
		return (maxLevelString + killString);
	}

	public string GetNameString()
	{
		string nameString = coloredText.GetText("<align=center>" + battleground.GetName() + "\n\n", nameColor);

		return nameString;
	}

	public string GetLevelString()
	{
		string maxLevelString = (battleground.state.cleared) ? "Cleared" : "Max Level Reached: " + battleground.GetMaxLevelReached();

		return coloredText.GetText(maxLevelString + "\n", levelColor);
	}

	public string GetClearedString()
	{
		return (battleground.state.cleared) ? "Cleared" : "";
	}

	public string GetKillString()
	{
		string killString = coloredText.GetText("Kills: " + battleground.GetKillCount() + "\n", killColor);

		return killString;
	}

	public string GetLootString()
	{
		if(battleground == null) return "";

		int currentLootCount = battleground.GetLootCount();

		if(currentLootCount <= 0) return "";

		Color color = (currentLootCount >= (lootSpace - 5)) ? lootWarningColor : lootColor;

		string toReturn = coloredText.GetText(currentLootCount.ToString(), color);

		return toReturn;

	}

	public void OnLaunchButtonPress()
	{
		
	}
}
