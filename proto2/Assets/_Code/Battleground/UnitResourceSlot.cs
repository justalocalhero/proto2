﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitResourceSlot : MonoBehaviour 
{
	public SpriteManager spriteManager;
	private Image[] icons;
	private int[] pascal = {0, 0, 1, 3, 6, 10, 15};
	private int maxIndex;

	public void Start()
	{
		icons = GetComponentsInChildren<Image>();

		for(int i = 0; i < pascal.Length; i++)
		{
			if(icons.Length >= pascal[i]) maxIndex = i - 1;
		}
	}

	public void Set(UnitResource newResource)
	{
		if(newResource == null) 
		{
			Clear();
			return;
		}

		Sprite sprite = spriteManager.GetSprite(newResource.GetSprite());
		if(sprite == null)
		{
			ClearSprites();
			return;
		}

		int resourceValue = newResource.GetValue();
		int startIndex = (resourceValue <= maxIndex) ? pascal[resourceValue] : pascal[maxIndex];
		
		for(int i = 0; i < icons.Length; i++)
		{
			if(i < startIndex) ClearSprite(i);
			else if(i < startIndex + resourceValue) ShowSprite(sprite, i);
			else ClearSprite(i);
		}		
	}

	public void Clear()
	{
		ClearSprites();
	}

	public void ClearSprites()
	{
		for(int i = 0; i < icons.Length; i++)
		{				
			ClearSprite(i);
		}
	}

	public void ClearSprite(int index)
	{
		if(index >= icons.Length || index < 0) return;
		icons[index].sprite = null;
		icons[index].enabled = false;	
	}

	public void ShowSprite(Sprite sprite, int index)
	{
		if(sprite == null) 
		{
			ClearSprite(index);
			return;
		}

		if(index >= icons.Length || index < 0) return;

		icons[index].sprite = sprite;
		icons[index].enabled = true;

	}
}
