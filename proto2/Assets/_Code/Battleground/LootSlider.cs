﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LootSlider : MonoBehaviour 
{
	public UIToggler toggler;
	public Slider slider;

	public void Set(float percent)
	{
		slider.value = percent;
		toggler.Show();
	}

	public void Hide()
	{
		toggler.Hide();

	}

	public void Show()
	{
		toggler.Show();
	}
}
