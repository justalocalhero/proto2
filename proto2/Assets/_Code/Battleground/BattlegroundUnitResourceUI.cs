﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattlegroundUnitResourceUI : MonoBehaviour 
{
	public BattlegroundUI battlegroundUI;
	private Battleground battleground;
	private UnitResourceSlot[] slots;
	private Unit unit;

	public void Start()
	{
		battleground = battlegroundUI.GetBattleground();
		slots = GetComponentsInChildren<UnitResourceSlot>();
		battleground.onPlayerChanged += SetUnit;
	}

	public void SetUnit(Unit newUnit)
	{
		if(newUnit == null)
		{
			Clear();
			return;
		}
		unit = newUnit;
		unit.onUnitResourceChanged += UpdateUI;
		unit.onDeath += ClearUI;
		UpdateUI();
	}

	public void UpdateUI()
	{
		if(unit == null)
		{
			for(int i = 0; i < slots.Length; i++)
			{
				slots[i].Clear();
			}
		}

		List<UnitResource> unitResources = unit.GetResourceList();

		for(int i = 0; i < slots.Length; i++)
		{
			if(i < unitResources.Count)
			{
				if(slots[i] != null) slots[i].Set(unitResources[i]);
				else slots[i].Clear();
			}
			else slots[i].Clear();
		}
	}

	public void ClearUI(Damage damage)
	{
		for(int i =0; i < slots.Length; i++)
		{
			slots[i].Clear();
		}
	}

	public void Clear()
	{
		if(unit != null)
		{
			unit.onUnitResourceChanged -= UpdateUI;
			unit.onDeath -= ClearUI;
			unit = null;
		}
	}
}
