﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BattlegroundUI : MonoBehaviour 
{
	public Battleground battleground;
	public ExpandedBattlegroundUI expandedUI;
	public UnitTooltip unitTooltip;
	public Button battlegroundButton;
	public TextMeshProUGUI nameMesh;
	public TextMeshProUGUI levelMesh;
	public UIToggler uiToggler;

	public ScrollingBackground scrollingBackground;
	public FloatingTextManager playerFloatingText;
	public FloatingTextManager enemyFloatingText;
	public Transform enemyDamageTextPosition;
	public Transform playerDamageTextPosition;

	private Unit player;
	private Unit enemy;

	public DamageColorMap damageColorMap;

	public void Awake()
	{
		Debug.Log("Rawr");
		playerFloatingText.SetParentToggler(uiToggler);
		enemyFloatingText.SetParentToggler(uiToggler);
	}

	public void Start()
	{
		battleground.onBattlegroundLevelUp += UpdateUI;
		battleground.onBattlegroundChanged += UpdateUI;
		battleground.onEnemyDamaged += QueueEnemyFloatingText;
		battleground.onPlayerDamaged += QueuePlayerFloatingText;
		battleground.onPlayerChanged += SetPlayer;
		battleground.onEnemyChanged += SetEnemy;
		battleground.onStateChanged += UpdateScrolling;
	}

	public void Initialize()
	{
		UpdateUI();
	}

	public void OnButtonPress()
	{
		expandedUI.SetBattleground(battleground);
		unitTooltip.Set(battleground.GetPlayer());
	}

	public void Enable()
	{
		battlegroundButton.interactable = true;
	}

	public void Disable()
	{
		battlegroundButton.interactable = false;
	}

	public void SetPlayer(Unit unit)
	{
		if(unit == null) return;

		if(player != null)
		{
			player.onActionTaken -= HandlePlayerActionTaken;
		}

		player = unit;

		player.onActionTaken += HandlePlayerActionTaken;
	}

	public void SetEnemy(Unit unit)
	{
		if(unit == null) return;

		if(enemy != null)
		{
			enemy.onActionTaken -= HandleEnemyActionTaken;
		}

		enemy = unit;

		enemy.onActionTaken += HandleEnemyActionTaken;
	}

	public void UpdateUI()
	{
		if(battleground.IsActive())
		{
			levelMesh.SetText("lvl " + battleground.GetCurrentLevel());
			levelMesh.enabled = true;
		}
		else
		{
			levelMesh.SetText("");
			levelMesh.enabled = false;
		}

		UpdateName();
	}

	public void UpdateScrolling(BattlegroundState previousState, BattlegroundState currentState)
	{
		if(scrollingBackground == null) return;

		if(currentState == BattlegroundState.travel) scrollingBackground.Activate();		
		else scrollingBackground.Deactivate();
	}

	public void ShownName()
	{
		nameMesh.fontSize = 36;
		UpdateName();
	}

	public void LockedName()
	{
		nameMesh.fontSize = 24;
		UpdateName();
	}

	public void UpdateName()
	{
		nameMesh.SetText(battleground.GetName());
	}

	public void QueueEnemyFloatingText(Damage damage)
	{
		if(damage.value == 0) return;
		
		string text = GetDamageText(damage);
		
		enemyFloatingText.Prepare(text);
	}

	public void QueuePlayerFloatingText(Damage damage)
	{
		if(damage.value == 0) return;
		
		string text = GetDamageText(damage);

		playerFloatingText.Prepare(text);
	}

	public void HandlePlayerActionTaken(Action action)
	{
		string text = GetActionText(action);
		playerFloatingText.Prepare(text);
	}

	public void HandleEnemyActionTaken(Action action)
	{
		string text = GetActionText(action);
		enemyFloatingText.Prepare(text);
	}

	public string GetActionText(Action action)
	{
		switch(action.result)
		{
			case ActionResult.block:
				return "Block";
			case ActionResult.dodge:
				return "Dodge";
			case ActionResult.reflect:
				return "Reflect";
			case ActionResult.miss:
				return "Miss";
			default:
				return "";
		}
	}

	public Battleground GetBattleground()
	{
		return battleground;
	}

	public string GetDamageText(Damage damage)
	{
		Color color = damageColorMap.GetColor(damage.type);
		string colorString = ColorUtility.ToHtmlStringRGBA(color);
		string valueString = "" + Mathf.Round(Mathf.Abs(damage.value));
		if(damage.type == DamageType.healing) valueString = "+" + valueString;
		if(damage.result == ActionResult.crit) valueString += "!";

		return "<color=#" + colorString + ">" + valueString + "</color>";
	}
}
