﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattlegroundTimedEffectUI : MonoBehaviour 
{
	public BattlegroundUI battlegroundUI;
	private Battleground battleground;
	private TimedEffectSlot[] slots;
	private Unit unit;
	public EffectType effectType;
	public UnitType unitType;


	public void Start()
	{
		battleground = battlegroundUI.GetBattleground();
		slots = GetComponentsInChildren<TimedEffectSlot>();
		if(unitType == UnitType.player) battleground.onPlayerChanged += SetUnit;
		if(unitType == UnitType.enemy) battleground.onEnemyChanged += SetUnit;
		if(unitType == UnitType.enemy) battleground.onEnemyDeath += Clear;
		battleground.onPlayerDeath += Clear;
	}

	public void SetUnit(Unit newUnit)
	{
		if(newUnit == null) return;
		unit = newUnit;
		//unit.onTimedEffectsChanged += UpdateUI;
		unit.onDeath += ClearUI;
		UpdateUI();
	}

	public void Clear(Player player)
	{
		Clear();
	}
	
	public void Clear()
	{
		if(unit != null)
		{
			//unit.onTimedEffectsChanged -= UpdateUI;
			unit.onDeath -= ClearUI;
			unit = null;
		}
		ClearUI();
	}

	public void ClearUI()
	{
		for(int i = 0; i < slots.Length; i++)
		{
			slots[i].Clear();
		}
	}

	public void ClearUI(Damage damage)
	{
		ClearUI();
	}

	public void UpdateUI()
	{
		if(unit == null)
		{
			ClearUI();
			return;
		}
		
		if(effectType == EffectType.buff) 
			UpdateSlots(unit.buffs);
		if(effectType == EffectType.debuff)
			UpdateSlots(unit.debuffs);

	}

	public void UpdateSlots<T>(List<T> effects) where T : TimedEffect
	{

		for(int i = 0; i < slots.Length; i++)
		{
			if(i < effects.Count)
			{
				if(slots[i] != null) slots[i].Set(effects[i]);
				else slots[i].Clear();
			}
			else slots[i].Clear();
		}

	}

	public void UpdateDebuffs()
	{

	}

	public enum EffectType {buff, debuff,};
	public enum UnitType {player, enemy,};
}


