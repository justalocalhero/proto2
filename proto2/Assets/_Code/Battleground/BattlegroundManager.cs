﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BattlegroundManager : MonoBehaviour 
{
	public SaveManager saveManager;

	public Transform battlegroundUIContainer;
	private Battleground[] battlegrounds;
	private Battling.BattlegroundUI[] battlegroundUIs;
	public FloatReference requiredLevel;
	public GraveyardManager graveyardManager;
	public TextMeshProUGUI battlingMesh;
	private int availableBattlegrounds = 0;
	public ReputationBuildingManager[] reputationManagers;
	// Use this for initialization

	public delegate void OnBattlegroundChanged();
	public OnBattlegroundChanged onBattlegroundChanged;

	public ColorReference maxedColor;
	public ColorReference defaultColor;
	public ColoredText coloredText;

	public delegate void OnEnemyDeath();
	public OnEnemyDeath onEnemyDeath;

	public delegate void OnPlayerDeath(Player player);
	public OnPlayerDeath onPlayerDeath;

	public void Awake()
	{
		saveManager.onPreSave += Save;
		saveManager.onLoad += Load;
	}

	public void Save(SaveFile saveFile)
	{
		if(saveFile == null) return;
		List<Battleground.State> toReturn = new List<Battleground.State>();
		
		foreach(Battleground battleground in battlegrounds)
		{
			toReturn.Add(battleground.Save());
		}

		saveFile.battlegroundStates = toReturn;
	}

	public void Load(SaveFile saveFile)
	{
		if(saveFile == null) return;
		if(saveFile.battlegroundStates == null) return;

		for(int i = 0; i < saveFile.battlegroundStates.Count; i++)
		{
			if(i < battlegrounds.Length) battlegrounds[i].Load(saveFile, saveFile.battlegroundStates[i]);
		}
		CheckBattlegrounds();
		UpdateBattlingMesh();
	}

	public void Initialize()
	{
		battlegroundUIs = battlegroundUIContainer.GetComponentsInChildren<Battling.BattlegroundUI>();
		battlegrounds = new Battleground[battlegroundUIs.Length];
		for(int i = 0; i < battlegroundUIs.Length; i++)
		{
			battlegrounds[i] = battlegroundUIs[i].battleground;
			battlegrounds[i].onBattlegroundChanged += UpdateBattlingMesh;
			battlegrounds[i].onBattlegroundChanged += RaiseOnBattlegroundChanged;
			battlegrounds[i].onBattlegroundLevelUp += CheckBattlegrounds;
			battlegrounds[i].SetReputationManager(reputationManagers);
			battlegrounds[i].onEnemyDeath += RaiseOnEnemyDeath;
			battlegrounds[i].onPlayerDeath += RaiseOnPlayerDeath;
		}
		graveyardManager.onGraveyardChanged += CheckBattlegrounds;
		CheckBattlegrounds();
		InitializeBattlegrounds();
		UpdateBattlingMesh();
	}

	public void InitializeBattlegrounds()
	{
		for(int i = 0; i < battlegroundUIs.Length; i++)
		{
			battlegrounds[i].Initialize();
		}
	}
	
	public void ShowBattleground(int index)
	{
		battlegroundUIs[index].Unlock();
		battlegrounds[index].SetShown(true);
		battlegroundUIs[index].UpdateName();
	}

	public void HideBattleground(int index)
	{
		battlegroundUIs[index].Lock();
		battlegrounds[index].SetShown(false);

		if(index > 0)
		{
			string unlockName = "Reach " + battlegrounds[index - 1].GetBaseName() + " LVL " + requiredLevel.Value;
			battlegrounds[index].SetUnlockName(unlockName);
			battlegroundUIs[index].UpdateName();

		}
	}
	
	public void CheckBattlegrounds()
	{
		int count = 0;

		for(int i = 0; i < battlegrounds.Length; i++)
		{
			if(CheckBattleground(i)) count++;
		}

		availableBattlegrounds = count;

		UpdateBattlingMesh();
	}

	public bool CheckBattleground(int index)
	{
		bool toShow = false;

		if(index <= 0)
		{
			ShowBattleground(index);
			toShow = true;
		}
		else if(battlegrounds[index - 1].GetMaxLevelReached() >= requiredLevel.Value)
		{
			ShowBattleground(index);
			toShow = true;
		}
		else
		{
			HideBattleground(index);
			toShow = false;
		}

		return toShow;

	}

	public int GetAvailableCount()
	{
		return availableBattlegrounds;
	}

	public int GetActiveCount()
	{
		int count = 0;
		for(int i = 0; i < battlegrounds.Length; i++)
		{
			if(battlegrounds[i].IsActive()) count++;
		}

		return count;
	}

	public void UpdateBattlingMesh()
	{
		int active = GetActiveCount();
		int available = GetAvailableCount();
		string text = (available > 0) ? active + " / " + available : "";
		Color color = (active == available) ? maxedColor : defaultColor;
		battlingMesh.SetText(coloredText.GetText(text, color));
	}

	public void RaiseOnBattlegroundChanged()
	{
		if(onBattlegroundChanged != null) onBattlegroundChanged();
	}

	public void RaiseOnEnemyDeath()
	{
		if(onEnemyDeath != null) onEnemyDeath();
	}

	public void RaiseOnPlayerDeath(Player player)
	{
		if(onPlayerDeath != null) onPlayerDeath(player);
	}
}
