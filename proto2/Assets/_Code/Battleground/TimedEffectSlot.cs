﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimedEffectSlot : MonoBehaviour 
{
	public SpriteManager spriteManager;
	public Image icon;
	private TimedEffect effect;

	public void Set(TimedEffect newEffect)
	{
		if(newEffect == null) 
		{
			Clear();
			return;
		}

		effect = newEffect;

		Sprite sprite = spriteManager.GetSprite(effect.GetSprite());
		if(sprite != null) ShowSprite(sprite);
		else ClearSprite();
	}

	public void Clear()
	{
		ClearSprite();
		effect = null;
	}

	public void ClearSprite()
	{
		icon.sprite = null;
		icon.enabled = false;
	}

	public void ShowSprite(Sprite sprite)
	{
		if(sprite == null) 
		{
			ClearSprite();
			return;
		}

		icon.sprite = sprite;
		icon.enabled = true;

	}
}
