﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class Battleground : MonoBehaviour
{
    public EncounterGenerator encounter;
    public IntReference maxLootSpace;

    [System.Serializable]
    public struct State 
    {

        public Player player;
        public Enemy enemy;
        public TriggeredEvent triggeredEvent;
    
        public List<Equipment> loot;
        
        public bool shown;
        public BattlegroundState currentState;

        public bool cleared;

        public float timeActive, damageDealt, damageTaken, lootTimer, exploringTimer, 
        exploringStartTimer, campingTimer, campingStartTimer, travelTimer, prepTimer;
        public int level, encounterCount, killCount, maxLevelReached, campEncounterCount;

    };

    public State state = new State();

    public Inventory inventory;
    public Forge forge;

    public Camp camp;
    
    private ReputationBuildingManager[] reputationManagers;

    new public StringReference name;
    private string unlockName;
    public FloatVariable maxLootTimer;
    public FloatReference maxCampingTimer, minCampingTimer, maxTravelTimer, maxPrepTimer;
    public IntReference encountersPerLevel, startingLevel, encountersPerCamp, ascensionLevel;
    public ArtifactEquipmentGenerator ascensionArtifact;
    public ArtifactManager artifactManager;
    public RandomEventGenerator randomEventGenerator;

    public delegate void OnBattlegroundLevelUp();
    public OnBattlegroundLevelUp onBattlegroundLevelUp;

    public delegate void OnBattlegroundChanged();
    public OnBattlegroundChanged onBattlegroundChanged;

    public delegate void OnPlayerDamaged(Damage damage);
    public OnPlayerDamaged onPlayerDamaged;

    public delegate void OnEnemyDamaged(Damage damage);
    public OnEnemyDamaged onEnemyDamaged; 
    
    public delegate void OnPlayerActionTaken(Action action);
    public OnPlayerActionTaken onPlayerActionTaken;

    public delegate void OnEnemyActionTaken(Action action);
    public OnEnemyActionTaken onEnemyActionTaken;

    public delegate void OnPlayerChanged(Unit unit);
    public OnPlayerChanged onPlayerChanged;

    public delegate void OnEnemyChanged(Unit unit);
    public OnEnemyChanged onEnemyChanged;

    public delegate void PlayerDeath(Player player);
    public PlayerDeath onPlayerDeath;

    public delegate void OnAscended(Player player);
    public OnAscended onAscended;

    public delegate void EnemyDeath();
    public EnemyDeath onEnemyDeath;

    public delegate void StateChanged(BattlegroundState previousState, BattlegroundState newState);
    public StateChanged onStateChanged;

	public delegate void OnLoot(Equipment equipment);
	public OnLoot onLoot;

    public delegate void OnRandomEvent(RandomEventGenerator.EventType type);
    public OnRandomEvent onRandomEvent;

    public void Initialize()
    {
        InitializeState();
        if(ValidPlayer())
        {
            AddPlayer(state.player);
        }
        else
        {
            ClearPlayer();
        }
        if(ValidEnemy())
        {
            AddEnemy(state.enemy);
        }
        else
        {
            ClearPlayer();
        }

        ResetLootTimer();
        ResetTravelTimer();
        SetIdle();
    }

    public void InitializeState()
    {
        state.loot = new List<Equipment>();
        state.currentState = BattlegroundState.camping;    
        state.level = 1;
        state.maxLevelReached = 1;
    }

    public void Update()
    {        
        UpdateTimers();
        if(!IsIdle()) 
        {
            if(state.player != null) 
                state.player.UpdateTimers(Time.deltaTime, (state.enemy != null && state.enemy.IsAlive() && state.prepTimer <= 0) ? state.enemy : null);
            if(state.enemy != null)
                state.enemy.UpdateTimers(Time.deltaTime, (state.player != null && state.player.IsAlive() && state.prepTimer <= 0) ? state.player : null);
            state.timeActive += Time.deltaTime;
        }        
    }

    public State Save()
    {
        return state;
    }

    public void Load(SaveFile saveFile, State newState)
    {
        if(saveFile == null) return;

        state = newState;

        if(ValidateUnit(newState.player))
        {
            newState.player.Load(saveFile);
            SetPlayer(newState.player);
        }
        else 
        {
            ClearPlayer();
        }

        if(ValidateUnit(newState.enemy))
        {
            newState.enemy.Load(saveFile);
            SetEnemy(newState.enemy);
        }
        else 
        {
            ClearEnemy();
        }

        if(newState.triggeredEvent != null) 
            AddEvent(newState.triggeredEvent);

        SetState(newState.currentState);
        RaiseOnBattlegroundChanged();


    }

    public void SetPlayers()
    {
        
    }

    public void UpdateTimers()
    {
        UpdateLootTimer();
        UpdateCampingTimer();
        UpdateExploringTimer();
        UpdateTravelTimer();
        UpdatePrepTimer();
    }

    public void UpdateLootTimer()
    {
        if(!HasLoot()) return;

        if(state.lootTimer > 0) state.lootTimer -= Time.deltaTime;
        else
        {
            if(state.loot.Count > 0)
            {
                ResetLootTimer();
                GatherLoot();
            }
        }        
    }

    public void UpdateCampingTimer()
    {
        if(!IsCamping()) return;

        if(state.campingTimer > 0) 
        {
            state.campingTimer -= Time.deltaTime;
        }
        else 
        {
            NextEncounter();
        }
    }

    public void UpdateTravelTimer()
    {
        if(!IsTravel()) return;

        if(state.travelTimer > 0)
        {
            state.travelTimer -= Time.deltaTime;
        }
        else
        {
            if(CheckCampingCount()) ResetCampingTimer();
            else NextEncounter();
        }
    }

    public void UpdatePrepTimer()
    {
        if(!IsFighting()) return;

        if(state.prepTimer > 0)
        {
            state.prepTimer -= Time.deltaTime;
        }
    }

    public void ResetCampingTimer()
    {
        if(state.player == null) return;
        state.campEncounterCount = 0;
        SetCamping();
        state.player.Camp();
        float walkSpeed = (state.player == null) ? 1 : state.player.GetWalkSpeedFactor();
        state.campingStartTimer = (walkSpeed <= 0) ? minCampingTimer.Value : maxCampingTimer.Value / walkSpeed;
        state.campingTimer = state.campingStartTimer;
    }

    public void ResetTravelTimer()
    {
        SetTravel();
        state.travelTimer = maxTravelTimer;
    }

    public bool CheckCampingCount()
    {
        return state.campEncounterCount >= encountersPerCamp;
    }

    public void UpdateExploringTimer()
    {
        if(!IsExploring()) return;
        if(state.exploringTimer > 0) 
        {
            state.exploringTimer -= Time.deltaTime;
        }
        else 
        {
            state.triggeredEvent = null;
            ResetTravelTimer();
        }
    }

    public void ResetExploringTimer(float duration)
    {
        if(state.player == null) return;
        SetExploring();
        float walkSpeed = state.player.GetWalkSpeedFactor();

        //fix these magic numbers... acquire from triggered event
        state.exploringStartTimer = (walkSpeed <= 0 || duration <= 0) ? 2 : duration / walkSpeed;
        state.exploringTimer = state.exploringStartTimer;
    }

    public void GatherLoot()
    {
        if(inventory == null) return;
        if(forge == null) return;
        if(state.loot == null) return;
        if(state.loot.Count <= 0) return;
        
        Equipment toAdd = state.loot[0];

        if(forge.CheckAutoForge(toAdd)) forge.Add(toAdd);
        else inventory.Add(toAdd);
        state.loot.RemoveAt(0);

        RaiseOnLoot(toAdd);
    }

    public void AddPlayer(Player newPlayer)
    {
        if(!ValidateUnit(newPlayer)) 
            return;

        SetPlayer(newPlayer);
        ResetPlayer();
    }

    public void SetPlayer(Player newPlayer)
    {
        if(state.player != null) 
            ClearPlayer();

        if(!ValidateUnit(newPlayer)) 
            return;

        state.player = newPlayer;
        state.player.onDeath += OnPlayerDeath;
        state.player.onDamage += OnPlayerDamage;
        state.player.onEscape += TryEscape;
        state.player.onActionTaken += RaiseOnPlayerActionTaken;

        RaiseOnPlayerChanged(newPlayer);
        
    }

    public void ResetPlayer()
    {
        if(state.player == null) return;

        state.player.ResetHitpoints();
        state.player.ResetGlobalCooldown();
    }

    public void ClearPlayer()
    {
        if(state.player != null)
        {
            state.player.onDeath -= OnPlayerDeath;
            state.player.onDamage -= OnPlayerDamage;
            state.player.onEscape -= TryEscape;
            state.player.onActionTaken -= RaiseOnPlayerActionTaken;
            state.player = null;
        }
    }

    public void AddEnemy(Enemy newEnemy)
    {
        if(!ValidateUnit(newEnemy)) 
            return;

        SetEnemy(newEnemy);
        ResetEnemy();
        
        if(!state.enemy.IsAlive())
        {
            Debug.LogWarning("Attempted to add non-alive Enemy to battleground");
            ResetTravelTimer();
            return;
        }
    }
    
    public void SetEnemy(Enemy newEnemy)
    {
        if(!ValidPlayer()) return;
        if(!ValidateUnit(newEnemy))
        {
            Debug.LogWarning("Attempted to Add Null Enemy To Battleground");
            ResetTravelTimer();
            return;
        }
        ClearEnemy();
        
        state.enemy = newEnemy;
        state.enemy.onDeath += OnEnemyDeath;
        state.enemy.onDamage += OnEnemyDamage;
        state.enemy.onActionTaken += RaiseOnEnemyActionTaken;
        state.prepTimer = maxPrepTimer;
        SetFighting();
        RaiseOnEnemyChanged(newEnemy);
    }    

    public void ResetEnemy()
    {
        if(!ValidEnemy()) return;

        state.enemy.ResetHitpoints();
        state.enemy.PrepCooldowns();
    }

    public void ClearEnemy()
    {
        if(state.enemy != null)
        {
            state.enemy.onDeath -= OnEnemyDeath;
            state.enemy.onDamage -= OnEnemyDamage;
            state.enemy.onActionTaken -= RaiseOnEnemyActionTaken;
            state.enemy = null;
        }
    }

    public void AddEvent(TriggeredEvent newEvent)
    {
        if(newEvent == null) return;
        state.triggeredEvent = newEvent;
        bool triggerCheck = newEvent.TriggerCheck(this, state.player, state.level);
        ResetExploringTimer(newEvent.GetDuration());
    }

    public void End()
    {
        SetIdle();
        ClearPlayer();
        ClearEnemy();
        RaiseOnBattlegroundChanged();
        RaiseOnBattlegroundLevelUp();
    }

    public void Reset()
    {
        ResetStats();
        ResetEncounter();
        RaiseOnBattlegroundChanged();
        RaiseOnBattlegroundLevelUp();
    }

    public void ResetStats()
    {
        state.timeActive = state.damageDealt = state.damageTaken = 0;
        state.killCount = state.encounterCount = state.campEncounterCount = 0;
        state.level = 1;
    }

    public void ResetLoot()
    {
        state.loot.Clear();
    }

    public void ResetEncounter()
    {
        ResetTravelTimer();
    }

    public void ResetLootTimer()
    {
		state.lootTimer = maxLootTimer.Value;
    }

    public void NextEncounter()
    {
        state.encounterCount++;
        state.campEncounterCount++;
        
        if(state.encounterCount > encountersPerLevel.Value)
        {
            LevelUp();     
        }
        
        if(!IsActive()) return;

        if(CheckRandomEvent()) 
            RandomEvent();

        System.Object newEncounter = encounter.Generate(state.level + startingLevel);
        if(newEncounter is Enemy) 
        {
            
            AddEnemy(newEncounter as Enemy);      
            if(GetStealthCheck()) 
            {
                state.enemy.ZeroCooldowns();
                state.player.Stealth();
            }
        }
        if(newEncounter is TriggeredEvent) AddEvent(newEncounter as TriggeredEvent);
    }

    public bool CheckRandomEvent()
    {
        if(state.player == null) return false;

        return UnityEngine.Random.Range(0,1f) < state.player.GetPerceptionCheck();
    }

    public void RandomEvent()
    {
        RaiseOnRandomEvent(randomEventGenerator.Generate(this));
    }

    public void LevelUp()
    {
        state.encounterCount = 0;
        state.level++;

        if(state.level > state.maxLevelReached) 
        {
            state.maxLevelReached = state.level;            
        }

        if(state.level > ascensionLevel)
        {
            Ascend(state.player);
        }

        RaiseOnBattlegroundLevelUp(); 

    }

    public void Ascend(Player player)
    {
        if(player == null) return;
        PushLoot(ascensionArtifact.Generate());
        
        state.cleared = true;
        End();
        RaiseOnAscended(player);
    }

    public bool GetStealthCheck()
    {
        float stealthRoll;
        float r;

        r = UnityEngine.Random.Range(0, 1.0f);
        stealthRoll = state.player.GetStealthRoll();
        return r < stealthRoll;
    }

    public bool NextPlayer(Player newPlayer)
    {
        if(newPlayer == null) return false;
        AddPlayer(newPlayer);
        return true;
    }

    public void TryEscape()
    {
        NextEncounter();
    }

    public void OnPlayerDeath(Damage damage)
    {
        Snapshot snapshot = new Snapshot(state.player, damage, state.killCount, state.level);
        RaiseOnPlayerDeath(state.player);
        End();
    }

    public void OnPlayerDamage(Damage damage)
    {
        state.damageTaken += damage.value;
        RaiseOnPlayerDamaged(damage);
    }

    public void RaiseOnPlayerDamaged(Damage damage)
    {
        if(onPlayerDamaged != null) onPlayerDamaged(damage);
    }

    public void OnEnemyDeath(Damage damage)
    {
        state.killCount++;

        if(CanAddLoot())
        {
            Equipment loot = (artifactManager.CheckDrop()) ? artifactManager.GetDrop() : state.enemy.GenerateLoot();
            if(loot != null) AddLoot(loot);
        }
        
        ResetTravelTimer();
        RaiseOnEnemyDeath();
    }

    public void OnEnemyDamage(Damage damage)
    {
        state.damageDealt += damage.value;
        RaiseOnEnemyDamaged(damage);
    }

    public void RaiseOnEnemyDamaged(Damage damage)
    {
        if(onEnemyDamaged != null) onEnemyDamaged(damage);
    }

    public void HealPlayer(float amount)
    {
        if(state.player == null) return;

        state.player.Heal(amount);
    }

    public void BuffPlayer(Buff buff)
    {
        if(buff == null) return;
        if(state.player == null) return;

        state.player.AddBuff(buff);
    }
    
    public bool CanAddLoot()
    {
        return !(state.loot == null || state.loot.Count >= maxLootSpace);
    }

    public void AddLoot(Equipment newLoot)
    {
        if(newLoot == null) return;

        if(CanAddLoot()) state.loot.Add(newLoot);
        else artifactManager.Add(newLoot);
    }
    
    public void PushLoot(Equipment newLoot)
    {
        if(newLoot == null) return;
        inventory.Add(newLoot);
    }

    public void RaiseOnBattlegroundLevelUp()
    {
        if(onBattlegroundLevelUp != null) onBattlegroundLevelUp();
    }

    public void RaiseOnBattlegroundChanged()
    {
        if(onBattlegroundChanged != null) onBattlegroundChanged();
    }

    public void RaiseOnPlayerChanged(Unit unit)
    {
        if(onPlayerChanged != null) onPlayerChanged(unit);
    }

    public void RaiseOnEnemyChanged(Unit unit)
    {
        if(onEnemyChanged != null) onEnemyChanged(unit);
    }

    public void RaiseOnEnemyDeath()
    {
        if(onEnemyDeath != null) onEnemyDeath();
    }

    public void RaiseOnPlayerDeath(Player player)
    {
        if(onPlayerDeath != null) onPlayerDeath(player);
    }

    public void RaiseOnStateChanged(BattlegroundState previousState, BattlegroundState newState)
    {
        if(onStateChanged != null) onStateChanged(previousState, newState);
    }

    public void RaiseOnEnemyActionTaken(Action action)
    {
        if(onEnemyActionTaken != null) onEnemyActionTaken(action);
    }

    public void RaiseOnPlayerActionTaken(Action action)
    {
        if(onPlayerActionTaken != null) onPlayerActionTaken(action);        
    }

    public void RaiseOnRandomEvent(RandomEventGenerator.EventType type)
    {
        if(onRandomEvent != null) onRandomEvent(type);
    }

	public void RaiseOnLoot(Equipment equipment)
	{
		if(onLoot != null) 
			onLoot(equipment);
	}

    public void RaiseOnAscended(Player player)
    {
        if(onAscended != null) onAscended(player);
    }
    public void SetUnlockName(string newUnlockName)
    {
        unlockName = newUnlockName;
    }

    public void SetReputationManager(ReputationBuildingManager[] newReputationManagers)
    {
        if(newReputationManagers == null) return;
        reputationManagers = newReputationManagers;
    }

    public void SetShown(bool isShown)
    {
        state.shown = isShown;
    }

    public int GetMaxLevelReached()
    {
        return state.maxLevelReached;
    }

    public int GetCurrentLevel()
    {
        return state.level;
    }

    public float GetCampingScale()
    {
        if(state.campingStartTimer == 0 || state.campingTimer == 0) return 1;
        return Mathf.Clamp01(1 - state.campingTimer / state.campingStartTimer);
    }

    public float GetExploringScale()
    {
        if(state.exploringStartTimer == 0 || state.exploringTimer == 0) return 1;
        return Mathf.Clamp01(1 - state.exploringTimer / state.exploringStartTimer);
    }

    public float GetPlayerHPScale()
    {
        if(state.player == null) return 0;
        if(state.player.GetMaxHitpoints() <= 0) return 0;
        return Mathf.Clamp01(state.player.GetCurrentHitpoints() / state.player.GetMaxHitpoints());
    }

    public float GetEnemyHPScale()
    {
        if(state.enemy == null) return 0;
        if(state.enemy.GetMaxHitpoints() <= 0) return 0;
        return Mathf.Clamp01(state.enemy.GetCurrentHitpoints() / state.enemy.GetMaxHitpoints());
    }

    public bool HasLoot()
    {
        if(state.loot == null) return false;
        return state.loot.Count > 0;
    }

    public float LootGatherScale()
    {
        return (maxLootTimer.Value == 0) ? 0 : 1 - (state.lootTimer / maxLootTimer.Value);
    }

    public void SetState(BattlegroundState newState)
    {
        BattlegroundState previousState = state.currentState;
        state.currentState = newState;

        RaiseOnStateChanged(previousState, newState);
        
    }

    public void SetIdle()
    {
        SetState(BattlegroundState.idle);
    }

    public void SetTravel()
    {
        SetState(BattlegroundState.travel);
    }

    public void SetExploring()
    {
        SetState(BattlegroundState.exploring);
    }

    public void SetFighting()
    {
        SetState(BattlegroundState.fighting);
    }

    public void SetCamping()
    {
        SetState(BattlegroundState.camping);
    }

    public bool IsIdle()
    {
        return state.currentState == BattlegroundState.idle;
    }

    public bool IsCamping()
    {
        return state.currentState == BattlegroundState.camping;
    }

    public bool IsFighting()
    {
        return state.currentState == BattlegroundState.fighting;
    }

    public bool IsExploring()
    {
        return state.currentState == BattlegroundState.exploring;
    }

    public bool IsTravel()
    {
        return state.currentState == BattlegroundState.travel;
    }

    public bool IsActive()
    {
        return !IsIdle();
    }

    public string GetBaseName()
    {
        return encounter.GetName(0);
    }

    public string GetName()
    {
        if(state.shown) 
            return encounter.GetName(GetCurrentLevel());
        else 
            return unlockName;
    }

    public Player GetPlayer()
    {
        return state.player;
    }

    public Enemy GetEnemy()
    {
        return state.enemy;
    }

    public TriggeredEvent GetTriggeredEvent()
    {
        return state.triggeredEvent;
    }

    public int GetLootCount()
    {
        return state.loot.Count;
    }

    public int GetEncounterCount()
    {
        return state.encounterCount;
    }

    public int GetKillCount()
    {
        return state.killCount;
    }

    public bool ValidPlayer()
    {
        return ValidateUnit(state.player);
    }

    public bool ValidEnemy()
    {
        return ValidateUnit(state.enemy);
    }

    public bool ValidateUnit(Unit unit)
    {
        return (unit != null && !unit.IsDefault());
    }
}

public enum BattlegroundState {camping, fighting, exploring, travel, idle};