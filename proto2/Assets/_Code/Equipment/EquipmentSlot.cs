﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tooltips;

public class EquipmentSlot : MonoBehaviour 
{
    public SpriteManager spriteManager;
    private RectTransform rect;
    public Image icon;
    public Image shadowIcon;
    public Image highlightMask;
    public Equipment equipment;
    public EquipmentManager equipmentManager {get; set;}
    public Inventory inventory {get; set;}
    public EquipmentTooltip tooltip {get;  set;}

    public void Awake()
    {
        rect = transform as RectTransform;
    }

    public void Add(Equipment newEquipment)
    {
        if(newEquipment == null) 
        {
            Clear();
            return;
        }
        equipment = newEquipment;
        icon.sprite = spriteManager.GetSprite(newEquipment.GetSprite());
        icon.enabled = true;
        if(shadowIcon != null) shadowIcon.enabled = false;
    }

    public void Clear()
    {
        equipment = null;
        icon.sprite = null;
        icon.enabled = false;
        if(shadowIcon != null) shadowIcon.enabled = true;
    }

    public void OnButtonPress()
    {
        if(inventory == null) return;
        if(equipment == null) return;
        inventory.Add(equipment);
        equipmentManager.Remove(equipment.slot);
        Clear();

    }

    public void OnPointerEnter()
    {
        if(equipment != null) tooltip.Set(equipment, rect);
    }

    public void OnPointerExit()
    {
        tooltip.Hide();
    }

    public void Highlight(bool toHighlight)
    {
        highlightMask.enabled = toHighlight;
    }

    public RectTransform GetRect()
    {
        return rect;
    }
}
