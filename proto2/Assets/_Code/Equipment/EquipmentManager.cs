﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EquipmentManager : MonoBehaviour, IEquipmentReciever
{
    public SaveManager saveManager;

    public EquipmentUI equipmentUI;
    public Equipment[] equipment  {get; set;}
    public EquipSlot highlightSlot {get; set;}
    public bool toHighlight {get; set;}

    public Inventory inventory;
    public EquipmentRouter equipmentRouter;

    public delegate void OnEquipmentChanged();
    public OnEquipmentChanged onEquipmentChanged;

    public delegate void OnHighlightChanged();
    public OnHighlightChanged onHighlightChanged;

    public void Awake()
    {
        Clear();

        if(saveManager != null)
        {
            saveManager.onPreSave += Save;
            saveManager.onLoad += Load;
        }
    }

    public void Save(SaveFile saveFile)
    {
        if(saveFile == null) return;

        List<Equipment> toSave = new List<Equipment>();

        foreach(Equipment equipment in equipment)
        {
            if(equipment != null) toSave.Add(equipment);
        }

        saveFile.equipped = toSave.ToArray();

    }

    public void Load(SaveFile saveFile)
    {
        if(saveFile == null) return;
        if(saveFile.equipped == null) return;

        Clear();
        
        SetEquipment(saveFile.equipped);

    }

    public void Clear()
    {
        equipment = new Equipment[Enum.GetNames(typeof(EquipSlot)).Length];     
    }

    public void Recieve(Equipment equipment, Inventory inventory)
    {
        inventory.Remove(equipment);
        Equipment toReturn = Add(equipment);
        if(toReturn != null) inventory.Add(toReturn);
    }

    public Equipment Add(Equipment newEquipment)
    {
        return Add(newEquipment, true);
    }

    public Equipment Add(Equipment newEquipment, bool toRaise)
    {
        Equipment toReturn = null;

        if(newEquipment != null)
        {
            int index = (int)newEquipment.slot;
            toReturn = equipment[index];
            equipment[index] = newEquipment;
            if(toRaise) RaiseEquipmentChanged();
        }

        return toReturn;
    }

    public void SetEquipment(Equipment[] newEquipment)
    {
        if(newEquipment != null)
        {
            foreach(Equipment equipment in newEquipment)
            {
                Add(equipment, false);
            }

            RaiseEquipmentChanged();
        }
    }

    public Equipment Remove(EquipSlot slot, bool toRaise)
    {
        Equipment toReturn = equipment[(int)slot];
        ClearSlot(slot, toRaise);
        return toReturn;
    }

    public Equipment Remove(EquipSlot slot)
    {
        return Remove(slot, true);
    }

    public Equipment[] RemoveAll()
    {
        return RemoveAll(true);
    }

    public Equipment[] RemoveAll(bool toRaise)
    {
        Equipment[] toReturn = new Equipment[equipment.Length];

        for(int i = 0; i < equipment.Length; i++)
        {
            toReturn[i] = Remove((EquipSlot)i, false);
        }

        if(toRaise) RaiseEquipmentChanged();

        return toReturn;
    }

    public bool HasAtSlot(Equipment toCheck)
    {
        if(toCheck == null) return false;
        int slot = (int) toCheck.GetSlot();

        if(slot < 0  || slot >= equipment.Length) return false;

        if(equipment[slot] == null) return false;

        return true;
    }

    public void ClearSlot(EquipSlot slot)
    {
        ClearSlot(slot, true);
    }

    public void ClearSlot(EquipSlot slot, bool toRaise)
    {
        if(equipment[(int)slot] == null) return;
        equipment[(int)slot] = null;
        if(toRaise) RaiseEquipmentChanged();

    }

    public Equipment GetEquipment(int slot)
    {
        if(equipment.Length <= slot)
        {
            Debug.LogWarning("Unsupported Equipment type");
            return null;
        }
        return equipment[slot];
    }

    public Equipment GetEquipment(EquipSlot slot)
    {
        return GetEquipment((int)slot);
    }

    public RectTransform GetSlotRect(EquipSlot slot)
    {
        return equipmentUI.GetSlotRect(slot);
    }

    public void RaiseEquipmentChanged()
    {
        if (onEquipmentChanged != null) onEquipmentChanged();
    }
    
    public void RaiseHighlightChanged()
    {
        if (onHighlightChanged != null) onHighlightChanged();
    }

    public int GetTotalSlots(Inventory inventory)
    {
        int count = 0;

        for(int i = 0; i < inventory.items.Length; i++)
        {
            if(i < equipment.Length)
            {
                if(inventory.items[i].Count > 0 || equipment[i] != null)
                    count++;
            }
        }

        return count;
    }

    public int GetFilledSlots()
    {
        int count = 0;
        for(int i = 0; i < equipment.Length; i++)
        {
            if(equipment[i] != null) count++;
        }

        return count;
    }

    public void SetHighlightSlot(EquipSlot slot)
    {
        highlightSlot = slot;
        toHighlight = true;
        RaiseHighlightChanged();
    }

    public void ClearHighlightSlot()
    {
        toHighlight = false;
        RaiseHighlightChanged();
    }

    public void SetAsRouterTarget()
    {
        equipmentRouter.SetActiveReciever(this);
    }
    
}