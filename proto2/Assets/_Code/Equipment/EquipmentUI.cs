﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Tooltips;

public class EquipmentUI : MonoBehaviour 
{
    public Transform equipmentSlotHolder;
    protected EquipmentSlot[] slots;
    public EquipmentManager equipmentManager;
    public EquipmentTooltip tooltip;
    public Inventory inventory;
    public TextMeshProUGUI filledSlotsText;
    public ColorReference maxedColor;
    public ColorReference defaultColor;
    public ColoredText coloredText;
    public UIToggler toggler;

    public void Start()
    {
        slots = equipmentSlotHolder.GetComponentsInChildren<EquipmentSlot>();
        equipmentManager.onEquipmentChanged += UpdateUI;
        inventory.onInventoryChanged += UpdateButton;
        equipmentManager.onHighlightChanged += UpdateHighlight;
        toggler.onShow += OnShow;
        PassReferences();
        UpdateUI();
    }

    public void PassReferences()
    {
        for(int i = 0; i < slots.Length; i++)
        {
            slots[i].tooltip = tooltip;
            slots[i].equipmentManager = equipmentManager;
            slots[i].inventory = inventory;
        }
    }

    public void UpdateUI()
    {
        UpdateSlots();
        UpdateButton();
    }

    public void UpdateSlots()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (equipmentManager.equipment != null && i < equipmentManager.equipment.Length && equipmentManager.equipment[i] != null)
            {
                slots[i].Add(equipmentManager.equipment[i]);
            }
            else
            {
                slots[i].Clear();
            }
        }
    }

    public void UpdateButton()
    {
        if(filledSlotsText == null) return;
        int total = equipmentManager.GetTotalSlots(inventory);
        int filled = equipmentManager.GetFilledSlots();
        string text = (total > 0) ? filled + " / " + total : "";
        Color color = (total == filled) ? maxedColor : defaultColor;
        filledSlotsText.SetText(coloredText.GetText(text, color));
    }

    public void UpdateHighlight()
    {
        slots[(int)equipmentManager.highlightSlot].Highlight(equipmentManager.toHighlight);
    }

    public RectTransform GetSlotRect(EquipSlot slot)
    {
        int index = (int)slot;
        if(index < 0 || index >= slots.Length) 
        {
            Debug.LogWarning("Slot out of bounds");
            return null;
        }
        return slots[(int)slot].GetRect();
    }

    public void OnShow()
    {
        equipmentManager.SetAsRouterTarget();
    }
}
