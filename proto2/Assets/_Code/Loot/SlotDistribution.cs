﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	[CreateAssetMenu(menuName="Loot/LootProfile/SlotDistribution")]
	public class SlotDistribution : ScriptableObject 
	{		
        public List<SlotChance> slots;

		public EquipSlot Get()
		{
			float total = 0;

			foreach(SlotChance current in slots)
			{
				total += current.chance;
			}

			float r = UnityEngine.Random.Range(0, total);

			foreach(SlotChance current in slots)
			{
				if(r < current.chance) return current.category;
				else r -= current.chance;
			}

			Debug.LogWarning("Equip Slot Not Found.");
			return EquipSlot.Mainhand;
		}
	}	
	[System.Serializable]
    public struct SlotChance { public EquipSlot category; public float chance; }
}