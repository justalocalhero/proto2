﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	public class EffectWindow : MonoBehaviour 
	{
		public UIToggler toggler;
		public FloatField durationField, periodField;
		public DamageWindow effectDamages;
		public StatWindow effectStats;

		private TimedEffectBuilder effect;

		public void Awake()
		{
			periodField.onChanged += HandlePeriodEdit;
			durationField.onChanged += HandleDurationEdit;
			effectStats.onEdit += HandleStatEdit;
			effectDamages.onEdit += HandleDamageEdit;
		}
		
		public void Set(TimedEffectBuilder timedEffect)
		{
			if(timedEffect == null) return;

			effect = timedEffect;

			effectStats.Set(Utility.ToList(timedEffect.stats));
			effectDamages.Set(Utility.ToList(timedEffect.periodicDamages));
			durationField.Set(timedEffect.duration);
			periodField.Set(timedEffect.period);

			Show();
		}


		public void Clear()
		{
			effect = null;
			
			effectStats.Clear();
			effectDamages.Clear();
			durationField.Clear();
			periodField.Clear();

			Hide();
		}

		public void HandleStatEdit()
		{
			if(effect == null) return;

			effect.SetStats(effectStats.GetStats());
		}

		public void HandleDamageEdit()
		{
			if(effect == null) return;

			effect.SetPeriodicDamages(effectDamages.GetDamages());

		}

		public void HandleDurationEdit(float duration)
		{
			if(effect == null) return;

			effect.EditDuration(duration);
		}

		public void HandlePeriodEdit(float period)
		{
			if(effect == null) return;

			effect.EditPeriod(period);
		}

		public void Show()
		{
			toggler.Show();
		}

		public void Hide()
		{
			toggler.Hide();
		}
	}
}