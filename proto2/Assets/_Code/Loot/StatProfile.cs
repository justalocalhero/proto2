﻿using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Generators/StatProfile")]
public class StatProfile : ScriptableObject
{
    public GeneratorStat[] stats;

    public GeneratorStat GetNextStat()
    {
        float totalFrequency = 0;

        for(int i = 0; i < stats.Length; i++)
        {
            totalFrequency += stats[i].frequency;
        }

        float r = UnityEngine.Random.Range(0, totalFrequency);

        for(int i = 0; i < stats.Length; i++)
        {
            if(r < stats[i].frequency)
            {
                return stats[i];
            }
            else
            {
                r -= stats[i].frequency;
            }
        }
        Debug.LogWarning("Failed to Find GeneratorStat");
        return new GeneratorStat { };
    }
    
}

[Serializable]
public struct GeneratorStat { public ModifierType modifier;  public StatType stat; public float frequency; };