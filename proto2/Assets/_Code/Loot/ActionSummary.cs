﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	public class ActionSummary : MonoBehaviour 
	{
		public UIToggler toggler;
		public AbilityWindow abilityWindow;
		public TriggeredActionWindow triggeredActionWindow;

		public void Set(AbilityBuilder builder)
		{
			triggeredActionWindow.Clear();
			abilityWindow.Set(builder);
			Show();
		}

		public void Set(TriggeredActionBuilder builder)
		{
			abilityWindow.Clear();
			triggeredActionWindow.Set(builder);
			Show();
		}

		public void Clear()
		{
			abilityWindow.Clear();
			triggeredActionWindow.Clear();
			Hide();
		}

		public void Show()
		{
			toggler.Show();
		}

		public void Hide()
		{
			toggler.Hide();
		}
	}
}