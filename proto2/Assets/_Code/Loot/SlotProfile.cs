﻿using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Generators/SlotProfile")]
public class SlotProfile : ScriptableObject {
    public GeneratorSlot[] slots;

    public GeneratorSlot GetNextSlot()
    {

        float totalFrequency = 0;

        for (int i = 0; i < slots.Length; i++)
        {
            totalFrequency += slots[i].frequency;
        }

        float r = UnityEngine.Random.Range(0, totalFrequency);

        for (int i = 0; i < slots.Length; i++)
        {
            if (r < slots[i].frequency)
            {
                return slots[i];
            }
            else
            {
                r -= slots[i].frequency;
            }
        }
        Debug.LogWarning("Failed to Find GeneratorSlot");
        return new GeneratorSlot { };
    }
}

[Serializable]
public struct GeneratorSlot { public EquipSlot slot; public float frequency; public Sprite sprite; };