﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Generators/ArchetypeProfile")]
public class ArchetypeProfile : ScriptableObject
{
	public GeneratorEquipmentArchetype[] archetypes;

	public EquipmentArchetype GetNext()
    {

        float totalFrequency = 0;

        for (int i = 0; i < archetypes.Length; i++)
        {
            totalFrequency += archetypes[i].frequency;
        }

        float r = UnityEngine.Random.Range(0, totalFrequency);

        for (int i = 0; i < archetypes.Length; i++)
        {
            if (r < archetypes[i].frequency)
            {
                return archetypes[i].equipmentArchetype;
            }
            else
            {
                r -= archetypes[i].frequency;
            }
        }
        Debug.LogWarning("Failed to Find GeneratorSlot");
        return new EquipmentArchetype { };
    }

    public EquipmentArchetype[] GetAll()
    {
        List<EquipmentArchetype> archetypesList = new List<EquipmentArchetype>();

        for(int i = 0; i < archetypes.Length; i++)
        {
            archetypesList.Add(archetypes[i].equipmentArchetype);
        }

        return archetypesList.ToArray();
    }
	
}
[Serializable]
public struct GeneratorEquipmentArchetype { public EquipmentArchetype equipmentArchetype; public float frequency;};