﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Loot
{
	public class ActionWindow : MonoBehaviour 
	{
		public UIToggler toggler;
		public DamageWindow damageWindow;
		public EffectWindow effectWindow;
		public Button left, right;
		private ActionBuilder action;
		private int currentEffect = 0;

		public void Awake()
		{
			damageWindow.onEdit += HandleDamageEdit;
			left.onClick.AddListener(DecrementEffect);
			right.onClick.AddListener(IncrementEffect);
		}

		public void Set(ActionBuilder builder)
		{
			if(builder == null) return;
			currentEffect = 0;

			action = builder;
			damageWindow.Set(Utility.ToList(action.damages));

			UpdateEffect();
			UpdateUI();

			Show();
		}

		public void Clear()
		{
			action = null;
			currentEffect = 0;

			damageWindow.Clear();
			effectWindow.Clear();
			
			UpdateUI();

			Hide();
		}

		public void UpdateEffect()
		{
			if(HasEffect(currentEffect))
			{
				effectWindow.Set(GetEffect(currentEffect));
			}
			else
			{
				effectWindow.Clear();
			}
		}

		private void UpdateUI()
		{
			UpdateEffectButtons();
		}

		private void UpdateEffectButtons()
		{
			SetButtonVisible(left, currentEffect > 0);
			SetButtonVisible(right, (currentEffect + 1) < GetEffectCount());			
		}

		private void IncrementEffect()
		{
			currentEffect++;

			UpdateEffect();
			UpdateEffectButtons();
		}

		private void DecrementEffect()
		{			
			currentEffect--;

			UpdateEffect();
			UpdateEffectButtons();
		}

		private void SetButtonVisible(Button button, bool isVisible)
		{
			if(button.gameObject.activeSelf != isVisible)
			{
				button.gameObject.SetActive(isVisible);
			}
		}

		public void HandleDamageEdit()
		{
			if(action == null) return;

			action.SetDamages(damageWindow.GetDamages());
		}

		private int GetBuffCount()
		{
			if(action == null) return 0;

			return action.buffs.Length;
		}

		private int GetDebuffCount()
		{
			if(action == null) return 0;

			return action.debuffs.Length;
		}

		private int GetEffectCount()
		{
			if(action == null) return 0;

			return GetBuffCount() + GetDebuffCount();
		}

		private bool HasEffect(int index)
		{
			return (index >= 0 && index < GetEffectCount());
		}

		private TimedEffectBuilder GetEffect(int index)
		{
			if(index < 0 || index >= GetEffectCount()) return null;

			int buffCount = GetBuffCount();

			if(index < buffCount) return action.buffs[index];
			return action.debuffs[index - buffCount];
		}

		public void Show()
		{
			toggler.Show();
		}

		public void Hide()
		{
			toggler.Hide();
		}
	}
}