﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Loot
{
	public class TriggeredActionWindow : MonoBehaviour 
	{
		public UIToggler toggler;
		public TextMeshProUGUI nameMesh;
		public EnumDropdown target, trigger;
		public FloatField chance;
		public ActionWindow actionWindow;
		public ProcTriggerNameDictionary dictionary;
		private TriggeredActionBuilder triggeredActionBuilder;

		public void Awake()
		{
			target.onChanged += HandleProc;
			trigger.onChanged += HandleProc;
			chance.onChanged += HandleProc;

			target.SetOptions(new ProcTarget());
			trigger.SetOptions(new ProcTrigger());
		}

		public void Set(TriggeredActionBuilder newBuilder)
		{
			if(newBuilder == null) return;

			triggeredActionBuilder = newBuilder;


			nameMesh.SetText(triggeredActionBuilder.name);
			trigger.Set((int)triggeredActionBuilder.procTrigger);
			target.Set((int)triggeredActionBuilder.procTarget);
			chance.Set(triggeredActionBuilder.procChance);
			actionWindow.Set(triggeredActionBuilder);
			Show();
		}

		public void Clear()
		{
			actionWindow.Clear();
			Hide();
		}

		private void HandleProc(float f)
		{
			HandleProc();
		}

		private void HandleProc(int index)
		{
			HandleProc();
		}

		private void HandleProc()
		{
			triggeredActionBuilder.EditCondition((ProcTrigger)trigger.GetIndex(),  (ProcTarget)target.GetIndex(), chance.GetValue());
		}

		public void Show()
		{
			toggler.Show();
		}

		public void Hide()
		{
			toggler.Hide();
		}
	}
}