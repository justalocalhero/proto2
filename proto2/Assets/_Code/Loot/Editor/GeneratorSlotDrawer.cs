﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(GeneratorSlot))]
public class GeneratorSlotDrawer : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int slotSize = 80;
        int frequencySize = 50;
        int spriteSize = 120;
        int spacing = 5;
        int slotIndent = 0;
        int frequencyIndent = slotIndent + slotSize + spacing;
        int spriteIndent = frequencyIndent + frequencySize + spacing;

        Rect slotRect = new Rect(position.x + slotIndent, position.y, slotSize, position.height);
        Rect frequencyRect = new Rect(position.x + frequencyIndent, position.y, frequencySize, position.height);
        Rect spriteRect = new Rect(position.x + spriteIndent, position.y, spriteSize, position.height);

        EditorGUI.PropertyField(slotRect, property.FindPropertyRelative("slot"), GUIContent.none);
        EditorGUI.PropertyField(frequencyRect, property.FindPropertyRelative("frequency"), GUIContent.none);
        EditorGUI.ObjectField(spriteRect, property.FindPropertyRelative("sprite"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}