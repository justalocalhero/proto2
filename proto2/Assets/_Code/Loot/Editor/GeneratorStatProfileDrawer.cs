﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(GeneratorStatProfile))]
public class GeneratorStatProfileDrawer : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int statSize = 160;
        int amountSize = 50;
        int spacing = 5;
        int statIndent = 0;
        int amountIndent = statSize + statIndent + spacing;

        Rect statRect = new Rect(position.x + statIndent, position.y, statSize, position.height);
        Rect amountRect = new Rect(position.x + amountIndent, position.y, amountSize, position.height);

        EditorGUI.PropertyField(statRect, property.FindPropertyRelative("statProfile"), GUIContent.none);
        EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("frequency"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}