﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(Loot.SlotChance))]
public class SlotChanceDrawer : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int categorySize = 80;
        int chanceSize = 50;
        int spacing = 5;
        int categoryIndent = 0;
        int frequencyIndent = categoryIndent + categorySize + spacing;

        Rect categoryRect = new Rect(position.x + categoryIndent, position.y, categorySize, position.height);
        Rect chanceRect = new Rect(position.x + frequencyIndent, position.y, chanceSize, position.height);

        EditorGUI.PropertyField(categoryRect, property.FindPropertyRelative("category"), GUIContent.none);
        EditorGUI.PropertyField(chanceRect, property.FindPropertyRelative("chance"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}