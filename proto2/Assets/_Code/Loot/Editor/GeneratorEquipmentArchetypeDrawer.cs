﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(GeneratorEquipmentArchetype))]
public class GeneratorEquipmentArchetypeDrawer : PropertyDrawer 
{

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int archetypeSize = 150;
        int frequencySize = 50;
        int spacing = 5;
        int archetypeIndent = 0;
        int frequencyIndent = archetypeIndent + archetypeSize + spacing;

        Rect archetypeRect = new Rect(position.x + archetypeIndent, position.y, archetypeSize, position.height);
        Rect frequencyRect = new Rect(position.x + frequencyIndent, position.y, frequencySize, position.height);

        EditorGUI.PropertyField(archetypeRect, property.FindPropertyRelative("equipmentArchetype"), GUIContent.none);
        EditorGUI.PropertyField(frequencyRect, property.FindPropertyRelative("frequency"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
