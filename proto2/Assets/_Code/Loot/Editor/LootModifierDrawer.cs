﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(LootModifier))]
public class LootModifierDrawer : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int modifierTypeSize = 50;	
        int statTypeSize = 120;	
		int lootTypeSize = 80;
		int tierSize = 50;
        int spacing = 5;
        int modifierTypeIndent = 0;	
        int statTypeIndent = modifierTypeIndent + modifierTypeSize + spacing;	
		int lootTypeIndent = statTypeIndent + statTypeSize + spacing;	
		int tierIndent = lootTypeIndent + lootTypeSize + spacing;	
		
        Rect modifierTypeRect = new Rect(position.x + modifierTypeIndent, position.y, modifierTypeSize, position.height);
		Rect statTypeRect = new Rect(position.x + statTypeIndent, position.y, statTypeSize, position.height);
		Rect lootTypeRect = new Rect(position.x + lootTypeIndent, position.y, lootTypeSize, position.height);
        Rect tierRect = new Rect(position.x + tierIndent, position.y, tierSize, position.height);

        EditorGUI.PropertyField(modifierTypeRect, property.FindPropertyRelative("modifierType"), GUIContent.none);
        EditorGUI.PropertyField(statTypeRect, property.FindPropertyRelative("statType"), GUIContent.none);
		EditorGUI.PropertyField(lootTypeRect, property.FindPropertyRelative("lootType"), GUIContent.none);
		EditorGUI.PropertyField(tierRect, property.FindPropertyRelative("tier"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}