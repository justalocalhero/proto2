﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(ModEQ))]
public class ModEQDrawer : PropertyDrawer 
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int typeSize = 80;
        int amountSize = 50;
        int spacing = 5;
        int typeIndent =  0;
        int amountIndent = typeIndent + typeSize + spacing;

        Rect typeRect = new Rect(position.x + typeIndent, position.y, typeSize, position.height);
        Rect amountRect = new Rect(position.x + amountIndent, position.y, amountSize, position.height);

        EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("type"), GUIContent.none);
        EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("value"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

}
