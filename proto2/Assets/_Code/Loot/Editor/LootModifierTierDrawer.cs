﻿// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using UnityEditor;

// [CustomPropertyDrawer(typeof(LootModifierTier))]
// public class LootModifierTierDrawer : PropertyDrawer
// {

//     public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
//     {
//         EditorGUI.BeginProperty(position, label, property);

//         var indent = EditorGUI.indentLevel;
//         EditorGUI.indentLevel = 0;

//         int modifierSize = 80;        
//         int tierLabelSize = 50;
//         int tierSize = 50;
//         int frequencyLabelSize = 80;
//         int frequencySize = 50;
//         int spacing = 5;
//         int modifierIndent = 0;	
//         int tierLabelIndent = modifierIndent + modifierSize + spacing;
//         int tierIndent = tierLabelIndent + tierLabelSize + spacing;
//         int frequencyLabelIndent = tierIndent + tierSize + spacing;
//         int frequencyIndent = frequencyLabelIndent + frequencyLabelSize + spacing;
		
//         Rect modifierRect = new Rect(position.x + modifierIndent, position.y, modifierSize, position.height);
// 		Rect tierLabelRect = new Rect(position.x + tierLabelIndent, position.y, tierLabelSize, position.height);        
// 		Rect tierRect = new Rect(position.x + tierIndent, position.y, tierSize, position.height);
//         Rect frequencyLabelRect = new Rect(position.x + frequencyLabelIndent, position.y, frequencyLabelSize, position.height);
//         Rect frequencyRect = new Rect(position.x + frequencyIndent, position.y, frequencySize, position.height);

//         EditorGUI.PropertyField(modifierRect, property.FindPropertyRelative("modifier"), GUIContent.none);
//         EditorGUI.LabelField(tierLabelRect, "Tier");
//         EditorGUI.PropertyField(tierRect, property.FindPropertyRelative("tier"), GUIContent.none);
//         EditorGUI.LabelField(frequencyLabelRect, "Frequency");
//         EditorGUI.PropertyField(frequencyRect, property.FindPropertyRelative("frequency"), GUIContent.none);

//         EditorGUI.indentLevel = indent;

//         EditorGUI.EndProperty();
//     }
// }