﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(GeneratorStat))]
public class GeneratorStatDrawer : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int multSize = 80;
        int typeSize = 80;
        int amountSize = 50;
        int spacing = 5;
        int multIndent = 0;
        int typeIndent = multIndent + multSize + spacing;
        int amountIndent = typeIndent + typeSize + spacing;

        Rect multRect = new Rect(position.x + multIndent, position.y, multSize, position.height);
        Rect typeRect = new Rect(position.x + typeIndent, position.y, typeSize, position.height);
        Rect amountRect = new Rect(position.x + amountIndent, position.y, amountSize, position.height);

        EditorGUI.PropertyField(multRect, property.FindPropertyRelative("modifier"), GUIContent.none);
        EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("stat"), GUIContent.none);
        EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("frequency"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}