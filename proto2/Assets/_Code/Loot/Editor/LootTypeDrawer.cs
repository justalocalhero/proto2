﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(LootType))]
public class LootTypeDrawer : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int typeSize = 120;	
        int archetypeSize = 120;	
        int spacing = 5;
        int typeIndent = 0;	
        int archetypeIndent = typeIndent + typeSize + spacing;
		
        Rect typeRect = new Rect(position.x + typeIndent, position.y, typeSize, position.height);
		Rect archetypeRect = new Rect(position.x + archetypeIndent, position.y, archetypeSize, position.height);

        EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("type"), GUIContent.none);
        EditorGUI.PropertyField(archetypeRect, property.FindPropertyRelative("archetype"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}