﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName="Loot/LootDictionary")]
public class LootDictionary : ScriptableObject
{
    public List<LootType> lootTypes;

    public ArchetypeProfile Generate(EquipmentType type)
    {
        for(int i = 0; i < lootTypes.Count; i++)
        {
            if(lootTypes[i].type == type) return lootTypes[i].archetype;
        }

        Debug.LogWarning("Could not find " + type + " in LootDictionary.");
        return null;
    }
}

public enum EquipmentType {cloth, leather, fur, plate, darkPlate, jewelry, physWeapon, magicWeapon, shield, physOffhand, magicOffhand,}

[Serializable]
public struct LootType{public EquipmentType type; public ArchetypeProfile archetype;}