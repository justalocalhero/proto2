﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Generators/EquipmentArchetype")]
public class EquipmentArchetype : ScriptableObject 
{
	new public string name;
	public SpriteManager spriteManager;
	public Rarity rarity;
	public List<EquipStat> staticStats;
	public List<EquipStat> scalingStats;
	public List<AbilityBuilder> abilities;
	public List<TriggeredActionBuilder> triggeredActions;
	public Sprite sprite;
	public int spriteIndex = -1;
	public EquipSlot slot;
	public RecipeCost recipeCost;
	
	public void OnEnable()
	{
		ValidateSprite();
	}

	public void SetSpriteIndex()
	{
		if(sprite == null) return;
		if(spriteManager == null) return;
		spriteIndex = spriteManager.GetIndex(sprite);
		if(spriteIndex == -1)
			spriteIndex = spriteManager.AddSprite(sprite);
		if(spriteIndex == -1)
			Debug.LogWarning("Panic");
	}

	public void OnValidate()
	{
		ValidateSprite();
	}

	public void ValidateSprite()
	{
		if(sprite == null) return;
		if(spriteManager == null) return;

		if(sprite == spriteManager.GetSprite(spriteIndex)) 
			return;
		else 
			SetSpriteIndex();
	}

	public EquipStat[] GetStaticStats()
	{
		return staticStats.ToArray();
	}

	public EquipStat[] GetScalingStats()
	{
		return scalingStats.ToArray();
	}

	public EquipStat[] GetScalingStats(int level)
	{
		EquipStat[] toReturn = new EquipStat[scalingStats.Count];
		EquipStat currentStat;
		for(int i = 0; i < toReturn.Length; i++)
		{
			currentStat = scalingStats[i];

			toReturn[i] = new EquipStat()
			{
				StatType = currentStat.StatType,
				ModifierType = currentStat.ModifierType,
				Value = currentStat.Value * level
			};
		}

		return toReturn;
	}

	// public EquipStat[] GetStats(int level)
	// {

	// }

	public List<Ability> GetAbilities()
	{
		List<Ability> toReturn = new List<Ability>();

		if(abilities == null) return toReturn;

		foreach(AbilityBuilder builder in abilities)
		{
			toReturn.Add(builder.Generate());
		}

		return toReturn;
	}

	public List<TriggeredAction> GetTriggeredActions()
	{
		List<TriggeredAction> toReturn = new List<TriggeredAction>();

		if(triggeredActions == null) return toReturn;	

		foreach(TriggeredActionBuilder builder in triggeredActions)
		{
			toReturn.Add(builder.Generate());
		}

		return toReturn;
	}

	public EquipSlot GetSlot()
	{
		return slot;
	}

	public Sprite GetSprite()
	{
		return sprite;
	}

	public string GetName()
	{
		return name;
	}

	public Rarity GetRarity()
	{
		return rarity;
	}

	// public Equipment Generate(int level)
	// {
	// 	return new Equipment(GetName(), level, GetSlot(), GetStats(), GetAbilities(), GetTriggeredActions(), GetSprite());
	// }
}
