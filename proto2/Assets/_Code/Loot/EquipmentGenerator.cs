﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Loot;

[CreateAssetMenu(menuName = "Generators/EquipmentGenerator")]
public class EquipmentGenerator : ScriptableObject, IEquipmentGenerator
{
    //public List<LootModifierTier> lootModifierTiers;
    public LootModifierDictionary lootModifierDictionary;
    //public GeneratorStatProfile[] statProfiles;
    public ArchetypeProfile archetypeProfile;
    public StatEquivalencies statEQ;

    public List<GeneratorStat> generatorStats;
    private NamedEquipmentBuilder namedEquipmentBuilder;
    public ItemValueCalculator itemValueCalculater;

    public void OnEnable()
    {
        //if(lootModifierDictionary != null) PrepStats();
    }

    public Equipment Generate(EquipmentArchetype archetype)
    {
        if(namedEquipmentBuilder == null) namedEquipmentBuilder = new NamedEquipmentBuilder();
        
        Equipment toReturn = namedEquipmentBuilder.Generate(archetype);
        toReturn.level = itemValueCalculater.GetValue(toReturn);

        return toReturn;
    }

    // public void PrepStats()
    // {
    //     generatorStats = new List<GeneratorStat>();

    //     for(int i = 0; i < lootModifierTiers.Count; i++)
    //     {
    //         List<GeneratorStat> currentStats = lootModifierDictionary.Generate(lootModifierTiers[i].modifier, lootModifierTiers[i].tier, lootModifierTiers[i].frequency);
            
    //         generatorStats.AddRange(currentStats);
    //     }
    // }

    public Equipment Generate()
    {
        EquipmentArchetype archetype = archetypeProfile.GetNext();
        
        return Generate(archetype);
    }

    public Equipment[] GenerateAll()
    {
        EquipmentArchetype[] archetypes = archetypeProfile.GetAll();
        Equipment[] equipments = new Equipment[archetypes.Length];

        for(int i = 0; i < archetypes.Length; i++)
        {
            equipments[i] = Generate(archetypes[i]);
        }

        return equipments;
    }

    public List<EquipStat> GetArchetypeStats(EquipmentArchetype archetype, int level)
    {
        EquipStat[] archetypeStaticStats = archetype.GetStaticStats();
        EquipStat[] archetypeScalingStats = archetype.GetScalingStats(level);
        List<EquipStat> statsList = new List<EquipStat>();

        for(int i = 0; i < archetypeStaticStats.Length; i++)
        {
            statsList.Add(archetypeStaticStats[i]);
        }

        for(int i = 0; i < archetypeScalingStats.Length; i++)
        {
            statsList.Add(archetypeScalingStats[i]);
        }

        return statsList;
    }

    // public List<EquipStat> GetProfileStats(int level, int modCount)
    // {
    //     List<EquipStat> statsList = new List<EquipStat>();
    //     GeneratorStat nextProfileStat;
    //     for(int i = 0; i < modCount; i++)
    //     {
    //         nextProfileStat = GenerateStatProfile().GetNextStat();
    //         statsList.Add(StatFromGenerator(nextProfileStat, level));
    //     }

    //     return statsList;
    // }

    public List<EquipStat> GetProfileStats(int level,  int modCount)
    {
        List<EquipStat> toReturn = new List<EquipStat>();

        if(generatorStats.Count > 0)
        {
            for(int i = 0; i < modCount; i++)
            {
                toReturn.Add(GetStat(level));
            }
        }



        return toReturn;
    }

    public EquipStat GetStat(int level)
    {
        float totalFrequency = 0;

        for(int i = 0; i < generatorStats.Count; i++)
        {
            totalFrequency += generatorStats[i].frequency;
        }

        float r = UnityEngine.Random.Range(0, totalFrequency);

        for(int i = 0; i < generatorStats.Count; i++)
        {
            if(r < generatorStats[i].frequency)
                return StatFromGenerator(generatorStats[i], level);
            else
                r -= generatorStats[i].frequency;
        }

        return new EquipStat{};
    }
    
    public EquipStat StatFromGenerator(GeneratorStat stat, int level)
    {
        return new EquipStat
        {
            ModifierType = stat.modifier,
            StatType = stat.stat,
            Value = (1 + level * .2f) * statEQ.GetFactor(stat.stat, stat.modifier)
        };
    }

    // public StatProfile GenerateStatProfile()
    // {
    //     float totalFrequency = 0;

    //     for(int i = 0; i < statProfiles.Length; i++)
    //     {
    //         totalFrequency += statProfiles[i].frequency;
    //     }

    //     float r = UnityEngine.Random.Range(0, totalFrequency);

    //     for(int i = 0; i < statProfiles.Length; i++)
    //     {
    //         if(r < statProfiles[i].frequency)
    //             return statProfiles[i].statProfile;
    //         else
    //             r -= statProfiles[i].frequency;
    //     }
    //     Debug.LogWarning("Failed to Find GeneratorStat");
    //     return new StatProfile { };
    // }
}

[Serializable]
public struct GeneratorStatProfile { public StatProfile statProfile; public float frequency;}