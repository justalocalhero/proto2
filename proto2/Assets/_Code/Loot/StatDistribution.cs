﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	[CreateAssetMenu(menuName="Loot/LootProfile/StatDistribution")]
	public class StatDistribution : ScriptableObject 
	{		
        public List<StatChance> slots;

		public StatCategories Get()
		{
			float total = 0;

			foreach(StatChance current in slots)
			{
				total += current.chance;
			}

			float r = UnityEngine.Random.Range(0, total);

			foreach(StatChance current in slots)
			{
				if(r < current.chance) return current.category;
				else r -= current.chance;
			}

			Debug.LogWarning("Stat Category Not Found.");
			return StatCategories.none;
		}
	}	
	[System.Serializable]
    public struct StatChance { public StatCategories category; public float chance; }
}