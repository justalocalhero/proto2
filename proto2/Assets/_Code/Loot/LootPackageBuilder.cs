﻿// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using System.IO;
// using UnityEditor;
// using System;

// #if(UNITY_EDITOR)
// [CreateAssetMenu(menuName="Loot/LootPackageBuilder")]
// public class LootPackageBuilder : PackageBuilder 
// {
// 	public LootDictionary lootDictionary;
// 	public LootModifierDictionary lootModifierDictionary;
// 	public StatEquivalencies statEquivalencies;
// 	public List<Derp> derps;
// 	public EnemyGenerator targetGenerator;
	
// 	public override void BuildPackage()
// 	{
		
// 		string thisPath = AssetDatabase.GetAssetPath(this);
// 		string folderPath = thisPath.Substring(0,thisPath.LastIndexOf('/'));
// 		string lootPath = folderPath + "/Loot";
// 		string parentPath = lootPath + "/Ordinary";
// 		string archetypePath = parentPath + "/Archetypes";

// 		if(!Directory.Exists(lootPath))
// 		{
// 			var folder = Directory.CreateDirectory(lootPath);
// 		}

// 		if(!Directory.Exists(parentPath))
// 		{
// 			var folder = Directory.CreateDirectory(parentPath);
// 		}

// 		if(!Directory.Exists(archetypePath))
// 		{
// 			var folder = Directory.CreateDirectory(archetypePath);
// 		}
		
// 		//Create stat profiles
// 		//Create archetype profile
// 		List<LootGenerator> lootGenerators = new List<LootGenerator>();

// 		if(targetGenerator != null)
// 		{
// 			foreach(LootGenerator tempGenerator in targetGenerator.lootGenerators)
// 			{
// 				if(tempGenerator.equipmentGenerator != null) lootGenerators.Add(new LootGenerator{
// 					equipmentGenerator = tempGenerator.equipmentGenerator,
// 					frequency = tempGenerator.frequency,
// 				});
// 			}
// 		}

// 		foreach(Derp derp in derps)
// 		{
// 			string generatorPath = parentPath + "/" + Utility.ToCamelCase("" + derp.type) + ".asset";
// 			string currentArchetypePath = archetypePath + "/" + Utility.ToCamelCase("" + derp.type) + ".asset";

// 			ArchetypeProfile toClone = lootDictionary.Generate(derp.type);
// 			ArchetypeProfile archetypeProfile = ScriptableObject.Instantiate(toClone) as ArchetypeProfile;

// 			AssetDatabase.CreateAsset(archetypeProfile, currentArchetypePath);

// 			EditorUtility.SetDirty(archetypeProfile);

// 			EquipmentGenerator currentGenerator = ScriptableObject.CreateInstance<EquipmentGenerator>();

// 			currentGenerator.lootModifierDictionary = lootModifierDictionary;
// 			currentGenerator.archetypeProfile = archetypeProfile;
// 			currentGenerator.statEQ = statEquivalencies;
// 			currentGenerator.lootModifierTiers = new List<LootModifierTier>();
			
// 			foreach(LootModifierTier tempTier in derp.mods)
// 			{
// 				currentGenerator.lootModifierTiers.Add(tempTier);
// 			}

			
// 			lootGenerators.Add(new LootGenerator{
// 				equipmentGenerator = currentGenerator,
// 				frequency = derp.frequency,
// 			});

// 			AssetDatabase.CreateAsset(currentGenerator, generatorPath);

// 			EditorUtility.SetDirty(currentGenerator);

// 		}

// 		if(targetGenerator != null)
// 		{
// 			targetGenerator.lootGenerators = lootGenerators.ToArray();
// 		}

// 		AssetDatabase.SaveAssets();
// 		AssetDatabase.Refresh();
// 		EditorUtility.FocusProjectWindow();

// 		targetGenerator = null;
// 	}
// }

// #endif

// [Serializable]
// public struct LootModifierTier {public LootModifierType modifier; public int tier; public float frequency;}

// [Serializable]
// public struct Derp {public EquipmentType type; public float frequency; public List<LootModifierTier> mods; }