﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Generators/StatEQ")]
public class StatEquivalencies : ScriptableObject 
{
	public StatEQ[] stats = new StatEQ[Enum.GetNames(typeof(StatType)).Length];
	public ModEQ[] mods = new ModEQ[Enum.GetNames(typeof(ModifierType)).Length];

	public float GetFactor(EquipStat stat)
	{
		return GetFactor(stat.StatType, stat.ModifierType);
	}

	public float GetFactor(StatType stat, ModifierType mod) 
	{
		if(stats.Length < (int)stat)
		{
			Debug.LogWarning("Equivalency does not contain stat: " + stat);
			return -1;
		}
		if(mods.Length < (int)mod)
		{
			Debug.LogWarning("Equivalency doe not contain mod: " + mod);
			return -1;
		}
		float total = 1;
		if(mod == ModifierType.Flat) total = stats[(int)stat].value;
		else total = mods[(int)mod].value;
		return total;
	}

	public float GetInverseFactor(EquipStat stat)
	{
		return GetInverseFactor(stat.StatType, stat.ModifierType);
	}

	public float GetInverseFactor(StatType type, ModifierType modifier)
	{	
		float factor = GetFactor(type, modifier);
		float inverseFactor = (factor == 0) ? 0 : 1 / factor;

		return inverseFactor;
	}

	public EquipStat FactorStat(EquipStat stat)
	{
		float factor = GetFactor(stat.StatType, stat.ModifierType);

		return new EquipStat
		{
			StatType = stat.StatType,
			ModifierType = stat.ModifierType,
			Value = factor * stat.Value
		};
	}

	public EquipStat DefactorStat(EquipStat stat)
	{
		float factor = GetFactor(stat.StatType, stat.ModifierType);
		float inverseFactor = (factor == 0) ? 0 : 1 / factor;

		return new EquipStat
		{
			StatType = stat.StatType,
			ModifierType = stat.ModifierType,
			Value = inverseFactor * stat.Value
		};
	}
}

[Serializable]
public struct StatEQ { public StatType type; public float value; };

[Serializable]
public struct ModEQ { public ModifierType type; public float value; };
