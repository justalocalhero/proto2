﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
    [CreateAssetMenu(menuName="Loot/LootProfile/LootProfile")]
    [System.Serializable]
    public class LootProfile : ScriptableObject
    {	
        public EquipDistribution equipDistribution;

        [Space(20)]
        public SlotDistribution armorSlots;
        public SlotDistribution weaponSlots, jewelrySlots, shieldSlots;

        [Space(20)]
        public StatDistribution statDistribution;

        [Space(20)]
        public DamageDistribution physicalDamage;
        public DamageDistribution magicalDamage;

        public EquipCategories GetEquipCategory()
        {
            return equipDistribution.Get();
        }

        public EquipSlot GetSlotCategory(EquipCategories e)
        {
            SlotDistribution distribution = null;

            switch(e)
            {
                case EquipCategories.cloth:
                    distribution = armorSlots;
                    break;                
                case EquipCategories.leather:
                    distribution = armorSlots;
                    break;                
                case EquipCategories.plate:
                    distribution = armorSlots;
                    break;                
                case EquipCategories.weapon:
                    distribution = weaponSlots;
                    break;                
                case EquipCategories.shield:
                    distribution = shieldSlots;
                    break;                
                case EquipCategories.jewelry:
                    distribution = jewelrySlots;
                    break;
                default:
                    break; 
            }

            if(distribution != null) return distribution.Get();
            else return EquipSlot.Mainhand;
        }

        public StatCategories GetStatCategory()
        {
            return statDistribution.Get();
        }

        public DamageCategories GetDamageCategory(StatCategories e)
        {
            DamageDistribution distribution = null;

            switch(e)
            {
                case StatCategories.physical:
                    distribution = physicalDamage;
                    break;
                case StatCategories.magical:
                    distribution = magicalDamage;
                    break;
                default:
                    break;
            }

            if(distribution != null) return distribution.Get();
            else return DamageCategories.none;
        }

        
    }
}