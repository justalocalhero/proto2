﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	[CreateAssetMenu(menuName="Loot/LootManager")]
	public class LootManager : ScriptableObject 
	{
		public LootComponentDictionary dict;
		public GenericEquipmentGenerator genericEquipmentGenerator = new GenericEquipmentGenerator();
		public EquipmentBuilder equipmentBuilder = new EquipmentBuilder();
		public EquipSlotMultiplierMap slotMap;

		public Equipment Generate(LootRollManager rolls, LootProfile profile)
		{
			EquipCategories equip = profile.GetEquipCategory();
			StatCategories stat = profile.GetStatCategory();			
			EquipSlot slot = profile.GetSlotCategory(equip);
			DamageCategories damage = profile.GetDamageCategory(stat);

			List<Tag> tags = Tag.GetTags(stat, equip, GetSlotCategory(slot), damage);

			Equipment equipment = equipmentBuilder.Begin()
				.WithTags(tags)
				.WithSlot(slot, slotMap.GetMultiplier(slot))
				.WithRarity(Rarity.ordinary)
				.WithDesign(GetDesign(tags, rolls, dict))
				.WithQuality(GetQuality(tags, rolls, dict))
				.WithDivinity(GetDivinity(tags, rolls, dict))
				.WithEnchantment(GetEnchantment(tags, rolls, dict))
				.WithMaterials(GetMaterial(tags, rolls, dict))
				.Build();

			return equipment;
		}	

		public Design GetDesign(List<Tag> tags, LootRollManager rolls, LootComponentDictionary dict)
		{
			return dict.GenerateDesign(tags, rolls.DesignDifficulty());
		}

		public Quality GetQuality(List<Tag> tags, LootRollManager rolls, LootComponentDictionary dict)
		{
			return dict.GenerateQuality(tags, 0, rolls.QualityLevel());
		}

		public Divinity GetDivinity(List<Tag> tags, LootRollManager rolls, LootComponentDictionary dict)
		{
			return dict.GenerateDivinity(tags, 0, rolls.DivinityLevel());
		}

		public Enchantment GetEnchantment(List<Tag> tags, LootRollManager rolls, LootComponentDictionary dict)
		{
			return dict.GenerateEnchantment(tags, rolls.EnchantmentDifficulty(), rolls.EnchantmentLevel());
		}

		public Material GetMaterial(List<Tag> tags, LootRollManager rolls, LootComponentDictionary dict)
		{
			return dict.GenerateMaterial(tags, rolls.MaterialDifficulty());
		}		

		public SlotCategories GetSlotCategory(EquipSlot slot)
		{
			int index = (int) slot;
			return (SlotCategories) (index + 1);
		}
	}
}