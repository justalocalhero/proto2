﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	[CreateAssetMenu(menuName="Loot/LootProfile/DamageDistribution")]
	public class DamageDistribution : ScriptableObject 
	{		
        public List<DamageChance> damages;

		public DamageCategories Get()
		{
			float total = 0;

			foreach(DamageChance current in damages)
			{
				total += current.chance;
			}

			float r = UnityEngine.Random.Range(0, total);

			foreach(DamageChance current in damages)
			{
				if(r < current.chance) return current.category;
				else r -= current.chance;
			}

			Debug.LogWarning("Damage Category Not Found.");
			return DamageCategories.none;
		}
	}	

	[System.Serializable]
    public struct DamageChance { public DamageCategories category; public float chance; }
}