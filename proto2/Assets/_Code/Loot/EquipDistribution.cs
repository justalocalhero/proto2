﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Loot
{
	[CreateAssetMenu(menuName="Loot/LootProfile/EquipDistribution")]
	public class EquipDistribution : ScriptableObject 
	{		
        public List<EquipChance> equips;
		
		public EquipCategories Get()
		{
			float total = 0;

			foreach(EquipChance current in equips)
			{
				total += current.chance;
			}

			float r = UnityEngine.Random.Range(0, total);

			foreach(EquipChance current in equips)
			{
				if(r < current.chance) return current.category;
				else r -= current.chance;
			}

			Debug.LogWarning("Equip Category Not Found.");
			return EquipCategories.none;
		}
	}	
	[System.Serializable]
    public struct EquipChance { public EquipCategories category; public float chance; }
}