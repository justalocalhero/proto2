﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName="Loot/LootModifierDictionary")]
public class LootModifierDictionary : ScriptableObject 
{
	public List<LootModifier> lootModifiers;

	public List<GeneratorStat> Generate(LootModifierType type, int tier, float frequencyMultiplier)
	{
		List<GeneratorStat> toReturn = new List<GeneratorStat>();
		LootModifier current;
		float inverseFrequency = (tier > 0) ? 1.0f / (tier) : 1.0f;

		for(int i = 0; i < lootModifiers.Count; i++)
		{
			current = lootModifiers[i];
			if(current.lootType == type && current.tier < tier)
			{
				GeneratorStat temp = new GeneratorStat 
				{
					stat = current.statType,
					modifier = current.modifierType,
					frequency = Utility.Truncate((frequencyMultiplier * (tier - current.tier) * inverseFrequency), 2),
				};

				toReturn.Add(temp);
			}
		}

		return toReturn;
	}

}

[Serializable]
public struct LootModifier {public StatType statType; public ModifierType modifierType; public LootModifierType lootType; public int tier;}
public enum LootModifierType {phys, magic, defense, utility,}
