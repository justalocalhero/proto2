﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Loot
{
	public class AbilityWindow : MonoBehaviour 
	{
		public UIToggler toggler;
		public TextMeshProUGUI nameMesh;
		public FloatField cooldown;
		public ActionWindow actionWindow;
		private AbilityBuilder abilityBuilder;

		public void Awake()
		{
			cooldown.onChanged += HandleCooldown;
		}

		public void Set(AbilityBuilder newBuilder)
		{
			if(newBuilder == null) return;

			this.abilityBuilder = newBuilder;
			
			nameMesh.SetText(abilityBuilder.name);
			cooldown.Set(abilityBuilder.period);
			actionWindow.Set(abilityBuilder);

			Show();
		}

		public void Clear()
		{
			actionWindow.Clear();
			Hide();
		}

		public void HandleCooldown(float newCooldown)
		{
			abilityBuilder.EditDuration(newCooldown);
		}

		public void Show()
		{
			toggler.Show();
		}

		public void Hide()
		{
			toggler.Hide();
		}
	}
}