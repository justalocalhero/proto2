﻿using UnityEngine;
using UnityEngine.UI;

public class BestiaryEntry 
{
	private RaceClass raceClass;
	private bool isRecorded, isPrimary, isNewlyRecorded;
	private int killCount, deathCount;

	public delegate void OnStatsChanged();
	public OnStatsChanged onStatsChanged;

	public delegate void OnRecorded();
	public OnRecorded onRecorded;

	public BestiaryEntry(RaceClass raceClass)
	{
		this.raceClass = raceClass;
		this.isPrimary = false;
		this.isRecorded = false;
		this.killCount = 0;
	}

	public BestiaryEntry(RaceClass raceClass, bool isPrimary)
	{
		this.raceClass = raceClass;
		this.isPrimary = isPrimary;
		this.isRecorded = false;
		this.killCount = 0;
	}

	public BestiaryEntry(RaceClass raceClass, bool isPrimary, bool isRecorded)
	{
		this.raceClass = raceClass;
		this.isPrimary =  isPrimary;
		this.isRecorded = isRecorded;
		this.killCount = 0;
	}

	public BestiaryEntry(RaceClass raceClass, bool isPrimary, bool isRecorded, int killCount)
	{
		this.raceClass = raceClass;
		this.isPrimary =  isPrimary;
		this.isRecorded = isRecorded;
		this.killCount = killCount;
	}

	public void Record()
	{
		if(isRecorded) return;
		isRecorded = true;
		isNewlyRecorded = true;
		RaiseOnRecorded();
	}

	public void View()
	{
		isNewlyRecorded = false;
	}

	public void Killed()
	{
		killCount++;
		RaiseOnStatsChanged();
	}

	public void Died()
	{
		deathCount++;
		RaiseOnStatsChanged();
	}
	
	public bool GetIsRecorded()
	{
		return isRecorded;
	}

	public bool GetIsPrimary()
	{
		return isPrimary;
	}

	public bool GetIsNewlyRecorded()
	{
		return isNewlyRecorded;
	}

	public RaceClass GetRaceClass()
	{
		return raceClass;
	}

	public int GetKillCount()
	{
		return killCount;
	}

	public int GetDeathCount()
	{
		return deathCount;
	}

	public void RaiseOnStatsChanged()
	{
		if(onStatsChanged != null) onStatsChanged();
	}

	public void RaiseOnRecorded()
	{
		if(onRecorded != null) onRecorded();
	}

}
