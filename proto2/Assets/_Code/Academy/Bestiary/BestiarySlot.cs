﻿using UnityEngine;
using UnityEngine.UI;

public class BestiarySlot : MonoBehaviour 
{
	private BestiaryEntry bestiaryEntry;
	public Button button;
	public Image buttonimage;
	public Image recordedIconImage;
	public Image notRecordedIconImage;
	public Image newlyRecordedIconImage;
	public RaceClassText hoverTooltip;
	public RaceClassText clickTooltip;

	public void Add(BestiaryEntry newEntry)
	{
		if(newEntry == null) return;
		newEntry.onRecorded += UpdateRecordedIcons;
		bestiaryEntry = newEntry;
		button.enabled = true;
		buttonimage.enabled = true;
		UpdateRecordedIcons();
	}

	public void Clear()
	{
		if(bestiaryEntry != null) bestiaryEntry.onRecorded -= UpdateRecordedIcons;
		bestiaryEntry = null;
		button.enabled = false;
		buttonimage.enabled = false;
		UpdateRecordedIcons();
	}

	public BestiaryEntry GetBestiaryEntry()
	{
		return bestiaryEntry;
	}

	public void UpdateRecordedIcons()
	{
		if(bestiaryEntry == null)
		{
			recordedIconImage.enabled = false;
			notRecordedIconImage.enabled = false;
			newlyRecordedIconImage.enabled = false;
		}
		else
		{
			newlyRecordedIconImage.enabled = bestiaryEntry.GetIsNewlyRecorded();
			recordedIconImage.enabled = bestiaryEntry.GetIsRecorded();
			notRecordedIconImage.enabled = !bestiaryEntry.GetIsRecorded();

			button.interactable = bestiaryEntry.GetIsRecorded();
		}
	}

	public void OnButtonPressed()
	{
		if(bestiaryEntry == null) return;
		bestiaryEntry.View();
		UpdateRecordedIcons();

		if(clickTooltip == null) return;
		if(bestiaryEntry.GetIsRecorded())
			clickTooltip.Set(bestiaryEntry.GetRaceClass());
	}

	public void OnPointerEnter()
	{
		if(hoverTooltip == null) return;
		if(bestiaryEntry == null) return;
		if(bestiaryEntry.GetIsRecorded())
		{
			hoverTooltip.Set(bestiaryEntry.GetRaceClass());
			bestiaryEntry.View();
			UpdateRecordedIcons();
		} 
		else hoverTooltip.Hide();
	}

	public void OnPointerExit()
	{
		if(hoverTooltip == null) return;
		hoverTooltip.Hide();
	}
}
