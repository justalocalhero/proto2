﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BestiaryManager : MonoBehaviour 
{
	public delegate void OnBestiaryChanged();
	public OnBestiaryChanged onBestiaryChanged;
	
	public List<BestiaryEntry> bestiaryEntries = new List<BestiaryEntry>();

	public void Add(BestiaryEntry newEntry)
	{
		Add(newEntry, true);
	}

	public void Add(BestiaryEntry newEntry, bool raiseUpdate)
	{
		if(newEntry == null) return;
		if(bestiaryEntries == null) return;
		if(bestiaryEntries.Contains(newEntry)) return;
		bestiaryEntries.Add(newEntry);
		if(raiseUpdate) RaiseOnBestiaryChanged();
	}

	public void Add(BestiaryEntry[] newEntries)
	{
		if(newEntries == null) return;

		for(int i = 0; i < newEntries.Length; i++)
		{
			Add(newEntries[i], false);
		}
		RaiseOnBestiaryChanged();
	}
	
	public void Record(RaceClass raceClass)
	{
		if(raceClass == null) return;
		if(bestiaryEntries == null) return;

		BestiaryEntry[] matchingEntries = GetMatchingEntries(raceClass);
		for(int i = 0; i < matchingEntries.Length; i++)
		{
			matchingEntries[i].Record();
		}
	}

	public void Record(Unit unit)
	{
		if(unit == null) return;
		if(unit.raceClasses == null) return;

		for(int i = 0; i < unit.raceClasses.Count; i++)
		{
			Record(unit.raceClasses[i]);
		}
	}

	public void Killed(RaceClass raceClass)
	{
		if(raceClass == null) return;
		if(bestiaryEntries == null) return;

		BestiaryEntry[] matchingEntries = GetMatchingEntries(raceClass);
		for(int i = 0; i < matchingEntries.Length; i++)
		{
			matchingEntries[i].Killed();
		}
	}

	public void Killed(Unit unit)
	{
		if(unit == null) return;
		if(unit.raceClasses == null) return;

		for(int i = 0; i < unit.raceClasses.Count; i++)
		{
			Killed(unit.raceClasses[i]);
		}
	}

	public void Died(RaceClass raceClass)
	{
		if(raceClass == null) return;
		if(bestiaryEntries == null) return;

		BestiaryEntry[] matchingEntries = GetMatchingEntries(raceClass);
		for(int i = 0; i < matchingEntries.Length; i++)
		{
			matchingEntries[i].Died();
		}
	}

	public void Died(Unit unit)
	{
		if(unit == null) return;
		if(unit.raceClasses == null) return;

		for(int i = 0; i < unit.raceClasses.Count; i++)
		{
			Died(unit.raceClasses[i]);
		}
	}

	public BestiaryEntry[] GetMatchingEntries(RaceClass raceClass)
	{
		if(raceClass == null) return null;
		if(bestiaryEntries == null) return null;
		List<BestiaryEntry> matchingEntries = new List<BestiaryEntry>();
		BestiaryEntry currentEntry;
		for(int i = 0; i < bestiaryEntries.Count; i++)
		{
			currentEntry = bestiaryEntries[i];
			if(currentEntry != null)
			{
				if(currentEntry.GetRaceClass() == raceClass) matchingEntries.Add(currentEntry);
			}
		}

		return matchingEntries.ToArray();

	}

	public void RaiseOnBestiaryChanged()
	{
		if(onBestiaryChanged != null) onBestiaryChanged();
	}
}