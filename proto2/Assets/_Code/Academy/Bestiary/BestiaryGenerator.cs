﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BestiaryGenerator : MonoBehaviour 
{
	public BestiaryManager bestiaryManager;
	public EncounterGenerator[] encounterGenerators;

	public void Generate()
	{
		if(bestiaryManager == null)  return;
		if(encounterGenerators == null) return;

		for(int i = 0; i < encounterGenerators.Length; i++)
		{
			bestiaryManager.Add(GenerateBestiaryEntriesFromEncounter(encounterGenerators[i]));
		}
	}

	public BestiaryEntry[] GenerateBestiaryEntriesFromEncounter(EncounterGenerator encounter)
	{
		List<BestiaryEntry> primaryEntries = new List<BestiaryEntry>();
		List<BestiaryEntry> secondaryEntries = new List<BestiaryEntry>();

		EncounterEnemyGenerator[] encounterEnemyGenerators = encounter.enemies;
		EnemyGenerator currentGenerator;
		GeneratorRaceClass[] currentPrimaryRaces;
		GeneratorRaceClass[] currentSecondaryRaces;
		RaceClass currentRaceClass;

		for(int i = 0; i < encounterEnemyGenerators.Length; i++)
		{
			currentGenerator = encounterEnemyGenerators[i].enemyGenerator;
			currentPrimaryRaces = currentGenerator.primaryRaceClass;
			for(int j = 0; j < currentPrimaryRaces.Length; j++)
			{
				currentRaceClass = currentPrimaryRaces[j].raceClass;
				primaryEntries.Add(
					new BestiaryEntry(currentRaceClass, true)
				);
			}

			currentSecondaryRaces = currentGenerator.secondaryRaceClass;

			for(int j = 0; j < currentSecondaryRaces.Length; j++)
			{
				currentRaceClass = currentSecondaryRaces[j].raceClass;
				secondaryEntries.Add(
					new BestiaryEntry(currentRaceClass, false)
				);
			}
		}
		
		primaryEntries.AddRange(secondaryEntries);
		return primaryEntries.ToArray();
	}
}
