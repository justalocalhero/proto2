﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BestiaryUI : MonoBehaviour 
{
	public BestiaryManager bestiaryManager;
	public Transform SlotContainer;
	private BestiarySlot[] slots;
	public RaceClassText hoverTooltip;
	public RaceClassText clickTooltip;
	public Button previousButton;
	public Button nextButton;
	private int entriesPerPage = 25;
	private int currentPage = 0;
	// Use this for initialization
	void Start () 
	{
		slots = SlotContainer.GetComponentsInChildren<BestiarySlot>();
		bestiaryManager.onBestiaryChanged += UpdateUI;
		PassReferences();
	}

	public void PassReferences()
	{
		for(int i = 0; i < slots.Length; i++)
		{
			slots[i].hoverTooltip = hoverTooltip;
			slots[i].clickTooltip = clickTooltip;
		}
	}

	public void UpdateUI()
	{
		if(bestiaryManager == null) return;
		if(bestiaryManager.bestiaryEntries == null) return;

		int pageIndex = currentPage * entriesPerPage;
		int entryIndex;

		for(int i = 0; i < slots.Length; i++)
		{
			entryIndex = pageIndex + i;
			if(entryIndex >= bestiaryManager.bestiaryEntries.Count) 
				slots[i].Clear();
			else
				slots[i].Add(bestiaryManager.bestiaryEntries[entryIndex]);
		}
		UpdateButtons();
	}

	public void ResetPage()
	{
		currentPage = 0;
		UpdateUI();
	}

	public void OnPreviousButtonPressed()
	{
		AlterPage(-1);
		clickTooltip.Hide();
	}

	public void OnNextButtonPressed()
	{
		AlterPage(1);
		clickTooltip.Hide();
	}

	public void AlterPage(int value)
	{
		currentPage += value;
		UpdateUI();
	}

	public void UpdateButtons()
	{
		if(bestiaryManager.bestiaryEntries == null) return;

		if(previousButton != null) previousButton.gameObject.SetActive(currentPage > 0);
		if(nextButton != null) nextButton.gameObject.SetActive(bestiaryManager.bestiaryEntries.Count > (1 + currentPage) * entriesPerPage);
	}
}
