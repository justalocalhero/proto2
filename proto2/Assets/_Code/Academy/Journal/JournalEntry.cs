﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JournalEntry
{

	private TriggeredEvent triggeredEvent;
	private bool isRecorded, isNewlyRecorded;
	private int succeedCount, failCount;

	public delegate void OnStatsChanged();
	public OnStatsChanged onStatsChanged;

	public delegate void OnRecorded();
	public OnRecorded onRecorded;

	public JournalEntry(TriggeredEvent triggeredEvent)
	{
		this.triggeredEvent = triggeredEvent;
		this.isRecorded = false;
		this.isNewlyRecorded = false;
		this.succeedCount = 0;
		this.failCount = 0;
	}
	
	public void Record()
	{
		if(isRecorded) return;
		isRecorded = true;
		isNewlyRecorded = true;
		RaiseOnRecorded();
	}

	public void View()
	{
		isNewlyRecorded = false;
	}

	public void Succeed()
	{
		succeedCount++;
		RaiseOnStatsChanged();
	}

	public void Fail()
	{
		failCount++;
		RaiseOnStatsChanged();
	}
	
	public bool GetIsRecorded()
	{
		return isRecorded;
	}

	public bool GetIsNewlyRecorded()
	{
		return isNewlyRecorded;
	}

	public TriggeredEvent GetTriggeredEvent()
	{
		return triggeredEvent;
	}

	public int GetSucceedCount()
	{
		return succeedCount;
	}

	public int GetFailCount()
	{
		return failCount;
	}
	
	public void RaiseOnStatsChanged()
	{
		if(onStatsChanged != null) onStatsChanged();
	}

	public void RaiseOnRecorded()
	{
		if(onRecorded != null) onRecorded();
	}
}
