﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JournalUI : MonoBehaviour 
{
	public JournalManager journalManager;
	public Transform SlotContainer;
	private JournalSlot[] slots;

	// Use this for initialization
	void Start () 
	{
		slots = SlotContainer.GetComponentsInChildren<JournalSlot>();
		journalManager.onJournalChanged += UpdateUI;
		PassReferences();
	}

	public void PassReferences()
	{
		for(int i = 0; i < slots.Length; i++)
		{
		}
	}

	public void UpdateUI()
	{
		if(journalManager == null) return;
		if(journalManager.journalEntries == null) return;


		for(int i = 0; i < slots.Length; i++)
		{
			if(i >= journalManager.journalEntries.Count) 
				slots[i].Clear();
			else
				slots[i].Add(journalManager.journalEntries[i]);
		}
	}
}
