﻿using UnityEngine;
using UnityEngine.UI;

public class JournalSlot : MonoBehaviour 
{
	private JournalEntry journalEntry;
	public Button button;
	public Image buttonimage;
	public Image recordedIconImage;
	public Image notRecordedIconImage;
	public Image newlyRecordedIconImage;

	public void Add(JournalEntry newEntry)
	{
		if(newEntry == null) return;
		newEntry.onRecorded += UpdateRecordedIcons;
		journalEntry = newEntry;
		button.enabled = true;
		buttonimage.enabled = true;
		UpdateRecordedIcons();
	}

	public void Clear()
	{
		if(journalEntry != null) journalEntry.onRecorded -= UpdateRecordedIcons;
		journalEntry = null;
		button.enabled = false;
		buttonimage.enabled = false;
		UpdateRecordedIcons();
	}

	public JournalEntry GetJournalEntry()
	{
		return journalEntry;
	}

	public void UpdateRecordedIcons()
	{
		if(journalEntry == null)
		{
			recordedIconImage.enabled = false;
			notRecordedIconImage.enabled = false;
			newlyRecordedIconImage.enabled = false;
		}
		else
		{
			newlyRecordedIconImage.enabled = journalEntry.GetIsNewlyRecorded();
			recordedIconImage.enabled = journalEntry.GetIsRecorded();
			notRecordedIconImage.enabled = !journalEntry.GetIsRecorded();

			button.interactable = journalEntry.GetIsRecorded();
		}
	}

	public void OnButtonPressed()
	{
		if(journalEntry == null) return;
		journalEntry.View();
		UpdateRecordedIcons();
		
	}

	public void OnPointerEnter()
	{
		if(journalEntry == null) return;
		if(journalEntry.GetIsRecorded())
		{
			journalEntry.View();
			UpdateRecordedIcons();
		} 
	}

	public void OnPointerExit()
	{
		
	}
}
