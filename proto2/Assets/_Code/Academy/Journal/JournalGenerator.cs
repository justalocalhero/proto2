﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JournalGenerator : MonoBehaviour 
{
	public JournalManager journalManager;
	public EncounterGenerator[] encounterGenerators;

	public void Generate()
	{
		if(journalManager == null)  return;
		if(encounterGenerators == null) return;

		for(int i = 0; i < encounterGenerators.Length; i++)
		{
			journalManager.Add(GenerateJournalEntriesFromEncounter(encounterGenerators[i]));
		}
	}

	public JournalEntry[] GenerateJournalEntriesFromEncounter(EncounterGenerator encounter)
	{
		List<JournalEntry> entries = new List<JournalEntry>();

		EncounterTriggerGenerator[] encounterTriggerGenerator = encounter.triggers;
		TriggeredEventGenerator currentGenerator;
		GeneratorTriggeredEvent[] currentEvents;
		TriggeredEvent currentEvent;

		for(int i = 0; i < encounterTriggerGenerator.Length; i++)
		{
			currentGenerator = encounterTriggerGenerator[i].triggeredEventGenerator;
			currentEvents = currentGenerator.events;
			for(int j = 0; j < currentEvents.Length; j++)
			{
				currentEvent = currentEvents[j].triggeredEvent;
				entries.Add(
					new JournalEntry(currentEvent)
				);
			}
		}

		return entries.ToArray();
	}
}
