﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JournalManager : MonoBehaviour 
{

	public delegate void OnJournalChanged();
	public OnJournalChanged onJournalChanged;
	
	public List<JournalEntry> journalEntries = new List<JournalEntry>();

	public void Add(JournalEntry newEntry)
	{
		Add(newEntry, true);
	}

	public void Add(JournalEntry newEntry, bool raiseUpdate)
	{
		if(newEntry == null) return;
		if(journalEntries == null) return;
		if(journalEntries.Contains(newEntry)) return;
		journalEntries.Add(newEntry);
		if(raiseUpdate) RaiseOnJournalChanged();
	}

	public void Add(JournalEntry[] newEntries)
	{
		if(newEntries == null) return;

		for(int i = 0; i < newEntries.Length; i++)
		{
			Add(newEntries[i], false);
		}
		RaiseOnJournalChanged();
	}
	
	public void Record(TriggeredEvent triggeredEvent)
	{
		if(triggeredEvent == null) return;
		if(journalEntries == null) return;

		JournalEntry[] matchingEntries = GetMatchingEntries(triggeredEvent);
		for(int i = 0; i < matchingEntries.Length; i++)
		{
			matchingEntries[i].Record();
		}
	}

	public void Succeeded(TriggeredEvent triggeredEvent)
	{
		if(triggeredEvent == null) return;
		if(journalEntries == null) return;

		JournalEntry[] matchingEntries = GetMatchingEntries(triggeredEvent);
		for(int i = 0; i < matchingEntries.Length; i++)
		{
			matchingEntries[i].Succeed();
		}
	}

	public void Failed(TriggeredEvent triggeredEvent)
	{
		if(triggeredEvent == null) return;
		if(journalEntries == null) return;

		JournalEntry[] matchingEntries = GetMatchingEntries(triggeredEvent);
		for(int i = 0; i < matchingEntries.Length; i++)
		{
			matchingEntries[i].Fail();
		}
	}

	public JournalEntry[] GetMatchingEntries(TriggeredEvent triggeredEvent)
	{
		if(triggeredEvent == null) return null;
		if(journalEntries == null) return null;
		List<JournalEntry> matchingEntries = new List<JournalEntry>();
		JournalEntry currentEntry;
		for(int i = 0; i < journalEntries.Count; i++)
		{
			currentEntry = journalEntries[i];
			if(currentEntry != null)
			{
				if(currentEntry.GetTriggeredEvent() == triggeredEvent) matchingEntries.Add(currentEntry);
			}
		}

		return matchingEntries.ToArray();

	}

	public void RaiseOnJournalChanged()
	{
		if(onJournalChanged != null) onJournalChanged();
	}
}
