﻿using UnityEngine;
using UnityEngine.UI;

public class FeatSlot : MonoBehaviour 
{
	private Feat feat;
	public Button button;
	public Image buttonImage;
	public Image unavailableImage;
	public Image achievedImage;
	public Image newlyAchievedImage;
	
	public void Add(Feat newFeat)
	{
		if(newFeat == null) return;
		newFeat.onFeatChanged += UpdateAchievedIcons;
		feat = newFeat;
		button.enabled = true;
		buttonImage.enabled = true;
		UpdateAchievedIcons();
	}

	public void Clear()
	{
		if(feat != null) feat.onFeatChanged -= UpdateAchievedIcons;
		feat = null;
		button.enabled = false;
		buttonImage.enabled = false;
		UpdateAchievedIcons();
	}

	public void UpdateAchievedIcons()
	{
		if(feat == null)
		{
			unavailableImage.enabled = false;
			achievedImage.enabled = false;
			newlyAchievedImage.enabled = false;
		}
		else
		{
			unavailableImage.enabled = !feat.GetAvailable();
			achievedImage.enabled = feat.GetAchieved();
			newlyAchievedImage.enabled = feat.GetNewlyAchieved();
		}
	}

	public void OnButtonPressed()
	{
		if(feat == null) return;
		feat.View();
	}

}
