﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeatManager : MonoBehaviour 
{
	public delegate void OnFeatsChanged();
	public OnFeatsChanged onFeatsChanged;

	public List<Feat> feats = new List<Feat>();

	public void Add(Feat newFeat)
	{
		Add(newFeat, true);
	}

	public void Add(Feat newFeat, bool raiseUpdate)
	{
		if(newFeat == null) return;
		if(feats == null) return;
		if(feats.Contains(newFeat)) return;
		feats.Add(newFeat);
		if(raiseUpdate) RaiseOnFeatsChanged();

	}

	public void Add(Feat[] newFeats)
	{
		if(newFeats == null) return;
		for(int i = 0; i < newFeats.Length; i++)
		{
			Add(newFeats[i], false);
		}
		RaiseOnFeatsChanged();
	}

	public void Remove(Feat oldFeat)
	{
		Remove(oldFeat, true);
	}

	public void Remove(Feat oldFeat, bool raiseUpdate)
	{
		if(oldFeat == null) return;
		if(feats == null) return;
		if(!feats.Contains(oldFeat)) return;
		feats.Remove(oldFeat);
		if(raiseUpdate) RaiseOnFeatsChanged();
	}

	public void Remove(Feat[] oldFeats)
	{
		if(oldFeats == null) return;
		for(int i = 0; i < oldFeats.Length; i++)
		{
			Remove(oldFeats[i], false);
		}
		RaiseOnFeatsChanged();
	}

	public void RaiseOnFeatsChanged()
	{
		if(onFeatsChanged != null) onFeatsChanged();
	}
}
