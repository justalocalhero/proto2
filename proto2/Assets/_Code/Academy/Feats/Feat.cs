﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Feat 
{
	private bool available;
	private bool achieveed;
	private bool newlyAchieved;

	public delegate void OnFeatChanged();
	public OnFeatChanged onFeatChanged;

	public Feat()
	{
		available = false;
		achieveed = false;
		newlyAchieved = false;
	}

	public Feat(bool isAvailable)
	{
		available = isAvailable;
		achieveed = false;
		newlyAchieved = false;
	}

	public Feat(bool isAvailable, bool isAchieved)
	{
		available = isAvailable;
		achieveed = isAchieved;
		newlyAchieved = false;
	}

	public Feat(bool isAvailable, bool isAchieved, bool isNewlyAchieved)
	{
		available = isAvailable;
		achieveed = isAchieved;
		newlyAchieved = isNewlyAchieved;
	}

	public void Unlock()
	{
		if(available) return;
		SetAvailable(true);
		RaiseOnFeetChanged();
	}

	public void Achieve()
	{
		if(achieveed) return;
		SetAchieved(true);
		SetNewlyAchieved(true);
		SetAvailable(false);
		RaiseOnFeetChanged();
	}

	public void View()
	{
		if(!newlyAchieved) return;
		SetNewlyAchieved(false);
		RaiseOnFeetChanged();
	}

	public void SetAvailable(bool isAvailable)
	{
		available = isAvailable;
	}

	public void SetAchieved(bool isAchieved)
	{
		achieveed = isAchieved;
	}

	public void SetNewlyAchieved(bool isNewlyAchieved)
	{
		newlyAchieved = isNewlyAchieved;
	}

	public bool GetAvailable()
	{
		return available;
	}

	public bool GetAchieved()
	{
		return achieveed;
	}

	public bool GetNewlyAchieved()
	{
		return newlyAchieved;
	}

	public void RaiseOnFeetChanged()
	{
		if(onFeatChanged != null) onFeatChanged();
	}
}
