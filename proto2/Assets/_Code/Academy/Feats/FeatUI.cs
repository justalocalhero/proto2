﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeatUI : MonoBehaviour 
{
	public Transform slotContainer;
	public FeatManager featManager;
	private FeatSlot[] slots;

	void Start () 
	{
		featManager.onFeatsChanged += UpdateUI;
		slots = slotContainer.GetComponentsInChildren<FeatSlot>();
	}

	public void UpdateUI()
	{
		if(slots == null) return;
		if(featManager == null) return;
		if(featManager.feats == null) return;

		for(int i = 0; i < slots.Length; i++)
		{
			if(i < featManager.feats.Count)
				slots[i].Add(featManager.feats[i]);
			else 
				slots[i].Clear();
		}
	}	
}
