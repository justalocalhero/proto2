﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Loot;

public class RandomEventGenerator : MonoBehaviour 
{
	private float eventCount = System.Enum.GetNames(typeof(EventType)).Length;
	public FloatReference fountainHealing;
	public List<BuffBuilder> shrineBuffs;
	public LootGenerator lootGenerator;
	public LootRollManager lootRollManager;
	public LootProfile lootProfile;

	public EventType Generate(Battleground battleground)
	{
		if(battleground == null) return 0;

		EventType eventType = Utility.RandomFromEnum<EventType>();

		HandleEvent(battleground, eventType);

		return eventType;
	}

	public void HandleEvent(Battleground battleground, EventType eventType)
	{
		switch(eventType)
		{
			case EventType.Shrine:
				Buff toSend = GenerateBuff();
				if(toSend!= null) battleground.BuffPlayer(toSend);
				break;
			case EventType.Fountain:
				battleground.HealPlayer(fountainHealing);
				break;
			case EventType.Treasure:
				battleground.AddLoot(GenerateLoot());
				break;
			default:
				break;
		}
	}

	public Equipment GenerateLoot()
	{
		return lootGenerator.Generate(lootRollManager, lootProfile);
	}

	public Buff GenerateBuff()
	{
		if(shrineBuffs == null || shrineBuffs.Count == 0) return null;

		return Utility.RandomFromList(shrineBuffs).Generate();
	}

	public enum EventType {Shrine, Fountain, Treasure, }
}