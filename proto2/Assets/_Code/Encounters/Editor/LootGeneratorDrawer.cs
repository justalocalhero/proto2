﻿// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using UnityEditor;

// [CustomPropertyDrawer(typeof(LootGenerator))]
// public class LootGeneratorDrawer : PropertyDrawer 
// {

// 	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
// 	{
//         EditorGUI.BeginProperty(position, label, property);

//         var indent = EditorGUI.indentLevel;
//         EditorGUI.indentLevel = 0;

//         int equipmentGeneratorSize = 150;
//         int frequencySize = 50;
//         int spacing = 5;
//         int equipmentGeneratorIndent = 0;
//         int frequencyIndent = equipmentGeneratorIndent + equipmentGeneratorSize + spacing;

//         Rect equipmentGeneratorRect = new Rect(position.x + equipmentGeneratorIndent, position.y, equipmentGeneratorSize, position.height);
//         Rect frequencyRect = new Rect(position.x + frequencyIndent, position.y, frequencySize, position.height);

//         EditorGUI.PropertyField(equipmentGeneratorRect, property.FindPropertyRelative("equipmentGenerator"), GUIContent.none);
//         EditorGUI.PropertyField(frequencyRect, property.FindPropertyRelative("frequency"), GUIContent.none);

//         EditorGUI.indentLevel = indent;

//         EditorGUI.EndProperty();
//     }
// }
