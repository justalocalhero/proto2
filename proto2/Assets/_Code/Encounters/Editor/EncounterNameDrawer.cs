﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(EncounterName))]
public class EncounterNameDrawer : PropertyDrawer 
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int nameSize = 150;
        int levelSize = 50;
        int spacing = 5;
        int nameIndent = 0;
        int levelIndent = nameIndent + nameSize + spacing;

        Rect nameRect = new Rect(position.x + nameIndent, position.y, nameSize, position.height);
        Rect levelRect = new Rect(position.x + levelIndent, position.y, levelSize, position.height);

        EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("name"), GUIContent.none);
        EditorGUI.PropertyField(levelRect, property.FindPropertyRelative("startLevel"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
