﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(GeneratorRaceClass))]
public class GeneratorRaceClassDrawer : PropertyDrawer 
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int raceClassSize = 150;
        int frequencySize = 50;
        int spacing = 5;
        int raceClassIndent = 0;
        int frequencyIndent = raceClassIndent + raceClassSize + spacing;

        Rect raceClassRect = new Rect(position.x + raceClassIndent, position.y, raceClassSize, position.height);
        Rect frequencyRect = new Rect(position.x + frequencyIndent, position.y, frequencySize, position.height);

        EditorGUI.PropertyField(raceClassRect, property.FindPropertyRelative("raceClass"), GUIContent.none);
        EditorGUI.PropertyField(frequencyRect, property.FindPropertyRelative("frequency"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
