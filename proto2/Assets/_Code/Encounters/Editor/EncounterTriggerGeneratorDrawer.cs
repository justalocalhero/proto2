﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(EncounterTriggerGenerator))]
public class EncounterTriggerGeneratorDrawer : PropertyDrawer 
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int triggerSize = 150;
        int frequencySize = 50;
        int spacing = 5;
        int triggerIndent = 0;
        int frequencyIndent = triggerIndent + triggerSize + spacing;

        Rect triggerRect = new Rect(position.x + triggerIndent, position.y, triggerSize, position.height);
        Rect frequencyRect = new Rect(position.x + frequencyIndent, position.y, frequencySize, position.height);

        EditorGUI.PropertyField(triggerRect, property.FindPropertyRelative("triggeredEventGenerator"), GUIContent.none);
        EditorGUI.PropertyField(frequencyRect, property.FindPropertyRelative("frequency"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
