﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(GeneratorTriggeredEvent))]
public class GeneratorTriggeredEventDrawer : PropertyDrawer
{

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int triggeredEventSize = 150;
        int frequencySize = 50;
        int spacing = 5;
        int triggeredEventIndent = 0;
        int frequencyIndent = triggeredEventIndent + triggeredEventSize + spacing;

        Rect triggeredEventRect = new Rect(position.x + triggeredEventIndent, position.y, triggeredEventSize, position.height);
        Rect frequencyRect = new Rect(position.x + frequencyIndent, position.y, frequencySize, position.height);

        EditorGUI.PropertyField(triggeredEventRect, property.FindPropertyRelative("triggeredEvent"), GUIContent.none);
        EditorGUI.PropertyField(frequencyRect, property.FindPropertyRelative("frequency"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
