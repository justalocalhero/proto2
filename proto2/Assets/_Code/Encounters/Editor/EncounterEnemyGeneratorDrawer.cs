﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(EncounterEnemyGenerator))]
public class EncounterEnemyGeneratorDrawer : PropertyDrawer 
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int enemyGeneratorSize = 150;
        int frequencySize = 50;
        int spacing = 5;
        int enemyGeneratorIndent = 0;
        int frequencyIndent = enemyGeneratorIndent + enemyGeneratorSize + spacing;

        Rect enemyGeneratorRect = new Rect(position.x + enemyGeneratorIndent, position.y, enemyGeneratorSize, position.height);
        Rect frequencyRect = new Rect(position.x + frequencyIndent, position.y, frequencySize, position.height);

        EditorGUI.PropertyField(enemyGeneratorRect, property.FindPropertyRelative("enemyGenerator"), GUIContent.none);
        EditorGUI.PropertyField(frequencyRect, property.FindPropertyRelative("frequency"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
