﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Loot;

[CreateAssetMenu(menuName = "Generators/EnemyGenerator")]
public class EnemyGenerator : ScriptableObject
{
	#if UNITY_EDITOR
        [Multiline]
        public string DeveloperDescription = "";
	#endif

	public LootProfile lootProfile;
	public LootGenerator lootGenerator;

	public GeneratorRaceClass[] primaryRaceClass;
	public GeneratorRaceClass[] secondaryRaceClass;
	public int minLevel;
	public int maxLevel;

	public Enemy Generate(int level)
	{
		Enemy enemy = new Enemy("Enemy", level, new UnitResourceManager());
		if(primaryRaceClass != null) 
		{
			if(primaryRaceClass.Length > 0)
			{
				RaceClass toAdd = GenerateFromArray(primaryRaceClass);
				float multiplier = 1 + toAdd.statScalingPerLevel * level;
				enemy.AddRaceClass(toAdd, multiplier);
			}
		}
		if(secondaryRaceClass != null)
		{
			if(secondaryRaceClass.Length > 0)
			{
				RaceClass toAdd = GenerateFromArray(secondaryRaceClass);
				float multiplier = 1 + toAdd.statScalingPerLevel * level;
				enemy.AddRaceClass(toAdd, multiplier);
			}
		}

		enemy.SetLootProfile(lootProfile);
		enemy.SetLootGenerator(lootGenerator);
		
		return enemy;
	}

	public RaceClass GenerateFromArray(GeneratorRaceClass[] array)
	{
		if(array == null)
		{
			Debug.LogWarning("null raceClass array in enemy generator");
			return null;
		}

		float total = 0;
		
		for(int i = 0; i < array.Length; i++)
		{
			total += array[i].frequency;
		}

		float r = UnityEngine.Random.Range(0f, total);
		for(int i = 0; i < array.Length; i++)
		{
			r -= array[i].frequency;
			if(r <= 0) return array[i].raceClass;
		}

		//fail case
		Debug.LogWarning("Could not generate RaceClass from array in EnemyGenerator");
		return null;
	}
}

[Serializable]
public struct GeneratorRaceClass { public RaceClass raceClass; public float frequency; };