﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName="Generators/EncounterGenerator")]
public class EncounterGenerator : ScriptableObject 
{
    public EncounterName[] names;
	public EncounterTriggerGenerator[] triggers;
    public EncounterEnemyGenerator[] enemies;


    public System.Object Generate(int level)
    {
        float total = 0;
        // for(int i = 0; i < triggers.Length; i++)
        // {
        //     if(CheckLevel(triggers[i].triggeredEventGenerator, level))
        //         total += triggers[i].frequency;
        // }

        for(int i = 0; i < enemies.Length; i++)
        {
            if(CheckLevel(enemies[i].enemyGenerator, level))
                total += enemies[i].frequency;
        }

        float r = UnityEngine.Random.Range(0f, total);

        // for(int i = 0; i < triggers.Length; i++)
        // {
        //     if(CheckLevel(triggers[i].triggeredEventGenerator, level))
        //     {
        //         r -= triggers[i].frequency;
        //         if(r <= 0) return triggers[i].triggeredEventGenerator.Generate();
        //     }
        // }

        for(int i = 0; i < enemies.Length; i++)
        {
            if(CheckLevel(enemies[i].enemyGenerator, level))
            {
                r -= enemies[i].frequency;
                if(r <= 0) return enemies[i].enemyGenerator.Generate(level);
            }
        }

        Debug.LogWarning("Unable to generate Encounter");
        return null;
    }

    public bool CheckLevel(TriggeredEventGenerator toCheck, int level)
    {
        return (level >= toCheck.minLevel && level <= toCheck.maxLevel);
    }

    public bool CheckLevel(EnemyGenerator toCheck, int level)
    {
        return (level >= toCheck.minLevel && level <= toCheck.maxLevel);
    }

    public string GetName(int level)
    {
        string currentName ="Void";

        for(int i = 0; i < names.Length; i++)
        {
            if(names[i].startLevel <= level)
                currentName = names[i].name;
            else
                break;
        }


        return currentName;
    }
}



[Serializable]
public struct EncounterTriggerGenerator { public TriggeredEventGenerator triggeredEventGenerator; public float frequency; };

[Serializable]
public struct EncounterEnemyGenerator { public EnemyGenerator enemyGenerator; public float frequency; };

[Serializable]
public struct EncounterName { public string name; public int startLevel; }