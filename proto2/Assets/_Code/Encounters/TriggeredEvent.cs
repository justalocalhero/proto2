﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Loot;

[CreateAssetMenu(menuName ="Generators/TriggeredEvent")]
public class TriggeredEvent : ScriptableObject 
{
	public string encounterName;
	public EncounterType type;
	public float triggerChance;
	public LootRollManager lootRollManager;
	public LootProfile lootProfile;
	public LootGenerator lootGenerator;
	public BuffBuilder buff;
	public DebuffBuilder debuff;
	public Damage[] damage;
	public float effectScaling;
	public float checkScaling;
	public float duration;
	public Sprite icon;

	public bool TriggerCheck(Battleground battleground, Player player, int level)
	{
		bool check = GetCheck(player, level);
		if(check)
		{
			ApplyDamage(player, level);
			ApplyBuff(player, level);
			ApplyDebuff(player, level);
			ApplyLoot(battleground, level);
		}
		return check;
	}

	public bool GetCheck(Player player, int level)
	{
		bool isPositiveEffect = (type == EncounterType.Blessing || type == EncounterType.Treasure);
		float roll = UnityEngine.Random.Range(0, 1.0f);
		int scalingSign = (isPositiveEffect) ? -1 : 1;
		float levelScaling = checkScaling * level;
		float playerScaling = player.GetPerceptionCheck();
		float checkValue = triggerChance + scalingSign * (levelScaling - playerScaling);
		float clampedValue = Mathf.Clamp(checkValue, 0, 1);
		bool check = roll < clampedValue;

		return check;
	}

	public float GetCheckScaling()
	{
		bool isPositiveEffect = (type == EncounterType.Blessing || type == EncounterType.Treasure);
		int scalingSign = (isPositiveEffect) ? -1 : 1;

		return scalingSign * checkScaling;
	}

	public void ApplyBuff(Player player, int level)
	{
		if(buff != null) player.ApplyBuff(ScaleBuff(buff.Generate(), level * effectScaling));
	}

	public void ApplyDebuff(Player player, int level)
	{
		if(debuff != null) player.ApplyDebuff(ScaleDebuff(debuff.Generate(), level * effectScaling));
	}

	public void ApplyDamage(Player player, int level)
	{
		player.ApplyDamage(ScaleDamage(damage, level * effectScaling));
	}

	public void ApplyLoot(Battleground battleground, int level)
	{
		battleground.AddLoot(GetLoot(level));
	}

	public Equipment GetLoot(int level)
	{
		if(lootGenerator == null) return null;
		
		return lootGenerator.Generate(lootRollManager, lootProfile);
	}

	public Buff ScaleBuff(Buff toScale, float factor)
	{
		toScale.periodicDamages = ScaleDamage(toScale.periodicDamages, factor);
		toScale.finalDamages = ScaleDamage(toScale.finalDamages, factor);		
		toScale.stats = ScaleStats(toScale.stats, factor);
		return toScale;
	}

	public Debuff ScaleDebuff(Debuff toScale, float factor)
	{
		toScale.periodicDamages = ScaleDamage(toScale.periodicDamages, factor);
		toScale.finalDamages = ScaleDamage(toScale.finalDamages, factor);		
		toScale.stats = ScaleStats(toScale.stats, factor);
		return toScale;
	}

	public Damage[] ScaleDamage(Damage[] toScale, float factor)
	{
		Damage[] toReturn = new Damage[toScale.Length];
		for(int i = 0; i < toScale.Length; i++)
		{
			toReturn[i] = new Damage()
			{
				type = toScale[i].type,
				value = toScale[i].value * (1 + factor)
			};
		}
		return toReturn;
	}

	public EquipStat[] ScaleStats(EquipStat[] toScale, float factor)
	{
		EquipStat[] toReturn = new EquipStat[toScale.Length];

		for(int i = 0; i < toScale.Length; i++)
		{
			toReturn[i] = new EquipStat()
			{
				ModifierType = toScale[i].ModifierType,
				StatType = toScale[i].StatType,
				Value = toScale[i].Value
			};
			if(toReturn[i].ModifierType == ModifierType.Flat) toReturn[i].Value *= (1 + factor);
		}
		return toReturn;
	}

	public float GetDuration()
	{
		return duration;
	}

	public Sprite GetIcon()
	{
		return icon;
	}

	public TimedEffect GetBuff()
	{
		if(buff == null) return null;
		return buff.Generate();
	}

	public TimedEffect GetDebuff()
	{
		if(debuff == null) return null;
		return debuff.Generate();
	}

	public string GetName()
	{
		return encounterName;
	}

    public bool IsDefault()
    {
        return duration > 0;
    }
}


public enum EncounterType {Blessing, Treasure, Trap, Hazard}