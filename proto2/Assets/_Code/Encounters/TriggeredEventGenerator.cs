﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Generators/TriggeredEventGenerator")]
public class TriggeredEventGenerator : ScriptableObject 
{
	public int minLevel;
	public int maxLevel;
	public GeneratorTriggeredEvent[] events;

	public TriggeredEvent Generate()
	{
		return GenerateFromArray(events);
	}

	public TriggeredEvent GenerateFromArray(GeneratorTriggeredEvent[] array)
	{
		if(array == null)
		{
			Debug.LogWarning("null raceClass array in enemy generator");
			return null;
		}
		
		float total = 0;
		
		for(int i = 0; i < array.Length; i++)
		{
			total += array[i].frequency;
		}

		float r = UnityEngine.Random.Range(0f, total);
		for(int i = 0; i < array.Length; i++)
		{
			r -= array[i].frequency;
			if(r <= 0) return array[i].triggeredEvent;
		}

		//fail case
		Debug.LogWarning("Could not generate RaceClass from array in EnemyGenerator");
		return null;
	}
}
[Serializable]
public struct GeneratorTriggeredEvent{ public TriggeredEvent triggeredEvent; public float frequency; };