﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AbilityAugment  
{
	public ActionCondition[] actionConditions;
    public float period;
	public Buff[] buffs;
    public Debuff[] debuffs;
    public Damage[] damages;
    public Damage[] selfDamages;
    public ConditionalStat[] instanceStats;    
    public ConditionalResource[] conditionalResources;
    public float damageFactor;
    public int purgeCount;
    public int cleanseCount;

    public AbilityAugment(ActionCondition[] actionConditions, float period, Buff[] buffs, Debuff[] debuffs, Damage[] damages, Damage[] selfDamages, ConditionalStat[] instanceStats, ConditionalResource[] conditionalResources, float damageFactor, int purgeCount, int cleanseCount)
    {
        this.actionConditions = actionConditions;
        this.period = period;
        this.buffs = buffs;
        this.debuffs = debuffs;
        this.damages = damages;
        this.selfDamages = selfDamages;
        this.instanceStats = instanceStats;
        this.conditionalResources = conditionalResources;
        this.damageFactor = damageFactor;
        this.purgeCount = purgeCount;
        this.cleanseCount = cleanseCount;
    }

    public string GetString()
    {
        string toReturn = 
        (this.actionConditions.Length + " actionConditions\n" +
        this.buffs.Length + " buffs\n" +
        this.debuffs.Length + " debuffs\n" +
        this.damages.Length + " damages\n" +
        this.selfDamages.Length + " selfDamages\n" +
        this.instanceStats.Length + " instanceStats\n" +
        this.conditionalResources.Length + " conditionalResources\n" +
        this.damageFactor + " damageFactor\n" +
        this.purgeCount + " purgeCount\n" +
        this.cleanseCount + " cleanseCount\n");

        return toReturn;
    }

    public bool CheckCanUse(Unit parent, Unit target)
    {
        return ActionConditionChecker.CheckConditions(actionConditions, parent, target) && ActionConditionChecker.CheckResources(conditionalResources, parent, target);
    }

    public AbilityAugment Clone()
    {
        AbilityAugment toReturn = new AbilityAugment(actionConditions, period, buffs, debuffs, damages, selfDamages, instanceStats, conditionalResources, damageFactor, purgeCount, cleanseCount);
        return toReturn;
    }
}
