﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalentTriggeredAbility : TriggeredAction
{
	private AbilityAugment selectedAugment;

    public TalentTriggeredAbility(string name, ProcTarget procTarget, ProcTrigger procTrigger, float procChance, Action action, int spriteIndex) 
        : base(name, procTarget, procTrigger, procChance, action, spriteIndex)
    {
        
    }

    public void SelectAugment(AbilityAugment talentOption)
    {
        selectedAugment = talentOption;
        action.SelectAugment(talentOption);
    }

    public override TriggeredAction Clone()
    {
        TalentTriggeredAbility toReturn = new TalentTriggeredAbility(name, procTarget, procTrigger, procChance, action.Clone(), spriteIndex);
        
        return toReturn;
    }
}
