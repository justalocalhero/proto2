﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Action 
{
    public string name;
    public int spriteIndex;
    public ProficiencyType proficiencyType;
    public ActionCondition[] actionConditions;
	public Buff[] buffs;
    public Debuff[] debuffs;
    public Damage[] damages;
    public Damage[] selfDamages;
    public ConditionalStat[] instanceStats;    
    public ConditionalResource[] conditionalResources;
    public List<UnitResourceAdder> unitResources = new List<UnitResourceAdder>();
    public float damageFactor;
    public int purgeCount;
    public int cleanseCount;
    public ActionResult result;
    public StatManager statManager;
    public AbilityAugment selectedAugment;
    public bool[] usedTriggers;

    public Action(string name, int spriteIndex, ProficiencyType proficiencyType, ActionCondition[] actionConditions, Buff[] buffs, Debuff[] debuffs, Damage[] damages, Damage[] selfDamages, ConditionalStat[] instanceStats, ConditionalResource[] conditionalResources, float damageFactor, int purgeCount, int cleanseCount)
    {
        this.name = name;
        this.spriteIndex = spriteIndex;
        this.proficiencyType = proficiencyType;
        this.actionConditions = actionConditions;
	    this.buffs = buffs;
        this.debuffs = debuffs;
        this.damages = damages;
        this.selfDamages = selfDamages;
        this.instanceStats = instanceStats;
        this.conditionalResources = conditionalResources;
        this.damageFactor = damageFactor;
        this.purgeCount = purgeCount;
        this.cleanseCount = cleanseCount;
        statManager = new StatManager();
        usedTriggers = new bool[Enum.GetNames(typeof(ProcTrigger)).Length];
    }

	public void Use(Unit parent, Unit target)
	{
        Action toUse = Clone();
        toUse.PrepareStats(parent, target);
        parent.PrepareAction(toUse, target);
	}

    public void SelectAugment(AbilityAugment augment)
    {
        selectedAugment = augment;
    }

    public void PrepareStats(Unit parent, Unit target)
    {
        PrepareAugment(parent, target);
        PrepareParentStats(parent);
        PrepareInstanceStats(parent, target);
        PrepareUnitResources(parent, target);
        
    }

    public void PrepareAugment(Unit parent, Unit target)
    {
        if(selectedAugment == null) return;
        if(!ActionConditionChecker.CheckConditions(selectedAugment.actionConditions, parent, target)) return;
        if(!ActionConditionChecker.CheckResources(selectedAugment.conditionalResources, parent, target)) return;
        buffs = Utility.ExtendArrayByArray(buffs, selectedAugment.buffs);
        debuffs = Utility.ExtendArrayByArray(debuffs, selectedAugment.debuffs);
        damages = Utility.ExtendArrayByArray(damages, selectedAugment.damages);
        selfDamages = Utility.ExtendArrayByArray(selfDamages, selectedAugment.selfDamages);
        instanceStats = Utility.ExtendArrayByArray(instanceStats, selectedAugment.instanceStats);
        conditionalResources = Utility.ExtendArrayByArray(conditionalResources, selectedAugment.conditionalResources);
        damageFactor = damageFactor + selectedAugment.damageFactor;
        purgeCount = purgeCount + selectedAugment.purgeCount;
        cleanseCount = cleanseCount + selectedAugment.cleanseCount;
    }

    public void PrepareParentStats(Unit parent)
    {
        statManager.AdjustStats(parent.GetStats());
    }

    public void PrepareInstanceStats(Unit parent, Unit target)
    {
        for(int i =  0; i < instanceStats.Length; i++)
        {
            if(ActionConditionChecker.CheckCondition(instanceStats[i].condition, parent, target)) 
                statManager.AdjustStats(instanceStats[i].stats);
        }
    }

    public void PrepareUnitResources(Unit parent, Unit target)
    {
        unitResources = new List<UnitResourceAdder>();

        for(int i =  0; i < conditionalResources.Length; i++)
        {
            if(ActionConditionChecker.CheckCondition(conditionalResources[i].actionCondition, parent, target)) 
                unitResources.Add(conditionalResources[i].resource);
        }
    }
    
    public bool CheckCanUse(Unit parent, Unit target)
    {
        return ActionConditionChecker.CheckConditions(actionConditions, parent, target) && ActionConditionChecker.CheckResources(conditionalResources, parent, target);
    }

    public void PassActionResult()
    {
        for(int i = 0; i < damages.Length; i++)
        {
            damages[i].result = result;
        }
    }

    public bool IsHit()
    {
        return result == ActionResult.crit || result == ActionResult.hit;
    }

    public bool IsCrit()
    {
        return result == ActionResult.crit;
    }

    public bool IsDodge()
    {
        return result == ActionResult.dodge;
    }

    public bool IsBlock()
    {
        return result == ActionResult.block;
    }

    public bool IsMiss()
    {
        return result == ActionResult.miss;
    }

    public bool IsReflectable()
    {
        return proficiencyType == ProficiencyType.spell || proficiencyType == ProficiencyType.curse;
    }

    public bool IsBlockable()
    {
        return true;
    }

    public bool IsDodgeable()
    {
        return true;
    }

    public bool IsCrittable()
    {
        return true;
    }

    public bool IsMissable()
    {
        return true;
    }

    public bool IsSupport()
    {
        return (damages.Length == 0 && debuffs.Length == 0 && purgeCount <= 0);
    }

    public float GetStat(StatType statType)
    {
        return statManager.GetStat(statType);

    }

	public float GetStat(StatType statType, ModifierType modifierType)
    {
        return statManager.GetStat(statType, modifierType);
    }

    public StatManager GetStats()
    {
        return statManager;
    }

    public List<DamageType> GetDamageTypes(ProcTarget target)
    {
        int length = Enum.GetNames(typeof(DamageType)).Length;
        bool[] direct = new bool[length];
        bool[] effect = new bool[length];
        List<DamageType> toReturn = new List<DamageType>();

        if(target == ProcTarget.self)
        {
            if(Utility.IsNonEmpty(selfDamages)) direct = ContainsDamageTypes(selfDamages);
            if(Utility.IsNonEmpty(buffs)) effect = ContainsDamageTypes(buffs);
        }
        if(target == ProcTarget.target)
        {
            if(Utility.IsNonEmpty(damages)) direct = ContainsDamageTypes(damages);
            if(Utility.IsNonEmpty(debuffs)) effect = ContainsDamageTypes(debuffs);
        }

        for(int i = 0; i < length; i++)
        {
            if(direct[i] || effect[i]) toReturn.Add((DamageType)i);
        }

        return toReturn;

    }

    private bool[] ContainsDamageTypes(Damage[] damages)
    {
        bool[] toReturn = new bool[Enum.GetNames(typeof(DamageType)).Length];

        for(int i = 0; i < damages.Length; i++)
        {
            toReturn[(int)damages[i].type] = true;
        }

        return toReturn;
    }

    private bool[] ContainsDamageTypes(TimedEffect[] effects)
    {
        int length = Enum.GetNames(typeof(DamageType)).Length;
        bool[] toReturn = new bool[length];
        bool[] final = new bool[length];
        bool[] periodic = new bool[length];

        for(int i = 0; i < effects.Length; i++)
        {
            final = ContainsDamageTypes(effects[i].finalDamages);
            periodic =  ContainsDamageTypes(effects[i].periodicDamages);
        }

        for(int i = 0; i < length; i++)
        {
            toReturn[i] = final[i] || periodic[i];
        }

        return toReturn;
    }

    public void SetUsedTriggers(bool[] triggers)
    {
        for(int i = 0; i < usedTriggers.Length; i++)
        {
            usedTriggers[i] = triggers[i];
        }
    }

    public void UseTrigger(ProcTrigger trigger)
    {
        usedTriggers[(int)trigger] = true;
    }

    public Action Clone()
    {
        Action toReturn = new Action(name, spriteIndex, proficiencyType, actionConditions, buffs, debuffs, damages, selfDamages, instanceStats, conditionalResources, damageFactor, purgeCount, cleanseCount);
        toReturn.SelectAugment(selectedAugment);
        toReturn.SetUsedTriggers(usedTriggers);
        
        return toReturn;
    }

    public int GetSprite()
    {
        return spriteIndex;
    }

}
public enum ConditionTarget { self, target, };
public enum ConditionComparison { atLeast, atMost, };
public enum ConditionType { always, healthPercentage, buffCount, debuffCount, sneakCount, rageCount, madnessCount, displacementCount, enduranceCount};

[Serializable]
public struct ActionCondition { public ConditionTarget target; public ConditionComparison comparison; public ConditionType type; public float value; };

[Serializable]
public struct ConditionalStat { public ActionCondition condition;  public EquipStat[] stats;}

[Serializable]
public struct ConditionalResource {public UnitResourceAdder resource; public ActionCondition actionCondition;}

[Serializable]
public struct UnitResourceAdder {public UnitResourceType type; public int value;}