﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TimedEffect : Timable
{
    public EquipStat[] stats;
    public Damage[] finalDamages;
    public Damage[] periodicDamages;
    public float damageFactor, period, currentPeriod;
    
    public delegate void OnExpire(TimedEffect timedEffect);
    public OnExpire onExpire;

    public TimedEffect() : base()
    {

    }

    public TimedEffect(string name, float duration, float period, float damageFactor, EquipStat[] stats, Damage[] finalDamages, Damage[] periodicDamages, int spriteIndex) 
        : base(name, duration, spriteIndex)
    {
        this.stats = stats;
        this.finalDamages = finalDamages;
        this.damageFactor = damageFactor;
        this.period = currentPeriod = period;
        this.periodicDamages = periodicDamages;
    }


    public override void OnRegister(Unit unit)
    {
        if(stats.Length > 0) unit.AdjustStats(stats);
        base.OnRegister(unit);
    }

    public override void OnDeregister(Unit unit)
    {
        if(stats.Length > 0) unit.AdjustStats(stats, -1);
        base.OnDeregister(unit);
        RaiseOnExpire();
    }

    public override void OnTimerComplete(float remainder, Unit parent, Unit target)
    {
        active = false;
        if(finalDamages.Length > 0 && parent != null) parent.ApplyDamage(finalDamages);
        base.OnTimerComplete(remainder, parent, target);
    }

    public void RaiseOnExpire()
    {
        if(onExpire != null) onExpire(this);
    }
    
    public override void UpdateTimer(float timePassed, Unit parent, Unit target)
    {
        if(ApplyTimerUpdate(ref currentPeriod, timePassed, parent)) OnPeriodComplete(currentPeriod, parent);
        base.UpdateTimer(timePassed, parent, target);
    }

    public virtual void OnPeriodComplete(float remainder, Unit parent)
    {
        if(period <= 0) return;
        currentPeriod = period + remainder;
        if(periodicDamages.Length > 0) parent.ApplyDamage(periodicDamages);
    }

    public override Timable Clone()
    {
        TimedEffect clone = new TimedEffect(name, duration, period, damageFactor, stats, finalDamages, periodicDamages, spriteIndex);

        return clone;
    }

}
