﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TalentAbility : Ability
{
    private AbilityAugment selectedAugment;

    public TalentAbility(string name, float duration, Action action, int spriteIndex) 
    : base(name, duration, action, spriteIndex)
    {
        
    }

    public void SelectAugment(AbilityAugment talentOption)
    {
        if(talentOption == null) return;
        selectedAugment = talentOption;
        action.SelectAugment(selectedAugment);
    }

    public override Timable Clone()
    {
        TalentAbility toReturn = new TalentAbility(name, duration, action.Clone(), spriteIndex);

        return toReturn;
    }
}