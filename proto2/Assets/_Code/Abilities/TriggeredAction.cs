﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TriggeredAction 
{
	public Action action;
	public string name;
	public int spriteIndex;
	public ProcTarget procTarget;
	public ProcTrigger procTrigger;
	public float procChance;
	

	public TriggeredAction()
	{

	}

	public TriggeredAction(string name, ProcTarget procTarget, ProcTrigger procTrigger, float procChance, Action action, int spriteIndex)
    {
		this.name = name;
		this.spriteIndex = spriteIndex;
		this.procTarget = procTarget;
		this.procTrigger = procTrigger;
		this.procChance = procChance;
		this.action = action;
    }

    public virtual void OnRegister(Unit unit)
    {
        
    }

    public virtual void OnDeregister(Unit unit)
    {
        
    }

	public void Trigger(Unit parent, Unit target, bool[] usedTriggers)
    {
		if(!GetProcCheck()) return;
		if(parent == null) return;
        if(CheckUse(parent, target)) Use(parent, target, usedTriggers);
    }

	public bool CheckUse(Unit parent, Unit target)
	{
		return action != null && action.CheckCanUse(parent, target);
	}

	public virtual void Use(Unit parent, Unit target, bool[] usedTriggers)
    {
		if(!usedTriggers[(int)procTrigger])
		{
			Action sendAction = action.Clone();
			sendAction.SetUsedTriggers(usedTriggers);
			sendAction.UseTrigger(procTrigger);
			sendAction.Use(parent, target);
		}
    }

	public bool GetProcCheck()
	{
		float r = UnityEngine.Random.Range(0, 1.0f);
		bool isSuccess = r < procChance;
		
		return isSuccess;
	}

    public int GetSprite()
    {
        return spriteIndex;
    }

	public virtual TriggeredAction Clone()
	{
		TriggeredAction clone = new TriggeredAction(name, procTarget, procTrigger, procChance, action, spriteIndex);		
		return clone;
	}


}