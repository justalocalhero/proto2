﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Debuff : TimedEffect
{
    public Debuff() : base()
    {

    }

    public Debuff(string name, float duration, float period, float damageFactor, EquipStat[] stats, Damage[] finalDamages, Damage[] periodicDamages, int spriteIndex) 
        : base(name, duration, period, damageFactor, stats, finalDamages, periodicDamages, spriteIndex)
    {
        
    }


    public override void OnTimerComplete(float remainder, Unit parent, Unit target)
    {
        if(parent != null) parent.RemoveDebuff(this);
        base.OnTimerComplete(remainder, parent, target);
    }

    public override Timable Clone()
    {
        Debuff clone = new Debuff(name, duration, period, damageFactor, stats, finalDamages, periodicDamages, spriteIndex);

        return clone;
    }
}
