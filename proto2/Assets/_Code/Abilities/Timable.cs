﻿using UnityEngine;

public class Timable 
{ 
    public float duration, currentTimer;
    public bool active;
    public string name;
    public int spriteIndex;

    public Timable()
    {

    }
    
    public Timable(string name, float duration, int spriteIndex)
    {
        this.name = name;
        this.spriteIndex = spriteIndex;
        this.duration = duration;
        currentTimer = duration;
        active = true;
    }

    public void SetSprite(int spriteIndex)
    {
        this.spriteIndex = spriteIndex;
    }

    public int GetSprite()
    {
        return spriteIndex;
    }

    public virtual void UpdateTimer(float timePassed, Unit parent, Unit target)
    {
        if(ApplyTimerUpdate(ref currentTimer, timePassed, parent)) 
            OnTimerComplete(currentTimer, parent, target);
    }

    public virtual void UpdateTimer(float timePassed, bool hasTarget, Unit parent, Unit target)
    {
        UpdateTimer(timePassed, parent, target);
    }

    public bool ApplyTimerUpdate(ref float timer, float timePassed, Unit parent)
    {
        bool timerComplete = false;

        if(active)
        {
            if(timer > 0) timer -= timePassed;
            if(timer <= 0) timerComplete = true;
        }

        return timerComplete;
    }

    public void PrepCooldown()
    {
        PrepCooldown(1);
    }

    public void ZeroCooldown()
    {
        currentTimer = duration;
    }

    public void PrepCooldown(float factor)
    {
        float halfDuration = .5f * duration * factor;
        currentTimer = UnityEngine.Random.Range(0, halfDuration) + UnityEngine.Random.Range(0, halfDuration);

    }

    public virtual void OnRegister(Unit unit)
    {
        
    }

    public virtual void OnDeregister(Unit unit)
    {
        
    }

    public virtual void OnTimerComplete(float remainder, Unit parent, Unit target)
    {
        currentTimer = GetDuration(parent, target) + remainder;
    }

    public virtual Timable Clone()
    {
        return new Timable(name, duration, spriteIndex);
    }

    public virtual float GetDuration(Unit parent, Unit target)
    {
        return duration;
    }

}
