﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Ability : TimedAction 
{
    public Ability(string name, float period, Action action, int spriteIndex) : base(name, period, action, spriteIndex) 
    {
        

    }

    public override Timable Clone()
    {
        return new Ability(name, duration, action, spriteIndex);
    }
}

public enum ProficiencyType 
{
    spell, support, pierce, blunt, tech, curse,
};