﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedAction : Timable
{
    public Action action;

    public TimedAction() : base()
    {
        
    }

    public TimedAction(string name, float period, Action action, int spriteIndex) 
        : base(name, period, spriteIndex) 
    { 
        this.action = action;

    }

    public override void UpdateTimer(float timePassed, Unit parent, Unit target)
    {
        UpdateTimer(timePassed, true, parent, target);
    }

    public override void UpdateTimer(float timePassed, bool hasTarget, Unit parent, Unit target)
    {
        bool timerComplete = ApplyTimerUpdate(ref currentTimer, timePassed, parent);
        bool hasGlobal = parent.GlobalCooldownAvailable();
        bool willUse = timerComplete && hasGlobal && hasTarget;
        if(willUse) OnTimerComplete(currentTimer, parent, target);
    }

    public void RefreshTimer(float timePassed)
    {
        currentTimer = Mathf.Clamp(currentTimer - timePassed, 0, currentTimer - timePassed);
    }

    public void RefreshTimer()
    {
        currentTimer = 0;
    }

    public override void OnTimerComplete(float remainder, Unit parent, Unit target)
    {
        if(CheckUse(parent, target))
        {
            Use(parent, target);
            base.OnTimerComplete(remainder, parent, target);
        }
    }

    public bool CheckUse(Unit parent, Unit target)
    {
        return (action != null && action.CheckCanUse(parent, target));
    }

    public virtual void Use(Unit parent, Unit target)
    {
        if(parent == null) return;
        if(duration <= 0) return;
        parent.ResetGlobalCooldown();
        Action sendAction = action.Clone();
        sendAction.Use(parent, target);


    }

    public Action GenerateAction()
    {
        return action;
    }
    
    public override float GetDuration(Unit parent, Unit target)
    {
        float toReturn = duration;
        if(action.selectedAugment != null && action.selectedAugment.CheckCanUse(parent, target)) 
            toReturn += action.selectedAugment.period;

        return toReturn;
    }

    public override Timable Clone()
    {
        TimedAction clone = new TimedAction(name, duration, action, spriteIndex);
        
        return clone;
    }
}
