﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public struct Damage {

    public DamageType type;
    public ActionResult result;
    public float damageFactor;
    public float value;

    public bool IsHit()
    {
        return result == ActionResult.crit || result == ActionResult.hit;
    }

    public bool IsCrit()
    {
        return result == ActionResult.crit;
    }

    public bool IsDodge()
    {
        return result == ActionResult.dodge;
    }

    public bool IsBlock()
    {
        return result == ActionResult.block;
    }

    public bool IsMiss()
    {
        return result == ActionResult.miss;
    }

    public float GetValue()
    {
        return value;
    }

    public float GetDamageFactor()
    {
        return damageFactor;
    }
}

public enum DamageType { physical, poison, bleed, healing, fire, cold, arcane, dark, };
public enum ActionResult { miss, hit, dodge, block, crit, reflect, support,};