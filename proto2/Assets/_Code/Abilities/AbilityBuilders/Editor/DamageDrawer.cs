﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(Damage))]
public class DamageDrawer : PropertyDrawer {

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int typeSize = 80;
        int amountLabelSize = 50;
        int amountSize = 40;        
        int scaleLabelSize = 50;
        int scaleSize = 40;
        int spacing = 5;
        int typeIndent = 0;
        int amountLabelIndent = typeIndent + typeSize + spacing;
        int amountIndent = amountLabelIndent + amountLabelSize;
        int scaleLabelIndent = amountIndent + amountSize + spacing;
        int scaleIndent = scaleLabelIndent + scaleLabelSize;

        Rect typeRect = new Rect(position.x + typeIndent, position.y, typeSize, position.height);        
        Rect amountLabelRect = new Rect(position.x + amountLabelIndent, position.y, amountLabelSize, position.height);   
        Rect amountRect = new Rect(position.x + amountIndent, position.y, amountSize, position.height);        
        Rect scaleLabelRect = new Rect(position.x + scaleLabelIndent, position.y, scaleLabelSize, position.height);
        Rect scaleRect = new Rect(position.x + scaleIndent, position.y, scaleSize, position.height);

        EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("type"), GUIContent.none);
        EditorGUI.LabelField(amountLabelRect, "Amount");
        EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("value"), GUIContent.none);        
        EditorGUI.LabelField(scaleLabelRect, "Factor");
        EditorGUI.PropertyField(scaleRect, property.FindPropertyRelative("damageFactor"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
