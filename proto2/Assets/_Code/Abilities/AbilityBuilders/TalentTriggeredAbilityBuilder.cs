﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Builders/TalentTriggeredAbility")]
public class TalentTriggeredAbilityBuilder : TriggeredActionBuilder 
{
	new public TalentTriggeredAbility Generate()
	{
		Action action = new Action(name, spriteIndex, proficiencyType, conditions, GenerateArray(buffs), GenerateArray(debuffs), damages, selfDamages, instanceStats, unitResources, damageFactor, purgeCount, cleanseCount);
		TalentTriggeredAbility ability = new TalentTriggeredAbility(name, procTarget, procTrigger, procChance, action, spriteIndex);

		return ability;
	}

}
