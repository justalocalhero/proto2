﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Builders/Ability")]
public class AbilityBuilder : ActionBuilder 
{
	[Space(20)]
	public float period;
	
	public virtual Ability Generate()
	{
		Action action = new Action(name, spriteIndex, proficiencyType, conditions, GenerateArray(buffs), GenerateArray(debuffs), damages, selfDamages, instanceStats, unitResources, damageFactor, purgeCount, cleanseCount);
		Ability ability = new Ability(name, period, action, spriteIndex);

		return ability;
	}

	public Buff[] GenerateArray(BuffBuilder[] array)
	{
		Buff[] toReturn = new Buff[array.Length];
		for(int i = 0; i < array.Length; i++)
		{
			toReturn[i] = array[i].Generate();
		}
		return toReturn;
	}

	public Debuff[] GenerateArray(DebuffBuilder[] array)
	{
		Debuff[] toReturn = new Debuff[array.Length];
		for(int i = 0; i < array.Length; i++)
		{
			toReturn[i] = array[i].Generate();
		}
		return toReturn;
	}

	public void EditDuration(float duration)
	{
		period = duration;

		Save();
	}
}
