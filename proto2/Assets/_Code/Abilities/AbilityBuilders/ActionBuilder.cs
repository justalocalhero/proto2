﻿using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;

public class ActionBuilder : ScriptableObject
{	
	new public string name;
	public ProficiencyType proficiencyType;	

	[Space(20)]
	public Sprite sprite;
	public SpriteManager spriteManager;
	public int spriteIndex;

	[Space(20)]
	public float damageFactor;
	public ActionCondition[] conditions;
	public Damage[] damages;
	public Damage[] selfDamages;
	public BuffBuilder[] buffs;
	public DebuffBuilder[] debuffs;
	public ConditionalStat[] instanceStats;
	public ConditionalResource[] unitResources;

	[Space(20)]
	public int purgeCount;
	public int cleanseCount;

	public void OnEnable()
	{
		ValidateSprite();
	}

	public void SetSpriteIndex()
	{
		if(sprite == null) return;
		if(spriteManager == null) return;
		spriteIndex = spriteManager.GetIndex(sprite);
		if(spriteIndex == -1)
			spriteIndex = spriteManager.AddSprite(sprite);
		if(spriteIndex == -1)
			Debug.LogWarning("Panic");
	}

	public void OnValidate()
	{
		ValidateSprite();
	}

	public void ValidateSprite()
	{
		if(sprite == null) return;
		if(spriteManager == null) return;

		if(sprite == spriteManager.GetSprite(spriteIndex)) 
			return;
		else 
			SetSpriteIndex();
	}

	public void DeleteDamage(int index)
	{
		if(index < 0 || index >= damages.Length) return;

		var stats = new List<Damage>(damages);
		stats.RemoveAt(index);
		damages = stats.ToArray();

		Save();
	}

	public void SetDamages(List<Damage> damages)
	{
		if(damages == null) return;

		this.damages = damages.ToArray();

		Save();
	}

	public void EditDamage(int index, Damage damage)
	{
		if(index < 0 || index >= damages.Length) return;

		damages[index] = damage;

		Save();
	}

	public void ExtendDamage()
	{
		var stats = new List<Damage>(damages);
		stats.Add(new Damage());
		damages = stats.ToArray();

		Save();
	}

	protected void Save()
	{
		
 		#if UNITY_EDITOR
		EditorUtility.SetDirty(this);
		#endif
	}
}
