﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Builders/Debuff")]
public class DebuffBuilder : TimedEffectBuilder 
{

	public Debuff Generate()
	{
		Debuff debuff = new Debuff(name, duration, period, damageFactor, stats, finalDamages, periodicDamages, spriteIndex);

		return debuff;		
	}
}
