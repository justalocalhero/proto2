﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Builders/TalentAbility")]
public class TalentAbilityBuilder : AbilityBuilder 
{
	new public TalentAbility Generate()
	{
		Action action = new Action(name, spriteIndex, proficiencyType, conditions, GenerateArray(buffs), GenerateArray(debuffs), damages, selfDamages, instanceStats, unitResources, damageFactor, purgeCount, cleanseCount);
		TalentAbility ability = new TalentAbility(name, period, action, spriteIndex);

		return ability;
	}
}
