﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class TimedEffectBuilder : ScriptableObject 
{
	new public string name;
	public SpriteManager spriteManager;
	public int spriteIndex;
	public Sprite sprite;
	public float duration;
	public float period;
	public float damageFactor;
	public Damage[] periodicDamages;
	public Damage[] finalDamages;
	public EquipStat[] stats;


	public void OnEnable()
	{
		ValidateSprite();
	}

	public void SetSpriteIndex()
	{
		if(sprite == null) return;
		if(spriteManager == null) return;
		spriteIndex = spriteManager.GetIndex(sprite);
		if(spriteIndex == -1)
			spriteIndex = spriteManager.AddSprite(sprite);
		if(spriteIndex == -1)
			Debug.LogWarning("Panic");
	}

	public void OnValidate()
	{
		ValidateSprite();
	}

	public void ValidateSprite()
	{
		if(sprite == null) return;
		if(spriteManager == null) return;

		if(sprite == spriteManager.GetSprite(spriteIndex)) 
			return;
		else 
			SetSpriteIndex();
	}

	public void RemoveStat(int index)
	{
		if(StatIndexInvalid(index)) return;

		var temp = new List<EquipStat>(stats);
		temp.RemoveAt(index);
		stats = temp.ToArray();

		Save();
	}

	public void AddStat()
	{
		var temp = new List<EquipStat>(stats);
		temp.Add(new EquipStat());
		stats = temp.ToArray();

		Save();
	}

	public void EditStat(int index, EquipStat stat)
	{
		if(StatIndexInvalid(index)) return;

		stats[index] = stat;

		Save();
	}

	public void EditPeriodicDamage(int index, Damage damage)
	{
		if(PeriodicDamageInvalidIndex(index)) return;

		periodicDamages[index] = damage;

		Save();
	}
	
	public void AddPeriodicDamage()
	{
		var temp = new List<Damage>(periodicDamages);
		temp.Add(new Damage());
		periodicDamages = temp.ToArray();

		Save();
	}

	public void RemovePeriodicDamage(int index)
	{
		if(PeriodicDamageInvalidIndex(index)) return;

		var temp = new List<Damage>(periodicDamages);
		temp.RemoveAt(index);
		periodicDamages = temp.ToArray();

		Save();
	}

	public void SetPeriodicDamages(List<Damage> damages)
	{
		if(damages == null) return;
		this.periodicDamages = damages.ToArray();

		Save();
	}

	public void SetStats(List<EquipStat> stats)
	{
		if(stats == null) return;

		this.stats = stats.ToArray();

		Save();
	}

	public void EditDuration(float duration)
	{
		this.duration = duration;

		Save();
	}

	public void EditPeriod(float period)
	{
		this.period = period;
		
		Save();
	}

	protected bool StatIndexInvalid(int index)
	{
		return (stats == null || index < 0 || index >= stats.Length);
	}

	protected bool PeriodicDamageInvalidIndex(int index)
	{
		return (periodicDamages == null || index < 0 || index >= periodicDamages.Length);
	}

	private void Save()
	{
		#if UNITY_EDITOR
		EditorUtility.SetDirty(this);
		#endif
	}

}
