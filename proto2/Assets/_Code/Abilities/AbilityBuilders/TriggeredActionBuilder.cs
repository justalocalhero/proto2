﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Builders/TriggeredAction")]
public class TriggeredActionBuilder : ActionBuilder 
{
	[Space(20)]
	public ProcTarget procTarget;
	public ProcTrigger procTrigger;
	public float procChance;
	
	
	public virtual TriggeredAction Generate()
	{
		Action action = new Action(name, spriteIndex, proficiencyType, conditions, GenerateArray(buffs), GenerateArray(debuffs), damages, selfDamages, instanceStats, unitResources, damageFactor, purgeCount, cleanseCount);
		TriggeredAction triggeredAbility = new TriggeredAction(name, procTarget, procTrigger, procChance, action, spriteIndex);

		return triggeredAbility;
	}
  
	public Buff[] GenerateArray(BuffBuilder[] array)
	{
		Buff[] toReturn = new Buff[array.Length];
		for(int i = 0; i < array.Length; i++)
		{
			toReturn[i] = array[i].Generate();
		}
		return toReturn;
	}

	public Debuff[] GenerateArray(DebuffBuilder[] array)
	{
		Debuff[] toReturn = new Debuff[array.Length];
		for(int i = 0; i < array.Length; i++)
		{
			toReturn[i] = array[i].Generate();
		}
		return toReturn;
	}

	public void EditCondition(ProcTrigger trigger, ProcTarget target, float chance)
	{
		procTarget = target;
		procTrigger = trigger;
		procChance = chance;

		Save();
	}
}
