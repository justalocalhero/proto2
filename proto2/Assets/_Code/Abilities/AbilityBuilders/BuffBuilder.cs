﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Builders/Buff")]
public class BuffBuilder : TimedEffectBuilder 
{
	public Buff Generate()
	{
		Buff buff = new Buff(name, duration, period, damageFactor, stats, finalDamages, periodicDamages, spriteIndex);

		return buff;
	}
}
