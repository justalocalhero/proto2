﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Builders/AbilityAugment")]
public class AbilityAugmentBuilder : ScriptableObject 
{
	public ActionCondition[] actionConditions;
	public float period;
	public BuffBuilder[] buffs;
    public DebuffBuilder[] debuffs;
    public Damage[] damages;
    public Damage[] selfDamages;
    public ConditionalStat[] instanceStats;    
    public ConditionalResource[] unitResources;
    public float damageFactor;
    public int purgeCount;
    public int cleanseCount;

	public AbilityAugment Generate()
	{
		AbilityAugment toReturn = new AbilityAugment(actionConditions, period, GenerateArray(buffs), GenerateArray(debuffs), damages, selfDamages, instanceStats, unitResources, damageFactor, purgeCount, cleanseCount);

		return toReturn;
	}

	public Buff[] GenerateArray(BuffBuilder[] array)
	{
		Buff[] toReturn = new Buff[array.Length];
		for(int i = 0; i < array.Length; i++)
		{
			toReturn[i] = array[i].Generate();
		}
		return toReturn;
	}

	public Debuff[] GenerateArray(DebuffBuilder[] array)
	{
		Debuff[] toReturn = new Debuff[array.Length];
		for(int i = 0; i < array.Length; i++)
		{
			toReturn[i] = array[i].Generate();
		}
		return toReturn;
	}
}
