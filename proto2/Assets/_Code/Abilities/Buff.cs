﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Buff : TimedEffect
{
    public Buff() : base()
    {

    }

    public Buff(string name, float duration, float period, float damageFactor, EquipStat[] stats, Damage[] finalDamages, Damage[] periodicDamages, int spriteIndex) 
        : base(name, duration, period, damageFactor, stats, finalDamages, periodicDamages, spriteIndex)
    {
        
    }


    public override void OnTimerComplete(float remainder, Unit parent, Unit target)
    {
        if(parent != null) parent.RemoveBuff(this);
        base.OnTimerComplete(remainder, parent, target);
    }
    
    public override Timable Clone()
    {
        Buff clone = new Buff(name, duration, period, damageFactor, stats, finalDamages, periodicDamages, spriteIndex);

        return clone;
    }
}
