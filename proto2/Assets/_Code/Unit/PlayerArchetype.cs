﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Player/PlayerArchetype")]
public class PlayerArchetype : ScriptableObject
{
	public List<AbilityBuilder> abilityPool;
	public List<EquipmentGenerator> equipmentPool;

	[Space(10)]
	public int baseStr;
	public int levelStr;

	[Space(10)]
	public int baseDex;
	public int levelDex;

	[Space(10)]
	public int baseCon;
	public int levelCon;

	[Space(10)]
	public int baseInt;
	public int levelInt;

	public Ability GetAbility()
	{
		return Utility.RandomFromList(abilityPool).Generate();
	}

	public Equipment GetEquipment()
	{
		return Utility.RandomFromList(equipmentPool).Generate();
	}

	public List<EquipStat> GetStats(int level)
	{
		List<EquipStat> toReturn = new List<EquipStat>();

		toReturn.Add(
			new EquipStat {
				StatType = StatType.Strength,
				ModifierType = ModifierType.Flat,
				Value = LevelValue(baseStr, levelStr, level),
			}
		);

		toReturn.Add(
			new EquipStat {
				StatType = StatType.Dexterity,
				ModifierType = ModifierType.Flat,
				Value = LevelValue(baseDex, levelDex, level),
			}
		);

		toReturn.Add(
			new EquipStat {
				StatType = StatType.Intelligence,
				ModifierType = ModifierType.Flat,
				Value = LevelValue(baseInt, levelInt, level),
			}
		);

		toReturn.Add(
			new EquipStat {
				StatType = StatType.Constitution,
				ModifierType = ModifierType.Flat,
				Value = LevelValue(baseCon, levelCon, level),
			}
		);

		return  toReturn;
	}

	public int LevelValue(int baseValue, int levelValue, int level)
	{
		int toReturn = baseValue;

		for(int i = 0; i < level; i++)
		{
			toReturn += UnityEngine.Random.Range(0, levelValue + 1);
		}

		return toReturn;
	}
}
