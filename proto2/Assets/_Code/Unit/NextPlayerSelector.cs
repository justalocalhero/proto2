﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextPlayerSelector : MonoBehaviour 
{
	public SaveManager saveManager;
	public Bullpen bullpen;
	public Tavern tavern;
	public IntReference playerChoices;
	private List<PlayerCard> cards = new List<PlayerCard>();
	public List<PlayerArchetype> playerArchetypes;
	public LevelManager levelManager;
	public ClassManager raceManager;
	public ClassManager occupationManager;
	public ClassManager classManager;

	public FloatVariable abilityChance, equipmentChance;

	public delegate void OnCardsChanged();
	public OnCardsChanged onCardsChanged;

	public delegate void OnCardSelected(PlayerCard playerCard);
	public OnCardSelected onCardSelected;

	public void Start()
	{
		bullpen.onPlayerSent += Generate;
		tavern.onUnlock += Generate;
		saveManager.onPostLoad += Generate;
	}
	
	public void Generate(Player player)
	{
		Generate();
	}

	public void Generate()
	{
		cards = new List<PlayerCard>();
		int level = levelManager.GetNextHeroLevel();

		for(int i = 0; i < playerChoices; i++)
		{
			cards.Add(GenerateCard(Utility.RandomFromList(playerArchetypes), level));
		}

		RaiseOnCardsChanged();
	}

	public List<PlayerCard> GetPlayerCards()
	{
		return cards;
	}
	
	public PlayerCard GenerateCard(PlayerArchetype playerArchetype, int level)
	{

		PlayerCard toReturn = new PlayerCard()
			.WithLevel(level)
			.WithStats(playerArchetype.GetStats(level))
			.WithAbility(GetAbility(playerArchetype))
			.WithEquipment(GetEquipment(playerArchetype))
			.WithRace(raceManager.GetClass())
			.WithOccupation(occupationManager.GetClass())
			.WithClass(classManager.GetClass());

		return toReturn;

	}

	public Ability GetAbility(PlayerArchetype playerArchetype)
	{
		bool hasAbility = UnityEngine.Random.Range(0, 1.0f) < abilityChance.Value;

		return (hasAbility) ? playerArchetype.GetAbility() : null;
	}

	public Equipment GetEquipment(PlayerArchetype playerArchetype)
	{		
		bool hasEquipment = UnityEngine.Random.Range(0, 1.0f) < equipmentChance.Value;

		return (hasEquipment) ? playerArchetype.GetEquipment() : null;
	}

	public void RaiseOnCardsChanged()
	{
		if(onCardsChanged != null) onCardsChanged();
	}

	public void Select(PlayerCard playerCard)
	{
		if(playerCard == null) return;

		RaiseOnCardSelected(playerCard);
	}

	public void RaiseOnCardSelected(PlayerCard playerCard)
	{
		if(onCardSelected != null) onCardSelected(playerCard);
	}
}
