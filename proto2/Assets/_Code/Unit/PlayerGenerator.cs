﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Generators/PlayerGenerator")]
public class PlayerGenerator : ScriptableObject 
{
	public List<RaceClass> raceClass;
	public UnitResourceDictionary unitResourceDictionary;

	int count = 0;
	
	public Player Generate(int talentLevel)
	{
		Player player = new Player("player" + count++, new UnitResourceManager(unitResourceDictionary), 0);
		player.SetTalentLevel(talentLevel);
		player.AddRaceClass(raceClass);
		return player;
	}

	public Player Generate(int talentLevel, CampSupplyManager campSupplyManager)
	{
		Player player = new Player("player" + count++, new UnitResourceManager(unitResourceDictionary), campSupplyManager.campHealing.Value);
		player.SetTalentLevel(talentLevel);
		player.AddRaceClass(raceClass);
		return player;
	}

	public Player Generate(PlayerCard card, CampSupplyManager campSupplyManager)
	{
		Player player = new Player("player" + count++, new UnitResourceManager(unitResourceDictionary), campSupplyManager.campHealing.Value);
		player.SetTalentLevel(card.level);

		if(card.stats != null) player.AdjustStats(card.stats);
		if(card.currentRace != null) player.AddRaceClass(card.currentRace);
		if(card.currentOccupation != null) player.AddRaceClass(card.currentOccupation);
		if(card.currentClass != null) player.AddRaceClass(card.currentClass);

		if(card.ability != null) player.AddAction(card.ability);

		return player;
	}
}
