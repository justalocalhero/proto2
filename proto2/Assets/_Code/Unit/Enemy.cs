﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Loot;

[System.Serializable]
public class Enemy : Unit
{
    public LootGenerator loot;
    public int level;
    public LootRollManager lootRollManager;
    public LootProfile lootProfile;

    public Enemy(string name, int level, UnitResourceManager unitResourceManager) : base(name, unitResourceManager)
    {
        this.level = level;
        lootRollManager = new LootRollManager();
        lootRollManager.AddLevel(level);
    }
    
    public void SetLootGenerator(LootGenerator loot)
    {
        this.loot = loot;
    }

    public void SetLootProfile(LootProfile lootProfile)
    {
        this.lootProfile = lootProfile;
    }

    public Equipment GenerateLoot()
    {
        if(loot == null) return null;
        return loot.Generate(lootRollManager, lootProfile);
    }

    public override void AddRaceClass(RaceClass raceClass, float multiplier)
    {
        lootRollManager.AddLootValues(raceClass.lootValue);
        base.AddRaceClass(raceClass, multiplier);
    }

    public int GetLevel()
    {
        return level;
    }

    public override float GetAccuracyRoll(Unit target)
    {
        float accuracyLevel = -statManager.GetStat(StatType.Accuracy);
        float inverseFactor = (accuracyLevel < 0) ? Sigmoid(-accuracyLevel, 120, 1.1f) : 0;
        float factor = 1 - inverseFactor;
        return factor;
    }
}
