﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tooltips;

public class TavernUI : MonoBehaviour 
{
	public PlayerCardUI playerCardUIPrototype;
	private List<PlayerCardUI> cards;
	private PlayerCardUI selectedCard;
	public Transform cardContainer;
	public IntReference cardCount;

	public AbilityTooltip abilityTooltip;
	public TriggeredActionTooltip triggeredActionTooltip;
	public EquipmentTooltip equipmentTooltip;
	public NextPlayerSelector nextPlayerSelector;
	
	public delegate void OnCardSelected(PlayerCard playerCard);
	public OnCardSelected onCardSelected;

	void Start () 
	{
		BuildCards();
		nextPlayerSelector.onCardsChanged += SetUI;
	}
	
	private void BuildCards()
	{
		cards = new List<PlayerCardUI>();

		RectTransform containerRect = cardContainer as RectTransform;
        RectTransform slotRect = playerCardUIPrototype.transform as RectTransform;

        float containerWidth = containerRect.rect.width;
        float containerHeight = containerRect.rect.height;

		for(int i = 0; i < cardCount; i++)
		{
			float xMin = (i + 0.0f) / cardCount;
			float xMax = (i + 1.0f) / cardCount;
			float yMin = 0;
			float yMax = 1;



			PlayerCardUI temp = Object.Instantiate(playerCardUIPrototype, cardContainer);
			RectTransform tempRect = (temp.transform as RectTransform);
			tempRect.anchorMin = new Vector2(xMin, yMin);
			tempRect.anchorMax = new Vector2(xMax, yMax);

			cards.Add(temp);
			PassReferences(temp);
		}
        
	}

	private void PassReferences(PlayerCardUI card)
	{
		card.SetAbilityText(abilityTooltip);
		card.SetTriggeredActionText(triggeredActionTooltip);
		card.SetEquipmentText(equipmentTooltip);

		card.onCardSelected += Select;
	}

	public void SetUI()
	{
		ClearSelected();

		List<PlayerCard> nextCards = nextPlayerSelector.GetPlayerCards();

		for(int i = 0; i < cards.Count; i++)
		{
			if(nextCards == null || i >= nextCards.Count) cards[i].Clear();
			else cards[i].Set(nextCards[i]);
		}		
	}

	public void UpdateSelectedUI()
	{
		foreach(PlayerCardUI card in cards)
		{
			if(card == selectedCard || selectedCard == null) card.Show();
			else card.Hide();
		}
	}

	public void Select(PlayerCardUI toSelect)
	{
		if(selectedCard != null) return;

		selectedCard = toSelect;
		nextPlayerSelector.Select(toSelect.playerCard);

		UpdateSelectedUI();
	}

	public void ClearSelected()
	{
		selectedCard = null;
	}
}
