﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using Tooltips;

public class PlayerCardUI : MonoBehaviour
{
	public PlayerCard playerCard {get; private set;}
	public Button button;
	public AbilityIcon abilityIcon;
	public EquipmentIcon equipmentIcon;
	public StatText statText;

	public TextMeshProUGUI titleText;
	public TextMeshProUGUI bodyText;
	public ColorReference levelColor;

	public UIToggler toggler;

	public delegate void OnCardSelected(PlayerCardUI playerCard);
	public OnCardSelected onCardSelected;

	public void Start()
	{
		button.onClick.AddListener(OnPointerDown);
	}

	public void Set(PlayerCard card)
	{
		if(card == null) return;
		playerCard = card;
		titleText.SetText(GetTitleText(card));
		bodyText.SetText(GetBodyText(card));
		SetAbilityIcon(card);
		SetEquipmentIcon(card);
		Show();
	}

	public void Clear()
	{
		Hide();
	}

	public void Show()
	{
		toggler.Show();
	}

	public void Hide()
	{
		toggler.Hide();
	}

	public void SetEquipmentText(EquipmentTooltip equipmentTooltip)
	{
		equipmentIcon.equipmentTooltip = equipmentTooltip;
	}

	public void SetAbilityText(AbilityTooltip abilityTooltip)
	{
		abilityIcon.abilityTooltip = abilityTooltip;
	}

	public void SetTriggeredActionText(TriggeredActionTooltip triggeredActionTooltip)
	{
		abilityIcon.triggeredActionTooltip = triggeredActionTooltip;
	}

	public string GetTitleText(PlayerCard card)
	{
		string toReturn = "";

		toReturn += card.GetName();
		toReturn += Tooltips.ColoredText.GetText("\nLvl. " + card.level, levelColor);

		return toReturn;
	}

	public string GetBodyText(PlayerCard card)
	{
		string statsText = GetStatsText(card);

		return statsText;
	}

	public string GetStatsText(PlayerCard card)
	{
		if(card.stats == null) return "";

		string toReturn = "";

		foreach(EquipStat stat in card.stats)
		{
			toReturn += statText.GetText(stat) + "\n";
		}

		return toReturn;
	}

	public void SetAbilityIcon(PlayerCard card)
	{
		if(card.ability != null) abilityIcon.Set(card.ability);
		else abilityIcon.Clear();
	}

	public void SetEquipmentIcon(PlayerCard card)
	{
		if(card.equipment != null) equipmentIcon.Set(card.equipment);
		else equipmentIcon.Clear();
	}

    public void OnPointerDown()
    {
		if(playerCard == null) return;

		RaiseOnCardSelected();
    }

	public void RaiseOnCardSelected()
	{
		if(onCardSelected != null) onCardSelected(this);
	}
}
