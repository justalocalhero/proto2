﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCard
{
	public RaceClass currentRace {get; set;}
	public RaceClass currentOccupation {get; set;}
	public RaceClass currentClass {get; set;}

	public Equipment equipment {get; set;}
	public Ability ability {get; set;}
	public int level {get; set;}
	public List<EquipStat> stats;


	public PlayerCard()
	{

	}

	public PlayerCard WithLevel(int level)
	{
		this.level = level;

		return this;
	}

	public PlayerCard WithStats(List<EquipStat> stats)
	{
		this.stats = stats;

		return this;
	}
	
	public PlayerCard WithRace(RaceClass newRace)
	{
		currentRace = newRace;

		return this;
	}

	public PlayerCard WithOccupation(RaceClass newOccupation)
	{
		currentOccupation = newOccupation;

		return this;
	}

	public PlayerCard WithClass(RaceClass newClass)
	{
		currentClass = newClass;

		return this;
	}

	public PlayerCard WithEquipment(Equipment equipment)
	{
		this.equipment = equipment;

		return this;
	}

	public PlayerCard WithAbility(Ability ability)
	{
		this.ability = ability;

		return this;
	}

	public string GetName()
	{
		string toReturn = "";

		if(currentRace != null) toReturn += currentRace.GetName();
		if(currentOccupation != null) toReturn += " " + currentOccupation.GetName();
		if(currentClass != null) toReturn += " " + currentClass.GetName();

		return toReturn;
	}
}
