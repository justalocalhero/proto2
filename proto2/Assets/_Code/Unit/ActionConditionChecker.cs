﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionConditionChecker 
{	
    public static bool CheckResources(ConditionalResource[] resources, Unit parent, Unit target)
    {
        if(resources == null) return true;

        for(int i = 0; i <  resources.Length; i++)
        {
            if(!CheckResource(resources[i], parent, target)) return false;
        }

        return true;
    }

    public static bool CheckResource(ConditionalResource resource, Unit parent, Unit target)
    {
        return parent.GetResourceCount(resource.resource.type) + resource.resource.value >= 0;
    }


    public static bool CheckConditions(ActionCondition[] conditions, Unit parent, Unit target)
    {
        if(conditions == null) return true;

        ActionCondition condition;
        Unit unit;

        for(int i = 0; i < conditions.Length; i++)
        {
            condition = conditions[i];
            unit = (condition.target == ConditionTarget.self) ? parent : target;
            if(!CheckCondition(conditions[i], unit)) return false;
        }

        return true;
    }

    public static bool CheckCondition(ActionCondition condition, Unit parent, Unit target)
    {
        return CheckCondition(condition, condition.target == ConditionTarget.self ? parent : target);
    }

    public static bool CheckCondition(ActionCondition condition, Unit target)
    {
        //toFix may want to fire with null target
        if(target == null) return false;

        switch(condition.type)
        {
            case ConditionType.always:
                return true;
            case ConditionType.healthPercentage:
                return CheckHealthPercentage(condition, target);
            case ConditionType.buffCount:
                return CheckBuffCount(condition, target);
            case ConditionType.debuffCount:
                return CheckDebuffCount(condition, target);
            case ConditionType.sneakCount:
                return CheckUnitResource(condition, target, UnitResourceType.sneak);
            case ConditionType.rageCount:
                return CheckUnitResource(condition, target, UnitResourceType.rage);
            case ConditionType.madnessCount:
                return CheckUnitResource(condition, target, UnitResourceType.madness);
            case ConditionType.displacementCount:
                return CheckUnitResource(condition, target, UnitResourceType.displacement);
            case ConditionType.enduranceCount:
                return CheckUnitResource(condition, target, UnitResourceType.endurance);
        }
        Debug.LogWarning("Condition " + condition + " not handled at Action.CheckCondition");
        return false;
    }

    public static bool CheckHealthPercentage(ActionCondition condition, Unit target)
    {
        float toCheck = target.GetHealthPercentage();
        return (condition.comparison == ConditionComparison.atLeast) ? toCheck >= condition.value 
            : toCheck <= condition.value;
    }

    public static bool CheckBuffCount(ActionCondition condition, Unit target)
    {
        int toCheck = target.GetBuffCount();
        return (condition.comparison == ConditionComparison.atLeast) ? toCheck >= condition.value 
            : toCheck <= condition.value;
    }

    public static bool CheckDebuffCount(ActionCondition condition, Unit target)
    {
        int toCheck = target.GetDebuffCount();
        return (condition.comparison == ConditionComparison.atLeast) ? toCheck >= condition.value 
            : toCheck <= condition.value;
    }

    public static bool CheckUnitResource(ActionCondition condition, Unit target, UnitResourceType type)
    {
        int toCheck = target.GetResourceCount(type);
        return (condition.comparison == ConditionComparison.atLeast) ? toCheck >= condition.value 
            : toCheck <= condition.value;
    }

}
