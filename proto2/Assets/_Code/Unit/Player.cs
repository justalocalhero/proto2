﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Player : Unit
{
    public int talentLevel;
    public float campHealing;
    public Buff campBuff;

    public Player(string name, UnitResourceManager resourceManager, float campHealing) : base(name, resourceManager)
    {
        this.campHealing = campHealing;
    }

    public int GetTalentLevel()
    {
        return talentLevel;
    }

    public void SetTalentLevel(int newTalentLevel)
    {
        talentLevel = newTalentLevel;
    }

    public void SetCampBuff(Buff campBuff)
    {
        this.campBuff = campBuff;
    }

    public void Stealth()
    {
        MaxStealth();

        RaiseOnStealth();
    }

    public void MaxStealth()
    {
        resourceManager.MaxStat(UnitResourceType.sneak);
    }

    public void Camp()
    {
        Heal(GetCampHealing());
        ClearUnitResources();
        AddCampBuff();
    }

    public void AddCampBuff()
    {
        if(campBuff == null) return;

        Buff temp = campBuff.Clone() as Buff;

        AddBuff(temp);
    }


    public float GetCampHealing()
    {
        float maxHP = GetMaxHitpoints();
        float factor = GetCampHealingFactor();

        float toHeal = maxHP * factor;
        
        return toHeal;
    }

    public float GetCampHealingFactor()
    {

        float baseHealingFactor = campHealing;
        float preparationFactor = 1 + GetPreparationFactor();

        return (baseHealingFactor * preparationFactor);
    }

    public float GetPreparationFactor()
    {
        float preperation = statManager.GetStat(StatType.Preparation);

        return Sigmoid(preperation, 120, 1.1f);
    }

    public float GetPerceptionCheck()
    {
        float perception = statManager.GetStat(StatType.Perception);

        float checkValue = Sigmoid(perception, 120, 1.1f);

        return checkValue;
    }

    public float GetStealthRoll()
    {
        float stealth = statManager.GetStat(StatType.Stealth);
        float checkValue = Sigmoid(stealth, 120, 1.1f);

        return checkValue;
    }

    public void ClearUnitResources()
    {
        resourceManager.Reset();
        RaiseOnUnitResourceChanged();
    }
}
