﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Unit 
{
    public bool active;
    public string name;

    public float currentHP;
    public float maxGlobalCooldown = 1f;
    public float currentGlobalCooldownTimer = 0f;

    public int procTriggerCount = Enum.GetNames(typeof(ProcTrigger)).Length;
    public int procTargetCount = Enum.GetNames(typeof(ProcTarget)).Length;
    public int talentCount;

    
    public StatManager statManager;
    public UnitResourceManager resourceManager;

    [System.Serializable]
    public class ActionList
    {
        public List<TriggeredAction> actions = new List<TriggeredAction>();
    }

    public ActionList[] triggeredActions;

    public List<Ability> actions = new List<Ability>();
    public List<Buff> buffs = new List<Buff>();
    public List<Debuff> debuffs = new List<Debuff>();
    public List<Equipment> equipment = new List<Equipment>();
    public List<Talent> talents = new List<Talent>();
    public List<RaceClass> raceClasses = new List<RaceClass>();
    public List<Boon> blessings = new List<Boon>();
    public List<Boon> spells = new List<Boon>();
    
    public Potion potion;

    public delegate void OnDeath(Damage killingBlow);
    public OnDeath onDeath;

    public delegate void OnDamage(Damage damage);
    public OnDamage onDamage;

    public delegate void OnUnitResourceChanged();
    public OnUnitResourceChanged onUnitResourceChanged;

    public delegate void OnBuffAdded(Buff buff);
    public OnBuffAdded onBuffAdded;

    public delegate void OnDebuffAdded(Debuff debuff);
    public OnDebuffAdded onDebuffAdded;
    
    public delegate void OnActionUsed(Action action);
    public OnActionUsed onActionUsed;

    public delegate void OnActionTaken(Action action);
    public OnActionTaken onActionTaken;

    public delegate void OnPotionUsed(Potion potion);
    public OnPotionUsed onPotionUsed;

    public delegate void OnEscape();
    public OnEscape onEscape;

	public delegate void OnResourceAdded(UnitResource resource);
	public OnResourceAdded onResourceAdded;

    public delegate void OnStealth();
    public OnStealth onStealth;

    public delegate void OnPurge();
    public OnPurge onPurge;

    public delegate void OnDispel();
    public OnDispel onDispel;

    public Unit()
    {
        statManager = new StatManager();
        Reset();
    }

    public Unit(string name)
    {
        this.name = name;
        statManager = new StatManager();
        Reset();
    }

    public Unit(string name, UnitResourceManager resourceManager)
    {
        this.name = name;
        statManager = new StatManager();
        this.resourceManager = resourceManager;
        SetUnitResourceManager();

        Reset();
    }

    public void SetUnitResourceManager()
    {
        statManager.SetUnitResourceManager(resourceManager);
        resourceManager.onResourceAdded += RaiseOnResourceAdded;
    }

    public void Load(SaveFile saveFile)
    {
        if(saveFile == null) return;

        Load();
    }

    public void Load()
    {
        SetUnitResourceManager();
    }

    public void Reset()
    {
        ResetGlobalCooldown();
        RemoveAllEffects();
        RemoveAllEquipment();
        RemoveAllTalents();
        ResetStats();
        InitializeTriggeredActions();
        active = true;
    }

    public void SoftReset()
    {
        RemoveAllEffects();
        ResetHitpoints();
        ResetGlobalCooldown();
        active = true;
    }

    public void ClearBuffs()
    {
        for(int i = buffs.Count - 1; i >= 0; i--)
        {
            RemoveBuff(buffs[i]);
        }
    }

    public void ClearDebuffs()
    {
        for(int i = debuffs.Count - 1; i >= 0; i--)
        {
            RemoveDebuff(debuffs[i]);
        }
    }

    public void Escape()
    {
        RaiseOnEscape();
    }

    public void UpdateTimers(float timePassed, Unit target)
    {
        if(!active) return;
        UpdateGlobalCooldown(timePassed * GetSpeedFactor());
        UpdateListTimers(actions, (timePassed * GetSpeedFactor()), (target != null), target);
        UpdateListTimers(buffs, timePassed, target);
        UpdateListTimers(debuffs, timePassed, target);
    }

    public void RefreshCooldowns(float timePassed)
    {
        if(!active) return;
        
        foreach(Ability ability in actions)
        {
            ability.RefreshTimer(timePassed);   
        }
    }

    public void RefreshCooldowns()
    {
        if(!active) return;

        foreach(Ability ability in actions)
        {
            ability.RefreshTimer();   
        }
    }

    public void UpdateGlobalCooldown(float timePassed)
    {
        if(currentGlobalCooldownTimer > 0) 
            currentGlobalCooldownTimer -= timePassed;
    }

    public void PrepCooldowns()
    {
        float hasteFactor = GetSpeedFactor();
        float inverseFactor = (hasteFactor == 0) ? 0 : 1 /  GetSpeedFactor();

        for(int i = 0; i < actions.Count; i++)
        {
            actions[i].PrepCooldown(inverseFactor);
        }
    }

    public void ZeroCooldowns()
    {
        for(int i = 0; i < actions.Count; i++)
        {
            actions[i].ZeroCooldown();
        }
    }

    public void ResetGlobalCooldown()
    {
        currentGlobalCooldownTimer = maxGlobalCooldown;
    }

    public float GetGlobalCooldown()
    {
        return currentGlobalCooldownTimer;
    }

    public bool GlobalCooldownAvailable()
    {
        return currentGlobalCooldownTimer <= 0;
    }

    public void UpdateListTimers<T>(List<T> list, float timePassed, Unit target) where T : Timable
    {
        for(int i = list.Count - 1; i >= 0; i--)
        {
            if(i >= 0  && i < list.Count && list[i] != null) list[i].UpdateTimer(timePassed, this, target);
        }
    }

    public void UpdateListTimers<T>(List<T> list, float timePassed, bool hasTarget, Unit target) where T : Timable
    {
        for(int i = list.Count - 1; i >= 0; i--)
        {
            if(i >= 0  && i < list.Count && list[i] != null) list[i].UpdateTimer(timePassed, hasTarget, this, target);
        }
    }



    #region RegisterTImers
    
    public void AddAction(Ability newAction)
    {
        actions.Add(newAction);
        newAction.OnRegister(this);
    }

    public void AddTriggeredAction(TriggeredAction newAction)
    {
        triggeredActions[(int)newAction.procTrigger + (int)newAction.procTarget * procTriggerCount].actions.Add(newAction);
        newAction.OnRegister(this);
    }

    public void AddTriggeredAction(TriggeredAction[] newActions)
    {
        if(newActions == null) return;
        for(int i = 0; i < newActions.Length; i++)
        {
            AddTriggeredAction(newActions[i]);
        }
    }

    public void AddTriggeredAction(List<TriggeredAction> newActions)
    {
        if(newActions == null) return;
        for(int i = 0; i < newActions.Count; i++)
        {
            AddTriggeredAction(newActions[i]);
        }
    }

    public void AddAction(Ability[] newActions)
    {
        if(newActions == null) return;
        for(int i = 0; i < newActions.Length; i++)
        {
            AddAction(newActions[i]);
        }
    }

    public void AddAction(List<Ability> newActions)
    {
        if(newActions == null) return;
        for(int i = 0; i < newActions.Count; i++)
        {
            AddAction(newActions[i]);
        }
    }

    public void AddBuff(Buff newBuff)
    {
        buffs.Add(newBuff);
        newBuff.OnRegister(this);
        
        RaiseOnBuffAdded(newBuff);
    }

    public void AddDebuff(Debuff newDebuff)
    {
        debuffs.Add(newDebuff);
        newDebuff.OnRegister(this);

        RaiseOnDebuffAdded(newDebuff);
    }

    public void RemoveAction(Ability action)
    {
        
        if (actions.Contains(action))
        {
            actions.Remove(action);
            action.OnDeregister(this);
        }
    }

    public void RemoveTriggeredAction(TriggeredAction oldAction)
    {
        int index = (int)oldAction.procTrigger + (int)oldAction.procTarget * procTriggerCount;
        if(triggeredActions[index].actions.Contains(oldAction))
        {
            triggeredActions[index].actions.Remove(oldAction);
            oldAction.OnDeregister(this);
        }
    }

    public void RemoveTriggeredAction(TriggeredAction[] oldActions)
    {
        if(oldActions == null) return;
        for(int i = 0; i < oldActions.Length; i++)
        {
            RemoveTriggeredAction(oldActions[i]);
        }
    }

    public void RemoveTriggeredAction(List<TriggeredAction> oldActions)
    {
        if(oldActions == null) return;
        for(int i = 0; i < oldActions.Count; i++)
        {
            RemoveTriggeredAction(oldActions[i]);
        }
    }

    public void RemoveAction(Ability[] newActions)
    {
        if(newActions == null) return;
        for(int i = 0; i < newActions.Length; i++)
        {
            RemoveAction(newActions[i]);
        }
    }

    public void RemoveAction(List<Ability> newActions)
    {
        if(newActions == null) return;
        for(int i = 0; i < newActions.Count; i++)
        {
            RemoveAction(newActions[i]);
        }
    }

    public void RemoveBuff(Buff buff)
    {
        if(buffs.Contains(buff))
        {
            buffs.Remove(buff);
            buff.OnDeregister(this);
        }
    }

    public void RemoveDebuff(Debuff debuff)
    {
        if(debuffs.Contains(debuff))
        {
            debuffs.Remove(debuff);
            debuff.OnDeregister(this);
        }
    }

    public void AddEquipment(Equipment newEquip)
    {
        if(newEquip == null) return;
        equipment.Add(newEquip);
        newEquip.OnRegister(this);
    }

    public void AddEquipment(Equipment[] newEquips)
    {
        if(newEquips != null)
        {
            for(int i = 0; i < newEquips.Length; i++)
            {
                AddEquipment(newEquips[i]);
            }
        }
    }

    public void AddEquipment(List<Equipment> newEquips)
    {
        if(newEquips != null)
        {
            for(int i = 0; i < newEquips.Count; i++)
            {
                AddEquipment(newEquips[i]);
            }
        }
    }

    public void RemoveEquipment(Equipment equip)
    {
        if (equipment.Contains(equip))
        {
            equipment.Remove(equip);
            equip.OnDeregister(this);
        }
    }

    public void RemoveAllEquipment()
    {
        for(int i = equipment.Count - 1; i >= 0; i--)
        {
            RemoveEquipment(equipment[i]);
        }
    }

    public void RemoveAllTalents()
    {
        for(int i = talents.Count - 1; i >= 0; i--)
        {
            RemoveTalent(talents[i]);
        }
    }

    public void RemoveAllEffects()
    {
        RemoveAllBuffs();
        RemoveAllDebuffs();
    }

    public void RemoveAllBuffs()
    {
        for(int i = buffs.Count - 1; i >= 0; i--)
        {
            RemoveBuff(buffs[i]);
        }
    }

    public void RemoveAllDebuffs()
    {
        for(int i = debuffs.Count - 1; i >= 0; i--)
        {
            RemoveDebuff(debuffs[i]);
        }
    }

    public void RemoveAllActions()
    {
        for(int i = actions.Count - 1; i >= 0; i--)
        {
            RemoveAction(actions[i]);
        }
    }

    public void UpdateTalentCount()
    {
        talentCount = talents.Count;
    }

    public void AddTalent(Talent newTalent)
    {
        if(newTalent == null) return;
        talents.Add(newTalent);
        newTalent.OnRegister(this);
        UpdateTalentCount();
    }

    public void AddTalent(Talent[] newTalents)
    {
        if(newTalents == null) return;
        for(int i = 0; i < newTalents.Length; i++)
        {
            AddTalent(newTalents[i]);
        }
    }

    public void AddTalent(List<Talent> newTalents)
    {
        if(newTalents == null) return;
        for(int i = 0; i < newTalents.Count; i++)
        {
            AddTalent(newTalents[i]);
        }
    }

    public void RemoveTalent(Talent talent)
    {
        if(talents.Contains(talent))
        {
            talents.Remove(talent);
            talent.OnDeregister(this);            
            UpdateTalentCount();
        }
    }

    public virtual void AddRaceClass(RaceClass raceClass)
    {
        AddRaceClass(raceClass, 1);
    }

    public virtual void AddRaceClass(RaceClass raceClass, float multiplier)
    {
        if(raceClass == null) return;
        raceClasses.Add(raceClass);
        if(raceClass.staticStats != null) AdjustStats(raceClass.staticStats, multiplier);
        if(raceClass.abilities != null) AddAction(raceClass.GenerateAbilities());
        if(raceClass.triggeredActions != null) AddTriggeredAction(raceClass.GenerateTriggeredActions());
        if(raceClass.unitResourceAugments != null) SetBaseResourceAugments(raceClass.unitResourceAugments);
    }

    public virtual void AddRaceClass(List<RaceClass> raceClasses)
    {
        AddRaceClass(raceClasses, 1);
    }

    public virtual void AddRaceClass(List<RaceClass> raceClasses, float multiplier)
    {
        if(raceClasses == null) return;
        for(int i = 0; i < raceClasses.Count; i++)
        {
            AddRaceClass(raceClasses[i], multiplier);
        }
    }


    public void AddBlessing(Boon newBlessing)
    {
        if(newBlessing == null) return;
        blessings.Add(newBlessing);
        newBlessing.OnRegister(this);
    }

    public void AddBlessing(List<Boon> newBlessings)
    {
        if(newBlessings != null)
        {
            for(int i = 0; i < newBlessings.Count; i++)
            {
                AddBlessing(newBlessings[i]);
            }
        }
    }

    public void RemoveBlessing(Boon blessing)
    {
        if(blessing == null) return;
        if (blessings.Contains(blessing))
        {
            blessings.Remove(blessing);
            blessing.OnDeregister(this);
        }
    }

    public void RemoveAllBlessings()
    {
        for(int i = blessings.Count - 1; i >= 0; i--)
        {
            RemoveBlessing(blessings[i]);
        }
    }

    public void AddSpell(Boon newSpell)
    {
        if(newSpell == null) return;
        spells.Add(newSpell);
        newSpell.OnRegister(this);
    }

    public void AddSpell(List<Boon> newSpells)
    {
        if(newSpells != null)
        {
            for(int i = 0; i < newSpells.Count; i++)
            {
                AddSpell(newSpells[i]);
            }
        }
    }

    public void RemoveSpell(Boon spell)
    {
        if(spell == null) return;
        if (spells.Contains(spell))
        {
            spells.Remove(spell);
            spell.OnDeregister(this);
        }
    }

    public void RemoveAllSpells()
    {
        for(int i = spells.Count - 1; i >= 0; i--)
        {
            RemoveSpell(spells[i]);
        }
    }

    #endregion

    #region Prepare/Apply

    public void PrepareAction(Action action, Unit actionTarget)
    {    
        bool isSupport = action.IsSupport();
        bool isAccurate = !isSupport && (!action.IsMissable() || GetAccuracyCheck(action, actionTarget));
        bool isCrit = isAccurate && action.IsCrittable() && GetCritCheck(action);
        if(isSupport) action.result = ActionResult.support;
        else if(isCrit) action.result = ActionResult.crit;
        else if(isAccurate) action.result = ActionResult.hit;
        else action.result = ActionResult.miss;

        action.PassActionResult();

        if(isAccurate) action.damages = PrepareDamage(action.damages, action.GetStats(), action.proficiencyType);
        if(isAccurate) action.damages = AddPreDamageConversions(action.damages);
        action.selfDamages = PrepareSelfDamage(action.selfDamages, action.GetStats(), action.proficiencyType);
        action.selfDamages = AddPreSelfDamageConversions(action.selfDamages);
        action.buffs = PrepareBuff(action.buffs, action.GetStats(), action.proficiencyType);
        action.debuffs = PrepareDebuff(action.debuffs, action.GetStats(), action.proficiencyType);

        if(actionTarget != null) action = actionTarget.ApplyAction(action, this);

        
        ApplyAttackEffects(action, actionTarget);

        ApplyBuff(action.buffs);
        action.selfDamages = AddPostSelfDamageConversions(action.selfDamages);
        ApplySelfDamage(action.selfDamages);

        RaiseOnActionUsed(action);
           
    }
    
    public Action ApplyAction(Action action, Unit actionOwner)
    {

        bool isDodge = action.IsDodgeable() && GetDodgeCheck();
        bool isBlock = action.IsBlockable() && GetBlockCheck();
        bool isReflect = action.IsReflectable() && GetReflectCheck();

        if(action.result != ActionResult.support)
        {
            if(isReflect) action.result = ActionResult.reflect;  
            else if(isDodge) action.result = ActionResult.dodge;
            else if(isBlock) action.result = ActionResult.block;
        }

        ApplyPreAttackEffects(action);

        ApplyDefenseEffects(action, actionOwner);

        if(ShouldApplyDamage(action.result)) ApplyDamage(action.damages);
        if(Utility.IsNonEmpty(action.debuffs) && ShouldApplyHitEffects(action.result)) ApplyDebuff(action.debuffs);

        RaiseOnActionTaken(action);
        
        return action;
    }

    public void ApplyDamage(Damage[] damages)
    {
        if(!Utility.IsNonEmpty(damages)) return;

        for (int i = 0; i < damages.Length; i++)
        {
            ApplyDamage(damages[i]);
        }
    }

    public void ApplyDamage(Damage damage)
    {   
        StatType damageStat = GetDamageStat(damage.type) + 1;
        float damageValue = damage.value * GetResistanceFactor(damageStat);
        if(damage.type == DamageType.healing) damageValue = -Utility.RandomRound(damageValue);
        else damageValue = Utility.RandomRound(damageValue);
        damage.value = damageValue;
        TakeDamage(damage);
    }

    public void ApplyDodge()
    {
        
    }

    public void ApplyBlock()
    {
        
    }

    public void ApplyReflect(Action action, Unit reflectTarget)
    {
        if(reflectTarget == null) return;
        Action reflectedAction = action.Clone();
        reflectedAction.result = ActionResult.hit;
        reflectTarget.ApplyAction(reflectedAction, this);
    }

    public void ApplyThorns(Damage[] damages, Unit thornsTarget)
    {
        if(Utility.IsNonEmpty(damages))
        {
            for(int i = 0; i <  damages.Length; i++)
            {
                ApplyThorns(damages[i], thornsTarget);
            }
        }
    }

    public void ApplyThorns(Damage damage, Unit thornsTarget)
    {
        if(thornsTarget == null) return;
        if(damage.type != DamageType.physical) return;
        float factor = GetConversionFactor(StatType.Thorns);
        damage.value = damage.value * factor;
        damage.type = DamageType.physical;
        thornsTarget.ApplyDamage(damage);

    }

    public void ApplyDisplacement(Action action)
    {
        bool willMiss = (action.result == ActionResult.miss || action.result == ActionResult.dodge);
        if(willMiss) return;
        if(GetResourceCount(UnitResourceType.displacement) > 0)
        {
            action.result = ActionResult.miss;
            AdjustResource(UnitResourceType.displacement, -1);
        }
    }

    public void ApplyPreAttackEffects(Action action)
    {
        ApplyDisplacement(action);
    }

    public void ApplyAttackEffects(Action action, Unit target)
    {
        bool hitEffects = ShouldApplyHitEffects(action.result);
        ApplyActionTriggers(action, ProcTarget.target, target);
        if(hitEffects)
            ApplyDamageTypeTriggers(action, ProcTarget.target, target);
        
        ApplyProficiencyTypeTrigger(action.proficiencyType, ProcTarget.target, action.usedTriggers, target);        
        ApplySelfDamageTypeTriggers(action, ProcTarget.self, target);
        //apply triggered proceduress
        ApplyCleanse(action.cleanseCount);
        if(Utility.IsNonEmpty(action.buffs)) 
            ApplyTrigger(ProcTrigger.onBuff, ProcTarget.self, action.usedTriggers, target);
        if(hitEffects && Utility.IsNonEmpty(action.debuffs)) 
            ApplyTrigger(ProcTrigger.onDebuff, ProcTarget.target, action.usedTriggers, target);
        if(Utility.IsNonEmpty(action.unitResources))
            AdjustResource(action.unitResources);

    }

    public void ApplyDefenseEffects(Action action, Unit target)
    {
        bool hitEffects = ShouldApplyHitEffects(action.result);
        ApplyActionTriggers(action, ProcTarget.self, target);
        if(hitEffects)
            ApplyDamageTypeTriggers(action, ProcTarget.self, target);
        ApplyProficiencyTypeTrigger(action.proficiencyType, ProcTarget.self, action.usedTriggers, target);
        ApplySelfDamageTypeTriggers(action, ProcTarget.target, target);
        //apply triggered procedures
        if(hitEffects) 
            ApplyPurge(action.purgeCount);
        if(hitEffects)
            ApplyThorns(action.damages, target);
        if(Utility.IsNonEmpty(action.buffs)) 
            ApplyTrigger(ProcTrigger.onBuff, ProcTarget.target, action.usedTriggers, target);
        if(hitEffects && Utility.IsNonEmpty(action.debuffs)) 
            ApplyTrigger(ProcTrigger.onDebuff, ProcTarget.self, action.usedTriggers, target);
        if(action.result == ActionResult.reflect)
            ApplyReflect(action, target);
    }

    public void ApplyActionTriggers(Action action, ProcTarget procTarget, Unit target)
    {
        ActionResult result = action.result;
        switch(result)
        {
            case ActionResult.miss:
                ApplyTrigger(ProcTrigger.onMiss, procTarget, action.usedTriggers, target);
                break;
            case ActionResult.hit:            
                ApplyTrigger(ProcTrigger.onHit, procTarget, action.usedTriggers, target);
                ApplyTrigger(ProcTrigger.onNonCrit, procTarget, action.usedTriggers, target);
                break;
            case ActionResult.crit:
                ApplyTrigger(ProcTrigger.onHit, procTarget, action.usedTriggers, target);
                ApplyTrigger(ProcTrigger.onCrit, procTarget, action.usedTriggers, target);
                break;
            case ActionResult.dodge:
                ApplyTrigger(ProcTrigger.onDodge, procTarget, action.usedTriggers, target);
                break;
            case ActionResult.block:
                ApplyTrigger(ProcTrigger.onBlock, procTarget, action.usedTriggers, target);
                break;
            case ActionResult.support:
                break;
        }
    }

    public void ApplySelfDamageTypeTriggers(Action action, ProcTarget procTarget, Unit target)
    {
        List<DamageType> damageTypes = action.GetDamageTypes(ProcTarget.self);
        for(int i = 0; i < damageTypes.Count; i++)
        {
            ApplyDamageTypeTrigger(damageTypes[i], procTarget, action.usedTriggers, target);
        }
    }

    public void ApplyDamageTypeTriggers(Action action, ProcTarget procTarget, Unit target)
    {

        List<DamageType> damageTypes = action.GetDamageTypes(ProcTarget.target);
        
        for(int i = 0; i < damageTypes.Count; i++)
        {
            ApplyDamageTypeTrigger(damageTypes[i], procTarget, action.usedTriggers, target);
        }

    }

    public void ApplyDamageTypeTrigger(DamageType type, ProcTarget procTarget, bool[] usedTriggers, Unit target)
    {
        switch(type)
        {
            case DamageType.arcane:
                ApplyTrigger(ProcTrigger.onArcane, procTarget, usedTriggers, target);
                break;
            case DamageType.physical:
                ApplyTrigger(ProcTrigger.onPhysical, procTarget, usedTriggers, target);
                break;
            case DamageType.poison:
                ApplyTrigger(ProcTrigger.onPoison, procTarget, usedTriggers, target);
                break;
            case DamageType.bleed:
                ApplyTrigger(ProcTrigger.onBleed, procTarget, usedTriggers, target);
                break;
            case DamageType.healing:
                ApplyTrigger(ProcTrigger.onHeal, procTarget, usedTriggers, target);
                break;
            case DamageType.fire:
                ApplyTrigger(ProcTrigger.onFire, procTarget, usedTriggers, target);
                break;
            case DamageType.cold:
                ApplyTrigger(ProcTrigger.onCold, procTarget, usedTriggers, target);
                break;
            case DamageType.dark:
                ApplyTrigger(ProcTrigger.onDark, procTarget, usedTriggers, target);
                break;
        }
    }

    public void ApplyProficiencyTypeTrigger(ProficiencyType type, ProcTarget procTarget, bool[] usedTriggers, Unit target)
    {
        switch(type)
        {
            case ProficiencyType.blunt:
                ApplyTrigger(ProcTrigger.onBlunt, procTarget, usedTriggers, target);
                break;
            case ProficiencyType.pierce:
                ApplyTrigger(ProcTrigger.onPierce, procTarget, usedTriggers, target);
                break;
            case ProficiencyType.spell:
                ApplyTrigger(ProcTrigger.onSpell, procTarget, usedTriggers, target);
                break;
            case ProficiencyType.curse:
                ApplyTrigger(ProcTrigger.onCurse, procTarget, usedTriggers, target);
                break;
            case ProficiencyType.tech:
                ApplyTrigger(ProcTrigger.onTech, procTarget, usedTriggers, target);
                break;
            case ProficiencyType.support:
                ApplyTrigger(ProcTrigger.onSupport, procTarget, usedTriggers, target);
                break;
        }
    }

    public Damage[] AddPreDamageConversions(Damage[] damages)
    {
        List<Damage> toExtend = new List<Damage>();

        toExtend.AddRange(GetDamageConversions(damages, StatType.DamageToBleed, DamageType.physical, DamageType.bleed));
        toExtend.AddRange(GetDamageConversions(damages, StatType.DamageToPoison, DamageType.physical, DamageType.poison));

        return Utility.ExtendArrayByList(damages, toExtend);
    }

    public Damage[] AddPostDamageConversions(Damage[] damages)
    {
        List<Damage> toExtend = new List<Damage>();

        return Utility.ExtendArrayByList(damages, toExtend);
    }

    public Damage[] AddPreSelfDamageConversions(Damage[] damages)
    {
        List<Damage> toExtend = new List<Damage>();

        return Utility.ExtendArrayByList(damages, toExtend);
    }

    public Damage[] AddPostSelfDamageConversions(Damage[] damages)
    {
        List<Damage> toExtend = new List<Damage>();

        toExtend.AddRange(GetDamageConversions(damages, StatType.Lifesteal, DamageType.physical, DamageType.healing));

        return Utility.ExtendArrayByList(damages, toExtend);
    }

    public List<Damage> GetDamageConversions (Damage[] damages, StatType conversionStat, DamageType convertFrom, DamageType convertTo)
    {
        List<Damage> toAdd = new List<Damage>();        
        Damage toCheck;

        for(int i = 0; i < damages.Length; i++)
        {
            toCheck = GetDamageConversion(damages[i], conversionStat, convertFrom, convertTo);
            if(toCheck.value != 0) toAdd.Add(toCheck);
        }

        return toAdd;

    }

    public Damage GetDamageConversion(Damage damage, StatType conversionStat, DamageType convertFrom, DamageType convertTo)
    {
        if(damage.type == convertFrom)
        {
            float factor = GetConversionFactor(conversionStat);
            if(damage.IsHit() && factor > 0)
                return ReadyDamageConversion(damage, factor, convertTo);
        }
        return new Damage() { value = 0 };
    }

    public Damage ReadyDamageConversion(Damage damage, float factor, DamageType convertedType)
    {
        Damage toReturn = new Damage()
        {
            type = convertedType,
            value = damage.value * factor,
            result = ActionResult.hit,
            damageFactor = damage.damageFactor,
        };
        return toReturn;
    }

    public List<Buff> ApplyPurge(int purgeCount)
    {
        List<Buff> toReturn = new List<Buff>();

        Buff toCheck = null;
        for(int i = 0; i < purgeCount; i++)
        {
            if(CanPurge()) toCheck = ApplyPurge();
            if(toCheck != null) toReturn.Add(toCheck);
        }

        return toReturn;
    }

    public Buff ApplyPurge()
    {
        Buff purgedEffect = null;

        if(!CanPurge()) return purgedEffect;
        int index = UnityEngine.Random.Range(0, buffs.Count);
        purgedEffect = buffs[index];
        RemoveBuff(buffs[index]);

        RaiseOnPurge();

        return purgedEffect;

    }

    public bool CanPurge()
    {
        return buffs.Count > 0;
    }

    public List<Debuff> ApplyCleanse(int cleanseCount)
    {
        List<Debuff> toReturn = new List<Debuff>();

        Debuff toCheck = null;
        for(int i = 0; i < cleanseCount; i++)
        {
            if(CanCleanse()) toCheck = ApplyCleanse();
            if(toCheck != null) toReturn.Add(toCheck);
        }

        return toReturn;
    }

    public Debuff ApplyCleanse()
    {
        Debuff cleansedEffect = null;

        if(!CanCleanse()) return cleansedEffect;
        int index = UnityEngine.Random.Range(0, debuffs.Count);
        cleansedEffect = debuffs[index];
        RemoveDebuff(debuffs[index]);

        RaiseOnDispel();

        return cleansedEffect;
    }

    public bool CanCleanse()
    {
        return debuffs.Count > 0;
    }

    public void ApplyBuff(Buff[] buffs)
    {
        if(buffs != null)
        {
            for(int i = 0; i < buffs.Length; i++)
            {
                ApplyBuff(buffs[i]);
            }
        }
    }

    public void ApplyBuff(Buff buff)
    {
        if(buff != null) ApplyBuffEffects(buff);
    }

    public void ApplyBuffEffects(Buff buff)
    {
        AddBuff(buff);
    }
    
    public void ApplyDebuff(Debuff[] debuffs)
    {
        if(debuffs != null)
        {
            for(int i = 0; i < debuffs.Length; i++)
            {
                ApplyDebuff(debuffs[i]);
            }
        }
    }

    public void ApplyDebuff(Debuff debuff)
    {
        if(debuff == null) return;
        debuff.duration = debuff.duration / GetResilienceFactor();
        AddDebuff(debuff);
    }

    public void ApplyTrigger(ProcTrigger procTrigger, ProcTarget procTarget, bool[] usedTriggers, Unit target)
    {
        int triggerIndex = procTriggerCount * (int)procTarget + (int)procTrigger;
        
        if(triggeredActions == null || triggerIndex >= triggeredActions.Length) return;

        for(int i = 0; i < triggeredActions[triggerIndex].actions.Count; i++)
        {
            triggeredActions[triggerIndex].actions[i].Trigger(this, target, usedTriggers);
        }
            
    
    }

    public Damage PrepareDamage(Damage damage, StatManager stats, ProficiencyType proficiencyType)
    {
        return PrepareDamage(damage, stats, proficiencyType, 1);
    }

    public Damage PrepareDamage(Damage damage, ProficiencyType proficiencyType)
    {
        return PrepareDamage(damage, statManager, proficiencyType, 1);
        
    }

    public Damage PrepareDamage(Damage damage, StatManager stats, ProficiencyType proficiencyType, float externalScale)
    {
        return ScaleDamage(damage, stats, proficiencyType, externalScale);
    }
    
    public Damage[] PrepareDamage(Damage[] damages, StatManager stats, ProficiencyType proficiencyType, float externalScale)
    {
        Damage[] toReturn = new Damage [0];

        if(Utility.IsNonEmpty(damages))
        {
            toReturn = new Damage[damages.Length];

            for(int i = 0; i < damages.Length; i++)
            {
                toReturn[i] = (PrepareDamage(damages[i], stats, proficiencyType, externalScale));
            }
        }        
        
        return toReturn;
    }

    public Damage[] PrepareDamage(Damage[] damages, ProficiencyType proficiencyType)
    {
        return PrepareDamage(damages, statManager, proficiencyType, 1);
    }
    
    public Damage[] PrepareDamage(Damage[] damages, ProficiencyType proficiencyType, float externalScale)
    {
        return PrepareDamage(damages, statManager, proficiencyType, externalScale);
    }

    public Damage[] PrepareDamage(Damage[] damages, StatManager stats, ProficiencyType proficiencyType)
    {
        return PrepareDamage(damages, stats, proficiencyType, 1);
    }
    
    public Damage PrepareSelfDamage(Damage damage, ProficiencyType proficiencyType)
    {
        return PrepareSelfDamage(damage, statManager, proficiencyType, 1);
        
    }

    public Damage PrepareSelfDamage(Damage damage, StatManager stats, ProficiencyType proficiencyType)
    {
        return PrepareSelfDamage(damage, stats, proficiencyType, 1);
        
    }

    public Damage PrepareSelfDamage(Damage damage, StatManager stats, ProficiencyType proficiencyType, float externalScale)
    {
        Damage toApply = ScaleDamage(damage, stats, proficiencyType, externalScale);
        return toApply;
        
    }

    public Damage[] PrepareSelfDamage(Damage[] damages, StatManager stats, ProficiencyType proficiencyType, float externalScale)
    {
        Damage[] toReturn = new Damage[0];

        if(Utility.IsNonEmpty(damages))
        {
            toReturn = new Damage[damages.Length];

            for(int i = 0; i < damages.Length; i++)
            {
                toReturn[i] = PrepareSelfDamage(damages[i], stats, proficiencyType, externalScale);
            }
        }

        return toReturn;
    }

    public Damage[] PrepareSelfDamage(Damage[] damages, ProficiencyType proficiencyType)
    {
        return PrepareSelfDamage(damages, statManager, proficiencyType, 1);
    }

    public Damage[] PrepareSelfDamage(Damage[] damages, StatManager stats, ProficiencyType proficiencyType)
    {
        return PrepareSelfDamage(damages, stats, proficiencyType, 1);
    }

    public void ApplySelfDamage(Damage[] damages)
    {
        if(Utility.IsNonEmpty(damages))
        {
            for(int i = 0; i < damages.Length; i++)
            {
                ApplySelfDamage(damages[i]);
            }
        }
    }

    public void ApplySelfDamage(Damage damage)
    {
        float damageValue = damage.value;
        if(damage.type == DamageType.healing) damageValue = -Mathf.Round(damageValue);
        else damageValue = Mathf.Round(damageValue);
        damage.value = damageValue;
        TakeDamage(damage);
    }

    public Damage ScaleDamage(Damage damage, ProficiencyType proficiencyType)
    {
        return ScaleDamage(damage, statManager, proficiencyType, 1);
    }

    public Damage ScaleDamage(Damage damage, StatManager stats, ProficiencyType proficiencyType)
    {
        return ScaleDamage(damage, stats, proficiencyType, 1);
    }

    public Damage ScaleDamage(Damage damage, StatManager stats, ProficiencyType proficiencyType, float externalScale)
    {
        Damage toReturn = new Damage()
        {
            type = damage.type,
            result = damage.result
        };

        StatType damageStat = GetDamageStat(damage.type);
        float flat = stats.GetStat(damageStat, ModifierType.Flat);
        float mult = 1 + stats.GetStat(damageStat, ModifierType.Multiplier);
        float globalMult = 1 + stats.GetStat(damageStat, ModifierType.GlobalMultiplier);
        float proficiencyMult = GetProficiencyFactor(proficiencyType);
        float critMult = GetCritFactor(damage.result == ActionResult.crit);

        toReturn.value = (damage.value - damage.value * damage.damageFactor + (damage.value + flat) * damage.damageFactor * globalMult * mult * proficiencyMult) * critMult * externalScale;

        return toReturn;
    }

    public Damage[] ScaleDamage(Damage[] damages, ProficiencyType proficiencyType)
    {
        return ScaleDamage(damages, statManager, proficiencyType, 1);
    }

    public Damage[] ScaleDamage(Damage[] damages, StatManager stats, ProficiencyType proficiencyType)
    {
        return ScaleDamage(damages, stats, proficiencyType, 1);
    }

    public Damage[] ScaleDamage(Damage[] damages, ProficiencyType proficiencyType, float externalScale)
    {
        return ScaleDamage(damages, statManager, proficiencyType, externalScale);
    }

    public Damage[] ScaleDamage(Damage[] damages, StatManager stats, ProficiencyType proficiencyType, float externalScale)
    {
        if(damages == null) return null;
        Damage[] toReturn = new Damage[damages.Length];
        for(int i = 0; i < damages.Length; i++)
        {
            toReturn[i] = ScaleDamage(damages[i], stats, proficiencyType, externalScale);
        }

        return toReturn;
    }

    public float GetProficiencyFactor(ProficiencyType proficiencyType)
    {
        float factor = .0048f;
        StatType statType = GetProficiencyStat(proficiencyType);
        float statValue = statManager.GetStat(statType);

        return (1 + factor * statValue);
    }

    public StatType GetProficiencyStat(ProficiencyType proficiencyType)
    {
        switch(proficiencyType)
        {
            case ProficiencyType.blunt:
                return StatType.BluntProficiency;
            case ProficiencyType.pierce:
                return StatType.PierceProficiency;
            case ProficiencyType.spell:
                return StatType.SpellProficiency;
            case ProficiencyType.support:
                return StatType.SupportProficiency;
            case ProficiencyType.tech:
                return StatType.TechProficiency;
            case ProficiencyType.curse:
                return StatType.CurseProficiency;
            default:
                break;
        }
        Debug.LogWarning("Proficiency Stat Not Found");
        return 0;
    }

    public ProficiencyType GetProficiencyProficiencyType(StatType proficiencyStat)
    {
         switch(proficiencyStat)
        {
            case StatType.BluntProficiency:
                return ProficiencyType.blunt;
            case StatType.PierceProficiency:
                return ProficiencyType.pierce;
            case StatType.SpellProficiency:
                return ProficiencyType.spell;
            case StatType.SupportProficiency:
                return ProficiencyType.support;
            case StatType.TechProficiency:
                return ProficiencyType.tech;
            case StatType.CurseProficiency:
                return ProficiencyType.curse;
            default:
                break;
        }
        Debug.LogWarning("Proficiency Stat Not Found");
        return 0;
    }

    public Buff PrepareBuff(Buff buff, StatManager stats, ProficiencyType proficiencyType)
    {
        Buff clone = (Buff)buff.Clone();
        clone.finalDamages = ScaleDamage(clone.finalDamages, stats, proficiencyType);
        clone.periodicDamages = ScaleDamage(clone.periodicDamages, stats, proficiencyType);
        clone.duration = clone.duration * GetResilienceFactor(stats);
        return clone;
    }

    public Buff[] PrepareBuff(Buff[] buffs, StatManager stats, ProficiencyType proficiencyType)
    {
        Buff[] toReturn = new Buff[0];

        if(Utility.IsNonEmpty(buffs))
        {
            toReturn = new Buff[buffs.Length];

            for(int i = 0; i < buffs.Length; i++)
            {
                toReturn[i] = PrepareBuff(buffs[i], stats, proficiencyType);
            }
        }

        return toReturn;            
    }

    public Debuff PrepareDebuff(Debuff debuff, StatManager stats, ProficiencyType proficiencyType)
    {
        Debuff clone = (Debuff)debuff.Clone();
        clone.finalDamages = ScaleDamage(clone.finalDamages, stats, proficiencyType);
        clone.periodicDamages = ScaleDamage(clone.periodicDamages, stats, proficiencyType);
        clone.duration = clone.duration * GetAfflictionFactor(stats);
        return clone;
    }

    public Debuff[] PrepareDebuff(Debuff[] debuffs, StatManager stats, ProficiencyType proficiencyType)
    {
        Debuff[] toReturn = new Debuff[0];

        if(Utility.IsNonEmpty(debuffs))
        {
            toReturn = new Debuff[debuffs.Length];

            for(int i = 0; i < debuffs.Length; i++)
            {
                toReturn[i] = PrepareDebuff(debuffs[i], stats, proficiencyType);
            }
        }

        return toReturn;
    }

    #endregion

    public void AdjustHP(int amount)
    {
        currentHP += amount;
        ClampHP();
    }

    public void ClampHP()
    {
        currentHP = Mathf.Clamp(currentHP, 0, statManager.GetStat(StatType.Hitpoints));
    }

    public void TakeDamage(Damage damage)
    {        
        if(active)
        {
            AdjustHP(-(int)damage.value);
            RaiseOnDamage(damage);

            if(!IsAlive()) TryPotion();
            if(!IsAlive()) Die(damage);            
        }
    }

    public void Heal(float amount)
    {
        if(amount <= 0) return;

        TakeDamage(new Damage() {
            type = DamageType.healing,
            result = ActionResult.hit,
            value = -amount,
        });
    }

    public void TryPotion()
    {
        if(potion == null) return;

        if(potion.CanUse())
        {
            potion.Use(this);
            RaiseOnPotionUsed(potion);
        }        
    }

    public int GetPotionCount()
    {
        if(potion == null) return 0;

        return potion.maxCount;
    }

    public float GetCritChance()
    {
        float chanceFactor = .0048f;
        float chanceValue = chanceFactor * statManager.GetStat(StatType.CritChance);

        return chanceValue;
    }

    public bool GetCritCheck()
    {
        float r = UnityEngine.Random.Range(0, 1.0f);
        float roll = GetCritChance();

        return r < roll;
    }

    public float GetCritChance(Action action)
    {
        float chanceFactor = .0048f;
        float chanceValue = chanceFactor * action.statManager.GetStat(StatType.CritChance);

        return chanceValue;
    }

    public bool GetCritCheck(Action action)
    {
        float r = UnityEngine.Random.Range(0, 1.0f);
        float roll = GetCritChance(action);

        return r < roll;
    }

    public float GetCritFactor(bool isCrit)
    {
        
        float multFactor = .0048f;        
        float multValue = 1.5f + multFactor * statManager.GetStat(StatType.CritMultiplier);

        return (isCrit) ? multValue : 1;
        
    }

    public float GetConversionFactor(StatType type)
    {
        float value = statManager.GetStat(type);
        float factor = .0048f;

        return Mathf.Clamp((value * factor), 0, (value * factor));
    }

    public float GetLifestealFactor()
    {
        return GetLifestealFactor(statManager);
    }

    public float GetLifestealFactor(StatManager stats)
    {
        float value = stats.GetStat(StatType.Lifesteal);
        float factor = .0048f;

        return Mathf.Clamp((value * factor), 0, (value * factor));
    }

    public float GetSpeedFactor()
    {
        return GetHasteFactor() * GetWeightFactor();
    }

    public float GetHasteFactor()
    {
        return GetHasteFactor(statManager);
    }

    public float GetHasteFactor(StatManager stats)
    {
        float value = statManager.GetStat(StatType.Haste);
        float factor = (value > 0) ? Sigmoid(value, 120, 1.1f) : -Sigmoid(-value, 120, 1.1f);
        float total = 1 + factor;
        return total;
    }

    public float GetWeightFactor()
    {
        return GetWeightFactor(statManager);
    }

    public float GetWeightFactor(StatManager stats)
    {
        float value = stats.GetStat(StatType.Weight);
        float factor = (value > 0) ? Sigmoid(value, 40, 1.1f) : -Sigmoid(-value, 40, 1.1f);
        float total = 1 - factor;
        return total;
    }

    public float GetAfflictionFactor()
    {
        return GetAfflictionFactor(statManager);
    }

    public float GetAfflictionFactor(StatManager stats)
    {
        float value = stats.GetStat(StatType.Affliction);
        float factor = (value > 0) ? Sigmoid(value, 120, 1.1f) : -Sigmoid(-value, 120, 1.1f);
        float total = 1 + factor;
        return total;
    }

    public float GetResilienceFactor()
    {
        return GetResilienceFactor(statManager);
    }

    public float GetResilienceFactor(StatManager stats)
    {
        float value = stats.GetStat(StatType.Resilience);
        float factor = (value > 0) ? Sigmoid(value, 120, 1.1f) : -Sigmoid(-value, 120, 1.1f);
        float total = 1 + factor;
        return total;
    }

    public float GetPathfindingFactor()
    {
        float value = statManager.GetStat(StatType.Pathfinding);
        float factor = (value > 0) ? Sigmoid(value, 50, 1.1f) : -Sigmoid(-value, 50, 1.1f);
        float total = 1 + factor;
        return total;
    }

    public float GetWalkSpeedFactor()
    {
        return GetPathfindingFactor() * GetWeightFactor();
    }

    public float GetResistanceFactor(StatType type)
    {
        float value = statManager.GetStat(type);
        float midPoint = (type == StatType.Armor) ?  80 : 50;
        float factor = (value > 0) ? Sigmoid(value, midPoint, 1.1f) : -Sigmoid(-value, midPoint, 1.1f);
        float total = 1 - factor;
        return total;

    }
    
    public float GetResistanceFactor(DamageType type)
    {
        StatType statType = GetDamageStat(type) + 1;
        return GetResistanceFactor(statType);
    }

    public bool GetReflectCheck()
    {
        float roll = UnityEngine.Random.Range(0, 1.0f);
        float check = GetReflectRoll();
        return roll < check;
    }

    public float GetReflectRoll()
    {
        float value = statManager.GetStat(StatType.Reflect);
        float factor = (value > 0) ? Sigmoid(value, 120, 1.1f) : 0;
        return factor;
    }

    public bool GetAccuracyCheck(Action action, Unit target)
    {
        float r = UnityEngine.Random.Range(0, 1.0f);
        float roll = GetAccuracyRoll(action, target);
        return r < roll;
    }

    public virtual float GetAccuracyRoll(Action action, Unit target)
    {
        if(target is Enemy)
        {
            Enemy enemy = target as Enemy;
            float accuracyLevel = enemy.GetLevel() - action.statManager.GetStat(StatType.Accuracy);
            float inverseFactor = (accuracyLevel > 0) ? Sigmoid(accuracyLevel, 60, 1.1f) : 0;
            float factor = 1 - inverseFactor;
            return factor;
        }

        else return 1.0f;
    }

    public bool GetAccuracyCheck(Unit target)
    {
        float r = UnityEngine.Random.Range(0, 1.0f);
        float roll = GetAccuracyRoll(target);
        return r < roll;
    }

    public virtual float GetAccuracyRoll(Unit target)
    {
        if(target is Enemy)
        {
            Enemy enemy = target as Enemy;
            float accuracyLevel = enemy.GetLevel() - statManager.GetStat(StatType.Accuracy);
            float inverseFactor = (accuracyLevel > 0) ? Sigmoid(accuracyLevel, 60, 1.1f) : 0;
            float factor = 1 - inverseFactor;
            return factor;
        }

        else return 1.0f;
    }

    public bool GetDodgeCheck()
    {
        float r = UnityEngine.Random.Range(0, 1.0f);
        float roll = GetDodgeRoll();
        return r < roll;
    }

    public float GetDodgeRoll()
    {
        float value = statManager.GetStat(StatType.Dodge);
        float factor = (value > 0) ? Sigmoid(value, 120, 1.1f) : 0;
        return factor;
    }

    public bool GetBlockCheck()
    {
        float r = UnityEngine.Random.Range(0, 1.0f);
        float roll = GetBlockRoll();
        return r < roll;
    }

    public float GetBlockRoll()
    {
        float value = statManager.GetStat(StatType.Block);
        float factor = (value > 0) ? Sigmoid(value, 120, 1.1f) : 0;
        return factor;
    }

    public float Sigmoid(float value, float constant, float power)
    {
        float v = Mathf.Pow(value, power);
        float c = Mathf.Pow(constant, power);

        return (v / (v + c));
    }

    public void Die(Damage killingBlow)
    {
        if(active)
        {
            active = false;
            RaiseOnDeath(killingBlow);
        }
    }

    public bool IsAlive()
    {
        return currentHP > 0;
    }

    public virtual void ResetStats()
    {
       statManager.ZeroStats();
    }

    public void InitializeTriggeredActions()
    {
        
        triggeredActions = new ActionList[procTriggerCount * procTargetCount];
        
        for(int i = 0; i < triggeredActions.Length; i++)
        {
            triggeredActions[i] = new ActionList();
        }
    }

    public void ResetHitpoints()
    {
        currentHP = statManager.GetStat(StatType.Hitpoints);
    }

    public StatType GetDamageStat(DamageType type)
    {
        switch(type)
        {
            case DamageType.bleed:
                return StatType.Bleed;
            case DamageType.poison:
                return StatType.Poison;
            case DamageType.physical:
                return StatType.Damage;
            case DamageType.healing:
                return StatType.Healing;
            case DamageType.arcane:
                return StatType.Arcane;
            case DamageType.cold:
                return StatType.Cold;
            case DamageType.dark:
                return StatType.Dark;
            case DamageType.fire:
                return StatType.Fire;
            

        }
        Debug.LogWarning("Unsupported Damage Type");
        return StatType.Damage;
    }
    
    public List<RaceClass> GetRaceClasses()
    {
        return raceClasses;
    }

    // public void SetTarget(Unit newTarget)
    // {
    //     target = newTarget;
    // }

    // public void ClearTarget()
    // {
    //     target = null;
    // }

    public float GetCurrentHitpoints()
    {
        return Mathf.Ceil(currentHP);
    }

    public float GetHealthPercentage()
    {
        float maxHP = statManager.GetStat(StatType.Hitpoints);
        return maxHP > 0 ? currentHP / maxHP : 0;
    }

    public int GetBuffCount()
    {
        return buffs.Count;
    }

    public int GetDebuffCount()
    {
        return debuffs.Count;
    }

    public int GetResourceCount(UnitResource resource)
    {
        return GetResourceCount(resource.GetResourceType());
    }

    public int GetResourceCount(UnitResourceType type)
    {
        return resourceManager.GetResourceCount(type);
    }

    public List<UnitResource> GetResourceList()
    {
        if(resourceManager == null) return null;
        return resourceManager.GetResourceList();
    }

    public List<UnitResource> GetResourceAvailableList()
    {
        if(resourceManager == null) return null;
        return resourceManager.GetResourceAvailableList();
    }

    public float GetMaxHitpoints()
    {
        return Mathf.Ceil(statManager.GetStat(StatType.Hitpoints));
    }

    public void AdjustResource(List<UnitResourceAdder> toAdd)
    {
        for(int i = 0; i < toAdd.Count;  i++)
        {
            AdjustResource(toAdd[i]);
        }

    }

    public void AdjustResource(UnitResourceType resource, int value)
    {
        if(resourceManager.CheckChange(resource, value))
        {
            resourceManager.AdjustResource(resource, value);
            RaiseOnUnitResourceChanged();
        }
    }

    public void AdjustResource(UnitResourceAdder unitResource)
    {
        AdjustResource(unitResource.type, unitResource.value);
    }

    public void AddResourceAugment(UnitResourceAugment augment)
    {
        resourceManager.AddResourceAugment(augment);
    }

    public void RemoveResourceAugment(UnitResourceAugment augment)
    {
        resourceManager.RemoveResourceAugment(augment);
    }

    public void AddResourceAugments(List<UnitResourceAugment> augments)
    {
        if(augments == null) return;
        for(int i = 0; i < augments.Count; i++)
        {
            AddResourceAugment(augments[i]);
        }
    }

     public void RemoveResourceAugments(List<UnitResourceAugment> augments)
    {
        if(augments == null) return;
        for(int i = 0; i < augments.Count; i++)
        {
            RemoveResourceAugment(augments[i]);
        }
    }

    public void SetBaseResourceAugments(List<UnitResourceAugment> augments)
    {
        if(augments == null) return;
        for(int i = 0; i < augments.Count; i++)
        {
            SetBaseResourceAugment(augments[i]);
        }
    }

    public void SetBaseResourceAugment(UnitResourceAugment augment)
    {
        resourceManager.SetBaseResourceAugment(augment); 
    }

    public void RaiseOnDeath(Damage killingBlow)
    {
        if(onDeath != null) onDeath(killingBlow);
    }

    public void RaiseOnDamage(Damage damage)
    {
        if(onDamage != null) onDamage(damage);
    }

    public void RaiseOnUnitResourceChanged()
    {
        if(onUnitResourceChanged != null) onUnitResourceChanged();
    }

    public void RaiseOnBuffAdded(Buff buff)
    {
        if(onBuffAdded != null) onBuffAdded(buff);
    }

    public void RaiseOnDebuffAdded(Debuff debuff)
    {
        if(onDebuffAdded != null) onDebuffAdded(debuff);
    }

    public void RaiseOnActionUsed(Action action)
    {
        if(onActionUsed != null) onActionUsed(action);
    }

    public void RaiseOnActionTaken(Action action)
    {
        if(onActionTaken != null) onActionTaken(action);
    }    

    public void RaiseOnPotionUsed(Potion potion)
    {
        if(onPotionUsed != null) onPotionUsed(potion);
    }    

    public void RaiseOnEscape()
    {
        if(onEscape != null) onEscape();
    }

	public void RaiseOnResourceAdded(UnitResource resource)
	{
		if(onResourceAdded != null) onResourceAdded(resource);
	}

    public void RaiseOnStealth()
    {
        if(onStealth != null) onStealth();
    }

    public void RaiseOnPurge()
    {
        if(onPurge != null) onPurge();
    }

    public void RaiseOnDispel()
    {
        if(onDispel != null) onDispel();
    }

    public bool ShouldApplyDamage(ActionResult result)
    {
        return result == ActionResult.hit || result == ActionResult.crit;
    }

    public bool ShouldApplyHitEffects(ActionResult result)
    {
        return result == ActionResult.hit || result == ActionResult.crit || result == ActionResult.block;
    }

    // public Unit GetTarget()
    // {
    //     return target;
    // }

    public StatManager GetStats()
    {
        return statManager;
    }

    public float GetStat(StatType type, ModifierType mod)
    {
        return statManager.GetStat(type, mod);
    }

    public float GetStat(StatType type)
    {
        return statManager.GetStat(type);
    }

    public void AdjustStats(StatType type, ModifierType mod, float amount)
    {
        float hpDif = statManager.AdjustStats(type, mod, amount);
        if(hpDif > 0) AdjustHP(Mathf.CeilToInt(hpDif));
    }

    public void AdjustStats(EquipStat toAdjust)
    {
        float hpDif = statManager.AdjustStats(toAdjust);        
        if(hpDif > 0) AdjustHP(Mathf.CeilToInt(hpDif));
    }

    public void AdjustStats(EquipStat[] toAdjust)
    {
       float hpDif = statManager.AdjustStats(toAdjust);
        if(hpDif > 0) AdjustHP(Mathf.CeilToInt(hpDif));
    }

    public void AdjustStats(List<EquipStat> toAdjust)
    {
        float hpDif = statManager.AdjustStats(toAdjust);
        if(hpDif > 0) AdjustHP(Mathf.CeilToInt(hpDif));
    }

    public void AdjustStats(StatManager manager)
    {
        float hpDif = statManager.AdjustStats(manager);
        if(hpDif > 0) AdjustHP(Mathf.CeilToInt(hpDif));
    }

    public void AdjustStats(EquipStat toAdjust, float factor)
    {
        float hpDif = statManager.AdjustStats(toAdjust, factor);
        if(hpDif > 0) AdjustHP(Mathf.CeilToInt(hpDif));
    }

    public void AdjustStats(EquipStat[] toAdjust, float factor)
    {
        float hpDif = statManager.AdjustStats(toAdjust, factor);
        if(hpDif > 0) AdjustHP(Mathf.CeilToInt(hpDif));
    }

    public void AdjustStats(List<EquipStat> toAdjust, float factor)
    {
        float hpDif = statManager.AdjustStats(toAdjust, factor);
        if(hpDif > 0) AdjustHP(Mathf.CeilToInt(hpDif));
    }

    public void AdjustStats(StatManager toAdjust, float factor)
    {
        float hpDif = statManager.AdjustStats(toAdjust, factor);
        if(hpDif > 0) AdjustHP(Mathf.CeilToInt(hpDif));
    }

    public bool IsDefault()
    {
        return GetMaxHitpoints() == 0;
    }

    public string GetName()
    {
        string toReturn = "";

        foreach(RaceClass raceClass in raceClasses)
        {
            toReturn += raceClass.GetName() + " ";
        }

        return toReturn;
    }
}