﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class StatManager 
{
	public float[] stats;
    
    [NonSerialized]
    private UnitResourceManager unitResourceManager;
    private int statTypeLength = Enum.GetNames(typeof(StatType)).Length;
	private int modifierTypeLength = Enum.GetNames(typeof(ModifierType)).Length;

	public StatManager() 
	{
		ZeroStats();
	}
	
	public void ZeroStats()
	{
        stats = new float[statTypeLength * modifierTypeLength];
	}

    public void SetUnitResourceManager(UnitResourceManager newManager)
    {
        unitResourceManager = newManager;
    }

    public UnitResourceManager GetUnitResourceManager()
    {
        return unitResourceManager;
    }

    public List<EquipStat> GetNonZeroStats()
    {
        return GetNonZeroStats(1);
    }

    public List<EquipStat> GetNonZeroStats(float factor)
    {
        List<EquipStat> toReturn = new List<EquipStat>();

        for(int i = 0; i < statTypeLength; i++)
        {
            for(int j = 0; j < modifierTypeLength; j++)
            {
                if(GetStat(i, j) != 0) toReturn.Add(new EquipStat{                    
                    StatType = (StatType) i,
                    ModifierType = (ModifierType) j,
                    Value = factor * GetStat(i, j)
                });
            }
        }

        return toReturn;
    }

    public List<EquipStat> GetFactorOrderedStats(StatEquivalencies statEquivalencies, float factor)
    {
        List<EquipStat> toReturn = new List<EquipStat>();

        for(int i = 0; i < statTypeLength; i++)
        {
            for(int j = 0; j < modifierTypeLength; j++)
            {
                if(GetStat(i, j) != 0) 
                {
                    EquipStat temp = new EquipStat{                    
                        StatType = (StatType) i,
                        ModifierType = (ModifierType) j,
                        Value = (factor * GetStat(i, j)) / statEquivalencies.GetFactor((StatType) i, (ModifierType)j)
                    };

                    int addAt = toReturn.Count;

                    for(int k = 0; k < toReturn.Count; k++)
                    {
                        if(toReturn[k].Value < temp.Value)
                        {
                            addAt = k;
                            break;
                        }
                    }

                    toReturn.Insert(addAt, temp);
                }
            }
        }

        for(int i = 0; i < toReturn.Count; i++)
        {
            toReturn[i] = statEquivalencies.FactorStat(toReturn[i]);
        }

        return toReturn;
    }
    

	public void SetStats(float[] newStats)
	{
		stats = newStats;
	}

    private int GetIndex(int type, int mod)
    {
        return type * modifierTypeLength + mod;
    }

    private float GetStat(int type, int mod)
    {
        return stats[GetIndex(type, mod)];
    }

    private void SetStat(int type, int mod, float value)
    {
        stats[GetIndex(type, mod)] = value;
    }

    private void AdjustStat(int type, int mod, float value)
    {
        stats[GetIndex(type, mod)] += value;
    }

 	public float GetStat(StatType statType)
    {
        float[] resourceStat = (unitResourceManager == null) ? new float[modifierTypeLength] : unitResourceManager.GetStat(statType);
        float mult = 1 + GetStat((int)statType, (int)ModifierType.Multiplier) + resourceStat[(int)ModifierType.Multiplier];
        float globalMult = 1 + GetStat((int)statType, (int)ModifierType.GlobalMultiplier) + resourceStat[(int)ModifierType.GlobalMultiplier];
        float flat = GetStat((int)statType, (int)ModifierType.Flat) + resourceStat[(int)ModifierType.Flat];

        float total = mult * globalMult * flat;

        switch(statType)
        {
            case StatType.Hitpoints:
                total += GetStat(StatType.Constitution);
                break;
            case StatType.Resilience:
                total += GetStat(StatType.Constitution);
                break;
            case StatType.CritChance:
                total += GetStat(StatType.Dexterity);
                break;
            case StatType.Dodge:
                total += GetStat(StatType.Dexterity);
                break;
            case StatType.PierceProficiency:
                total += GetStat(StatType.Dexterity);
                break;
            case StatType.Weight:
                total = Mathf.Clamp(total - GetStat(StatType.Strength), 0, total - GetStat(StatType.Strength));
                break;
            case StatType.BluntProficiency:
                total += GetStat(StatType.Strength);
                break;
            case StatType.TechProficiency:
                total += GetStat(StatType.Intelligence);
                break;
            case StatType.SpellProficiency:
                total += GetStat(StatType.Intelligence);
                break;
            case StatType.CurseProficiency:
                total += GetStat(StatType.Intelligence);
                break;
            case StatType.SupportProficiency:
                total += GetStat(StatType.Intelligence);
                break;            
        }

        return total;

    }

	public float GetStat(StatType statType, ModifierType modifierType)
    {
        float total = GetStat((int)statType, (int)modifierType);
        total += (unitResourceManager == null) ? 0 : unitResourceManager.GetStat(statType, modifierType);

        switch(statType)
        {
            case StatType.Hitpoints:
                total += GetStat(StatType.Constitution, modifierType);
                break;
            case StatType.Resilience:
                total += GetStat(StatType.Constitution, modifierType);
                break;
            case StatType.CritChance:
                total += GetStat(StatType.Dexterity, modifierType);
                break;
            case StatType.Dodge:
                total += GetStat(StatType.Dexterity, modifierType);
                break;
            case StatType.PierceProficiency:
                total += GetStat(StatType.Dexterity, modifierType);
                break;
            case StatType.Weight:
                total = Mathf.Clamp(total - GetStat(StatType.Strength, modifierType), 0, total - GetStat(StatType.Strength, modifierType));
                break;
            case StatType.BluntProficiency:
                total += GetStat(StatType.Strength, modifierType);
                break;
            case StatType.TechProficiency:
                total += GetStat(StatType.Intelligence, modifierType);
                break;
            case StatType.SpellProficiency:
                total += GetStat(StatType.Intelligence, modifierType);
                break;
            case StatType.CurseProficiency:
                total += GetStat(StatType.Intelligence, modifierType);
                break;
            case StatType.SupportProficiency:
                total += GetStat(StatType.Intelligence, modifierType);
                break;            
        }

        return total;
    }

	public int GetDamageIndex(DamageType type)
    {
        switch((int)type)
        {
            case (int)DamageType.bleed:
                return (int)StatType.Bleed;
            case (int)DamageType.poison:
                return (int)StatType.Poison;
            case (int)DamageType.physical:
                return (int)StatType.Damage;
            case (int)DamageType.healing:
                return (int)StatType.Healing;
            case (int)DamageType.arcane:
                return (int)StatType.Arcane;
            case (int)DamageType.cold:
                return (int)StatType.Cold;
            case (int)DamageType.dark:
                return (int)StatType.Dark;
            case (int)DamageType.fire:
                return (int)StatType.Fire;
            

        }
        Debug.LogWarning("Unsupported Damage Type");
        return -1;
    }

	public float AdjustStats(StatType type, ModifierType mod, float amount)
    {
        bool isHP = type == StatType.Hitpoints;
        float prevHP = 0;
        float hpDif = 0;
        if(isHP) prevHP = GetStat(StatType.Hitpoints);

        AdjustStat((int)type, (int)mod, amount);

        if(isHP) hpDif = GetStat(StatType.Hitpoints) - prevHP;

        return hpDif;


    }

    public float AdjustStats(EquipStat toAdjust)
    {
        return AdjustStats(toAdjust.StatType, toAdjust.ModifierType, toAdjust.Value);
    }

    public float AdjustStats(EquipStat[] toAdjust)
    {
        float hpDif = 0;

        if (toAdjust != null)
        {
            for (int i = 0; i < toAdjust.Length; i++)
            {
                hpDif += AdjustStats(toAdjust[i]);
            }
        }

        return hpDif;
    }

    public float AdjustStats(List<EquipStat> toAdjust)
    {
        float hpDif = 0;

        if(toAdjust != null)
        {
            for(int i = 0; i < toAdjust.Count; i++)
            {
                hpDif += AdjustStats(toAdjust[i]);
            }
        }

        return hpDif;
    }

    public float AdjustStats(EquipStat toAdjust, float factor)
    {
        return AdjustStats(toAdjust.StatType, toAdjust.ModifierType, factor * toAdjust.Value);
    }

    public float AdjustStats(EquipStat[] toAdjust, float factor)
    {
        float hpDif = 0;

        if (toAdjust != null)
        {
            for (int i = 0; i < toAdjust.Length; i++)
            {
                hpDif += AdjustStats(toAdjust[i], factor);
            }
        }

        return hpDif;
    }

    public float AdjustStats(List<EquipStat> toAdjust, float factor)
    {
        float hpDif = 0;

        if (toAdjust != null)
        {
            for (int i = 0; i < toAdjust.Count; i++)
            {
                hpDif += AdjustStats(toAdjust[i], factor);
            }
        }

        return hpDif;
    }

    public float AdjustStats(StatManager manager)
    {
        return AdjustStats(manager, 1);
    }

    public float AdjustStats(StatManager manager, float factor)
    {
        float hpDif = 0;
        float prevHP = GetStat(StatType.Hitpoints);

        float[] newStats = manager.stats;
        
        for(int i = 0; i < newStats.Length; i++)
        {
            stats[i] += factor * newStats[i];
        }

        hpDif = GetStat(StatType.Hitpoints) - prevHP;

        return hpDif;
    }

    public void MergeStats(StatManager statManager)
    {
        List<EquipStat> mergingStats = statManager.GetNonZeroStats();

        foreach(EquipStat stat in mergingStats)
        {
            int typeIndex = (int)stat.StatType;
            int modIndex = (int)stat.ModifierType;
            float value = stat.Value;

            if(GetStat(typeIndex, modIndex) != 0)
            {
                AdjustStats(stat);
                statManager.AdjustStats(stat, -1);
            }
        }
    }
}
