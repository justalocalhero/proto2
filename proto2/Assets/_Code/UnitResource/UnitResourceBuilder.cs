﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitResourceBuilder 
{
	private UnitResource resource;

	public UnitResourceBuilder Begin()
	{
		resource = new UnitResource();
		return this;
	}

	public UnitResourceBuilder SpriteIndex(int spriteIndex)
	{
		resource.SetSpriteIndex(spriteIndex);
		return this;
	}

	public UnitResourceBuilder Type(UnitResourceType type)
	{
		resource.SetType(type);
		return this;
	}

	public UnitResourceBuilder BaseCap(int baseCap)
	{
		resource.SetBaseCap(baseCap);
		return this;
	}

	public UnitResourceBuilder AbsoluteCap(int absoluteCap)
	{
		resource.SetAbsoluteCap(absoluteCap);
		return this;
	}

	public UnitResourceBuilder EquipStats(List<EquipStat> stats)
	{
		resource.SetEquipStats(stats);
		return this;
	}

	public UnitResource Build()
	{
		return resource;
	}
}
