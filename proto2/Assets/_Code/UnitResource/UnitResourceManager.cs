﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class UnitResourceManager 
{
	public List<UnitResource> resources;
	public int modifierTypeLength = Enum.GetNames(typeof(ModifierType)).Length;
	public int statTypeLength = Enum.GetNames(typeof(StatType)).Length;

	public delegate void OnResourceAdded(UnitResource resource);
	public OnResourceAdded onResourceAdded;

	public void RaiseOnResourceAdded(UnitResource resource)
	{
		if(onResourceAdded != null) onResourceAdded(resource);
	}

	public UnitResourceManager()
	{
		resources = UnitResourceDictionary.GenerateEmpty();
	}

	public UnitResourceManager(UnitResourceDictionary dictionary)
	{
		resources = dictionary.Generate();
	}

	public bool CheckChange(UnitResourceType type, int value)
	{
		if(!CheckResource(type)) return false;
		
		return resources[(int)type].CheckChange(value);
	}

	public void AdjustResource(UnitResourceAdder resourceAdder)
	{
		AdjustResource(resourceAdder.type, resourceAdder.value);
	}

	public void AdjustResource(UnitResourceType type, int value)
	{
		if(!CheckResource(type)) return;
	
		UnitResource resource = resources[(int)type];

		bool raiseOnAdded = (resource.GetValue() == 0 && value > 0);

		resources[(int)type].AdjustValue(value);

		if(raiseOnAdded) RaiseOnResourceAdded(resource);
	}

	public void AdjustCap(UnitResourceType type, int value)
	{
		if(!CheckResource(type)) return;

		resources[(int)type].AdjustCap(value);
	}

	public void AddResourceAugment(UnitResourceAugment augment)
	{
		AddResourceStats(augment.type, augment.stats);
		AdjustCap(augment.type, augment.cap);
	}

	public void RemoveResourceAugment(UnitResourceAugment augment)
	{
		RemoveResourceStats(augment.type, augment.stats);
		AdjustCap(augment.type, -augment.cap);
	}

	public void SetBaseResourceAugment(UnitResourceAugment augment)
	{
		AddResourceStats(augment.type, augment.stats);
		MergeCap(augment.type, augment.cap);
	}

	public void MergeCap(UnitResourceType type, int value)
	{
		if(!CheckResource(type)) return;

		resources[(int)type].MergeCap(value);
	}

	public void AddResourceStats(UnitResourceType type, EquipStat[] stats)
	{
		if(!CheckResource(type)) return;

		resources[(int)type].AddStats(stats);
	}

	public void RemoveResourceStats(UnitResourceType type, EquipStat[] stats)
	{
		if(!CheckResource(type)) return;

		resources[(int)type].RemoveStats(stats);
	}

	public int GetResourceCount(UnitResourceType type)
	{
		if(!CheckResource(type)) return -1;

		return resources[(int)type].GetValue();
	}

	private bool CheckResource(UnitResourceType type)
	{
		if((int) type >= resources.Count)
		{
			Debug.LogWarning("Unit resource out of range: " + type);
			
			return false;
		}

		return true;
	}

	public void MaxStat(UnitResourceType type)
	{
		if(!CheckResource(type)) return;

		int oldValue = resources[(int)type].value;

		resources[(int)type].Max();		

		if(oldValue != resources[(int)type].value)
			RaiseOnResourceAdded(resources[(int)type]);
	}

	public void ZeroStat(UnitResourceType type)
	{
		if(!CheckResource(type)) return;

		int oldValue = resources[(int)type].value;

		resources[(int)type].Zero();

		if(oldValue != resources[(int)type].value)
			RaiseOnResourceAdded(resources[(int)type]);
	}

	public List<UnitResource> GetResourceList()
	{
		List<UnitResource> toReturn = new List<UnitResource>();

		for(int i = 0; i < resources.Count; i++)
		{
			if(resources[i].GetValue() > 0) toReturn.Add(resources[i]);
		}

		return toReturn;
	}

	public List<UnitResource> GetResourceAvailableList()
	{
		List<UnitResource> toReturn = new List<UnitResource>();

		for(int i = 0; i < resources.Count; i++)
		{
			if(resources[i].GetCap() > 0) toReturn.Add(resources[i]);
		}

		return toReturn;
	}

	public float GetStat(StatType statType, ModifierType modifierType)
    {
		if(resources == null) return 0;
		float total = 0;
        for(int i = 0; i < resources.Count; i++)
		{
			total += resources[i].GetStat(statType, modifierType);
		}

		return total;
    }

	public float[] GetStat(StatType statType)
	{
		float[] toReturn = new float[modifierTypeLength];
		float[] temp;
		if(resources == null) return toReturn;
		for(int i = 0; i < resources.Count; i++)
		{
			temp = resources[i].GetStat(statType);
			for(int j = 0; j < temp.Length; j++)
			{
				if(j < toReturn.Length) toReturn[j] += temp[j];
			}
		}

		return toReturn;
	}

	public float[][] GetStats()
	{
		float[][] toReturn = new float[statTypeLength][];
		UnitResource currentResource;
		List<EquipStat> currentStats;
		EquipStat currentStat;

		for(int i = 0; i < toReturn.Length; i++)
		{
			toReturn[i] = new float[modifierTypeLength];
		}

		for(int i = 0; i < resources.Count; i++)
		{
			currentResource = resources[i];
			currentStats = currentResource.GetStats();

			for(int j = 0; j < currentStats.Count; j++)
			{
				currentStat = currentStats[j];
				toReturn[(int)currentStat.StatType][(int)currentStat.ModifierType] += (currentStat.Value * currentResource.GetValue());
			}
		}

		return toReturn;
	}

	public void Reset()
	{
		for(int i = 0; i < resources.Count; i++)
		{
			resources[i].Zero();
		}
	}
}
