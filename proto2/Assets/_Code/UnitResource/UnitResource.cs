﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class UnitResource 
{
	public UnitResourceType type;
	public int baseCap, currentCap, absoluteCap, value;
	public List<EquipStat> stats = new List<EquipStat>();
	private int modifierTypeLength = Enum.GetNames(typeof(ModifierType)).Length;
	public int spriteIndex;

	public delegate void OnResourceChanged(int value);
	public OnResourceChanged onResourceChanged;

	public void RaiseOnChanged()
	{
		if(onResourceChanged != null) onResourceChanged(value);
	}

	public void SetType(UnitResourceType type)
	{
		this.type = type;
	}

	public void SetBaseCap(int baseCap)
	{
		this.baseCap = baseCap;
		this.currentCap = baseCap;
	}

	public void SetSpriteIndex(int spriteIndex)
	{
		this.spriteIndex = spriteIndex;
	}

	public void SetAbsoluteCap(int absoluteCap)
	{
		this.absoluteCap = absoluteCap;
	}

	public void SetEquipStats(List<EquipStat> stats)
	{
		this.stats = new List<EquipStat>();
		for(int i = 0; i < stats.Count; i++)
		{
			this.stats.Add(stats[i]);
		}
	}

	public void AddStats(EquipStat[] stats)
	{
		if(stats == null) return;
		for(int i = 0; i < stats.Length; i++)
		{
			AddStat(stats[i]);
		}
	}

	public void AddStat(EquipStat stat)
	{
		stats.Add(stat);
	}

	public void RemoveStats(EquipStat[] stats)
	{
		if(stats == null) return;
		for(int i = 0; i < stats.Length; i++)
		{
			RemoveStat(stats[i]);
		}
	}

	public void RemoveStat(EquipStat stat)
	{
		if(stats.Contains(stat)) stats.Remove(stat);
	}

	public UnitResourceType GetResourceType()
	{
		return type;
	}

    public int GetSprite()
    {
        return spriteIndex;
    }
	
	public int GetBaseCap()
	{
		return baseCap;
	}

	public int GetAbsoluteCap()
	{
		return absoluteCap;
	}

	public List<EquipStat> GetStats()
	{
		return stats;
	}

	public float GetStat(StatType statType, ModifierType modifierType)
    {
		float total = 0;
		for(int i = 0; i < stats.Count; i++)
		{
			if(stats[i].StatType == statType && stats[i].ModifierType == modifierType) total += (stats[i].Value * value);
		}
        return total;
    }

	public float[] GetStat(StatType type)
	{
		float[] toReturn = new float[modifierTypeLength];

		for(int i = 0; i < stats.Count; i++)
		{
			if(stats[i].StatType == type)
				toReturn[(int)stats[i].ModifierType] += (stats[i].Value * value);
		}

		return toReturn;

	}

	public void AdjustCap(int change)
	{
		currentCap += change;
	}

	public void AdjustValue(int change)
	{
		int oldValue = value;

		value += change;
		value = Mathf.Clamp(value, 0, currentCap);
		value = Mathf.Clamp(value, 0, absoluteCap);

		if(oldValue != value)
			RaiseOnChanged();
	}

	public void MergeCap(int cap)
	{
		if(currentCap < cap) currentCap = cap;
		currentCap = Mathf.Clamp(currentCap, 0, absoluteCap);
	}

	public bool CheckChange(int change)
	{
		int toCheck = value + change;
		return (toCheck >= 0);

	}
	
	public int GetCap()
	{
		return currentCap;
	}

	public int GetValue()
	{
		return value;
	}

	public void Zero()
	{
		value = 0;

		RaiseOnChanged();
	}

	public void Max()
	{
		AdjustValue(currentCap);
	}
}

public enum UnitResourceType {sneak, rage, endurance, defiance, superiority, dominance, displacement, madness,}
