﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BattleTest
{
	public class ClassSelector : MonoBehaviour 
	{
		public Button playerButton, enemyButton;

		public PlayerClassSelector playerClassSelector;
		public EnemyClassSelector enemyClassSelector;

		public void Awake()
		{
			playerButton.onClick.AddListener(PlayerSelect);
			enemyButton.onClick.AddListener(EnemySelect);
		}

		public void PlayerSelect()
		{
			playerClassSelector.Show();
			enemyClassSelector.Hide();
		}

		public void EnemySelect()
		{
			enemyClassSelector.Show();
			playerClassSelector.Hide();
		}
	}
}