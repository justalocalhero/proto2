﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace BattleTest
{
	public class BattleReportViewer : MonoBehaviour 
	{
		public FloatReference hsvPowHigh, hsvPowLow, hsvPowConstant, hsvValue;
		public Transform slotContainer;
		public ColorViewer slotPrototype;
		public Button left, right;
		public BattleReport battleReport;
		public TextMeshProUGUI labelMesh;
		public Tooltips.TextTooltip tooltip;
		private List<ColorViewer> slots;
		private int battleIndex, rows, columns;

		public void Update()
		{
			if(Input.GetKeyDown(KeyCode.U))
			{
				UpdateUI();
			}
		}

		public void Awake()
		{
			BuildSlots();
			left.onClick.AddListener(Decrement);
			right.onClick.AddListener(Increment);
		}

		public void Start()
		{
			battleIndex = battleReport.GetCount() - 1;
			UpdateUI();
		}

		public void UpdateUI()
		{
			UpdateLabel();
			UpdateSlots();
			UpdateButtons();
		}

		public void BuildSlots()
		{
			GridBuilder<ColorViewer> builder = new GridBuilder<ColorViewer>();

			slots = builder
				.Begin()
				.WithPrototype(slotPrototype)
				.WithContainer(slotContainer)
				.WithSpacing(2, 2)
				.WithPadding(4, 4)
				.Build();

			rows = builder.GetRows();
			columns = builder.GetColumns();

			RegisterDelegates();
			ResetSlots();
		}

		public void RegisterDelegates()
		{
			foreach(ColorViewer slot in slots)
			{
				slot.onEnter += HandleEnter;
				slot.onExit += HandleExit;
			}
		}

		public void HandleEnter(ColorViewer slot)
		{
			if(slot == null) return;
			BattleReport.BattleSummary selected = GetSelected();
			if(selected == null) return;
			BattleReport.FightSummary fight = selected.GetFight(slot.GetKey());
			if(fight == null) return;
			tooltip.Set(fight.GetStringRepresentation(), slot.GetRect());
		}

		public void HandleExit(ColorViewer slot)
		{
			tooltip.Hide();
		}

		public void UpdateLabel()
		{
			BattleReport.BattleSummary battle = GetSelected();

			labelMesh.SetText(
				(battle == null) ? "" : battle.GetKey()
			);
		}

		public void UpdateSlots()
		{
			ResetSlots();

			BattleReport.BattleSummary battle = GetSelected();
			if(battle == null) return;

			int currentColumn = -1;
			int currentRow = 0;

			string lastPrimary = "";

			foreach(KeyValuePair<string, BattleReport.FightSummary> fightPair in battle.fights)
			{
				BattleReport.FightSummary fight = fightPair.Value;

				if(lastPrimary != fight.primary)
				{

					currentColumn++;
					currentRow = 0;
					lastPrimary = fight.primary;
				}

				int index = GetIndex(currentColumn, currentRow++);

				if(index >= 0 && index < slots.Count)
					UpdateSlot(fightPair.Key, slots[index], fight);
			}
		}

		public void UpdateSlot(string key, ColorViewer slot, BattleReport.FightSummary fight)
		{
			float kdr = 0;
			if(fight.playerSurvivalTime == 0) kdr = 100f;
			else if(fight.enemySurvivalTime == 0) kdr = .0001f;
			else kdr = fight.playerSurvivalTime / fight.enemySurvivalTime;
			
			slot.SetKey(key);
			slot.Show();
			slot.SetColor(GetKDRColor(kdr));
		}

		public void ResetSlots()
		{
			foreach(ColorViewer slot in slots)
			{
				ResetSlot(slot);
			}
		}

		public void ResetSlot(ColorViewer slot)
		{
			slot.SetKey("");
			slot.Hide();
		}

		public BattleReport.BattleSummary GetSelected()
		{
			if(battleIndex < 0 || battleIndex >= battleReport.GetCount()) return null;

			return battleReport.GetBattle(battleIndex);
		}

		public void Increment()
		{
			battleIndex++;
			UpdateUI();
		}

		public void Decrement()
		{
			battleIndex--;
			UpdateUI();
		}

		public void UpdateButtons()
		{
			SetButtonActive(left, battleIndex > 0);
			SetButtonActive(right, battleReport.GetCount() > (battleIndex + 1));
		}

		public void SetButtonActive(Button button, bool toSet)
		{
			if(button.IsActive() != toSet) button.gameObject.SetActive(toSet);
		}

		public Color GetKDRColor(float kdr)
		{
			float h = GetH(kdr);
			float s = GetS(kdr);
			float v = GetV(kdr);

			return Color.HSVToRGB(h, s, v);
		}

		public float GetH(float kdr)
		{
			return (kdr > 1) ? .33f : 1f;
		}

		public float GetS(float kdr)
		{
			return(kdr > 1) ? (Mathf.Pow(kdr, hsvPowHigh) / (Mathf.Pow(kdr, hsvPowHigh) + Mathf.Pow(hsvPowConstant, hsvPowHigh))) : Mathf.PerlinNoise(kdr, hsvPowLow);
		}

		public float GetV(float kdr)
		{
			return hsvValue;
		}
		
		public int GetIndex(int column, int row)
		{
			int index = row * columns + column;

			return index;
		}
	}
}