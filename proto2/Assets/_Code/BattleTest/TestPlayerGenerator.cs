﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BattleTest
{
	public class TestPlayerGenerator : MonoBehaviour 
	{		
		public Button addButton, clearButton, loadButton;
		public TestBattle testBattle;
		public Outfitter outfitter;
		public UnitResourceDictionary unitResourceDictionary;
		public PlayerClassSelector playerClassSelector;
		public EquipmentManager equipmentManager;
		public Inventory inventory;
		public TalentTreeManager talentManager;
		public UnitTooltip playerTooltip;
		public Boons.BoonManager religionManager;
		public Boons.BoonManager cabalManager;
		private Player player;
		private Queue<Player> testQueue = new Queue<Player>();
		public PlayerList playerList;
		public EquipmentGenerator testEquipment; 
		
		public void Awake()
		{
			addButton.onClick.AddListener(QueueTestPlayer);
			loadButton.onClick.AddListener(QueuePlayerList);
			clearButton.onClick.AddListener(ClearPlayerQueue);
			playerClassSelector.onChanged += Reset;
			testBattle.onReset += Ship;
			outfitter.onEquipmentChanged += UpdatePlayerEquipment;

			religionManager.onChanged += UpdatePlayerBlessings;
			cabalManager.onChanged += UpdatePlayerSpells;
			equipmentManager.onEquipmentChanged += UpdatePlayerEquipment;
			talentManager.onTalentPreSelected += ClearPlayerTalents;
			talentManager.onTalentSelected += UpdatePlayerTalents;

		}

		public void Start()
		{			
			religionManager.SetAvailableTier(5);
			cabalManager.SetAvailableTier(5);
			QueuePlayerList();
		}

		public void Ship()
		{
			if(testQueue.Count > 0) testBattle.AddPlayer(testQueue.Dequeue());
			else testBattle.AddPlayer(player);
		}

		public void ClearPlayerQueue()
		{
			testQueue.Clear();
		}

		public void QueuePlayerList()
		{
			foreach(PlayerList.PlayerState playerState in playerList.GetPlayers())
			{
				testQueue.Enqueue(BuildPlayer(playerState));
			}
		}

		public Player BuildPlayer(PlayerList.PlayerState playerState)
		{
			List<RaceClass> raceClasses = playerClassSelector.GetClasses(playerState.raceClassIndeces);
			List<Talent> talents = BuildTalents(raceClasses, playerState.talentIndeces);
			Equipment equipment = outfitter.GetEquipment(playerState.equipmentIndex);

			string name = "Empty";

			for(int i = 0; i < raceClasses.Count; i++)
			{
				if(i == 0) name = raceClasses[i].GetName();
				else name += " " + raceClasses[i].GetName();
			}

			Player toReturn = new Player(name, new UnitResourceManager(unitResourceDictionary), 0);

			toReturn.AddRaceClass(raceClasses);
			toReturn.AddTalent(talents);
			toReturn.AddEquipment(equipment);

			return toReturn;
		}

		public List<Talent> BuildTalents(List<RaceClass> raceClasses, List<int> talentIndeces)
		{
			List<Talent> toReturn = new List<Talent>();

			int startPoint = 0;

			foreach(RaceClass currentClass in raceClasses)
			{
				int length = currentClass.talents.Length * 2;
				toReturn.AddRange(currentClass.GetTalentsFromIndeces(talentIndeces.GetRange(startPoint, length)));
				startPoint += length;
			}

			return toReturn;
		}

		public void QueueTestPlayer()
		{
			if(player == null) return;

			testQueue.Enqueue(player);
			
			playerList.Add(new PlayerList.PlayerState(
				playerClassSelector.GetSelectedIndeces(),
				talentManager.GetSelectedIndeces(),
				outfitter.GetSelectedIndex()
			));

			Reset();
		}

		public void Reset()
		{
			player = new Player("TestBoy", new UnitResourceManager(unitResourceDictionary), 0);
			player.SetTalentLevel(20);
			player.AddRaceClass(playerClassSelector.GetSelected());
			UpdateTalentTrees();
			UpdatePlayer();
		}
		
		public void UpdatePlayer()
		{
			UpdatePlayerEquipment();
			ClearPlayerTalents();
			UpdatePlayerTalents();
			UpdatePlayerBlessings();
			UpdatePlayerSpells();
		}
		
		public void UpdateTalentTrees()
		{
			if(player == null) return;
			if(player.raceClasses == null) return;
			talentManager.Set(player);
		}

		public void UpdatePlayerEquipment()
		{
			if(player == null) return;
			player.RemoveAllEquipment();
			player.AddEquipment(equipmentManager.equipment);
			if(outfitter.HasEquipment()) player.AddEquipment(outfitter.GetEquipment());
			UpdatePlayerTooltip();
		}

		public void ClearPlayerTalents()
		{
			if(player == null) return;
			player.RemoveAllTalents();
			UpdatePlayerTooltip();
		}

		public void UpdatePlayerTalents()
		{
			if(player == null) return;
			player.AddTalent(talentManager.GetSelected());
			UpdatePlayerTooltip();
		}

		public void UpdatePlayerBlessings()
		{
			if(player == null) return;
			player.RemoveAllBlessings();
			UpdatePlayerTooltip();
		}

		public void UpdatePlayerSpells()
		{
			if(player == null) return;
			player.RemoveAllSpells();
			UpdatePlayerTooltip();
		}

		public void UpdatePlayerTooltip()
		{
			if(player != null) playerTooltip.Set(player);
		}

		public bool HasTestQueue()
		{
			return testQueue.Count > 0;
		}

		public enum TestWeapons {physicalLow, poisonLow, bleedLow, fireLow, coldLow, arcaneLow, darkLow, physicalHigh, poisonHigh, bleedHigh, fireHigh, coldHigh, arcaneHigh, darkHigh,}
	}
}