﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace BattleTest
{
	[CreateAssetMenu(menuName="Test/PlayerList")]
	public class PlayerList : ScriptableObject 
	{
		private float minHP = 85f;
		public List<PlayerState> players;

		public void Add(PlayerState player)
		{
			if(players == null) players = new List<PlayerState>();

			players.Add(player);

			Save();
		}

		public List<PlayerState> GetPlayers()
		{
			if(players == null) players = new List<PlayerState>();

			return players;
		}		

		private void Save()
		{			
			#if UNITY_EDITOR
			EditorUtility.SetDirty(this);
			#endif
		}		

		[System.Serializable]
		public class PlayerState
		{
			public List<int> raceClassIndeces;
			public List<int> talentIndeces;
			public int equipmentIndex;

			public PlayerState(List<int> raceClassIndeces, List<int> talentIndeces, int equipmentIndex)
			{
				this.raceClassIndeces = raceClassIndeces;
				this.talentIndeces = talentIndeces;
				this.equipmentIndex = equipmentIndex;
			}
		}
	}
}