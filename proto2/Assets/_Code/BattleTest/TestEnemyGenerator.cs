﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BattleTest
{
	public class TestEnemyGenerator : MonoBehaviour 
	{
		public Button testButton;
		private bool testing = false;
		private float currentKillCount = 0;
		private int testIndex = 0;
		public float testKills;
		public TestBattle testBattle;
		public TestPlayerGenerator testPlayerGenerator;
		public EnemyClassSelector enemyClassSelector;
		public BattleReport.BattleSummary battleSummary;
		public BattleReport battleReport;
		private Enemy enemy;

		public void Awake()
		{
			testButton.onClick.AddListener(ToggleTest);
			testBattle.onReset += Ship;
			testBattle.onEnemyDeath += UpdateTest;
			testBattle.onPlayerAdded += UpdateBattleSummary;
			enemyClassSelector.onChanged += Reset;
		}

		public void ToggleTest()
		{
			testing = true;
			Debug.Log((testing) ? "Testing Activated" : "Testing Deactivated");
			if(testing) ResetTest();

		}

		public void ResetTest()
		{
			currentKillCount = 0;
			testIndex = 0;
			FetchTest();
			testBattle.Reset();
			testBattle.SetActive(true);
		}

		public void UpdateBattleSummary(Player player)
		{
			battleSummary = new BattleReport.BattleSummary(player);
		}

		public void UpdateTest()
		{
			if(!testing) return;

			if(currentKillCount++ >= testKills)
			{
				currentKillCount = 0;
				UpdateTestString();
				FetchTest();
			}
		}

		public void FetchTest()
		{
			List<RaceClass> currentClasses = enemyClassSelector.Fetch(testIndex++);
			if(currentClasses.Count == 0) 
			{
				HandleTestEnd();
			}
			else 
			{
				BuildEnemy(currentClasses);
				Ship();
			}	
		}

		public void UpdateTestString()
		{
			BattleReport.FightSummary toAdd = testBattle.GetFightSummary();
			battleSummary.AddFight(toAdd);
		}

		public void HandleTestEnd()
		{
			if(battleSummary != null) battleReport.AddBattle(battleSummary);
			if(testPlayerGenerator.HasTestQueue()) 
			{
				ResetTest();
			}
			else 
			{
				testing = false;
				testBattle.Reset();
			}
		}

		public void Ship()
		{
			testBattle.AddEnemy(enemy);
		}

		public void Reset()
		{

			BuildEnemy(enemyClassSelector.GetSelected());
		}

		public void BuildEnemy(List<RaceClass> raceClasses)
		{
			enemy = new Enemy("TestEnemy", 5, new UnitResourceManager());
			enemy.AddRaceClass(raceClasses);

		}
	}
}