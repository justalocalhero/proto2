﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Loot;

namespace BattleTest
{
	public class ClassList : MonoBehaviour 
	{
		public TextSlot slotPrototype;
		public Transform slotContainer;
		private List<TextSlot> slots;
		private List<RaceClass> classes;
		private int selected = -1;

		public delegate void OnSelect(RaceClass raceClass);
		public OnSelect onSelect;

		private void RaiseOnSelect()
		{
			if(onSelect != null) onSelect(GetSelected());
		}
		
		private void Awake()
		{
			BuildSlots();
		}

		public void Set(List<RaceClass> newClasses)
		{
			this.classes = newClasses;
			selected = -1;
			
			for(int i = 0; i < slots.Count; i++)
			{
				if(i < classes.Count) 
				{
					slots[i].SetText(classes[i].GetName());
				}
				else slots[i].Hide();
			}

			UpdateUI();


		}

		public void BuildSlots()
		{
			slots = new GridBuilder<TextSlot>()
			.Begin()
			.WithPrototype(slotPrototype)
			.WithContainer(slotContainer)
			.WithColumns(1)
			.WithSpacing(2, 2)
			.Build();

			RegisterSlots();
		}

		public void RegisterSlots()
		{
			for(int i = 0; i < slots.Count; i++)
			{
				slots[i].SetIndex(i);
				slots[i].onClicked += Select;
			}
		}

		public void Clear()
		{
			selected = -1;
			
			UpdateUI();
		}
		
		public void UpdateUI()
		{
			if(classes == null) return;

			for(int i = 0; i < slots.Count; i++)
			{
				if(i < classes.Count) 
				{
					slots[i].SetSelected(i == selected);
				}
			}
		}

		public void Select(int index)
		{
			if(selected == index) selected = -1;
			else selected = index;

			UpdateUI();
			RaiseOnSelect();
		}

		public int GetSelectedIndex()
		{
			return selected;
		}

		public RaceClass GetSelected()
		{
			return GetClass(selected);
		}

		public RaceClass GetClass(int index)
		{
			if(classes == null || index < 0 || index >= classes.Count)
				return null;

			return classes[index];

		}
	}
}
