﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace BattleTest
{
	[CreateAssetMenu(menuName="Test/BattleReport")]
	public class BattleReport : ScriptableObject 
	{
		[System.Serializable]
		public class BattleSummaryDictionary : SerializableDictionaryBase<string, BattleSummary> {};
		public BattleSummaryDictionary battles;

		public void AddBattle(BattleSummary battle)
		{
			if(battle == null) return;
			if(battles == null) battles = new BattleSummaryDictionary();
			battles[battle.GetKey()] = battle;

			Save();
		}

		public int GetCount()
		{
			return battles.Values.Count();
		}

		public BattleSummary GetBattle(int index)
		{
			if(index < 0 || index >= battles.Count) return null;

			return battles.Values.ElementAt(index);
		}	

		private void Save()
		{			
			#if UNITY_EDITOR
			EditorUtility.SetDirty(this);
			#endif
		}	

		[System.Serializable]
		public class BattleSummary
		{
			public string playerName;
			public int talentCount;
			
			[System.Serializable]
			public class FightSummaryDictionary : SerializableDictionaryBase<string, FightSummary> {};
			public FightSummaryDictionary fights;

			public BattleSummary(Player player)
			{
				playerName = player.GetName();
				talentCount = player.talentCount;
			}

			public string GetKey()
			{
				return playerName + talentCount;
			}

			public void AddFight(FightSummary fight)
			{
				if(fight == null) return;
				if(fights == null) fights = new FightSummaryDictionary();
				fights[fight.GetKey()] = fight;
			}

			public FightSummary GetFight(string key)
			{
				if(fights == null) fights = new FightSummaryDictionary();
				
				return fights[key];
			}
		}

		[System.Serializable]
		public class FightSummary
		{
			public string primary, secondary;
			public float enemySurvivalTime, playerSurvivalTime, playerDPS, enemyDPS, playerHPS, enemyHPS, healthCost;

			public FightSummary(string primary, string secondary, float playerSurvivalTime, float enemySurvivalTime, float playerDPS, float enemyDPS, float playerHPS, float enemyHPS, float healthCost)
			{
				this.primary = primary;
				this.secondary = secondary;
				this.playerSurvivalTime = playerSurvivalTime;
				this.enemySurvivalTime = enemySurvivalTime;
				this.playerDPS = playerDPS;
				this.enemyDPS = enemyDPS;
				this.playerHPS = playerHPS;
				this.enemyHPS = enemyHPS;
				this.healthCost = healthCost;
			}

			public string GetKey()
			{
				return primary + secondary;
			}

			public string GetStringRepresentation()
			{
				string toReturn = "";

				float kdr = 0;
				if(playerSurvivalTime == 0) kdr = 100f;
				else if(enemySurvivalTime == 0) kdr = 0;
				else kdr = playerSurvivalTime / enemySurvivalTime;

				toReturn += primary;
				toReturn += " " + secondary;
				toReturn += "\n\nKDR: " + Utility.Truncate(kdr, 2);
				toReturn += "\n\nPlayerSurvivalTime: " + Utility.Truncate(playerSurvivalTime, 2) + "s";
				toReturn += "\nPlayerDPS: " + Utility.Truncate(playerDPS, 2) + "/s";
				toReturn += "\nPlayerHPS: " + Utility.Truncate(playerHPS, 2) + "/s";
				toReturn += "\n\nEnemySurvivalTime: " + Utility.Truncate(enemySurvivalTime, 2) + "s";
				toReturn += "\nEnemyDPS: " + Utility.Truncate(enemyDPS, 2) + "/s";
				toReturn += "\nEnemyHPS: " + Utility.Truncate(enemyHPS, 2) + "/s";

				return toReturn;
			}
		}
	}
}