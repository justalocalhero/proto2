﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ColorViewer : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	private RectTransform rect;
	private int index = -1;
	private string key = "";
	public Image image;
	public UIToggler toggler;

	public delegate void OnEnter(ColorViewer colorViewer);
	public OnEnter onEnter;

	public delegate void OnExit(ColorViewer colorViewer);
	public OnExit onExit;
	
	public void Awake()
	{
		rect = transform as RectTransform;
	}

	public void RaiseOnEnter()
	{
		if(onEnter != null) onEnter(this);
	}

	public void RaiseOnExit()
	{
		if(onExit != null) onExit(this);
	}

	public void SetIndex(int index)
	{
		this.index = index;
	}

	public int GetIndex()
	{
		return index;
	}

	public void SetKey(string key)
	{
		this.key = key; 
	}

	public string GetKey()
	{
		return key;
	}

	public RectTransform GetRect()
	{
		return rect;
	}

	public void SetColor(Color color)
	{
		image.color = color;
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
		RaiseOnEnter();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
		RaiseOnExit();
    }

	public void Show()
	{
		toggler.Show();
	}

	public void Hide()
	{
		toggler.Hide();
	}
}
