﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Loot;

namespace BattleTest
{
	public class Outfitter : MonoBehaviour 
	{
		public SlotList slotList;
		public EquipmentGenerator equipmentGenerator;
		private Equipment[] equipments;

		public delegate void OnEquipmentChanged();
		public OnEquipmentChanged onEquipmentChanged;

		public void RaiseOnEquipmentChanged()
		{
			if(onEquipmentChanged != null) onEquipmentChanged();
		}

		public void Awake()
		{
			equipments = equipmentGenerator.GenerateAll();
			slotList.onSelected += RaiseOnEquipmentChanged;
		}

		public void Start()
		{
			SetSlots();
		}

		public void SetSlots()
		{
			List<string> slotNames = new List<string>();

			foreach(Equipment equipment in equipments)
			{
				slotNames.Add(equipment.GetName());
			}

			slotList.SetSlots(slotNames);
		}

		public bool HasEquipment()
		{
			int index = slotList.GetSelected();
			return (index >= 0 && index < equipments.Length);
			
		}

		public int GetSelectedIndex()
		{
			return slotList.GetSelected();
		}

		public Equipment GetEquipment()
		{
			return GetEquipment(slotList.GetSelected());
		}

		public Equipment GetEquipment(int index)
		{
			if(index < 0  || index >= equipments.Length) return null;

			Equipment toShip = equipments[index];

			toShip.slotMultiplier = 1;

			return toShip;
		}
	}
}