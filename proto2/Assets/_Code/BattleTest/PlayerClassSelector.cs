﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BattleTest
{
	public class PlayerClassSelector : MonoBehaviour 
	{
		public UIToggler toggler;
		public List<RaceClass> raceList, occupationList, classList;
		private List<TextSlot> encounterSlots;
		private int selected = 0;
		public ClassList races, occupations, classes;
		public RaceClassEditor raceClassEditor;

		public delegate void OnChanged();
		public OnChanged onChanged;

		private void RaiseOnChanged()
		{
			if(onChanged != null) onChanged();
		}

		public void Awake()
		{
			races.onSelect += HandleSelected;
			occupations.onSelect += HandleSelected;
			classes.onSelect += HandleSelected;
			
			races.Set(raceList);
			occupations.Set(occupationList);
			classes.Set(classList);
		}

		public void HandleSelected(RaceClass raceClass)
		{
			if(raceClass != null) raceClassEditor.Set(raceClass);
			else raceClassEditor.Clear();

			RaiseOnChanged();
		}

		public List<RaceClass> GetClasses(List<int> indeces)
		{
			List<RaceClass> toReturn = new List<RaceClass>();

			if(races.GetClass(indeces[0]) != null) toReturn.Add(races.GetClass(indeces[0]));
			if(occupations.GetClass(indeces[1]) != null) toReturn.Add(occupations.GetClass(indeces[1]));
			if(classes.GetClass(indeces[2]) != null) toReturn.Add(classes.GetClass(indeces[2]));

			return toReturn;
		}

		public List<int> GetSelectedIndeces()
		{
			List<int> toReturn = new List<int>();

			toReturn.Add(races.GetSelectedIndex());
			toReturn.Add(occupations.GetSelectedIndex());
			toReturn.Add(classes.GetSelectedIndex());

			return toReturn;
		}

		public List<RaceClass> GetSelected()
		{
			List<RaceClass> toReturn = new List<RaceClass>();

			if(races.GetSelected() != null) toReturn.Add(races.GetSelected());
			if(occupations.GetSelected() != null) toReturn.Add(occupations.GetSelected());
			if(classes.GetSelected() != null) toReturn.Add(classes.GetSelected());

			return toReturn;
		}

		public void Show()
		{
			toggler.Show();
		}

		public void Hide()
		{
			toggler.Hide();
		}
	}
}