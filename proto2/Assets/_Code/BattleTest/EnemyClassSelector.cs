﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BattleTest
{
	public class EnemyClassSelector : MonoBehaviour 
	{
		public UIToggler toggler;
		public List<EncounterGenerator> encounters;
		public TextSlot slotPrototype;
		public Transform slotContainer;
		private List<TextSlot> encounterSlots;
		private int selected = 0;
		public ClassList primary, secondary;
		public RaceClassEditor raceClassEditor;

		public delegate void OnChanged();
		public OnChanged onChanged;

		private void RaiseOnChanged()
		{
			if(onChanged != null) onChanged();
		}

		public void Awake()
		{
			primary.onSelect += HandleSelected;
			secondary.onSelect += HandleSelected;

			BuildSlots();
		}

		public void Start()
		{
			UpdateUI();
		}

		public void BuildSlots()
		{
			encounterSlots = new GridBuilder<TextSlot>()
			.Begin()
			.WithPrototype(slotPrototype)
			.WithContainer(slotContainer)
			.WithColumns(1)
			.WithRows(encounters.Count)
			.WithSpacing(2, 2)
			.Build();

			RegisterSlots();
		}

		public void RegisterSlots()
		{
			for(int i = 0; i < encounterSlots.Count; i++)
			{
				encounterSlots[i].SetIndex(i);
				encounterSlots[i].SetText(encounters[i].name);
				encounterSlots[i].onClicked += Select;
			}
		}

		public void UpdateUI()
		{
			UpdateSlots();
		}

		public void UpdateSlots()
		{
			for(int i = 0; i < encounterSlots.Count; i++)
			{
				encounterSlots[i].SetSelected(i == selected);
			}
		}

		public void Select(int index)
		{
			if(index < 0 || index >= encounters.Count) return;

			selected = index;
			SetEncounter(encounters[index]);

			UpdateUI();
			RaiseOnChanged();
		}

		public void HandleSelected(RaceClass raceClass)
		{
			if(raceClass != null) raceClassEditor.Set(raceClass);
			else raceClassEditor.Clear();

			RaiseOnChanged();
		}

		public List<RaceClass> GetSelected()
		{
			List<RaceClass> toReturn = new List<RaceClass>();

			if(primary.GetSelected() != null) toReturn.Add(primary.GetSelected());
			if(secondary.GetSelected() != null) toReturn.Add(secondary.GetSelected());

			return toReturn;
		}

		public void SetEncounter(EncounterGenerator encounterGenerator)
		{
			if(encounterGenerator == null) return;

			List<RaceClass> primaryClasses = new List<RaceClass>();
			List<RaceClass> secondaryClasses = new List<RaceClass>();

			foreach(EncounterEnemyGenerator enemyGenerator in encounterGenerator.enemies)
			{
				foreach(GeneratorRaceClass current in enemyGenerator.enemyGenerator.primaryRaceClass)
				{
					primaryClasses.Add(current.raceClass);
				}

				foreach(GeneratorRaceClass current in enemyGenerator.enemyGenerator.secondaryRaceClass)
				{
					secondaryClasses.Add(current.raceClass);
				}
			}

			primary.Set(primaryClasses);
			secondary.Set(secondaryClasses);
		}

		public void Show()
		{
			toggler.Show();
		}

		public void Hide()
		{
			toggler.Hide();
		}

		public List<RaceClass> Fetch(int index)
		{
			int remaining = index;

			List<RaceClass> toReturn = new List<RaceClass>();

			for(int i = 0; i < encounters.Count; i++)
			{
				EncounterGenerator encounterGenerator = encounters[i];

				foreach(EncounterEnemyGenerator enemyGenerator in encounterGenerator.enemies)
				{
					int primaryCount = enemyGenerator.enemyGenerator.primaryRaceClass.Length;
					int secondaryCount = enemyGenerator.enemyGenerator.secondaryRaceClass.Length;
					int totalCount = primaryCount * secondaryCount;

					if(remaining >= totalCount) 
					{
						remaining -=  totalCount;
					}
					else if(primaryCount > 0 && secondaryCount > 0)
					{
						int selectedPrimary = remaining / secondaryCount;
						int selectedSecondary = remaining % secondaryCount;

						toReturn.Add(enemyGenerator.enemyGenerator.primaryRaceClass[selectedPrimary].raceClass);
						toReturn.Add(enemyGenerator.enemyGenerator.secondaryRaceClass[selectedSecondary].raceClass);

						return toReturn;
					}
				}
			}
			
			return toReturn;
		}
	}
}