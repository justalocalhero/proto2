﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;

namespace BattleTest
{
	public class TestBattle : MonoBehaviour 
	{
		public Button battleButton, pauseButton;
		public float speed;
		public float failTime;
		public TextMeshProUGUI playerStatus;
		public TextMeshProUGUI enemyStatus;

		private Player player;
		private Enemy enemy;
		bool active = false;

		float playerBattleStartHealth = 0;
		float playerBattleHealthLoss = 0;
		float timeActive = 0;
		float currentBattleTime = 0;
		float enemyDamageTotal = 0;
		float enemyHealingTotal = 0;
		float[] enemyDamage = new float[Enum.GetNames(typeof(DamageType)).Length];
		int enemyDeaths = 0;
		float playerDamageTotal = 0;
		float playerHealingTotal = 0;
		float[] playerDamage = new float[Enum.GetNames(typeof(DamageType)).Length];
		int playerDeaths = 0;
		float kdr = 0;

		public delegate void OnPlayerAdded(Player player);
		public OnPlayerAdded onPlayerAdded;

		public delegate void OnReset();
		public OnReset onReset;

		public delegate void OnEnemyDeath();
		public OnEnemyDeath onEnemyDeath;

		public void RaiseOnUpdate()
		{
			if(onReset != null) onReset();
		}

		public void RaiseOnEnemyDeath()
		{
			if(onEnemyDeath != null) onEnemyDeath();
		}

		public void RaiseOnPlayerAdded(Player player)
		{
			if(onPlayerAdded != null) onPlayerAdded(player);
		}
		
		public void Awake()
		{
			battleButton.onClick.AddListener(Fire);
			pauseButton.onClick.AddListener(ToggleActive);
		}

		public void Update()
		{
			if(active) 
			{
				float deltaTime = Time.deltaTime;
				float remainingTime = speed * deltaTime;

				while(remainingTime >= deltaTime)
				{
					remainingTime -= deltaTime;
					UpdateTime(deltaTime);
				}

				if(remainingTime > 0)
				{
					UpdateTime(remainingTime);
				}
			}        
		}

		public void UpdateTime(float sampleTime)
		{
			if(player != null && enemy != null) player.UpdateTimers(sampleTime, enemy);
			if(enemy != null && player != null) enemy.UpdateTimers(sampleTime, player);
			timeActive += sampleTime;
			currentBattleTime += sampleTime;

			if(currentBattleTime >= failTime) FailOut();

		}

		public void Fire()
		{
			Reset();
			SetActive(true);
		}

		public void Reset()
		{
			ResetStats();
			SetActive(false);
			RaiseOnUpdate();
		}

		public void ResetStats()
		{
			timeActive = 0;
			enemyDamageTotal = 0;
			enemyHealingTotal = 0;
			enemyDamage = new float[Enum.GetNames(typeof(DamageType)).Length];
			enemyDeaths = 0;
			playerDamageTotal = 0;
			playerHealingTotal = 0;
			playerDamage = new float[Enum.GetNames(typeof(DamageType)).Length];
			playerDeaths = 0;
		}

		public void AddPlayer(Player newPlayer)
		{
			if(newPlayer == null) return;

			ResetStats();
			
			if(player != null) 
			{
				player.onDeath -= ResetPlayer;
				player.onDamage -= UpdatePlayerStatus;
			}
			player = newPlayer;

			player.onDeath += ResetPlayer;
			player.onDamage += UpdatePlayerStatus;
			player.SoftReset();
			UpdatePlayerStatus(new Damage());
			RaiseOnPlayerAdded(newPlayer);
		}

		public void AddEnemy(Enemy newEnemy)
		{
			ResetStats();

			if(enemy != null)
			{
				enemy.onDeath -= ResetEnemy;
				enemy.onDamage -= UpdateEnemyStatus;
			}
			enemy = newEnemy;
			
			enemy.onDeath += ResetEnemy;
			enemy.onDamage += UpdateEnemyStatus;
			enemy.SoftReset();
			UpdateEnemyStatus(new Damage());
		}

		public void ResetEnemy(Damage damage)
		{
			currentBattleTime = 0;

			enemyDeaths++;
			enemy.SoftReset();
			RaiseOnEnemyDeath();
		}

		public void FailOut()
		{
			currentBattleTime = 0;

			enemy.SoftReset();
			RaiseOnEnemyDeath();
		}

		public void RecordPlayerHealth()
		{
			float playerHitpoints = player.GetCurrentHitpoints();
			float healthLoss = playerBattleStartHealth - playerHitpoints;
			playerBattleHealthLoss = (playerBattleHealthLoss == 0) ? healthLoss : .9f * playerBattleHealthLoss + .1f * healthLoss;
			playerBattleStartHealth = playerHitpoints;
		}

		public void ResetPlayer(Damage damage)
		{
			playerBattleHealthLoss = 0;
			playerDeaths++;
			player.SoftReset();
			playerBattleStartHealth = player.GetCurrentHitpoints();
		}

		public void ToggleActive()
		{
			SetActive(!active);
		}

		public void SetActive(bool toSet)
		{
			active = toSet;
		}

		public void UpdatePlayerStatus(Damage damage)
		{
			if(damage.type != DamageType.healing) 
			{
				enemyDamageTotal += damage.value;
				enemyDamage[(int)damage.type] += damage.value;
			} 
			else 
			{
				playerHealingTotal -= damage.value;
				playerDamage[(int)DamageType.healing] -= damage.value;
			}

			if(player != null) 
			{
				playerStatus.SetText(
					GetUnitText(player, damage) 
					+ GetPlayerDPSText()
					+ GetSummaryText()
				);

			}
		}

		public void UpdateEnemyStatus(Damage damage)
		{
			if(damage.type != DamageType.healing) 
			{
				playerDamageTotal += damage.value;
				playerDamage[(int)damage.type] += damage.value;
			} 
			else 
			{
				enemyHealingTotal -= damage.value;
				enemyDamage[(int)DamageType.healing] -= damage.value;
			}

			if(enemy != null) enemyStatus.SetText(GetUnitText(enemy, damage) + GetEnemyDPSText());
		}

		public string GetUnitText(Unit unit, Damage damage)
		{
			string nameText = unit.GetName() + "\n";

			nameText += "\n";

			string hpText = (int)unit.GetCurrentHitpoints() + "/" + (int)Mathf.Round(unit.GetStat(StatType.Hitpoints)) + " ";
			string damageText = damage.value + " " + damage.type + "\n";
			string buffText = "Buffs: " + unit.buffs.Count + " ";
			string debuffText = "Debuffs: " + unit.debuffs.Count + "\n\n";

			return nameText + hpText + damageText + buffText + debuffText;
		}

		public string GetSummaryText()
		{
			string toReturn = "";

			toReturn += "\n\nKDR: " + GetKDR();
			toReturn += "\nHealthLossPerKill: " + playerBattleHealthLoss;

			return toReturn;
		
		}

		public BattleReport.FightSummary GetFightSummary()
		{
			string primary = (enemy.raceClasses.Count > 0) ? enemy.raceClasses[0].GetName() : "";
			string secondary = (enemy.raceClasses.Count > 1) ? enemy.raceClasses[1].GetName() : "";

			return new BattleReport.FightSummary(
				primary,
				secondary,
				GetSurvivalTime(playerDeaths), 
				GetSurvivalTime(enemyDeaths), 
				GetDPS(playerDamageTotal), 
				GetDPS(enemyDamageTotal),
				GetHPS(playerHealingTotal),
				GetHPS(enemyHealingTotal),
				playerBattleHealthLoss
			);

		}

		public string GetTestString()
		{
			if(enemy == null) return "Null Enemy";

			return (enemy.GetName() + GetKDR());
			
		}

		public float GetKDR()
		{
			return Utility.Truncate((0.0f + ((enemyDeaths == 0) ? 1 : enemyDeaths)) / (0.0f + ((playerDeaths == 0) ? 1: playerDeaths)), 2);
		}

		public string GetPlayerDPSText()
		{
			return GetDPSReport(playerDamageTotal, playerDamage, playerDeaths);

		}

		public string GetEnemyDPSText()
		{
			return GetDPSReport(enemyDamageTotal, enemyDamage, enemyDeaths);
		}

		public string GetDPSReport(float totalDamage, float[] itemizedDamage, float deathCount)
		{
			float inverseTime = (timeActive == 0) ? 0 : 1 / timeActive;
			string survivalText = (deathCount > 0) ? "Time To Kill: " + (timeActive / deathCount).ToString("n2") + "\n\n" : "";
			string dpsText = "DPS " + (totalDamage * inverseTime).ToString("n2") + "\n";
			for(int i = 0; i < itemizedDamage.Length; i++)
			{
				if(itemizedDamage[i] != 0) dpsText += ((DamageType)i + " " + (itemizedDamage[i] * inverseTime).ToString("n2")) + "\n";
			}
			
			return (survivalText + dpsText);
		}

		public float GetHPS(float healValue)
		{
			return (timeActive > 0) ? healValue / timeActive : 0;
		}

		public float GetDPS(float damageValue)
		{
			return (timeActive > 0) ? damageValue / timeActive : 0;
		}

		public float GetSurvivalTime(int deathCount)
		{
			return (deathCount > 0) ? timeActive / deathCount : 0;
		}
	}
}