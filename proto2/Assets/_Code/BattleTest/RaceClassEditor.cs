﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Loot;
using UnityEngine.UI;

namespace BattleTest
{
	public class RaceClassEditor : MonoBehaviour 
	{
		public StatWindow statWindowPrototype;
		public ActionSummary actionSummary;
		public StatWindow statWindow;
		private RaceClass raceClass;
		public Button decrement, increment;
		private int currentAction = 0;

		public void Awake()
		{
			statWindow.onEdit += HandleEdit;
			decrement.onClick.AddListener(DecrementAction);
			increment.onClick.AddListener(IncrementAction);
		}


		public void Set(RaceClass raceClass)
		{
			if(raceClass == null) return;
			this.raceClass = raceClass;
			currentAction = 0;

			statWindow.Set(raceClass.GetStatList());

			UpdateAction();

			UpdateUI();
		}

		public void Clear()
		{
			raceClass = null;

			currentAction = 0;

			statWindow.Clear();
			actionSummary.Clear();

			UpdateUI();
		}

		public void HandleEdit()
		{
			if(raceClass == null) return;

			raceClass.SetStats(statWindow.GetStats());
		}

		
		private void UpdateAction()
		{
			if(HasAction(currentAction))
			{
				SetAction(currentAction);
			}
			else
			{
				actionSummary.Clear();
			}
		}

		private void UpdateUI()
		{
			UpdateEffectButtons();
		}

		private void UpdateEffectButtons()
		{
			SetButtonVisible(decrement, currentAction > 0);
			SetButtonVisible(increment, (currentAction + 1) < GetActionCount());			
		}

		private void IncrementAction()
		{
			currentAction++;

			UpdateAction();
			UpdateEffectButtons();
		}

		private void DecrementAction()
		{			
			currentAction--;

			UpdateAction();
			UpdateEffectButtons();
		}

		private int GetAbilityCount()
		{
			if(raceClass == null) return 0;

			return raceClass.abilities.Length;
		}

		private int GetTriggerCount()
		{
			if(raceClass == null) return 0;

			return raceClass.triggeredActions.Length;
		}

		private int GetActionCount()
		{
			if(raceClass == null) return 0;

			return GetAbilityCount() + GetTriggerCount();
		}

		private bool HasAction(int index)
		{
			return (index >= 0 && index < GetActionCount());
		}

		private void SetAction(int index)
		{
			if(index < 0 || index >= GetActionCount()) 
			{
				actionSummary.Clear();
				return;
			}

			int abilityCount = GetAbilityCount();

			if(index < abilityCount) actionSummary.Set(raceClass.abilities[index]);
			else actionSummary.Set(raceClass.triggeredActions[index - abilityCount]);
		}

		private void SetButtonVisible(Button button, bool isVisible)
		{
			if(button.gameObject.activeSelf != isVisible)
			{
				button.gameObject.SetActive(isVisible);
			}
		}
	}
}