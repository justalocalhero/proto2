﻿/*

human
	Pillars

	Stats
		perception
		preperation
		purges
		cleanses
		healing
	Flavor
		Specialization
		Divine right


streety
	Pillars

	Stats
		Dodge
		Stealth
	Flavor


high human
	Pillars
	Stats
		Armor
		Reflect
		Block
	Flavor


stoutheart
	Pillars
		block
		revenge

	Stats
	Flavor
	Talents
		Cleave -- timed pierce fastish
			encouragement -- buff
			disarm -- debuff
			wild -- critty
		Hammer -- timed blunt strongish
			Crunch -- debuff phys
			Knock -- debuff stumble
			Brain -- spell debuff

		Shield Clap --  blunt onHit taken
			Raise Shield -- buffs block
			Kick/shove -- secondary attack
			knowledge -- buffs accuracy and damage
		Shout -- timed debuff
			fear -- debuff
			endurance charge?
			adrenaline -- speed and damage
			resilience buff

		Grandfather's shield -- stats
			polished -- reflect
			massive -- lots of block lots of negative haste
			enchanted -- hp
		Old Blood -- stats hp healing
			Warrior -- damage
			Tinker -- tech
			Ruler -- utility

		crossChop -- onBlock
			tired/hopeless -- debuff
			invincible -- buff
			tripleStrike -- three lighter strikes
		Bane -- onSpelltaken
			keen -- reflect
			anger -- damage buff
			emergency -- if low on life heal

		Sunder -- pierce armor shedding timed
			Shredding -- reverse execute? instance Affliction, cooldown, less damage
			Decapitate -- execute mode?
			Thrill -- buff
		Avatar -- onHitTaken low chance large buff
			lifeblood -- HoT
			resistances or reflect
			armor or block




goblin
	Pillars
	Stats
	Flavor

giant
	Pillars
	Stats
	Flavor


wanderers
	Pillars
	Stats
	Flavor

Tinkers
	Pillars
		Technology
	Stats
	Flavor


*/