﻿/*

-----
TODO
-----

Apply sneak charge on stealth
add talent effects  e.g. "your piercing attacks do 1 additional bleed damage" "Your spells grant the following buff"


Human
    Rallying cry -augment names




Equipment
    Template
        Icon
        Slot
        Slot multiplier
        base stats

    Enchantment
        magic stats

    Divinity

    quality


upgrades
    smelter
        dismantling blows
        scrap percent
        bonus scrap per kill

    tavern
        min level
        max level
        races
        relic chance
        innate ability chance

    camp
        unlock occupations
        loot time
        camping effectiveness
        camping buff

    academy
        unlock physical classes
        unlock magical classes
        unlock tech classes
        unlock support classes
        
        unlock bestiary
        bestiary unlock chance
        unlock journal
        journal unlock chance
        unlock census

    armory
        discover design
        discover material
        superior crafting
        inferior crafting

    shrine
        shrines
        offerings per kill
        gift chance
        chance for dead to return as a weapon

    tower
        spells
        offerings per kill
        chance for dead to return as a ring

    altar
        blessings
        starting buff
        grant bonus blessing
    
    apothecary
        potion count
        potion effect
        chance to find potion when resting
        chance to find potion in encounter

    Arena
        rounds
        prizes


    currency
        upgrades
            scrap

        crafting
            designs
                scrap
            materials
                refined materials
            enchanting
                essence
            divining
                holy water


    potions
        Supplies
            immunity
            invisibility - escape battle
            more healing
            remove all debuffs
            explosion - kill current enemy and guarantee loot
            buff - bit redundant
            reset cooldowns
            




lvl 5 human barkeep;

Bear Burly 2.75
Bear Frenzied 2.75
Bear Meek 5.5
Wolf Burly 11
Wolf Frenzied 11
Wolf Meek 11
Snake Burly 11
Snake Frenzied 5.5
Snake Meek 11
Arachnid Frenzied 0.48
Arachnid Spitter 1.1
Arachnid Striker 1.22
Arachnid Weaver 1.22
Druid Savage Spirit 0.14
Druid Oaken Spirit 0.27
Druid Faekin 0.73
Druid Exile 0.38
Dryad Savage Spirit 0.12
Dryad Oaken Spirit 0.38
Dryad Faekin 0.79
Dryad Exile 0.44
Elder Krull Clan Keeper 2.2
Elder Krull Staff Holder 1.83
Elder Krull Snow Hider 5.5
Elder Krull Magna 1.57
Krull Clan Keeper 2.2
Krull Staff Holder 1
Krull Snow Hider 2.2
Krull Magna 2.2
Balth Barbarous 0.12
Balth Fallen 0.14
Balth Massive 0.17
Balth Warded 0.31
Stitchwork Bonecaster 0.58
Stitchwork Legionaire 0.61
Stitchwork Nulifier 0.92
Stitchwork Plague Spiller 0.65
Undying Bonecaster 0.61
Undying Legionaire 0.65
Undying Nulifier 1.38
Undying Plague Spiller 0.92
Darter Potent 0.85
Darter Putrid 1.22
Darter Spiny 1
Grabber Potent 0.07
Grabber Putrid 0.08
Grabber Spiny 0.01
Skitterer Potent 0.85
Skitterer Putrid 0.85
Skitterer Spiny 0.61
Hosk Bone Picker 0.79
Hosk Caller 0.5
Hosk Dart Slinger 0.52
Hosk Shrouded 0.44
Witch Sorrowful 1.57
Witch 0 1.1
Witch Wangateur 2.2
Warlock Sorrowful 1.83
Warlock 0 1.22
Warlock Wangateur 2.75
Brute Grizzled 0.3
Brute Sadistic 1.1
Brute Tactician 2.2
Practiced Grizzled 1.57
Practiced Sadistic 1.22
Practiced Tactician 2.75
Rogue Grizzled 1
Rogue Sadistic 1.1
Rogue Tactician 1.83
Deep Dweller Vanguard 0.34
Deep Dweller Warrior 0.65
Deep Dweller Tinker 1.1
Ranger Vanguard 0.52
Ranger Warrior 1.22
Ranger Tinker 1.38
Royal Vanguard 0.12
Royal Warrior 0.46
Royal Tinker 0.85
Paragon Indignant 0.09
Paragon Duelist 0.08
Paragon 0 0.09
Scion Indignant 0.12
Scion Duelist 0.14
Scion 0 0.15

Human Barkeep Bucaneer lvl 10

Bear Burly 10.2
Bear Frenzied 12.75
Bear Meek 17
Wolf Burly 51
Wolf Frenzied 25.5
Wolf Meek 51
Snake Burly 51
Snake Frenzied 25.5
Snake Meek 51
Arachnid Frenzied 2.12
Arachnid Spitter 3.92
Arachnid Striker 4.25
Arachnid Weaver 5.67
Druid Savage Spirit 1.21
Druid Oaken Spirit 17
Druid Faekin 3.92
Druid Exile 2.04
Dryad Savage Spirit 1.46
Dryad Oaken Spirit 17
Dryad Faekin 4.25
Dryad Exile 2.32
Elder Krull Clan Keeper 17
Elder Krull Staff Holder 6.38
Elder Krull Snow Hider 17
Elder Krull Magna 8.5
Krull Clan Keeper 12.75
Krull Staff Holder 6.38
Krull Snow Hider 17
Krull Magna 12.75
Balth Barbarous 0.67
Balth Fallen 0.75
Balth Massive 1.06
Balth Warded 2.55
Stitchwork Bonecaster 2.68
Stitchwork Legionaire 2.32
Stitchwork Nulifier 3.64
Stitchwork Plague Spiller 1.89
Undying Bonecaster 3.4
Undying Legionaire 3.4
Undying Nulifier 5.67
Undying Plague Spiller 3.64
Darter Potent 6.38
Darter Putrid 5.67
Darter Spiny 3
Grabber Potent 3
Grabber Putrid 3.19
Grabber Spiny 1.55
Skitterer Potent 3.19
Skitterer Putrid 2.68
Skitterer Spiny 2.22
Hosk Bone Picker 3.4
Hosk Caller 2.32
Hosk Dart Slinger 2.22
Hosk Shrouded 1.96
Witch Sorrowful 5.1
Witch 0 4.25
Witch Wangateur 8.5
Warlock Sorrowful 6.38
Warlock 0 4.25
Warlock Wangateur 17
Brute Grizzled 2.55
Brute Sadistic 3.92
Brute Tactician 12.75
Practiced Grizzled 6.38
Practiced Sadistic 5.1
Practiced Tactician 10.2
Rogue Grizzled 5.67
Rogue Sadistic 4.25
Rogue Tactician 8.5
Deep Dweller Vanguard 2.12
Deep Dweller Warrior 5.67
Deep Dweller Tinker 5.67
Ranger Vanguard 2.55
Ranger Warrior 10.2
Ranger Tinker 7.29
Royal Vanguard 1.38
Royal Warrior 7.29
Royal Tinker 5.67
Paragon Indignant 0.46
Paragon Duelist 0.41
Paragon 0 0.5
Scion Indignant 0.51
Scion Duelist 0.56
Scion 0 0.7

*/