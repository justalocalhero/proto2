﻿/*

Manipulation
	haste
	dispel
	purge
	mirage
	illusion
	duplicate
	blink
	blink armor

flow - draws magical energy from atunement with the ways of the universe
	leaf on the wind

Maladiction
	hex
	blood boil
	reverse organs
	blindness
	atrophy
	madness

evocation - draws magical energy from within--from their own potential
	fireball
	cone of cold
	prismatic flash
	upheaval
	flashfreeze

Warlock - draws magical energies from pacts with ancient beings of great power
	word of binding
	ritual
	remove year
	invalidate

Frost
Necromancy
Blood
Nature
Ancient - draws magical abilities by gathering, bit by bit, the lifeforce and memories of their ancestors	
	Tiger spirit
	snake spirit
	omen
	Whispers of the ancestors
	fungal growth
	vines
	
	
Enhancement
Alchemy
Divination - granted magical ability by the dieties
	guiding light
	bless
	rebuke

Mischief


 */