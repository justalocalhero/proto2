﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ArmoryManager : MonoBehaviour 
{
	public Button[] buttons;
	public Armory armory;

	public void Start () 
	{
		UpdateButtons();
		armory.onUnlock += UpdateButtons;
	}
	
	public void UpdateButtons () 
	{


		for(int i = 0; i < buttons.Length; i++)
		{
			if(buttons[i] != null)
			{
				if(armory.unlocked) ShowButton(buttons[i]);
				else HideButton(buttons[i]);
			}
		}
	}

	public void HideButton(Button button)
	{
		if(button == null) return;
		button.gameObject.SetActive(false);
	}

	public void ShowButton(Button button)
	{
		if(button == null) return;
		button.gameObject.SetActive(true);
	}
}
