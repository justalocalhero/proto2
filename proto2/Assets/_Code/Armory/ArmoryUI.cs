﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmoryUI : EquipmentUI 
{
	public EquipmentManager activeEquipmentManager;

	public void OnEquipButtonPress()
	{
		SwapInventory();
	}

	public void SwapInventory()
	{
		Equipment removedEquipment;

		for(int i = 0; i < slots.Length; i++)
		{
			if(slots[i].equipment != null)
			{
				removedEquipment = activeEquipmentManager.Add(slots[i].equipment);
				slots[i].Clear();
				if(removedEquipment != null) inventory.Add(removedEquipment);
			}
		}
	}
}
