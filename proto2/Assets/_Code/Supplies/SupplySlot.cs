﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SupplySlot : MonoBehaviour 
{

	int index;

	public virtual Supply supply {get; set;}

	public Button button;
	public TextMeshProUGUI mesh;

	public void Awake()
	{
		button.onClick.AddListener(OnPointerClick);
	}

	public virtual void UpdateText()
	{

	}

	public void Clear()
	{
		Hide();
	}

	public void Show()
	{
		if(!gameObject.activeSelf) gameObject.SetActive(true);
	}

	public void Hide()
	{
		if(gameObject.activeSelf) gameObject.SetActive(false);
	}

    public virtual void OnPointerClick()
    {
		supply.OnClick();
    }
}
