﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SupplyListUI : MonoBehaviour 
{

	public virtual SupplySlot supplySlotPrototype {get; set;}

	public Transform slotContainer;
	public FloatReference spacing;
	public IntReference rows, columns;

	public void Start () 
	{
		BuildSlots();
		UpdateUI();
		RegisterDelegates();
	}

	public virtual void RegisterDelegates()
	{
		
	}
	
	public void BuildSlots()
	{
		GridBuilder<MonoBehaviour> gridBuilder = new GridBuilder<MonoBehaviour>();

		List<MonoBehaviour> slotMono = gridBuilder.Begin()
			.WithPrototype(supplySlotPrototype)
			.WithContainer(slotContainer)
			.WithSpacing(spacing, spacing)
			.WithRows(rows)
			.WithColumns(columns)
			.Build();

		ConvertSlots(slotMono);
	}

	public virtual void ConvertSlots(List<MonoBehaviour> slotMono)
	{
		
	}

	public virtual void UpdateUI()
	{

	}
}
