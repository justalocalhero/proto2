﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Supply
{
	public IUnlocker parent {get; set;}
	public SupplyState currentState;
	public string name {get; set;}

	public virtual void RaiseOnChanged()
	{
		
	}

	public void OnClick()
	{
		if(CanDeselect()) Deselect();
		else if(CanSelect()) Select();
	}

	public bool CanSelect()
	{
		if(!IsAvailable()) return false;
		if(!parent.CanSelect()) return false;

		return true;
	}

	public bool CanDeselect()
	{
		if(!IsSelected()) return false;

		return true;
	}
	
	public void Select()
	{
		currentState = SupplyState.selected;
		RaiseOnChanged();
	}

	public void Deselect()
	{
		currentState = SupplyState.available;
		RaiseOnChanged();
	}

	public void Unlock()
	{
		currentState = SupplyState.available;
	}

	public bool IsAvailable()
	{
		return currentState == SupplyState.available;
	}

	public bool IsSelected()
	{
		return currentState == SupplyState.selected;
	}

	public bool IsLocked()
	{
		return currentState == SupplyState.locked;
	}

	public enum SupplyState {locked, available, selected};
}
