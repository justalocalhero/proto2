﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotList : MonoBehaviour 
{	
	public Transform slotContainer;
	public Slot slotPrototype;
	public ColorReference availableColor, selectedColor;
	public IntReference columns;
	private List<Slot> slots;
	private int selected;

	public delegate void OnSelected();
	public OnSelected onSelected;

	public void RaiseOnSelected()
	{
		if(onSelected != null) onSelected();
	}

	public void Awake()
	{
		BuildSlots();
	}

	public void HandleSelected(int index)
	{
		selected = (selected == index) ? -1 : index;
		RaiseOnSelected();
		UpdateUI();
	}

	public int GetSelected()
	{
		return selected;
	}

	public void BuildSlots()
	{
		slots = new GridBuilder<Slot>()
		.Begin()
		.WithPrototype(slotPrototype)
		.WithContainer(slotContainer)
		.WithColumns(columns)
		.WithSpacing(2, 2)
		.WithPadding(4, 4)
		.Build();

		RegisterSlots();
	}

	public void RegisterSlots()
	{
		for(int i = 0; i < slots.Count; i++)
		{
			slots[i].SetIndex(i);
			slots[i].onClick += HandleSelected;
		}
	}

	public void SetSlots(List<string> slotNames)
	{
		ClearSlots();	

		for(int i = 0; i < slots.Count; i++)
		{
			if(i < slotNames.Count) 
			{
				slots[i].SetText(slotNames[i]);
				slots[i].Show();
			}
		}
	}

	public void ClearSlots()
	{
		selected = -1;

		foreach(Slot slot in slots)
		{
			slot.Clear();
		}
	}

	public void UpdateUI()
	{
		UpdateSlots();
	}

	public void UpdateSlots()
	{
		for(int i = 0; i < slots.Count; i++)
		{
			slots[i].SetColor((slots[i].GetIndex() == selected) ? selectedColor : availableColor);
		}
	}
}
