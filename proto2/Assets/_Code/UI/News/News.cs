﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class News : MonoBehaviour, IPointerClickHandler
{
	public MaterialUnlocker materialUnlocker;
	public DesignUnlocker designUnlocker;
	public EnchantmentUnlocker enchantmentUnlocker;
	public SaveManager saveManager;

	[Space(20)]
	public UIToggler toggler;
	public TextMeshProUGUI text;
	public Queue<string> news = new Queue<string>();
	public FloatReference fadeTime;
	private float nextFadeTime;
	private bool active;

	public void Awake()
	{
		materialUnlocker.onUnlocked += ((string name) => Push("New Crafting Material Discovered!\n" + name));
		designUnlocker.onUnlocked += ((string name) => Push("New Crafting Design Discovered!\n" + name));
		enchantmentUnlocker.onUnlocked += ((string name) => Push("New Crafting Enchantment Discovered!\n" + name));
		saveManager.onSave += ((SaveFile saveFile) => Push("Game Saved"));

	}

	public void Update()
	{
		if(active)
		{
			float time = Time.time;

			if(time >= nextFadeTime)
			{
				Fade();
			}
		}
	}

	public void Push(string newNews)
	{
		if(newNews == null) return;

		news.Enqueue(newNews);
		if(!active)
		{
			active = true;
			ResetFadeTime();
		}

		UpdateUI();
	}

	public void ResetFadeTime()
	{
		nextFadeTime = Time.time + fadeTime;
	}

	public void Fade()
	{
		Pop();
		ResetFadeTime();
	}

	public void Pop()
	{
		if(news.Count == 0)
		{
			active = false;
			return;
		}

		news.Dequeue();

		if(news.Count == 0)
		{
			active = false;
		}

		UpdateUI();
	}

	private void UpdateUI()
	{
		if(!active)
		{
			Clear();
			return;
		}

		string top = news.Peek();
		if(top == null) return;
		text.SetText(news.Peek());
		Show();
	}

	public void Show()
	{
		toggler.Show();
	}

	public void Hide()
	{
		toggler.Hide();
	}

	public void Clear()
	{
		text.SetText("");
		Hide();
	}

    public void OnPointerClick(PointerEventData eventData)
    {
        Fade();
    }
}
