﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformFitter : MonoBehaviour 
{
	public UIToggler toggler;
	private RectTransform rect;
	private float offsetX, offsetY;

	void Start()
	{
		rect = transform as RectTransform;
	}

	public void SetOffset(float x, float y)
	{
		offsetX = x;
		offsetY = y;
	}
	
	public void Fit(RectTransform t1, RectTransform t2)
	{
		Vector3[] worldCorners1 = new Vector3[4];
		Vector3[] worldCorners2 = new Vector3[4];

		t1.GetWorldCorners(worldCorners1);
		t2.GetWorldCorners(worldCorners2);

		float worldLeft = Utility.Lesser(worldCorners1[1].x, worldCorners2[1].x);
		float worldRight = Utility.Greater(worldCorners1[3].x, worldCorners2[3].x);
		float worldTop = Utility.Greater(worldCorners1[1].y, worldCorners2[1].y);
		float worldBottom = Utility.Lesser(worldCorners1[3].y, worldCorners2[3].y);

		float width = worldRight - worldLeft;
		float height = worldTop - worldBottom;

		Vector2 pivot = rect.pivot;
		Vector2 size = rect.InverseTransformVector(new Vector2(width, height));
		Vector2 worldOffset = rect.TransformVector(new Vector2(offsetX, offsetY));

		float pivotWidth = width * pivot.x;
		float pivotHeight = height * (pivot.y - 1);

		float pivotOffsetX = worldOffset.x * (2 * pivot.x - 1);
		float pivotOffsetY = worldOffset.y *  (2 * pivot.y - 1);

		rect.position = new Vector2(worldLeft + pivotWidth + pivotOffsetX, worldTop + pivotHeight + pivotOffsetY);
		rect.sizeDelta = new Vector2(size.x + 2 * offsetX, size.y + 2 * offsetY);
	}

	public void Show()
	{
		toggler.Show();
	}

	public void Hide()
	{
		toggler.Hide();
	}
}
