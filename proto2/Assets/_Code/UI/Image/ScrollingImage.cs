﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ScrollingImage : MonoBehaviour 
{
	public Material material;
	public Image image;
	private float speed;

	private void Start()
	{
		image.material = Instantiate(material) as Material;
	}

	public void SetSprite(Sprite sprite)
	{
		image.sprite = sprite;
	}

	public void SetSpeed(float speed)
	{
		this.speed = speed;
	}

	public void Scroll(float updateTime)
	{
		Vector2 offset = new Vector2(speed * updateTime, 0);
		image.material.mainTextureOffset += offset;
	}
}
