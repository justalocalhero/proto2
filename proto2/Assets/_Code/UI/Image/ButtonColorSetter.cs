﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonColorSetter : MonoBehaviour 
{

	public ColorReference normalColor, highlightedColor, pressedColor, disabledColor;
	private Button button;

	void OnValidate()
	{
		GetButton();
		Set();
	}

	public void Start()
	{
		GetButton();
		Set();
	}

	public void GetButton()
	{
		if(button == null) button = GetComponent<Button>();
	}

	public void Set()
	{
		if(button == null) return;
		if(normalColor == null) return;
		if(highlightedColor == null) return;
		if(pressedColor == null) return;
		if(disabledColor == null) return;

		ColorBlock colors = button.colors;

		colors.normalColor = normalColor;
		colors.highlightedColor = highlightedColor;
		colors.pressedColor = pressedColor;
		colors.disabledColor = disabledColor;

		button.colors = colors;
	}
}
