﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollingBackground : MonoBehaviour 
{
	public bool active;
	public FloatReference maxSpeed;
	public ScrollingImage imagePrototype;
	public List<Sprite> backSprites, frontSprites;
	public Transform backContainer, frontContainer;
	private List<ScrollingImage> images = new List<ScrollingImage>();

	public void Start()
	{
		BuildImages();
	}

	public void BuildImages()
	{
		BuildImageList(backSprites, backContainer);
		BuildImageList(frontSprites, frontContainer);

		int factor = 1 + images.Count;

		foreach(ScrollingImage image in images)
		{
			image.SetSpeed(maxSpeed / (factor--));
		}
	}

	public void BuildImageList(List<Sprite> sprites, Transform container)
	{
		foreach(Sprite sprite in sprites)
		{
			ScrollingImage temp = Object.Instantiate(imagePrototype, container, false);
			temp.SetSprite(sprite);
			images.Add(temp);
		}
	}

	public void Update()
	{
		if(active) Scroll(Time.deltaTime);
	}

	public void Scroll(float delta)
	{
		if(images == null) return;

		foreach(ScrollingImage image in images)
		{
			image.Scroll(delta);
		}

	}

	public void Activate()
	{
		active = true;
	}

	public void Deactivate()
	{
		active = false;
	}
}
