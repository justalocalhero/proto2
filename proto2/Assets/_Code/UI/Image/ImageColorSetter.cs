﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageColorSetter : MonoBehaviour 
{
	public ColorReference color;
	private Image image;

	void OnValidate()
	{
		GetImage();
		Set();
	}

	public void Start()
	{
		GetImage();
		Set();
	}

	public void GetImage()
	{
		if(image == null) image = GetComponent<Image>();

	}

	public void Set()
	{
		if(image == null) return;
		if(color == null) return;

		image.color = color;
	}
}
