﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tooltips;

public class StatTypeText : MonoBehaviour 
{
	public ColorReference defaultColor;
	public StatNameDictionary dictionary;

	public string GetText(EquipStat stat)
	{
		return GetText(stat.StatType);
	}

	public string GetText(EquipStat stat, Color color)
	{	
		return GetText(stat.StatType, color);
	}

	public string GetText(StatType type)
	{
		return GetText(type, defaultColor);
	}

	public string GetText(StatComparison stat, Color color)
	{
		return GetText(stat.stat.StatType, color);
	}

	public string GetText(StatType type, Color color)
	{
		string typeString = dictionary.GetTypeName(type);
		string toReturn = ("<color=#" + ColorUtility.ToHtmlStringRGBA(color) + ">" + typeString + "</color>");

		return toReturn;
	}
}
