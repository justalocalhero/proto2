﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tooltips;

public class AbilityIcon : MonoBehaviour
{
	public SpriteManager spriteManager;
	private RectTransform rect;
	private Ability ability;
	private TriggeredAction triggeredAction;
	private Unit unit;
	public Image icon;
	public Button button;
	public Image buttonImage;
	private Transform tooltipContainer;
	public TriggeredActionTooltip triggeredActionTooltip {get; set;}
	public AbilityTooltip abilityTooltip {get; set;}

	public void Awake()
	{
		rect = transform as RectTransform;
	}

	public void SetTooltipContainer(Transform newTooltipContainer)
	{
		tooltipContainer = newTooltipContainer;
		triggeredActionTooltip = tooltipContainer.GetComponentInChildren<TriggeredActionTooltip>();
		abilityTooltip = tooltipContainer.GetComponentInChildren<AbilityTooltip>();
	}

	public void Set(Ability newAbility)
	{
		if(newAbility == null) return;
		triggeredAction = null;
		ability = newAbility;		
		button.enabled = true;
		buttonImage.enabled = true;
		SetIcon(spriteManager.GetSprite(newAbility.GetSprite()));

	}

	public void Set(Ability newAbility, Unit newUnit)
	{
		if(newAbility == null) return;
		if(newUnit == null) return;
		unit = newUnit;
		triggeredAction = null;
		ability = newAbility;		
		button.enabled = true;
		buttonImage.enabled = true;
		SetIcon(spriteManager.GetSprite(newAbility.GetSprite()));
	}

	public void Set(TriggeredAction newTriggeredAction)
	{
		if(newTriggeredAction == null) return;
		ability = null;
		triggeredAction = newTriggeredAction;
		button.enabled = true;
		buttonImage.enabled = true;
		SetIcon(spriteManager.GetSprite(newTriggeredAction.GetSprite()));
	}

	public void Set(TriggeredAction newTriggeredAction, Unit newUnit)
	{
		if(newTriggeredAction == null) return;
		if(newUnit == null) return;
		unit = newUnit;
		ability = null;
		triggeredAction = newTriggeredAction;
		button.enabled = true;
		buttonImage.enabled = true;
		SetIcon(spriteManager.GetSprite(newTriggeredAction.GetSprite()));
	}

	public void Clear()
	{
		unit = null;
		ability = null;
		triggeredAction = null;
		button.enabled = false;
		buttonImage.enabled = false;
		ClearIcon();
	}

	public void OnPointerEnter()
	{
		if(ability != null)
		{
			if(unit != null) abilityTooltip.Set(ability, unit, rect);
			else abilityTooltip.Set(ability, rect);
		}

		if (triggeredAction != null)
		{
			if(unit != null) triggeredActionTooltip.Set(triggeredAction, unit, rect);
			else triggeredActionTooltip.Set(triggeredAction, rect);
		}
	}

	public void SetIcon(Sprite sprite)
	{
		if(sprite == null) ClearIcon();
		else
		{			
			icon.sprite = sprite;
			icon.enabled = true;
		}	
	}

	public void ClearIcon()
	{
		icon.sprite = null;
		icon.enabled = false;
	}

	public void OnPointerExit()
	{
		abilityTooltip.Hide();
		triggeredActionTooltip.Hide();
	}
}
