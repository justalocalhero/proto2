﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerText : ColoredText
{
	public ProcTriggerNameDictionary triggerNames;

	public string GetText(ProcTarget target, ProcTrigger trigger, float chance)
	{
		return GetText(target, trigger, chance, defaultColor.Value);
	}

	public string GetText(ProcTarget target, ProcTrigger trigger, float chance, Color color)
	{
		string triggerString = (chance < 1) ? Utility.ToPercent(chance) + " chance " + triggerNames.GetString(target, trigger)
			: triggerNames.GetString(target, trigger);

		return GetText(triggerString, color);
	}
}
