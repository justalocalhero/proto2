﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitResourceAugmentText : ColoredText 
{
	public StatText statText;

	public string GetText(UnitResourceAugment augment)
	{
		return GetText(augment, defaultColor.Value);
	}

	public string GetText(UnitResourceAugment augment, Color color)
	{
		string capString = GetCapString(augment);
		string statString = GetStatString(augment);

		string totalString = capString + statString;
		return GetText(totalString, color);
	}

	public string GetCapString(UnitResourceAugment augment)
	{
		string toReturn = "";

		if(augment.cap == 0) 
		{
			return toReturn;
		}
		else if(augment.cap > 0)
		{
			toReturn += "Unit may hold " + augment.cap + " aditional " + augment.type;
			toReturn += (augment.cap > 1) ? " charges" : " charge";
			toReturn += "\n";
		}
		else 
		{
			toReturn += "Unit may hold " + augment.cap + " fewer " + augment.type;
			toReturn += (augment.cap < -1) ? " charges" : " charge";
			toReturn += "\n";
		}
		
		return toReturn;
	}

	public string GetStatString(UnitResourceAugment augment)
	{
		string toReturn = "";

		for(int i = 0; i < augment.stats.Length; i++)
		{
			toReturn += statText.GetText(augment.stats[i]) + "\n";
		}

		if(augment.stats.Length > 0) toReturn = augment.type + " charges grant an additional\n" + toReturn + "\n";

		return toReturn;
	}
}
