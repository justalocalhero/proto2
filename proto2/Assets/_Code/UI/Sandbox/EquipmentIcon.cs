﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Tooltips;

public class EquipmentIcon : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public SpriteManager spriteManager;
	private RectTransform rect;
	private Equipment equipment;
	public EquipmentTooltip equipmentTooltip{get; set;}
	public Image icon;
	public UIToggler toggler;

	public void Awake()
	{
		rect = transform as RectTransform;
	}

	public void Set(Equipment equipment)
	{
		this.equipment = equipment;
		SetIcon(equipment);
	}

	public void Clear()
	{
		this.equipment = null;
		ClearIcon();
	}

	public void SetIcon(Equipment equipment)
	{
		icon.sprite = spriteManager.GetSprite(equipment.GetSprite());
		toggler.Show();
	}

	public void ClearIcon()
	{
		toggler.Hide();
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
		if(equipment == null) return;

		equipmentTooltip.Set(equipment, rect);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        equipmentTooltip.Hide();
    }
}
