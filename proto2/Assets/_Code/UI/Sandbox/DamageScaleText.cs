﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageScaleText : MonoBehaviour 
{
	public  ColorReference scaleColor;

	public string GetText(float scaleValue)
	{
		string scaleString = scaleValue.ToString("n2");
		string toReturn = ("<color=#" + ColorUtility.ToHtmlStringRGBA(scaleColor) + ">" + scaleString + "x</color>");

		return toReturn;
	}
}
