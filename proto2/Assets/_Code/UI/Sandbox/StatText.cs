﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tooltips;

public class StatText : MonoBehaviour 
{
	public StatModText statModText;
	public StatValueText statValueText;
	public StatTypeText statTypeText;

	public string GetText(EquipStat stat)
	{
		return GetText(stat, 1);
	}

	public string GetText(EquipStat stat, Color color)
	{
		return GetText(stat, 1, color);
	}

	public string GetText(EquipStat stat, float multiplier)
	{
		string modString = statModText.GetText(stat) + " ";
		string typeString = statTypeText.GetText(stat) + " ";
		string valueString = statValueText.GetText(stat, multiplier);

		return (valueString + modString + typeString);
	}

	public string GetText(EquipStat stat, float multiplier, Color color)
	{
		return GetText(stat, multiplier, color, color, color);
	}

	public string GetText(EquipStat stat, Color modColor, Color typeColor, Color valueColor)
	{
		return GetText(stat, 1, modColor, typeColor, valueColor);
	}

	public string GetText(EquipStat stat, float multiplier, Color modColor, Color typeColor, Color valueColor)
	{
		string modString = (statModText.GetText(stat, modColor) + " ");
		string typeString = (statTypeText.GetText(stat, typeColor) + " ");
		string valueString = (statValueText.GetText(stat, multiplier, valueColor));
		return (valueString + modString + typeString);
	}
	
	public string GetText(StatComparison stat, Color statColor, Color comparisonColor)
	{
		// string modString = (statModText.GetText(stat, statColor) + " ");
		// string typeString = (statTypeText.GetText(stat, statColor) + " ");
		// string valueString = (stat.difValue != 0) ? 
		// 	(statValueText.GetText(stat.stat., stat.equippedValue, statColor)) : "";
		
		// float comparisonValue =  stat.highlightedValue - stat.equippedValue;
		
		// string comparisonString = (comparisonValue != 0) ? 
		// 	(statValueText.GetText(stat.modifierType, comparisonValue, comparisonColor) + 
		// 	statModText.GetText(stat.modifierType, comparisonColor)) : "";

		// return (valueString + modString + typeString + comparisonString);
		return "";
	}
	
}
