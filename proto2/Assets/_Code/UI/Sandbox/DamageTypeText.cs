﻿using UnityEngine;
using TMPro;
using System;

public class DamageTypeText : MonoBehaviour 
{
	public DamageColorMap damageColorMap;

	public string GetText(Damage damage)
	{
		Color color = damageColorMap.GetColor(damage.type);
		string colorString = ColorUtility.ToHtmlStringRGBA(color);
		return "<color=#" + colorString + ">" + damage.type + "</color>";
	}
}
