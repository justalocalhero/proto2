﻿using UnityEngine;
using System;

[CreateAssetMenu(menuName = "UI/DamageColorMap")]
public class DamageColorMap : ScriptableObject 
{
	public DamageColor[] colors = new DamageColor[Enum.GetNames(typeof(DamageType)).Length];

	public Color GetColor(DamageType type)
	{
		Color failColor = new Color();
		if(colors.Length <= 0)
		{
			Debug.LogWarning(" Can not get color. Color map not initialized. ");
			return failColor;
		}

		if((int)type >= colors.Length)
		{
			Debug.LogWarning("Color out of range.  Expected Range: " + 0 + " - " + (colors.Length - 1));
			return failColor;
		}

		DamageColor toCheck = colors[(int)type];

		if(toCheck.damageType != type)
		{
			Debug.LogWarning("Improperly ordered Color Set.  Expected: " + type + " Found: " + toCheck.damageType);
			return failColor;
		}

		return toCheck.color;
	}
}


[Serializable]
public struct DamageColor {public DamageType damageType; public ColorReference color;};