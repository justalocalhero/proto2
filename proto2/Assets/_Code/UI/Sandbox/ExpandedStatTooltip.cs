﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ExpandedStatTooltip : MonoBehaviour 
{
	public ColorReference primaryColor, secondaryColor;
	public StatNameDictionary statNames;
	public StatNameDictionary descriptions;

	public string GetText(StatType type, Player unit)
	{
		string defaultString = GetDefaultString(type, unit);
		string additionsString = "";
		switch(type)
		{			
			case StatType.Haste:
				float hasteFactor = unit.GetHasteFactor();
				additionsString = 
					(hasteFactor > 1) ?
					"Cooldowns reduced by " + Utility.ToPercent(hasteFactor - 1) :
					"Cooldowns increased by " + Utility.ToPercent(1 / hasteFactor - 1);
				break;
			case StatType.Weight:
				float weightFactor = unit.GetWeightFactor();
				additionsString = 
					(weightFactor > 1) ?
					"Cooldowns reduced by " + Utility.ToPercent(weightFactor - 1) :
					"Cooldowns increased by " + Utility.ToPercent(1 / weightFactor - 1);
				break;
			case StatType.CritChance: 
				additionsString = ("Chance to critically strike " + Utility.ToPercent(unit.GetCritChance()));
				break;
			case StatType.CritMultiplier: 
				additionsString = ("Critical strike damage " + Utility.ToX(unit.GetCritFactor(true)));
				break; 
			case StatType.Armor: 
				additionsString = GetResistanceString(DamageType.physical, unit);
				break;
			case StatType.BleedResistance: 
				additionsString = GetResistanceString(DamageType.bleed, unit);
				break;
			case StatType.PoisonResistance: 
				additionsString = GetResistanceString(DamageType.poison, unit);
				break;
			case StatType.HealingResistance: 
				additionsString = GetResistanceString(DamageType.healing, unit);
				break;
			case StatType.FireResistance: 
				additionsString = GetResistanceString(DamageType.fire, unit);
				break;
			case StatType.ColdResistance: 
				additionsString = GetResistanceString(DamageType.cold, unit);
				break;
			case StatType.ArcaneResistance: 
				additionsString = GetResistanceString(DamageType.arcane, unit);
				break;
			case StatType.DarkResistance: 
				additionsString = GetResistanceString(DamageType.dark, unit);
				break;
			case StatType.ArmorToDamage:
				break; 
			case StatType.DamageToPoison:
				additionsString = ("Deal " + Utility.ToPercent(unit.GetConversionFactor(StatType.DamageToPoison)) + " of Damage as Poison");
				break; 
			case StatType.DamageToBleed:
				additionsString = ("Deal " + Utility.ToPercent(unit.GetConversionFactor(StatType.DamageToBleed)) + " of Damage as Bleed");
				break;
			case StatType.Lifesteal:
				additionsString = ("Gain " + Utility.ToPercent(unit.GetConversionFactor(StatType.Lifesteal)) + " of Damage as Healing");
				break; 
			case StatType.Thorns: 
				additionsString = ("Return " + Utility.ToPercent(unit.GetConversionFactor(StatType.Thorns)) + " of Damage taken");
				break; 
			case StatType.Reflect: 
				additionsString = (Utility.ToPercent(unit.GetReflectRoll()) + " chance to reflect spells back upon the enemy");
				break;
			case StatType.Perception: 
				additionsString = ("Chance to find treasures and avoid traps improved by " + Utility.ToPercent(unit.GetPerceptionCheck()));
				break;
			case StatType.Preparation: 
				additionsString = (Utility.ToPercent(unit.GetCampHealingFactor()) + " of Hitpoints healed while camping between fights");
				break;
			case StatType.Stealth: 
				additionsString = (Utility.ToPercent(unit.GetStealthRoll()) + " chance to catch enemies off guard (all their abilities will be on cooldown)");
				break;
			case StatType.Dodge: 
				additionsString = (Utility.ToPercent(unit.GetDodgeRoll()) + " chance to dodge an incoming attack and all its effects");
				break;
			case StatType.Block: 
				additionsString = (Utility.ToPercent(unit.GetBlockRoll()) + " chance to block incoming damage but not its effects");
				break;
			case StatType.Resilience: 			
				additionsString = (Utility.ToPercent(unit.GetResilienceFactor() - 1) + " improved duration of Buffs and Debuffs applied to you");
				break;
			case StatType.Multistrike: 
				break;
			case StatType.MultiBuff: 
				break;
			case StatType.PurgeChance: 
				break;
			case StatType.CleanseChance: 
				break;
			case StatType.Pathfinding: 
				additionsString = ("Time between encounters decreased by " + Utility.ToPercent(unit.GetPathfindingFactor() - 1));
				break;
			case StatType.Accuracy: 
				additionsString = ("Never miss targets up to level " + (int) Mathf.Floor(unit.GetStat(StatType.Accuracy)));
				break;
			case StatType.Affliction: 
				additionsString = ("Debuffs applied last " + Utility.ToPercent(unit.GetAfflictionFactor()) + " longer");
				break;
			case StatType.SpellProficiency: 
				additionsString = GetProficiencyString(StatType.SpellProficiency, unit);
				break;
			case StatType.PierceProficiency: 		
				additionsString = GetProficiencyString(StatType.PierceProficiency, unit);
				break;
			case StatType.BluntProficiency: 
				additionsString = GetProficiencyString(StatType.BluntProficiency, unit);
				break;
			case StatType.SupportProficiency: 
				additionsString = GetProficiencyString(StatType.SupportProficiency, unit);
				break;
			case StatType.TechProficiency: 
				additionsString = GetProficiencyString(StatType.TechProficiency, unit);
				break;
			case StatType.CurseProficiency: 
				additionsString = GetProficiencyString(StatType.CurseProficiency, unit);
				break;
			default:
				break;
		}

		string lineBreak = (additionsString == "") ? "" : "\n\n";
		additionsString = Tooltips.ColoredText.GetText(additionsString, primaryColor);
		return (defaultString + lineBreak + additionsString);
	}

	public string GetResistanceString(DamageType type, Unit unit)
	{
		return (type + " damage taken reduced by " + Utility.ToPercent(1 - unit.GetResistanceFactor(type)));
	}

	public string GetProficiencyString(StatType proficiencyStat, Unit unit)
	{
		ProficiencyType proficiencyType = unit.GetProficiencyProficiencyType(proficiencyStat);
		return (proficiencyType + " damage increased by " + Utility.ToPercent(unit.GetProficiencyFactor(proficiencyType) - 1));
	}

	public string GetDefaultString(StatType type, Unit unit)
	{
		string nameString = (statNames.GetTypeName(type) + "\n");
		string descriptionString = (descriptions.GetTypeName(type));

		string multString = GetPercentString(unit, type, ModifierType.Multiplier);
		string globalString = GetPercentString(unit, type, ModifierType.GlobalMultiplier);		
		string flatString = (globalString != "" || multString != "")  ? GetFlatString(unit, type, ModifierType.Flat) :  "";
		string totalString = GetTotalString(unit, type);

		return
		(
			Tooltips.ColoredText.GetText(nameString, primaryColor)
			+ Tooltips.ColoredText.GetText(descriptionString + flatString + multString + globalString, secondaryColor)
			+ Tooltips.ColoredText.GetText(totalString, primaryColor)		
		);
	}

	private string GetFlatString(Unit unit, StatType statType, ModifierType modifierType)
	{
		int value = Mathf.CeilToInt(unit.GetStat(statType, modifierType));

		return (value == 0) ? "" : ("\n\n" + descriptions.GetModName(modifierType) + " " +  value);
	}

	private string GetPercentString(Unit unit, StatType statType, ModifierType modifierType)
	{
		float value = unit.GetStat(statType, modifierType);
		float truncValue = Utility.Truncate(value, 2);

		return (truncValue == 0) ?  "" : ("\n" + descriptions.GetModName(modifierType) + " " +  Utility.ToPercent(truncValue));
	}

	private string GetTotalString(Unit unit, StatType type)
	{		
		int value = Mathf.CeilToInt(unit.GetStat(type));
		return (value == 0) ? "" :  ("\n\nTotal: " + " " + value);
	}
}
