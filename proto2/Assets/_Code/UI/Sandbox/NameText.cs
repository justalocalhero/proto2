﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NameText : MonoBehaviour 
{
	public ColorReference nameColor;

	public string GetText(string name)
	{
		return GetText(name, nameColor);
	}

	public string GetText(string name, Color color)
	{
		string toReturn = ("<color=#" + ColorUtility.ToHtmlStringRGBA(color) + ">" + name + "</color>");

		return toReturn;
	}


}
