﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DurationText : MonoBehaviour 
{
	public ColorReference durationColor;

	public string GetText(float durationValue)
	{
		return GetText("", durationValue,"");
	}

	public string GetText(string prefix, float durationValue, string suffix)
	{
		string durationString = durationValue.ToString("n2");

		string toReturn = ("<color=#" + ColorUtility.ToHtmlStringRGBA(durationColor) + ">" + prefix + durationString + "s" + suffix + "</color>");

		return toReturn;
	}
}
