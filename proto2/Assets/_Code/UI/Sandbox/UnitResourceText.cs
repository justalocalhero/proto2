﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UnitResourceText : ColoredText 
{
	public UnitResourceNameDictionary resourceNames;

	public string GetText(UnitResourceAdder resource)
	{
		return GetText(resource, defaultColor.Value);
	}

	public string GetText(UnitResourceAdder resource, Color color)
	{
		int value = resource.value;
		string valueString = (value > 0) ? "Grants " + Mathf.Abs(value) + " ": "Consumes " + Mathf.Abs(value) + " ";
		string typeString = resourceNames.GetString(resource.type);
		string resourceString = (Mathf.Abs(value) > 1) ? "s " : " ";
		string finalString = (valueString + typeString + resourceString);

		return GetText(finalString, color);
	}
	
}
