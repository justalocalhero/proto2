﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatValueText : MonoBehaviour 
{
	public ColorReference statValueColor;
	public StatNameDictionary dictionary;

	public string GetText(EquipStat stat)
	{
		return GetText(stat, 1, statValueColor);
	}

	public string GetText(EquipStat stat, float multiplier)
	{
		return GetText(stat, multiplier, statValueColor);
	}

	public string GetText(EquipStat stat, float multiplier, Color color)
	{
		return GetText(stat.ModifierType, stat.Value, multiplier, color);
	}

	public string GetText(ModifierType modifier, float value, Color color)
	{		
		return GetText(modifier, value, 1, color);
	}

	public string GetText(ModifierType modifier, float value, float multiplier, Color color)
	{
		bool isFlat = modifier == ModifierType.Flat;
		int statInt = isFlat ? (int)Mathf.CeilToInt(value * multiplier) : (int)Mathf.CeilToInt(100 * value * multiplier);
		string valueString = statInt >= 0 ? ("+" + statInt) : ("" + statInt);

		string toReturn = ("<color=#" + ColorUtility.ToHtmlStringRGBA(color) + ">" + valueString + "</color>");

		return toReturn;
	}
}
