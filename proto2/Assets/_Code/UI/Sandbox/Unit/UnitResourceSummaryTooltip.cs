﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitResourceSummaryTooltip : UnitSummaryTooltip
{
	private UnitResource unitResource;
	public UnitResourceNameDictionary resourceNames;
	public UnitResourceNameDictionary resourceDescriptions;
	public StatNameDictionary statNames;

	public void Set(Player unit, UnitResource unitResource)
	{
		if(unitResource == null) return;

		this.unitResource = unitResource;
		Set(unit);
	}

	public override string GetDescriptionText()
	{
		if(unit == null) return "";
		if(unitResource == null) return "";

		return resourceNames.GetString(unitResource.GetResourceType());
	}

	public override string GetValueText()
	{
		if(unit == null) return "";
		if(unitResource == null) return "";

		return "" + unitResource.GetCap();
	}

	public override string GetExpandedText()
	{
		if(unit == null) return "";
		if(unitResource == null) return "";
		
		List<EquipStat> currentStats = Utility.MergeStats(unitResource.GetStats());

		int cap = unitResource.GetCap();
		string nameString = resourceNames.GetString(unitResource.GetResourceType());
		string capString = "Unit can hold " + cap + " " + nameString + "";
		capString += (cap > 1) ? "s" : "";
		string statString = (currentStats.Count > 0) ? "\n\nEach " + nameString + " grants:\n\n" : "";
		string descriptionString = resourceDescriptions.GetString(unitResource.GetResourceType());
		if(descriptionString != "") descriptionString = "\n\n" + descriptionString;


		for(int i = 0; i < currentStats.Count; i++)
		{
			EquipStat currentStat = currentStats[i];
			string valueString = (currentStat.Value >= 0) ? "+" : "";
			valueString += (currentStat.ModifierType == ModifierType.Flat)? currentStat.Value + " ": 
				Utility.ToPercent(currentStat.Value) + " ";

			statString += (valueString + statNames.GetTypeName(currentStat.StatType) + "\n");
		}

		return 
		(
			Tooltips.ColoredText.GetText(capString, primaryColor)
			+ Tooltips.ColoredText.GetText(statString, secondaryColor)
			+ Tooltips.ColoredText.GetText(descriptionString, secondaryColor)
		);
	}
}
