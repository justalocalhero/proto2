﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Tooltips;

public class UnitSummaryTooltip : MonoBehaviour 
{
	protected Color primaryColor, secondaryColor;
	private RectTransform rect;
	public TextMeshProUGUI descriptionMesh;
	public TextMeshProUGUI valueMesh;
	protected Player unit;
	private TextTooltip textTooltip;

	public void Awake()
	{
		rect = transform as RectTransform;
	}

	public void Set(Player unit)
	{
		if(unit == null)
		{
			Clear();
			return;
		}
		this.unit = unit;		
		OnSet();
		descriptionMesh.SetText(GetDescriptionText());
		valueMesh.SetText(GetValueText());
		descriptionMesh.enabled = true;
		valueMesh.enabled = true;
	}

	public void Clear()
	{
		unit = null;
		descriptionMesh.enabled = false;
		valueMesh.enabled = false;
	}
	
	public void SetTextTooltip(TextTooltip textTooltip)
	{
		this.textTooltip = textTooltip;
	}

	public void SetPrimaryColor(Color color)
	{
		primaryColor = color;
	}

	public void SetSecondaryColor(Color color)
	{
		secondaryColor = color;
	}

	public virtual void OnSet()
	{

	}
	
	public virtual string GetDescriptionText()
	{
		return "";
	}

	public virtual string GetValueText()
	{
		return "";
	}

	public virtual string GetExpandedText()
	{
		return "";
	}

	public virtual void OnPointerEnter()
	{
		textTooltip.Set(GetExpandedText(), rect);
	}

	public virtual void OnPointerExit()
	{
		textTooltip.Hide();
	}
}
