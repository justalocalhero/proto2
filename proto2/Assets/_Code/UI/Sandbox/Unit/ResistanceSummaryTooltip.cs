﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ResistanceSummaryTooltip : UnitSummaryTooltip
{
    public ColorReference labelColor, statColor, invisibleColor;
	private int damageTypes = Enum.GetNames(typeof(DamageType)).Length;
	private List<ResistanceSummary> resistanceSummaries;
	private float averageResistance;

	public override void OnSet()
	{		
		CalculateResistanceSummary();
	}

	public override string GetDescriptionText()
	{
		return Tooltips.ColoredText.GetText("Resistance", primaryColor);
	}

	public override string GetValueText()
	{
		string valueString = (resistanceSummaries.Count > 0) ? "" + Utility.ToPercent(averageResistance) : "None";
		
		return Tooltips.ColoredText.GetText(valueString, primaryColor);
	}

	public override string GetExpandedText()
	{
		string headerText = (resistanceSummaries.Count > 0) ? "Resistances\n\n" : "No Resistances";
		string summaryText = "";

		for(int i = 0; i < resistanceSummaries.Count; i++)
		{
			summaryText += GetSummaryText(resistanceSummaries[i]) + "\n";
		}

		headerText = Tooltips.ColoredText.GetText(headerText, primaryColor);
		summaryText = Tooltips.ColoredText.GetText(summaryText, secondaryColor);

		return 
		(
			Tooltips.ColoredText.GetText(headerText, primaryColor)
			+ Tooltips.ColoredText.GetText(summaryText, secondaryColor)
		);
	}

	public string GetSummaryText(ResistanceSummary summary)
	{
		return Tooltips.TwoColumnText.GetText("" + summary.type + " resistance", labelColor, Utility.ToPercent(summary.value), statColor, invisibleColor); 
	}

	public void CalculateResistanceSummary()
	{
		resistanceSummaries = new List<ResistanceSummary>();
		if(unit == null) return;

		int resistanceCount = 0;
		float resistanceTotal = 0;

		for(int i = 0; i < damageTypes; i++)
		{
			DamageType currentType = (DamageType)i;

			if(currentType != DamageType.healing && currentType != DamageType.physical)
			{
				resistanceCount++;
				float factor = 1 - unit.GetResistanceFactor(currentType);

				if(factor > 0)
				{
					resistanceTotal += factor;
					resistanceSummaries.Add(new ResistanceSummary{
						type = currentType,
						value = factor,
					});
				}
			}
		}

		averageResistance = (resistanceCount > 0) ? resistanceTotal / resistanceCount : 0;
	}

	public struct ResistanceSummary {public DamageType type; public float value;}
}
