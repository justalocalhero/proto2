﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Tooltips;

public class UnitStatTab : UnitTooltipTab 
{	
	private List<UnitStatSummaryTooltip> unitStats = new List<UnitStatSummaryTooltip>();
	public ExpandedStatTooltip expandedStatTooltip;
	public StatOrderDictionary[] statsToShow;
	public TextMeshProUGUI titleMesh;
	public UnitStatSummaryTooltip baseTooltip;
	public Transform resourceTransform;
	public IntReference resourceCount;
	public FloatReference resourceHeight, paddingTop;
	public TextTooltip textTooltip;
	public ColorReference primaryColor, secondaryColor;

	public override void Start()
	{
		RectTransform rect = transform as RectTransform;
		float xLeft = 0 * rect.rect.width;
		float xRight = .5f * rect.rect.width;

		for(int i = 0; i <  resourceCount; i++)
		{
			UnitStatSummaryTooltip temp = Object.Instantiate(baseTooltip, resourceTransform);
			(temp.transform as RectTransform).anchoredPosition = new Vector3(xLeft, -i * resourceHeight - paddingTop);
			
			temp.expandedStatTooltip = expandedStatTooltip;
			unitStats.Add(temp);
			
		}

		for(int i = 0; i <  resourceCount; i++)
		{
			UnitStatSummaryTooltip temp = Object.Instantiate(baseTooltip, resourceTransform);
			(temp.transform as RectTransform).anchoredPosition = new Vector3(xRight, -i * resourceHeight - paddingTop);
			temp.expandedStatTooltip = expandedStatTooltip;
			unitStats.Add(temp);
			
		}
		RegisterTooltips();
		base.Start();

	}

	public void RegisterTooltips()
	{
		foreach(UnitSummaryTooltip stat in unitStats)
		{
			stat.SetTextTooltip(textTooltip);
			stat.SetPrimaryColor(primaryColor);
			stat.SetSecondaryColor(secondaryColor);
		}
	}

	public override void UpdateUI()
	{
		if(unit == null) return;

		SetStats();
		titleMesh.SetText("Statistics");
	}

	private void SetStats()
	{

		int currentIndex = 0;
		float currentValue = 0;
		StatType currentType;

		for(int i = 0; i < statsToShow.Length; i++)
		{
			for(int j = 0; j < statsToShow[i].stats.Length; j++)
			{
				currentType = statsToShow[i].stats[j];
				currentValue = unit.GetStat(currentType);

				if(currentValue != 0)
				{
					if(currentIndex >= unitStats.Count)
					{
						Debug.LogWarning("Insufficient UnitStatText provided for UnitTooltip to fully update.");
						break;
					}
					else
					{
						unitStats[currentIndex++].Set(unit, currentType);
					}
				}
			}
		}

		for(int i = currentIndex; i < unitStats.Count; i++)
		{
			unitStats[i].Clear();
		}		
	}
}
