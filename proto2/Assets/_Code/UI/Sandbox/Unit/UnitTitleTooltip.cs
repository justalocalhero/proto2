﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UnitTitleTooltip : MonoBehaviour 
{
	public TextMeshProUGUI mesh;
	private Unit unit;

	public void Set(Unit unit)
	{
		if(unit == null) return;
		this.unit = unit;

		mesh.SetText(GetText(unit));
	}

	public void Clear()
	{
		unit = null;

		mesh.SetText("");
	}

	public string GetText(Unit unit)
	{
		return "";
	}
}
