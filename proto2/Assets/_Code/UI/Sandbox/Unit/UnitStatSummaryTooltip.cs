﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitStatSummaryTooltip : UnitSummaryTooltip
{
	private StatType statType;
	public StatNameDictionary statNames;
	public ExpandedStatTooltip expandedStatTooltip;

	public void Set(Player unit, StatType stat)
	{
		statType = stat;
		Set(unit);
	}

	public override string GetDescriptionText()
	{
		if(unit == null) return "";

		return statNames.GetTypeName(statType);
	}

	public override string GetValueText()
	{
		if(unit == null) return "";

		return "" + Mathf.CeilToInt(unit.GetStat(statType));
	}

	public override string GetExpandedText()
	{
		if(unit == null) return "";

		return expandedStatTooltip.GetText(statType, unit);
	}
}
