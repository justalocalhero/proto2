﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmorSummaryTooltip : UnitSummaryTooltip
{
	public override string GetDescriptionText()
	{
		if(unit == null) return "";
		
		return "Armor";
		
	}

	public override string GetValueText()
	{
		if(unit == null) return "";

		float armor = 1 - unit.GetResistanceFactor(DamageType.physical);

		return (armor > 0) ? "" + Utility.ToPercent(armor) : "Unarmored";
	}

	public override string GetExpandedText()
	{
		if(unit == null) return "";

		float total = Mathf.CeilToInt(unit.GetStat(StatType.Armor));
		float flat = Mathf.CeilToInt((unit.GetStat(StatType.Armor, ModifierType.Flat)));
		float mult = Utility.Truncate(unit.GetStat(StatType.Armor, ModifierType.Multiplier), 2);
		float global = Utility.Truncate(unit.GetStat(StatType.Armor, ModifierType.GlobalMultiplier), 2);		
		float percent = 1 - unit.GetResistanceFactor(DamageType.physical);

		string headerText = "Armor";
		string multText = (mult == 0) ? "" : "\nEquipment Multiplier: " + Utility.ToPercent(mult);
		string globalText = (global == 0) ? "" : "\nGlobal Multiplier: " + Utility.ToPercent(global);
		string flatText = (mult != 0 || global != 0) ? "\n\nFlat: " + Mathf.CeilToInt(flat) : "";
		string totalText = "\n\nTotal: " + Mathf.CeilToInt(total);
		string percentText = "\n\nPhysical Resitance: " + Utility.ToPercent(percent);

		return 
		(
			Tooltips.ColoredText.GetText(headerText, primaryColor)
			+ Tooltips.ColoredText.GetText(flatText + multText + globalText, secondaryColor)
			+ Tooltips.ColoredText.GetText(totalText + percentText, primaryColor)
		);
	}
}
