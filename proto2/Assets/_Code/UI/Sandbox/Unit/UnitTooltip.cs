﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UnitTooltip : MonoBehaviour 
{
	private Player unit;
	private UIToggler toggler;
	public Transform TabContainer;
	private UnitTooltipTab[] tooltipTabs;
	private UnitTooltipTab currentTab;

	public void Start()
	{
		tooltipTabs = GetComponentsInChildren<UnitTooltipTab>();
		toggler = GetComponent<UIToggler>();
		toggler.onShow  += ResetTab;
		SetTabIndeces();
		ResetTab();
	}

	public void Set(Player newUnit)
	{
		if(newUnit == null) return;

		unit = newUnit;
		
		for(int i = 0; i < tooltipTabs.Length; i++)
		{
			tooltipTabs[i].Set(unit);
		}
	}

	public void SetTab(UnitTooltipTab newTab)
	{
		if(newTab == null) return;

		if(currentTab.GetIndex() != newTab.GetIndex())
		{
			currentTab.Hide();
			currentTab = newTab;
			currentTab.Show();
		}
	}

	public void SetTab(int index)
	{
		if(index < 0 || index >= tooltipTabs.Length) return;

		SetTab(tooltipTabs[index]);
	}

	public void OnMenuButtonPressed()
	{
		ResetTab();
	}

	public void SetTabIndeces()
	{
		for(int i = 0; i < tooltipTabs.Length; i++)
		{
			tooltipTabs[i].SetIndex(i);
		}
	}

	public void ResetTab()
	{
		if(currentTab != null) currentTab.Hide();
		if(tooltipTabs.Length > 0) 
		{
			currentTab = tooltipTabs[0];
			currentTab.Show();
		}

	}

}