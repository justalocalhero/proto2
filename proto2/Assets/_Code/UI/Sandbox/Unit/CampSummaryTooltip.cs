﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampSummaryTooltip : UnitSummaryTooltip
{	
	public ColorReference summaryColor;

	public override string GetDescriptionText()
	{
		if(unit == null) return "";		

		return Tooltips.ColoredText.GetText("Camp Healing: ", summaryColor);
	}

	public override string GetValueText()
	{
		if(unit == null) return "";

		return Tooltips.ColoredText.GetText(Mathf.RoundToInt(unit.GetCampHealing()) + " HP", summaryColor);
	}

	public override void OnPointerEnter()
	{

	}

	public override void OnPointerExit()
	{

	}

}
