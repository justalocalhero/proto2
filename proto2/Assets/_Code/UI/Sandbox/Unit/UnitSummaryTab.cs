﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Tooltips;

public class UnitSummaryTab : UnitTooltipTab
{
    private UnitSummaryTooltip[] summaryTooltips;
	public TextMeshProUGUI titleMesh;
	public TextTooltip textTooltip;
	public ColorReference primaryColor, secondaryColor;

	public override void Start()
	{
		base.Start();
		summaryTooltips = GetComponentsInChildren<UnitSummaryTooltip>();
		RegisterTooltip();
	}

	public void RegisterTooltip()
	{
		foreach(UnitSummaryTooltip tooltip in summaryTooltips)
		{
			tooltip.SetTextTooltip(textTooltip);
			tooltip.SetPrimaryColor(primaryColor);
			tooltip.SetSecondaryColor(secondaryColor);
		}
	}

	public override void UpdateUI()
	{
		if(unit == null) return;
		
		titleMesh.SetText(GetClassText());

		for(int i = 0; i < summaryTooltips.Length; i++)
		{
			summaryTooltips[i].Set(unit);
		}
	}

	public void ClearSummaryTooltips()
	{
		for(int i = 0; i < summaryTooltips.Length; i++)
		{
			summaryTooltips[i].Clear();
		}
	}

	public string GetClassText()
	{
		if(unit == null) return "";

		string toReturn = "";

		for(int i = 0; i < unit.raceClasses.Count; i++)
		{
			toReturn += unit.raceClasses[i].GetName() + " ";
		}

		return toReturn;
	}
}
