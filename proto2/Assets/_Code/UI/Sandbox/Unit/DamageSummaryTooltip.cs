﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DamageSummaryTooltip : UnitSummaryTooltip 
{
    public ColorReference labelColor, statColor, invisibleColor;
    private int damageTypes = Enum.GetNames(typeof(DamageType)).Length;
    private List<DamageSummary> damageSummaries;
    private DamageType maxType;

	public override string GetDescriptionText()
	{
        if(unit == null) return "";

        return "Prefered Damage Type: ";

        
	}

	public override string GetValueText()
	{
        if(unit == null) return "";
        
        return "" + maxType;


	}

	public override string GetExpandedText()
	{
        if(unit == null) return "";

        string headerString = (damageSummaries.Count > 0) ? "Damage Types\n\n" : "";
        string summaryString = "";

        for(int i = 0; i < damageSummaries.Count; i++)
        {
            summaryString += GetSummaryText(damageSummaries[i]) + "\n";
        }

        return (headerString + summaryString);
	}

    public string GetSummaryText(DamageSummary summary)
    {
        return Tooltips.TwoColumnText.GetText(GetTypeString(summary), labelColor, GetValueString(summary), statColor, invisibleColor);
    }

    public string GetTypeString(DamageSummary summary)
    {
        return summary.type + " ";
    }

    public string GetValueString(DamageSummary summary)
    {
        string toReturn = "";

        toReturn += (summary.flat > 0) ? "+" + Utility.Truncate(summary.flat, 2) + " " : "";
        toReturn += (summary.mult > 0) ? "x" + Utility.Truncate(summary.mult, 2) + " " : "";

        return toReturn;

    }

    public override void OnSet()
    {
        CalculateDamageSummaries();
    }

    public void CalculateDamageSummaries()
    {
        damageSummaries = new List<DamageSummary>();

        float maxFactor = -1;

        for(int i = 0; i < damageTypes; i++)
        {
            DamageType current = (DamageType)i;
            if(current != DamageType.healing)
            {                
                StatType damageStatType = unit.GetDamageStat((DamageType)i);
                float flat = unit.GetStat(damageStatType, ModifierType.Flat);
                float mult = unit.GetStat(damageStatType, ModifierType.Multiplier);
                float global = unit.GetStat(damageStatType, ModifierType.GlobalMultiplier);
                float totalMult = (1 + mult) * (1 + global);

                //arbitrarily calculating with a 5 + 1x attack
                float factor = (flat + 5) * totalMult;
                
                if(factor > maxFactor) 
                {
                    maxFactor = factor;
                    maxType = current;
                }

                if(flat > 0 || totalMult > 1)
                {
                    damageSummaries.Add(new DamageSummary{
                        type = current,
                        flat = flat,
                        mult = totalMult,
                    });
                }
            }
        }
    }

    public struct DamageSummary {public DamageType type; public float flat; public float mult;}
}
