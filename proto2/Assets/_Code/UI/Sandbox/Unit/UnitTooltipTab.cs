﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitTooltipTab : MonoBehaviour 
{
	public UIToggler toggler;
	protected Player unit;
	protected int tabIndex;

	public virtual void Start()
	{
		toggler = GetComponent<UIToggler>();
		toggler.onShow += UpdateUI;
	}

	public void Set(Player unit)
	{
		if(unit == null)
		{
			Clear();
			return;
		}

		this.unit = unit;
		UpdateUI();

	}

	public void SetIndex(int newIndex)
	{
		tabIndex = newIndex;
	}

	public int GetIndex()
	{
		return tabIndex;
	}

	public virtual void UpdateUI()
	{

	}

	public void Clear()
	{
		this.unit = null;
		Hide();
	}

	public virtual void Show()
	{
		toggler.Show();
	}

	public virtual void Hide()
	{
		toggler.Hide();
	}
}
