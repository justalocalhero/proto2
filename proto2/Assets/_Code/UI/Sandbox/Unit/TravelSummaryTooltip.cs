﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TravelSummaryTooltip : UnitSummaryTooltip 
{	
	public ColorReference summaryColor;
	public FloatVariable travelTime;

	public override string GetDescriptionText()
	{
		if(unit == null) return "";		

		return Tooltips.ColoredText.GetText("Travel Time: ", summaryColor);
	}

	public override string GetValueText()
	{
		if(unit == null) return "";

		return Tooltips.ColoredText.GetText(Utility.Truncate(travelTime.Value / unit.GetWalkSpeedFactor(), 2) + "s", summaryColor);
	}

	public override void OnPointerEnter()
	{

	}

	public override void OnPointerExit()
	{

	}
}
