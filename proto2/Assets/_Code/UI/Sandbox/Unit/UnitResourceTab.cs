﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Tooltips;

public class UnitResourceTab : UnitTooltipTab
{
	private List<UnitResourceSummaryTooltip> resourceTooltips = new List<UnitResourceSummaryTooltip>();
	public TextMeshProUGUI titleMesh;
	public UnitResourceSummaryTooltip baseTooltip;
	public Transform resourceTransform;
	public TextTooltip textTooltip;
	public ColorReference primaryColor, secondaryColor;
	public IntReference resourceCount, spacing;

	public override void Start()
	{
		BuildSlots();
		base.Start();

	}

	public void BuildSlots()
	{
		GridBuilder<UnitResourceSummaryTooltip> gridBuilder = new GridBuilder<UnitResourceSummaryTooltip>();

		resourceTooltips = gridBuilder.Begin()
			.WithPrototype(baseTooltip)
			.WithContainer(resourceTransform)
			.WithSpacing(spacing, spacing)
			.WithRows(resourceCount)
			.WithColumns(1)
			.Build();

		RegisterTooltips();
	}

	private void RegisterTooltips()
	{
		foreach(UnitResourceSummaryTooltip tooltip in resourceTooltips)
		{
			tooltip.SetTextTooltip(textTooltip);
			tooltip.SetPrimaryColor(primaryColor);
			tooltip.SetSecondaryColor(secondaryColor);
		}
	}

	public override void UpdateUI()
	{
		if(unit == null) return;

		List<UnitResource> unitResources = unit.GetResourceAvailableList();

		for(int i = 0; i < resourceTooltips.Count; i++)
		{
			if(i < unitResources.Count) resourceTooltips[i].Set(unit, unitResources[i]);
			else resourceTooltips[i].Clear();
		}
				
		titleMesh.SetText("Combat Resources");
	}
}
