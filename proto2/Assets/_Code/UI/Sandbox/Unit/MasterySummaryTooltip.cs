﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MasterySummaryTooltip : UnitSummaryTooltip
{
    public ColorReference labelColor, statColor, invisibleColor;
	private List<ProficiencySummary> proficiencySummaries;
	private ProficiencyType maxType;
	private int proficiencyTypes = Enum.GetNames(typeof(ProficiencyType)).Length;

	public override void OnSet()
	{
		CalculateProficiencySummaries();
	}

	public override string GetDescriptionText()
	{
		if(unit == null) return "";
		return "Preferred Proficiency Type: ";
	}

	public override string GetValueText()
	{
		if(unit == null) return "";
		return (proficiencySummaries.Count > 0) ? "" + maxType : "None";
	}

	public override string GetExpandedText()
	{
		if(unit == null) return "";

		string headerText = (proficiencySummaries.Count > 0) ? "Proficiencies\n\n" : "Not Proficient";
		string summaryText = "";

		for(int i = 0; i < proficiencySummaries.Count; i++)
		{
			summaryText += GetSummaryText(proficiencySummaries[i]) + "\n";
		}

		return 
		(
			Tooltips.ColoredText.GetText(headerText, primaryColor) 
			+ Tooltips.ColoredText.GetText(summaryText, secondaryColor)
		);
	}

	public string GetSummaryText(ProficiencySummary summary)
	{
		return Tooltips.TwoColumnText.GetText("" + summary.type, labelColor, Utility.ToPercent(summary.value), statColor, invisibleColor); 
	}

	public void CalculateProficiencySummaries()
	{
		proficiencySummaries = new List<ProficiencySummary>();
		if(unit == null) return;

		float maxFactor = -1;

		for(int i = 0; i < proficiencyTypes; i++)
		{
			ProficiencyType currentType = (ProficiencyType)i;
			float factor = unit.GetProficiencyFactor(currentType) - 1;

			if(factor > maxFactor)
			{
				maxType = currentType;
				maxFactor = factor;
				
			}
			if(factor > 0)
			{
				proficiencySummaries.Add(new ProficiencySummary{
					type = currentType,
					value = factor,
				});
			}	
		}
	}

	public struct ProficiencySummary{public ProficiencyType type; public float value;}
}
