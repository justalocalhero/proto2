﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionSummaryTooltip : UnitSummaryTooltip 
{
	public ColorReference summaryColor;
	public IntVariable potionCount;

	public override string GetDescriptionText()
	{
		if(unit == null) return "";

		int potions = potionCount.Value;

		if(potions <= 0) return "";

		return Tooltips.ColoredText.GetText("Potions: ", summaryColor);
	}

	public override string GetValueText()
	{
		if(unit == null) return "";

		int potions = potionCount.Value;

		if(potions <= 0) return "";

		return Tooltips.ColoredText.GetText(potions.ToString(), summaryColor);
	}

	public override void OnPointerEnter()
	{

	}

	public override void OnPointerExit()
	{

	}
}
