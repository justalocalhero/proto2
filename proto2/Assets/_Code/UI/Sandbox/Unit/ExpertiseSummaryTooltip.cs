﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpertiseSummaryTooltip : UnitSummaryTooltip
{
	private float expertise;

	public override string GetDescriptionText()
	{
		if(unit == null) return "";

		return "Combat Expertise";
	}

	public override string GetExpandedText()
	{
		if(unit == null) return "";

		float speed = unit.GetSpeedFactor();
		bool hasSpeed = speed != 1;
		bool isFaster = speed >= 1;

		float adjustedSpeed = (isFaster) ? speed - 1 : (1 / speed) - 1;

		float critChance = unit.GetCritChance();
		float critFactor = unit.GetCritFactor(true);
		
		bool hasCrit = critChance > 0;



		string headerText = (hasSpeed || hasCrit) ? "Combat Expertise\n\n" : "No Expertise";
		string speedText = "";
		if(hasSpeed)
		{
			speedText = Utility.ToPercent(adjustedSpeed) + " ";
			speedText += (isFaster) ? "Faster\n" : "Slower\n";
		}

		string critChanceText = (!hasCrit) ? "" : Utility.ToPercent(Mathf.Clamp(critChance, 0, critChance)) + " Chance To Critically Strike\n";
		string critFactorText = (!hasCrit) ? "" : "Critical Strikes Deal " + Utility.ToX(critFactor) + " Damage";
		
		return 
		(
			Tooltips.ColoredText.GetText(headerText, primaryColor)
			+ Tooltips.ColoredText.GetText(speedText + critChanceText + critFactorText, secondaryColor)
		);

	}

	public override string GetValueText()
	{
		if(unit == null) return "";

		float speed = unit.GetSpeedFactor();
		float critChance = unit.GetCritChance();
		float critFactor = unit.GetCritFactor(true);

		float total = (speed) * (1 + (critChance * critFactor)) - 1;

		return (total > 0) ? "" + Utility.ToPercent(total) : "None";		
	}
}
