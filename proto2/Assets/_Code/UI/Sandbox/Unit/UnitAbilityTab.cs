﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UnitAbilityTab : UnitTooltipTab 
{
	private AbilityIcon[] abilityIcons;	
	public TextMeshProUGUI titleMesh;
	public Transform abilityIconContainer;
	public Transform abilityTooltipContainer;

	public override void Start()
	{
		base.Start();
		abilityIcons = abilityIconContainer.GetComponentsInChildren<AbilityIcon>();
		PassReferences();
	}

	public void PassReferences()
	{
		for(int i = 0; i < abilityIcons.Length; i++)
		{
			abilityIcons[i].SetTooltipContainer(abilityTooltipContainer);
		}
	}

	public override void UpdateUI()
	{
		if(unit == null) return;
		SetAbilities();
		titleMesh.SetText("Abilities");
	}

	private void SetAbilities()
	{
		int abilityCount = 0;

		for(int i = 0; i < unit.actions.Count; i++)
		{
			if(abilityCount < abilityIcons.Length)
			{
				abilityIcons[abilityCount++].Set(unit.actions[i] as Ability, unit);
			}
			else
			{
				Debug.LogWarning("Insufficient AbilityIcon provided for UnitTooltip to fully update.");
				i = unit.actions.Count;
			}
		}

		for(int i = 0; i < unit.triggeredActions.Length; i++)
		{
			for(int j = 0; j < unit.triggeredActions[i].actions.Count; j++)
			{
				if(abilityCount < abilityIcons.Length)
				{
					abilityIcons[abilityCount++].Set(unit.triggeredActions[i].actions[j], unit);
				}
				else
				{
					Debug.LogWarning("Insufficient AbilityIcon provided for UnitTooltip to fully update.");
					break;
				}
			}
		}

		for( ; abilityCount < abilityIcons.Length; abilityCount++)
		{
			abilityIcons[abilityCount].Clear();
		}
	}
}
