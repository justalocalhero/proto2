﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitpointSummaryTooltip : UnitSummaryTooltip
{

	public override string GetDescriptionText()
	{
		if(unit == null) return "";

		string descriptionText = "Hitpoints";

		return descriptionText;
	}

	public override string GetValueText()
	{
		if(unit == null) return "";

		int hitpoints = Mathf.CeilToInt(unit.GetMaxHitpoints());

		string valueText = "" + hitpoints;

		return valueText;
	}

	public override string GetExpandedText()
	{
		if(unit == null) return "";

		float total = Mathf.CeilToInt(unit.GetStat(StatType.Hitpoints));
		float flat = Mathf.CeilToInt((unit.GetStat(StatType.Hitpoints, ModifierType.Flat)));
		float mult = Utility.Truncate(unit.GetStat(StatType.Hitpoints, ModifierType.Multiplier), 2);
		float global = Utility.Truncate(unit.GetStat(StatType.Hitpoints, ModifierType.GlobalMultiplier), 2);

		string headerText = "Max Hitpoints";
		string multText = (mult == 0) ? "" : "\nEquipment Multiplier: " + Utility.ToPercent(mult);
		string globalText = (global == 0) ? "" : "\nGlobal Multiplier: " + Utility.ToPercent(global);
		string flatText = (mult != 0 || global != 0) ? "\n\nFlat: " + Mathf.CeilToInt(flat) : "";
		string totalText = "\n\nTotal: " + Mathf.CeilToInt(total);

		return 
		(
			Tooltips.ColoredText.GetText(headerText, primaryColor) 
			+ Tooltips.ColoredText.GetText(flatText + multText + globalText, secondaryColor) 
			+ Tooltips.ColoredText.GetText(totalText, primaryColor)
		);
	}
}
