﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionConditionText : ColoredText 
{
	public ActionConditionNameDictionary conditionNames;

	public string GetText(ActionCondition condition)
	{
		return GetText(condition, defaultColor.Value);
	}

	public string GetText(ActionCondition condition, Color color)
	{
		if(condition.type == ConditionType.always) return "";
		string conditionString = conditionNames.GetConditionString(condition.value, condition.target, condition.type,condition.comparison);

		return GetText(conditionString, color);
	}
}
