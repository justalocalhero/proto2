﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProficiencyText : ColoredText
{

	public string GetText(ProficiencyType type)
	{
		return GetText(type, defaultColor.Value);
	}

	public string GetText(ProficiencyType type, Color color)
	{
		return GetText("" + type, color);
	}
}
