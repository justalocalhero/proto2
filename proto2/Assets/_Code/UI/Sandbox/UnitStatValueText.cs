﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitStatValueText : MonoBehaviour
{

	public ColorReference statValueColor;
	public StatNameDictionary dictionary;

	public string GetText(StatType type, Unit unit)
	{
		return GetText(type, unit.GetStat(type));
	}
	public string GetText(StatType type, float value)
	{
		int valueInt = (int)Mathf.Round(value);

		string toReturn = ("<color=#" + ColorUtility.ToHtmlStringRGBA(statValueColor) + ">" + valueInt + "</color>");

		return toReturn;
	}
}
