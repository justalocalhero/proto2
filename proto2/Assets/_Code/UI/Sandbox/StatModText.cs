﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tooltips;

public class StatModText : MonoBehaviour
{

	public ColorReference statModColor;
	public StatNameDictionary dictionary;
	
	public string GetText(EquipStat stat)
	{
		return GetText(stat, statModColor);
	}

	public string GetText(EquipStat stat, Color color)
	{
		return GetText(stat.ModifierType, color);
	}

	public string GetText(StatComparison stat, Color color)
	{
		return GetText(stat.stat.ModifierType, color);
	}

	public string GetText(ModifierType modifier, Color color)
	{
		string modString = dictionary.GetModName(modifier);

		string toReturn = ("<color=#" + ColorUtility.ToHtmlStringRGBA(color) + ">" + modString + "</color>");

		return toReturn;
	}
}
