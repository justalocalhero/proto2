﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DispelText : MonoBehaviour 
{

	public ColorReference dispelColor;

	public string GetText(int dispelCount)
	{
		string prefix = "Dispel ";
		string suffix = dispelCount == 1 ? " Debuff" : " Debuffs";

		string toReturn = ("<color=#" + ColorUtility.ToHtmlStringRGBA(dispelColor) + ">" + prefix + dispelCount + suffix + "</color>");

		return toReturn;
	}
}
