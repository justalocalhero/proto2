﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurgeText : MonoBehaviour 
{
	public ColorReference purgeColor;

	public string GetText(int purgeCount)
	{
		string prefix = "Purge ";
		string suffix = purgeCount == 1 ? " Buff" : " Buffs";

		string toReturn = ("<color=#" + ColorUtility.ToHtmlStringRGBA(purgeColor) + ">" + prefix + purgeCount + suffix + "</color>");

		return toReturn;
	}
}
