﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageText : MonoBehaviour 
{
	public DamageValueText damageValueText;
	public DamageTypeText damageTypeText;
	public DamageScaleText damageScaleText;

	public string GetText(Damage damage, float scaleValue)
	{
		string valueString = damageValueText.GetText(damage) + " ";
		string typeString = damageTypeText.GetText(damage) + " ";
		string scaleString = scaleValue != 0  ? "+" + damageScaleText.GetText(scaleValue) + " " : "";

		return valueString + scaleString + typeString;
	}

	public string GetText(Damage damage)
	{
		return GetText(damage, 0);
	}

	public string GetText(Damage damage, ProficiencyType proficiencyType, Unit unit)
	{
		damage = unit.ScaleDamage(damage, proficiencyType);
		
		string valueString = damageValueText.GetText(damage) + " ";
		string typeString = damageTypeText.GetText(damage) + " ";

		return valueString + typeString;
	}

	public string GetText(Damage damage, TriggeredEvent triggeredEvent)
	{
		string valueString = (damageValueText.GetText(damage) + " ");
		string scaleString = "";
		if(triggeredEvent.effectScaling != 0) scaleString =  ("(+ " + Mathf.Round(triggeredEvent.effectScaling * damage.value) + " per Level) ");
		string typeString = (damageTypeText.GetText(damage) + " ");

		return (valueString + scaleString + typeString);
	}
}
