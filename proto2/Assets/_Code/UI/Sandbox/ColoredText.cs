﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColoredText : MonoBehaviour {

	public ColorReference defaultColor;

	public string GetText(string text)
	{
		return GetText(text, defaultColor.Value);
	}

	public string GetText(string text, Color color)
	{
		string toReturn = ("<color=#" + ColorUtility.ToHtmlStringRGBA(color) + ">" + text + "</color>");

		return toReturn;
	}

}
