﻿
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(DamageColor))]
public class DamageColorDrawer : PropertyDrawer
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int typeSize = 120;
        int colorSize = 80;
        int spacing = 5;
        int typeIndent =  0;
        int colorIndent = typeIndent + typeSize + spacing;

        Rect typeRect = new Rect(position.x + typeIndent, position.y, typeSize, position.height);
        Rect colorRect = new Rect(position.x + colorIndent, position.y, colorSize, position.height);

        EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("damageType"), GUIContent.none);
        EditorGUI.PropertyField(colorRect, property.FindPropertyRelative("color"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
