﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DamageValueText : MonoBehaviour 
{
	public ColorReference damageColor;
	public ColorReference healingColor;

	public string GetText(Damage damage)
	{
		Color color = (damage.type == DamageType.healing) ? healingColor : damageColor;

		int roundedValue = (int)Mathf.Round(damage.value);

		string toReturn = ("<color=#" + ColorUtility.ToHtmlStringRGBA(color) + ">" + roundedValue + "</color>");

		return toReturn;
	}

}
