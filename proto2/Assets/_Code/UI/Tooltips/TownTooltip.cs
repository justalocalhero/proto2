﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Tooltips;

public class TownTooltip : MonoBehaviour 
{
	public UIToggler toggler;
	public TextMeshProUGUI mesh;
	public ColorReference descriptionColor;

	public void Set(TownBuilding townBuilding)
	{
		mesh.SetText(GetText(townBuilding));
		Show();

	}

	public void Clear()
	{
		mesh.SetText("");
		Hide();
	}

	public void Show()
	{
		toggler.Show();
	}

	public void Hide()
	{
		toggler.Hide();
	}

	public string GetText(TownBuilding townBuilding)
	{
		string nameString = SizedText.GetText(townBuilding.GetName(),2);
		string descriptionString = "\n\n"  + Tooltips.ColoredText.GetText(townBuilding.GetDescription(), descriptionColor);

		return nameString + descriptionString;
	}
}
