﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RaceClassText : Tooltip 
{
	public TextMeshProUGUI mesh;
	public StatText statText;
	public NameText nameText;
	public Image backgroundImage;
	public Transform abilityIconContainer;
	public Transform abilityTooltipContainer;
	private AbilityIcon[] abilityIcons;

	public void Start()
	{
		abilityIcons = abilityIconContainer.GetComponentsInChildren<AbilityIcon>();
		PassReferences();
		tooltipType = "RaceClass";
	}

	public void PassReferences()
	{
		for(int i = 0; i < abilityIcons.Length; i++)
		{
			abilityIcons[i].SetTooltipContainer(abilityTooltipContainer);
		}
	}

	public void Set(RaceClass raceClass)
	{
		StartSet();

		mesh.SetText(GetText(raceClass));
		

		int abilityCount = raceClass.abilities.Length;

		for(int i = 0; i < abilityIcons.Length; i++)
		{

			if(i < abilityCount) 
				abilityIcons[i].Set(raceClass.abilities[i].Generate());
			else if(i < (abilityCount + raceClass.triggeredActions.Length)) 
				abilityIcons[i].Set(raceClass.triggeredActions[i - abilityCount].Generate());
			else 
				abilityIcons[i].Clear();
		}

		StartResize();
	}

	public override void Clear()
	{
		base.Clear();
		mesh.SetText("");
		for(int i = 0; i < abilityIcons.Length; i++)
		{
			abilityIcons[i].Clear();
		}
		HideUI();
	}

	public string GetText(RaceClass raceClass)
	{
		string nameString = "<align=center>" + nameText.GetText(raceClass.name) + "</align>\n\n";
		string textString =  "";

		for(int i = 0; i < raceClass.staticStats.Length; i++)
		{
			textString += (statText.GetText(raceClass.staticStats[i]) + "\n");
		}

		return (nameString + textString);
	}
}
