﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TownUpgradeTooltip : Tooltip 
{

	public TextMeshProUGUI mesh;

	public void Start()
	{

		tooltipType = "TownUpgrade";
	}

	public void Set(TownUpgrade townUpgrade)
	{
		StartSet();

		mesh.SetText(GetText(townUpgrade));

		StartResize();
	}

	public override void Clear()
	{
		base.Clear();
		mesh.SetText("");
		HideUI();
	}

	public string GetText(TownUpgrade townUpgrade)
	{
		string nameString = townUpgrade.GetNameString() + "\n";
		string costString = townUpgrade.GetCostString() + "\n\n";
		string descriptionString = townUpgrade.GetDescriptionString();

		return nameString + costString + descriptionString;
	}
}
