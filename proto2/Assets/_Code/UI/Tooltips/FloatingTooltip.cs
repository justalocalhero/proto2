﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingTooltip : Tooltip 
{
	private RectTransform rectTransform;
	private RectTransform targetRect;

	public virtual void Start()
	{
		rectTransform = (RectTransform) transform;
	}

	protected override void ShowUI()
	{
		if(targetRect != null) 
		{
			MoveToRect(targetRect);
			targetRect = null;
		}
		base.ShowUI();
	}

	public void Move(RectTransform targetRect)
	{
		this.targetRect = targetRect;
	}

	private void MoveToRect(RectTransform targetRect)
	{
		rectTransform.position = targetRect.position;
		Vector2 targetPivot = targetRect.pivot;
		Vector2 selfPivot = rectTransform.pivot;

		rectTransform.localPosition = new Vector3(
			rectTransform.localPosition.x + (selfPivot.x) * rectTransform.rect.width - (targetPivot.x) * targetRect.rect.width,
			rectTransform.localPosition.y + (selfPivot.y) * rectTransform.rect.height - (targetPivot.x) * targetRect.rect.height,
			0);
	}
}
