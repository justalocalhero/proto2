﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tooltip : MonoBehaviour 
{
	public UIToggler toggler;
	protected List<Tooltip> children = new List<Tooltip>();
	public Tooltip parentTooltip {get; set;}
	private float maxHideTime = .25f;
	private float nextHideTime;
	protected string tooltipType = "Generic";

	public TooltipState currentState {get; set;}

	protected Vector2 targetSize;
	protected Vector2 targetPosition;
	protected Vector2 targetLocalPosition;
	protected Vector2 targetAnchoredPosition;

	private IEnumerator resizeCoroutine;

	public void LateUpdate()
	{
		
	}

	protected void SetAsChild(Tooltip tooltip)
	{
		if(tooltip == null) return;

		children.Add(tooltip);
		tooltip.parentTooltip = this;
	}

	protected void SetAsChild(Tooltip[] tooltips)
	{
		if(tooltips == null) return;
		
		foreach(Tooltip tooltip in tooltips)
		{
			SetAsChild(tooltip);
		}
	}

	private void TryHide()
	{

	}

	private void TryShow()
	{
		if(currentState == TooltipState.showing && ParentNullOrShowing())
		{
			ShowUI();
			foreach(Tooltip child in children)
			{
				child.TryShow();
			}
		}
	}

	private void TryResize()
	{
		// foreach(Tooltip child in children)
		// {
		// 	child.TryResize();
		// }

		if(currentState == TooltipState.resizing && !ChildrenResizing()) 
		{
			ResizeUI();
			StartShow();
			if(parentTooltip != null) parentTooltip.TryResize();
			else TryShow();
		}
	}

	private IEnumerator InvokeResize()
	{
		
		//if(parentTooltip != null) yield break;

		yield return new WaitForEndOfFrame();

		TryResize();
	}

	public void Hide()
	{
		HideUI();
	}	

	public virtual void Clear()
	{

	}

	protected void StartSet()
	{
		
	}

	protected void StartResize()
	{
		HideUI();
				
		currentState = TooltipState.resizing;

		if(resizeCoroutine != null) StopCoroutine(resizeCoroutine);
		resizeCoroutine = InvokeResize();
		StartCoroutine(resizeCoroutine);
	}

	protected void StartShow()
	{
		currentState = TooltipState.showing;
	}	

	protected void StartHide()
	{

	}

	protected virtual void HideUI()
	{
		ZeroSize();
		toggler.Hide();
		currentState = TooltipState.hiding;
	}

	protected virtual void ShowUI()
	{
		toggler.Show();
	}

	protected virtual void ResizeUI()
	{

	}

	private bool ChildrenResizing()
	{
		foreach(Tooltip child in children)
		{
			if(child.currentState == TooltipState.resizing) return true;
		}

		return false;
	}

	private bool ParentNullOrShowing()
	{
		if(parentTooltip == null || parentTooltip.currentState == TooltipState.showing) return true;
		return false;
	}

	public void ZeroSize()
	{
		(transform as RectTransform).sizeDelta = new Vector2(0, 0);
	}

	private void BreakResize()
	{
		
	}

	protected string GetStatePrintout()
	{
		RectTransform rect = transform as RectTransform;

		return "Visible: " + toggler.IsVisible() + " Position: " + rect.position.x + ", " + rect.position.y + " Size: " + rect.rect.width + ", " + rect.rect.height;  
	}

	public enum TooltipState{hiding, resizing, showing}
}
