﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FloatingSpriteManager : MonoBehaviour 
{
	public ObjectPool pool;
	public Transform startPosition;
	public Transform endPosition;
	public Queue<FloatingSprite> spriteQueue = new Queue<FloatingSprite>();
	public FloatReference launchTime;
	public FloatReference fadeTime;
	public FloatReference scatterXMin;
	public FloatReference scatterXMax;
	public FloatReference scatterYMin;
	public FloatReference scatterYMax;
	public FloatReference scatterTime;
	public FloatReference travelTime;
	public IntReference maxQueue;
	private float nextLaunchTime;
	public UIToggler parentToggler;
	public EaseReference scatterEase;
	public EaseReference travelEase;
	public EaseReference fadeEase;

	public void Start()
	{
		nextLaunchTime = Time.time;
	}

	public void Update()
	{
		if(Time.time > nextLaunchTime)
		{
			LaunchNext();
		}
	}

	public void Prepare(int count, Sprite sprite)
	{
		int clampedCount = Mathf.Clamp(count, 0, maxQueue);
		float batchLaunchTime = (clampedCount > 0) ? launchTime / clampedCount : 0;

		for(int i = 0; i < count; i++)
		{
			Prepare(sprite, startPosition, batchLaunchTime);
		}
	}

	private void Prepare(Sprite sprite, Transform startPoint, float launchTime)
	{
		FloatingSprite floatingSprite = pool.Get().GetComponent<FloatingSprite>();
		floatingSprite.SetSprite(sprite);
		floatingSprite.launchTime = launchTime;
		floatingSprite.image.color = Color.white;
		floatingSprite.transform.SetParent(startPoint, false);
		floatingSprite.transform.localPosition = Vector3.zero;
		spriteQueue.Enqueue(floatingSprite);
	}

	private void Launch(FloatingSprite floatingSprite, Transform endPoint)
	{
		nextLaunchTime = Time.time + floatingSprite.launchTime;
		Sequence newSequence = DOTween.Sequence();
		newSequence
		.Append(
			floatingSprite.transform.DOLocalMove(
				new Vector3
				(
					UnityEngine.Random.Range(scatterXMin, scatterXMax),
					UnityEngine.Random.Range(scatterYMin, scatterYMax),
					0
				),
				scatterTime)
			.SetEase(travelEase)
		)
		.Append(
			floatingSprite.transform.DOMove(endPoint.position, travelTime)
			.SetEase(travelEase)
		)
		.Append(
			floatingSprite.image.DOFade(0, fadeTime)
			.SetEase(fadeEase)
			.OnComplete(()=>ResetFloatingSprite(floatingSprite))
		);
		floatingSprite.gameObject.SetActive(true);

	}

	public void LaunchNext()
	{
		if(spriteQueue.Count <= 0) return;
		if(parentToggler.IsVisible()) Launch(spriteQueue.Dequeue(), endPosition);
		else spriteQueue.Dequeue();
	}

	private void ResetFloatingSprite(FloatingSprite floatingSprite)
	{
		floatingSprite.gameObject.SetActive(false);
		floatingSprite.transform.SetParent(pool.poolContainer);
		floatingSprite.transform.localPosition = Vector3.zero;
	}

}
