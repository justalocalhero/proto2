﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingEquipmentManager : MonoBehaviour 
{	
	public SpriteManager spriteManager;
	public Crafter crafter;
	public FloatingSpriteManager floatingSpriteManager;

	void Awake()
	{
		crafter.onCraft += Prepare;
	}

	public void Prepare(Equipment equipment)
	{
		if(equipment ==  null) return;

		Sprite sprite = spriteManager.GetSprite(equipment.GetSprite());

		if(sprite == null) return;

		floatingSpriteManager.Prepare(1, sprite);
	}
}
