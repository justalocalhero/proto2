﻿using UnityEngine;
using TMPro;

public class FloatingText : MonoBehaviour 
{
	public TextMeshProUGUI mesh;

	public void SetText(string text)
	{
		mesh.SetText(text);
	}	
}
