﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingResourceManager : MonoBehaviour 
{
	public Forge forge;
	public FloatingSpriteManager floatingSpriteManager;
	public ResourceNameMap resourceNameMap;

	// Use this for initialization
	void Awake () 
	{
		
		forge.onResourceAdded += Prepare;
	}	

	public Sprite GetSprite(Resource resource)
	{
		return resourceNameMap.GetSprite(resource.type);
	}

	public void Prepare(List<Resource> resources)
	{
		foreach(Resource resource in resources)
		{
			if(resource.value > 0) floatingSpriteManager.Prepare(resource.value, GetSprite(resource));
		}
	}
}
