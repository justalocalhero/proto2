﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingLootManager : MonoBehaviour 
{	
	public SpriteManager spriteManager;
	public ExpandedBattlegroundUI battleground;
	public FloatingSpriteManager floatingSpriteManager;

	void Awake()
	{
		battleground.onLoot += Prepare;
	}

	public void Prepare(Equipment equipment)
	{
		if(equipment ==  null) return;

		Sprite sprite = spriteManager.GetSprite(equipment.GetSprite());

		if(sprite == null) return;

		floatingSpriteManager.Prepare(1, sprite);
	}
}
