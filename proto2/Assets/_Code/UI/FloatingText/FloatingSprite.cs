﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingSprite : MonoBehaviour 
{

	public Image image;
	public float launchTime {get; set;}

	public void SetSprite(Sprite sprite)
	{
		image.sprite = sprite;
	}
}
