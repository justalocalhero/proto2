﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FloatingTextManager : MonoBehaviour 
{
	private ObjectPool pool;
	public Transform startPosition;
	public Transform endPosition;
	public Queue<FloatingText> textQueue = new Queue<FloatingText>();
	public FloatReference maxLaunchTimer;
	public FloatReference textFadeTime;
	public FloatReference textTravelTime;
	public IntReference maxQueue;
	private float launchTimer;
	private UIToggler parentToggler;
	public Ease travelEase;
	public Ease fadeEase;

	public void Update()
	{
		if(launchTimer > 0) launchTimer -= Time.deltaTime;
		else LaunchNext();
	}

	public void SetParentToggler(UIToggler parentToggler)
	{
		this.parentToggler = parentToggler;
	}

	public void SetFloatingTextPool(ObjectPool objectPool)
	{
		pool = objectPool;
	}

	public void Prepare(string text)
	{
		Prepare(text, startPosition);
	}

	private void Prepare(string text, Transform startPoint)
	{
		if(textQueue.Count >= maxQueue) return;
		FloatingText floatingText = pool.Get().GetComponent<FloatingText>();
		floatingText.SetText(text);
		floatingText.mesh.color = Color.white;
		floatingText.transform.SetParent(startPoint, false);
		floatingText.transform.localPosition = Vector3.zero;
		textQueue.Enqueue(floatingText);
	}

	private void Launch(FloatingText floatingText, Transform endPoint)
	{
		Sequence newSequence = DOTween.Sequence();
		newSequence
		.Append(
			floatingText.transform.DOMove(endPoint.position, textTravelTime)
			.SetEase(travelEase)
		)
		.Append(
			floatingText.mesh.DOFade(0, textFadeTime)
			.SetEase(fadeEase)
			.OnComplete(()=>ResetFloatingText(floatingText))
		);
		floatingText.gameObject.SetActive(true);

	}

	public void LaunchNext()
	{
		if(textQueue.Count <= 0) return;
		if(parentToggler.IsVisible()) Launch(textQueue.Dequeue(), endPosition);
		else textQueue.Dequeue();
		launchTimer = maxLaunchTimer;
	}

	private void ResetFloatingText(FloatingText floatingText)
	{
		floatingText.gameObject.SetActive(false);
		floatingText.transform.SetParent(pool.poolContainer);
		floatingText.transform.localPosition = Vector3.zero;
	}
}