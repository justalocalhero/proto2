﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIToggler : MonoBehaviour 
{
    public delegate void OnShow();
    public OnShow onShow;

    public delegate void OnHide();
    public OnHide onHide;

    private Canvas canvas;
    private GraphicRaycaster raycaster;
    private CanvasGroup canvasGroup;

    void Awake()
    {
        canvas = GetComponent<Canvas>();
        raycaster = GetComponent<GraphicRaycaster>();
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public void Toggle()
    {
        if(canvas != null)
        {
            if (IsVisible())
                Hide();
            else
                Show();
        }
    }

    public void Hide()
    {
        if(canvas != null && IsVisible())
        {
            canvas.enabled = false;
            SetRaycast(false);
            RaiseOnHide();
        }
    }

    public void Show()
    {
        if (canvas != null && !IsVisible())
        {
            canvas.enabled = true;
            SetRaycast(true);
            RaiseOnShow();
        }
    }

    public void Set(bool toShow)
    {
        if(toShow) Show();
        else Hide();
    }

    public bool IsVisible()
    {
        return (canvas == null) ? true : canvas.enabled;
    }

    public void RaiseOnShow()
    {
        if(onShow != null) onShow();
    }

    public void RaiseOnHide()
    {
        if(onHide != null) onHide();
    }

    public void SetRaycast(bool toCast)
    {
        if(raycaster != null) raycaster.enabled = toCast;
        if(canvasGroup != null) canvasGroup.blocksRaycasts = toCast;
    }
}
