﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HeaderSlot : MonoBehaviour 
{
	public TextMeshProUGUI label;
	public string text {get; set;}
	public ColorReference selectedColor, availableColor;

	public void SetText(string text)
	{
		this.text = text;
	}

	public void Selected()
	{
		label.SetText(Tooltips.ColoredText.GetText(text, selectedColor));
	}

	public void Available()
	{
		label.SetText(Tooltips.ColoredText.GetText(text, availableColor));
	}

	public void Hide()
	{
		label.SetText("");
	}
}
