﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LaunchButton : MonoBehaviour 
{
	public Button button;
	public TextMeshProUGUI labelMesh;
	public Battleground battleground;
	public Bullpen bullpen;
	public ColorReference enabledColor, disabledColor;
	public string labelString;

	void Awake()
	{
		button.onClick.AddListener(OnButtonPress);
	}

	void Start()
	{
		UpdateUI();
	}

	private void UpdateUI()
	{
		if(battleground == null) 
		{
			SetInteractable(false);
			return;
		}

		if(battleground.IsActive()) SetInteractable(false);
		else SetInteractable(true);
	}

	private void SetInteractable(bool isActive)
	{
		button.interactable = isActive;		
		labelMesh.SetText(Tooltips.ColoredText.GetText(
			labelString,
			(isActive) ? enabledColor : disabledColor
		));
	}

	public void OnButtonPress()
	{
		if(!battleground.IsActive()) 
		{
			battleground.NextPlayer(bullpen.GetNextPlayer());
			battleground.Reset();
		}	
	}

	public void SetBattleground(Battleground newBattleground)
	{
		if(newBattleground == null) return;
		if(battleground != null) battleground.onBattlegroundChanged -= UpdateUI;
		battleground = newBattleground;
		battleground.onBattlegroundChanged += UpdateUI;

		UpdateUI();
		
	}
}
