﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Slot : MonoBehaviour 
{
	private int index = -1;
	public Button button;
	public TextMeshProUGUI text;
	public UIToggler toggler;

	public delegate void OnClick(int index);
	public OnClick onClick;

	public void RaiseOnClick()
	{
		if(onClick != null) onClick(index);
	}

	public void Awake()
	{
		button.onClick.AddListener(HandleClick);
	}

	public void Clear()
	{
		SetText("");
		Hide();
	}

	public void HandleClick()
	{
		RaiseOnClick();
	}

	public void SetIndex(int newIndex)
	{
		index = newIndex;
	}

	public int GetIndex()
	{
		return index;
	}

	public void SetText(string textString)
	{
		text.SetText(textString);
	}

	public void SetColor(Color color)
	{
		if(color != button.image.color) button.image.color = color;
	}

	public void SetVisible(bool toShow)
	{
		if(toShow) Show();
		else Hide();
	}

	public void Show()
	{
		toggler.Show();
	}

	public void Hide()
	{
		toggler.Hide();
	}
}
