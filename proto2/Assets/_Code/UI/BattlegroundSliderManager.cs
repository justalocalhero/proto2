﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattlegroundSliderManager : MonoBehaviour 
{
	// public UIToggler enemyToggler;
	// public UIToggler playerToggler;
	// public UIToggler campingToggler;
	// public UIToggler exploringToggler;

	// public Slider playerOverBar;
	// public Slider playerUnderBar;
	// public Slider enemyOverBar;
	// public Slider enemyUnderBar;
	// public Slider campingSlider;
	// public Slider exploringSlider;
	// public Image exploringIcon;

	// private float playerOverVelocity;
	// private float playerUnderVelocity;
	// private float enemyOverVelocity;
	// private float enemyUnderVelocity;

	// public Battleground battleground;
	// public BattlegroundState currentState;
	// private BattlegroundState nextState;
	
	// private bool newFight = true;
	// private bool fightersAlive = true;

	// private float deltaTime;
	// private float lastPlayerHP;
	// private float maxOverTime = .25f;
	// private float maxUnderTime = .5f;
	// private float deathTimer = 0;

	// public void Start()
	// {
	// 	lastPlayerHP = 1;
	// 	playerOverBar.value = 1;
	// 	playerUnderBar.value = 1;
	// 	enemyOverBar.value = 1;
	// 	enemyUnderBar.value = 1;
	// 	ResetVelocities();
	// 	battleground.onBattlegroundChanged += SetIdle;
	// }

	// public void Update()
	// {
	// 	deltaTime = Time.deltaTime;
	// 	UpdateFightTimer();
	// 	bool deathTimerActive = deathTimer > 0;
	// 	bool isActive = battleground.IsActive();
	// 	bool showPlayer = isActive || deathTimerActive;
	// 	bool showFighting = (battleground.IsFighting() && isActive) || deathTimerActive;
	// 	bool showCamping = battleground.IsCamping() && isActive && !deathTimerActive;
	// 	bool showExploring = battleground.IsExploring() && isActive && !deathTimerActive;

	// 	UpdatePlayer(showPlayer);
	// 	UpdateFighting(showFighting);
	// 	UpdateCamping(showCamping);
	// 	UpdateExploring(showExploring);
		
	// }

	// public void UpdateFightTimer()
	// {
	// 	if(!fightersAlive)
	// 	{
	// 		if(deathTimer > 0)
	// 		{
	// 			deathTimer -= deltaTime;
	// 		}
	// 	}
	// }

	// public void UpdatePlayer(bool showPlayer)
	// {
	// 	float playerHP = battleground.GetPlayerHPScale();
	// 	if(playerHP <= 0) fightersAlive = false;

	// 	playerOverBar.value = Mathf.SmoothDamp(playerOverBar.value, playerHP, ref playerOverVelocity, maxOverTime);
	// 	playerUnderBar.value = Mathf.SmoothDamp(playerUnderBar.value, playerHP, ref playerUnderVelocity, maxUnderTime);

	// 	playerToggler.Set(showPlayer);

	// }

	// public void UpdateFighting(bool showFighting)
	// {
		
	// 	float enemyHP = battleground.GetEnemyHPScale();
	// 	if(enemyHP <= 0) fightersAlive = false;

	// 	enemyOverBar.value = Mathf.SmoothDamp(enemyOverBar.value, enemyHP, ref enemyOverVelocity, maxOverTime);
	// 	enemyUnderBar.value = Mathf.SmoothDamp(enemyUnderBar.value, enemyHP, ref enemyUnderVelocity, maxUnderTime);

	// 	if(showFighting) 
	// 	{
	// 		if(newFight)
	// 		{
	// 			newFight = false;
	// 			fightersAlive = true;
	// 			enemyOverBar.value = 1;
	// 			enemyUnderBar.value = 1;
	// 			deathTimer = maxUnderTime;			
	// 		}			

	// 		if(!showFighting && !newFight)
	// 		{
	// 			newFight = true;
	// 		}
	// 	}

	// 	if(showFighting && !IsFighting()) SetFighting();

	// }

	// public void UpdateCamping(bool showCamping)
	// {		
	// 	float campValue = battleground.GetCampingScale();
	// 	campingSlider.value = campValue;
	// 	if(showCamping && !IsCamping()) SetCamping();
	// }

	// public void UpdateExploring(bool showExploring)
	// {
	// 	float exploreValue = battleground.GetExploringScale();
	// 	exploringSlider.value = exploreValue;
	// 	if(showExploring && !IsExploring()) SetExploring();
	// }

	// public void UpdateExploringIcon()
	// {
	// 	TriggeredEvent currentEvent = battleground.GetTriggeredEvent();
	// 	Sprite newIcon = null;
	// 	if(currentEvent != null) newIcon = currentEvent.GetIcon();
	// 	if(newIcon != null) exploringIcon.sprite = newIcon;
	// }

	// public void ResetVelocities()
	// {
	// 	playerOverVelocity = 0;
	// 	playerUnderVelocity = 0;
	// 	enemyOverVelocity = 0;
	// 	enemyUnderVelocity = 0;
	// }

	// public void SetIdle()
	// {
	// 	HideCurrentState();
	// 	SetCurrentState(BattlegroundState.idle);

	// }

	// public void SetFighting()
	// {
	// 	HideCurrentState();
	// 	SetCurrentState(BattlegroundState.fighting);
	// 	ShowFighting();
	// }

	// public void ShowFighting()
	// {
	// 	enemyToggler.Set(true);
	// }

	// public void HideFighting()
	// {
	// 	enemyToggler.Set(false);
	// }

	// public bool IsFighting()
	// {
	// 	return currentState == BattlegroundState.fighting;
	// }

	// public void SetCamping()
	// {
	// 	HideCurrentState();
	// 	SetCurrentState(BattlegroundState.camping);
	// 	ShowCamping();
	// }

	// public void ShowCamping()
	// {
	// 	campingToggler.Set(true);
	// }

	// public void HideCamping()
	// {
	// 	campingToggler.Set(false);
	// }

	// public bool IsCamping()
	// {
	// 	return currentState == BattlegroundState.camping;
	// }

	// public void SetExploring()
	// {
	// 	HideCurrentState();
	// 	SetCurrentState(BattlegroundState.exploring);		
	// 	UpdateExploringIcon();		
	// 	ShowExploring();
	// }

	// public void ShowExploring()
	// {
	// 	exploringToggler.Set(true);
	// }

	// public void HideExploring()
	// {
	// 	exploringToggler.Set(false);
	// }

	// public bool IsExploring()
	// {
	// 	return currentState == BattlegroundState.exploring;
	// }

	// public void HideCurrentState()
	// {
	// 	switch(currentState)
	// 	{
	// 		case BattlegroundState.fighting:
	// 			HideFighting();
	// 			break;
	// 		case BattlegroundState.camping:
	// 			HideCamping();
	// 			break;
	// 		case BattlegroundState.exploring:
	// 			HideExploring();
	// 			break;
	// 		default:
	// 			break;
	// 	}
	// }

	// public void SetCurrentState(BattlegroundState newState)
	// {
	// 	currentState = newState;
	// }
}
