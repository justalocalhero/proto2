﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class PlayerStatWindow : MonoBehaviour 
{

	public Player player;
	public StatNameDictionary statNames;
	public ExpandedTooltip expandedTooltip;
	public UIStatBox[] statBoxes = new UIStatBox[Enum.GetNames(typeof(StatType)).Length];

	public void Start()
	{
		PassReferences();
		UpdateText();
	}

	public void PassReferences()
	{
		UIStatBox box;
		PlayerStatBox playerBox;
		for(int i = 0; i < statBoxes.Length; i++)
		{
			box = statBoxes[i];
			playerBox = box.playerBox;
			playerBox.SetStatType(box.statType);
			playerBox.SetStatNames(statNames);
			playerBox.SetTooltip(expandedTooltip);
		}

	}

	public void PassPlayerReference()
	{
		for(int i = 0; i < statBoxes.Length; i++)
		{
			statBoxes[i].playerBox.SetPlayer(player);
		}
	}

	public void SetPlayer(Player newPlayer)
	{
		player = newPlayer;
		PassPlayerReference();
		UpdateText();
	}

	public void UpdateText()
	{
		
		Clear();
		if(player == null) return;
		for(int i = 0; i < statBoxes.Length; i++)
		{
			statBoxes[i].playerBox.UpdateText();
		}
	}

	public void Clear()
	{
		for(int i = 0; i < statBoxes.Length; i++)
		{
			statBoxes[i].playerBox.Clear();
		}
	}


}

[Serializable]
public struct UIStatBox {public StatType statType; public PlayerStatBox playerBox;};