﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class Link : MonoBehaviour 
{
	[DllImport("__Internal")]
    private static extern void OpenWindow(string url);


	public Button button;
	public StringReference url;

	public void Awake()
	{
		button.onClick.AddListener(HandleClick);
	}

	public void HandleClick()
	{
		#if !UNITY_EDITOR
		OpenWindow(url);
		#endif
	}
}
