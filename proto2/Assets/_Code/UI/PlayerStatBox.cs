﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerStatBox : MonoBehaviour 
{
	private Player player;
	private StatType statType;
	private ExpandedTooltip tooltip;
	private StatNameDictionary statNames;
	public TextMeshProUGUI labelText;
	public TextMeshProUGUI statText;

	public void SetStatType(StatType newType)
	{
		statType = newType;
	}

	public void SetStatNames(StatNameDictionary newNames)
	{
		statNames = newNames;
	}

	public void SetTooltip(ExpandedTooltip newTooltip)
	{
		tooltip = newTooltip;
	}

	public void SetPlayer(Player newPlayer)
	{
		player = newPlayer;
	}

	public void UpdateText()
	{
		if(player == null) return;
		int value = (int)Mathf.Round(player.GetStat(statType));
		int compositeValue = (int)Mathf.Round(player.GetStat(statType, ModifierType.Flat)) 
		+ (int)Mathf.Round(player.GetStat(statType, ModifierType.Multiplier)) 
		+ (int)Mathf.Round(player.GetStat(statType, ModifierType.GlobalMultiplier));
		if(compositeValue == 0) return;
		statText.text = "" + value;
		labelText.text = statNames.GetTypeName(statType);
		Show();

	}

	public void Clear()
	{
		labelText.enabled = false;
		statText.enabled = false;
	}

	public void Show()
	{
		labelText.enabled = true;
		statText.enabled = true;
	}

	public void OnPointerEnter()
	{
		if(player == null)
		{
			return;
		}
		if(statNames == null)
		{
			return;
		}
		if(tooltip == null)
		{
			return;
		}

		tooltip.StatEntered(statType, player, statNames);

	}

	public void OnPointerExit()
	{
		tooltip.Hide();
	}
}
