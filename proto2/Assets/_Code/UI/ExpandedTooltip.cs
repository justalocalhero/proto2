﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ExpandedTooltip : MonoBehaviour 
{
    private TextMeshProUGUI mesh;

    public void Start()
    {
        mesh = GetComponent<TextMeshProUGUI>();
    }

    public void StatEntered(StatType type, Player player, StatNameDictionary names)
    {
        Hide();
        if(player == null) return;
        SetMeshText(type, player, names);
        Show();
    }

    public void SetMeshText(StatType type, Player player, StatNameDictionary names)
    {
        string toSet = names.GetTypeName(type) + ":";
        int flatValue = (int)Mathf.Round(player.GetStat(type, ModifierType.Flat));
        int multValue = (int)Mathf.Round(100 * player.GetStat(type, ModifierType.Multiplier));
        int globalValue = (int)Mathf.Round(100 * player.GetStat(type, ModifierType.GlobalMultiplier));
        if(flatValue != 0) toSet += ("\n" + names.GetModName(ModifierType.Flat) + flatValue);
        if(multValue != 0) toSet += ("\n" + names.GetModName(ModifierType.Multiplier) + multValue);
        if(globalValue != 0) toSet += ("\n" + names.GetModName(ModifierType.GlobalMultiplier) + globalValue);
        mesh.text = toSet;
    }

    public void Hide()
    {
        mesh.enabled = false;
    }

    public void Show()
    {
        mesh.enabled = true;
    }
}
