﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Tooltips;

namespace Battling
{
	public class PotionSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
	{
		public SpriteManager spriteManager;
		private PotionTooltip potionTooltip;
		public RectTransform rect {get; private set;}
		public UIToggler toggler;
		public Transform iconContainer;
		public PotionIcon iconPrototype;
		public Image highlight;
		public Image background;
		private List<PotionIcon> icons = new List<PotionIcon>();
		private int count = 0;
		private int toSetValue = 0;
		public Potion potion {get; private set;}
		private bool frozen = false;
		public bool toClear = false;
		private bool toSet = false;
		
		public delegate void OnClear();
		public OnClear onClear;

		public void RaiseOnClear()
		{
			if(onClear != null) onClear();
		}

		public void Awake()
		{			
			rect = transform as RectTransform;
			BuildIcons();
		}

		public void BuildIcons()
		{			
			Transform[] tempRects = iconContainer.GetComponentsInChildren<Transform>();

			foreach(Transform rect in tempRects)
			{
				if(rect != iconContainer)
				{
					PotionIcon tempIcon = Object.Instantiate(iconPrototype, rect, false);
					icons.Add(tempIcon);
				}
			}

			if(icons.Count > 0) icons[0].toggler.onHide += Clear;
		}

		public void SetSprite(Sprite sprite)
		{
			foreach(PotionIcon icon in icons)
			{
				icon.SetSprite(sprite);
			}
		}

		public void SetParentToggler(UIToggler parentToggler)
		{
			foreach(PotionIcon icon in icons)
			{
				icon.SetParentToggler(parentToggler);
			}
		}

		public void SetPotionTooltip(PotionTooltip potionTooltip)
		{
			this.potionTooltip = potionTooltip;
		}


		public void Set(Potion potion)
		{
			if(potion == null) return;
			if(frozen) return;

			ClearPotion();
			this.potion = potion;
			
			SetSprite(spriteManager.GetSprite(potion.GetSprite()));
			SetCount(potion.GetCount());
		}

		public void Clear()
		{
			if(frozen)
			{
				toClear = true;
				toSet = false;
				return;
			}
			
			foreach(PotionIcon icon in icons)
			{
				icon.Out();
			}

			ClearPotion();
		}

		private void ClearPotion()
		{
			if(potion != null)
			{
				potion = null;

				RaiseOnClear();
			}
			Hide();
		}

		private void CheckToClear()
		{
			if(!toClear) return;

			Clear();

			toClear = false;
		}

		private void CheckToSet()
		{
			if(!toSet) return;

			SetCount(toSetValue);

			toSet = false;
		}

		private void OnUse(Potion potion)
		{
			SetCount(potion.GetCount());
		}

		private void SetCount(int newCount)
		{
			if(frozen)
			{
				toSetValue = newCount;
				toSet = true;
				toClear = false;
				return;
			}

			for(int i = 0; i < icons.Count; i++)
			{
				if(i >= count && i < newCount) icons[i].In();
				if(i < count && i >= newCount) icons[i].Out();
			}

			count = newCount;
			if(count > 0) Show();
		}

		public bool HasPotion()
		{
			return potion != null;
		}

		public void Freeze()
		{
			frozen = true;
			highlight.enabled = true;
		}

		public void UnFreeze()
		{
			frozen = false;
			highlight.enabled = false;
			CheckToClear();
			CheckToSet();

		}

		public void Show()
		{
			toggler.Show();
		}

		public void Hide()
		{
			toggler.Hide();
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			if(potion == null) return;

			Freeze();
			potionTooltip.Set(potion, rect);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			UnFreeze();

			potionTooltip.Hide();
		}
	}
}
