﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tooltips;

namespace Battling
{
	public class PotionIcon : MonoBehaviour
	{
		private UIToggler parentToggler;
		public UIToggler toggler;
		public Image icon;
		public Flash inFlash;
		public Puff outPuff;
		public Flash outFlash;
		
		private void Awake()
		{
			outFlash.onComplete += toggler.Hide;
			outFlash.onComplete += outFlash.Reset;
			outPuff.target = icon.transform as RectTransform;
		}

		public void SetParentToggler(UIToggler parentToggler)
		{
			this.parentToggler = parentToggler;
		}
		
		public void SetSprite(Sprite sprite)
		{
			icon.sprite = sprite;
		}

		public void In()
		{
			if(ParentVisible())
			{
				outFlash.Reset();
				outPuff.Reset();
				inFlash.Play();
			}
			Show();
		}

		public void Out()
		{
			if(ParentVisible())
			{
				inFlash.Reset();
				outFlash.Play();
				outPuff.Play();
			}
			else Hide();
		}

		private void Show()
		{
			toggler.Show();
		}

		private void Hide()
		{
			toggler.Hide();
		}

		public bool ParentVisible()
		{
			return (parentToggler == null || parentToggler.IsVisible());
		}
	}
}