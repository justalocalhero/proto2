﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Tooltips;

public class ActionIcon : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public SpriteManager spriteManager;
	private RectTransform rect;
	private UIToggler parentToggler;
	private ActionTooltip actionTooltip;
	public UIToggler toggler;
	public Action action;
	public Image icon;
	public Image highlight;
	public Flash inEffect;
	public Flash outEffect;
	public Puff puffEffect;
	private bool frozen = false;
	private bool toClear = false;

	public void Awake()
	{
		rect = transform as RectTransform;
		puffEffect.target = transform as RectTransform;

		inEffect.onComplete += Clear;
		outEffect.onComplete += outEffect.Reset;
		outEffect.onComplete += Hide;
		outEffect.onComplete += ClearAction;
	}

	public void SetParentToggler(UIToggler parentToggler)
	{
		this.parentToggler = parentToggler;
	}

	public void SetActionTooltip(ActionTooltip actionTooltip)
	{
		this.actionTooltip = actionTooltip;
	}

	public void Set(Action action)
	{
		if(action == null) return;
		if(frozen) return;

		ClearAction();
		this.action = action;
		icon.sprite = spriteManager.GetSprite(action.GetSprite());

		In();
	}

	public void Clear()
	{
		if(frozen)
		{
			toClear = true;
			return;
		}

		Out();
	}

	private void ClearAction()
	{
		if(action == null) return;

		action = null;
	}

	private void CheckToClear()
	{
		if(!toClear) return;

		Out();

		toClear = false;
	}

	private void In()
	{
		
		if(ParentVisible()) 
		{
			PlayIn();		
			Show();
		}
	}

	private void Out()
	{
		if(ParentVisible()) PlayOut();
		else 
		{
			ClearAction();
			Hide();
		}
	}

	private void PlayIn()
	{
		outEffect.Reset();
		inEffect.Play();
		puffEffect.Play();
	}

	private void PlayOut()
	{
		inEffect.Reset();
		puffEffect.Reset();
		outEffect.Play();
	}

	private void Show()
	{
		toggler.Show();
	}

	private void Hide()
	{
		toggler.Hide();
	}

	public bool HasAction()
	{
		return action != null;
	}

	private bool ParentVisible()
	{
		return (parentToggler == null || parentToggler.IsVisible());
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
		PlayIn();
		frozen = true;
		highlight.enabled = true;
		actionTooltip.Set(action, action.name, rect);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
		frozen = false;
		highlight.enabled = false;
		actionTooltip.Hide();
		CheckToClear();
    }
}
