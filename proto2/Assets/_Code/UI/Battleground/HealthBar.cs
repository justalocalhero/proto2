﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Juice;

public class HealthBar : MonoBehaviour 
{
	private UIToggler parentToggler;
	public UIToggler toggler;
	public AsymptoticSlider sliderPrototype;
	public Puff puffPrototype;
	public Flash flashPrototype;

	public Transform container;
	public ColorReference overColor, underColor;
	public FloatReference overTimeScale, underTimeScale, underDelay, underDelayReduction;
	private float startValue = 1;
	private AsymptoticSlider overBar, underBar;
	private Puff overPuff, underPuff;
	private Flash overFlash, underFlash;
	private bool hideOnComplete = false;

	public void Start()
	{
		BuildSliders();
		BuildJuice();
	}

	public void Update()
	{
		if(hideOnComplete && overBar.IsZero() && underBar.IsZero())
			Hide();
		
	}

	public void SetParentToggler(UIToggler parentToggler)
	{
		this.parentToggler = parentToggler;
	}

	public void SetToHide(bool toSet)
	{
		hideOnComplete = toSet;
	}

	public void Clear()
	{
		overBar.Reset();
		underBar.Reset();
		Hide();
	}

	public void Show()
	{
		toggler.Show();
	}

	public void Hide()
	{
		toggler.Hide();
	}

	public void Play(float value)
	{
		SetToHide(false);
		Show();

		bool parentVisible = (parentToggler == null || parentToggler.IsVisible());

		if(overBar != null)	
		{
			if(parentVisible)
			{
				overBar.Play(value);
				overPuff.Play();
				if(value < overBar.Value()) overFlash.Play();
			}
			else
			{
				overBar.SetTarget(value);
				overBar.SetValue(value);

			}
			
		}
		if(underBar != null) 
		{
			if(parentVisible)
			{
				underBar.Play(value);
				//underPuff.Play();
				//if(value < underBar.Value()) underFlash.Play();
			}
			else
			{
				underBar.SetTarget(value);
				underBar.SetValue(value);
			}
			
		}
	}

	private void BuildSliders()
	{
		underBar = BuildSlider(underColor, underTimeScale, underDelay, underDelayReduction);
		overBar = BuildSlider(overColor, overTimeScale, 0, 0);
	}

	private void BuildJuice()
	{
		overPuff = BuildPuff(overBar.slider.fillRect);
		overFlash = BuildFlash(overBar.slider.fillRect);
		underPuff = BuildPuff(underBar.slider.fillRect);
		underFlash = BuildFlash(underBar.slider.fillRect);
	}

	public Flash BuildFlash(Transform container)
	{
		Flash toReturn = Object.Instantiate(flashPrototype, container, false);

		return toReturn;
	}

	public Puff BuildPuff(RectTransform container)
	{
		Puff toReturn = Object.Instantiate(puffPrototype, container, false);
		toReturn.target = container;

		return toReturn;
	}

	public AsymptoticSlider BuildSlider(ColorReference color, float timeScale, float delay, float reduction)
	{
		AsymptoticSlider toReturn = Object.Instantiate(sliderPrototype, container, false);
		
		return toReturn
			.WithColor(color)
			.WithTimeScale(timeScale)
			.WithStartValue(startValue)
			.WithDelay(delay, reduction);
	}	
}
