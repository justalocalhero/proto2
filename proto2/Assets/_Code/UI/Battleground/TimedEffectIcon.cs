﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Tooltips;

public class TimedEffectIcon : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public SpriteManager spriteManager;
	private TimedEffectTooltip timedEffectTooltip;
	private EffectType effectType;
	public RectTransform rect {get; private set;}
	private UIToggler parentToggler;
	public UIToggler toggler;
	public TimedEffect effect;
	public Image icon;
	public Image highlight;
	public Flash inEffect;
	public Flash outEffect;
	private bool frozen, toClear;

	public delegate void OnClear();
	public OnClear onClear;

	public void RaiseOnClear()
	{
		if(onClear != null) onClear();
	}

	public void Awake()
	{
		rect = transform as RectTransform;
		outEffect.onComplete += outEffect.Reset;
		outEffect.onComplete += Hide;
		outEffect.onComplete += ClearEffect;
	}

	public void SetParentToggler(UIToggler parentToggler)
	{
		this.parentToggler = parentToggler;
	}

	public void SetEffectType(EffectType effectType)
	{
		this.effectType = effectType;
	}

	public void SetTimedEffectTooltip(TimedEffectTooltip timedEffectTooltip)
	{
		this.timedEffectTooltip = timedEffectTooltip;
	}

	public void Set(TimedEffect newEffect)
	{
		if(!ValidEffect(newEffect)) return;
		if(frozen) return;
		
		if(effect != null) 
		{
			effect.onExpire -= HandleExpire;
		}

		effect = newEffect;
		icon.sprite = spriteManager.GetSprite(effect.GetSprite());

		effect.onExpire += HandleExpire;

		if(ParentVisible()) PlayIn();
		
		Show();
	}

	public bool ValidEffect(TimedEffect toCheck)
	{
		if(toCheck == null) return false;
		if(toCheck.spriteIndex <= 0) return false;

		return true;

	}

	public void Clear()
	{
		if(frozen)
		{
			toClear = true;
			return;
		}
		else 
		{
			ClearEffect();
		}
	}

	private void ClearEffect()
	{

		Hide();

		if(effect == null) return;

		effect.onExpire -= HandleExpire;

		effect = null;

		RaiseOnClear();
		
	}

	public void HandleExpire(TimedEffect effect)
	{
		if(frozen) toClear = true;
		else Out();
	}

	private void CheckToClear()
	{
		if(!toClear) return;

		ClearEffect();

		toClear = false;
	}

	private void Out()
	{
		if(ParentVisible()) PlayOut();
		else 
		{
			ClearEffect();
		}
	}

	private void PlayIn()
	{
		outEffect.Reset();
		inEffect.Play();
	}

	private void PlayOut()
	{
		inEffect.Reset();
		outEffect.Play();
	}

	private void Show()
	{
		toggler.Show();
	}

	private void Hide()
	{
		toggler.Hide();
	}

	public bool HasEffect()
	{
		return effect != null;
	}

	private bool ParentVisible()
	{
		return (parentToggler == null || parentToggler.IsVisible());
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
		frozen = true;
		highlight.enabled = true;
		timedEffectTooltip.Set(effect, effectType, rect);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
		frozen = false;
		highlight.enabled = false;
		timedEffectTooltip.Hide();
		CheckToClear();
    }
}
