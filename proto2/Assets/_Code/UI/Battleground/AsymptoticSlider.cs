﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AsymptoticSlider : MonoBehaviour 
{
	public UIToggler toggler;
	public Slider slider;
	private Image fillImage;
	private float target;
	private float velocity;
	private bool active;
	public float timeScale;
	public float startValue;
	public float tolerance;
	public float maxDelay;
	public float delayReduction;
	private float nextDelay;
	private float nextFireTime = 0.0f;
	private int delaysSinceFire = 0;
	
	public void Awake()
	{
		fillImage = slider.fillRect.GetComponent<Image>();
		Reset();
	}

	public void Reset()
	{
		velocity = 0;
		slider.value = startValue;
	}

	public void Update()
	{
		if(!active) return;
		if(Time.time < nextFireTime) return;

		slider.value = Mathf.SmoothDamp(slider.value, target, ref velocity, timeScale);
		ResetDelay();
		Clamp();
	}

	public void Play(float newTarget)
	{
		SetDelay();
		SetTarget(newTarget);
		Activate();
		Show();
	}

	public void SetDelay()
	{
		float time = Time.time;
		float target = Utility.Greater(time, nextFireTime);
		float max = Time.time + maxDelay;

		nextFireTime = Mathf.Clamp(target + nextDelay, time, max);
		nextDelay = Mathf.Clamp(nextDelay - delayReduction, 0, maxDelay);
	}

	public void ResetDelay()
	{
		nextDelay = maxDelay;
	}

	public void End()
	{
		active = false;
		Hide();
	}

	public AsymptoticSlider WithTolerance(float value)
	{
		tolerance = value;

		return this;

	}

	public AsymptoticSlider WithStartValue(float value)
	{
		startValue = Mathf.Clamp(value, 0, 1);

		return this;

	}

	public AsymptoticSlider WithTimeScale(float value)
	{
		timeScale = value;

		return this;
	}

	public AsymptoticSlider WithColor(Color color)
	{
		fillImage.color = color;
		
		return this;
	}

	public AsymptoticSlider WithDelay(float delay, float reduction)
	{
		maxDelay = Mathf.Clamp(delay, 0, delay);
		delayReduction = Mathf.Clamp(reduction, 0, delay);

		return this;
	}

	public void SetValue(float newValue)
	{
		slider.value = newValue;
	}

	public void SetTarget(float newTarget)
	{
		target = newTarget;
	}

	public void Show()
	{
		toggler.Show();
	}

	public void Hide()
	{
		toggler.Hide();
	}

	private void Activate()
	{
		active = true;
	}

	private void Deactivate()
	{
		active = false;
	}

	private void Clamp()
	{
		if(Mathf.Abs(velocity) < tolerance) 
		{
			velocity = 0;
			slider.value = target;
			active = false;
		}
	}

	public float Value()
	{
		return slider.value;
	}

	public bool IsZero()
	{
		return slider.value <= 0;
	}
}
