﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Battling;
using Tooltips;

public class UnitHUD : MonoBehaviour 
{
	private UIToggler parentToggler;

	public Transform healthBarContainer, floatingTextContainer, buffContainer,
		debuffContainer, unitResourceContainer, actionContainer, potionContainer;
	
	public ActionFloatingText floatingTextPrototype;
	private ActionFloatingText floatingText;

	public HealthBar healthBarPrototype;
	private HealthBar healthBar;
	private float floatingTextSide;

	public TimedEffectIconManager timedEffectPrototype;
	private TimedEffectIconManager buffs;
	private TimedEffectIconManager debuffs;

	public UnitResourceSlotManager unitResourcePrototype;
	private UnitResourceSlotManager unitResources;

	public PotionSlot potionPrototype;
	private PotionSlot potionIcon;

	public ActionIcon actionPrototype;
	private ActionIcon actionIcon;

	private Unit unit;

	public void Awake()
	{
		BuildFloatingText();
		BuildHealthBar();
		BuildTimedEffects();
		BuidlUnitResources();
		BuildActionIcon();
		BuildPotion();
	}

	public void BuildFloatingText()
	{
		floatingText = Object.Instantiate(floatingTextPrototype, floatingTextContainer, false);
	}

	public void BuildHealthBar()
	{
		healthBar = Object.Instantiate(healthBarPrototype, healthBarContainer, false);
	}

	public void BuildTimedEffects()
	{
		buffs = BuildTimedEffect(buffContainer);
		debuffs = BuildTimedEffect(debuffContainer);

		buffs.SetEffectType(EffectType.buff);
		debuffs.SetEffectType(EffectType.debuff);

		buffs.SetParent(this);
		debuffs.SetParent(this);
	}
	
	public void BuidlUnitResources()
	{		
		unitResources = Object.Instantiate(unitResourcePrototype, unitResourceContainer, false);
	}

	public void BuildActionIcon()
	{
		actionIcon = Object.Instantiate(actionPrototype, actionContainer, false);
	}

	public void BuildPotion()
	{
		potionIcon = Object.Instantiate(potionPrototype, potionContainer, false);
	}

	public TimedEffectIconManager BuildTimedEffect(Transform container)
	{
		TimedEffectIconManager toReturn = Object.Instantiate(timedEffectPrototype, container, false);

		return toReturn;
	}

	public void SetUnit(Unit newUnit)
	{
		if(newUnit == null) return;

		ClearUnit();

		unit = newUnit;

		unit.onDamage += HandleDamage;
		unit.onActionTaken += HandleActionTaken;
		unit.onActionUsed += HandleActionUsed;
		unit.onDeath += HandleDeath;
		unit.onBuffAdded += HandleBuff;
		unit.onDebuffAdded += HandleDebuff;
		unit.onResourceAdded += HandleResource;
		unit.onPotionUsed += HandlePotion;
		unit.onStealth += HandleStealth;
		unit.onPurge += HandlePurge;
		unit.onDispel += HandleDispel;

		HandleHealthChange();
		HandlePotion(unit.potion);
		HandleResource(newUnit.GetResourceList());
	}

	public void Clear()
	{
		ClearUnit();
		healthBar.Clear();
		buffs.Clear();
		debuffs.Clear();
		unitResources.Clear();
		actionIcon.Clear();
	}

	public void SetParentToggler(UIToggler parentToggler)
	{
		floatingText.SetParentToggler(parentToggler);
		healthBar.SetParentToggler(parentToggler);
		buffs.SetParentToggler(parentToggler);
		debuffs.SetParentToggler(parentToggler);
		actionIcon.SetParentToggler(parentToggler);
		potionIcon.SetParentToggler(parentToggler);
	}

	public void SetActionTooltip(ActionTooltip actionTooltip)
	{
		actionIcon.SetActionTooltip(actionTooltip);
	}

	public void SetTimedEffectTooltip(TimedEffectTooltip timedEffectTooltip)
	{
		buffs.SetTimedEffectTooltip(timedEffectTooltip);
		debuffs.SetTimedEffectTooltip(timedEffectTooltip);
	}

	public void SetPotionTooltip(PotionTooltip potionTooltip)
	{
		potionIcon.SetPotionTooltip(potionTooltip);
	}

	public void SetUnitResourceTooltip(UnitResourceTooltip unitResourceTooltip)
	{
		unitResources.SetUnitResourceTooltip(unitResourceTooltip);
	}

	public void SetFloatingTextPool(ObjectPool objectPool)
	{
		floatingText.SetFloatingTextPool(objectPool);
	}

	public void HandleDeath(Damage killingBlow)
	{
		ClearUnit();
		healthBar.SetToHide(true);
		buffs.Clear();
		debuffs.Clear();
		unitResources.Clear();		
		actionIcon.Clear();
	}
	

	public void ClearUnit()
	{		
		if(unit == null) return;

		unit.onDamage -= HandleDamage;
		unit.onActionTaken -= HandleActionTaken;
		unit.onActionUsed -= HandleActionUsed;
		unit.onDeath -= HandleDeath;
		unit.onBuffAdded -= HandleBuff;
		unit.onDebuffAdded -= HandleDebuff;
		unit.onResourceAdded -= HandleResource;
		unit.onPotionUsed -= HandlePotion;
		unit.onStealth -= HandleStealth;
		unit.onPurge -= HandlePurge;
		unit.onDispel -= HandleDispel;

		unit = null;
		
	}

	public void HandleDamage(Damage damage)
	{
		if(damage.value == 0) return;
		floatingText.HandleDamage(damage);
		HandleHealthChange();
	}

	public void HandleHealthChange()
	{
		if(unit != null) healthBar.Play(unit.GetHealthPercentage());
	}

	public void HandleActionTaken(Action action)
	{
		if(floatingText != null) floatingText.HandleAction(action);
	}

	public void HandleActionUsed(Action action)
	{		
		if(action != null) actionIcon.Set(action);
	}

	public void HandleBuff(TimedEffect buff)
	{
		if(buffs == null) return;

		buffs.HandleEffectAdded(buff);
	}

	public void HandleDebuff(TimedEffect debuff)
	{
		if(debuffs == null) return;

		debuffs.HandleEffectAdded(debuff);
	}

	public void HandleResource(List<UnitResource> resources)
	{
		if(resources == null) return;
		
		foreach(UnitResource resource in resources)
		{
			HandleResource(resource);
		}
	}

	public void HandleResource(UnitResource resource)
	{
		if(unitResources == null) return;

		unitResources.HandleResourceAdded(resource);
	}

	public void HandlePotion(Potion potion)
	{
		if(potion == null) return;

		potionIcon.Set(potion);
	}

	public void HandleStealth()
	{
		floatingText.Push("Stealth");
	}

	public void HandleDispel()
	{
		floatingText.Push("Dispel");
	}

	public void HandlePurge()
	{
		floatingText.Push("Purge");
	}

	public void PushFloatingText(string text)
	{
		floatingText.Push(text);
	}

	public bool HasUnit()
	{
		return unit != null;
	}
}
