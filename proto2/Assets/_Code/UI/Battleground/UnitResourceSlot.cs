﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Tooltips;

namespace Battling
{
	public class UnitResourceSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
	{
		public SpriteManager spriteManager;
		private UnitResourceTooltip unitResourceTooltip;
		public RectTransform rect {get; private set;}
		public UIToggler toggler;
		public Transform iconContainer;
		public UnitResourceIcon iconPrototype;
		public Image highlight;
		private List<UnitResourceIcon> icons = new List<UnitResourceIcon>();
		private int count = 0;
		private int toSetValue = 0;
		public UnitResource resource {get; private set;}
		private bool frozen = false;
		public bool toClear = false;
		private bool toSet = false;
		
		public delegate void OnClear();
		public OnClear onClear;

		public void RaiseOnClear()
		{
			if(onClear != null) onClear();
		}

		public void Awake()
		{			
			rect = transform as RectTransform;
			BuildIcons();
		}

		public void BuildIcons()
		{			
			Transform[] tempRects = iconContainer.GetComponentsInChildren<Transform>();

			foreach(Transform rect in tempRects)
			{
				if(rect != iconContainer)
				{
					UnitResourceIcon tempIcon = Object.Instantiate(iconPrototype, rect, false);
					icons.Add(tempIcon);
				}
			}

			if(icons.Count > 0) icons[0].toggler.onHide += Clear;
		}

		public void SetSprite(Sprite sprite)
		{
			foreach(UnitResourceIcon icon in icons)
			{
				icon.SetSprite(sprite);
			}
		}

		public void SetParentToggler(UIToggler toggler)
		{
			foreach(UnitResourceIcon icon in icons)
			{
				icon.SetParentToggler(toggler);
			}
		}

		public void SetUnitResourceTooltip(UnitResourceTooltip unitResourceTooltip)
		{
			this.unitResourceTooltip = unitResourceTooltip;
		}

		public void Set(UnitResource resource)
		{
			if(resource == null) return;
			if(frozen) return;

			ClearResource();
			this.resource = resource;
			
			resource.onResourceChanged += SetCount;

			SetSprite(spriteManager.GetSprite(resource.GetSprite()));
			SetCount(resource.GetValue());
		}

		public void Clear()
		{
			if(frozen)
			{
				toClear = true;
				toSet = false;
				return;
			}
			
			foreach(UnitResourceIcon icon in icons)
			{
				icon.Out();
			}

			ClearResource();
		}

		private void ClearResource()
		{
			if(resource != null)
			{
				resource.onResourceChanged -= SetCount;

				resource = null;

				RaiseOnClear();
			}
			
			Hide();
		}

		private void CheckToClear()
		{
			if(!toClear) return;

			Clear();

			toClear = false;
		}

		private void CheckToSet()
		{
			if(!toSet) return;

			SetCount(toSetValue);

			toSet = false;
		}

		private void SetCount(int newCount)
		{
			if(frozen)
			{
				toSetValue = newCount;
				toSet = true;
				toClear = false;
				return;
			}

			for(int i = 0; i < icons.Count; i++)
			{
				if(i >= count && i < newCount) icons[i].In();
				if(i < count && i >= newCount) icons[i].Out();
			}

			count = newCount;
			if(count > 0) Show();
		}

		public bool HasResource()
		{
			return resource != null;
		}

		public void Freeze()
		{
			frozen = true;
			highlight.enabled = true;
		}

		public void UnFreeze()
		{
			frozen = false;
			highlight.enabled = false;
			CheckToClear();
			CheckToSet();

		}

		public void Show()
		{
			toggler.Show();
		}

		public void Hide()
		{
			toggler.Hide();
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			if(resource == null) return;

			Freeze();

			unitResourceTooltip.Set(resource, rect);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			UnFreeze();

			unitResourceTooltip.Hide();

		}
	}
}
