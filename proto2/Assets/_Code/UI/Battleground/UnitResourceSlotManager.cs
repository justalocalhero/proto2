﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tooltips;

namespace Battling
{
	public class UnitResourceSlotManager : MonoBehaviour
	{
		public Transform slotContainer, backgroundContainer;
		public FloatReference spacing, backgroundOffset;
		public UnitResourceSlot slotPrototype;
		private List<UnitResourceSlot> slots;
		public TransformFitter backgroundPrototype;
		private TransformFitter background;

		public void Awake()
		{
			BuildSlots();
			if(slots.Count > 0) BuildBackground();
		}

		public void SetParentToggler(UIToggler parentToggler)
		{
			foreach(UnitResourceSlot slot in slots)
			{
				slot.SetParentToggler(parentToggler);
			}
		}
		
		public void SetUnitResourceTooltip(UnitResourceTooltip unitResourceTooltip)
		{
			foreach(UnitResourceSlot slot in slots)
			{
				slot.SetUnitResourceTooltip(unitResourceTooltip);
			}
		}

		public void BuildSlots()
		{
			GridBuilder<UnitResourceSlot> gridBuilder = new GridBuilder<UnitResourceSlot>();

			slots = gridBuilder.Begin()
				.WithPrototype(slotPrototype)
				.WithContainer(slotContainer)
				.WithSpacing(backgroundOffset, backgroundOffset)
				.WithRows(1)
				.Build();

			RegisterDelegates();
		}

		public void RegisterDelegates()
		{			
			foreach(UnitResourceSlot slot in slots)
			{
				slot.onClear += UpdateBackground;
			}
		}

		public void BuildBackground()
		{
			background = Object.Instantiate(backgroundPrototype, backgroundContainer, false);
			background.SetOffset(spacing, spacing);
		}	

		public void HandleResourceAdded(UnitResource resource)
		{
			foreach(UnitResourceSlot slot in slots)
			{
				if(slot.resource == resource) return;
			}

			foreach(UnitResourceSlot slot in slots)
			{
				if(!slot.HasResource()) 
				{
					slot.Set(resource);
					break;
				}
			}

			UpdateBackground();
		}
		
		public void UpdateBackground()
		{
			RectTransform minRect = null;
			RectTransform maxRect = null;

			for(int i = 0; i < slots.Count; i++)
			{
				if(slots[i].HasResource())
				{
					maxRect = slots[i].rect;
					if(minRect == null) minRect = slots[i].rect;
				}
			}

			if(minRect != null && maxRect != null)
			{
				background.Fit(minRect, maxRect);
				background.Show();
			}
			else
			{
				background.Hide();
			}
		}

		public void Clear()
		{
			foreach(UnitResourceSlot slot in slots)
			{
				slot.Clear();
			}

			background.Hide();
		}
	}
}