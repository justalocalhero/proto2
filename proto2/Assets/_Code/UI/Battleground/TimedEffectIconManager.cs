﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tooltips;

public class TimedEffectIconManager : MonoBehaviour 
{
	public UnitHUD parent;
	public Transform iconContainer, backgroundContainer;
	public FloatReference spacing, backgroundOffset;
	public TimedEffectIcon timedEffectIconPrototype;
	private List<TimedEffectIcon> icons;
	public TransformFitter backgroundPrototype;
	private TransformFitter background;
	
	public void Awake()
	{
		BuildIcons();
		if(icons.Count > 0) BuildBackground();
	}

	public void SetParent(UnitHUD hud)
	{
		parent = hud;
	}

	public void SetParentToggler(UIToggler parentToggler)
	{
		foreach(TimedEffectIcon icon in icons)
		{
			icon.SetParentToggler(parentToggler);
		}
	}

	public void SetTimedEffectTooltip(TimedEffectTooltip timedEffectTooltip)
	{
		foreach(TimedEffectIcon icon in icons)
		{
			icon.SetTimedEffectTooltip(timedEffectTooltip);
		}
	}

	public void SetEffectType(EffectType effectType)
	{
		foreach(TimedEffectIcon icon in icons)
		{
			icon.SetEffectType(effectType);
		}
	}

	public void Clear()
	{
		foreach(TimedEffectIcon icon in icons)
		{
			icon.Clear();
		}
		background.Hide();
	}

	public void BuildIcons()
	{
		GridBuilder<TimedEffectIcon> gridBuilder = new GridBuilder<TimedEffectIcon>();

		icons = gridBuilder.Begin()
			.WithPrototype(timedEffectIconPrototype)
			.WithContainer(iconContainer)
			.WithSpacing(spacing, spacing)
			.WithRows(1)
			.Build();

		RegisterDelegates();
	}

	public void BuildBackground()
	{
		background = Object.Instantiate(backgroundPrototype, backgroundContainer, false);
		background.SetOffset(backgroundOffset, backgroundOffset);
	}

	public void RegisterDelegates()
	{
		foreach(TimedEffectIcon icon in icons)
		{
			icon.onClear += Collapse;
		}
	}

	public void Collapse()
	{
		int insert = icons.Count;

		for(int i = 0; i < icons.Count; i++)
		{
			if(!icons[i].HasEffect() && i < insert) insert = i;
			else if(icons[i].HasEffect() && i > insert) 
			{
				Collapse(icons[i], icons[insert]);
			}
		}

		UpdateBackground();
	}

	public void Collapse(TimedEffectIcon send, TimedEffectIcon recieve)
	{
		if(parent == null || !parent.HasUnit()) return;
		if(!send.HasEffect() || recieve.HasEffect()) return;
		TimedEffect toSend = send.effect;
		recieve.Set(toSend);
		send.Clear();
	}

	public void UpdateBackground()
	{
		RectTransform minRect = null;
		RectTransform maxRect = null;

		for(int i = 0; i < icons.Count; i++)
		{
			if(icons[i].HasEffect())
			{
				maxRect = icons[i].rect;
				if(minRect == null) minRect = icons[i].rect;
			}
		}

		if(minRect != null && maxRect != null)
		{
			background.Fit(minRect, maxRect);
			background.Show();
		}
		else
		{
			background.Hide();
		}
	}

	public void HandleEffectAdded(TimedEffect effect)
	{
		foreach(TimedEffectIcon icon in icons)
		{
			if(!icon.HasEffect()) 
			{
				icon.Set(effect);
				break;
			}
		}

		UpdateBackground();
	}
}
