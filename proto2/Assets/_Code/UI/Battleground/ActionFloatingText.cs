﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionFloatingText : MonoBehaviour 
{
	public DamageColorMap damageColorMap;
	public FloatingTextManager floatingText;

	public void SetParentToggler(UIToggler parentToggler)
	{
		floatingText.SetParentToggler(parentToggler);
	}

	public void SetFloatingTextPool(ObjectPool objectPool)
	{
		floatingText.SetFloatingTextPool(objectPool);
	}

	public void Push(string text)
	{
		floatingText.Prepare(text);
	}

	public void HandleDamage(Damage damage)
	{
		if(damage.value == 0) return;
		
		string text = GetDamageText(damage);

		floatingText.Prepare(text);
	}
	
	public void HandleAction(Action action)
	{
		string text = GetActionText(action);
		floatingText.Prepare(text);
	}

	private string GetActionText(Action action)
	{
		switch(action.result)
		{
			case ActionResult.block:
				return "Block";
			case ActionResult.dodge:
				return "Dodge";
			case ActionResult.reflect:
				return "Reflect";
			case ActionResult.miss:
				return "Miss";
			default:
				return "";
		}
	}
	

	private string GetDamageText(Damage damage)
	{
		Color color = damageColorMap.GetColor(damage.type);
		string colorString = ColorUtility.ToHtmlStringRGBA(color);
		string valueString = "" + Mathf.Round(Mathf.Abs(damage.value));
		if(damage.type == DamageType.healing) valueString = "+" + valueString;
		if(damage.result == ActionResult.crit) valueString += "!";

		return "<color=#" + colorString + ">" + valueString + "</color>";
	}
}
