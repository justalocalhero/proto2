﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CampingUI : MonoBehaviour 
{
	public Image tentSprite;
	public UIToggler tentToggler;
	public Flash darknessMask;
	public Flash tentMaskIn;
	public Flash tentMaskOut;

	public void Awake()
	{
		darknessMask.onMid += HideTent;
		tentMaskOut.onComplete += tentMaskOut.Reset;
		tentMaskOut.onComplete += tentToggler.Hide;
	}

	public void Play()
	{
		ShowTent();
		darknessMask.Play();
	}

	public void ShowTent()
	{
		tentMaskIn.Play();
		tentToggler.Show();
	}

	public void HideTent()
	{
		tentMaskOut.Play();
	}
}
