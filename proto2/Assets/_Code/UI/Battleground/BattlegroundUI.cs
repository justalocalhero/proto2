﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Tooltips;

namespace Battling
{
	public class BattlegroundUI : MonoBehaviour 
	{
		public ExpandedBattlegroundUI expandedBattlegroundUI;
		public Button button;
		public TextMeshProUGUI label;
		public ObjectPool floatingTextPool;
		public UIToggler parentToggler;
		public ScrollingBackground scrollingBackground;
		public Battleground battleground;
		public CampingUI campingUI;
		public Transform playerHUDContainer, enemyHUDContainer;
		public UnitHUD playerHUDPrototype, enemyHUDPrototype;
		private UnitHUD playerHUD, enemyHUD;
		private BattlegroundState currentState;

		public ActionTooltip actionTooltip;
		public TimedEffectTooltip timedEffectTooltip;
		public PotionTooltip potionTooltip;
		public UnitResourceTooltip unitResourceTooltip;
		public ColorReference randomEventColor;

		public void Awake()
		{
			BuildHUDS();
		}

		public void Start()
		{
			if(battleground != null) battleground.onPlayerChanged += HandlePlayerChanged;
			if(battleground != null) battleground.onEnemyChanged += HandleEnemyChanged;
			if(battleground != null) battleground.onStateChanged += HandleBattlegroundStateChange;			
			if(battleground != null) battleground.onRandomEvent += HandleRandomEvent;
			button.onClick.AddListener(HandleClick);
		}

		public void BuildHUDS()
		{
			playerHUD = BuildHUD(playerHUDContainer, playerHUDPrototype);
			enemyHUD = BuildHUD(enemyHUDContainer, enemyHUDPrototype);
			if(battleground != null) battleground.onPlayerDeath += (Player player) => enemyHUD.Clear();
		}

		public UnitHUD BuildHUD(Transform container, UnitHUD prototype)
		{
			UnitHUD toReturn = Object.Instantiate(prototype, container, false);
			toReturn.SetParentToggler(parentToggler);
			toReturn.SetFloatingTextPool(floatingTextPool);
			toReturn.SetActionTooltip(actionTooltip);
			toReturn.SetTimedEffectTooltip(timedEffectTooltip);
			toReturn.SetPotionTooltip(potionTooltip);
			toReturn.SetUnitResourceTooltip(unitResourceTooltip);

			return toReturn;
		}

		public void HandleClick()
		{
			expandedBattlegroundUI.SetBattleground(battleground);
		}

		public void HandlePlayerChanged(Unit unit)
		{
			if(unit == null) return;

			playerHUD.Clear();
			enemyHUD.Clear();
			playerHUD.SetUnit(unit);
		}

		public void HandleEnemyChanged(Unit unit)
		{
			if(unit == null) return;
			
			enemyHUD.SetUnit(unit);
		}

		public void HandleRandomEvent(RandomEventGenerator.EventType eventType)
		{
			string text = GetEventText(eventType);
			playerHUD.PushFloatingText(text);
		}

		public string GetEventText(RandomEventGenerator.EventType eventType)
		{
			return Tooltips.ColoredText.GetText(eventType.ToString(), randomEventColor);
		}

		public void HandleBattlegroundStateChange(BattlegroundState previousState, BattlegroundState nextState)
		{
			SetState(nextState);
		}

		public void SetState(BattlegroundState battlegroundState)
		{
			ClearActive();

			currentState = battlegroundState;
			SetActive();
		}

		private void SetActive()
		{
			switch(currentState)
			{
				case BattlegroundState.travel:
					SetTravel();
					break;
				case BattlegroundState.camping:
					SetCamping();
					break;
				case BattlegroundState.fighting:
					SetFighting();
					break;
				case BattlegroundState.exploring:
					SetExploring();
					break;
				case BattlegroundState.idle:
					SetIdle();
					break;
				default:
					break;
			}

		}

		private void SetTravel()
		{
			scrollingBackground.Activate();
		}

		private void SetCamping()
		{
			campingUI.Play();
		}

		private void SetExploring()
		{

		}

		private void SetFighting()
		{

		}

		private void SetIdle()
		{

		}

		private void ClearActive()
		{
			switch(currentState)
			{
				case BattlegroundState.travel:
					ClearTravel();
					break;
				case BattlegroundState.camping:
					ClearCamping();
					break;
				case BattlegroundState.fighting:
					ClearFighting();
					break;
				case BattlegroundState.exploring:
					ClearExploring();
					break;
				case BattlegroundState.idle:
					ClearIdle();
					break;
				default:
					break;
			}
		}

		private void ClearTravel()
		{
			scrollingBackground.Deactivate();
		}

		private void ClearCamping()
		{

		}

		private void ClearExploring()
		{

		}

		private void ClearFighting()
		{

		}

		private void ClearIdle()
		{
			
		}

		public void Unlock()
		{
			button.interactable = true;
		}

		public void Lock()
		{
			button.interactable = false;
		}

		public void UpdateName()
		{
			label.SetText(battleground.GetName());

		}
	}
}
