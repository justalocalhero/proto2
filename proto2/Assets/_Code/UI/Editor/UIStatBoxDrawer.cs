﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(UIStatBox))]
public class UIStatBoxDrawer : PropertyDrawer 
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int typeSize = 80;
        int meshSize = 80;
        int spacing = 5;
        int typeIndent =  0;
        int meshIndent = typeIndent + typeSize + spacing;

        Rect typeRect = new Rect(position.x + typeIndent, position.y, typeSize, position.height);
        Rect meshRect = new Rect(position.x + meshIndent, position.y, meshSize, position.height);

        EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("statType"), GUIContent.none);
        EditorGUI.PropertyField(meshRect, property.FindPropertyRelative("playerBox"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

}