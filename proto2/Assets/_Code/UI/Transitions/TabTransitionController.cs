﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TabTransitionController : MonoBehaviour 
{
	private UIToggler toggler;

	void Start () 
	{
		toggler = GetComponent<UIToggler>();
	}
	
	public void SetParent(Transform newParent)
	{
		transform.SetParent(newParent);
	}

	public void Hide()
	{
		toggler.Hide();
	}

	public void Show()
	{
		toggler.Show();
	}

	public void SetRaycast(bool toCast)
	{
		toggler.SetRaycast(toCast);
	}

	public void ResetTab(Transform target)
	{		
		Hide();
		SetParent(target);
	}

	public void Activate(Transform target)
	{
		Show();
		SetParent(target);
	}

	public void SnapPosition(Transform target)
	{
		SetParent(target);
		transform.localPosition = Vector3.zero;
	}
}
