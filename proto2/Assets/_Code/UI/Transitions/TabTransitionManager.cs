﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TabTransitionManager : MonoBehaviour 
{
	private TabTransitionController activeTab;
	public Transform startTarget;
	public Transform inTarget;
	public Transform outTarget;
	public EaseReference ease;
	public FloatReference transitionTime;

	private void HideTab(TabTransitionController tab)
	{
		if(tab != null) 
		{
			tab.Hide();
			activeTab = null;
			tab.SnapPosition(outTarget);
		}
	}

	public void SetTab(TabTransitionController tab)
	{
		if(tab == activeTab) return;
		HideTab(activeTab);
		if(tab == null) return;
		activeTab = tab;
		tab.SnapPosition(inTarget);
		tab.Show();
	}
}
