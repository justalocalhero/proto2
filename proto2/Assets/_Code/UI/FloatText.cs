﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FloatText : MonoBehaviour 
{
	public FloatVariable text;
	public TextMeshProUGUI mesh;

	public void Start()
	{
		if(mesh == null) mesh = GetComponent<TextMeshProUGUI>();
		UpdateText();
	}

	public void UpdateText()
	{
		if(text != null) mesh.text = "" + text.Value;
	}
}
