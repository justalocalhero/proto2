﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class EffectNameText : MonoBehaviour 
	{
		public ColorReference buff, debuff;

		public string GetText(TimedEffect effect, EffectType effectType)
		{
			if(effect == null) return "";


			return ColoredText.GetText(effect.name, GetColor(effectType));
		}

		private Color GetColor(EffectType effectType)
		{
			switch(effectType)
			{
				case EffectType.buff:
					return buff;
				case EffectType.debuff:
					return debuff;
				default:
					return new Color();
			}
		}
	}
}