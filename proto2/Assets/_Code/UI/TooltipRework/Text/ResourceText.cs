﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class ResourceText : MonoBehaviour 
	{
		public NameText nameText;

		public ColorReference defaultColor, affordColor, noAffordColor;
		public ResourceNameMap resourceNames;

		
		public string GetText(List<Resource> resources, string lineDelimiter, string nonEmptyPrefix, string nonEmptySuffix)
		{
			if(resources == null || resources.Count == 0) return "";
		
			string toReturn = "";
			
			foreach(Resource resource in resources)
			{
				toReturn += (GetText(resource) + lineDelimiter);
			}

			return (nonEmptyPrefix + toReturn + nonEmptySuffix);
		}

		public string GetText(List<Resource> resources, ResourceManager manager, string lineDelimiter, string nonEmptyPrefix, string nonEmptySuffix)
		{
			if(resources == null || resources.Count == 0) return "";
		
			string toReturn = "";
			
			foreach(Resource resource in resources)
			{
				toReturn += (GetText(resource, manager) + lineDelimiter);
			}

			return (nonEmptyPrefix + toReturn + nonEmptySuffix);
		}

		public string GetNameText(Resource resource)
		{
			string nameString = nameText.GetText(resourceNames.GetName(resource.type)) + " ";

			return (nameString);
		}

		public string GetValueText(Resource resource)
		{
			string valueString = ColoredText.GetText(FormatValue(resource.value), defaultColor);

			return (valueString);
		}

		public string GetText(Resource resource)
		{
			string nameString = nameText.GetText(resourceNames.GetName(resource.type)) + " ";
			string valueString = ColoredText.GetText(FormatValue(resource.value), defaultColor);

			return (nameString + valueString);
		}

		public string GetText(Resource resource, ResourceManager manager)
		{
			string nameString = nameText.GetText(resourceNames.GetName(resource.type)) + " ";
			string valueString = ColoredText.GetText(FormatValue(resource.value), GetAffordColor(resource, manager));

			return (nameString + valueString);
		}

		private string FormatValue(float value)
		{
			return Mathf.Abs(value).ToString();
		}

		private Color GetAffordColor(Resource resource, ResourceManager manager)
		{
			return (manager.CheckChange(resource)) ? affordColor  : noAffordColor;
		}
	}
}