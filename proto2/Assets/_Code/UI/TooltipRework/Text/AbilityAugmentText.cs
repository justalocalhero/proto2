﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class AbilityAugmentText : MonoBehaviour 
	{
		public NameText nameText;
		public ActionConditionText actionConditionText;
		public DurationChangeText durationChangeText;
		public DamageText damageText;
		public ConditionalResourceText conditionalResourceText;
		public ConditionalStatText conditionalStatText;
		public PurgeText purgeText;
		public DispelText dispelText;
		public TimedEffectText timedEffectText;

		public string GetText(AbilityAugment augment, ProficiencyType proficiencyType, Unit unit)
		{
			if(augment == null) return "";
			string augmentConditionString = actionConditionText.GetText(augment.actionConditions, "\n\n", "", "");
			string durationString = durationChangeText.GetText(augment.period, "\n\n",  "");
			string unitResourceString = conditionalResourceText.GetText(augment.conditionalResources, "\n", "\n", "");
			string damageString = damageText.GetText(augment.damages, proficiencyType, unit, "\n", "", "\n", "");
			string selfDamageString = damageText.GetText(augment.selfDamages, proficiencyType, unit, "\n", " to self", "\n", "");
			string conditionalStatString = conditionalStatText.GetText(augment.instanceStats, "\n", "\n", "");
			string dispelString = (augment.purgeCount + augment.cleanseCount > 0) ? "\n" : "";
			string purgeString = purgeText.GetText(augment.purgeCount, "\n", "");
			string cleanseString = dispelText.GetText(augment.cleanseCount, "\n", "");
			string buffString = timedEffectText.GetBuffText(augment.buffs, proficiencyType, unit, "\n", "\n", "");
			string debuffString = timedEffectText.GetDebuffText(augment.debuffs, proficiencyType, unit, "\n", "\n", "");
			
			return (augmentConditionString + durationString + unitResourceString + damageString + selfDamageString + conditionalStatString + dispelString + purgeString + cleanseString + buffString + debuffString);		
		}

		public string GetText(AbilityAugment augment)
		{
			if(augment == null) return "";

			string augmentConditionString = actionConditionText.GetText(augment.actionConditions, "\n\n", "", "");
			string durationString = durationChangeText.GetText(augment.period, "\n\n", "");
			string unitResourceString = conditionalResourceText.GetText(augment.conditionalResources, "\n", "\n", "");
			string damageString = damageText.GetText(augment.damages, "\n", "", "\n", "");
			string selfDamageString = damageText.GetText(augment.selfDamages, "\n", " to self", "\n", "");
			string conditionalStatString = conditionalStatText.GetText(augment.instanceStats, "\n", "\n", "");
			string dispelString = (augment.purgeCount + augment.cleanseCount > 0) ? "\n" : "";
			string purgeString = purgeText.GetText(augment.purgeCount, "\n", "");
			string cleanseString = dispelText.GetText(augment.cleanseCount, "\n", "");
			string buffString = timedEffectText.GetBuffText(augment.buffs, "\n", "\n", "");
			string debuffString = timedEffectText.GetDebuffText(augment.debuffs, "\n", "\n", "");
			
			return (augmentConditionString + durationString + unitResourceString + damageString + selfDamageString + conditionalStatString + dispelString + purgeString + cleanseString + buffString + debuffString);		
		}
	}
}