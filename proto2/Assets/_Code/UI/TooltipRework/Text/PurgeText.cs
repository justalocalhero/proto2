﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class PurgeText : MonoBehaviour 
	{
		public ColorReference purgeColor;
		public StringReference prefix, singularSuffix, pluralSuffix;

		public string GetText(int count, string nonZeroPrefix, string  nonZeroSuffix)
		{
			if(count == 0) return "";
			string suffix = (count == 1) ? singularSuffix : pluralSuffix;

			return ColoredText.GetText((nonZeroPrefix + prefix + count + suffix + nonZeroSuffix), purgeColor);
		}
	}
}