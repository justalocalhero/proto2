﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class NameText : MonoBehaviour 
	{
		public ColorReference nameColor;

		public string GetText(string name)
		{
			return GetText(name, "", "");
		}
		
		public string GetText(string name, string nonZeroPrefix, string nonZeroSuffix)
		{
			if(name == null || name == "") return "";

			return ColoredText.GetText(nonZeroPrefix + name + nonZeroSuffix, nameColor);
		}
	}
}