﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class DurationChangeText : MonoBehaviour 
	{
		public ColorReference durationChangeColor;
		public DurationText durationText;
		public StringReference positivePrefix, negativePrefix, suffix;

		public string GetText(float duration, string nonZeroPrefix, string nonZeroSuffix)
		{
			float absDuration = Mathf.Abs(duration);
			float truncDuration =  Utility.Truncate(absDuration, 2);
			if(truncDuration == 0) return "";
			string prefix = (duration > 0) ? positivePrefix : negativePrefix;

			return ColoredText.GetText((nonZeroPrefix + prefix + absDuration + suffix + nonZeroSuffix), durationChangeColor);
		}
	}
}