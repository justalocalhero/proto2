﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class StatComparisonGenerator : MonoBehaviour 
	{
		public List<StatComparison> Generate(Equipment initial, Equipment toCompare)
		{		
			if(initial == null) return new List<StatComparison>();

			List<StatComparison> toReturn = GenerateInitialList(initial);

			if(toCompare == null) return toReturn;

			foreach(EquipStat stat in toCompare.GetBaseStats())
			{
				CompareStat(toReturn, stat);
			}

			foreach(EquipStat stat in toCompare.GetMagicStats())
			{
				CompareStat(toReturn, stat);
			}

			UpdateUnchecked(toReturn);

			return toReturn;
		}

		private List<StatComparison> GenerateInitialList(Equipment initial)
		{
			List<StatComparison> toReturn = new List<StatComparison>();

			if(initial == null) return toReturn;

			foreach(EquipStat currentStat in initial.GetBaseStats())
			{
				if(CheckVisible(currentStat))
				{
					
					toReturn.Add(new StatComparison
					{
						stat = currentStat,
						difValue = 0,
						type = IsNegative(currentStat) ? StatTooltipType.negative : StatTooltipType.basic,
						isChecked = false
					});
				}
			}

			foreach(EquipStat currentStat in initial.GetMagicStats())
			{
				if(CheckVisible(currentStat))
				{
					toReturn.Add(new StatComparison
					{
						stat = currentStat,
						difValue = 0,
						type = IsNegative(currentStat) ? StatTooltipType.negative : StatTooltipType.rolled,
						isChecked = false
					});
				}
			}

			return toReturn;
		}

		private void UpdateUnchecked(List<StatComparison> stats)
		{
			for(int i = 0; i < stats.Count; i++)
			{
				if(!stats[i].isChecked)
				{					
					StatComparison current = stats[i];				
				
					stats[i] = new StatComparison
					{
						stat = current.stat,
						difValue = -FormattedValue(current.stat),
						type = current.type,
						isChecked = true
					};
				}
			}
		}

		private void CompareStat(List<StatComparison> initial, EquipStat toCompare)
		{
			for(int i = 0; i < initial.Count; i++)
			{
				StatComparison current = initial[i];

				if(ComparableStat(current.stat, toCompare))
				{
					float tempDif = FormattedValue(toCompare) - FormattedValue(current.stat);

					initial[i] = new StatComparison
					{
						stat = current.stat,
						difValue = tempDif,
						type = current.type,
						isChecked = true
					};
					return;
				}
			}

			initial.Add(
				new StatComparison
				{
					stat = new EquipStat
					{
						Value = 0,
						StatType = toCompare.StatType,
						ModifierType = toCompare.ModifierType
					},
					difValue = toCompare.Value,
					type = StatTooltipType.ghost,
					isChecked = true
				}
			);
		}

		private bool ComparableStat(EquipStat stat1, EquipStat stat2)
		{
			return (stat1.StatType == stat2.StatType && stat1.ModifierType == stat2.ModifierType);
		}

		private bool CheckVisible(EquipStat stat)
		{
			switch(stat.ModifierType)
			{
				case ModifierType.Flat:
					return (Mathf.CeilToInt(stat.Value) != 0);
				case ModifierType.Multiplier:
					return (Utility.Truncate(stat.Value, 2) != 0);
				case ModifierType.GlobalMultiplier:
					return (Utility.Truncate(stat.Value, 2) != 0);
				default:
					return false;
			}
		}

		private float FormattedValue(EquipStat stat)
		{
			switch(stat.ModifierType)
			{
				case ModifierType.Flat:
					return Mathf.CeilToInt(stat.Value);
				case ModifierType.Multiplier:
					return Utility.Truncate(stat.Value, 2);
				case ModifierType.GlobalMultiplier:
					return Utility.Truncate(stat.Value, 2);
				default:
					return 0;
			}
		}

		private bool IsNegative(EquipStat stat)
		{
			bool isNegative = stat.Value < 0;
			bool isDetrimental = stat.StatType == StatType.Weight;

			return (isNegative ^ isDetrimental);
		}
	}

	public struct StatComparison {public EquipStat stat; public float difValue; public StatTooltipType type; public bool isChecked;}
	public enum StatTooltipType { negative, basic, rolled, ghost, }
}