﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class PotionNameText : MonoBehaviour 
	{
		public IntReference size;
		public ColorReference color;
		public List<StringReference> healingLevelNames;
		public StringReference noIngredientString, baseName;

		public string GetText(Potion potion)
		{
			if(potion == null) return "";

			string levelString = GetHealingLevelString(potion.healingAmount) + " ";
			string addonString = GetAddonString(potion.potionSupplies) + " ";
			string nameString = baseName;

			return SizedText.GetText(ColoredText.GetText(levelString + addonString + nameString, color), size);
		}

		private string GetHealingLevelString(float healingAmount)
		{
			if(healingLevelNames == null || healingLevelNames.Count == 0) return "";

			int index = Mathf.Clamp(Mathf.RoundToInt(healingAmount - 10) / 10, 0, healingLevelNames.Count - 1);

			return healingLevelNames[index];
		}

		private string GetAddonString(List<PotionSupply> supplies)
		{
			if(supplies == null || supplies.Count == 0) return noIngredientString;

			else return supplies[0].name;
		}
	}
}