﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class EquipStatText : MonoBehaviour 
	{
		public StatText statText;
		public ColorReference basicColor, rolledColor, negativeColor;

		public string GetText(List<EquipStat> baseStats, List<EquipStat> rolledStats, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{
			if(baseStats == null || rolledStats == null || baseStats.Count + rolledStats.Count == 0) return "";

			string toReturn = "";

			foreach(EquipStat stat in baseStats)
			{
				if(CheckVisible(stat)) toReturn += (GetBasicText(stat) +  lineDelimiter);
			}

			foreach(EquipStat stat in rolledStats)
			{
				if(CheckVisible(stat)) toReturn += (GetRolledText(stat) + lineDelimiter);
			}

			return (nonZeroPrefix + toReturn + nonZeroSuffix);
		}

		public string GetRolledText(EquipStat[] stats, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{
			if(stats == null || stats.Length == 0) return "";

			string toReturn = "";

			foreach(EquipStat stat in stats)
			{
				if(CheckVisible(stat)) toReturn += (GetRolledText(stat) + lineDelimiter);
			}

			return (nonZeroPrefix + toReturn + nonZeroSuffix);

		}

		public string GetBasicText(EquipStat[] stats, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{
			if(stats == null || stats.Length == 0) return "";

			string toReturn = "";

			foreach(EquipStat stat in stats)
			{
				if(CheckVisible(stat)) toReturn += (GetBasicText(stat) + lineDelimiter);
			}

			return (nonZeroPrefix + toReturn + nonZeroSuffix);

		}

		public string GetRolledText(EquipStat stat)
		{
			return GetText(stat, rolledColor);
		}

		public string GetBasicText(EquipStat stat)
		{
			return GetText(stat, basicColor);
		}

		private string GetText(EquipStat stat, Color positiveColor)
		{
			if(!CheckVisible(stat)) return "";

			bool isNegative = (stat.Value < 0);
			bool isDetrimental = stat.StatType == StatType.Weight;

			Color color = (isNegative ^ isDetrimental) ? negativeColor : positiveColor;

			return ColoredText.GetText(statText.GetText(stat), color);

		}

		private bool CheckVisible(EquipStat stat)
		{
			bool visible;

			switch(stat.ModifierType)
			{
				case ModifierType.Flat:
					visible = (Mathf.CeilToInt(stat.Value) != 0);
					break;
				case ModifierType.Multiplier:
					visible = (Utility.Truncate(stat.Value, 2) != 0);
					break;
				case ModifierType.GlobalMultiplier:
					visible = (Utility.Truncate(stat.Value, 2) != 0);
					break;
				default:
					visible = false;
					break;
			}
			
			return visible;
		}
	}
}