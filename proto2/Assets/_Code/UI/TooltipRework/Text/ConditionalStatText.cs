﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class ConditionalStatText : MonoBehaviour 
	{
		public ActionConditionText actionConditionText;
		public EffectStatText effectStatText;
		
		public string GetText(ConditionalStat[] stats, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{
			if(stats == null || stats.Length == 0) return "";

			string toReturn = "";

			foreach(ConditionalStat stat in stats)
			{
				toReturn += (lineDelimiter + GetText(stat, lineDelimiter, nonZeroPrefix, nonZeroSuffix));
			}

			return toReturn;
		}

		public string GetText(ConditionalStat conditionalStat, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{
			string conditionString = actionConditionText.GetText(conditionalStat.condition, nonZeroPrefix, "");
			string statString = effectStatText.GetText(conditionalStat.stats, lineDelimiter, "", nonZeroSuffix);

			return (conditionString + statString);
		}

	}
}