﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class EffectStatText : MonoBehaviour 
	{
		public StatText statText;
		public ColorReference positiveColor, negativeColor;

		public string GetText(List<EquipStat> stats, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{
			if(stats == null || stats.Count == 0) return "";

			string toReturn = "";

			foreach(EquipStat stat in stats)
			{
				toReturn += (lineDelimiter + GetText(stat));
			}

			return (nonZeroPrefix + toReturn + nonZeroSuffix);

		}
		
		public string GetText(EquipStat[] stats, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{
			if(stats == null || stats.Length == 0) return "";

			string toReturn = "";

			foreach(EquipStat stat in stats)
			{
				toReturn += (lineDelimiter + GetText(stat));
			}

			return (nonZeroPrefix + toReturn + nonZeroSuffix);

		}

		public string GetText(EquipStat stat)
		{
			Color color = (stat.Value >= 0) ? positiveColor : negativeColor;

			return ColoredText.GetText(statText.GetText(stat), color);
		}
	}
}
