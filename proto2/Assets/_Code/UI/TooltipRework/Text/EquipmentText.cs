﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class EquipmentText : MonoBehaviour 
	{
		public EquipmentNameText equipmentNameText;
		public EquipSlotText equipSlotText;
		public EquipmentValueText equipmentValueText;
		public EquipStatText equipStatText;
		public AbilityText abilityText;
		public TriggeredActionText triggeredActionText;

		public string GetText(Equipment equipment)
		{
			if(equipment == null) return "";
			
			string nameString = equipmentNameText.GetText(equipment) + "\n";
			string slotString = equipSlotText.GetText(equipment.GetSlot()) + " ";
			string valueString = equipmentValueText.GetText(equipment.level) + "\n";
			string statString = equipStatText.GetText(equipment.GetBaseStats(), equipment.GetMagicStats(), "\n", "\n", "");
			string abilityString = abilityText.GetText(equipment.abilities, "\n", "\n", "");
			string triggerString = triggeredActionText.GetText(equipment.triggeredActions, "\n", "\n", "");

			return (nameString + slotString + valueString + statString + abilityString + triggerString);
		}
	}
}