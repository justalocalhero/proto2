﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
    public class TimedEffectText : MonoBehaviour 
    {
        public EffectStatText effectStatText;
        public DamageText damageText;
        public EffectNameText effectNameText;
        public PeriodText periodText;
        public DurationText durationText;
        public EffectTypeText effectTypeText;

        public string GetBuffText(TimedEffect[] timedEffects, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
        {
            if(timedEffects == null || timedEffects.Length == 0) return "";

            string toReturn = "";

            foreach(TimedEffect effect in timedEffects)
            {
                toReturn += (lineDelimiter + GetBuffText(effect));
            }

            return nonZeroPrefix + toReturn + nonZeroSuffix;
        }
        
        public string GetBuffText(TimedEffect[] timedEffects, ProficiencyType type, Unit unit, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
        {
            if(timedEffects == null || timedEffects.Length == 0) return "";

            string toReturn = "";

            foreach(TimedEffect effect in timedEffects)
            {
                toReturn += (lineDelimiter + GetBuffText(effect, type, unit));
            }

            return nonZeroPrefix + toReturn + nonZeroSuffix;

        }

        public string GetDebuffText(TimedEffect[] timedEffects, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
        {
            if(timedEffects == null || timedEffects.Length == 0) return "";

            string toReturn = "";

            foreach(TimedEffect effect in timedEffects)
            {
                toReturn += (lineDelimiter + GetDebuffText(effect));
            }

            return nonZeroPrefix + toReturn + nonZeroSuffix;

        }
        
        public string GetDebuffText(TimedEffect[] timedEffects, ProficiencyType type, Unit unit, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
        {
            if(timedEffects == null || timedEffects.Length == 0) return "";

            string toReturn = "";

            foreach(TimedEffect effect in timedEffects)
            {
                toReturn += (lineDelimiter + GetDebuffText(effect, type, unit));
            }

            return nonZeroPrefix + toReturn + nonZeroSuffix;

        }

        public string GetBuffText(TimedEffect timedEffect, ProficiencyType type, Unit unit)
        {
            return GetText(timedEffect, type, unit, EffectType.buff);
        }

        public string GetBuffText(TimedEffect timedEffect)
        {
            return GetText(timedEffect, EffectType.buff);
        }

        public string GetDebuffText(TimedEffect timedEffect, ProficiencyType type, Unit unit)
        {
            return GetText(timedEffect, type, unit, EffectType.debuff);
        }

        public string GetDebuffText(TimedEffect timedEffect)
        {
            return GetText(timedEffect, EffectType.debuff);
        }

        private string GetText(TimedEffect timedEffect, ProficiencyType type, Unit unit, EffectType effectType)
        {
            bool hasPeriodic = timedEffect.period > 0;

            string nameString = effectNameText.GetText(timedEffect, effectType) + "\n";
            string typeString = effectTypeText.GetText(effectType) + "\n";
            string durationString = durationText.GetText(timedEffect.duration, unit, effectType);
            string periodString = (!hasPeriodic) ? "" : periodText.GetText(timedEffect.period);
            string finalDamageString = damageText.GetText(timedEffect.finalDamages, type, unit, "\n", "", "\n", "");
            string periodicDamageString = (!hasPeriodic) ? "" : damageText.GetText(timedEffect.periodicDamages, type, unit, "\n", periodString, "\n", "");
            string statString = effectStatText.GetText(timedEffect.stats, "\n", "\n", "");

            return (nameString + typeString + durationString + finalDamageString + periodicDamageString + statString);
        }

        private string GetText(TimedEffect timedEffect, EffectType effectType)
        {
            bool hasPeriodic = timedEffect.period > 0;
            
            string nameString = effectNameText.GetText(timedEffect, effectType) + "\n";
            string typeString = effectTypeText.GetText(effectType) + "\n";
            string durationString = durationText.GetText(timedEffect.duration);
            string periodString =  (!hasPeriodic) ? "" : " " + periodText.GetText(timedEffect.period);
            string finalDamageString = damageText.GetText(timedEffect.finalDamages, "\n", "", "\n", "");
            string periodicDamageString =  (!hasPeriodic) ? "" : damageText.GetText(timedEffect.periodicDamages, "\n", periodString, "\n", "");
            string statString = effectStatText.GetText(timedEffect.stats, "\n", "\n", "");

            return (nameString + typeString + durationString + finalDamageString + periodicDamageString + statString);
        }
    }
}