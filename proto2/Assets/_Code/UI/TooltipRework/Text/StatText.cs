﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class StatText : MonoBehaviour 
	{
		public StatNameDictionary statDictionary;
		public StatValueText statValueText;

		public string GetText(EquipStat[] stats, string lineDelimiter, string nonEmptyPrefix, string nonEmptySuffix)
		{
			return GetText(stats, 1, lineDelimiter, nonEmptyPrefix, nonEmptySuffix);
		}

		public string GetText(EquipStat[] stats, float multiplier, string lineDelimiter, string nonEmptyPrefix, string nonEmptySuffix)
		{
			string toReturn = "";

            foreach(EquipStat stat in stats)
            {
                toReturn +=  GetText(stat) + lineDelimiter;
            }
			if(stats.Length > 0) toReturn = nonEmptyPrefix + toReturn + nonEmptySuffix;

            return toReturn;
		}

		public string GetText(EquipStat stat)
		{
			return GetText(stat, 1);
		}

		public string GetText(EquipStat stat, float multiplier)
		{
			string valueString = statValueText.GetText(stat.ModifierType, stat.Value * multiplier);
			string modString = statDictionary.GetModName(stat.ModifierType) + " ";
			string typeString = statDictionary.GetTypeName(stat.StatType);

			return (valueString + modString + typeString);
		}
	}
}