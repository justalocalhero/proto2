﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Loot;

namespace Tooltips
{
	public class ComponentProfileText : MonoBehaviour 
	{
		public NameText nameText;
		public EquipStatText statText;
		public ResourceText resourceText;
		public StatEquivalencies statEquivalencies;
		public EquipSlotMultiplierMap slotMap;

		public string GetStatsText(List<ComponentProfile> main, List<ComponentProfile> replaced, EquipSlot slot)
		{
			string nameString = NameString(main);
			string statString = StatString(main, replaced, slot, "\n", "\n", "");

			return (nameString + statString);
		}

		public string GetStatsText(List<ComponentProfile> main, EquipSlot slot)
		{
			string nameString = NameString(main);
			string statString = StatString(main, slot, "\n", "\n", "");

			return (nameString + statString);
		}

		public string GetStatsText(ComponentProfile main, ComponentProfile replaced, EquipSlot slot)
		{
			string nameString = NameString(main);
			string statString = StatString(main, replaced, slot, "\n", "\n", "");

			return (nameString + statString);
		}

		public string GetStatsText(ComponentProfile main, EquipSlot slot)
		{
			string nameString = NameString(main);
			string statString = StatString(main, slot, "\n", "\n", "");

			return (nameString + statString);
		}

		public string GetCraftText(List<ComponentProfile> profile, List<ComponentProfile> replaced, EquipSlot slot, ResourceManager manager)
		{
			string nameString = NameString(profile);
			string costString = CraftingCostString(profile, manager, "\n", "\n", "");
			string statString = StatString(profile, replaced, slot, "\n", "\n", "");

			return (nameString + costString + statString);
		}
		
		public string GetUpgradeText(List<ComponentProfile> profile, List<ComponentProfile> replaced, EquipSlot slot, ResourceManager manager)
		{
			string nameString = NameString(profile);
			string costString = UpgradeCostString(profile, manager, "\n", "\n", "");
			string statString = StatString(profile, replaced, slot, "\n", "\n", "");

			return (nameString + costString + statString);
		}

		public string GetCraftText(List<ComponentProfile> profile, EquipSlot slot, ResourceManager manager)
		{
			string nameString = NameString(profile);
			string costString = CraftingCostString(profile, manager, "\n", "\n", "");
			string statString = StatString(profile, slot, "\n", "\n", "");

			return (nameString + costString + statString);
		}
		
		public string GetUpgradeText(List<ComponentProfile> profile, EquipSlot slot, ResourceManager manager)
		{
			string nameString = NameString(profile);
			string costString = UpgradeCostString(profile, manager, "\n", "\n", "");
			string statString = StatString(profile, slot, "\n", "\n", "");

			return (nameString + costString + statString);
		}

		public string GetCraftText(ComponentProfile profile, ComponentProfile replaced, EquipSlot slot, ResourceManager manager)
		{
			string nameString = NameString(profile);
			string costString = CraftingCostString(profile, manager, "\n", "\n", "");
			string statString = StatString(profile, replaced, slot, "\n", "\n", "");

			return (nameString + costString + statString);
		}

		public string GetUpgradeText(ComponentProfile profile, ComponentProfile replaced, EquipSlot slot, ResourceManager manager)
		{
			string nameString = NameString(profile);
			string costString = UpgradeCostString(profile, manager, "\n", "\n", "");
			string statString = StatString(profile, replaced, slot, "\n", "\n", "");

			return (nameString + costString + statString);
		}
	
		public string GetCraftText(ComponentProfile profile, EquipSlot slot, ResourceManager manager)
		{
			string nameString = NameString(profile);
			string costString = CraftingCostString(profile, manager, "\n", "\n", "");
			string statString = StatString(profile, slot, "\n", "\n", "");

			return (nameString + costString + statString);
		}

		public string GetUpgradeText(ComponentProfile profile, EquipSlot slot, ResourceManager manager)
		{
			string nameString = NameString(profile);
			string costString = UpgradeCostString(profile, manager, "\n", "\n", "");
			string statString = StatString(profile, slot, "\n", "\n", "");

			return (nameString + costString + statString);
		}

		public string NameString(List<ComponentProfile> profiles)
		{
			string nameString = "";

			foreach(ComponentProfile profile in profiles)
			{
				nameString += profile.name + " ";
			}

			return nameText.GetText(nameString) + "\n";
		}

		public string NameString(ComponentProfile profile)
		{
			return nameText.GetText(profile.name) + "\n";
		}

		public string UpgradeCostString(List<ComponentProfile> profiles, ResourceManager manager, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{			
			List<Resource> toCheck = new List<Resource>();

			foreach(ComponentProfile profile in profiles)
			{
				toCheck.AddRange(profile.recipeCost.upgradeCost);
			}

			return CostString(manager.MergeResources(toCheck), manager, lineDelimiter, nonZeroPrefix, nonZeroSuffix);
		}

		public string CraftingCostString(List<ComponentProfile> profiles, ResourceManager manager, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{
			List<Resource> toCheck = new List<Resource>();
			
			foreach(ComponentProfile profile in profiles)
			{
				toCheck.AddRange(profile.recipeCost.craftingCost);
			}

			return CostString(manager.MergeResources(toCheck), manager, lineDelimiter, nonZeroPrefix, nonZeroSuffix);
		}

		public string UpgradeCostString(ComponentProfile profile, ResourceManager manager, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{			
			return CostString(profile.recipeCost.upgradeCost, manager, lineDelimiter, nonZeroPrefix, nonZeroSuffix);
		}

		public string CraftingCostString(ComponentProfile profile, ResourceManager manager, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{
			return CostString(profile.recipeCost.craftingCost, manager, lineDelimiter, nonZeroPrefix, nonZeroSuffix);
		}

		public string CostString(List<Resource> resources, ResourceManager manager, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{
			return resourceText.GetText(resources, manager, lineDelimiter, nonZeroPrefix, nonZeroSuffix);
		}

		public string StatString(List<ComponentProfile> profiles, List<ComponentProfile> replaced, EquipSlot slot, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{
			List<EquipStat> stats = new List<EquipStat>();

			foreach(ComponentProfile profile in profiles)
			{
				stats.AddRange(profile.stats);
			}
			
			List<EquipStat> replacedStats = new List<EquipStat>();

			foreach(ComponentProfile toReplace in replaced)
			{
				stats.AddRange(toReplace.stats);
			}

			replacedStats = EquipStat.NegateStats(replacedStats);

			stats.AddRange(replacedStats);

			return statText.GetBasicText(AdjustedStats(EquipStat.MergeStats(stats), slot).ToArray(), lineDelimiter, nonZeroPrefix, nonZeroSuffix);
		}
		
		public string StatString(ComponentProfile profile, ComponentProfile replaced, EquipSlot slot, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{
			List<EquipStat> stats = new List<EquipStat>();

			stats.AddRange(profile.stats);
			stats.AddRange(EquipStat.NegateStats(replaced.stats));

			return statText.GetBasicText(AdjustedStats(EquipStat.MergeStats(stats), slot).ToArray(), lineDelimiter, nonZeroPrefix, nonZeroSuffix);
		}
		
		public string StatString(List<ComponentProfile> profiles, EquipSlot slot, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{
			List<EquipStat> stats = new List<EquipStat>();

			foreach(ComponentProfile profile in profiles)
			{
				stats.AddRange(profile.stats);
			}

			return statText.GetBasicText(AdjustedStats(EquipStat.MergeStats(stats), slot).ToArray(), lineDelimiter, nonZeroPrefix, nonZeroSuffix);
		}

		public string StatString(ComponentProfile profile, EquipSlot slot, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{
			return statText.GetBasicText(AdjustedStats(profile.stats, slot).ToArray(), lineDelimiter, nonZeroPrefix, nonZeroSuffix);
		}

		private List<EquipStat> AdjustedStats(List<EquipStat> stats, EquipSlot slot)
		{
			List<EquipStat> toReturn = new List<EquipStat>();
			float slotFactor = slotMap.GetMultiplier(slot);

			foreach(EquipStat stat in stats)
			{
				EquipStat current = statEquivalencies.FactorStat(stat);
				current.Value = current.Value * slotFactor;

				toReturn.Add(current);				
			}

			return toReturn;
		}

		
	}
}