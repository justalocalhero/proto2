﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{

	public class EquipmentNameText : MonoBehaviour 
	{
		public ColorReference artifactColor, namedColor, startingColor, enchantedColor, blessedColor, cursedColor, highQualityColor, lowQualityColor;
		public QualityNames qualityNames;

		public string GetText(Equipment equipment)
		{
			if(equipment.rarity == Rarity.artifact) return SizedText.GetText(ColoredText.GetText(equipment.GetName(), artifactColor), 2);
			if(equipment.rarity == Rarity.named) return SizedText.GetText(ColoredText.GetText(equipment.GetName(), namedColor), 2);
			if(equipment.rarity == Rarity.starting) return SizedText.GetText(ColoredText.GetText(equipment.GetName(), startingColor), 2);

			string design = "";
			string divinity = "";
			string quality = "";
			string enchantment = "";
			string material = "";
			string toReturn;

			if(equipment.divinity != null && equipment.divinity.currentLevel != 0)
			{
				divinity = (equipment.divinity.currentLevel > 0) ? "Blessed " : "Cursed ";	
			}		

			if(equipment.quality != null && equipment.quality.currentLevel != 0)
			{
				quality = qualityNames.Get(equipment.quality.currentLevel) + " ";
			}

			if(equipment.material != null) 
			{
				material = equipment.material.name + " ";
			}
			
			if(equipment.design != null)
			{
				design = equipment.design.name + " ";
			}

			if(equipment.enchantment != null)
			{	
				enchantment = equipment.enchantment.name + " ";
			}

			toReturn = SizedText.GetText(ColoredText.GetText((divinity + quality + material + design + enchantment), GetColor(equipment)), 2);

			return toReturn;
		}

		private Color GetColor(Equipment equipment)
		{
			if(equipment == null) return highQualityColor;
			else if(equipment.divinity != null && equipment.divinity.currentLevel != 0)
			{
				if(equipment.divinity.currentLevel > 0) return blessedColor;
				return cursedColor;
			}
			else if(equipment.enchantment != null && equipment.enchantment.currentLevel > 0)
			{
				return enchantedColor;
			}
			else if(equipment.quality != null)
			{
				if(equipment.quality.currentLevel < 0) return lowQualityColor;
				else return highQualityColor;
			}
			else return highQualityColor;
		}
	}
}