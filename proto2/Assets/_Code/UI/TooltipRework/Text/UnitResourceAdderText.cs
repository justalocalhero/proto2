﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class UnitResourceAdderText : MonoBehaviour 
	{
		public UnitResourceNameDictionary resourceNames;
		public StringReference positivePrefix, negativePrefix;
		public ColorReference color;

		public string GetText(UnitResourceAdder resource)
		{
			int value = resource.value;
			int absValue = Mathf.Abs(value);
			string valueString = (value > 0) ? positivePrefix + absValue + " ": negativePrefix + absValue + " ";
			string typeString = resourceNames.GetString(resource.type);
			string resourceString = (absValue > 1) ? "s " : " ";

			return ColoredText.GetText(valueString + typeString + resourceString, color);
		}
	}
}