﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class ProficiencyNameText : MonoBehaviour 
	{
		public ColorReference support, spell, physical, curse, tech;

		public string GetText(Ability ability)
		{
			if(ability == null || ability.action == null) return "";


			return GetText(ability.name, ability.action.proficiencyType);
		}
		
		public string GetText(TriggeredAction triggeredAction)
		{
			if(triggeredAction == null  || triggeredAction.action == null) return "";

			return GetText(triggeredAction.name, triggeredAction.action.proficiencyType);
		}

		private string GetText(string text, ProficiencyType type)
		{
			return SizedText.GetText(ColoredText.GetText(text, GetColor(type)), 2);
		}

		private Color GetColor(ProficiencyType type)
		{
			switch(type)
			{
				case ProficiencyType.blunt:
					return physical;
				case ProficiencyType.pierce:
					return physical;
				case ProficiencyType.tech:
					return tech;
				case ProficiencyType.curse:
					return curse;
				case ProficiencyType.spell:
					return spell;
				case ProficiencyType.support:
					return support;
				default:
					return new Color();

			}
		}
	}
}