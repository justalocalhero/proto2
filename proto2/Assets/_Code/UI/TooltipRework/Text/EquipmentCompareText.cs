﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class EquipmentCompareText : MonoBehaviour 
	{
		public EquipmentNameText equipmentNameText;
		public EquipSlotText equipSlotText;
		public EquipmentValueText equipmentValueText;
		public StatComparisonText statComparisonText;
		public AbilityText abilityText;
		public TriggeredActionText triggeredActionText;

		public string GetText(Equipment equipment, List<StatComparison> stats)
		{
			string nameString = equipmentNameText.GetText(equipment) + "\n";
			string slotString = equipSlotText.GetText(equipment.GetSlot()) + " ";
			string valueString = equipmentValueText.GetText(equipment.level) + "\n";
			string statString = statComparisonText.GetText(stats, "\n", "\n", "");
			string abilityString = abilityText.GetText(equipment.abilities, "\n", "\n", "");
			string triggerString = triggeredActionText.GetText(equipment.triggeredActions, "\n", "\n", "");

			return (nameString + slotString + valueString + statString + abilityString + triggerString);
		}
	}
}