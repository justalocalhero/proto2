﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class StatComparisonText : MonoBehaviour 
	{
		public StatText statText;
		public StatValueText statValueText;
		public StatNameDictionary statNameDictionary;
		public ColorReference negativeColor, basicColor, rolledColor, ghostColor, positiveComparison, negativeComparison, invisibleColor;

		public string GetText(List<StatComparison> statComparisons, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{
			if(statComparisons == null || statComparisons.Count == 0) return "";

			string body = "";

			foreach(StatComparison stat in statComparisons)
			{
				body += (GetText(stat) + lineDelimiter);
			}

			return (nonZeroPrefix + body + nonZeroSuffix);
		}

		public string GetText(StatComparison statComparison)
		{

			Color statColor = GetStatColor(statComparison.type);
			
			bool hasDif = statComparison.difValue != 0;
			bool isNegative = statComparison.difValue < 0;
			bool isDetrimental = statComparison.stat.StatType == StatType.Weight;

			Color comparisonColor = (isNegative ^ isDetrimental) ? negativeComparison : positiveComparison;

			string statString = statText.GetText(statComparison.stat);

			string comparisonValueString = (statValueText.GetText(statComparison.stat.ModifierType, statComparison.difValue));
			string comparisonModString = (hasDif) ? statNameDictionary.GetModName(statComparison.stat.ModifierType)  : "";
			
			string comparisonString = "  " + comparisonValueString + comparisonModString;
			
			return TwoColumnText.GetText(statString, statColor, comparisonString, comparisonColor, invisibleColor);

		}

		public Color GetStatColor(StatTooltipType type)
		{
			switch(type)
			{
				case StatTooltipType.negative:
					return negativeColor;
				case StatTooltipType.basic:
					return basicColor;
				case StatTooltipType.rolled:
					return rolledColor;
				case StatTooltipType.ghost:
					return ghostColor;
				default:
					return new Color();
			}
		}
	}
}