﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class TriggeredActionText : MonoBehaviour 
	{
		public ActionText actionText;
		public ProficiencyNameText abilityNameText;
		public TriggerText triggerText;
		public ProficiencyText proficiencyText;

		public string GetText(List<TriggeredAction> triggeres, string lineDelimiter, string nonEmptyPrefix, string nonEmptySuffix)
		{
			if(triggeres == null || triggeres.Count == 0) return "";
		
			string toReturn = "";
			
			foreach(TriggeredAction trigger in triggeres)
			{
				toReturn += (GetText(trigger) + lineDelimiter);
			}

			return (nonEmptyPrefix + toReturn + nonEmptySuffix);
		}

		public string GetText(TriggeredAction trigger, Unit unit)
		{
			if(trigger == null) return "";

			string nameString = abilityNameText.GetText(trigger);
			string proficiencyString = (trigger.action == null) ? "" :  "\n" + proficiencyText.GetText(trigger.action.proficiencyType);
			string triggerString = "\n" + triggerText.GetText(trigger.procTarget, trigger.procTrigger, trigger.procChance);
			string actionString = actionText.GetText(trigger.action, unit, "");
			
			return (nameString + proficiencyString + triggerString + actionString);
		}

		public string GetText(TriggeredAction trigger)
		{
			if(trigger == null) return "";

			string nameString = abilityNameText.GetText(trigger);
			string proficiencyString = (trigger.action == null) ? "" :  "\n" + proficiencyText.GetText(trigger.action.proficiencyType);
			string triggerString = "\n" + triggerText.GetText(trigger.procTarget, trigger.procTrigger, trigger.procChance);
			string actionString = actionText.GetText(trigger.action, "");
			
			return (nameString + proficiencyString + triggerString + actionString);
		}

		public string GetText(TriggeredAction trigger, AbilityAugment abilityAugment, string subName)
		{
			if(trigger == null || abilityAugment == null) return "";

			string nameString = abilityNameText.GetText(trigger) + GetSubName(subName);
			string proficiencyString = (trigger.action == null) ? "" :  "\n" + proficiencyText.GetText(trigger.action.proficiencyType);
			string triggerString = "\n" + triggerText.GetText(trigger.procTarget, trigger.procTrigger, trigger.procChance);
			string actionString = actionText.GetText(trigger.action, abilityAugment, "");
			
			return (nameString + proficiencyString + triggerString + actionString);
		}

		private string GetSubName(string subName)
		{
			return (subName == null || subName == "") ? "" : " (" + subName + ")";
		}
	}
}