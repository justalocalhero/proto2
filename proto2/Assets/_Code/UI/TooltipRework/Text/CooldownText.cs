﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class CooldownText : MonoBehaviour 
	{
		public ColorReference cooldownColor;
		public StringReference prefix, suffix;

		public string GetText(float duration, Unit unit)
		{
			if(unit == null) GetText(duration);

			float speedFactor = unit.GetSpeedFactor();
			float inverseFactor = (speedFactor > 0) ? 1 / speedFactor : 1;
			
			return GetText(inverseFactor * duration);
		}

		public string GetText(float duration)
		{
			return ColoredText.GetText((prefix + Utility.Truncate(duration, 2) + suffix), cooldownColor);
		}
	}
}