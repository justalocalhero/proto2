﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public static class ColoredText 
	{
		public static string GetText(string text, Color color)
		{
			return GetText("", text, "", color);
		}

		public static string GetText(string prefix, string text, string suffix, Color color)
		{
			string toReturn = (prefix + "<color=#" + ColorUtility.ToHtmlStringRGBA(color) + ">" + text + "</color>" + suffix);

			return toReturn;
		}
	}
}