﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class TwoColumnText : MonoBehaviour 
	{
		public static string GetText(string leftString, Color leftColor, string rightString, Color rightColor, Color invisibleColor)
		{
			return 
			(
				"<line-height=1%>" + ColoredText.GetText(leftString, leftColor) + "       " + Tooltips.ColoredText.GetText(rightString, invisibleColor) + "\n</line-height>"
				+ "<align=right>" +  "       " + ColoredText.GetText(rightString, rightColor) + "</align>"
			);
		}
	}
}