﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public static class SizedText 
	{
		public static string GetText(string text, int size)
		{
			return GetText("", text, "", size);
		}

		public static string GetText(string prefix, string text, string suffix, int size)
		{
			string sizeString = (size >= 0) ? "+" + size : "-" + Mathf.Abs(size);
			string toReturn = ("<size=" + sizeString + ">" + prefix + text + suffix + "</size>");

			return toReturn;
		}
	}
}