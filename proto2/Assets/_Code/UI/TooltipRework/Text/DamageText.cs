﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class DamageText : MonoBehaviour 
	{
		public DamageColorMap damageColorMap;
		
		public string GetText(List<Damage> damages, ProficiencyType proficiencyType, Unit unit, string linePrefix, string lineSuffix, string nonEmptyPrefix, string nonEmptySuffix)
		{
			if(damages == null || damages.Count == 0) return "";

			string toReturn = "";

            foreach(Damage damage in damages)
            {
                toReturn +=  (linePrefix + GetText(damage, proficiencyType, unit) + lineSuffix);
            }
			
			return nonEmptyPrefix + toReturn + nonEmptySuffix;
		}

		public string GetText(Damage[] damages, ProficiencyType proficiencyType, Unit unit, string linePrefix, string lineSuffix, string nonEmptyPrefix, string nonEmptySuffix)
		{
			if(damages == null || damages.Length == 0) return "";

			string toReturn = "";

            foreach(Damage damage in damages)
            {
                toReturn +=  (linePrefix + GetText(damage, proficiencyType, unit) + lineSuffix);
            }
			
			return nonEmptyPrefix + toReturn + nonEmptySuffix;

		}

		public string GetText(List<Damage> damages, string linePrefix, string lineSuffix, string nonEmptyPrefix, string nonEmptySuffix)
		{
			if(damages == null || damages.Count == 0) return "";

			string toReturn = "";

            foreach(Damage damage in damages)
            {
                toReturn +=  (linePrefix + GetText(damage) + lineSuffix);
            }
			return nonEmptyPrefix + toReturn + nonEmptySuffix;
		}

		public string GetText(Damage[] damages, string linePrefix, string lineSuffix, string nonEmptyPrefix, string nonEmptySuffix)
		{
			if(damages == null || damages.Length == 0) return "";

			string toReturn = "";

            foreach(Damage damage in damages)
            {
                toReturn +=  (linePrefix + GetText(damage) + lineSuffix);
            }

			return nonEmptyPrefix + toReturn + nonEmptySuffix;

		}

		public string GetText(Damage damage, ProficiencyType type, Unit unit)
		{
			Damage scaledDamage = unit.ScaleDamage(damage, type);

			string valueText = GetValueText(scaledDamage) + " ";
			string typeText = GetTypeText(scaledDamage);

			return (valueText + typeText);
		}
		
		public string GetText(Damage damage)
		{
			string valueText = GetValueText(damage) + " ";
			string typeText = GetTypeText(damage);
			string scaleText = GetScaleText(damage) + " ";

			return (valueText + scaleText + typeText);
		}
		
		private string GetTypeText(Damage damage)
		{
			return ColoredText.GetText("" + damage.type, damageColorMap.GetColor(damage.type));
		}

		private string GetValueText(Damage damage)
		{
			return Mathf.CeilToInt(damage.value).ToString();
		}

		private string GetScaleText(Damage damage)
		{
			float truncScale = Utility.Truncate(damage.damageFactor, 2);
			return (truncScale > 0) ? "+" + truncScale + "x" : "";
		}
	}
}