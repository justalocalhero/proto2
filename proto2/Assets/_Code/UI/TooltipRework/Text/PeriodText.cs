﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class PeriodText : MonoBehaviour 
	{
		public ColorReference periodColor;
		public StringReference prefix, suffix;

		public string GetText(float period)
		{
			return ColoredText.GetText((prefix + Utility.Truncate(period, 2) + suffix), periodColor);
		}		
	}
}
