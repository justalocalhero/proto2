﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class EquipmentValueText : MonoBehaviour 
	{
		public ColorReference color;
		public StringReference prefix;

		public string GetText(int value)
		{
			if(value == 0) return "";
			
			return ColoredText.GetText(prefix + value, color);
		}
	}
}