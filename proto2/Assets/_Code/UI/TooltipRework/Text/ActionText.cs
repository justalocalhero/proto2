﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class ActionText : MonoBehaviour 
	{
		public NameText nameText;
		public ActionConditionText actionConditionText;
		public DamageText damageText;
		public PurgeText purgeText;
		public DispelText dispelText;
		public ConditionalResourceText conditionalResourceText;
		public ConditionalStatText conditionalStatText;
		public AbilityAugmentText abilityAugmentText;
		public TimedEffectText timedEffectText;

		public string GetText(Action action, Unit unit, string name)
		{
			if(action == null) return "";

			string nameString = nameText.GetText(name, "", "");
			string actionConditionString = actionConditionText.GetText(action.actionConditions, "\n", "", "");
			string damageString =  damageText.GetText(action.damages, action.proficiencyType, unit, "\n", "", "\n", "");
			string selfDamageString = damageText.GetText(action.selfDamages, action.proficiencyType, unit, "\n", " to self", "\n", "");
			string unitResourceString = conditionalResourceText.GetText(action.conditionalResources, "\n", "\n", "");
			string dispelString = (action.purgeCount + action.cleanseCount > 0) ? "\n" : "";
			string purgeString = purgeText.GetText(action.purgeCount, "\n", "");
			string cleanseString = dispelText.GetText(action.cleanseCount, "\n", "");
			string instanceStatString = conditionalStatText.GetText(action.instanceStats, "\n", "\n", "");
			string buffString = timedEffectText.GetBuffText(action.buffs, action.proficiencyType, unit, "\n", "\n", "");
			string debuffString = timedEffectText.GetDebuffText(action.debuffs, action.proficiencyType, unit, "\n", "\n", "");
			string abilityAugmentString = abilityAugmentText.GetText(action.selectedAugment, action.proficiencyType, unit);

			return (
				nameString
				+ actionConditionString 
				+ damageString 
				+ selfDamageString 
				+ unitResourceString 
				+ dispelString 
				+ purgeString 
				+ cleanseString 
				+ instanceStatString 
				+ buffString 
				+ debuffString 
				+ abilityAugmentString
			);
		}

		public string GetText(Action action, string name)
		{
			if(action == null) return "";

			string nameString = nameText.GetText(name, "", "");
			string actionConditionString = actionConditionText.GetText(action.actionConditions, "\n", "", "");
			string damageString =  damageText.GetText(action.damages, "\n", "", "\n", "");
			string selfDamageString = damageText.GetText(action.selfDamages, "\n", " to self", "\n", "");
			string unitResourceString = conditionalResourceText.GetText(action.conditionalResources, "\n", "\n", "");
			string dispelString = (action.purgeCount + action.cleanseCount > 0) ? "\n" : "";
			string purgeString = purgeText.GetText(action.purgeCount, "\n", "");
			string cleanseString = dispelText.GetText(action.cleanseCount, "\n", "");
			string instanceStatString = conditionalStatText.GetText(action.instanceStats, "\n", "\n", "");
			string buffString = timedEffectText.GetBuffText(action.buffs, "\n", "\n", "");
			string debuffString = timedEffectText.GetDebuffText(action.debuffs, "\n", "\n", "");
			string abilityAugmentString = abilityAugmentText.GetText(action.selectedAugment);

			return (
				nameString
				+ actionConditionString 
				+ damageString 
				+ selfDamageString 
				+ unitResourceString 
				+ dispelString 
				+ purgeString 
				+ cleanseString 
				+ instanceStatString 
				+ buffString 
				+ debuffString 
				+ abilityAugmentString
			);
		}

		public string GetText(Action action, AbilityAugment abilityAugment, string name)
		{
			if(action == null || abilityAugment == null) return "";

			string nameString = nameText.GetText(name, "", "");
			string actionConditionString = actionConditionText.GetText(action.actionConditions, "\n", "", "");
			string damageString =  damageText.GetText(action.damages, "\n", "", "\n", "");
			string selfDamageString = damageText.GetText(action.selfDamages, "\n", " to self", "\n", "");
			string unitResourceString = conditionalResourceText.GetText(action.conditionalResources, "\n", "\n", "");
			string dispelString = (action.purgeCount + action.cleanseCount > 0) ? "\n" : "";
			string purgeString = purgeText.GetText(action.purgeCount, "\n", "");
			string cleanseString = dispelText.GetText(action.cleanseCount, "\n", "");
			string instanceStatString = conditionalStatText.GetText(action.instanceStats, "\n", "\n", "");
			string buffString = timedEffectText.GetBuffText(action.buffs, "\n", "\n", "");
			string debuffString = timedEffectText.GetDebuffText(action.debuffs, "\n", "\n", "");
			string abilityAugmentString = abilityAugmentText.GetText(abilityAugment);
			
			return (
				nameString
				+ actionConditionString 
				+ damageString 
				+ selfDamageString 
				+ unitResourceString 
				+ dispelString 
				+ purgeString 
				+ cleanseString 
				+ instanceStatString 
				+ buffString 
				+ debuffString 
				+ abilityAugmentString
			);

		}
	}
}