﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class UnitResourceAugmentText : MonoBehaviour 
	{
		public ColorReference capColor;
		public EffectStatText effectStatText;

		public string GetText(List<UnitResourceAugment> augments, string lineDelimiter, string nonEmptyPrefix, string nonEmptySuffix)
		{
			if(augments == null || augments.Count == 0) return "";

			string toReturn = "";

			foreach(UnitResourceAugment augment in augments)
			{
				toReturn += (lineDelimiter + GetText(augment));
			}

			return (nonEmptyPrefix + toReturn + nonEmptySuffix);
		}

		public string GetText(UnitResourceAugment augment)
		{
			string capString = ColoredText.GetText(GetCapString(augment), capColor);
			string statString = GetStatString(augment);

			return (capString + statString);
		}

		public string GetCapString(UnitResourceAugment augment)
		{
			string toReturn = "";

			if(augment.cap == 0) 
			{
				return toReturn;
			}

			else if(augment.cap > 0)
			{
				toReturn += "Unit may hold " + augment.cap + " aditional " + augment.type;
				toReturn += (augment.cap > 1) ? " charges" : " charge";
				toReturn += "\n";
			}
			else 
			{
				toReturn += "Unit may hold " + augment.cap + " fewer " + augment.type;
				toReturn += (augment.cap < -1) ? " charges" : " charge";
				toReturn += "\n";
			}
			
			return toReturn;
		}

		public string GetStatString(UnitResourceAugment augment)
		{
			return effectStatText.GetText(augment.stats,"\n", augment.type + " charges grant an additional\n", "\n");

		}
	}
}