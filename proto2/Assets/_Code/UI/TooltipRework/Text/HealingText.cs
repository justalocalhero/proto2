﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class HealingText : MonoBehaviour
	{
		public ColorReference color;
		public StringReference prefix, suffix;

		public string GetText(float amount)
		{
			if(amount == 0) return "";

			return ColoredText.GetText(prefix + amount + suffix, color);
		}
	}
}