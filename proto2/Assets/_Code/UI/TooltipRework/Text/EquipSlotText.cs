﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class EquipSlotText : MonoBehaviour
	{
		public ColorReference color;

		public string GetText(EquipSlot slot)
		{
			return ColoredText.GetText(slot + "", color);
		}
	}
}