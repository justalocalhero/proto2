﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class TalentText : MonoBehaviour 
	{
		public AbilityText abilityText;
		public TriggeredActionText triggeredActionText;
		public NameText nameText;
		public UnitResourceAugmentText unitResourceAugmentText;
		public EffectStatText effectStatText;

		public string GetText(Talent talent)
		{
			if(talent == null) return "";

			TalentOption tempOption = talent.GetSelectedOption();
			if(tempOption != null) return GetText(talent, tempOption);

			bool hasAbility = talent.HasAbility();
			bool hasStatic = talent.HasStatic();

			string nameString = (hasAbility && !hasStatic) ? "" : nameText.GetText(SizedText.GetText(talent.name, 2));
			string statString = effectStatText.GetText(talent.permanentStats, "\n", "\n", "");
			string resourceString = unitResourceAugmentText.GetText(talent.resourceAugments, "\n", "\n", "");
			string abilityString = "";
			string abilitySpacing = (hasAbility && hasStatic) ? "\n\n" : "";

			if(talent.timedAbility != null) abilityString = abilityText.GetText(talent.timedAbility);
			if(talent.triggeredAbility != null) abilityString = triggeredActionText.GetText(talent.triggeredAbility);

			return (nameString + statString + resourceString + abilitySpacing + abilityString);
		}

		public string GetText(TalentOption talentOption)
		{
			if(talentOption == null) return "";

			Talent tempTalent = talentOption.GetParent();
			if(tempTalent != null) return GetText(tempTalent, talentOption);

			return "";
		}

		public string GetText(Talent talent, TalentOption talentOption)
		{
			if(talent == null ||  talentOption == null) return "";
			

			bool hasAbility = talent.HasAbility();
			bool hasStatic = talent.HasStatic() || talentOption.HasStatic();

			string nameString = (hasAbility && !hasStatic) ? "" : nameText.GetText(SizedText.GetText(talent.name + "(" + talentOption.name + ")", 2));
			string statString = effectStatText.GetText(Utility.ExtendArrayByArray(talent.permanentStats, talentOption.permanentStats), "\n", "\n", "");
			string optionName = (hasAbility && !hasStatic) ? talentOption.name : "";
			string abilitySpacing = (hasAbility && hasStatic) ? "\n\n" : "";
			
			List<UnitResourceAugment> tempList = Utility.CloneList(talent.resourceAugments);
			tempList.AddRange(Utility.CloneList(talentOption.resourceAugments));

			string resourceString = unitResourceAugmentText.GetText(tempList, "\n", "\n", "");
			string abilityString = "";

			if(talent.timedAbility != null) 
			{
				if(talentOption.augment != null) abilityString = abilityText.GetText(talent.timedAbility, talentOption.augment, optionName);
				else abilityString = abilityText.GetText(talent.timedAbility);
			}
			if(talent.triggeredAbility != null) 
			{
				if(talentOption.augment != null) abilityString = triggeredActionText.GetText(talent.triggeredAbility, talentOption.augment, optionName);
				else abilityString = triggeredActionText.GetText(talent.triggeredAbility);
			}

			return (nameString + statString + resourceString + abilitySpacing + abilityString);
		}
	}
}