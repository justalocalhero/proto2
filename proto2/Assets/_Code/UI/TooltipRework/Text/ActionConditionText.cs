﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class ActionConditionText : MonoBehaviour 
	{
		public ActionConditionNameDictionary conditionNames;
		public ColorReference color;

		public string GetText(ActionCondition[] actionConditions, string lineDelimiter, string nonEmptyPrefix, string nonEmptySuffix)
		{
			if(actionConditions == null || actionConditions.Length == 0) return "";

			string toReturn = "";

            foreach(ActionCondition condition in actionConditions)
            {
                toReturn += GetText(condition, lineDelimiter, "");
            }

			return nonEmptyPrefix + toReturn + nonEmptySuffix;
		}
		
		public string GetText(ActionCondition condition, string prefix, string suffix)
		{
			if(condition.type == ConditionType.always) return "";
			string conditionString = conditionNames.GetConditionString(condition.value, condition.target, condition.type, condition.comparison);

			return ColoredText.GetText(prefix + conditionString + suffix, color);
		}
	}
}