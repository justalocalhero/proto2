﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class EffectTypeText : MonoBehaviour 
	{
		public ColorReference color;

		public string GetText(EffectType type)
		{
			string toReturn = "";

			switch(type)
			{
				case EffectType.buff:
					toReturn = "Buff";
					break;
				case EffectType.debuff:
					toReturn = "Debuff";
					break;					
			}

			return ColoredText.GetText(toReturn, color);
		}
	}	
		
	public enum EffectType {buff, debuff};
}