﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class SelectedText : MonoBehaviour 
	{
		public ColorReference selectedColor;
		public StringReference selectedString;

		public string GetText()
		{
			return ColoredText.GetText("\n" + selectedString, selectedColor);
		}
	}
}