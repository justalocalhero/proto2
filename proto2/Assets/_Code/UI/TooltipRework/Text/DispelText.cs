﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class DispelText : MonoBehaviour 
	{
		public ColorReference dispelColor;
		public StringReference prefix, singularSuffix, pluralSuffix;

		public string GetText(int count, string nonZeroPrefix, string  nonZeroSuffix)
		{
			if(count == 0) return "";
			string suffix = (count == 1) ? singularSuffix : pluralSuffix;

			return ColoredText.GetText((nonZeroPrefix + prefix + count + suffix + nonZeroSuffix), dispelColor);
		}
	}
}