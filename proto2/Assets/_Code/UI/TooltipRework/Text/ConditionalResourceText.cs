﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class ConditionalResourceText : MonoBehaviour 
	{
		public UnitResourceAdderText unitResourceText;
		public ActionConditionText actionConditionText;
		public StringReference conditionPrefix;

		public string GetText(ConditionalResource[] conditionalResources, string lineDelimiter, string nonZeroPrefix, string nonZeroSuffix)
		{
			if(conditionalResources == null || conditionalResources.Length == 0) return "";

			string toReturn = "";

			foreach(ConditionalResource resource in conditionalResources)
			{
				toReturn += (lineDelimiter + GetText(resource));
			}

			return (nonZeroPrefix + toReturn + nonZeroSuffix);
		}

		public string GetText(ConditionalResource conditionalResource)
		{
			string conditionString = actionConditionText.GetText(conditionalResource.actionCondition, conditionPrefix, "\n");
			string unitResourceString = unitResourceText.GetText(conditionalResource.resource);

			return (conditionString + unitResourceString);
		}
	}
}