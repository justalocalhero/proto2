﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class UnitResourceText : MonoBehaviour 
	{
		public UnitResourceNameDictionary resourceNames;
		public NameText nameText;
		public EffectStatText effectStatText;

		public string GetText(UnitResource unitResource)
		{
			if(unitResource == null) return "";
			
			List<EquipStat> currentStats = Utility.MergeStats(unitResource.GetStats());

			string nameString = SizedText.GetText(nameText.GetText(resourceNames.GetString(unitResource.GetResourceType())), 2);
			string statString = effectStatText.GetText(currentStats, "\n", "\n", ""); 

			return (nameString + statString);
		}
	}
}