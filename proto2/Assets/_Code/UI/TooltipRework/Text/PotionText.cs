﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class PotionText : MonoBehaviour 
	{
		public PotionNameText potionNameText;
		public PotionSupplyText potionSupplyText;
		public HealingText healingText;

		public string GetText(Potion potion)
		{
			if(potion == null) return "";

			string nameString = potionNameText.GetText(potion) + "\n";
			string healingString = healingText.GetText(potion.healingAmount) + "\n";
			string supplyString = potionSupplyText.GetText(potion.potionSupplies, "\n", "\n", "");

			return (nameString + healingString + supplyString);

		}
	}
}