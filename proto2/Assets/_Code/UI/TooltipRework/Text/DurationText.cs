﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class DurationText : MonoBehaviour 
	{
		public ColorReference durationColor;
		public StringReference prefix, suffix;

		public string GetText(float period, Unit unit, EffectType type)
		{
			return GetText(period * GetFactor(unit, type));
		}

		public string GetText(float duration)
		{
			return ColoredText.GetText((prefix + Utility.Truncate(duration, 2) + suffix), durationColor);
		}

		private float GetFactor(Unit unit, EffectType type)
		{
			float factor;
			switch(type)
			{
				case EffectType.buff:
					factor = unit.GetResilienceFactor();
					break;
				case EffectType.debuff:
					factor = unit.GetAfflictionFactor();
					break;
				default:
					factor = 1;
					break;
			}

			return factor;
		}
	}
}