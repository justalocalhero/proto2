﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class TriggerText : MonoBehaviour 
	{
		public ColorReference color;
		public ProcTriggerNameDictionary triggerNames;
		public StringReference chanceSuffix;

		public string GetText(ProcTarget target, ProcTrigger trigger, float chance)
		{
			string chanceString = (chance < 1) ?  Utility.ToPercent(chance) + chanceSuffix : "";;
			string triggerString = triggerNames.GetString(target, trigger);

			return ColoredText.GetText((chanceString + triggerString), color);
		}
	}
}