﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class ProficiencyText : MonoBehaviour 
	{
		public ColorReference proficiencyColor;

		public string GetText(ProficiencyType type)
		{
			return ColoredText.GetText("" + type, proficiencyColor);
		}
	}
}