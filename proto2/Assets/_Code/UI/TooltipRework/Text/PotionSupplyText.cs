﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class PotionSupplyText : MonoBehaviour 
	{

		public string GetText(List<PotionSupply> supplies, string lineDelimmiter, string nonZeroPrefix, string nonZeroSuffix)
		{
			if(supplies == null || supplies.Count == 0) return "";

			string toReturn = "";

			foreach(PotionSupply supply in supplies)
			{
				toReturn += (GetText(supply) + lineDelimmiter);
			}

			return (nonZeroPrefix + toReturn + nonZeroSuffix);
		}

		public string GetText(PotionSupply supply)
		{
			if(supply == null) return "";

			return ColoredText.GetText(supply.description, supply.descriptionColor);
		}
	}
}