﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class AbilityText : MonoBehaviour 
	{
		public ActionText actionText;
		public ProficiencyNameText abilityNameText;
		public CooldownText cooldownText;
		public ProficiencyText proficiencyText;

		public string GetText(List<Ability> abilities, string lineDelimiter, string nonEmptyPrefix, string nonEmptySuffix)
		{
			if(abilities == null || abilities.Count == 0) return "";
		
			string toReturn = "";
			
			foreach(Ability ability in abilities)
			{
				toReturn += (GetText(ability) + lineDelimiter);
			}

			return (nonEmptyPrefix + toReturn + nonEmptySuffix);
		}

		public string GetText(Ability ability, Unit unit)
		{
			if(ability == null) return "";

			string nameString = abilityNameText.GetText(ability);
			string proficiencyString = (ability.action == null) ? "" :  "\n" + proficiencyText.GetText(ability.action.proficiencyType);
			string cooldownString = "\n" + cooldownText.GetText(ability.duration, unit);
			string actionString = actionText.GetText(ability.action, unit, "");
			
			return (nameString + proficiencyString + cooldownString + actionString);
		}

		public string GetText(Ability ability)
		{
			if(ability == null) return "";

			string nameString = abilityNameText.GetText(ability);
			string proficiencyString = (ability.action == null) ? "" :  "\n" + proficiencyText.GetText(ability.action.proficiencyType);
			string cooldownString = "\n" + cooldownText.GetText(ability.duration);
			string actionString = actionText.GetText(ability.action, "");
			
			return (nameString + proficiencyString + cooldownString + actionString);
		}
		
		public string GetText(Ability ability, AbilityAugment abilityAugment, string subName)
		{
			if(ability == null || abilityAugment == null) return "";

			string nameString = abilityNameText.GetText(ability) + GetSubName(subName);
			string proficiencyString = (ability.action == null) ? "" :  "\n" + proficiencyText.GetText(ability.action.proficiencyType);
			string cooldownString = "\n" + cooldownText.GetText(ability.duration);
			string actionString = actionText.GetText(ability.action, abilityAugment, "");
			
			return (nameString + proficiencyString + cooldownString + actionString);
		}

		private string GetSubName(string subName)
		{
			return (subName == null || subName == "") ? "" : " (" + subName + ")";
		}
	}
}