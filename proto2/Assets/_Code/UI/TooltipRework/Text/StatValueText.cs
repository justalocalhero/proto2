﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class StatValueText : MonoBehaviour 
	{	
		public string GetText(ModifierType type, float scaledValue)
		{
			string toReturn = "";
			float value = scaledValue;
			switch(type)
			{
				case ModifierType.Flat:
					value = Mathf.CeilToInt(value);
					break;
				case ModifierType.GlobalMultiplier:
					value = 100 * Utility.Truncate(value, 2);
					break;
				case ModifierType.Multiplier:
					value = 100 * Utility.Truncate(value, 2);
					break;
			}
			if(value != 0) toReturn += value;
			if(value > 0) toReturn = "+" + toReturn;

			return toReturn;
		}
	}
}