﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class BoonText : MonoBehaviour 
	{
		public NameText nameText;
		public AbilityText abilityText;
		public EffectStatText effectStatText;
	
		public string GetText(Boon boon)
		{
			string nameString = (boon.ability != null) ? "" : nameText.GetText(boon.GetName());
			string statString = effectStatText.GetText(boon.stats, "\n", "\n", "");
			string abilityString = (boon.ability == null) ? "" : abilityText.GetText(boon.ability);

			return (nameString + statString + abilityString);
		}
	}
}