﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class RectBounds : MonoBehaviour 
	{
		public RectTransform boundary;
		private RectTransform rect;

		public void Awake()
		{
			rect = transform as RectTransform;
		}

		public void Fit()
		{
			Fit(boundary);
		}

		public void Fit(RectTransform boundary)
		{			
			Vector3[] boundaryCorners = new Vector3[4];
			Vector3[] corners = new Vector3[4];

			boundary.GetWorldCorners(boundaryCorners);
			rect.GetWorldCorners(corners);

			float leftDif = boundaryCorners[0].x - corners[0].x;
			float rightDif = boundaryCorners[2].x - corners[2].x;
			float topDif = boundaryCorners[1].y - corners[1].y;
			float bottomDif = boundaryCorners[0].y - corners[0].y;

			float moveXLeft = (leftDif > 0) ? leftDif : 0;
			float moveXRight = (rightDif < 0) ? rightDif : 0;
			float moveYTop = (topDif < 0) ? topDif : 0;
			float moveYBottom = (bottomDif > 0) ? bottomDif : 0;

			bool xFail = moveXLeft != 0 && moveXRight != 0;
			bool yFail = moveYTop != 0 && moveYBottom != 0;

			if(xFail || yFail)
			{
				if(xFail) Debug.LogWarning("X Can't Fit");
				if(yFail) Debug.LogWarning("Y Can't Fit");
				return;
			}

			float moveX = moveXLeft + moveXRight;
			float moveY = moveYTop + moveYBottom;

			rect.position = new Vector2(rect.position.x + moveX, rect.position.y + moveY);

		}
	}
}