﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class LabelButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
	public Button button;
	public TextMeshProUGUI text;
	public UIToggler toggler;

	public delegate void OnClick();
	public OnClick onClick;

	public void RaiseOnClick()
	{
		if(onClick != null) onClick();
	}

	public delegate void OnEnter();
	public OnEnter onEnter;

	public void RaiseOnEnter()
	{
		if(onEnter != null) onEnter();
	}

	public delegate void OnExit();
	public OnExit onExit;

	public void RaiseOnExit()
	{
		if(onExit != null) onExit();
	}


	public void SetText(string toSet)
	{
		text.SetText(toSet);
	}

	public void Show()
	{
		toggler.Show();
	}

	public void Hide()
	{
		toggler.Hide();
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
        RaiseOnEnter();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        RaiseOnExit();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
		RaiseOnClick();
    }
}
