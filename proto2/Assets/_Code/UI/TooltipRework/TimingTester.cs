﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class TimingTester : MonoBehaviour
{
	public Button button;
	public TextMeshProUGUI textMesh;
	public Transform testTarget;
	private RectTransform testRect;
	public NextFrameStrategy nextFrameStrategy;
	public NullStrategy nullStrategy;
	public ZeroStrategy zeroStrategy;
	public UpdateStrategy updateStrategy;
	public TimedStrategy timedStrategy;

	public void Start()
	{
		nextFrameStrategy.Init(this);
		nullStrategy.Init(this);
		zeroStrategy.Init(this);
		updateStrategy.Init(this);
		timedStrategy.Init(this);
	}

	public void Fire(TimingStrategy strategy)
	{
		if(strategy.routine != null) StopCoroutine(strategy.routine);
		strategy.routine = strategy.Fire();
		StartCoroutine(strategy.routine);
	}
}

[System.Serializable]
public class TimingStrategy
{
	private TimingTester parent;
	protected TimedValue initial, instant, final;
	protected float asymptoticStat, worstStat;
	public TextMeshProUGUI mesh, statMesh;
	public EventTrigger trigger;
	protected string name;
	public IEnumerator routine;

	public void Init(TimingTester parent)
	{
		SetName();
		this.parent = parent;
		if(trigger == null)
		{
			Debug.Log("Null Trigger");
			return;
		}

		EventTrigger.Entry entry = new EventTrigger.Entry();
		entry.eventID = EventTriggerType.PointerEnter;
		entry.callback = new EventTrigger.TriggerEvent();
		entry.callback.AddListener(new UnityEngine.Events.UnityAction<BaseEventData>(OnPointerEnter));
		trigger.triggers.Add(entry);
	}

	public void AlterValue()
	{
		mesh.SetText(mesh.text + "asdf");
	}

	public TimedValue GetTestValue()
	{
		return new TimedValue {value = mesh.GetPreferredValues().x, time = Time.time};
	}

	public void OnPointerEnter(BaseEventData eventData)
	{

		parent.Fire(this);
	}

	public void PrintSummary() 
	{
		string nameString = "Name: " + name;
		string initialString = "\nInitial: " + initial.value;
		string instantString = "\nInstant: " + instant.value + " in: " + (instant.time - initial.time);
		string finalString  = "\nFinal: " + final.value + " in: " + (final.time - initial.time);

		Debug.Log(nameString + initialString + instantString + finalString);
	}

	public void UpdateStats(float timing)
	{
		if(asymptoticStat <= 0) asymptoticStat = timing;
		else asymptoticStat = asymptoticStat * .9f + .1f * timing;
		if(timing > worstStat) worstStat = timing;

		statMesh.SetText(name + "\navg: " + asymptoticStat + "\nworst: " + worstStat);
	}

	public virtual IEnumerator Fire() 
	{		
		yield return null;
	}
	public virtual void SetName() {}

}

[System.Serializable]
public class NextFrameStrategy : TimingStrategy
{
	public override void SetName()
	{
		name = "NextFrame";
	}

	public override IEnumerator Fire()
	{
		initial = GetTestValue();
		AlterValue();
		instant = GetTestValue();
		yield return new WaitForEndOfFrame();
		final = GetTestValue();

		UpdateStats(final.time - initial.time);
		//PrintSummary();
	}
}

[System.Serializable]
public class NullStrategy : TimingStrategy
{
	public override void SetName()
	{
		name = "Null";
	}

	public override IEnumerator Fire()
	{
		initial = GetTestValue();
		AlterValue();
		instant = GetTestValue();
		yield return null;
		final = GetTestValue();

		UpdateStats(final.time - initial.time);
		//PrintSummary();
	}
}

[System.Serializable]
public class ZeroStrategy : TimingStrategy
{
	public override void SetName()
	{
		name = "Zero";
	}

	public override IEnumerator Fire()
	{
		initial = GetTestValue();
		AlterValue();
		instant = GetTestValue();
		yield return 0;
		final = GetTestValue();

		UpdateStats(final.time - initial.time);
		//PrintSummary();
	}
}

[System.Serializable]
public class UpdateStrategy : TimingStrategy
{
	public override void SetName()
	{
		name = "Update";
	}

	public override IEnumerator Fire()
	{
		initial = GetTestValue();
		AlterValue();
		instant = GetTestValue();
		yield return new WaitForFixedUpdate();
		final = GetTestValue();

		UpdateStats(final.time - initial.time);
		//PrintSummary();
	}
}

[System.Serializable]
public class TimedStrategy : TimingStrategy
{
	public override void SetName()
	{
		name = "Timed";
	}

	public override IEnumerator Fire()
	{
		initial = GetTestValue();
		AlterValue();
		instant = GetTestValue();
		yield return new WaitForSeconds(.1f);
		final = GetTestValue();

		UpdateStats(final.time - initial.time);
		//PrintSummary();
	}
}

public struct TimedValue
{
	public float value, time;
}
