﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class ActionTooltip : MonoBehaviour 
	{

		public FloatReference fadeTime;

		[Space(20)]
		public Tooltip tooltip;
		public ActionText actionText;

		public void Set(Action action, string name, Unit unit, RectTransform rect)
		{
			if(action == null || unit == null || rect == null) return;

			Set(actionText.GetText(action, unit, name), rect);
		}

		public void Set(Action action, string name, RectTransform rect)
		{
			if(action == null || rect == null) return;

			Set(actionText.GetText(action, name), rect);

		}

		private void Set(string text, RectTransform rect)
		{
			if(rect == null) return;


			tooltip.Reset()
			.WithBody(text)
			.WithFadeTime(fadeTime)
			.WithTarget(rect)
			.WithTargetCorner(Corner.BottomRight)
			.WithExpansionCorner(Corner.TopRight)
			.Set()
			.Start();
		}

		public void Hide()
		{
			tooltip.ForceHide();
		}
	}
}