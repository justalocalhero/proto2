﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class TriggeredActionTooltip : MonoBehaviour 
	{

		public FloatReference fadeTime;

		[Space(20)]
		public Tooltip tooltip;
		public TriggeredActionText triggeredActionText;

		public void Set(TriggeredAction triggeredAction, Unit unit, RectTransform rect)
		{
			if(triggeredAction == null || unit == null || rect == null) return;

			Set(triggeredActionText.GetText(triggeredAction, unit), rect);
		}

		public void Set(TriggeredAction triggeredAction, RectTransform rect)
		{
			if(triggeredAction == null || rect == null) return;

			Set(triggeredActionText.GetText(triggeredAction), rect);

		}
		public void Set(string  text, RectTransform rect)
		{
			if(rect == null) return;

			tooltip.Reset()
			.WithBody(text)
			.WithFadeTime(fadeTime)
			.WithTarget(rect)
			.WithTargetCorner(Corner.TopLeft)
			.WithExpansionCorner(Corner.TopRight)
			.Set()
			.Start();
		}

		public void Hide()
		{
			tooltip.ForceHide();
		}
	}
}