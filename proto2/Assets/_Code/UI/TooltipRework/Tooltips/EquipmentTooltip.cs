﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class EquipmentTooltip : MonoBehaviour 
	{
		public FloatReference fadeTime;

		[Space(20)]
		public Tooltip tooltip;
		public EquipmentText equipmentText;

		public void Set(Equipment equipment, RectTransform rect)
		{
			if(equipment == null || rect == null) return;

			string text = equipmentText.GetText(equipment);

			tooltip.Reset()
			.WithBody(text)
			.WithFadeTime(fadeTime)
			.WithTarget(rect)
			.WithTargetCorner(Corner.TopLeft)
			.WithExpansionCorner(Corner.BottomLeft)
			.Set()
			.Start();
		}

		public void Hide()
		{
			tooltip.ForceHide();
		}
	}
}