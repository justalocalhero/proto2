﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class AbilityTooltip : MonoBehaviour 
	{
		public FloatReference fadeTime;

		[Space(20)]
		public Tooltip tooltip;
		public AbilityText abilityText;

		public void Set(Ability ability, Unit unit, RectTransform rect)
		{
			if(ability == null || unit == null || rect == null) return;

			Set(abilityText.GetText(ability, unit), rect);
		}

		public void Set(Ability ability, RectTransform rect)
		{
			if(ability == null || rect == null) return;

			Set(abilityText.GetText(ability), rect);

		}

		private void Set(string text, RectTransform rect)
		{
			if(rect == null) return;


			tooltip.Reset()
			.WithBody(text)
			.WithFadeTime(fadeTime)
			.WithTarget(rect)
			.WithTargetCorner(Corner.TopLeft)
			.WithExpansionCorner(Corner.TopRight)
			.Set()
			.Start();
		}

		public void Hide()
		{
			tooltip.ForceHide();
		}
	}
}