﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class PotionTooltip : MonoBehaviour 
	{
		public Tooltip tooltip;
		public PotionText potionText;
		public Corner targetCorner, expansionCorner;

		public void Set(Potion potion, RectTransform rect)
		{
			if(potion == null || rect == null) return;

			string text = potionText.GetText(potion);

			tooltip.Reset()
			.WithBody(text)
			.WithTarget(rect)
			.WithTargetCorner(targetCorner)
			.WithExpansionCorner(expansionCorner)
			.Set()
			.Start();
		}

		public void Hide()
		{
			tooltip.ForceHide();
		}
		
	}
}