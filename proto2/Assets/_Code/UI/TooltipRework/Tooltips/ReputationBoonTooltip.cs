﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class ReputationBoonTooltip : MonoBehaviour 
	{

		public FloatReference fadeTime;

		[Space(20)]
		public Tooltip tooltip;
		public BoonText boonText;

		public void Set(Boon boon, RectTransform rect)
		{
			if(boon == null || rect == null) return;

			string text = boonText.GetText(boon);

			tooltip.Reset()
			.WithBody(text)
			.WithFadeTime(fadeTime)
			.WithTarget(rect)
			.WithTargetCorner(.5f, 0f)
			.WithExpansionCorner(.5f, 1f)
			.Set()
			.Start();
		}

		public void Hide()
		{
			tooltip.ForceHide();
		}
	}
}