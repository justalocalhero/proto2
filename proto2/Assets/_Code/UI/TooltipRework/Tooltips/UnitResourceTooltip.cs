﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class UnitResourceTooltip : MonoBehaviour 
	{
		public Tooltip tooltip;
		public UnitResourceText unitResourceText;
		public Corner targetCorner, expansionCorner;

		public void Set(UnitResource unitResource, RectTransform rect)
		{
			if(unitResource == null || rect == null) return;

			string text = unitResourceText.GetText(unitResource);

			tooltip.Reset()
			.WithBody(text)
			.WithTarget(rect)
			.WithTargetCorner(targetCorner)
			.WithExpansionCorner(expansionCorner)
			.Set()
			.Start();
		}

		public void Hide()
		{
			tooltip.ForceHide();
		}
		
	}
}