﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Loot;

namespace Tooltips
{
	public class ComponentProfileTooltip : MonoBehaviour 
	{
		public FloatReference fadeTime;

		[Space(20)]
		public Tooltip tooltip;
		public ComponentProfileText componentProfileText;
		public Corner targetCorner;
		public Corner expansionCorner;
		
		public void SetCraft(List<ComponentProfile> profiles, EquipSlot slot, ResourceManager manager, RectTransform rect)
		{
			if(profiles == null || profiles.Count == 0 || rect == null) return;

			Set(componentProfileText.GetCraftText(profiles, slot, manager), rect);
		}

		public void SetUpgrade(List<ComponentProfile> profiles, EquipSlot slot, ResourceManager manager, RectTransform rect)
		{
			if(profiles == null || profiles.Count == 0  || rect == null) return;

			Set(componentProfileText.GetUpgradeText(profiles, slot, manager), rect);
		}

		public void SetCraft(List<ComponentProfile> profiles, List<ComponentProfile> replaced, EquipSlot slot, ResourceManager manager, RectTransform rect)
		{
			if(profiles == null || profiles.Count == 0 || rect == null || replaced == null) return;

			Set(componentProfileText.GetCraftText(profiles, replaced, slot, manager), rect);
		}

		public void SetUpgrade(List<ComponentProfile> profiles, List<ComponentProfile> replaced, EquipSlot slot, ResourceManager manager, RectTransform rect)
		{
			if(profiles == null || profiles.Count == 0  || rect == null || replaced == null) return;

			Set(componentProfileText.GetUpgradeText(profiles, replaced, slot, manager), rect);
		}


		public void SetCraft(ComponentProfile profile, EquipSlot slot, ResourceManager manager, RectTransform rect)
		{
			if(profile == null || rect == null) return;

			Set(componentProfileText.GetCraftText(profile, slot, manager), rect);
		}

		public void SetUpgrade(ComponentProfile profile, EquipSlot slot, ResourceManager manager, RectTransform rect)
		{
			if(profile == null || rect == null) return;

			Set(componentProfileText.GetUpgradeText(profile, slot, manager), rect);
		}

		public void SetCraft(ComponentProfile profile, ComponentProfile replaced, EquipSlot slot, ResourceManager manager, RectTransform rect)
		{
			if(profile == null || replaced == null || rect == null) return;

			Set(componentProfileText.GetCraftText(profile, replaced, slot, manager), rect);
		}

		public void SetUpgrade(ComponentProfile profile, ComponentProfile replaced, EquipSlot slot, ResourceManager manager, RectTransform rect)
		{
			if(profile == null || replaced == null || rect == null) return;

			Set(componentProfileText.GetUpgradeText(profile, replaced, slot, manager), rect);
		}
		
		public void SetStats(List<ComponentProfile> profiles, EquipSlot slot, RectTransform rect)
		{
			if(profiles == null || profiles.Count == 0 || rect == null) return;

			Set(componentProfileText.GetStatsText(profiles, slot), rect);
		}

		public void SetStats(List<ComponentProfile> profiles, List<ComponentProfile> replaced, EquipSlot slot, RectTransform rect)
		{
			if(profiles == null || profiles.Count == 0 || rect == null || replaced == null) return;

			Set(componentProfileText.GetStatsText(profiles, replaced, slot), rect);
		}


		public void SetStats(ComponentProfile profile, EquipSlot slot, RectTransform rect)
		{
			if(profile == null || rect == null) return;

			Set(componentProfileText.GetStatsText(profile, slot), rect);
		}

		public void SetStats(ComponentProfile profile, ComponentProfile replaced, EquipSlot slot, RectTransform rect)
		{
			if(profile == null || replaced == null || rect == null) return;

			Set(componentProfileText.GetStatsText(profile, replaced, slot), rect);
		}

		private void Set(string text, RectTransform rect)
		{
			if(rect == null) return;


			tooltip.Reset()
			.WithBody(text)
			.WithFadeTime(fadeTime)
			.WithTarget(rect)
			.WithTargetCorner(targetCorner)
			.WithExpansionCorner(expansionCorner)
			.Set()
			.Start();
		}

		public void Hide()
		{
			tooltip.ForceHide();
		}
	}
}