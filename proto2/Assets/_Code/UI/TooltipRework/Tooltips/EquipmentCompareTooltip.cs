﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class EquipmentCompareTooltip : MonoBehaviour 
	{
		private RectTransform tooltipRect;
		public FloatReference fadeTime;

		[Space(20)]
		public Tooltip equipTooltip;
		public Tooltip highlightTooltip;
		public StatComparisonGenerator statComparisonGenerator;
		public SelectedText selectedText;
		public EquipmentText equipmentText;
		public EquipmentCompareText equipmentCompareText;

		public void Awake()
		{
			tooltipRect = highlightTooltip.transform as RectTransform;
		}

		public void Set(Equipment equipped, RectTransform targetRect, Equipment highlighted)
		{
			if(equipped == null || highlighted == null || targetRect == null || tooltipRect == null) return;

			SetEquipped(equipped, statComparisonGenerator.Generate(equipped, highlighted));
			SetHighlighted(highlighted, targetRect);
			highlightTooltip.Start();
		}

		public void Hide()
		{
			equipTooltip.ForceHide();
			highlightTooltip.ForceHide();
		}

		private void SetEquipped(Equipment equipped, List<StatComparison> stats)
		{
			string text = equipmentCompareText.GetText(equipped, stats) + selectedText.GetText();

			equipTooltip.Reset()
			.WithBody(text)
			.WithFadeTime(fadeTime)
			.WithTarget(tooltipRect)
			.WithTargetCorner(Corner.TopLeft)
			.WithExpansionCorner(Corner.BottomLeft)
			.Set();
		}

		private void SetHighlighted(Equipment highlighted, RectTransform targetRect)
		{
			string text = equipmentText.GetText(highlighted);

			highlightTooltip.Reset()
			.WithBody(text)
			.WithFadeTime(fadeTime)
			.WithTarget(targetRect)
			.WithTargetCorner(Corner.TopLeft)
			.WithExpansionCorner(Corner.BottomLeft)
			.WithChainTooltip(equipTooltip)
			.Set();
		}
	}
}