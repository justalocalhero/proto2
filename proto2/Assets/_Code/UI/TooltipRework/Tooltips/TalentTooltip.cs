﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class TalentTooltip : MonoBehaviour 
	{
		public FloatReference fadeTime;

		[Space(20)]
		public Tooltip tooltip;
		public TalentText talentText;

		public void Set(Talent talent, TalentOption talentOption, RectTransform rect)
		{
			if(talent == null || talentOption == null || rect == null) return;

			Set(talentText.GetText(talent, talentOption), rect);
		}

		public void Set(Talent talent, RectTransform rect)
		{
			if(talent == null || rect == null) return;

			Set(talentText.GetText(talent), rect);
		}

		public void Set(TalentOption talentOption, RectTransform rect)
		{
			if(talentOption == null || rect == null) return;

			Set(talentText.GetText(talentOption), rect);
		}

		private void Set(string text, RectTransform rect)
		{
			if(rect == null) return;

			tooltip.Reset()
			.WithBody(text)
			.WithFadeTime(fadeTime)
			.WithTarget(rect)
			.WithTargetCorner(Corner.TopLeft)
			.WithExpansionCorner(Corner.BottomLeft)
			.Set()
			.Start();
		}

		public void Hide()
		{
			tooltip.ForceHide();
		}
	}
}