﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class BoonTooltip : MonoBehaviour 
	{

		public FloatReference fadeTime;

		[Space(20)]
		public Tooltip tooltip;
		public BoonText boonText;

		public void Set(Boon boon, RectTransform rect)
		{
			if(boon == null || rect == null) return;

			string text = boonText.GetText(boon);

			tooltip.Reset()
			.WithBody(text)
			.WithFadeTime(fadeTime)
			.WithTarget(rect)
			.WithTargetCorner(Corner.TopLeft)
			.WithExpansionCorner(Corner.BottomLeft)
			.Set()
			.Start();
		}

		public void Hide()
		{
			tooltip.ForceHide();
		}
	}
}