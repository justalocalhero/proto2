﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class TimedEffectTooltip : MonoBehaviour 
	{
		public FloatReference fadeTime;

		[Space(20)]
		public Tooltip tooltip;
		public TimedEffectText timedEffectText;

		public void Set(TimedEffect timedEffect, EffectType type, RectTransform rect)
		{
			if(timedEffect == null || rect == null) return;

			if(type == EffectType.buff) SetBuff(timedEffect, rect);
			if(type == EffectType.debuff) SetDebuff(timedEffect, rect);
		}

		public void SetBuff(TimedEffect timedEffect, RectTransform rect)
		{
			if(timedEffect == null || rect == null) return;

			Set(timedEffectText.GetBuffText(timedEffect), rect);
		}

		public void SetDebuff(TimedEffect timedEffect, RectTransform rect)
		{
			if(timedEffect == null || rect == null) return;

			Set(timedEffectText.GetDebuffText(timedEffect), rect);
		}

		private void Set(string text, RectTransform rect)
		{
			if(rect == null) return;
			
			tooltip.Reset()
			.WithBody(text)
			.WithFadeTime(fadeTime)
			.WithTarget(rect)
			.WithTargetCorner(Corner.BottomRight)
			.WithExpansionCorner(Corner.TopRight)
			.Set()
			.Start();
		}

		public void Hide()
		{
			tooltip.ForceHide();
		}
	}
}