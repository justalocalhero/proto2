﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class TextTooltip : MonoBehaviour 
	{
		public FloatReference fadeTime;
		public Tooltip tooltip;
		public Corner targetCorner, expansionCorner;

		public void Set(string text, RectTransform rect)
		{
			if(rect == null) return;

			tooltip.Reset()
			.WithBody(text)
			.WithFadeTime(fadeTime)
			.WithTarget(rect)
			.WithTargetCorner(targetCorner)
			.WithExpansionCorner(expansionCorner)
			.Set()
			.Start();
		}

		public void Hide()
		{
			tooltip.ForceHide();
		}
	}
}