﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class BoonCompareTooltip : MonoBehaviour 
	{
		private RectTransform tooltipRect;
		public FloatReference fadeTime;

		[Space(20)]
		public Tooltip equipTooltip;
		public Tooltip highlightTooltip;
		public BoonText boonText;
		public SelectedText selectedText;

		public void Awake()
		{
			tooltipRect = highlightTooltip.transform as RectTransform;
		}

		public void Set(Boon equipped, RectTransform targetRect, Boon highlighted)
		{
			if(equipped == null || highlighted == null || targetRect == null || tooltipRect == null) return;

			SetEquipped(equipped);
			SetHighlighted(highlighted, targetRect);
			highlightTooltip.Start();
		}

		public void Hide()
		{
			equipTooltip.ForceHide();
			highlightTooltip.ForceHide();
		}

		private void SetEquipped(Boon equipped)
		{
			string text = boonText.GetText(equipped) + selectedText.GetText();

			equipTooltip.Reset()
			.WithBody(text)
			.WithFadeTime(fadeTime)
			.WithTarget(tooltipRect)
			.WithTargetCorner(Corner.TopLeft)
			.WithExpansionCorner(Corner.BottomLeft)
			.Set();
		}

		private void SetHighlighted(Boon highlighted, RectTransform targetRect)
		{
			string text = boonText.GetText(highlighted);

			highlightTooltip.Reset()
			.WithBody(text)
			.WithFadeTime(fadeTime)
			.WithTarget(targetRect)
			.WithTargetCorner(Corner.TopLeft)
			.WithExpansionCorner(Corner.BottomLeft)
			.WithChainTooltip(equipTooltip)
			.Set();
		}
	}
}