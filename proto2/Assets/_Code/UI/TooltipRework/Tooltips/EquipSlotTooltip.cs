﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Tooltips
{
	public class EquipSlotTooltip : MonoBehaviour 
	{
		public EquipSlotText equipSlotText;
		public FloatReference fadeTime;
		public Tooltip tooltip;
		public Corner targetCorner, expansionCorner;

		public void Set(EquipSlot slot, RectTransform rect)
		{
			if(rect == null) return;

			string text = equipSlotText.GetText(slot);
			
			tooltip.Reset()
			.WithBody(text)
			.WithFadeTime(fadeTime)
			.WithTarget(rect)
			.WithTargetCorner(targetCorner)
			.WithExpansionCorner(expansionCorner)
			.Set()
			.Start();
		}

		public void Hide()
		{
			tooltip.ForceHide();
		}
	}
}
