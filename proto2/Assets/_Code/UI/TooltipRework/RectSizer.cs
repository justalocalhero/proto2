﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class RectSizer : MonoBehaviour 
	{
		private RectTransform rect;
		
		public void Awake()
		{
			rect = transform as RectTransform;
		}

		public void Size(Vector2 size)
		{
			rect.sizeDelta = size;
		}
	}
}