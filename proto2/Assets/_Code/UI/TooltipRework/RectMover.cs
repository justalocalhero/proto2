﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class RectMover : MonoBehaviour 
	{
		private RectTransform rect;

		public void Awake()
		{
			rect = transform as RectTransform;
		}

		public void Move(RectTransform target, float xCorner, float yCorner, float xExp, float yExp)
		{
			if(target == null) return;

			Vector3[] corners = new Vector3[4];
			target.GetWorldCorners(corners);
			float targetWidth = corners[3].x - corners[1].x;
			float targetHeight = corners[3].y  - corners[1].y;
			
			float targetX = corners[1].x + targetWidth * xCorner;
			float targetY = corners[1].y + targetHeight * yCorner;


			Move(new Vector2(targetX, targetY), xExp, yExp);
		}

		public void Move(Vector2 target, float xExp, float yExp)
		{
			float width = rect.rect.width;
			float height = rect.rect.height;

			float expansionWidth = (xExp - 1) * width;
			float expansionHeight = (1 - yExp) * height;

			float pivotWidth = width * rect.pivot.x + expansionWidth;
			float pivotHeight = height * (rect.pivot.y - 1) + expansionHeight;
			
			Vector3 dimensionVector = rect.TransformVector(new Vector3(pivotWidth, pivotHeight));
			
			float x = target.x + dimensionVector.x;
			float y = target.y + dimensionVector.y;

			rect.position = new Vector2(x, y);

		}
	}
}
public enum Corner {BottomLeft = 0, TopLeft = 1, TopRight = 2, BottomRight = 3}