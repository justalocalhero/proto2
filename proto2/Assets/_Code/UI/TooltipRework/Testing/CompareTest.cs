﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Tooltips
{
	public class CompareTest : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
	{
		private RectTransform rect;
		public Button button;
		public RectTransform equippedRect;
		public EquipmentCompareTooltip tooltip;

		public EquipmentGenerator compareGenerator;
		public List<EquipmentGenerator> equipmentGenerators;
		private Equipment equipped;
		private List<Equipment> highlightEquipment;


		public void Start()
		{
			rect = transform as RectTransform;
			highlightEquipment = new List<Equipment>();
			foreach(EquipmentGenerator generator in equipmentGenerators)
			{
				highlightEquipment.Add(generator.Generate());
			}
			equipped = compareGenerator.Generate();
		}

		
		public void OnPointerEnter(PointerEventData eventData)
		{
			((IPointerEnterHandler)button).OnPointerEnter(eventData);
			tooltip.Set(equipped, rect, Utility.RandomFromList(highlightEquipment));
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			((IPointerExitHandler)button).OnPointerExit(eventData);
			tooltip.Hide();
		}
	}
}