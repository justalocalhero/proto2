﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


namespace Tooltips
{
	public class TalentTest : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
	{
		private RectTransform rect;
		public Button button;
		public TalentTooltip tooltip;

		public List<TalentBuilder> builders;
		private List<Talent> payload;


		public void Start()
		{
			rect = transform as RectTransform;
			payload = new List<Talent>();

			foreach(TalentBuilder builder in builders)
			{
				payload.Add(builder.Generate(0));
			}
		}

		
		public void OnPointerEnter(PointerEventData eventData)
		{
			((IPointerEnterHandler)button).OnPointerEnter(eventData);
			tooltip.Set(Utility.RandomFromList(payload), rect);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			((IPointerExitHandler)button).OnPointerExit(eventData);
			tooltip.Hide();
		}
	}
}