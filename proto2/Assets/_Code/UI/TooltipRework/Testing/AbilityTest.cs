﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


namespace Tooltips
{
	public class AbilityTest : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
	{
		private RectTransform rect;
		public Button button;
		public AbilityTooltip tooltip;

		public List<AbilityBuilder> builders;
		private List<Ability> payload;


		public void Start()
		{
			rect = transform as RectTransform;
			payload = new List<Ability>();
			
			foreach(AbilityBuilder builder in builders)
			{
				payload.Add(builder.Generate());
			}
		}

		
		public void OnPointerEnter(PointerEventData eventData)
		{
			((IPointerEnterHandler)button).OnPointerEnter(eventData);
			tooltip.Set(Utility.RandomFromList(payload), rect);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			((IPointerExitHandler)button).OnPointerExit(eventData);
			tooltip.Hide();
		}
	}
}