﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Tooltips
{
	public class TooltipTest : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
	{
		public Button button;
		public Tooltip tooltip;

		[Space(20)]
		public DamageText damageText;
		public EquipStatText equipStatText;
		public TimedEffectText timedEffectText;
		public AbilityText abilityText;
		public TalentText talentText;
		public EquipmentText equipmentText;
		public EquipmentCompareText equipmentCompareText;
		public StatComparisonGenerator StatComparisonGenerator;
		public BoonText boonText;

		[Space(20)]
		public List<BuffBuilder> buffBuilders;
		public List<AbilityBuilder> abilityBuilders;
		public List<TalentBuilder> talentBuilders;
		public List<TalentOptionBuilder> talentOptionBuilders;
		public List<EquipmentGenerator> equipmentBuilders;
		public List<BoonBuilder> boonBuilders;
		public EquipmentGenerator equipmentCompare;

		private List<Damage> damages;
		private List<EquipStat> stats;
		private List<TimedEffect> timedEffects;
		private List<Ability> abilities;
		private List<Talent> talents;
		private List<TalentOption> talentOptions;
		private List<Equipment> equipment;
		private List<Boon> boons;


		[Space(20)]		
		public TooltipType currentType;
		public enum TooltipType {talent, equipment, equipmentCompare, ability, timedEffect, boon};

        public void OnPointerEnter(PointerEventData eventData)
        {
            ((IPointerEnterHandler)button).OnPointerEnter(eventData);
			Test();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            ((IPointerExitHandler)button).OnPointerExit(eventData);
			tooltip.Hide();
        }

        public void Start()
		{
			boons = new List<Boon>();
			foreach(BoonBuilder builder in boonBuilders)
			{
				boons.Add(builder.Generate(0));
			}

			equipment = new List<Equipment>();
			foreach(EquipmentGenerator builder in equipmentBuilders)
			{
				equipment.Add(builder.Generate());
			}

			talentOptions = new List<TalentOption>();
			foreach(TalentOptionBuilder builder in talentOptionBuilders)
			{
				talentOptions.Add(builder.Generate(0));
			}

			talents = new List<Talent>();
			foreach(TalentBuilder builder in talentBuilders)
			{
				talents.Add(builder.Generate(0));
			}

			abilities = new List<Ability>();
			foreach(AbilityBuilder builder in abilityBuilders)
			{
				abilities.Add(builder.Generate());
			}

			timedEffects = new List<TimedEffect>();
			foreach(BuffBuilder builder in buffBuilders)
			{
				timedEffects.Add(builder.Generate());
			}

			damages = new List<Damage>();
			damages.Add(new Damage {value = 10f, damageFactor = .35f, type = DamageType.fire});
			damages.Add(new Damage {value = 2f, damageFactor = 0, type = DamageType.healing});
			damages.Add(new Damage {value = 1f, damageFactor = .5f, type = DamageType.physical});
			damages.Add(new Damage {value = 3f, damageFactor = 2f, type = DamageType.cold});

			stats = new List<EquipStat>();
			stats.Add(new EquipStat {Value = 2f, StatType = StatType.Hitpoints, ModifierType = ModifierType.Flat});
			stats.Add(new EquipStat {Value = 10f, StatType = StatType.SpellProficiency, ModifierType = ModifierType.Flat});
			stats.Add(new EquipStat {Value = .5f, StatType = StatType.Haste, ModifierType = ModifierType.Multiplier});
			stats.Add(new EquipStat {Value = -.25f, StatType = StatType.CritChance, ModifierType = ModifierType.GlobalMultiplier});
		}

		public void Test()
		{
			string testString = SelectedString();

			tooltip.Reset()
			.WithBody(testString)
			.WithFadeTime(.5f)
			.WithTarget(button.transform as RectTransform)
			.Set();
		}

		public string SelectedString()
		{
			switch(currentType)
			{
				case TooltipType.ability:
					return AbilityString();
				case TooltipType.boon:
					return BoonString();
				case TooltipType.equipment:
					return EquipmentString();
				case TooltipType.equipmentCompare:
					return EquipmentCompareString();
				case TooltipType.talent:
					return "";//TalentString();
				case TooltipType.timedEffect:
					return TimedEffectString();
				default:
					return "";
			}
		}

		public string AbilityString()
		{
			return abilityText.GetText(abilities[Random.Range(0, abilities.Count)]);
		}

		public string DamageString()
		{
			return damageText.GetText(damages[Random.Range(0, damages.Count)]);
		}

		public string StatString()
		{
			return equipStatText.GetRolledText(stats[Random.Range(0, stats.Count)]);
		}

		public string TimedEffectString()
		{
			return timedEffectText.GetBuffText(timedEffects[Random.Range(0, timedEffects.Count)]);
		}

		// public string TalentString()
		// {
		// 	Talent tempTalent = talents[Random.Range(0, talents.Count)];
		// 	TalentOption tempOption = talentOptions[Random.Range(0, talentOptions.Count)];

		// 	return talentText.GetText(tempTalent, tempOption);
		// }

		public string EquipmentString()
		{
			return equipmentText.GetText(equipment[Random.Range(0, equipment.Count)]);
		}

		public string EquipmentCompareString()
		{
			Equipment initial = equipmentCompare.Generate();
			Equipment highlight = equipment[Random.Range(0, equipment.Count)];
			List<StatComparison> comparisonList = StatComparisonGenerator.Generate(initial, highlight);

			return equipmentCompareText.GetText(initial, comparisonList);
		}

		public string BoonString()
		{
			return boonText.GetText(boons[Random.Range(0, boons.Count)]);
		}
	}
}