﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


namespace Tooltips
{
	public class EquipmentTest : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
	{
		private RectTransform rect;
		public Button button;
		public EquipmentTooltip tooltip;

		public List<EquipmentGenerator> builders;
		private List<Equipment> payload;


		public void Start()
		{
			rect = transform as RectTransform;
			payload = new List<Equipment>();

			foreach(EquipmentGenerator builder in builders)
			{
				payload.Add(builder.Generate());
			}
		}

		
		public void OnPointerEnter(PointerEventData eventData)
		{
			((IPointerEnterHandler)button).OnPointerEnter(eventData);
			tooltip.Set(Utility.RandomFromList(payload), rect);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			((IPointerExitHandler)button).OnPointerExit(eventData);
			tooltip.Hide();
		}
	}
}