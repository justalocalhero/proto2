﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


namespace Tooltips
{
	public class TimedEffectTest : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
	{
		private RectTransform rect;
		public Button button;
		public TimedEffectTooltip tooltip;

		public List<BuffBuilder> builders;
		private List<TimedEffect> payload;


		public void Start()
		{
			rect = transform as RectTransform;
			payload = new List<TimedEffect>();

			foreach(BuffBuilder builder in builders)
			{
				payload.Add(builder.Generate());
			}
		}

		
		public void OnPointerEnter(PointerEventData eventData)
		{
			((IPointerEnterHandler)button).OnPointerEnter(eventData);
			tooltip.SetBuff(Utility.RandomFromList(payload), rect);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			((IPointerExitHandler)button).OnPointerExit(eventData);
			tooltip.Hide();
		}
	}
}