﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tooltips
{
	public class Fader : MonoBehaviour 
	{
		private bool toClear;
		private float nextClearTime, fadeTime;
		public UIToggler toggler;

		void Update()
		{
			if(CheckToClear()) Clear();
		}

		public void SetFadeTime(float fadeTime)
		{
			this.fadeTime = fadeTime;	
		}

		public void SetToShow()
		{
			toClear = false;
			Show();
		}

		public void SetToClear()
		{
			toClear = true;
			nextClearTime = Time.time + fadeTime;
		}

		public void ForceClear()
		{
			Clear();
		}

		private bool CheckToClear()
		{
			return toClear && Time.time >= nextClearTime;
		}

		private void Clear()
		{
			toClear = false;
			Hide();
		}	
		
		private void Hide()
		{
			if(toggler != null) 
				toggler.Hide();
		}

		private void Show()
		{
			if(toggler != null) 
				toggler.Show();
		}
	}
}