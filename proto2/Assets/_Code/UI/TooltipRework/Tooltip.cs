﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Tooltips
{
	public class Tooltip : MonoBehaviour 
	{
		public Image icon;
		public TextMeshProUGUI titleMesh;
		public TextMeshProUGUI bodyMesh;

    	[System.NonSerialized]
		private Sprite sprite;
		private string title, body;
		private RectTransform target;
		private bool toShow;
		private float xCorner, yCorner, xExpansion, yExpansion;
		private Tooltip chainTooltip;

		public RectSizer sizer;
		public RectMover mover;
		public RectBounds bounds;
		public Fader fader;

		private IEnumerator showRoutine;
		
		public Tooltip Reset()
		{
			ForceHide();
			toShow = false;
			sprite = null;
			title = null;
			body = null;
			chainTooltip = null;

			return this;
		}

		public Tooltip WithSprite(Sprite sprite)
		{
			this.sprite = sprite;

			return this;
		}

		public Tooltip WithTitle(string title)
		{
			this.title = title;

			return this;
		}		

		public Tooltip WithBody(string body)
		{
			this.body = body;

			return this;
		}

		public Tooltip WithFadeTime(float fadeTime)
		{
			fader.SetFadeTime(fadeTime);

			return this;
		}

		public Tooltip WithTarget(RectTransform target)
		{
			this.target = target;

			return this;
		}

		public Tooltip WithTargetCorner(Corner targetCorner)
		{
			this.xCorner = IsLeft(targetCorner) ? 0 : 1;
			this.yCorner = IsTop(targetCorner) ? 0 : 1;

			return this;
		}

		public Tooltip WithTargetCorner(float x, float y)
		{
			this.xCorner = x;
			this.yCorner = y;

			return this;
		}

		public Tooltip WithExpansionCorner(Corner expansionCorner)
		{
			this.xExpansion = IsLeft(expansionCorner) ? 0 : 1;
			this.yExpansion = IsTop(expansionCorner) ? 0 : 1;

			return this;
		}

		public Tooltip WithExpansionCorner(float x, float y)
		{
			this.xExpansion = x;
			this.yExpansion = y;

			return this;
		}

		public Tooltip WithChainTooltip(Tooltip chainTooltip)
		{
			this.chainTooltip = chainTooltip;

			return this;
		}

		public Tooltip Set()
		{
			toShow = true;

			SetSprite();
			SetTitle();
			SetBody();

			return this;
		}

		public Tooltip Start()
		{
		
			if(showRoutine != null) StopCoroutine(showRoutine);
			showRoutine = ShowSequence();
			StartCoroutine(showRoutine);

			return this;
		}

		private IEnumerator ShowSequence()
		{
			yield return StartCoroutine(Size());

			yield return StartCoroutine(Position());

			yield return StartCoroutine(Fit());

			yield return new WaitForEndOfFrame();
			
			Show();
		}

		public void Show()
		{
			if(toShow) fader.SetToShow();
			if(chainTooltip != null) chainTooltip.Show();
		}

		public void Hide()
		{
			toShow = false;
			fader.SetToClear();
		}

		public void ForceHide()
		{
			toShow = false;
			if(showRoutine != null) StopCoroutine(showRoutine);
			
			fader.ForceClear();
		}

		private void SetSprite()
		{
			if(sprite != null && icon != null) icon.sprite = sprite;
		}

		private void SetTitle()
		{
			if(titleMesh != null && title != null) titleMesh.SetText(title);
		}

		private void SetBody()
		{
			if(bodyMesh != null && body != null) bodyMesh.SetText(body);
		}

		private IEnumerator Size()
		{
			Vector2 titleSize = titleMesh.GetPreferredValues(title);
			Vector2 bodySize = bodyMesh.GetPreferredValues(body);

			sizer.Size(new Vector2(Utility.Greater(titleSize.x, bodySize.x), titleSize.y + bodySize.y));

			yield return new WaitForEndOfFrame();

			if(chainTooltip != null) yield return StartCoroutine(chainTooltip.Size());
		}

		private IEnumerator Position()
		{
			if(target != null) mover.Move(target, xCorner, yCorner, xExpansion, yExpansion);

			yield return new WaitForEndOfFrame();

			if(chainTooltip != null) yield return StartCoroutine(chainTooltip.Position());
		}

		private IEnumerator Fit()
		{
			bounds.Fit();

			yield return new WaitForEndOfFrame();

			if(chainTooltip != null) yield return StartCoroutine(chainTooltip.Fit());
		}

		private bool IsLeft(Corner corner)
		{
			return (int)corner < 2;
		}

		private bool IsTop(Corner corner)
		{
			return ((int)corner + 3) % 4 < 2;
		}
	}
}