﻿using System.Collections.Generic;
using UnityEngine;

public class UIHider : MonoBehaviour {

    public List<UIToggler> elements =  new List<UIToggler>();

    public void Hide()
    {
        if (elements != null)
            for (int i = 0; i < elements.Count; i++)
            {
                if(elements[i] != null)
                {
                    elements[i].Hide();
                }
            }
    }
}
