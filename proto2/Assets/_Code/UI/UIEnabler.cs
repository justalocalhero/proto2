﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIEnabler : MonoBehaviour {

    public List<UIToggler> elements = new List<UIToggler>();

    public void Show()
    {
        if (elements != null)
            for (int i = 0; i < elements.Count; i++)
            {
                if (elements[i] != null)
                {
                    elements[i].Show();
                }
            }
    }
}
