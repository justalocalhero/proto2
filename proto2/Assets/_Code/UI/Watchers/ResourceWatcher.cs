﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResourceWatcher : MonoBehaviour 
{

	public ResourceManager resources;
	public TextMeshProUGUI mesh;

	public void Start()
	{
		resources.onResourceChanged += UpdateCostText;
		UpdateCostText();
	}

	public void UpdateCostText()
	{
		string costString = "";
		Resource[] resourceArray = resources.GetResources();
		for(int i = 0; i < resourceArray.Length; i++)
		{
			if(resourceArray[i].value > 0) costString += (resourceArray[i].type + ": " + resourceArray[i].value + "\n");
		}

		mesh.text = costString;
	}
}
