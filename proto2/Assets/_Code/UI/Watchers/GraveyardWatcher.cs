﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GraveyardWatcher : MonoBehaviour 
{
	public Snapshot snapshot;
	public IntVariable deathCount;
	public TextMeshProUGUI mesh;

	public void SetSnapshot(Snapshot newSnapshot)
	{
		if(newSnapshot != null) snapshot = newSnapshot;
		UpdateUI();
	}

	public void UpdateUI()
	{
		mesh.enabled = true;
		string toUpdate = "";
		toUpdate += "Graves: " + (int)Mathf.Floor(deathCount.Value) + "\n\n";
		if(snapshot == null) return;
		toUpdate += "Adventurer Slew: " + snapshot.killCount + "\n";
		toUpdate += "Killed By: " + snapshot.killingBlow.type + " " + snapshot.killingBlow.value + "\n";
		toUpdate += "After Reaching Level: " + snapshot.levelReached + "\n";

		mesh.text = toUpdate;
	}
}
