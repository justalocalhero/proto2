﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EquipStatWatcher : MonoBehaviour 
{
	public TextMeshProUGUI nameMesh;
	public TextMeshProUGUI valueMesh;
	public StatNameDictionary statNameDictionary;
	EquipStat equipStat;

	public void UpdateUI()
	{
		UpdateUI(1);
	}

	public void UpdateUI(float modifier)
	{
		if(nameMesh == null) 
		{
			Debug.LogWarning("nameMesh in equipStatWatcher was null: could not update text.");
			return;
		}
		if(valueMesh == null)
		{
			Debug.LogWarning("valueMesh in equipStatWatcher was null: could not update text.");
			return;
		}
		if(statNameDictionary == null) 
		{
			Debug.LogWarning("statNameDictionary in equipStatWatcher was null: could not update text.");
			return;
		}

		nameMesh.text = statNameDictionary.GetTypeName(equipStat.StatType);
		string sign;
		if(equipStat.Value >= 0) sign = "+";
		else sign = "";
		//if flat value
		if(equipStat.ModifierType == ModifierType.Flat) valueMesh.text = sign + Mathf.Round(equipStat.Value * modifier);

		//else if percent value, display in percent
		else valueMesh.text = sign + Mathf.Round(100 * equipStat.Value * modifier) + "%";

		nameMesh.enabled = true;
		valueMesh.enabled = true;
		
	}

	public void Add(EquipStat newStat, float modifier)
	{
		equipStat = newStat;
		UpdateUI(modifier);
	}

	public void Add(EquipStat newStat)
	{
		Add(newStat, 1);
	}

	public void Clear()
	{
		nameMesh.enabled = false;
		valueMesh.enabled = false;

	}
}
