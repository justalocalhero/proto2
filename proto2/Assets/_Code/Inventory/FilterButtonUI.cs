﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FilterButtonUI : MonoBehaviour 
{
	public Transform buttoContainer;
	public Inventory inventory;

	private FilterButton[] buttons;

	[SerializeField]
	private FloatReference buttonWidth;

	[SerializeField]
	private FloatReference unavailableHeight;

	[SerializeField]
	private FloatReference unavailableDuration;

	[SerializeField]
	private FloatReference availableHeight;

	[SerializeField]
	private FloatReference availableDuration;

	[SerializeField]
	private FloatReference selectedHeight;

	[SerializeField]
	private FloatReference selectedDuration;


	public void Start()
	{
		buttons = buttoContainer.GetComponentsInChildren<FilterButton>();
		inventory.onInventoryChanged += UpdateUI;

	}

	public void UpdateUI()
	{
		for(int i = 0; i < buttons.Length; i++)
		{
			if(i >= inventory.items.Length) SetUnavailable(buttons[i]);
			else if(i == inventory.selectedIndex) SetSelected(buttons[i]);
			else if(inventory.items[i].Count > 0) SetAvailable(buttons[i]);
			else SetUnavailable(buttons[i]);
		}
	}

	public void SetSelected(FilterButton button)
	{
		if(button.currentState == FilterButtonState.selected) return;
		button.currentState = FilterButtonState.selected;
		RectTransform rect = button.transform as RectTransform;
		DOTween.Kill(rect);
		rect.DOSizeDelta(new Vector2(buttonWidth, selectedHeight), selectedDuration)
		.OnComplete(() =>
		{
			if(!button.icon.enabled) button.icon.enabled = true;
		});
	}

	public void SetUnavailable(FilterButton button)
	{
		if(button.currentState == FilterButtonState.unavailable) return;
		button.currentState = FilterButtonState.unavailable;
		RectTransform rect = button.transform as RectTransform;
		DOTween.Kill(rect);
		rect.DOSizeDelta(new Vector2(buttonWidth, unavailableHeight), unavailableDuration);
		button.icon.enabled = false;
	}

	public void SetAvailable(FilterButton button)
	{
		if(button.currentState == FilterButtonState.available) return;
		button.currentState = FilterButtonState.available;
		RectTransform rect = button.transform as RectTransform;
		DOTween.Kill(rect);
		rect.DOSizeDelta(new Vector2(buttonWidth, availableHeight), availableDuration)
		.OnComplete(() =>
		{
			if(!button.icon.enabled) button.icon.enabled = true;
		});
	}
}

public enum FilterButtonState {selected, available, unavailable,};