﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Inventory : MonoBehaviour
{
    public IntReference maxInventorySpace;
    public SaveManager saveManager;

    public EquipmentRouter equipmentRouter;
    public Crafter crafter;
    public CraftingBench craftingBench;

    public List<Equipment>[] items;
    public List<Equipment>[] sortedEquipments;

    public List<Equipment> currentEquipments;
    public int selectedIndex {get; set;}

    private int totalIndex;

    public delegate void OnInventoryChanged();
    public OnInventoryChanged onInventoryChanged;

    public delegate void OnEquipmentUsed(Equipment equipment, Inventory inventory);
    public OnEquipmentUsed onEquipmentUsed;

    public delegate void OnEquipmentRecieved(Equipment equipment);
    public OnEquipmentRecieved onEquipmentRecieved;

    public ArtifactManager artifactManager;

    public void Awake()
    {
        if(saveManager != null)
        {
            saveManager.onPreSave += Save;
            saveManager.onLoad += Load;
        }

        selectedIndex = -1;
        totalIndex = Enum.GetNames(typeof(EquipSlot)).Length;
        
        Clear();
        SetCurrentEquipmentsTotal();
    }

    public void Start()
    {
        if(crafter != null) crafter.onCraft += Add;
        if(craftingBench != null) craftingBench.onChanged += RaiseInventoryChanged;
    }

    public void Save(SaveFile saveFile)
    {
        if(saveFile == null) return;

        List<Equipment> toReturn = items[totalIndex];

        saveFile.inventory = toReturn.ToArray();

    }

    public void Load(SaveFile saveFile)
    {
        if(saveFile == null) return;
        if(saveFile.inventory == null) return;

        Clear();
        
        Add(saveFile.inventory);
        SetCurrentEquipments(selectedIndex);

    }

    public void Clear()
    {        
        items = new List<Equipment>[totalIndex + 1];
        sortedEquipments = new List<Equipment>[totalIndex + 1];

        for(int i = 0; i <= totalIndex; i++)
        {
            items[i] = new List<Equipment>();
            sortedEquipments[i] = new List<Equipment>();
        }       
    }

    public void Use(Equipment equipment)
    {
        if(equipment == null) return;

        RaiseOnEquipmentUsed(equipment);
    }

    public void Add(Equipment item, bool toRaise)
    {
        if(item != null && items[totalIndex].Count < maxInventorySpace)
        {
            AddUnsorted(item);
            AddLevelSorted(item);
            if(toRaise) RaiseInventoryChanged();
            if(item is Equipment) RaiseOnEquipmentRecieved(item as Equipment);
        }
        else
        {
            if(item != null) artifactManager.Add(item);
        }
    }

    public void Add(Equipment item)
    {
        Add(item, true);
    }

    public void Add(Equipment[] items)
    {
        if(items == null) return;
        for(int i = 0; i < items.Length; i++)
        {
            if(items[i] != null)
            {
                Add(items[i], false);
            }
        }
        RaiseInventoryChanged();
    }

    public void AddLevelSorted(Equipment item)
    {
        if(item == null) 
        {
            Debug.LogWarning("Equipment to add to inventory was null." );
            return;
        }
        
        int slotIndex = (int)item.GetSlot();
        bool foundSlot = false;
        bool foundTotal = false;

        for(int i = 0; i < sortedEquipments[slotIndex].Count; i++)
        {
            if(sortedEquipments[slotIndex][i].level < item.level)
            {
                sortedEquipments[slotIndex].Insert(i, item);
                foundSlot = true;
                break;
            }
        }

        for(int i = 0; i < sortedEquipments[totalIndex].Count; i++)
        {
            if(sortedEquipments[totalIndex][i].level < item.level)
            {
                sortedEquipments[totalIndex].Insert(i, item);
                foundTotal = true;
                break;
            }
        }

        if(!foundSlot) sortedEquipments[slotIndex].Add(item);
        if(!foundTotal) sortedEquipments[totalIndex].Add(item);
    }

    public void AddUnsorted(Equipment item)
    {
        if(item == null)
        {
            Debug.LogWarning("Equipment to add to inventory was null." );
            return;
        }

        int slotIndex = (int)item.GetSlot();

        items[slotIndex].Add(item);
        items[totalIndex].Add(item);
    }
    
    public void ApplySort()
    {
        for(int i = 0; i < sortedEquipments.Length; i++)
        {        
            items[i].Clear();
            for(int j = 0; j < sortedEquipments[i].Count; j++)
            {
                items[i].Add(sortedEquipments[i][j]);
            }
        }

        RaiseInventoryChanged();
    }

    public void ApplyAutoforge(Forge forge)
    {
        if(!forge.IsAutoForging()) return;

        Equipment currentEquipment;
        for(int i = 0; i < items.Length; i++)
        {
            for(int j = 0; j < items[i].Count; j++)
            {
                currentEquipment = items[i][j];
                if(forge.CheckAutoForge(currentEquipment)) 
                {
                    forge.Add(currentEquipment);
                    Remove(currentEquipment);
                }                
            }
        }
    }

    public void SetCurrentEquipments(EquipSlot newSlot)
    {
        SetCurrentEquipments((int)newSlot);
    }

    public void SetCurrentEquipments(int newIndex)
    {
        if(newIndex < 0 || newIndex >= items.Length)
        {
            Debug.LogWarning("Equipment slot index to set out of bounds");
            return;
        }

        currentEquipments = items[newIndex];
        selectedIndex = newIndex;
        RaiseInventoryChanged();
    }

    public void SetCurrentEquipmentsTotal()
    {
        SetCurrentEquipments(totalIndex);
    }

    public void Remove(Equipment item)
    {
        if(item == null) 
        {
            Debug.LogWarning("Equipment to remove from inventory was null.");
            return;
        }

        if(items == null) 
        {
            Debug.LogWarning("Equipments List was null when attempting to remove from inventory.");
            return;
        }

        if(sortedEquipments == null)
        {
            Debug.LogWarning("Sorted items list was null when attempting to remove item");
            return;
        }

        int slotIndex = (int)item.GetSlot();
        items[slotIndex].Remove(item);
        items[totalIndex].Remove(item);
        sortedEquipments[slotIndex].Remove(item);
        sortedEquipments[totalIndex].Remove(item);
        RaiseInventoryChanged();
        
    }

    public int GetEquipmentCount()
    {
        return items[totalIndex].Count;
    }

    public void RaiseInventoryChanged()
    {
        if(onInventoryChanged != null)
        {
            onInventoryChanged();
        }
    }

    public bool HasSlot(EquipSlot slot)
    {
        int index = (int)slot;
        if(index < 0 || index >= items.Length) return false;

        return items[index].Count > 0;
    }

    public void RaiseOnEquipmentUsed(Equipment equipment)
    {
        if(onEquipmentUsed != null) onEquipmentUsed(equipment, this);
    }
    
    public void RaiseOnEquipmentRecieved(Equipment equipment)
    {
        if(onEquipmentRecieved != null) onEquipmentRecieved(equipment);
    }
}
