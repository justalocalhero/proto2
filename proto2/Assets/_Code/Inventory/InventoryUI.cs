﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tooltips;
using TMPro;

public class InventoryUI : MonoBehaviour 
{
    public Inventory inventory;
    public InventorySlot slotPrototype;
    public Transform slotContainer;
    private List<InventorySlot> slots = new List<InventorySlot>();
    public EquipmentCompareTooltip equipmentCompareTooltip;
    public EquipmentTooltip equipmentTooltip;
    public EquipmentManager equipment;
    public Button previousButton, nextButton, sortButton, forgeButton;
    public FloatReference paddingX, paddingY, spacing;
    public IntReference inventorySpace;
    public TextMeshProUGUI inventorySpaceText;
    private int entriesPerPage;
	private int currentPage = 0;
    public ColorReference lootColor, lootWarningColor;

	void Start () 
    {
        BuildSlots();
        entriesPerPage = slots.Count;
        inventory.onInventoryChanged += UpdateUI;
        sortButton.onClick.AddListener(OnSortButtonPressed);
        UpdateUI();

	}
	
    private void BuildSlots()
    {
        RectTransform containerRect = slotContainer as RectTransform;
        RectTransform slotRect = slotPrototype.transform as RectTransform;

        float containerWidth = containerRect.rect.width;
        float containerHeight = containerRect.rect.height;

        float slotWidth = slotRect.rect.width + spacing;
        float slotHeight = slotRect.rect.height + spacing;

        int xCount = Mathf.FloorToInt((containerWidth - 2 * paddingX) / (slotWidth));
        int yCount = Mathf.FloorToInt((containerHeight - 2 * paddingY) / (slotHeight));

        float offsetX = .5f * (containerWidth - xCount * slotWidth + spacing);
        float offsetY = .5f * (containerHeight - yCount * slotHeight + spacing);

        for(int i = 0; i < yCount; i++)
        {
            for(int j = 0; j < xCount; j++)
            {
                float x = (j * slotWidth + offsetX);
                float y = -(i * slotHeight + offsetY);
                float z = 0;

                Vector3 tempPosition = new Vector3(x, y, z);
                    
                InventorySlot temp = Object.Instantiate(slotPrototype, slotContainer);
                (temp.transform as RectTransform).anchoredPosition = tempPosition;

                slots.Add(temp);
                PassReferences(temp);
            }
        }

    }

    void PassReferences(InventorySlot slot)
    {
        slot.inventory = inventory;
        slot.equipmentCompareTooltip = equipmentCompareTooltip;
        slot.equipmentTooltip = equipmentTooltip;
        slot.equipment = equipment;
        
    }

	void UpdateUI ()
    {
        
        int maxPage = Mathf.CeilToInt((inventory.currentEquipments.Count - 1) / entriesPerPage);
        currentPage = Mathf.Clamp(currentPage, 0, maxPage);

        int pageIndex = currentPage * entriesPerPage;
		int entryIndex;


		for(int i = 0; i < slots.Count; i++)
        {
            entryIndex = pageIndex + i;
            if(entryIndex < inventory.currentEquipments.Count)
            {
                slots[i].Add(inventory.currentEquipments[entryIndex]);
            }
            else
            {
                slots[i].Clear();
            }
        }
        UpdateInventoryCountText();
        UpdateButtons();
	}

    public void ResetPage()
	{
		currentPage = 0;
		UpdateUI();
	}

	public void OnPreviousButtonPressed()
	{
		AlterPage(-1);
	}

	public void OnNextButtonPressed()
	{
		AlterPage(1);
	}

	public void AlterPage(int value)
	{
		currentPage += value;
		UpdateUI();
	}

    public void UpdateInventoryCountText()
    {
        inventorySpaceText.SetText(GetInventoryCountText());
    }

    public string GetInventoryCountText()
    {
		int currentLootCount = inventory.GetEquipmentCount();

		if(currentLootCount <= 0) return "";

		Color color = (currentLootCount >= (inventorySpace - 10)) ? lootWarningColor : lootColor;

		string toReturn = Tooltips.ColoredText.GetText(currentLootCount.ToString(), color);

		return toReturn;
	
    }

	public void UpdateButtons()
	{
		if(inventory == null) return;

		if(previousButton != null) previousButton.gameObject.SetActive(currentPage > 0);
		if(nextButton != null) nextButton.gameObject.SetActive(inventory.currentEquipments.Count > (1 + currentPage) * entriesPerPage);
	}

    public void OnSortButtonPressed()
    {
        inventory.ApplySort();
    }

}
