﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Tooltips;

public class InventorySlot : MonoBehaviour
{
    public SpriteManager spriteManager;
    private RectTransform rect;
    public Image icon;
    public Image background;
    public ColorReference emptyBackground, ordinaryBackground, namedBackground, artifactBackground;
    public Inventory inventory {get; set;}
    public Equipment item {get; set;}
    public EquipmentManager equipment {get; set;}
    private Forge forge;
    public EquipmentCompareTooltip equipmentCompareTooltip {get; set;}
    public EquipmentTooltip equipmentTooltip  {get; set;}

    public delegate void OnUpdate();
    public OnUpdate onUpdate;

    public void Start()
    {
        rect = transform as RectTransform;
        forge = Forge.instance;
    }

    public void Add(Equipment newEquipment)
    {
        if(newEquipment == null) return;
        if(newEquipment is Equipment) SetBackgroundSprite(newEquipment as Equipment);
        item = newEquipment;
        icon.sprite = spriteManager.GetSprite(newEquipment.GetSprite());
        icon.enabled = true;

        RaiseOnUpdate();
    }

    public void Clear()
    {
        background.color = emptyBackground;
        item = null;
        icon.sprite = null;
        icon.enabled = false;
        
        RaiseOnUpdate();
    }

    public void SetBackgroundSprite(Equipment equipment)
    {
        if(equipment == null) return;
        if(equipment.rarity == Rarity.named) background.color = namedBackground;
        else if(equipment.rarity == Rarity.artifact) background.color = artifactBackground;
        else background.color = ordinaryBackground;
    }

    public void Use()
    {
        if(item == null) return;
        inventory.Use(item);
    }

    public void OnPointerClick()
    {
        equipment.ClearHighlightSlot();
        
        Use();
        OnPointerEnter();
    }

    public void OnPointerEnter()
    {
        Equipment equip = null;
        if(item is Equipment)
        {
            Equipment hover = item as Equipment;
            EquipSlot hoverSlot = hover.GetSlot();
            equip = equipment.GetEquipment(hoverSlot);
            if(equip != null && hover != null)
            {
                equipmentCompareTooltip.Set(equip, rect, hover);
            }
            else if(hover != null) 
            {
                equipmentCompareTooltip.Hide();
                equipmentTooltip.Set(hover, rect);
            }
            equipment.SetHighlightSlot(hoverSlot);
        }
    }

    public void OnPointerExit()
    {
        if(item is Equipment)
        {
            equipment.ClearHighlightSlot();

        }
        equipmentTooltip.Hide();
        equipmentCompareTooltip.Hide();
    }

    public void RaiseOnUpdate()
    {
        if(onUpdate != null) onUpdate();
    }
}
