﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentRouter : MonoBehaviour
{
	private IEquipmentReciever activeReciever;
	public Inventory inventory;

	public delegate void OnChanged();
	public OnChanged onChanged;

	public void Awake()
	{
		inventory.onEquipmentUsed += Recieve;
	}
	
	public void Recieve(Equipment equipment, Inventory inventory)
	{
		if(activeReciever != null) activeReciever.Recieve(equipment, inventory);
	}

	public void SetActiveReciever(IEquipmentReciever itemReciever)
	{
		if(itemReciever != null) 
		{
			activeReciever = itemReciever;
			RaiseOnChanged();
		}
	}

	public void RaiseOnChanged()
	{
		if(onChanged != null) onChanged();
	}
}
