﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryFilterManager : MonoBehaviour 
{
	public Transform filterButtonContainer;
	private Button[] filterButtons;

	void Start () {
		filterButtons = filterButtonContainer.GetComponentsInChildren<Button>();
	}
	
	public void Clear()
	{
		for(int i = 0; i < filterButtons.Length; i++)
		{
			filterButtons[i].interactable = true;
		}
	}
}
