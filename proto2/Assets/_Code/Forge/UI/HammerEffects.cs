﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Juice;

public class HammerEffects : MonoBehaviour 
{
	public UIToggler parentToggler;
	public HammerAnimator hammerAnimator;
	public Shake shake;

	public void Awake()
	{
		hammerAnimator.onHammerStrike += Play;
	}

	public void Play()
	{
		if(parentToggler.IsVisible()) shake.Fire();
	}
}
