﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;

public class HammerTargetAnimator : MonoBehaviour, IPointerClickHandler 
{
	private Sequence hitSequence;
	private Sequence destroySequence;

	public HammerAnimator hammer;
	public ForgePath forgePath;
	public Image image;
	public ColorReference destroyColor;
	public FloatReference destroyColorTimer;
	public ColorReference defaultColor;
	public FloatReference defaultColorTimer;
	public ColorReference hitColor;
	public FloatReference hitColorTimer;
	public FloatReference hitAngle;
	public FloatReference hitTimer;
	public FloatReference overAngle;
	public FloatReference overTimer;
	public FloatReference reloadAngle;
	public FloatReference reloadTimer;


	public void Start()
	{
		CreateSequence();
	}

	public void Hit()
	{
		hitSequence.Restart();
	}

	public void Destroy()
	{
		destroySequence.Restart();
	}

	public void CreateSequence()
	{
		hitSequence = DOTween.Sequence().SetAutoKill(false);
		hitSequence.Pause();

		hitSequence
		.Append(
			image.transform.DOLocalRotate(new Vector3(0, 0, hitAngle), hitTimer)
		)
		.Join(
			image.DOColor(hitColor, hitColorTimer)
		)
		.Append(
			image.transform.DOLocalRotate(new Vector3(0, 0, overAngle), overTimer)
		)
		.Join(
			image.DOColor(defaultColor, defaultColorTimer)
		)
		.Append(
			image.transform.DOLocalRotate(new Vector3(0, 0, reloadAngle), reloadTimer)
		);

		destroySequence = DOTween.Sequence().SetAutoKill(false);
		destroySequence.Pause();

		destroySequence
		.Append(
			image.transform.DOLocalRotate(new Vector3(0, 0, hitAngle), hitTimer)
		)
		.Join(
			image.DOColor(hitColor, hitColorTimer)
		)
		.Append(
			image.transform.DOLocalRotate(new Vector3(0, 0, overAngle), overTimer)
		)
		.Join(
			image.DOColor(destroyColor, destroyColorTimer)			
			.OnComplete(() => HandleDestroy())
		)
		.Append(
			image.transform.DOLocalRotate(new Vector3(0, 0, reloadAngle), reloadTimer)
		);
		
	}

	public void Set(Sprite sprite)
	{
		image.sprite = sprite;
		image.color = defaultColor;
		image.enabled = true;
		hammer.SetToHammer(true);
	}

	public void Remove()
	{
		image.enabled = false;
		hammer.SetToHammer(false);
	}

	public void Interrupt()
	{
		image.enabled = false;
		hammer.SetToHammer(false);
		hammer.Interrupt();
	}

	public void OnPointerClick(PointerEventData eventData)
    {
        forgePath.HandleClick(0);
    }

	public void HandleDestroy()
	{
		forgePath.HandleDestroy();
	}
}
