﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerParticles : MonoBehaviour 
{

	public ParticleSystem hammerParticles;
	public HammerAnimator hammerAnimator;
	public UIToggler parentToggler;

	public void Start()
	{
		hammerAnimator.onHammerStrike += Play;
	}

	public void Play()
	{
		if(parentToggler.IsVisible()) hammerParticles.Play();
	}
	
}
