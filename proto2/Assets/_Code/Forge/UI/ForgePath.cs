﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ForgePath : MonoBehaviour 
{
	public Transform itemContainer;
	public Transform waypointContainer;
	public Forge forge;
	private TweenWaypoint[] waypoints;
	private List<TweenWrapper> activeWrappers;
	public ObjectPool pool;
	public Sequence sequence;
	public HammerTargetAnimator hammerTarget;
	public UIToggler toggler;
	
	public void Awake()
	{
		activeWrappers = new List<TweenWrapper>();
		waypoints = waypointContainer.GetComponentsInChildren<TweenWaypoint>();
		toggler.onShow += OnShow;
	}

	public void Clear()
	{
		while(activeWrappers.Count > 0) RemoveAt(0);
	}

	public void UpdateTargets()
	{
		int wrapperCount = activeWrappers.Count;
		int forgeCount = forge.items.Count;

		for(int i = 0; i < waypoints.Length; i++)
		{
			if(i < wrapperCount) activeWrappers[i].SetTarget(i);
			else if(i < forgeCount) AddAt(i, forge.GetSprite(i));
		}
	}
	
	public void AddAt(int index, Sprite sprite)
	{
		if(index >= waypoints.Length) return;
		TweenWrapper wrapper = GetWrapper(sprite);
		wrapper.SetTarget(index);
		activeWrappers.Add(wrapper);
	}

	public void RemoveAt(int index)
	{
		if(index < 0 && index >= activeWrappers.Count) return;

		if(index == 0) hammerTarget.Remove();
		TweenWrapper temp = activeWrappers[index];
		temp.Remove();
		temp.gameObject.SetActive(false);
		activeWrappers.RemoveAt(index);
		UpdateTargets();
		
	}

	public void HandleClick(int index)
	{
		if(index == 0) InterruptHammerTarget();
		forge.HandleClick(index);
	}

	public void HandleDestroy()
	{
		forge.ProcessFront();
	}

	public TweenWrapper GetWrapper(Sprite sprite)
	{
		TweenWrapper temp = pool.Get().GetComponent<TweenWrapper>();
		temp.parent = this;
		temp.gameObject.SetActive(true);
		temp.waypoints = waypoints;
		temp.transform.SetParent(itemContainer);
		temp.Reset();
		temp.SetImage(sprite);
		return temp;
	}

	public void SetHammerTarget()
	{
		if(activeWrappers.Count > 0)
		{
			hammerTarget.Set(activeWrappers[0].image.sprite);
			activeWrappers[0].ClearImage();
		}
	}

	public void RemoveHammerTarget()
	{
		hammerTarget.Remove();
	}

	public void InterruptHammerTarget()
	{
		hammerTarget.Interrupt();
	}

	public void OnShow()
	{
		forge.SetAsRouterTarget();
	}
}
