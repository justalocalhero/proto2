﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TweenWrapper : MonoBehaviour, IPointerClickHandler 
{

	public TweenWaypoint[] waypoints;
	private int currentWaypoint;
	private int targetWaypoint;
	private Tween tween;
	public ForgePath parent {get; set;}
	public Image image;

	public void Reset()
	{
		currentWaypoint = waypoints.Length - 1;
		transform.localPosition = waypoints[currentWaypoint].transform.localPosition;
	}

	public void Remove()
	{
		tween.Kill();
	}

	public void SetImage(Sprite sprite)
	{
		image.sprite = sprite;
		image.enabled = true;
	}

	public void ClearImage()
	{
		image.enabled = false;
	}

	public void SetTarget(int target)
	{
		if(target < 0 || target >= waypoints.Length) return;
		targetWaypoint = target;
		if(tween == null || !tween.IsActive() || !tween.IsPlaying()) TryMoveDown();
	}

	public void OnTweenComplete()
	{
		currentWaypoint--;
		TryMoveDown();
		TrySetHammerTarget();
	}

	public void TryMoveDown()
	{
		if(currentWaypoint > targetWaypoint)

		tween = transform.DOLocalMove(waypoints[currentWaypoint - 1].transform.localPosition, waypoints[currentWaypoint - 1].duration)
		.SetEase(Ease.Linear)
		.OnComplete(() => OnTweenComplete());
	}

	public void TrySetHammerTarget()
	{
		if(currentWaypoint <= 0)
		{
			parent.SetHammerTarget();
		}
	}

	public void SetDelay(float delay)
	{
		tween = transform.DOLocalMove(transform.localPosition, delay)
		.OnComplete(() => OnTweenComplete());
	}

    public void OnPointerClick(PointerEventData eventData)
    {
        parent.HandleClick(targetWaypoint);
    }
}
