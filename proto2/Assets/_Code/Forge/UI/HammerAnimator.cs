﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class HammerAnimator : MonoBehaviour 
{
	private Sequence hammerSequence;
	private Sequence resetSequence;
	public HammerTargetAnimator target;
	public Image image;
	public Transform backKeyOuter;
	public Transform backKeyInner;
	public Transform downKeyOuter;
	public Transform downKeyInner;
	public Transform reloadKeyOuter;
	public Transform reloadKeyInner;
	public FloatReference upTimeOuter;
	public FloatReference upTimeInner;
	public FloatReference downTimeOuter;
	public FloatReference downTimeInner;
	public FloatReference reloadTimeOuter;
	public FloatReference reloadTimeInner;
	public Transform outerContainer;
	public Transform innerContainer;

	public Forge forge;

	private int hammerCount;
	private bool toHammer;

	public delegate void OnHammerStrike();
	public OnHammerStrike onHammerStrike;

	public void Start()
	{
		CreateSequence();
		ResetCounts();
	}

	public void CreateSequence()
	{
		hammerSequence = DOTween.Sequence().SetAutoKill(false);
		hammerSequence.Pause();

		hammerSequence
		.Append(
			outerContainer.DOLocalRotate(new Vector3(0, 0, backKeyOuter.localRotation.eulerAngles.z), upTimeOuter)
		)
		.Join(
			innerContainer.DOLocalRotate(new Vector3(0, 0, backKeyInner.localRotation.eulerAngles.z), upTimeInner)
		)
		.Append(
			outerContainer.DOLocalRotate(new Vector3(0, 0, downKeyOuter.localRotation.eulerAngles.z), downTimeOuter)
			.OnComplete(() => HammerStrike())
		)
		.Join(
			innerContainer.DOLocalRotate(new Vector3(0, 0, downKeyInner.localRotation.eulerAngles.z), downTimeInner)
		)
		.Append(
			outerContainer.DOLocalRotate(new Vector3(0, 0, reloadKeyOuter.localRotation.eulerAngles.z), reloadTimeOuter)
			.OnComplete(() => CheckRestart())
		)
		.Join(
			innerContainer.DOLocalRotate(new Vector3(0, 0, reloadKeyInner.localRotation.eulerAngles.z), reloadTimeInner)
		);
	}

	public void Play()
	{
		resetSequence.Pause();
		hammerSequence.Restart();
	}

	public void Interrupt()
	{	
		ResetCounts();
		hammerSequence.Pause();
		resetSequence = DOTween.Sequence().SetAutoKill(false);

		resetSequence
		.Append(				
			outerContainer.DOLocalRotate(new Vector3(0, 0, 0), .5f)
			.OnComplete(() => CheckRestart())
		)
		.Join(			
			innerContainer.DOLocalRotate(new Vector3(0, 0, 0), .25f)
		);
		resetSequence.Restart();
	}

	public void HammerStrike()
	{
		hammerCount++;
		if(hammerCount >= forge.maxHammerCount.Value)
		{
			SetToHammer(false);
			ResetCounts();
			target.Destroy();
		}
		else
		{
			target.Hit();
		}
		RaiseOnHammerStrike();
	}

	public void Reset()
	{
		Interrupt();
		ResetCounts();
	}

	public void ResetCounts()
	{
		hammerCount = 0;
	}

	public void SetToHammer(bool toHammer)
	{
		this.toHammer = toHammer;
		CheckRestart();
	}

	public void CheckRestart()
	{		
		if(!IsActive() && toHammer) Play();
	}

	public bool IsHammering()
	{
		return hammerSequence.IsPlaying();
	}

	public bool IsResetting()
	{
		return !(resetSequence == null || !resetSequence.IsActive() || !resetSequence.IsPlaying());
	}

	public bool IsActive()
	{
		return IsHammering() || IsResetting();
	}

	public void RaiseOnHammerStrike()
	{
		if(onHammerStrike != null) onHammerStrike();
	}
}
