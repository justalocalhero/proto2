﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDismantler : MonoBehaviour 
{
	public FloatVariable dismantleMultiplier;

	public List<Resource> GetResources(Item item)
	{

		List<Resource> toReturn = new List<Resource>();

		if(!(item is Equipment)) return toReturn;
		
		float multiplier = dismantleMultiplier.Value;

		foreach(Resource resource in (item as Equipment).GetDismantleResources())
		{			
			toReturn.Add(new Resource 
			{
				type = resource.type,
				value = Utility.RandomRound(-resource.value * multiplier)
			});
		}

		return toReturn;
	}
}
