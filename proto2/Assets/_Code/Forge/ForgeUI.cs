﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ForgeUI : MonoBehaviour 
{

	public Forge forge;
	
	public Toggle autoForgeToggle;
	public TextMeshProUGUI autoForgeLevelMesh;

	public UIToggler toggler;
	public Inventory inventory;

	public void Awake()
	{
		toggler.onShow += OnShow;
	}

	public void OnAutoforgeTogglePressed()
	{
		forge.SetAutoForge(autoForgeToggle.isOn);
	}

	public void OnLevelLeftPressed()
	{
		forge.DecrementAutoForge();
		autoForgeLevelMesh.SetText("" + forge.GetAutoForgeLevel());
	}

	public void OnLevelRightPressed()
	{
		forge.IncrementAutoForge();
		autoForgeLevelMesh.SetText("" + forge.GetAutoForgeLevel());
	}

	public void OnShow()
	{
		forge.SetAsRouterTarget();
	}
}
