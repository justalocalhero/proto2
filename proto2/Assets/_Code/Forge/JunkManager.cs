﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JunkManager : MonoBehaviour 
{
	public BattlegroundManager battlegroundManager;
	public ResourceManager resourceManager;
	public FloatVariable junkPerKill;

	public void Start()
	{
		battlegroundManager.onEnemyDeath += CollectJunk;
	}

	public void CollectJunk()
	{
		resourceManager.ApplyChange(new Resource
		{
			type = ResourceType.scrap,
			value = Utility.RandomRound(junkPerKill.Value)
		});
	}
}
