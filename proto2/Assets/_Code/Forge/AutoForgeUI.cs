﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AutoForgeUI : MonoBehaviour 
{
	public Forge forge;
	public Button leftButton, rightButton, toggleButton;
	public Image toggleIcon;
	public TextMeshProUGUI levelText;

	public void Awake() 
	{
		leftButton.onClick.AddListener(LeftButton);
		rightButton.onClick.AddListener(RightButton);
		toggleButton.onClick.AddListener(ToggleButton);
		forge.onAutoForgeChanged += UpdateUI;
	}

	public void Start()
	{
		UpdateUI();
	}

	private void LeftButton()
	{
		forge.DecrementAutoForge();
	}

	private void RightButton()
	{
		forge.IncrementAutoForge();
	}

	private void ToggleButton()
	{
		forge.ToggleAutoForge();
	}

	private void UpdateUI()
	{
		levelText.SetText("" + forge.GetAutoForgeLevel());
		SetIconEnabled(forge.IsAutoForging());
	}

	private void SetIconEnabled(bool enabled)
	{
		toggleIcon.enabled = enabled;
	}
}
