﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Forge : MonoBehaviour, IEquipmentReciever
{
	public SpriteManager spriteManager;
	public SaveManager saveManager;

	public List<Equipment> items {get; set;}

	public ResourceManager resources;

	public Inventory inventory;
	public ItemDismantler itemDismantler;
	public EquipmentRouter equipmentRouter;

	public ForgePath forgePath;
	
	public IntVariable maxHammerCount;

	public delegate void OnDismantle();
	public OnDismantle onDismantle;

	public delegate void OnEquipmentDismantled(Equipment equipment);
	public OnEquipmentDismantled onEquipmentDismantled;
	
	public delegate void OnResourceAdded(List<Resource> resource);
	public OnResourceAdded onResourceAdded;

	public delegate void OnAutoForgeChanged();
	public OnAutoForgeChanged onAutoForgeChanged;

	#region singleton

	public static Forge instance;

	private bool autoForgeEnabled;
	private int autoForgeLevel;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Duplicate Forge");
            return;
        }

        instance = this;

		saveManager.onPreSave += Save;
		saveManager.onLoad += Load;
    }

	#endregion

	public void Start()
	{
		Clear();
	}

	public void Save(SaveFile saveFile)
	{
		if(saveFile == null) return;
		if(saveFile.forgeSave == null) return;

		ForgeSave toSave = new ForgeSave();

		toSave.equipment = Utility.CloneList(items);
		toSave.autoForgeEnabled = autoForgeEnabled;
		toSave.autoForgeLevel = autoForgeLevel;

		saveFile.forgeSave = toSave;
	}

	public void Load(SaveFile saveFile)
	{
		if(saveFile == null) return;
		if(saveFile.forgeSave == null) return;

		Clear();

		Add(saveFile.forgeSave.equipment);
		SetAutoForge(saveFile.forgeSave.autoForgeEnabled);
		SetAutoForgeLevel(saveFile.forgeSave.autoForgeLevel);
	}

	public void Clear()
	{
 		items = new List<Equipment>();
		forgePath.Clear();
	}

	public void Recieve(Equipment equipment, Inventory inventory)
	{
		Add(equipment);
		inventory.Remove(equipment);
	}

	public void Add(Equipment newItem)
	{
		if(newItem == null) return;
		if(items.Contains(newItem)) return;
		if(items.Count < 25)
		{
			items.Add(newItem);		
			forgePath.AddAt(items.Count - 1, spriteManager.GetSprite(newItem.GetSprite()));
		}
	}

	public void Add(List<Equipment> newEquipment)
	{
		foreach(Equipment equipment in newEquipment)
		{
			Add(equipment);
		}
	}

	public void RemoveAt(int index)
	{
		
		if(index < 0 || index > items.Count) return;
		
		items.RemoveAt(index);
		forgePath.RemoveAt(index);
		
	}

	public void ProcessFront()
	{
		if(items.Count <= 0) return;
		Item temp = items[0];

		List<Resource> resourcesValue = itemDismantler.GetResources(temp);

		resources.ApplyChange(resourcesValue);
		RemoveAt(0);
		
		RaiseOnResourceAdded(resourcesValue);
		RaiseOnDismantle();
		if(temp is Equipment) RaiseOnEquipmentDismantled(temp as Equipment);
	}

	public void ToggleAutoForge()
	{
		SetAutoForge(!autoForgeEnabled);
	}

	public void SetAutoForge(bool enabled)
	{
		autoForgeEnabled = enabled;
		
		RaiseOnAutoForgeChanged();
	}

	public void SetAutoForgeLevel(int level)
	{
		autoForgeLevel = Mathf.Clamp(level, 0, 99);
		
		RaiseOnAutoForgeChanged();
	}

	public void IncrementAutoForge()
	{
		SetAutoForgeLevel(autoForgeLevel + 1);
	}

	public void DecrementAutoForge()
	{
		SetAutoForgeLevel(autoForgeLevel - 1);
	}


	public bool CheckAutoForge(Item item)
    {
        if(item is Equipment)
        {
            Equipment equipment = item as Equipment;
            bool autoForging = IsAutoForging();
            int autoForgeLevel = GetAutoForgeLevel();
            return autoForging && equipment.level <= autoForgeLevel && equipment.rarity == Rarity.ordinary;
        }
        return false;        
    }
	
	public bool IsAutoForging()
	{
		return autoForgeEnabled;
	}

	public int GetAutoForgeLevel()
	{
		return autoForgeLevel;
	}

	public void HandleClick(int index)
	{
		if(index < 0 || index > items.Count) return;

		Equipment temp = items[index];
		inventory.Add(temp);
		RemoveAt(index);
	}

	public Sprite GetSprite(int index)
	{
		if(index < 0 || index > items.Count) return null;
		
		return spriteManager.GetSprite(items[index].GetSprite());
	}

	public void RaiseOnDismantle()
	{
		if(onDismantle != null) onDismantle();
	}

	public void RaiseOnEquipmentDismantled(Equipment equipment)
	{
		if(onEquipmentDismantled != null) onEquipmentDismantled(equipment);
	}

	public void RaiseOnResourceAdded(List<Resource> resources)
	{
		if(onResourceAdded != null) onResourceAdded(resources);
	}

	public void RaiseOnAutoForgeChanged()
	{
		if(onAutoForgeChanged != null) onAutoForgeChanged();
	}

	public void SetAsRouterTarget()
	{
		equipmentRouter.SetActiveReciever(this);
	}
}

public enum ForgeState {inactive, dismantling, reloading}