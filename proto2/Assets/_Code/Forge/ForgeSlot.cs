﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ForgeSlot : MonoBehaviour 
{
    public SpriteManager spriteManager;
    public Item item;
    public Image icon;
    public ForgeSlot target{get; set;}

	public void Clear()
    {
        item = null;
        icon.sprite = null;
        icon.enabled = false;
    }

    public void Add(Item newItem)
    {
        item = newItem;
        icon.sprite = spriteManager.GetSprite(newItem.GetSprite());
        icon.enabled = true;
    }

    public bool HasNext()
    {
        return target != null;
    }

    public void SetNext(ForgeSlot slot)
    {
        target = slot;
    }
}
